/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *      Sushanth Sankaran
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.common.math.graphs.tests;

import javax.vecmath.Point3d;

import ca.gc.asc_csa.apogy.common.math.graphs.ApogyCommonMathGraphsFactory;
import ca.gc.asc_csa.apogy.common.math.graphs.KDTree;
import junit.framework.TestCase;
import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>KD Tree</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#addNode(double[], java.lang.Object) <em>Add Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#search(double[]) <em>Search</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#removeNode(double[]) <em>Remove Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[]) <em>Find Nearest</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[], int) <em>Find Nearest</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#performRangeSearch(double[], double[]) <em>Perform Range Search</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class KDTreeTest extends TestCase {

	/**
	 * The fixture for this KD Tree test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KDTree fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(KDTreeTest.class);
	}

	/**
	 * Constructs a new KD Tree test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public KDTreeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this KD Tree test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(KDTree fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this KD Tree test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected KDTree getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ApogyCommonMathGraphsFactory.eINSTANCE.createKDTree());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#addNode(double[], java.lang.Object) <em>Add Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#addNode(double[], java.lang.Object)
	 * @generated
	 */
	public void testAddNode__double_Object() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#search(double[]) <em>Search</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#search(double[])
	 * @generated
	 */
	public void testSearch__double() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#removeNode(double[]) <em>Remove Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#removeNode(double[])
	 * @generated
	 */
	public void testRemoveNode__double() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[]) <em>Find Nearest</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[])
	 * @generated
	 */
	public void testFindNearest__double() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[], int) <em>Find Nearest</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#findNearest(double[], int)
	 * @generated
	 */
	public void testFindNearest__double_int() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#performRangeSearch(double[], double[]) <em>Perform Range Search</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#performRangeSearch(double[], double[])
	 * @generated
	 */
	public void testPerformRangeSearch__double_double() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.gc.asc_csa.apogy.common.math.graphs.KDTree#addNode(java.util.List, java.lang.Object) <em>Add Node</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.math.graphs.KDTree#addNode(java.util.List, java.lang.Object)
	 * @generated_NOT
	 */
	public void testAddNode__List_Object() 
	{
		getFixture().setDimension(2);
		
		Point3d point = new Point3d(0, 0, 0);
		
		double[] key = new double[]{point.x,point.y};
		
		// This should run ok
		try 
		{
			assertTrue(getFixture().addNode(key, point));
		} 
		catch (Exception e) 
		{
			fail(e.getMessage());
		}
		
		// This should not work.
		try 
		{
			assertFalse(getFixture().addNode(key, point));
		} 
		catch (Exception e) 
		{
			fail(e.getMessage());
		}
		
		// This should faile.
		try 
		{
			double[] badkey = new double[]{point.x,point.y, 0};
			getFixture().addNode(badkey, point);
			fail("Should throw an exception since key has bad dimensions.");
		} 
		catch (Exception e) 
		{
			// All is good !
		}
	}

} //KDTreeTest
