package ca.gc.asc_csa.apogy.examples.antenna.apogy;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PTU Dish Antenna Apogy System Api Adapter</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * This class is the specialized Apogy API adapter, used for connecting
 * the existing PTU dish antenna example, located at
 * {@link ca.gc.asc_csa.apogy.examples.antenna.PTUDishAntenna},
 * to Apogy; one can override the well-known callback functions to make
 * Apogy perform a variety of useful functions, including initialization,
 * disposal and other features.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.antenna.apogy.ApogyExamplesAntennaApogyPackage#getPTUDishAntennaApogySystemApiAdapter()
 * @model
 * @generated
 */
public interface PTUDishAntennaApogySystemApiAdapter extends ApogySystemApiAdapter
{
} // PTUDishAntennaApogySystemApiAdapter
