package ca.gc.asc_csa.apogy.examples.lander;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lander Simulated</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * This is a specific implementation of the lander, in which all
 * operations are simulated; while there is no physical components
 * interacted with, it attempts to emulate, wherever possible, the
 * actions and results of its real world counterpart.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.lander.ApogyExamplesLanderPackage#getLanderSimulated()
 * @model
 * @generated
 */
public interface LanderSimulated extends Lander {
} // LanderSimulated
