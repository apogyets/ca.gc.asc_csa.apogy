package ca.gc.asc_csa.apogy.examples.robotic_arm.ui.parts;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;
import ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage;
import ca.gc.asc_csa.apogy.examples.robotic_arm.RoboticArmFacade;
import ca.gc.asc_csa.apogy.examples.robotic_arm.ui.composites.RoboticArmControlComposite;

public class RoboticArmExampleDashboardPart extends AbstractSessionBasedPart 
{	
	private RoboticArmControlComposite roboticArmControlComposite;
			
	private Adapter activeRoboticArmAdapter;
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		if(invocatorSession != null)
		{			
			roboticArmControlComposite.setRoboticArm(RoboticArmFacade.INSTANCE.getActiveRoboticArm());		
		}
		else
		{
			roboticArmControlComposite.setRoboticArm(null);
		}		
	}

	@Override
	public void userPostConstruct(MPart mPart) 
	{				
		RoboticArmFacade.INSTANCE.eAdapters().add(getActiveRoboticArmAdapter());		
	}
	
	@Override
	public void userPreDestroy(MPart mPart) 
	{
		RoboticArmFacade.INSTANCE.eAdapters().remove(getActiveRoboticArmAdapter());
	}
	
	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		parent.setLayout(new FillLayout());
		roboticArmControlComposite = new RoboticArmControlComposite(parent, SWT.BORDER);		
		roboticArmControlComposite.setRoboticArm(RoboticArmFacade.INSTANCE.getActiveRoboticArm());
	}
	
	protected Adapter getActiveRoboticArmAdapter() 
	{
		if(activeRoboticArmAdapter == null)
		{
			activeRoboticArmAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof RoboticArmFacade)
					{
						int featureID = msg.getFeatureID(RoboticArmFacade.class);
						switch (featureID) 
						{
							case ApogyExamplesRoboticArmPackage.ROBOTIC_ARM_FACADE__ACTIVE_ROBOTIC_ARM:
																																
								if(roboticArmControlComposite != null && !roboticArmControlComposite.isDisposed())
								{
									// This needs to be done on the UI thread.
									roboticArmControlComposite.getDisplay().asyncExec(new Runnable() {
										
										@Override
										public void run() 
										{											
											roboticArmControlComposite.setRoboticArm(RoboticArmFacade.INSTANCE.getActiveRoboticArm());
										}
									});								
								}
							break;

							default:
							break;
						}								
					}
				}
			};
		}
		return activeRoboticArmAdapter;
	}
}
