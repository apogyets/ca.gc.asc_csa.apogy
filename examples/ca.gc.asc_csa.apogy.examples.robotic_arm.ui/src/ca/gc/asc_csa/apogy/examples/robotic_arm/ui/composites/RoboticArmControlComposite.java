package ca.gc.asc_csa.apogy.examples.robotic_arm.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;
import ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage;
import ca.gc.asc_csa.apogy.examples.robotic_arm.MoveSpeedLevel;
import ca.gc.asc_csa.apogy.examples.robotic_arm.RoboticArm;

public class RoboticArmControlComposite extends Composite
{
	private static final String DEGREE_SYM = "\u00b0";
	private static final String MOVING_STR = "Moving";
	private static final String STOPPED_STR = "Stopped";
	private static final String READY_STR = "Ready";
	private static final String NOT_READY_STR = "Not Ready";

	private DataBindingContext m_bindingContext;	
	private RoboticArm roboticArm;
	
	private Text txtInitialized;
	private Text txtMoving;
	private Text txtWristEditValue;
	private Text txtTurretEditValue;
	private Text txtShoulderEditValue;
	private Text txtElbowEditValue;
	
		
	private TypedElementSimpleUnitsComposite lblTurretValue;
	private TypedElementSimpleUnitsComposite lblElbowValue;
	private TypedElementSimpleUnitsComposite lblShoulderValue;
	private TypedElementSimpleUnitsComposite lblWristValue;
	private Button btnInitialized;
	private Button btnStow;
	private Button btnMoveTo;
	private Combo cmbSpeedMode;
	
	/**
	 * Create the parentComposite.
	 * @param parent
	 * @param style
	 */	
	public RoboticArmControlComposite(Composite parent, int style)
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		this.addDisposeListener(new DisposeListener()
		{
			@Override
			public void widgetDisposed(DisposeEvent e)
			{
				m_bindingContext.dispose();
			}
		});
		
		Group compositeStatus = new Group(this, SWT.BORDER);
		compositeStatus.setText("Status");
		compositeStatus.setLayout(new GridLayout(2, true));
		
		txtInitialized = new Text(compositeStatus, SWT.CENTER | SWT.NO_FOCUS);
		txtInitialized.setEditable(false);
		txtInitialized.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		
		txtMoving = new Text(compositeStatus, SWT.CENTER | SWT.NO_FOCUS);
		txtMoving.setEditable(false);
		txtMoving.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		txtMoving.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent e)
			{
				if (txtMoving.getText().equals(MOVING_STR) == true)
				{
					txtMoving.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
				}
				else if (txtMoving.getText().equals(STOPPED_STR) == true)
				{
					txtMoving.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
				}
			}
		});
		txtMoving.setText(STOPPED_STR);
		
		Group composite = new Group(this, SWT.BORDER);
		composite.setText("High Level Commands");
		composite.setLayout(new GridLayout(3, false));
		
		btnInitialized = new Button(composite, SWT.NONE);
		btnInitialized.setText("Initialize");
		btnInitialized.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				if (roboticArm != null)
				{
					Job initJob = new InitJob();
					
					initJob.schedule();
				}
			}
		});
		btnInitialized.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));
		
		btnStow = new Button(composite, SWT.NONE);
		btnStow.setText("Stow");
		btnStow.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				if (roboticArm != null)
				{
					Job stowJob = new StowJob();
					
					stowJob.schedule();
				}
			}
		});
		
		cmbSpeedMode = new Combo(composite, SWT.READ_ONLY);
		cmbSpeedMode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		cmbSpeedMode.add(MoveSpeedLevel.SLOW.getLiteral());
		cmbSpeedMode.add(MoveSpeedLevel.MEDIUM.getLiteral());
		cmbSpeedMode.add(MoveSpeedLevel.FAST.getLiteral());
			
		cmbSpeedMode.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				if (roboticArm != null)
				{
					String newSpeedMode = cmbSpeedMode.getText();
					MoveSpeedLevel newSpeedLevel = MoveSpeedLevel.get(newSpeedMode);
					
					Job speedJob = new SpeedModeJob(newSpeedLevel);
				
					speedJob.schedule();
				}
			}
		});
				
		Group compositeJoints = new Group(this, SWT.NONE);		
		compositeJoints.setText("Joints");
		compositeJoints.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1));
		compositeJoints.setLayout(new GridLayout(6, true));
		new Label(compositeJoints, SWT.NONE);
		
		Label lblTurret = new Label(compositeJoints, SWT.NONE);
		lblTurret.setText("Turret (" + DEGREE_SYM + ")");
		lblTurret.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));
		
		Label lblShoulder = new Label(compositeJoints, SWT.NONE);
		lblShoulder.setText("Shoulder (" + DEGREE_SYM + ")");
		lblShoulder.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));
		
		Label lblElbow = new Label(compositeJoints, SWT.NONE);
		lblElbow.setText("Elbow (" + DEGREE_SYM + ")");
		lblElbow.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));
		
		Label lblWrist = new Label(compositeJoints,  SWT.NONE);
		lblWrist.setText("Wrist (" + DEGREE_SYM + ")");
		lblWrist.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, false, 1, 1));
		
		Label lblNewLabel = new Label(compositeJoints, SWT.NONE);	
		lblNewLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		Label lblActuals = new Label(compositeJoints,  SWT.NONE);
		lblActuals.setText("Actuals");
		lblActuals.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		// lblTurretValue = formToolkit.createLabel(compositeJoints, "0.0", SWT.CENTER);
		lblTurretValue =  new TypedElementSimpleUnitsComposite(compositeJoints, SWT.BORDER, false, true, "N/A");
		lblTurretValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		lblTurretValue.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
		
		lblShoulderValue = new TypedElementSimpleUnitsComposite(compositeJoints, SWT.BORDER, false, true, "N/A");		
		lblShoulderValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		lblShoulderValue.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));

		lblElbowValue = new TypedElementSimpleUnitsComposite(compositeJoints, SWT.BORDER, false, true, "N/A");
		lblElbowValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		lblElbowValue.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
		
		lblWristValue = new TypedElementSimpleUnitsComposite(compositeJoints, SWT.BORDER, false, true, "N/A");
		lblWristValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		lblWristValue.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
		
		Button btnLoad = new Button(compositeJoints,  SWT.NONE);
		btnLoad.setText("Load");
		btnLoad.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				if(getRoboticArm() != null)
				{
					txtTurretEditValue.setText(Double.toString(getRoboticArm().getTurretAngle()));
					txtShoulderEditValue.setText(Double.toString(getRoboticArm().getShoulderAngle()));
					txtElbowEditValue.setText(Double.toString(getRoboticArm().getElbowAngle()));
					txtWristEditValue.setText(Double.toString(getRoboticArm().getWristAngle()));
				}
			}
		});
		btnLoad.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, true, 1, 1));
		
		Label lblCommanded = new Label(compositeJoints,  SWT.NONE);
		lblCommanded.setText("Commanded");
		lblCommanded.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		
		txtTurretEditValue = new Text(compositeJoints, SWT.CENTER);
		txtTurretEditValue.setText("0.0");
		txtTurretEditValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		txtTurretEditValue.addVerifyListener(new AngleValueVerifyListener());
		
		txtShoulderEditValue = new Text(compositeJoints, SWT.CENTER);
		txtShoulderEditValue.setText("0.0");
		txtShoulderEditValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		txtShoulderEditValue.addVerifyListener(new AngleValueVerifyListener());
		
		txtElbowEditValue = new Text(compositeJoints, SWT.CENTER);
		txtElbowEditValue.setText("0.0");
		txtElbowEditValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		txtElbowEditValue.addVerifyListener(new AngleValueVerifyListener());
		
		txtWristEditValue = new Text(compositeJoints, SWT.CENTER);
		txtWristEditValue.setText("0.0");
		txtWristEditValue.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		txtWristEditValue.addVerifyListener(new AngleValueVerifyListener());
		
		btnMoveTo = new Button(compositeJoints,  SWT.NONE);
		btnMoveTo.setText("MoveTo");
		btnMoveTo.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				if (roboticArm != null)
				{
					double turret = Math.toRadians(Double.parseDouble(txtTurretEditValue.getText()));
					double shoulder = Math.toRadians(Double.parseDouble(txtShoulderEditValue.getText()));
					double elbow = Math.toRadians(Double.parseDouble(txtElbowEditValue.getText()));
					double wrist = Math.toRadians(Double.parseDouble(txtWristEditValue.getText()));
		
					Job moveToJob = new MoveToJob(turret, shoulder, elbow, wrist);
					
					moveToJob.schedule();
				}
			}
		});
		btnMoveTo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		
		txtInitialized.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent e)
			{
				if (txtInitialized.getText().equals(READY_STR) == true)
				{
					txtInitialized.setBackground(SWTResourceManager.getColor(SWT.COLOR_GREEN));
					btnInitialized.setEnabled(false);
					btnStow.setEnabled(true);
					btnMoveTo.setEnabled(true);
					cmbSpeedMode.setEnabled(true);
				}
				else if (txtInitialized.getText().equals(NOT_READY_STR) == true)
				{
					txtInitialized.setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
					btnInitialized.setEnabled(true);
					btnStow.setEnabled(false);
					btnMoveTo.setEnabled(false);
					cmbSpeedMode.setEnabled(false);
				}
			}
		});
		txtInitialized.setText(NOT_READY_STR);
		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
			
	public void setRoboticArm(RoboticArm newRoboticArm)
	{
		this.roboticArm = newRoboticArm;
		
		if( m_bindingContext != null )
		{
			m_bindingContext.dispose();
			m_bindingContext = null;
		}
						
		if( newRoboticArm != null && !isDisposed())
		{
			m_bindingContext = initDataBindings();			
		}		
	}		
	
	public RoboticArm getRoboticArm()
	{
		return roboticArm;
	}
	
	private class AngleValueVerifyListener implements VerifyListener
	{
		@Override
		public void verifyText(VerifyEvent e)
		{
			String oldText = ((Text)e.widget).getText();
			String newText = oldText.substring(0, e.start) + e.text + oldText.substring(e.end);
			
			try
			{
				Double.parseDouble(newText);
			}
			catch(NumberFormatException ex)
			{
				e.text = "";
				e.doit = false;
			}
		}
	}
	
	public class InitJob extends Job
	{
		public InitJob()
		{
			super("Robotic Arm - Initialize");
			
			setSystem(true);
		}

		@Override
		public IStatus run(IProgressMonitor arg0)
		{
			try
			{
				roboticArm.init();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
				
			return Status.OK_STATUS;
		}					
	}
	
	public class StowJob extends Job
	{
		public StowJob()
		{
			super("Robotic Arm - Stow");
			
			setSystem(true);
		}

		@Override
		protected IStatus run(IProgressMonitor arg0)
		{
			try
			{
				roboticArm.stow();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
				
			return Status.OK_STATUS;
		}					
	};
	
	private class SpeedModeJob extends Job
	{
		private final MoveSpeedLevel speedModeLevel;
		
		public SpeedModeJob(MoveSpeedLevel level)
		{
			super("Robotic Arm - Speed Mode Change");
			
			speedModeLevel = level;
			
			setSystem(true);
		}
		
		@Override
		protected IStatus run(IProgressMonitor arg0)
		{
			try
			{
				roboticArm.cmdMoveSpeedLevel(speedModeLevel);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
			
			return Status.OK_STATUS;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected DataBindingContext initDataBindings() 
	{	
		DataBindingContext bindingContext = new DataBindingContext();
		
		// Start binding
		
		m_bindingContext = new DataBindingContext();
	
		// Turret Angle.
		try
		{
			lblTurretValue.setTypedElement(ApogyExamplesRoboticArmPackage.Literals.ROBOTIC_ARM__TURRET_ANGLE, getRoboticArm());
		}
		catch (Throwable t) 
		{
			t.printStackTrace();
		}
		
		// Shoulder Angle.
		try
		{
			lblShoulderValue.setTypedElement(ApogyExamplesRoboticArmPackage.Literals.ROBOTIC_ARM__SHOULDER_ANGLE, getRoboticArm());
		}
		catch (Throwable t) 
		{
			t.printStackTrace();
		}
		
		// ELbow Angle.
		try
		{
			lblElbowValue.setTypedElement(ApogyExamplesRoboticArmPackage.Literals.ROBOTIC_ARM__ELBOW_ANGLE, getRoboticArm());
		}
		catch (Throwable t) 
		{
			t.printStackTrace();
		}
		
		// Wrist Angle.
		try
		{
			lblWristValue.setTypedElement(ApogyExamplesRoboticArmPackage.Literals.ROBOTIC_ARM__WRIST_ANGLE, getRoboticArm());
		}
		catch (Throwable t) 
		{
			t.printStackTrace();
		}
		
		// Enablement.
		IObservableValue<?> observeInitializedValue = EMFProperties.value(ApogyExamplesRoboticArmPackage.Literals.ROBOTIC_ARM__INITIALIZED).observe(getRoboticArm());				
				
		// Stow Enablement
		IObservableValue<?> observeBtnStowEnable = WidgetProperties.enabled().observe(btnStow);
		bindingContext.bindValue(observeBtnStowEnable, observeInitializedValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), new UpdateValueStrategy());	
		
		// Cmd Speed Enablement
		IObservableValue<?> observeCmbSpeedModeEnable = WidgetProperties.enabled().observe(cmbSpeedMode);
		bindingContext.bindValue(observeCmbSpeedModeEnable, observeInitializedValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), new UpdateValueStrategy());	
				
		// Move to
		IObservableValue<?> observeBtnMoveToEnable = WidgetProperties.enabled().observe(btnMoveTo);
		bindingContext.bindValue(observeBtnMoveToEnable, observeInitializedValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), new UpdateValueStrategy());	
	
		
		// Moving
		IObservableValue<?> observeTextTxtMovingObserveWidget = WidgetProperties.text().observe(txtMoving);
		IObservableValue roboticArmArmMovingObserveValue = EMFProperties.value(ApogyExamplesRoboticArmPackage.Literals.ROBOTIC_ARM__ARM_MOVING).observe(getRoboticArm());		
		
		m_bindingContext.bindValue(observeTextTxtMovingObserveWidget, roboticArmArmMovingObserveValue, 
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE).setConverter(new Converter(boolean.class, String.class)
				{		
					@Override
					public Object convert(Object arg0)
					{
						return ((Boolean)arg0).booleanValue() ? MOVING_STR : STOPPED_STR;
					}
				})		
			);
		
		// Ready Enablement
		IObservableValue<?> observeTxtInitializedEnabled = WidgetProperties.enabled().observe(txtInitialized);
		bindingContext.bindValue(observeTxtInitializedEnabled, observeInitializedValue, new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), new UpdateValueStrategy());	
		
		// Ready
		IObservableValue<?> observeTxtInitializedObserveWidget = WidgetProperties.text().observe(txtInitialized);
		m_bindingContext.bindValue(observeTxtInitializedObserveWidget, observeInitializedValue, 
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE).setConverter(new Converter(boolean.class, String.class)
				{		
					@Override
					public Object convert(Object arg0)
					{
						return ((Boolean)arg0).booleanValue() ? READY_STR : NOT_READY_STR;
					}
				})		
			);
		
		
		IObservableValue observeTextCmdMoveSpeedObserveWidget = WidgetProperties.singleSelectionIndex().observe(cmbSpeedMode);
		IObservableValue roboticArmMoveSpeedObserveValue = EMFProperties.value(ApogyExamplesRoboticArmPackage.Literals.ROBOTIC_ARM__SPEED).observe(getRoboticArm());	
		m_bindingContext.bindValue(observeTextCmdMoveSpeedObserveWidget, roboticArmMoveSpeedObserveValue,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE).setConverter(new Converter(MoveSpeedLevel.class, Integer.class)
				{	
					@Override
					public Object convert(Object obj)
					{
						String literal = ((MoveSpeedLevel)obj).getLiteral();
						
						for (int index = 0; index < cmbSpeedMode.getItemCount(); index++)
						{
							if (literal.equals(cmbSpeedMode.getItems()[index]) == true)
							{
								return new Integer(index);
							}
						}
						return new Integer(-1);
					}
				})
			);
		
		
		return bindingContext;
	}
	
	private class MoveToJob extends Job
	{
		private final double turretValue;
		private final double shoulderValue;
		private final double elbowValue;
		private final double wristValue;
		
		public MoveToJob(double turret, double shoulder, double elbow, double wrist)
		{
			super("Robotic Arm - Move To");
			
			turretValue = turret;
			shoulderValue = shoulder;
			elbowValue = elbow;
			wristValue = wrist;
			
			setSystem(true);
		}
		
		@Override
		public IStatus run(IProgressMonitor monitor)
		{
			try
			{
				roboticArm.moveTo(turretValue, shoulderValue, elbowValue, wristValue);
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
			}
			
			return Status.OK_STATUS;
		}
	}
}