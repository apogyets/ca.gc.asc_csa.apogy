package ca.gc.asc_csa.apogy.examples.mobile_platform.apogy;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.core.environment.surface.ui.PoseVariableAnnotation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mobile Platform Apogy System Annotation</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * This class is used to specify an annotation on the
 * Map View of Apogy, corresponding to the position of
 * the mobile platform; in addition, the annotation is
 * capable of representing the platform's pose on the map.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.mobile_platform.apogy.ApogyExamplesMobilePlatformApogyPackage#getMobilePlatformApogySystemAnnotation()
 * @model
 * @generated
 */
public interface MobilePlatformApogySystemAnnotation extends PoseVariableAnnotation {
} // MobilePlatformApogySystemAnnotation
