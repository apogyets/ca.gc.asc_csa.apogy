package ca.gc.asc_csa.apogy.examples.camera.apogy;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PTU Camera Apogy System Api Adapter</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * This class is the Apogy API adapter that is used to
 * interface the existing PTU camera implementations with
 * Apogy; this provides a series of callback methods that
 * can be overridden to implement a variety of useful features.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.examples.camera.apogy.ApogyExamplesCameraApogyPackage#getPTUCameraApogySystemApiAdapter()
 * @model
 * @generated
 */
public interface PTUCameraApogySystemApiAdapter extends ApogySystemApiAdapter
{
} // PTUCameraApogySystemApiAdapter
