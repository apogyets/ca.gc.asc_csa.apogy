/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.examples.robotic_arm;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.examples.robotic_arm.impl.RoboticArmFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Robotic Arm Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.examples.robotic_arm.RoboticArmFacade#getActiveRoboticArm <em>Active Robotic Arm</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage#getRoboticArmFacade()
 * @model
 * @generated
 */
public interface RoboticArmFacade extends EObject 
{
	public RoboticArmFacade INSTANCE = RoboticArmFacadeImpl.getFacade();
	
	/**
	 * Returns the value of the '<em><b>Active Robotic Arm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The currently active RoboticArm.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Active Robotic Arm</em>' reference.
	 * @see #setActiveRoboticArm(RoboticArm)
	 * @see ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage#getRoboticArmFacade_ActiveRoboticArm()
	 * @model transient="true"
	 * @generated
	 */
	RoboticArm getActiveRoboticArm();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.examples.robotic_arm.RoboticArmFacade#getActiveRoboticArm <em>Active Robotic Arm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active Robotic Arm</em>' reference.
	 * @see #getActiveRoboticArm()
	 * @generated
	 */
	void setActiveRoboticArm(RoboticArm value);

} // RoboticArmFacade
