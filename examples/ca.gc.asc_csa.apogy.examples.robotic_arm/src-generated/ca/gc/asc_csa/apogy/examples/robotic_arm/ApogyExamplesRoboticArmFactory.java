package ca.gc.asc_csa.apogy.examples.robotic_arm;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage
 * @generated
 */
public interface ApogyExamplesRoboticArmFactory extends EFactory
{
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	ApogyExamplesRoboticArmFactory eINSTANCE = ca.gc.asc_csa.apogy.examples.robotic_arm.impl.ApogyExamplesRoboticArmFactoryImpl.init();

	/**
	 * Returns a new RoboticArm object, which has the same type as
	 * the given robotic arm.
	 * 
	 * @param roboticArm The robotic arm with a particular implementation. 
	 * @return The new robotic arm, which has the same type as the old one
	 * @generated_NOT
	 */
	RoboticArm makeRoboticArmSameType(RoboticArm roboticArm);
	
	/**
	 * Returns a new object of class '<em>Robotic Arm Stub</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Robotic Arm Stub</em>'.
	 * @generated
	 */
	RoboticArmStub createRoboticArmStub();

	/**
	 * Returns a new object of class '<em>Robotic Arm Simulated</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Robotic Arm Simulated</em>'.
	 * @generated
	 */
	RoboticArmSimulated createRoboticArmSimulated();

	/**
	 * Returns a new object of class '<em>Integrated Robotic Arm</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Integrated Robotic Arm</em>'.
	 * @generated
	 */
	IntegratedRoboticArm createIntegratedRoboticArm();

	/**
	 * Returns a new object of class '<em>Robotic Arm Facade</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Robotic Arm Facade</em>'.
	 * @generated
	 */
	RoboticArmFacade createRoboticArmFacade();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyExamplesRoboticArmPackage getApogyExamplesRoboticArmPackage();

} //ApogyExamplesRoboticArmFactory
