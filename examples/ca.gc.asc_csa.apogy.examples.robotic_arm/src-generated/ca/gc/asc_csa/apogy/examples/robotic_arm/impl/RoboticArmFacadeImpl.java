/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.examples.robotic_arm.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.examples.robotic_arm.ApogyExamplesRoboticArmPackage;
import ca.gc.asc_csa.apogy.examples.robotic_arm.RoboticArm;
import ca.gc.asc_csa.apogy.examples.robotic_arm.RoboticArmFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Robotic Arm Facade</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.examples.robotic_arm.impl.RoboticArmFacadeImpl#getActiveRoboticArm <em>Active Robotic Arm</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoboticArmFacadeImpl extends MinimalEObjectImpl.Container implements RoboticArmFacade 
{
	
	private static RoboticArmFacade instance;
	
	public static RoboticArmFacade getFacade()
	{
		if( instance == null )
		{
			instance = new RoboticArmFacadeImpl(); 			
		}
		return instance; 
	}
	/**
	 * The cached value of the '{@link #getActiveRoboticArm() <em>Active Robotic Arm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveRoboticArm()
	 * @generated
	 * @ordered
	 */
	protected RoboticArm activeRoboticArm;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoboticArmFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyExamplesRoboticArmPackage.Literals.ROBOTIC_ARM_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoboticArm getActiveRoboticArm() {
		if (activeRoboticArm != null && activeRoboticArm.eIsProxy()) {
			InternalEObject oldActiveRoboticArm = (InternalEObject)activeRoboticArm;
			activeRoboticArm = (RoboticArm)eResolveProxy(oldActiveRoboticArm);
			if (activeRoboticArm != oldActiveRoboticArm) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyExamplesRoboticArmPackage.ROBOTIC_ARM_FACADE__ACTIVE_ROBOTIC_ARM, oldActiveRoboticArm, activeRoboticArm));
			}
		}
		return activeRoboticArm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoboticArm basicGetActiveRoboticArm() {
		return activeRoboticArm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveRoboticArm(RoboticArm newActiveRoboticArm) {
		RoboticArm oldActiveRoboticArm = activeRoboticArm;
		activeRoboticArm = newActiveRoboticArm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyExamplesRoboticArmPackage.ROBOTIC_ARM_FACADE__ACTIVE_ROBOTIC_ARM, oldActiveRoboticArm, activeRoboticArm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyExamplesRoboticArmPackage.ROBOTIC_ARM_FACADE__ACTIVE_ROBOTIC_ARM:
				if (resolve) return getActiveRoboticArm();
				return basicGetActiveRoboticArm();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyExamplesRoboticArmPackage.ROBOTIC_ARM_FACADE__ACTIVE_ROBOTIC_ARM:
				setActiveRoboticArm((RoboticArm)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyExamplesRoboticArmPackage.ROBOTIC_ARM_FACADE__ACTIVE_ROBOTIC_ARM:
				setActiveRoboticArm((RoboticArm)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyExamplesRoboticArmPackage.ROBOTIC_ARM_FACADE__ACTIVE_ROBOTIC_ARM:
				return activeRoboticArm != null;
		}
		return super.eIsSet(featureID);
	}

} //RoboticArmFacadeImpl
