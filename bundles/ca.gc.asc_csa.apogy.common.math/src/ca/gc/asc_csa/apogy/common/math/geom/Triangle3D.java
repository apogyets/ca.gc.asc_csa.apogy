package ca.gc.asc_csa.apogy.common.math.geom;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import javax.vecmath.Point3d;


public class Triangle3D {

	public Point3d p0;
	public Point3d p1;
	public Point3d p2;

	public Triangle3D(Point3d p0, Point3d p1, Point3d p2) {
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
	}

}
