package ca.gc.asc_csa.apogy.core.programs.controllers.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.core.programs.controllers.*;
import java.util.List;
import java.util.TreeSet;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersFacade;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersFactory;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.BindedEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.programs.controllers.CenteredLinearInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.CenteredParabolicInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerEdgeTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerStateTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllersConfiguration;
import ca.gc.asc_csa.apogy.core.programs.controllers.CustomInputConditioningPoint;
import ca.gc.asc_csa.apogy.core.programs.controllers.EdgeType;
import ca.gc.asc_csa.apogy.core.programs.controllers.FixedValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.LinearInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding;
import ca.gc.asc_csa.apogy.core.programs.controllers.ParabolicInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.TimeTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.ToggleValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.UserDefinedInputConditioning;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCoreProgramsControllersFactoryImpl extends EFactoryImpl implements ApogyCoreProgramsControllersFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public static ApogyCoreProgramsControllersFactory init()
  {
		try {
			ApogyCoreProgramsControllersFactory theApogyCoreProgramsControllersFactory = (ApogyCoreProgramsControllersFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCoreProgramsControllersPackage.eNS_URI);
			if (theApogyCoreProgramsControllersFactory != null) {
				return theApogyCoreProgramsControllersFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCoreProgramsControllersFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ApogyCoreProgramsControllersFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE: return createApogyCoreProgramsControllersFacade();
			case ApogyCoreProgramsControllersPackage.CONTROLLERS_GROUP: return createControllersGroup();
			case ApogyCoreProgramsControllersPackage.CONTROLLERS_CONFIGURATION: return createControllersConfiguration();
			case ApogyCoreProgramsControllersPackage.OPERATION_CALL_CONTROLLER_BINDING: return createOperationCallControllerBinding();
			case ApogyCoreProgramsControllersPackage.TIME_TRIGGER: return createTimeTrigger();
			case ApogyCoreProgramsControllersPackage.CONTROLLER_EDGE_TRIGGER: return createControllerEdgeTrigger();
			case ApogyCoreProgramsControllersPackage.CONTROLLER_STATE_TRIGGER: return createControllerStateTrigger();
			case ApogyCoreProgramsControllersPackage.BINDED_EDATA_TYPE_ARGUMENT: return createBindedEDataTypeArgument();
			case ApogyCoreProgramsControllersPackage.FIXED_VALUE_SOURCE: return createFixedValueSource();
			case ApogyCoreProgramsControllersPackage.TOGGLE_VALUE_SOURCE: return createToggleValueSource();
			case ApogyCoreProgramsControllersPackage.CONTROLLER_VALUE_SOURCE: return createControllerValueSource();
			case ApogyCoreProgramsControllersPackage.LINEAR_INPUT_CONDITIONING: return createLinearInputConditioning();
			case ApogyCoreProgramsControllersPackage.CENTERED_LINEAR_INPUT_CONDITIONING: return createCenteredLinearInputConditioning();
			case ApogyCoreProgramsControllersPackage.PARABOLIC_INPUT_CONDITIONING: return createParabolicInputConditioning();
			case ApogyCoreProgramsControllersPackage.CENTERED_PARABOLIC_INPUT_CONDITIONING: return createCenteredParabolicInputConditioning();
			case ApogyCoreProgramsControllersPackage.USER_DEFINED_INPUT_CONDITIONING: return createUserDefinedInputConditioning();
			case ApogyCoreProgramsControllersPackage.CUSTOM_INPUT_CONDITIONING_POINT: return createCustomInputConditioningPoint();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
		switch (eDataType.getClassifierID()) {
			case ApogyCoreProgramsControllersPackage.EDGE_TYPE:
				return createEdgeTypeFromString(eDataType, initialValue);
			case ApogyCoreProgramsControllersPackage.LIST:
				return createListFromString(eDataType, initialValue);
			case ApogyCoreProgramsControllersPackage.TREE_SET:
				return createTreeSetFromString(eDataType, initialValue);
			case ApogyCoreProgramsControllersPackage.THREAD:
				return createThreadFromString(eDataType, initialValue);
			case ApogyCoreProgramsControllersPackage.ADAPTER:
				return createAdapterFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
		switch (eDataType.getClassifierID()) {
			case ApogyCoreProgramsControllersPackage.EDGE_TYPE:
				return convertEdgeTypeToString(eDataType, instanceValue);
			case ApogyCoreProgramsControllersPackage.LIST:
				return convertListToString(eDataType, instanceValue);
			case ApogyCoreProgramsControllersPackage.TREE_SET:
				return convertTreeSetToString(eDataType, instanceValue);
			case ApogyCoreProgramsControllersPackage.THREAD:
				return convertThreadToString(eDataType, instanceValue);
			case ApogyCoreProgramsControllersPackage.ADAPTER:
				return convertAdapterToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreProgramsControllersFacade createApogyCoreProgramsControllersFacade() {
		ApogyCoreProgramsControllersFacadeImpl apogyCoreProgramsControllersFacade = new ApogyCoreProgramsControllersFacadeImpl();
		return apogyCoreProgramsControllersFacade;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ControllersGroup createControllersGroup() {
		ControllersGroupImpl controllersGroup = new ControllersGroupImpl();
		return controllersGroup;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ControllersConfiguration createControllersConfiguration()
  {
		ControllersConfigurationImpl controllersConfiguration = new ControllersConfigurationImpl();
		return controllersConfiguration;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public OperationCallControllerBinding createOperationCallControllerBinding()
  {
		OperationCallControllerBindingImpl operationCallControllerBinding = new OperationCallControllerBindingImpl();
		return operationCallControllerBinding;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public TimeTrigger createTimeTrigger()
  {
		TimeTriggerImpl timeTrigger = new TimeTriggerImpl();
		return timeTrigger;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ControllerEdgeTrigger createControllerEdgeTrigger()
  {
		ControllerEdgeTriggerImpl controllerEdgeTrigger = new ControllerEdgeTriggerImpl();
		return controllerEdgeTrigger;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ControllerStateTrigger createControllerStateTrigger()
  {
		ControllerStateTriggerImpl controllerStateTrigger = new ControllerStateTriggerImpl();
		return controllerStateTrigger;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public BindedEDataTypeArgument createBindedEDataTypeArgument()
  {
		BindedEDataTypeArgumentImpl bindedEDataTypeArgument = new BindedEDataTypeArgumentImpl();
		return bindedEDataTypeArgument;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public FixedValueSource createFixedValueSource()
  {
		FixedValueSourceImpl fixedValueSource = new FixedValueSourceImpl();
		return fixedValueSource;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ToggleValueSource createToggleValueSource()
  {
		ToggleValueSourceImpl toggleValueSource = new ToggleValueSourceImpl();
		return toggleValueSource;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ControllerValueSource createControllerValueSource()
  {
		ControllerValueSourceImpl controllerValueSource = new ControllerValueSourceImpl();
		return controllerValueSource;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public LinearInputConditioning createLinearInputConditioning() {
		LinearInputConditioningImpl linearInputConditioning = new LinearInputConditioningImpl();
		return linearInputConditioning;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public CenteredLinearInputConditioning createCenteredLinearInputConditioning() {
		CenteredLinearInputConditioningImpl centeredLinearInputConditioning = new CenteredLinearInputConditioningImpl();
		return centeredLinearInputConditioning;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ParabolicInputConditioning createParabolicInputConditioning() {
		ParabolicInputConditioningImpl parabolicInputConditioning = new ParabolicInputConditioningImpl();
		return parabolicInputConditioning;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public CenteredParabolicInputConditioning createCenteredParabolicInputConditioning() {
		CenteredParabolicInputConditioningImpl centeredParabolicInputConditioning = new CenteredParabolicInputConditioningImpl();
		return centeredParabolicInputConditioning;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public UserDefinedInputConditioning createUserDefinedInputConditioning() {
		UserDefinedInputConditioningImpl userDefinedInputConditioning = new UserDefinedInputConditioningImpl();
		return userDefinedInputConditioning;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public CustomInputConditioningPoint createCustomInputConditioningPoint() {
		CustomInputConditioningPointImpl customInputConditioningPoint = new CustomInputConditioningPointImpl();
		return customInputConditioningPoint;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EdgeType createEdgeTypeFromString(EDataType eDataType, String initialValue)
  {
		EdgeType result = EdgeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public String convertEdgeTypeToString(EDataType eDataType, Object instanceValue)
  {
		return instanceValue == null ? null : instanceValue.toString();
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public List<?> createListFromString(EDataType eDataType, String initialValue) {
		return (List<?>)super.createFromString(initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public TreeSet<?> createTreeSetFromString(EDataType eDataType, String initialValue) {
		return (TreeSet<?>)super.createFromString(initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertTreeSetToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Thread createThreadFromString(EDataType eDataType, String initialValue) {
		return (Thread)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertThreadToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Adapter createAdapterFromString(EDataType eDataType, String initialValue) {
		return (Adapter)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertAdapterToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreProgramsControllersPackage getApogyCoreProgramsControllersPackage() {
		return (ApogyCoreProgramsControllersPackage)getEPackage();
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static ApogyCoreProgramsControllersPackage getPackage()
  {
		return ApogyCoreProgramsControllersPackage.eINSTANCE;
	}

} //ApogyCoreProgramsControllersFactoryImpl
