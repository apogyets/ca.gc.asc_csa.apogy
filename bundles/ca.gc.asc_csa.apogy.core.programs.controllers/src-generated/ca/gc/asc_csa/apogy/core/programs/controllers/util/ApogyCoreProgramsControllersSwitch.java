package ca.gc.asc_csa.apogy.core.programs.controllers.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Disposable;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.Startable;
import ca.gc.asc_csa.apogy.core.invocator.Argument;
import ca.gc.asc_csa.apogy.core.invocator.EDataTypeArgument;
import ca.gc.asc_csa.apogy.core.invocator.OperationCall;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallContainer;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramsGroup;
import ca.gc.asc_csa.apogy.core.invocator.TriggeredBasedProgram;
import ca.gc.asc_csa.apogy.core.invocator.TriggeredBasedProgramsGroup;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.programs.controllers.*;
import ca.gc.asc_csa.apogy.core.programs.controllers.AbstractInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersFacade;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.BindedEDataTypeArgument;
import ca.gc.asc_csa.apogy.core.programs.controllers.CenteredLinearInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.CenteredParabolicInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerEdgeTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerStateTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllersConfiguration;
import ca.gc.asc_csa.apogy.core.programs.controllers.CustomInputConditioningPoint;
import ca.gc.asc_csa.apogy.core.programs.controllers.FixedValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.LinearInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding;
import ca.gc.asc_csa.apogy.core.programs.controllers.ParabolicInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.TimeTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.ToggleValueSource;
import ca.gc.asc_csa.apogy.core.programs.controllers.Trigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.UserDefinedInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.ValueSource;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage
 * @generated
 */
public class ApogyCoreProgramsControllersSwitch<T1> extends Switch<T1> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected static ApogyCoreProgramsControllersPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCoreProgramsControllersSwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyCoreProgramsControllersPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T1 doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyCoreProgramsControllersPackage.APOGY_CORE_PROGRAMS_CONTROLLERS_FACADE: {
				ApogyCoreProgramsControllersFacade apogyCoreProgramsControllersFacade = (ApogyCoreProgramsControllersFacade)theEObject;
				T1 result = caseApogyCoreProgramsControllersFacade(apogyCoreProgramsControllersFacade);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CONTROLLERS_GROUP: {
				ControllersGroup controllersGroup = (ControllersGroup)theEObject;
				T1 result = caseControllersGroup(controllersGroup);
				if (result == null) result = caseTriggeredBasedProgramsGroup(controllersGroup);
				if (result == null) result = caseProgramsGroup(controllersGroup);
				if (result == null) result = caseNamed(controllersGroup);
				if (result == null) result = caseDescribed(controllersGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CONTROLLERS_CONFIGURATION: {
				ControllersConfiguration controllersConfiguration = (ControllersConfiguration)theEObject;
				T1 result = caseControllersConfiguration(controllersConfiguration);
				if (result == null) result = caseTriggeredBasedProgram(controllersConfiguration);
				if (result == null) result = caseOperationCallContainer(controllersConfiguration);
				if (result == null) result = caseProgram(controllersConfiguration);
				if (result == null) result = caseNamed(controllersConfiguration);
				if (result == null) result = caseDescribed(controllersConfiguration);
				if (result == null) result = caseStartable(controllersConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.OPERATION_CALL_CONTROLLER_BINDING: {
				OperationCallControllerBinding operationCallControllerBinding = (OperationCallControllerBinding)theEObject;
				T1 result = caseOperationCallControllerBinding(operationCallControllerBinding);
				if (result == null) result = caseOperationCall(operationCallControllerBinding);
				if (result == null) result = caseStartable(operationCallControllerBinding);
				if (result == null) result = caseDisposable(operationCallControllerBinding);
				if (result == null) result = caseVariableFeatureReference(operationCallControllerBinding);
				if (result == null) result = caseDescribed(operationCallControllerBinding);
				if (result == null) result = caseNamed(operationCallControllerBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.TRIGGER: {
				Trigger trigger = (Trigger)theEObject;
				T1 result = caseTrigger(trigger);
				if (result == null) result = caseNamed(trigger);
				if (result == null) result = caseDescribed(trigger);
				if (result == null) result = caseStartable(trigger);
				if (result == null) result = caseDisposable(trigger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.TIME_TRIGGER: {
				TimeTrigger timeTrigger = (TimeTrigger)theEObject;
				T1 result = caseTimeTrigger(timeTrigger);
				if (result == null) result = caseTrigger(timeTrigger);
				if (result == null) result = caseNamed(timeTrigger);
				if (result == null) result = caseDescribed(timeTrigger);
				if (result == null) result = caseStartable(timeTrigger);
				if (result == null) result = caseDisposable(timeTrigger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CONTROLLER_TRIGGER: {
				ControllerTrigger controllerTrigger = (ControllerTrigger)theEObject;
				T1 result = caseControllerTrigger(controllerTrigger);
				if (result == null) result = caseTrigger(controllerTrigger);
				if (result == null) result = caseNamed(controllerTrigger);
				if (result == null) result = caseDescribed(controllerTrigger);
				if (result == null) result = caseStartable(controllerTrigger);
				if (result == null) result = caseDisposable(controllerTrigger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CONTROLLER_EDGE_TRIGGER: {
				ControllerEdgeTrigger controllerEdgeTrigger = (ControllerEdgeTrigger)theEObject;
				T1 result = caseControllerEdgeTrigger(controllerEdgeTrigger);
				if (result == null) result = caseControllerTrigger(controllerEdgeTrigger);
				if (result == null) result = caseTrigger(controllerEdgeTrigger);
				if (result == null) result = caseNamed(controllerEdgeTrigger);
				if (result == null) result = caseDescribed(controllerEdgeTrigger);
				if (result == null) result = caseStartable(controllerEdgeTrigger);
				if (result == null) result = caseDisposable(controllerEdgeTrigger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CONTROLLER_STATE_TRIGGER: {
				ControllerStateTrigger controllerStateTrigger = (ControllerStateTrigger)theEObject;
				T1 result = caseControllerStateTrigger(controllerStateTrigger);
				if (result == null) result = caseControllerTrigger(controllerStateTrigger);
				if (result == null) result = caseTrigger(controllerStateTrigger);
				if (result == null) result = caseNamed(controllerStateTrigger);
				if (result == null) result = caseDescribed(controllerStateTrigger);
				if (result == null) result = caseStartable(controllerStateTrigger);
				if (result == null) result = caseDisposable(controllerStateTrigger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.BINDED_EDATA_TYPE_ARGUMENT: {
				BindedEDataTypeArgument bindedEDataTypeArgument = (BindedEDataTypeArgument)theEObject;
				T1 result = caseBindedEDataTypeArgument(bindedEDataTypeArgument);
				if (result == null) result = caseArgument(bindedEDataTypeArgument);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.VALUE_SOURCE: {
				ValueSource valueSource = (ValueSource)theEObject;
				T1 result = caseValueSource(valueSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.FIXED_VALUE_SOURCE: {
				FixedValueSource fixedValueSource = (FixedValueSource)theEObject;
				T1 result = caseFixedValueSource(fixedValueSource);
				if (result == null) result = caseEDataTypeArgument(fixedValueSource);
				if (result == null) result = caseValueSource(fixedValueSource);
				if (result == null) result = caseArgument(fixedValueSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.TOGGLE_VALUE_SOURCE: {
				ToggleValueSource toggleValueSource = (ToggleValueSource)theEObject;
				T1 result = caseToggleValueSource(toggleValueSource);
				if (result == null) result = caseValueSource(toggleValueSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CONTROLLER_VALUE_SOURCE: {
				ControllerValueSource controllerValueSource = (ControllerValueSource)theEObject;
				T1 result = caseControllerValueSource(controllerValueSource);
				if (result == null) result = caseValueSource(controllerValueSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.ABSTRACT_INPUT_CONDITIONING: {
				AbstractInputConditioning abstractInputConditioning = (AbstractInputConditioning)theEObject;
				T1 result = caseAbstractInputConditioning(abstractInputConditioning);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.LINEAR_INPUT_CONDITIONING: {
				LinearInputConditioning linearInputConditioning = (LinearInputConditioning)theEObject;
				T1 result = caseLinearInputConditioning(linearInputConditioning);
				if (result == null) result = caseAbstractInputConditioning(linearInputConditioning);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CENTERED_LINEAR_INPUT_CONDITIONING: {
				CenteredLinearInputConditioning centeredLinearInputConditioning = (CenteredLinearInputConditioning)theEObject;
				T1 result = caseCenteredLinearInputConditioning(centeredLinearInputConditioning);
				if (result == null) result = caseLinearInputConditioning(centeredLinearInputConditioning);
				if (result == null) result = caseAbstractInputConditioning(centeredLinearInputConditioning);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.PARABOLIC_INPUT_CONDITIONING: {
				ParabolicInputConditioning parabolicInputConditioning = (ParabolicInputConditioning)theEObject;
				T1 result = caseParabolicInputConditioning(parabolicInputConditioning);
				if (result == null) result = caseAbstractInputConditioning(parabolicInputConditioning);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CENTERED_PARABOLIC_INPUT_CONDITIONING: {
				CenteredParabolicInputConditioning centeredParabolicInputConditioning = (CenteredParabolicInputConditioning)theEObject;
				T1 result = caseCenteredParabolicInputConditioning(centeredParabolicInputConditioning);
				if (result == null) result = caseParabolicInputConditioning(centeredParabolicInputConditioning);
				if (result == null) result = caseAbstractInputConditioning(centeredParabolicInputConditioning);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.USER_DEFINED_INPUT_CONDITIONING: {
				UserDefinedInputConditioning userDefinedInputConditioning = (UserDefinedInputConditioning)theEObject;
				T1 result = caseUserDefinedInputConditioning(userDefinedInputConditioning);
				if (result == null) result = caseAbstractInputConditioning(userDefinedInputConditioning);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreProgramsControllersPackage.CUSTOM_INPUT_CONDITIONING_POINT: {
				CustomInputConditioningPoint customInputConditioningPoint = (CustomInputConditioningPoint)theEObject;
				T1 result = caseCustomInputConditioningPoint(customInputConditioningPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseApogyCoreProgramsControllersFacade(ApogyCoreProgramsControllersFacade object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controllers Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controllers Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseControllersGroup(ControllersGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controllers Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controllers Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseControllersConfiguration(ControllersConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call Controller Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call Controller Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseOperationCallControllerBinding(OperationCallControllerBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTrigger(Trigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTimeTrigger(TimeTrigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controller Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controller Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseControllerTrigger(ControllerTrigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controller Edge Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controller Edge Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseControllerEdgeTrigger(ControllerEdgeTrigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controller State Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controller State Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseControllerStateTrigger(ControllerStateTrigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binded EData Type Argument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binded EData Type Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseBindedEDataTypeArgument(BindedEDataTypeArgument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseValueSource(ValueSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Value Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Value Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseFixedValueSource(FixedValueSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Toggle Value Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Toggle Value Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseToggleValueSource(ToggleValueSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Controller Value Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Controller Value Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseControllerValueSource(ControllerValueSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Input Conditioning</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Input Conditioning</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractInputConditioning(AbstractInputConditioning object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Linear Input Conditioning</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Linear Input Conditioning</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseLinearInputConditioning(LinearInputConditioning object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Centered Linear Input Conditioning</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Centered Linear Input Conditioning</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCenteredLinearInputConditioning(CenteredLinearInputConditioning object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parabolic Input Conditioning</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parabolic Input Conditioning</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseParabolicInputConditioning(ParabolicInputConditioning object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Centered Parabolic Input Conditioning</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Centered Parabolic Input Conditioning</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCenteredParabolicInputConditioning(CenteredParabolicInputConditioning object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Defined Input Conditioning</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Defined Input Conditioning</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseUserDefinedInputConditioning(UserDefinedInputConditioning object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Input Conditioning Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Input Conditioning Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCustomInputConditioningPoint(CustomInputConditioningPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Described</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDescribed(Described object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Programs Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Programs Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T extends Program> T1 caseProgramsGroup(ProgramsGroup<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Triggered Based Programs Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triggered Based Programs Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTriggeredBasedProgramsGroup(TriggeredBasedProgramsGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Program</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Program</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseProgram(Program object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Triggered Based Program</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Triggered Based Program</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTriggeredBasedProgram(TriggeredBasedProgram object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseOperationCallContainer(OperationCallContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Feature Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Feature Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseVariableFeatureReference(VariableFeatureReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseOperationCall(OperationCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Startable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Startable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseStartable(Startable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Disposable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Disposable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseDisposable(Disposable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Argument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseArgument(Argument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EData Type Argument</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EData Type Argument</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseEDataTypeArgument(EDataTypeArgument object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T1 defaultCase(EObject object) {
		return null;
	}

} //ApogyCoreProgramsControllersSwitch
