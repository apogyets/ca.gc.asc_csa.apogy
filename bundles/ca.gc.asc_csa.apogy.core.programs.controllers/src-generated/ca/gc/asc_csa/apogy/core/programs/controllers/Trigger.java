package ca.gc.asc_csa.apogy.core.programs.controllers;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Disposable;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.Startable;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trigger</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * 
 * Trigger : Defines a trigger that invokes a OperationControllerBinding
 * update.
 * 
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.programs.controllers.Trigger#getOperationCallControllerBinding <em>Operation Call Controller Binding</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage#getTrigger()
 * @model abstract="true"
 * @generated
 */
public interface Trigger extends Named, Described, Startable, Disposable
{
  /**
	 * Returns the value of the '<em><b>Operation Call Controller Binding</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Operation Call Controller Binding</em>' container reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->	 * @return the value of the '<em>Operation Call Controller Binding</em>' container reference.
	 * @see #setOperationCallControllerBinding(OperationCallControllerBinding)
	 * @see ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage#getTrigger_OperationCallControllerBinding()
	 * @see ca.gc.asc_csa.apogy.core.programs.controllers.OperationCallControllerBinding#getTrigger
	 * @model opposite="trigger" transient="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='None'"
	 * @generated
	 */
  OperationCallControllerBinding getOperationCallControllerBinding();

  /**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.programs.controllers.Trigger#getOperationCallControllerBinding <em>Operation Call Controller Binding</em>}' container reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @param value the new value of the '<em>Operation Call Controller Binding</em>' container reference.
	 * @see #getOperationCallControllerBinding()
	 * @generated
	 */
  void setOperationCallControllerBinding(OperationCallControllerBinding value);

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Start the Trigger monitoring
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void start();

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Stops trigger monitoring.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void stop();

} // Trigger
