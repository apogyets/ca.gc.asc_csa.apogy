package ca.gc.asc_csa.apogy.core.programs.controllers.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.io.jinput.Activator;
import ca.gc.asc_csa.apogy.common.io.jinput.ApogyCommonIOJInputPackage;
import ca.gc.asc_csa.apogy.common.io.jinput.EComponent;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.ControllerEdgeTrigger;
import ca.gc.asc_csa.apogy.core.programs.controllers.EdgeType;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Controller Edge Trigger</b></em>'. <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.programs.controllers.impl.ControllerEdgeTriggerImpl#getEdgeType <em>Edge Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ControllerEdgeTriggerImpl extends ControllerTriggerImpl implements ControllerEdgeTrigger 
{
	private Adapter tmpAdapter;
	boolean locked = false;
	boolean oldValue = false;	
	
	/**
	 * The default value of the '{@link #getEdgeType() <em>Edge Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @see #getEdgeType()
	 * @generated
	 * @ordered
	 */
	protected static final EdgeType EDGE_TYPE_EDEFAULT = EdgeType.RISING;
	/**
	 * The cached value of the '{@link #getEdgeType() <em>Edge Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @see #getEdgeType()
	 * @generated
	 * @ordered
	 */
	protected EdgeType edgeType = EDGE_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	protected ControllerEdgeTriggerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_EDGE_TRIGGER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	public EdgeType getEdgeType() {
		return edgeType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	public void setEdgeType(EdgeType newEdgeType) {
		EdgeType oldEdgeType = edgeType;
		edgeType = newEdgeType == null ? EDGE_TYPE_EDEFAULT : newEdgeType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreProgramsControllersPackage.CONTROLLER_EDGE_TRIGGER__EDGE_TYPE, oldEdgeType, edgeType));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_EDGE_TRIGGER__EDGE_TYPE:
				return getEdgeType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_EDGE_TRIGGER__EDGE_TYPE:
				setEdgeType((EdgeType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_EDGE_TRIGGER__EDGE_TYPE:
				setEdgeType(EDGE_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreProgramsControllersPackage.CONTROLLER_EDGE_TRIGGER__EDGE_TYPE:
				return edgeType != EDGE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (edgeType: ");
		result.append(edgeType);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public Adapter getAdapter() 
	{		
		if (tmpAdapter == null) 
		{
			tmpAdapter = new MyAdapter();
	
			// ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCoreProgramsControllersPackage.Literals.CONTROLLER_TRIGGER__ADAPTER, adapter);
		}
		return tmpAdapter;
	}
	
	@Override
	public void start() 
	{	
		super.start();
	}
	
	@Override
	public void stop() {		
		super.stop();
	}
	
	@Override
	protected void update(boolean oldValue, boolean newValue) 
	{				
		try 
		{			
			switch (getEdgeType().getValue()) 
			{
				case EdgeType.RISING_VALUE: 
				{
					if (!oldValue && newValue) 
					{
						getOperationCallControllerBinding().update();
					}
				}
				break;
	
				case EdgeType.FALLING_VALUE: 
				{
					if (oldValue && !newValue) 
					{
						getOperationCallControllerBinding().update();
					}
				}
				break;
	
				case EdgeType.BOTH_VALUE: 
				{
					getOperationCallControllerBinding().update();
				}
				break;
	
				default:
				break;
			}
		} 
		catch (Throwable t) 
		{
			t.printStackTrace();
		}
	}
	
	class MyAdapter extends AdapterImpl
	{			
		public MyAdapter() {
			super();		
		}
		
		@Override
		synchronized public void notifyChanged(Notification msg) 
		{												
			locked = false;
			if(!locked)
			{
				locked = true;
				if(getOperationCallControllerBinding().isStarted())
				{
					/** If pollingCount */
					if (msg.getFeatureID(EComponent.class) == ApogyCommonIOJInputPackage.ECONTROLLER_ENVIRONMENT__POLLING_COUNT) 
					{
						EComponent component = Activator.getEControllerEnvironment().resolveEComponent(getComponentQualifier());
						
						/** If there is a component */
						if (component != null && !busy) 
						{
							busy = true;
							float pollData = component.getPollData();
							boolean latest = convert(pollData);
																															
							if (Boolean.logicalXor(oldValue, latest)) 
							{	
								boolean prev = new Boolean(oldValue);
								boolean now = new Boolean(latest);																			
								oldValue = new Boolean(now);			
								
								update(prev, now);											
							}		
							
							busy = false;
						}
					}						
				}
				locked = false;
			}
		}
	}
} // ControllerEdgeTriggerImpl
