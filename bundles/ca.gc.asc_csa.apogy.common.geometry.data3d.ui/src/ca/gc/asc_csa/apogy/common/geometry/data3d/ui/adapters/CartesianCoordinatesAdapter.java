package ca.gc.asc_csa.apogy.common.geometry.data3d.ui.adapters;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianCoordinatesSet;
import ca.gc.asc_csa.apogy.common.geometry.data3d.ui.ICartesianCoordinatesSetProvider;
import ca.gc.asc_csa.apogy.common.topology.ContentNode;

public class CartesianCoordinatesAdapter implements
		ICartesianCoordinatesSetProvider {

	@Override
	public CartesianCoordinatesSet getDataSet(ContentNode<?> contentNode) {
		if (contentNode.getContent() instanceof CartesianCoordinatesSet) {
			CartesianCoordinatesSet content = (CartesianCoordinatesSet) contentNode
					.getContent();
			return content;
		} else {
			return null;
		}
	}

}
