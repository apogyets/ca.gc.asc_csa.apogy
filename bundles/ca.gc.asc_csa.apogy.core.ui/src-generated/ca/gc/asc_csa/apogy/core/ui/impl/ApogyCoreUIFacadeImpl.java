package ca.gc.asc_csa.apogy.core.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestNode;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIFacade;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Facade</b></em>'. <!-- end-user-doc --> *
 * @generated
 */
public class ApogyCoreUIFacadeImpl extends MinimalEObjectImpl.Container
		implements ApogyCoreUIFacade {

	private static ApogyCoreUIFacade instance = null;

	public static ApogyCoreUIFacade getInstance() {
		if (instance == null) {
			instance = new ApogyCoreUIFacadeImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	protected ApogyCoreUIFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreUIPackage.Literals.APOGY_CORE_UI_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public FeatureOfInterestNode getFeatureOfInterestNode(FeatureOfInterest featureOfInterest) 
	{
		if(ApogyCoreTopologyFacade.INSTANCE.getApogyTopology() != null)
		{
			Node root = ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
			if(root != null)
			{
				List<Node> nodes = ApogyCommonTopologyFacade.INSTANCE.findNodesByType(ApogyCorePackage.Literals.FEATURE_OF_INTEREST_NODE, root);				
				
				Iterator<Node> iterator = nodes.iterator();
				while(iterator.hasNext())
				{
					FeatureOfInterestNode next = (FeatureOfInterestNode) iterator.next();
					if(next.getFeatureOfInterest() == featureOfInterest)
					{
						return next;
					}					
				}
			}
		}
		
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCoreUIPackage.APOGY_CORE_UI_FACADE___GET_FEATURE_OF_INTEREST_NODE__FEATUREOFINTEREST:
				return getFeatureOfInterestNode((FeatureOfInterest)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}


} // ApogyCoreUIFacadeImpl
