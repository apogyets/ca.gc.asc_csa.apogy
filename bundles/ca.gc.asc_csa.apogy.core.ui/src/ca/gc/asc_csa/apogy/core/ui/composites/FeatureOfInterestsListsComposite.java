package ca.gc.asc_csa.apogy.core.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestList;
import ca.gc.asc_csa.apogy.core.ui.Activator;

public class FeatureOfInterestsListsComposite extends Composite
{
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNewFOIList;
	private Button btnNewFOI;	
	private Button btnDelete;
	
	private List<FeatureOfInterestList> featureOfInterestLists = new ArrayList<FeatureOfInterestList>();
	
	private ISelectionChangedListener treeViewerSelectionChangedListener;
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	public FeatureOfInterestsListsComposite(Composite parent, int style) 
	{
		super(parent, style);	
		
		setLayout(new GridLayout(2, false));	
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new SessionFeatureOfInterestsContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		
		btnNewFOIList = new Button(composite, SWT.NONE);
		btnNewFOIList.setSize(74, 29);
		btnNewFOIList.setText("New FOI List");
		btnNewFOIList.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNewFOIList.setEnabled(true);
		btnNewFOIList.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				// new WizardDialog(parent.getShell(), new NewToolWizard()).open();
			}
		});
		
		btnNewFOI = new Button(composite, SWT.NONE);
		btnNewFOI.setSize(74, 29);
		btnNewFOI.setText("New FOI");
		btnNewFOI.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNewFOI.setEnabled(false);
		btnNewFOI.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				// new WizardDialog(parent.getShell(), new NewToolWizard()).open();
			}
		});		
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				String deleteMessage = "";
				
				// Delete of FeatureOfInterest.
				List<FeatureOfInterest> foiList = getSelectedFeatureOfInterests();
				if(!foiList.isEmpty())
				{
					Iterator<FeatureOfInterest> it = foiList.iterator();
					
					while(it.hasNext())
					{
						FeatureOfInterest foi = it.next();
						deleteMessage = deleteMessage + foi.getName();
	
						if (it.hasNext()) 
						{
							deleteMessage = deleteMessage + ", ";
						}
					}
					MessageDialog dialog = new MessageDialog(null, "Delete the selected FOI", null,
							"Are you sure to delete these FOI: " + deleteMessage, MessageDialog.QUESTION,
							new String[] { "Yes", "No" }, 1);
					int result = dialog.open();
					if (result == 0) 
					{
						for (FeatureOfInterest foi :  foiList) 
						{
							try 
							{								
								if(foi.eContainer() instanceof FeatureOfInterestList)
								{
									FeatureOfInterestList foiContainer = (FeatureOfInterestList) foi.eContainer();
									ApogyCommonTransactionFacade.INSTANCE.basicRemove(foiContainer, ApogyCorePackage.Literals.FEATURE_OF_INTEREST_LIST__FEATURES_OF_INTEREST, foi);
								}
							} 
							catch (Exception ex)	
							{
								Logger.INSTANCE.log(Activator.ID,
										"Unable to delete the FOI <"+ foi.getName() + ">",
										EventSeverity.ERROR, ex);
							}
						}
						
						if(!treeViewer.isBusy()) treeViewer.refresh();
					}				
				}
				
				// Delete of FeatureOfInterestList
				List<FeatureOfInterestList> foiListList = getSelectedFeatureOfInterestsList();
				if(!foiListList.isEmpty())
				{
					Iterator<FeatureOfInterestList> it = foiListList.iterator();
					
					while(it.hasNext())
					{
						FeatureOfInterestList foi = it.next();
						deleteMessage = deleteMessage + foi.getName();
	
						if (it.hasNext()) 
						{
							deleteMessage = deleteMessage + ", ";
						}
					}
					MessageDialog dialog = new MessageDialog(null, "Delete the selected FOI Lists", null,
							"Are you sure to delete these FOI Lists: " + deleteMessage, MessageDialog.QUESTION,
							new String[] { "Yes", "No" }, 1);
					int result = dialog.open();
					if (result == 0) 
					{
						for (FeatureOfInterestList featureOfInterestList :  foiListList) 
						{
							try 
							{		
								EObject eContainer = featureOfInterestList.eContainer();
								EStructuralFeature eFeature = featureOfInterestList.eContainingFeature();
								if(eFeature.isMany())
								{
									ApogyCommonTransactionFacade.INSTANCE.basicRemove(eContainer, eFeature, featureOfInterestList);
								}
								else
								{
									ApogyCommonTransactionFacade.INSTANCE.basicSet(eContainer, eFeature, null);
								}
								
								FeatureOfInterestsListsComposite.this.featureOfInterestLists.remove(featureOfInterestList);
							} 
							catch (Exception ex)	
							{
								Logger.INSTANCE.log(Activator.ID,
										"Unable to delete the FOI List <"+ featureOfInterestList.getName() + ">",
										EventSeverity.ERROR, ex);
							}
						}
																							
						if(!treeViewer.isBusy())
						{
							treeViewer.setInput(FeatureOfInterestsListsComposite.this.featureOfInterestLists);
							treeViewer.refresh();
						}
					}				
				}
			}
		});
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				treeViewer.removeSelectionChangedListener(getTreeViewerSelectionChangedListener());				
			}
		});
	}
	
	public void setFeatureOfInterestsLists(List<FeatureOfInterestList> featureOfInterestLists)
	{				
		
		this.featureOfInterestLists.clear();		
		if(featureOfInterestLists != null && !featureOfInterestLists.isEmpty())
		{
			this.featureOfInterestLists.addAll(featureOfInterestLists);
		}
		
		treeViewer.removeSelectionChangedListener(getTreeViewerSelectionChangedListener());
		treeViewer.setInput(this.featureOfInterestLists);
		treeViewer.addSelectionChangedListener(getTreeViewerSelectionChangedListener());
	}
	
	public void setSelectedFeatureOfInterest(FeatureOfInterest selectedFOI)
	{
		if(selectedFOI != null)
		{			
			if(selectedFOI.eContainer() instanceof FeatureOfInterestList)
			{
				FeatureOfInterestList featureOfInterestList = (FeatureOfInterestList) selectedFOI.eContainer();
				
				Object[] objects = new Object[]{featureOfInterestList, selectedFOI};
				TreePath treePath = new TreePath(objects);
				TreeSelection treeSelection = new TreeSelection(treePath);
				treeViewer.setSelection(treeSelection, true);	
			}							
		}
	}		
	
	/** 
	 * This method is called when a new selection is made in the parentComposite. 
	 * @param selection Reference to the selection.
	 */
	protected void newSelection(TreeSelection selection)
	{		
	}
	
	public FeatureOfInterest getSelectedFeatureOfInterest()
	{		
		if(treeViewer.getSelection() instanceof StructuredSelection)
		{
			StructuredSelection structuredSelection = (StructuredSelection) treeViewer.getSelection();
			if(structuredSelection.getFirstElement() instanceof FeatureOfInterest)
			{
				return (FeatureOfInterest) structuredSelection.getFirstElement(); 
			}					
		}
		return null;
	}	
	
	@SuppressWarnings("unchecked")
	public List<FeatureOfInterest>  getSelectedFeatureOfInterests()
	{
		List<FeatureOfInterest> list = new ArrayList<FeatureOfInterest>();
		
		if(treeViewer.getSelection() instanceof StructuredSelection)
		{
			StructuredSelection structuredSelection = (StructuredSelection) treeViewer.getSelection();
			List<Object> elements =  structuredSelection.toList();
			for(Object object : elements)
			{
				if(object instanceof FeatureOfInterest)
				{
					list.add((FeatureOfInterest) object);
				}
			}
		}
		return list;
	}
	@SuppressWarnings("unchecked")
	public List<FeatureOfInterestList>  getSelectedFeatureOfInterestsList()
	{
		List<FeatureOfInterestList> list = new ArrayList<FeatureOfInterestList>();
		
		if(treeViewer.getSelection() instanceof StructuredSelection)
		{
			StructuredSelection structuredSelection = (StructuredSelection) treeViewer.getSelection();
			List<Object> elements =  structuredSelection.toList();
			for(Object object : elements)
			{
				if(object instanceof FeatureOfInterestList)
				{
					list.add((FeatureOfInterestList) object);
				}
			}
		}
		return list;
	}
	
	
	private class SessionFeatureOfInterestsContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}

		@SuppressWarnings("rawtypes")
		@Override
		public Object[] getElements(Object inputElement) 
		{						
			Object[] elements = null;				
			if(inputElement instanceof List)
			{								
				return ((List) inputElement).toArray();
			}			
					
			return elements;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			Object[] children = null;			
			if(parentElement instanceof FeatureOfInterestList)
			{
				FeatureOfInterestList featureOfInterestList = (FeatureOfInterestList) parentElement;
				children = featureOfInterestList.getFeaturesOfInterest().toArray();
			}
			return children;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof FeatureOfInterestList)
			{
				FeatureOfInterestList featureOfInterestList = (FeatureOfInterestList) element;
				return !featureOfInterestList.getFeaturesOfInterest().isEmpty();
			}		
			else
			{
				return false;
			}
		}	
	}
	
	private ISelectionChangedListener getTreeViewerSelectionChangedListener() 
	{
		if (treeViewerSelectionChangedListener == null){
			treeViewerSelectionChangedListener = new ISelectionChangedListener() {
				
				@Override
				public void selectionChanged(SelectionChangedEvent event) 
				{					
					TreeSelection treeSelection = (TreeSelection)event.getSelection();
					
					// If a FeatureOfInterestList has been selected.
					if(treeSelection.getFirstElement() instanceof FeatureOfInterestList)
					{
						btnNewFOI.setEnabled(true);
					}
					else
					{
						btnNewFOI.setEnabled(false);
					}
					
					if(treeSelection.isEmpty())
					{
						btnDelete.setEnabled(false);
					}
					else
					{
						btnDelete.setEnabled(true);
					}
										
					FeatureOfInterestsListsComposite.this.newSelection(treeSelection);
				}
			};
		}
		return treeViewerSelectionChangedListener;
	}
}
