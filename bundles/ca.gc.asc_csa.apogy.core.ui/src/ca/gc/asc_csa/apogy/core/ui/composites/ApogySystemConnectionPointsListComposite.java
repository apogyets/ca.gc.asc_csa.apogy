package ca.gc.asc_csa.apogy.core.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ConnectionPoint;
import ca.gc.asc_csa.apogy.core.ConnectionPointsList;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIRCPConstants;


public class ApogySystemConnectionPointsListComposite extends Composite 
{	
	private ConnectionPointsList connectionPointsList;
	
	private boolean enableEditing = true;
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public ApogySystemConnectionPointsListComposite(Composite parent, int style) 
	{
		this(parent, style, true);
	}
	
	public ApogySystemConnectionPointsListComposite(Composite parent, int style, boolean enableEditing) 
	{
		super(parent, style);
		this.enableEditing = enableEditing;
		
		if(enableEditing)
		{
			setLayout(new GridLayout(2, false));
		}
		else
		{
			setLayout(new GridLayout(1, false));
		}
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newConnectionPointSelected((ConnectionPoint)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		if(enableEditing)
		{
			// Buttons.
			Composite composite = new Composite(this, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			composite.setLayout(new GridLayout(1, false));	
			
			btnNew = new Button(composite, SWT.NONE);
			btnNew.setSize(74, 29);
			btnNew.setText("New");
			btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnNew.setEnabled(true);
			btnNew.addListener(SWT.Selection, new Listener() 
			{		
				@Override
				public void handleEvent(Event event) 
				{				
					if (event.type == SWT.Selection) 
					{					
						MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();
						
						String name = ApogyCommonEMFFacade.INSTANCE.getDefaultName(getConnectionPointsList(), null, ApogyCorePackage.Literals.CONNECTION_POINTS_LIST__CONNECTION_POINTS);
						
						settings.getUserDataMap().put(ApogyCoreUIRCPConstants.NAME_ID, name);
						settings.getUserDataMap().put(ApogyCoreUIRCPConstants.APOGY_SYSTEM_ID, getConnectionPointsList().getApogySystem());					
												
						Wizard wizard = new ApogyEObjectWizard(ApogyCorePackage.Literals.CONNECTION_POINTS_LIST__CONNECTION_POINTS, getConnectionPointsList(), settings, ApogyCorePackage.Literals.CONNECTION_POINT); 
						WizardDialog dialog = new WizardDialog(getShell(), wizard);
						dialog.open();
						
						// Forces the viewer to refresh its input.
						if(!treeViewer.isBusy())
						{					
							treeViewer.setInput(getConnectionPointsList());
						}					
					}
				}
			});
					
			btnDelete = new Button(composite, SWT.NONE);
			btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnDelete.setSize(74, 29);
			btnDelete.setText("Delete");
			btnDelete.setEnabled(false);
			btnDelete.addSelectionListener(new SelectionAdapter() 
			{
				@Override
				public void widgetSelected(SelectionEvent event) 
				{
					String deleteMessage = "";
	
					Iterator<ConnectionPoint> connectionPointIterator = getSelectedConnectionPoints().iterator();
					while (connectionPointIterator.hasNext()) 
					{
						ConnectionPoint connectionPoint = connectionPointIterator.next();
						deleteMessage = deleteMessage + connectionPoint.getName();
	
						if (connectionPointIterator.hasNext()) 
						{
							deleteMessage = deleteMessage + ", ";
						}
					}
	
					MessageDialog dialog = new MessageDialog(null, "Delete the selected Connection Points", null,
							"Are you sure to delete these Connection Points : " + deleteMessage, MessageDialog.QUESTION,
							new String[] { "Yes", "No" }, 1);
					int result = dialog.open();
					if (result == 0) 
					{
						List<ConnectionPoint> toRemove = getSelectedConnectionPoints();
						ApogyCommonTransactionFacade.INSTANCE.basicRemove(connectionPointsList, ApogyCorePackage.Literals.CONNECTION_POINTS_LIST__CONNECTION_POINTS, toRemove);
					}
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getConnectionPointsList());
					}					
				}
			});
		}
				
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	public ConnectionPointsList getConnectionPointsList() {
		return connectionPointsList;
	}

	public void setConnectionPointsList(ConnectionPointsList newConnectionPointsList) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.connectionPointsList = newConnectionPointsList;
		
		if(newConnectionPointsList != null)
		{
			m_bindingContext = customInitDataBindings();
			treeViewer.setInput(newConnectionPointsList);
			
			ConnectionPoint connectionPoint = null;
			
			if(!newConnectionPointsList.getConnectionPoints().isEmpty())
			{
				connectionPoint = newConnectionPointsList.getConnectionPoints().get(0);
			}
			
			if(connectionPoint != null)
			{
				treeViewer.setSelection(new StructuredSelection(connectionPoint), true);
			}
		}
	}

	public void setSelectedConnectionPoint(ConnectionPoint selectedConnectionPoint)
	{
		if(selectedConnectionPoint != null)
		{
			treeViewer.setSelection(new StructuredSelection(selectedConnectionPoint), true);
		}
		else
		{
			treeViewer.setSelection(null, true);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<ConnectionPoint> getSelectedConnectionPoints()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	
	protected void newConnectionPointSelected(ConnectionPoint connectionPoint)
	{		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		if(enableEditing)
		{
			IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
			
			/* Delete Button Enabled Binding. */
			IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
			bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
					new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
							.setConverter(new Converter(Object.class, Boolean.class) {
								@Override
								public Object convert(Object fromObject) {
									return fromObject != null;
								}
							}));
		}
		return bindingContext;
	}
		
	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof ConnectionPointsList)
			{								
				ConnectionPointsList connectionPointsList = (ConnectionPointsList) inputElement;					
				return connectionPointsList.getConnectionPoints().toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof ConnectionPointsList)
			{								
				ConnectionPointsList connectionPointsList = (ConnectionPointsList) parentElement;
				
				// Keeps only CartesianTriangularMeshMapLayers.			
				return connectionPointsList.getConnectionPoints().toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof ConnectionPointsList)
			{
				ConnectionPointsList connectionPointsList = (ConnectionPointsList) element;		
				return connectionPointsList.getConnectionPoints().isEmpty();
			}		
			else
			{
				return false;
			}
		}
	}
}
