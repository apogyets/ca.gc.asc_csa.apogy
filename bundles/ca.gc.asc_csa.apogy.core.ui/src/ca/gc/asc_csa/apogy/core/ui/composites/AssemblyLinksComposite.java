package ca.gc.asc_csa.apogy.core.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.common.topology.ui.composites.TopologyTreeEditingComposite;
import ca.gc.asc_csa.apogy.core.AssemblyLink;
import ca.gc.asc_csa.apogy.core.AssemblyLinksList;

public class AssemblyLinksComposite extends Composite 
{
	private AssemblyLinksList assemblyLinksList;
	
	private AssemblyLinkListComposite assemblyLinkListComposite;
	private AssemblyLinkOverviewComposite assemblyLinkOverviewComposite;
	private TopologyTreeEditingComposite topologyTreeEditingComposite;
	
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public AssemblyLinksComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, true));
		
		ScrolledForm scrolledForm = formToolkit.createScrolledForm(this);
		scrolledForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrolledForm);		
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrolledForm.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnAssemblyList = formToolkit.createSection(scrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnAssemblyList = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 3, 1);
		twd_sctnAssemblyList.valign = TableWrapData.FILL;
		twd_sctnAssemblyList.grabVertical = true;
		twd_sctnAssemblyList.grabHorizontal = true;
		sctnAssemblyList.setLayoutData(twd_sctnAssemblyList);
		formToolkit.paintBordersFor(sctnAssemblyList);
		sctnAssemblyList.setText("Assembly Links");
		
		assemblyLinkListComposite = new AssemblyLinkListComposite(sctnAssemblyList, SWT.NONE)
		{
			@Override
			protected void newAssemblyLinkSelected(AssemblyLink selectedAssemblyLink) 
			{
				assemblyLinkOverviewComposite.setAssemblyLink(selectedAssemblyLink);
				
				if(selectedAssemblyLink != null)
				{
					topologyTreeEditingComposite.setRoot(selectedAssemblyLink.getGeometryNode());
				}
				else
				{
					topologyTreeEditingComposite.setRoot(null);
				}
			}
		};		
		formToolkit.adapt(assemblyLinkListComposite);
		formToolkit.paintBordersFor(assemblyLinkListComposite);
		sctnAssemblyList.setClient(assemblyLinkListComposite);
		
		// Overview
		Section sctnDemoverview = formToolkit.createSection(scrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDemoverview = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 3, 1);
		twd_sctnDemoverview.valign = TableWrapData.FILL;
		twd_sctnDemoverview.grabVertical = true;
		twd_sctnDemoverview.grabHorizontal = true;
		sctnDemoverview.setLayoutData(twd_sctnDemoverview);
		formToolkit.paintBordersFor(sctnDemoverview);
		sctnDemoverview.setText("Assembly Link Overview");
		
		assemblyLinkOverviewComposite = new AssemblyLinkOverviewComposite(sctnDemoverview, SWT.NONE);
		formToolkit.adapt(assemblyLinkOverviewComposite);
		formToolkit.paintBordersFor(assemblyLinkOverviewComposite);
		sctnDemoverview.setClient(assemblyLinkOverviewComposite);	
						
		// Topology
		Section sctnTopology = formToolkit.createSection(scrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);		
		TableWrapData twd_sctnTopology = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 2);
		twd_sctnTopology.valign = TableWrapData.FILL;
		twd_sctnTopology.grabVertical = true;
		twd_sctnTopology.grabHorizontal = true;
		sctnTopology.setLayoutData(twd_sctnTopology);	
		formToolkit.paintBordersFor(sctnTopology);
		sctnTopology.setText("Assembly Link Topology");
		
		topologyTreeEditingComposite = new TopologyTreeEditingComposite(sctnTopology, SWT.NONE, true);
		formToolkit.adapt(topologyTreeEditingComposite);
		formToolkit.paintBordersFor(topologyTreeEditingComposite);
		sctnTopology.setClient(topologyTreeEditingComposite);	
		
		// Do clean up upon dispose.
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent arg0) 
			{
				assemblyLinksList = null;
			}
		});		
	}

	public AssemblyLinksList getAssemblyLinksList() {
		return assemblyLinksList;
	}

	public void setAssemblyLinksList(AssemblyLinksList assemblyLinksList) 
	{
		this.assemblyLinksList = assemblyLinksList;
		
		if(assemblyLinkListComposite != null && !assemblyLinkListComposite.isDisposed())
		{
			if(assemblyLinksList != null)
			{
				assemblyLinkListComposite.setAssemblyLinksList(assemblyLinksList);
			}
			else
			{
				assemblyLinkListComposite.setAssemblyLinksList(null);
			}
		}
		
		if(assemblyLinkOverviewComposite != null && !assemblyLinkOverviewComposite.isDisposed())
		{
			if(assemblyLinksList != null && assemblyLinksList.getAssemblyLinks().size() > 0)
			{
				assemblyLinkOverviewComposite.setAssemblyLink(assemblyLinksList.getAssemblyLinks().get(0));
			}
			else
			{
				assemblyLinkOverviewComposite.setAssemblyLink(null);
			}
		}
		
		if(topologyTreeEditingComposite != null && !topologyTreeEditingComposite.isDisposed())
		{
			if(assemblyLinksList != null && assemblyLinksList.getAssemblyLinks().size() > 0)
			{
				topologyTreeEditingComposite.setRoot(assemblyLinksList.getAssemblyLinks().get(0).getGeometryNode());
			}
			else
			{
				topologyTreeEditingComposite.setRoot(null);
			}
		}
	}	
}
