/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.ui.wizards;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.math.ui.composites.TransformMatrixComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.AssemblyLink;

public class AssemblyLinkTransformWizardPage extends AbstractAssemblyLinkWizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.ui.wizards.AssemblyLinkTransformWizardPage";	
	private TransformMatrixComposite transformMatrixComposite;
		
	public AssemblyLinkTransformWizardPage(ApogySystem apogySystem, AssemblyLink assemblyLink) 
	{
		super(WIZARD_PAGE_ID, apogySystem, assemblyLink);	
		
		setTitle("Assembly link transform");
		setDescription("Set the link transform.");				
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{	
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, true));
		setControl(container);	
		
		transformMatrixComposite = new TransformMatrixComposite(container, SWT.NONE, ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(assemblyLink));
		transformMatrixComposite.setMatrix4x4(assemblyLink.getTransformationMatrix());		
	}
	
	protected void validate()
	{			
		setErrorMessage(null);		
		setPageComplete(getErrorMessage() == null);
	}
}
