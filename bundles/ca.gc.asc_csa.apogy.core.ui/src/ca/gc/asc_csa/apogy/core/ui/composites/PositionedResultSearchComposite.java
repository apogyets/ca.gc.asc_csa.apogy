package ca.gc.asc_csa.apogy.core.ui.composites;

import java.util.Collection;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.CompositeComparator;
import ca.gc.asc_csa.apogy.common.emf.CompositeFilter;
import ca.gc.asc_csa.apogy.common.emf.EComparator;
import ca.gc.asc_csa.apogy.common.emf.IFilter;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.ComparatorDetailsComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.CompositeComparatorComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.CompositeFilterComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.FilterDetailsComposite;
import ca.gc.asc_csa.apogy.core.PositionedResult;

public class PositionedResultSearchComposite extends Composite 
{
	// Filters.
	private CompositeFilter<PositionedResult> compositeFilter = ApogyCommonEMFFactory.eINSTANCE.createCompositeFilter(); 
	private CompositeFilterComposite<PositionedResult> compositeFilterComposite;
	private FilterDetailsComposite<PositionedResult> filterDetailsComposite;
	private Button btnApplyFilters;
		
	// Sorters
	private CompositeComparator<PositionedResult> compositeComparator = ApogyCommonEMFFactory.eINSTANCE.createCompositeComparator();
	private CompositeComparatorComposite<PositionedResult> compositeComparatorComposite;
	private ComparatorDetailsComposite<PositionedResult> comparatorDetailsComposite;
	private Button btnSort;
		
	public PositionedResultSearchComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(1, false));

		Composite top = new Composite(this, SWT.NONE);
		top.setLayout(new GridLayout(2, true));
		top.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		
		// Filters
		Group grpFilters = new Group(top, SWT.NONE);
		grpFilters.setLayout(new GridLayout(1, false));
		grpFilters.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		grpFilters.setText("Filters");
				
		// Composite Filter
		compositeFilterComposite = new CompositeFilterComposite<PositionedResult>(grpFilters, SWT.NONE)
		{
			protected void newSelection(IFilter<PositionedResult> filter)
			{
				filterDetailsComposite.setFilter(filter);
			}
		};
		compositeFilterComposite.setCompositeFilter(compositeFilter);
		GridData gd_compositeFilterComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_compositeFilterComposite.heightHint = 150;				
		gd_compositeFilterComposite.minimumHeight = 150;
		compositeFilterComposite.setLayoutData(gd_compositeFilterComposite);
		
		// Filter Details.
		filterDetailsComposite = new FilterDetailsComposite<PositionedResult>(grpFilters, SWT.NONE);
		GridData gd_filterDetailsComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_filterDetailsComposite.heightHint = 100;		
		gd_filterDetailsComposite.minimumWidth = 300;
		gd_filterDetailsComposite.widthHint = 300;
		gd_filterDetailsComposite.minimumHeight = 100;
		filterDetailsComposite.setLayoutData(gd_filterDetailsComposite);
				
		btnApplyFilters = new Button(grpFilters, SWT.PUSH);
		btnApplyFilters.setText("Apply Filters");
		btnApplyFilters.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				applyFilter();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		
		// Sorters
		Group grpSorters = new Group(top, SWT.NONE);
		grpSorters.setLayout(new GridLayout(1, false));
		grpSorters.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		grpSorters.setText("Sorters");
		
		// Composite Comparator
		compositeComparatorComposite = new CompositeComparatorComposite<PositionedResult>(grpSorters, SWT.NONE)
		{
			@Override
			protected void newSelection(EComparator<PositionedResult> filter) 
			{
				comparatorDetailsComposite.setComparator(filter);
			}
		};
		compositeComparatorComposite.setCompositeComparator(compositeComparator);
		GridData gd_compositeComparatorComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_compositeComparatorComposite.heightHint = 150;				
		gd_compositeComparatorComposite.minimumHeight = 150;
		compositeComparatorComposite.setLayoutData(gd_compositeComparatorComposite);
		
		comparatorDetailsComposite = new ComparatorDetailsComposite<PositionedResult>(grpSorters, SWT.NONE);
		GridData gd_comparatorDetailsComposite = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_comparatorDetailsComposite.heightHint = 100;
		gd_comparatorDetailsComposite.minimumHeight = 100;
		gd_comparatorDetailsComposite.widthHint = 300;
		gd_comparatorDetailsComposite.minimumWidth = 300;		
		comparatorDetailsComposite.setLayoutData(gd_comparatorDetailsComposite);
		
		btnSort = new Button(grpSorters, SWT.PUSH);
		btnSort.setText("Sort");
		btnSort.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				applySorter();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});			
	}		
	
	public Collection<PositionedResult> applyFilter(List<PositionedResult> input)
	{
		return compositeFilter.filter(input);
	}
	
	public SortedSet<PositionedResult> sort(Collection<PositionedResult> input)
	{		
		SortedSet<PositionedResult> sorted = new TreeSet<PositionedResult>(compositeComparator);	
		sorted.addAll(input);
		return sorted;		
	}
			
	public CompositeFilter<PositionedResult> getCompositeFilter() 
	{
		return compositeFilter;
	}

	public void setCompositeFilter(CompositeFilter<PositionedResult> compositeFilter) 
	{
		this.compositeFilter = compositeFilter;
		compositeFilterComposite.setCompositeFilter(compositeFilter);		
	}

	public CompositeComparator<PositionedResult> getCompositeComparator() 
	{
		return compositeComparator;
	}

	public void setCompositeComparator(CompositeComparator<PositionedResult> compositeComparator) 
	{
		this.compositeComparator = compositeComparator;
		compositeComparatorComposite.setCompositeComparator(compositeComparator);
	}

	protected void applyFilter()
	{		
	}
	
	protected void applySorter()
	{		
	}
}
