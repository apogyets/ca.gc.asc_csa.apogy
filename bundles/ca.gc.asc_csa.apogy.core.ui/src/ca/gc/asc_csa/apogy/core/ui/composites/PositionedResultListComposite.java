package ca.gc.asc_csa.apogy.core.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.PlatformUI;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.edit.utils.ApogyCommonEMFEditUtilsFacade;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.OperationCallPositionedResult;
import ca.gc.asc_csa.apogy.core.Positioned;
import ca.gc.asc_csa.apogy.core.PositionedResult;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResultValue;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.AttributeResultValue;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResult;
import ca.gc.asc_csa.apogy.core.invocator.ReferenceResultValue;
import ca.gc.asc_csa.apogy.core.ui.Activator;

public class PositionedResultListComposite extends Composite 
{
	private final static int NAME_COLUMN_ID = 0;
	private final static int DESCRIPTION_COLUMN_ID = 1;
	private final static int TIME_COLUMN_ID = 2;	
	private final static int POSITION_COLUMN_ID = 3;
	private final static int ORIENTATION_COLUMN_ID = 4;
	private final static int VALUE_COLUMN_ID = 5;
	
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private List<PositionedResult> positionedResultList = new ArrayList<PositionedResult>();

	protected PositionedResult selectedPositionedResult = null;
	protected List<PositionedResult> selectedPositionedResults = new ArrayList<PositionedResult>();
	
	private TableViewer tableViewer;
	private Table table;
	
	private Button btnNew;
	private Button btnDelete;
	
	public PositionedResultListComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new org.eclipse.swt.layout.GridLayout(2, false));
		
		// Filtered table viewer.
		tableViewer = new TableViewer(this, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI);
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		// First Column : NodeId
		TableColumn tblclmnNodeId = new TableColumn(table, SWT.NONE);
		tblclmnNodeId.setWidth(100);
		tblclmnNodeId.setText("Name");

		// Second Column : Node Description
		TableColumn tblclmnNodeDescription = new TableColumn(table, SWT.NONE);
		tblclmnNodeDescription.setWidth(300);
		tblclmnNodeDescription.setText("Description");	
		
		// Third Column : Date
		TableColumn tblclmnDate = new TableColumn(table, SWT.NONE);
		tblclmnDate.setWidth(220);
		tblclmnDate.setText("Date");	
		
		// Fourth Column : Position
		TableColumn tblclmnPosition= new TableColumn(table, SWT.NONE);
		tblclmnPosition.setWidth(130);
		tblclmnPosition.setText("Position (m)");
		
		// Fifth Column : Orientation
		TableColumn tblclmnOrientation= new TableColumn(table, SWT.NONE);
		tblclmnOrientation.setWidth(130);
		tblclmnOrientation.setText("Orientation (deg)");
		
		// Last Column : Value
		TableColumn tblclmnValue = new TableColumn(table, SWT.NONE);
		tblclmnValue.setWidth(300);
		tblclmnValue.setText("Value");	
				
		tableViewer.setContentProvider(new NodeContentProvider(adapterFactory));
		tableViewer.setLabelProvider(new PositionedResultLabelProvider(adapterFactory));
		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{			
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					
					// Clears the selected Nodes list.					
					selectedPositionedResults.clear();
					
					if(!iStructuredSelection.isEmpty())
					{					
						positionedResultSelectedChanged((PositionedResult) iStructuredSelection.getFirstElement());
						
						// Process the list of selected nodes.
						Collection<PositionedResult> nodesSelected = new ArrayList<PositionedResult>();		
						
						@SuppressWarnings("unchecked")
						List<Object> selectedObjects = iStructuredSelection.toList();
						for(Object object : selectedObjects)
						{
							if(object instanceof PositionedResult)	nodesSelected.add((PositionedResult) object);						
						}
						
						// Update selected results.
						selectedPositionedResults.addAll(nodesSelected);
						btnDelete.setEnabled(true);						
						positionedResultsSelectedChanged(nodesSelected);												
					}
					else
					{
						btnDelete.setEnabled(false);
						positionedResultSelectedChanged(null);
						positionedResultsSelectedChanged(new ArrayList<PositionedResult>());
						
					}				
				}
			}
		});
		
		Composite buttonsComposite = new Composite(this, SWT.NONE);
		buttonsComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		buttonsComposite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(buttonsComposite, SWT.NONE);
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnNew.setText("New");
		btnNew.setEnabled(false);
		
		btnDelete = new Button(buttonsComposite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnDelete.setText("Delete");
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String deleteMessage = "";

				Iterator<PositionedResult> results = selectedPositionedResults.iterator();
				while (results.hasNext()) 
				{
					PositionedResult result = results.next();
					deleteMessage = deleteMessage + result.getName();

					if (results.hasNext()) 
					{
						deleteMessage = deleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected Results", null,
						"Are you sure to delete these Results ? : " + deleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					List<PositionedResult> toDelete = new ArrayList<PositionedResult>();
					toDelete.addAll(selectedPositionedResults);
					
					for (PositionedResult positionedResult : toDelete) 
					{
						try 
						{
							ApogyCommonTransactionFacade.INSTANCE.basicRemove(positionedResult.getResultsList(), ApogyCoreInvocatorPackage.Literals.RESULTS_LIST__RESULTS, positionedResult, true);
						} 
						catch (Exception e)	
						{
							Logger.INSTANCE.log(Activator.ID, "Unable to delete the PositionnedResult <"+ positionedResult.getName() + ">", EventSeverity.ERROR, e);
						}
					}
				}
				
				// Forces the viewer to refresh its input.
				if(!tableViewer.isBusy())
				{					
					tableViewer.setInput(positionedResultList);
				}					
			}
		});
		
	}

	public Collection<PositionedResult> getPositionedResultList() 
	{
		return positionedResultList;
	}

	public void setPositionedResultList(Collection<PositionedResult> positionedResultList) 
	{
		this.positionedResultList.clear();
		
		if(positionedResultList != null)
		{
			this.positionedResultList.addAll(positionedResultList);
			
			// Update viewer input.
			tableViewer.setInput(this.positionedResultList);			
		}
	}		
	
	protected void positionedResultSelectedChanged(PositionedResult positionedResult)
	{		
	}
	
	protected void positionedResultsSelectedChanged(Collection<PositionedResult> positionedResults)
	{		
	}
	
	private class NodeContentProvider extends AdapterFactoryContentProvider {

		public NodeContentProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public Object[] getElements(Object object) 
		{
			if(object instanceof Collection)
			{
				return ((Collection<?>) object).toArray();
			}
			return null;
		}

		@Override
		public Object[] getChildren(Object object) 
		{
			return null;
		}

		@Override
		public boolean hasChildren(Object object) 
		{
			return false;
		}
	}
	
	private class PositionedResultLabelProvider extends AdapterFactoryLabelProvider implements ITableLabelProvider, ITableColorProvider 
	{	
		private DecimalFormat positionFormat = new DecimalFormat("0.00");
		private DecimalFormat orientationFormat = new DecimalFormat("0.0");
		
		public PositionedResultLabelProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public String getColumnText(Object object, int columnIndex) 
		{
			String str = "<undefined>";
			switch (columnIndex) 
			{
				case NAME_COLUMN_ID:
					if(object instanceof Named)
					{
						str = ((Named) object).getName();					
					}
					else if (object instanceof OperationCallResult) 
					{
						OperationCallResult result = (OperationCallResult) object;
						str = ApogyCommonEMFEditUtilsFacade.INSTANCE.getText(result.getOperationCall());
					}
				break;
				
				case DESCRIPTION_COLUMN_ID:
					if(object instanceof Described)
					{
						str = ((Described) object).getDescription();
					}
					
				break;
				case TIME_COLUMN_ID:
					if(object instanceof PositionedResult)
					{
						Date date = ((PositionedResult) object).getTime();
						if(date != null)
						{
							str = ApogyCommonEMFFacade.INSTANCE.format(date);
						}
					}
				break;
				case POSITION_COLUMN_ID:
					str = "N/A.";
					if (object instanceof Positioned) 
					{
						Positioned positioned = (Positioned) object;
						Tuple3d position = ApogyCommonMathFacade.INSTANCE.extractPosition(positioned.getPose());
						str = positionFormat.format(position.getX()) + ", " + positionFormat.format(position.getY()) + ", " + positionFormat.format(position.getZ());						
					}					
					
					break;

				case ORIENTATION_COLUMN_ID:
					str = "";
					if (object instanceof OperationCallResult) 
					{
						if (object instanceof OperationCallPositionedResult) 
						{
							OperationCallPositionedResult result = (OperationCallPositionedResult) object;
							if (result.getPose() == null) 
							{
								str = "Null";
							} 
							else 
							{

								Tuple3d orientation = ApogyCommonMathFacade.INSTANCE.extractOrientation(result.getPose());
								
								/** Convert in degrees. */
								orientation.setX(Math.toDegrees(orientation.getX()));
								orientation.setY(Math.toDegrees(orientation.getY()));
								orientation.setZ(Math.toDegrees(orientation.getZ()));
								
								str = orientationFormat.format(orientation.getX()) + ", " + orientationFormat.format(orientation.getY()) + ", " + orientationFormat.format(orientation.getZ());																
							}
						} else {
							str = "N/A.";
						}
					}
					break;
					
				case VALUE_COLUMN_ID:
					str = "";
					if (object instanceof OperationCallResult) 
					{
						OperationCallResult result = (OperationCallResult) object;

						if (result.getExceptionContainer() != null && result.getExceptionContainer().getException() != null) 
						{
							str = result.getExceptionContainer().getException().getMessage();
						} 
						else 
						{
							AbstractResultValue abstractResultValue = result.getResultValue();
							
							if(abstractResultValue instanceof AttributeResultValue)
							{
								AttributeResultValue attributeResultValue  = (AttributeResultValue) abstractResultValue;
								if(attributeResultValue.getValue().getObject() != null)
								{
									str = attributeResultValue.getValue().getObject().toString();
								}
								else
								{
									str = "<null>";
								}
							}
							else if(abstractResultValue instanceof ReferenceResultValue)
							{
								ReferenceResultValue referenceResultValue = (ReferenceResultValue) abstractResultValue;								
								str = result.getResultValue() == null ? "<null>": ApogyCommonEMFEditUtilsFacade.INSTANCE.getText(referenceResultValue.getValue());	
							}														
						}
					}
					break;					
				default:
				break;
			}
			return str;
		}
		
		@Override
		public Image getColumnImage(Object object, int columnIndex) 
		{
			return null;
		}
		
		@Override
		public Color getBackground(Object object, int columnIndex) 
		{
			Color color = super.getBackground(object, columnIndex);

			/** Put the background in red if any exception occurred on the call. */
			if (object instanceof OperationCallResult) 
			{
				OperationCallResult opResult = (OperationCallResult) object;
				if (opResult.getExceptionContainer() != null && opResult.getExceptionContainer().getException() != null) 
				{
					color = PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_RED);
				}
			}
			return color;
		}
	}

}