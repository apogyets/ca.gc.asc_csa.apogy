package ca.gc.asc_csa.apogy.core.ui.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResult;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.AbstractResultDetailsComposite;
import ca.gc.asc_csa.apogy.core.ui.ApogyCoreUIRCPConstants;

public class DataProductResultPart extends AbstractEObjectSelectionPart
{
	private AbstractResultDetailsComposite abstractResultDetailsComposite;
	
	@Override
	protected void setCompositeContents(EObject eObject) 
	{
		if(abstractResultDetailsComposite != null && !abstractResultDetailsComposite.isDisposed())
		{
			if(eObject instanceof AbstractResult)
			{
				abstractResultDetailsComposite.setResult((AbstractResult) eObject);
			}
			else
			{
				abstractResultDetailsComposite.setResult(null);
			}
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		abstractResultDetailsComposite = new AbstractResultDetailsComposite(parent, SWT.BORDER);
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() 
	{
		HashMap<String, ISelectionListener> map = new HashMap<>();

		map.put(ApogyCoreUIRCPConstants.PART__POSITIONED_RESULT_SEARCH__ID, new ISelectionListener() {
			@Override
			public void selectionChanged(MPart part, Object selection) 
			{
				if (selection instanceof AbstractResult) 
				{
					AbstractResult result = (AbstractResult) selection;
					setEObject(result);
				}
			}
		});

		return map;
	}	
}
