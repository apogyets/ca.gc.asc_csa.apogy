/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.ui.wizards;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.AssemblyLink;
import ca.gc.asc_csa.apogy.core.ConnectionPoint;
import ca.gc.asc_csa.apogy.core.ConnectionPointsList;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;

public class AssemblyLinkSourceWizardPage extends AbstractAssemblyLinkWizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.ui.wizards.AssemblyLinkConnectivityWizardPage";
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		
	// Parent System
	private Tree parentSystemTree;
	private TreeViewer parentSystemTreeViewer;
	private TypeMember selectedParentSystem = null;

	// Parent Conection Point
	private Tree parentConnectionPointTree;
	private TreeViewer parentConnectionPointTreeViewer;
	private ConnectionPoint selectedConnectionPoint = null;

	
	public AssemblyLinkSourceWizardPage(ApogySystem apogySystem, AssemblyLink assemblyLink) 
	{
		super(WIZARD_PAGE_ID, apogySystem, assemblyLink);	
		
		setTitle("Assembly link parent");
		setDescription("Set the parent system and connection point.");				
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{	
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, true));
		setControl(container);	
		
		Group parentSystemGroup = new Group(container, SWT.BORDER);
		parentSystemGroup.setLayout(new FillLayout());
		parentSystemGroup.setText("Parent System");
		parentSystemGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createParentSystemComposite(parentSystemGroup);		
		
		Group parentConnectionPointGroup = new Group(container, SWT.BORDER);
		parentConnectionPointGroup.setLayout(new FillLayout());
		parentConnectionPointGroup.setText("Parent Connection Point");
		parentConnectionPointGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createParentConnectionPointComposite(parentConnectionPointGroup);			
	}
	
	private Composite createParentSystemComposite(Composite parent)
	{
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(2, false));
		
		parentSystemTreeViewer = new TreeViewer(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		parentSystemTree = parentSystemTreeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 2, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		parentSystemTree.setLayoutData(gd_tree);
		parentSystemTree.setLinesVisible(true);
		
		parentSystemTreeViewer.setContentProvider(new ApogySystemContentProvider());
		parentSystemTreeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		parentSystemTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{				
				selectedParentSystem = (TypeMember)((IStructuredSelection) event.getSelection()).getFirstElement(); 
				
				// Updates parent system.
				ApogyCommonTransactionFacade.INSTANCE.basicSet(assemblyLink, ApogyCorePackage.Literals.ASSEMBLY_LINK__PARENT_TYPE_MEMBER, selectedParentSystem, true);													
				
				// Clear Connection point.
				ApogyCommonTransactionFacade.INSTANCE.basicSet(assemblyLink, ApogyCorePackage.Literals.ASSEMBLY_LINK__PARENT_CONNECTION_POINT, null, true);
				
				if(selectedParentSystem != null && selectedParentSystem.getMemberType() instanceof ApogySystem)
				{
					ApogySystem system = (ApogySystem) selectedParentSystem.getMemberType();
					parentConnectionPointTreeViewer.setInput(system.getConnectionPointsList());
				}
				else
				{
					// Clears parent system.
					ApogyCommonTransactionFacade.INSTANCE.basicSet(assemblyLink, ApogyCorePackage.Literals.ASSEMBLY_LINK__PARENT_TYPE_MEMBER, null, true);													
					
					// Clear Connection point.
					ApogyCommonTransactionFacade.INSTANCE.basicSet(assemblyLink, ApogyCorePackage.Literals.ASSEMBLY_LINK__PARENT_CONNECTION_POINT, null, true);
					
					// Displays this system connection points.
					parentConnectionPointTreeViewer.setInput(apogySystem.getConnectionPointsList());				
				}
									
				validate();	
			}
		});
		parentSystemTreeViewer.setInput(getAllTypeMembers(apogySystem));
				
		Button btnClearParentSystem = new Button(container, SWT.NONE);
		btnClearParentSystem.setSize(74, 29);
		btnClearParentSystem.setText("This");
		btnClearParentSystem.setToolTipText("Sets the parent system for the Assembly Link to this system.");
		btnClearParentSystem.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnClearParentSystem.setEnabled(true);
		btnClearParentSystem.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{								
				// Clears the selection. This will trig the parentSystemTreeViewer response.
				parentSystemTreeViewer.setSelection(new StructuredSelection(), true);
				
				validate();
			}
		});
		
		return container;
	}
	
	private Composite createParentConnectionPointComposite(Composite parent)
	{
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(1, false));
		
		parentConnectionPointTreeViewer = new TreeViewer(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		parentConnectionPointTree = parentConnectionPointTreeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		parentConnectionPointTree.setLayoutData(gd_tree);
		parentConnectionPointTree.setLinesVisible(true);
		
		parentConnectionPointTreeViewer.setContentProvider(new ConnectionPointsContentProvider());
		parentConnectionPointTreeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		parentConnectionPointTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{				
				selectedConnectionPoint = (ConnectionPoint)((IStructuredSelection) event.getSelection()).getFirstElement(); 				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(assemblyLink, ApogyCorePackage.Literals.ASSEMBLY_LINK__PARENT_CONNECTION_POINT, selectedConnectionPoint, true);
				
				validate();
			}
		});		
		// Displays this system connection points.
		parentConnectionPointTreeViewer.setInput(apogySystem.getConnectionPointsList());
		
		return container;
	}
	
	protected void validate()
	{		
		String errorMessage = null;
		
		if(assemblyLink.getParentConnectionPoint() == null)
		{
			errorMessage = "No Parent Connections Point specified !";
		}		
		
		setErrorMessage(errorMessage);
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private class ApogySystemContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof Collection<?>)
			{								
				Collection<?> collection = (Collection<?> ) inputElement;									
				return collection.toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof Collection<?>)
			{								
				Collection<?> collection = (Collection<?> ) parentElement;					
				return collection.toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof Collection<?>)
			{
				Collection<?> collection = (Collection<?> ) element;					
				return collection.isEmpty();
			}		
			else
			{
				return false;
			}
		}				
	}
	
	private class ConnectionPointsContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{						
			if(inputElement instanceof ConnectionPointsList)
			{
				ConnectionPointsList connectionPointsList = (ConnectionPointsList) inputElement;
				return connectionPointsList.getConnectionPoints().toArray();
			}
			else if(inputElement instanceof Collection<?>)
			{								
				Collection<?> collection = (Collection<?> ) inputElement;					
				return collection.toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof ConnectionPointsList)
			{
				ConnectionPointsList connectionPointsList = (ConnectionPointsList) parentElement;
				return connectionPointsList.getConnectionPoints().toArray();
			}
			else if(parentElement instanceof Collection<?>)
			{								
				Collection<?> collection = (Collection<?> ) parentElement;					
				return collection.toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof ConnectionPointsList)
			{
				ConnectionPointsList connectionPointsList = (ConnectionPointsList) element;
				return !connectionPointsList.getConnectionPoints().isEmpty();
			}
			if(element instanceof Collection<?>)
			{
				Collection<?> collection = (Collection<?> ) element;					
				return !collection.isEmpty();
			}		
			else
			{
				return false;
			}
		}
	}
}
