package ca.gc.asc_csa.apogy.core.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


public class ApogyCorePreferencesConstants 
{
	// Visibility Constants.
	public static final String DEFAULT_RESULT_NODE_FLAG_VISIBILITY_ID = "DEFAULT_RESULT_NODE_FLAG_VISIBILITY_ID";	
	public static final Boolean DEFAULT_RESULT_NODE_FLAG_VISIBILITY = new Boolean(true);

	// Flag Pole Constants.
	public static final String DEFAULT_RESULT_NODE_FLAG_POLE_HEIGHT_ID = "DEFAULT_RESULT_NODE_FLAG_POLE_HEIGHT_ID";	
	public static final Float DEFAULT_RESULT_NODE_FLAG_POLE_HEIGHT = new Float(1.0f);

}
