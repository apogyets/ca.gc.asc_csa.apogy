package ca.gc.asc_csa.apogy.core.ui.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestList;
import ca.gc.asc_csa.apogy.core.environment.AbstractWorksite;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;
import ca.gc.asc_csa.apogy.core.ui.composites.FeatureOfInterestDetailsComposite;
import ca.gc.asc_csa.apogy.core.ui.composites.FeatureOfInterestsListsComposite;

public class FeatureOfInterestPart extends AbstractSessionBasedPart
{
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	private FeatureOfInterestsListsComposite simpleToolListComposite;
	private FeatureOfInterestDetailsComposite simpleToolsDetailsComposite;	
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		simpleToolListComposite.setFeatureOfInterestsLists(resolveSimpleToolList());
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{				
		parent.setLayout(new FillLayout());
		
		Composite top = new Composite(parent, SWT.BORDER);
		GridLayout gl_parent = new GridLayout(2, false);
		gl_parent.verticalSpacing = 10;
		top.setLayout(gl_parent);
		
		// Tools section on the left.
		Section sctnTools = formToolkit.createSection(top, Section.TITLE_BAR);
		sctnTools.setForeground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND));
		sctnTools.setFont(SWTResourceManager.getFont("Ubuntu", 18, SWT.BOLD));
		sctnTools.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(sctnTools);
		sctnTools.setText("Feature Of Interest");
				
		simpleToolListComposite = new FeatureOfInterestsListsComposite(sctnTools, SWT.NONE)
		{
			protected void newSelection(TreeSelection selection) 
			{
				if(selection instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) selection;
					
					if(iStructuredSelection.getFirstElement() instanceof FeatureOfInterest)
					{					
						simpleToolsDetailsComposite.setFeatureOfInterest((FeatureOfInterest) iStructuredSelection.getFirstElement());
					}
				}
			};
		};
		GridLayout gridLayout = (GridLayout) simpleToolListComposite.getLayout();
		gridLayout.marginWidth = 0;
		formToolkit.adapt(simpleToolListComposite);
		formToolkit.paintBordersFor(simpleToolListComposite);
		sctnTools.setClient(simpleToolListComposite);
		
		
		// Tools section on the left.
		Section sctnToolsDetails = formToolkit.createSection(top, Section.TITLE_BAR);
		sctnToolsDetails.setForeground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND));
		sctnToolsDetails.setFont(SWTResourceManager.getFont("Ubuntu", 18, SWT.BOLD));
		sctnToolsDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(sctnToolsDetails);
		sctnToolsDetails.setText("Details");
				
		simpleToolsDetailsComposite = new FeatureOfInterestDetailsComposite(sctnToolsDetails, SWT.NONE);
		GridLayout gridLayout1 = (GridLayout) simpleToolsDetailsComposite.getLayout();
		gridLayout1.marginWidth = 0;
		formToolkit.adapt(simpleToolsDetailsComposite);
		formToolkit.paintBordersFor(simpleToolsDetailsComposite);
		sctnToolsDetails.setClient(simpleToolsDetailsComposite);
	}

	protected List<FeatureOfInterestList> resolveSimpleToolList()
	{
		List<FeatureOfInterestList> list = new ArrayList<FeatureOfInterestList>();
		
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		if(invocatorSession.getEnvironment() instanceof ApogyEnvironment)
		{
			ApogyEnvironment apogyEnvironment = (ApogyEnvironment) invocatorSession.getEnvironment();
			AbstractWorksite worksite = apogyEnvironment.getActiveWorksite();
			if(worksite != null)
			{
				List<EObject> children = ApogyCommonEMFFacade.INSTANCE.getEObjectsByType(worksite, ApogyCorePackage.Literals.FEATURE_OF_INTEREST_LIST);				
				for(EObject eObject : children)
				{
					if(eObject instanceof FeatureOfInterestList)
					{
						list.add((FeatureOfInterestList) eObject);
					}						
				}
			}			
		}
		
		return list;
	}
}
