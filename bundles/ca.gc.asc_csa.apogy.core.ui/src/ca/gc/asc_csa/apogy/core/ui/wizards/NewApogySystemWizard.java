package ca.gc.asc_csa.apogy.core.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.TreeRootNode;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.EClassSelectionComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.NamedDescribedWizardPage;
import ca.gc.asc_csa.apogy.common.topology.AggregateGroupNode;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsSet;
import ca.gc.asc_csa.apogy.common.topology.bindings.FeatureRootsList;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCoreFacade;
import ca.gc.asc_csa.apogy.core.ApogyCoreFactory;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.TopologyRoot;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;

public class NewApogySystemWizard extends Wizard 
{
	private ApogySystem apogySystem;
	private String fileAbsolutePath = "/home/pallard/tmp/TestApogySystem.ss";
	
	public NewApogySystemWizard()
	{
		super();
		
		// Creates an empty ApogySystem.
		apogySystem = ApogyCoreFactory.eINSTANCE.createApogySystem();
		apogySystem.setName("<unamed>");
		apogySystem.setDescription("None.");
		TopologyRoot topologyRoot = ApogyCoreFactory.eINSTANCE.createTopologyRoot();
		AggregateGroupNode rootNode = ApogyCommonTopologyFactory.eINSTANCE.createAggregateGroupNode();
		rootNode.setNodeId("CHANGE_ME_TO_A_UNIQUE_ID");
		rootNode.setDescription("Root node of the Apogy System Topology.");
		topologyRoot.setOriginNode(rootNode);
		apogySystem.setTopologyRoot(topologyRoot);
		
		apogySystem.setAssemblyLinksList(ApogyCoreFactory.eINSTANCE.createAssemblyLinksList());
		apogySystem.setConnectionPointsList(ApogyCoreFactory.eINSTANCE.createConnectionPointsList());
		
		BindingsSet bindingsSet = ApogyCommonTopologyBindingsFactory.eINSTANCE.createBindingsSet();
		bindingsSet.setBindingsList(ApogyCommonTopologyBindingsFactory.eINSTANCE.createBindingsList());
		
		FeatureRootsList featureRootsList = ApogyCommonTopologyBindingsFactory.eINSTANCE.createFeatureRootsList();
		featureRootsList.getFeatureRoots().add(ApogyCommonEMFFactory.eINSTANCE.createTreeRootNode());
		bindingsSet.setFeatureRootsList(featureRootsList);
		
		apogySystem.setBindingSet(bindingsSet);
		apogySystem.setTypeApiAdapterClass(ApogyCorePackage.Literals.APOGY_SYSTEM_API_ADAPTER);
	}
	
	@Override
	public void addPages() {
		super.addPages();
		
		// Named + Described
		addPage(new NamedDescribedWizardPage(apogySystem, apogySystem));
		
		addPage(new ApogySystemFilePathWizardPage());
		
		addPage(new ApogySystemInterfaceClassPage());
	}
	
	@Override
	public boolean canFinish() 
	{
		return (fileAbsolutePath != null) && (apogySystem.getInterfaceClass() != null);
	}
	
	@Override
	public boolean performFinish() 
	{		
		if(getFileAbsolutePath() != null)
		{
			if(getApogySystem() != null)
			{
				try 
				{
					ApogyCoreFacade.INSTANCE.saveApogySystemToFile(getApogySystem(), getFileAbsolutePath() );					
					return true;
				} 
				catch (Exception e) 
				{						
					e.printStackTrace();
				}
			}
		}		
		
		return false;
	}

	public ApogySystem getApogySystem() 
	{
		return apogySystem;
	}

	public String getFileAbsolutePath() {
		return fileAbsolutePath;
	}
	
	private class ApogySystemFilePathWizardPage extends WizardPage
	{
		private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.ui.wizards.ApogySystemFilePathWizardPage";
				
		private Label lblApogySystemFilePath;
		private Button btnOpen;
		
		public ApogySystemFilePathWizardPage()
		{
			super(WIZARD_PAGE_ID);
			setTitle("Apogy System File");
			setDescription("Select the path to the ApogySystem file to create.");
		}
		
		@Override
		public void createControl(Composite parent) 
		{			
			Composite container = new Composite(parent, SWT.None);
			container.setLayout(new GridLayout(4, false));
			
			Label fileLabel = new Label(container, SWT.NONE);
			fileLabel.setText("Apogy System File :");
			
			lblApogySystemFilePath = new Label(container, SWT.BORDER);
			lblApogySystemFilePath.setText("");
			GridData gd_lblApogySystemFilePath = new GridData(SWT.FILL, SWT.CENTER, true, false);
			lblApogySystemFilePath.setLayoutData(gd_lblApogySystemFilePath);
			
			btnOpen = new Button(container, SWT.PUSH);
			btnOpen.setText("Open");
			btnOpen.setToolTipText("Opens an existing Apogy System file.");
			GridData gd_btnOpen = new GridData(SWT.CENTER, SWT.TOP, false,false);
			btnOpen.setLayoutData(gd_btnOpen);
			btnOpen.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent arg0) 
				{
					// Get file selection from user.
					FileDialog fd = new FileDialog(getShell(), SWT.SAVE);
			        fd.setText("Select an ApogySystem File");
			        String[] filterExt = { "*.ss" };
			        fd.setFilterExtensions(filterExt);
			        
			        fileAbsolutePath = fd.open();	
			        
			        if(fileAbsolutePath != null)
			        {
			        	lblApogySystemFilePath.setText(fileAbsolutePath);
			        }
			        else
			        {
			        	lblApogySystemFilePath.setText("");
			        }
			        
			        validate();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {				
				}
			});
		
			
			setControl(container);
			
			validate();
		}
		
		/**
		 * This method is invoked to validate the content.
		 */
		protected void validate() 
		{
			String errorStr = null;

			if (fileAbsolutePath == null) 
			{
				errorStr = "A path must be provided.";
			}

			setErrorMessage(errorStr);
			setPageComplete(errorStr == null);
		}
	}
	
	private class ApogySystemInterfaceClassPage extends WizardPage
	{
		// TODO : Add selection of APIAdapter 
		
		private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.ui.wizards.ApogySystemInterfaceClassPage";
				
		private EClassSelectionComposite comboApogySystemInterfaceClass;
		private EClassSelectionComposite comboApogySystemApiAdapterClass;
		
		private DataBindingContext m_bindingContext;
		
		public ApogySystemInterfaceClassPage()
		{
			super(WIZARD_PAGE_ID);
			setTitle("Apogy System Interface Class");
			setDescription("Select the Apogy System Interface Class.");
		}
		
		@Override
		public void createControl(Composite parent) 
		{			
			Composite container = new Composite(parent, SWT.None);
			container.setLayout(new GridLayout(2, false));
			
			Label fileLabel = new Label(container, SWT.NONE);
			fileLabel.setText("Interface Class :");
			
			// Interface Class
			comboApogySystemInterfaceClass = new EClassSelectionComposite(container, SWT.NONE);
			comboApogySystemInterfaceClass.setToolTipText("The interface class this Apogy System is being used for.");
			GridData gd_comboApogySystemInterfaceClass = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
			gd_comboApogySystemInterfaceClass.widthHint = 250;
			gd_comboApogySystemInterfaceClass.minimumWidth = 250;
			comboApogySystemInterfaceClass.setLayoutData(gd_comboApogySystemInterfaceClass);
			
			comboApogySystemInterfaceClass.getComboViewer().getCombo().addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent arg0) 
				{
					TreeRootNode treeRootNode = getApogySystem().getBindingSet().getFeatureRootsList().getFeatureRoots().get(0);
					treeRootNode.setSourceClass(comboApogySystemInterfaceClass.getSelectedEclass());
					
					validate();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			
			// Api Adapter Class.
			Label apiAdapterLabel = new Label(container, SWT.NONE);
			apiAdapterLabel.setText("API Adapter Class :");
	
			
			comboApogySystemApiAdapterClass = new EClassSelectionComposite(container, SWT.NONE, ApogyCorePackage.Literals.APOGY_SYSTEM_API_ADAPTER);			
			comboApogySystemApiAdapterClass.setToolTipText("The ApiAdapter to used for this system.");
			GridData gd_comboApogySystemApiAdapterClass = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
			gd_comboApogySystemApiAdapterClass.minimumWidth = 250;
			gd_comboApogySystemApiAdapterClass.widthHint = 250;
			comboApogySystemApiAdapterClass.setLayoutData(gd_comboApogySystemApiAdapterClass);
			
			comboApogySystemApiAdapterClass.getComboViewer().getCombo().addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent arg0) {					
					validate();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			
			// Initialize Bindings
			m_bindingContext = initDataBindingsCustom();
			
			setControl(container);
			
			container.addDisposeListener(new DisposeListener() 
			{			
				@Override
				public void widgetDisposed(DisposeEvent e) 
				{
					// Dispose of bindings.
					if (m_bindingContext != null) m_bindingContext.dispose();			
				}
			});
			
			validate();
		}
		
		/**
		 * This method is invoked to validate the content.
		 */
		protected void validate() 
		{
			String errorStr = null;

			if (apogySystem.getInterfaceClass() == null) 
			{
				errorStr = "An Interface Class must be provided.";
			}
			
			if (apogySystem.getTypeApiAdapterClass() == null) 
			{
				errorStr = "An Api Adapter Class must be provided.";
			}

			setErrorMessage(errorStr);
			setPageComplete(errorStr == null);
		}
		
		@SuppressWarnings("unchecked")
		private DataBindingContext initDataBindingsCustom() 
		{
			DataBindingContext bindingContext = new DataBindingContext();

			// Interface class.		
			IObservableValue<EClass> observeInterfaceClass = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
					  										 FeaturePath.fromList(ApogyCoreInvocatorPackage.Literals.TYPE__INTERFACE_CLASS)).observe(getApogySystem());

			IObservableValue<?> observeInterfaceClassComboViewer = ViewerProperties.singleSelection().observe(comboApogySystemInterfaceClass.getComboViewer());
			bindingContext.bindValue(observeInterfaceClassComboViewer, 
									 observeInterfaceClass,
									 new UpdateValueStrategy(), 
									 new UpdateValueStrategy());
			
			// Api Adapter Class
			IObservableValue<EClass> observeApiAdapterClass = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
						 FeaturePath.fromList(ApogyCoreInvocatorPackage.Literals.TYPE__TYPE_API_ADAPTER_CLASS)).observe(getApogySystem());

			IObservableValue<?> observeApiAdapterClassComboViewer = ViewerProperties.singleSelection().observe(comboApogySystemApiAdapterClass.getComboViewer());
			bindingContext.bindValue(observeApiAdapterClassComboViewer, 
					observeApiAdapterClass,
					new UpdateValueStrategy(), 
					new UpdateValueStrategy());
			
			return bindingContext;
		}
	}
}
