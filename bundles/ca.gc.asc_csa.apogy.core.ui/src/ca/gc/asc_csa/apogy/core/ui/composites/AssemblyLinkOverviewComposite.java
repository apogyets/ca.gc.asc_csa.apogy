package ca.gc.asc_csa.apogy.core.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.math.ui.composites.TransformMatrixComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.AssemblyLink;
import ca.gc.asc_csa.apogy.core.ConnectionPoint;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;

public class AssemblyLinkOverviewComposite extends Composite 
{
	private AssemblyLink assemblyLink;
	
	private DataBindingContext m_bindingContext;
	private Text txtNamevalue;
	private Text txtDescriptionvalue;
	
	private Text txtParentSystem;
	private Text txtParentConnectionPoint;
	
	private Text txtChildSystem;
	
	private TransformMatrixComposite  transformMatrixComposite;
	
	public AssemblyLinkOverviewComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(3, false));		
		
		Label lblName = new Label(this, SWT.NONE);
		lblName.setAlignment(SWT.RIGHT);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name:");
		
		txtNamevalue = new Text(this, SWT.NONE);
		GridData gd_txtNamevalue = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
		gd_txtNamevalue.minimumWidth = 300;
		gd_txtNamevalue.widthHint = 300;
		txtNamevalue.setLayoutData(gd_txtNamevalue);
		
		Label lblDescription = new Label(this, SWT.NONE);
		lblDescription.setAlignment(SWT.RIGHT);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		txtDescriptionvalue = new Text(this, SWT.NONE | SWT.WRAP | SWT.V_SCROLL | SWT.MULTI);
		GridData gd_txtDescriptionvalue = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
		gd_txtDescriptionvalue.minimumWidth = 300;
		gd_txtDescriptionvalue.widthHint = 300;
		gd_txtDescriptionvalue.minimumHeight = 50;
		gd_txtDescriptionvalue.heightHint = 50;
		txtDescriptionvalue.setLayoutData(gd_txtDescriptionvalue);		
		
		// Parent
		Label lblParentMember = new Label(this, SWT.NONE);
		lblParentMember.setAlignment(SWT.RIGHT);
		lblParentMember.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblParentMember.setText("Parent System :");
		
		txtParentSystem = new Text(this, SWT.NONE);
		GridData gd_txtParentSystem = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
		gd_txtParentSystem.minimumWidth = 300;
		gd_txtParentSystem.widthHint = 300;
		txtParentSystem.setLayoutData(gd_txtParentSystem);
				
		Label lblParentConnectionPoint = new Label(this, SWT.NONE);
		lblParentConnectionPoint.setAlignment(SWT.RIGHT);
		lblParentConnectionPoint.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblParentConnectionPoint.setText("Connection point :");
		
		txtParentConnectionPoint = new Text(this, SWT.NONE);
		GridData gd_txtParentConnectionPoint = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
		gd_txtParentConnectionPoint.minimumWidth = 300;
		gd_txtParentConnectionPoint.widthHint = 300;
		txtParentConnectionPoint.setLayoutData(gd_txtParentConnectionPoint);
		
		// Child
		Label lblSubsytemMember = new Label(this, SWT.NONE);
		lblSubsytemMember.setAlignment(SWT.RIGHT);
		lblSubsytemMember.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSubsytemMember.setText("Child System :");
		
		txtChildSystem = new Text(this, SWT.NONE);
		GridData gd_txtChildSystem = new GridData(SWT.FILL, SWT.TOP, false, false, 2, 1);
		gd_txtChildSystem.minimumWidth = 300;
		gd_txtChildSystem.widthHint = 300;
		txtChildSystem.setLayoutData(gd_txtParentSystem);
		
		// Transform
		Label lbltransform = new Label(this, SWT.NONE);
		lbltransform.setAlignment(SWT.RIGHT);
		lbltransform.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lbltransform.setText("Transform :");
		
		transformMatrixComposite = new TransformMatrixComposite(this, SWT.NONE, ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain());
		GridData gd_ttransformMatrixComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1);
		gd_ttransformMatrixComposite.minimumWidth = 300;
		gd_ttransformMatrixComposite.widthHint = 300;
		transformMatrixComposite.setLayoutData(gd_ttransformMatrixComposite);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}

	public AssemblyLink getAssemblyLink() {
		return assemblyLink;
	}

	public void setAssemblyLink(AssemblyLink newAssemblyLink) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.assemblyLink = newAssemblyLink;
		
		if(newAssemblyLink != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}						
	}

	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Name Value. */
		IObservableValue<String> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.NAMED__NAME)).observe(getAssemblyLink());
		IObservableValue<String> observeNameValueText = WidgetProperties.text(SWT.Modify).observe(txtNamevalue);

		bindingContext.bindValue(observeNameValueText,
								observeName, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		/* Description Value. */
		IObservableValue<String> observeDescription = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.DESCRIBED__DESCRIPTION)).observe(getAssemblyLink());
		IObservableValue<String> observeDescriptionText = WidgetProperties.text(SWT.Modify).observe(txtDescriptionvalue);

		bindingContext.bindValue(observeDescriptionText,
								 observeDescription, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return (String) fromObject;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(String.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return (String) fromObject;
										}

									}));
		
		
		// Parent Type Member
		IObservableValue<TypeMember> observeParentTypeMember = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  											   FeaturePath.fromList(ApogyCorePackage.Literals.ASSEMBLY_LINK__PARENT_TYPE_MEMBER)).observe(getAssemblyLink());

		IObservableValue<String> observeParentTypeMemberText = WidgetProperties.text(SWT.Modify).observe(txtParentSystem);
		
		bindingContext.bindValue(observeParentTypeMemberText,
				observeParentTypeMember, 
				 new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				 new UpdateValueStrategy().setConverter(new Converter(TypeMember.class, String.class)
				 	{																		 											
						@Override
						public Object convert(Object fromObject) 
						{				
							if(fromObject instanceof TypeMember)
							{
								return ((TypeMember) fromObject).getName();
							}
							else
							{
								return "this";
							}
						}

					}));
		
		// Parent Connection Point.
		IObservableValue<ConnectionPoint> observeParentConnectionPoint = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  											   FeaturePath.fromList(ApogyCorePackage.Literals.ASSEMBLY_LINK__PARENT_CONNECTION_POINT)).observe(getAssemblyLink());

		IObservableValue<String> observeParentConnectionPointText = WidgetProperties.text(SWT.Modify).observe(txtParentConnectionPoint);
		
		bindingContext.bindValue(observeParentConnectionPointText,
				observeParentConnectionPoint, 
				 new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				 new UpdateValueStrategy().setConverter(new Converter(ConnectionPoint.class, String.class)
				 	{																		 											
						@Override
						public Object convert(Object fromObject) 
						{				
							if(fromObject instanceof ConnectionPoint)
							{
								return ((ConnectionPoint) fromObject).getName();
							}
							else
							{
								return "";
							}
						}
					}));

		
		// Child Type Member
		// Parent Type Member
		IObservableValue<TypeMember> observeChildTypeMember = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  											   FeaturePath.fromList(ApogyCorePackage.Literals.ASSEMBLY_LINK__SUB_SYSTEM_TYPE_MEMBER)).observe(getAssemblyLink());

		IObservableValue<String> observeChildTypeMemberText = WidgetProperties.text(SWT.Modify).observe(txtChildSystem);
		
		bindingContext.bindValue(observeChildTypeMemberText,
				observeChildTypeMember, 
				 new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				 new UpdateValueStrategy().setConverter(new Converter(TypeMember.class, String.class)
				 	{																		 											
						@Override
						public Object convert(Object fromObject) 
						{				
							if(fromObject instanceof TypeMember)
							{
								return ((TypeMember) fromObject).getName();
							}
							else
							{
								return "";
							}
						}
					}));
		
		
		// Transform
		transformMatrixComposite.setMatrix4x4(getAssemblyLink().getTransformationMatrix());
			
		return bindingContext;
	}
}
