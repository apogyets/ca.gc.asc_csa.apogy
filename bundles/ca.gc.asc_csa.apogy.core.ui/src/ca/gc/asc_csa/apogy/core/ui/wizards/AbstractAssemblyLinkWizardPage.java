package ca.gc.asc_csa.apogy.core.ui.wizards;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.AssemblyLink;
import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;

public abstract class AbstractAssemblyLinkWizardPage extends WizardPage 
{
	protected ApogySystem apogySystem;
	protected AssemblyLink assemblyLink;
	
	private List<AssemblyLink> allAssemblylinks;
	
	public AbstractAssemblyLinkWizardPage(String wizardPageId, ApogySystem apogySystem, AssemblyLink assemblyLink) 
	{		
		super(wizardPageId);
		
		this.apogySystem = apogySystem;
		this.assemblyLink = assemblyLink;
	}

	protected List<TypeMember> getAllTypeMembers(ApogySystem apogySystem)
	{
		List<TypeMember> list = new ArrayList<TypeMember>();		
		
		for(TypeMember member : apogySystem.getMembers())
		{
			list.addAll(recursiveGetAllTypeMembers(member));
		}
		
		return list;
	}
	
	protected List<TypeMember> recursiveGetAllTypeMembers(TypeMember typeMember)
	{
		List<TypeMember> list = new ArrayList<TypeMember>();					
		list.add(typeMember);
				
		Type type = load(typeMember.getMemberType()); 	
		
		if(type != null)
		{
			for(TypeMember member : type.getMembers())
			{				
				list.addAll(recursiveGetAllTypeMembers(member));
			}
		}
		
		return list;
	}
	
	protected List<AssemblyLink> getAllAssemblyLinks(ApogySystem apogySystem)
	{
		if(allAssemblylinks == null)
		{
			allAssemblylinks = new ArrayList<AssemblyLink>();
			List<TypeMember> allTypeMembers = getAllTypeMembers(apogySystem);
			
			if(apogySystem.getAssemblyLinksList() != null)
			{
				allAssemblylinks.addAll(apogySystem.getAssemblyLinksList().getAssemblyLinks());
			}
			
			for(TypeMember typeMember : allTypeMembers)
			{
				Type type = load(typeMember.getMemberType());
						
				if(type instanceof ApogySystem)
				{
					ApogySystem system = (ApogySystem) type;
					
					if(system.getAssemblyLinksList() != null)
					{
						allAssemblylinks.addAll(system.getAssemblyLinksList().getAssemblyLinks());
					}				
				}
			}
		}		
		return allAssemblylinks;
	}
	
	
	protected Type load(Type type)
	{
		if(type != null && type.eIsProxy())
		{
			Type loadedType = null;
			
			ResourceSet resourceSet = apogySystem.eResource().getResourceSet();
			
		    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	        Map<String, Object> m = reg.getExtensionToFactoryMap();
	        m.put("*", new XMIResourceFactoryImpl());
	
	        URI uri = EcoreUtil.getURI(type);
	        
	        // Get the resource
	        //Resource resource = resourceSet.getResource(uri, true);
	        Resource resource = resourceSet.createResource(uri);
	        
	        // Gets the loaded type.
	        try
	        {
	        	loadedType = (Type) resource.getContents().get(0);
	        }
	        catch (Exception e) 
	        {
				e.printStackTrace();
			}
	        
	        return loadedType;
		}
		else
		{
			return type;
		}
	}
}
