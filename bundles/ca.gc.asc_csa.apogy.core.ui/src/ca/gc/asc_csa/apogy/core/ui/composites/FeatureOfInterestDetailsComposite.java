package ca.gc.asc_csa.apogy.core.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.FeatureOfInterest;

public class FeatureOfInterestDetailsComposite extends Composite 
{
	private FeatureOfInterest featureOfInterest;		
	private Composite compositeDetails;
	
	public FeatureOfInterestDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.widthHint = 402;
		gd_compositeDetails.minimumWidth = 402;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	

	}

	public FeatureOfInterest getFeatureOfInterest() {
		return featureOfInterest;
	}

	public void setFeatureOfInterest(FeatureOfInterest newFeatureOfInterest) 
	{
		this.featureOfInterest = newFeatureOfInterest;
		
		// Update Details EMFForm.
		if(newFeatureOfInterest != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newFeatureOfInterest);
		}
	}		
}
