package ca.gc.asc_csa.apogy.common.topology.addons.primitives;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Spot Light</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * *
 * Represents a spot light. A spot light emits a cone of light from a position and along the +Z axis direction. It can be used to fake torch lights or car's lights.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight#getSpotRange <em>Spot Range</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight#getSpreadAngle <em>Spread Angle</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage#getSpotLight()
 * @model
 * @generated
 */
public interface SpotLight extends Light {
	/**
	 * Returns the value of the '<em><b>Spot Range</b></em>' attribute.
	 * The default value is <code>"10.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * The range beyond which the light is attenuated.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Spot Range</em>' attribute.
	 * @see #setSpotRange(float)
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage#getSpotLight_SpotRange()
	 * @model default="10.0" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	float getSpotRange();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight#getSpotRange <em>Spot Range</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @param value the new value of the '<em>Spot Range</em>' attribute.
	 * @see #getSpotRange()
	 * @generated
	 */
	void setSpotRange(float value);

	/**
	 * Returns the value of the '<em><b>Spread Angle</b></em>' attribute.
	 * The default value is <code>"0.52"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Spread Angle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * The light cone spread angle.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Spread Angle</em>' attribute.
	 * @see #setSpreadAngle(float)
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage#getSpotLight_SpreadAngle()
	 * @model default="0.52" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	float getSpreadAngle();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight#getSpreadAngle <em>Spread Angle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @param value the new value of the '<em>Spread Angle</em>' attribute.
	 * @see #getSpreadAngle()
	 * @generated
	 */
	void setSpreadAngle(float value);

} // SpotLight
