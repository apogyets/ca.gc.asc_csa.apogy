package ca.gc.asc_csa.apogy.common.topology.addons.primitives.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathPackage;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.AmbientLight;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.DirectionalLight;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.Label;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.Light;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.PickVector;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.Plane;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.PointLight;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesFacade;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpherePrimitive;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.Vector;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.WayPoint;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCommonTopologyAddonsPrimitivesPackageImpl extends EPackageImpl implements ApogyCommonTopologyAddonsPrimitivesPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass vectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass pickVectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass planeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass wayPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass labelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass spherePrimitiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass lightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass ambientLightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass directionalLightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass pointLightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass spotLightEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass apogyCommonTopologyAddonsPrimitivesFacadeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType point3dEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType vector3dEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonTopologyAddonsPrimitivesPackageImpl() {
		super(eNS_URI, ApogyCommonTopologyAddonsPrimitivesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonTopologyAddonsPrimitivesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonTopologyAddonsPrimitivesPackage init() {
		if (isInited) return (ApogyCommonTopologyAddonsPrimitivesPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyAddonsPrimitivesPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonTopologyAddonsPrimitivesPackageImpl theApogyCommonTopologyAddonsPrimitivesPackage = (ApogyCommonTopologyAddonsPrimitivesPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonTopologyAddonsPrimitivesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonTopologyAddonsPrimitivesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonTopologyPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonTopologyAddonsPrimitivesPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonTopologyAddonsPrimitivesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonTopologyAddonsPrimitivesPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonTopologyAddonsPrimitivesPackage.eNS_URI, theApogyCommonTopologyAddonsPrimitivesPackage);
		return theApogyCommonTopologyAddonsPrimitivesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getVector() {
		return vectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getVector_Coordinates() {
		return (EReference)vectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getVector_CurrentLength() {
		return (EAttribute)vectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getVector__SetLength__double() {
		return vectorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getPickVector() {
		return pickVectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getPickVector_IntersectionDistance() {
		return (EAttribute)pickVectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getPickVector_IntersectedNode() {
		return (EReference)pickVectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getPickVector_RelativeIntersectionPosition() {
		return (EAttribute)pickVectorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getPickVector_AbsoluteIntersectionPosition() {
		return (EAttribute)pickVectorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getPickVector_NodeTypesInIntersection() {
		return (EReference)pickVectorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getPickVector_NodeTypesToExcludeFromIntersection() {
		return (EReference)pickVectorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getPickVector__IsNodeIncludedInIntersection__Node() {
		return pickVectorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getPlane() {
		return planeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getPlane_V0() {
		return (EReference)planeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getPlane_V1() {
		return (EReference)planeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getPlane_Width() {
		return (EAttribute)planeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getPlane_Height() {
		return (EAttribute)planeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getWayPoint() {
		return wayPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getLabel() {
		return labelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getSpherePrimitive() {
		return spherePrimitiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getSpherePrimitive_Radius() {
		return (EAttribute)spherePrimitiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getLight() {
		return lightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getLight_Enabled() {
		return (EAttribute)lightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getLight_Color() {
		return (EReference)lightEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAmbientLight() {
		return ambientLightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getDirectionalLight() {
		return directionalLightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getDirectionalLight_Direction() {
		return (EReference)directionalLightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getPointLight() {
		return pointLightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getPointLight_Radius() {
		return (EAttribute)pointLightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getSpotLight() {
		return spotLightEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getSpotLight_SpotRange() {
		return (EAttribute)spotLightEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getSpotLight_SpreadAngle() {
		return (EAttribute)spotLightEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getApogyCommonTopologyAddonsPrimitivesFacade() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreateVector__Vector() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreateVector__Point3d_Point3d() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreateVector__double_double_double() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreatePlane__Vector3d_Vector3d_double_double() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreateAmbientLight__Tuple3d() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreateAmbientLight__boolean_Tuple3d() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreateDirectionalLight__Tuple3d_Tuple3d() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreateDirectionalLight__boolean_Tuple3d_Tuple3d() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreatePointLight__Tuple3d() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreatePointLight__Tuple3d_float() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyAddonsPrimitivesFacade__CreateSpotLight__Tuple3d_float_float() {
		return apogyCommonTopologyAddonsPrimitivesFacadeEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getPoint3d() {
		return point3dEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getVector3d() {
		return vector3dEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesFactory getApogyCommonTopologyAddonsPrimitivesFactory() {
		return (ApogyCommonTopologyAddonsPrimitivesFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		vectorEClass = createEClass(VECTOR);
		createEReference(vectorEClass, VECTOR__COORDINATES);
		createEAttribute(vectorEClass, VECTOR__CURRENT_LENGTH);
		createEOperation(vectorEClass, VECTOR___SET_LENGTH__DOUBLE);

		pickVectorEClass = createEClass(PICK_VECTOR);
		createEAttribute(pickVectorEClass, PICK_VECTOR__INTERSECTION_DISTANCE);
		createEReference(pickVectorEClass, PICK_VECTOR__INTERSECTED_NODE);
		createEAttribute(pickVectorEClass, PICK_VECTOR__RELATIVE_INTERSECTION_POSITION);
		createEAttribute(pickVectorEClass, PICK_VECTOR__ABSOLUTE_INTERSECTION_POSITION);
		createEReference(pickVectorEClass, PICK_VECTOR__NODE_TYPES_IN_INTERSECTION);
		createEReference(pickVectorEClass, PICK_VECTOR__NODE_TYPES_TO_EXCLUDE_FROM_INTERSECTION);
		createEOperation(pickVectorEClass, PICK_VECTOR___IS_NODE_INCLUDED_IN_INTERSECTION__NODE);

		planeEClass = createEClass(PLANE);
		createEReference(planeEClass, PLANE__V0);
		createEReference(planeEClass, PLANE__V1);
		createEAttribute(planeEClass, PLANE__WIDTH);
		createEAttribute(planeEClass, PLANE__HEIGHT);

		wayPointEClass = createEClass(WAY_POINT);

		labelEClass = createEClass(LABEL);

		spherePrimitiveEClass = createEClass(SPHERE_PRIMITIVE);
		createEAttribute(spherePrimitiveEClass, SPHERE_PRIMITIVE__RADIUS);

		lightEClass = createEClass(LIGHT);
		createEAttribute(lightEClass, LIGHT__ENABLED);
		createEReference(lightEClass, LIGHT__COLOR);

		ambientLightEClass = createEClass(AMBIENT_LIGHT);

		directionalLightEClass = createEClass(DIRECTIONAL_LIGHT);
		createEReference(directionalLightEClass, DIRECTIONAL_LIGHT__DIRECTION);

		pointLightEClass = createEClass(POINT_LIGHT);
		createEAttribute(pointLightEClass, POINT_LIGHT__RADIUS);

		spotLightEClass = createEClass(SPOT_LIGHT);
		createEAttribute(spotLightEClass, SPOT_LIGHT__SPOT_RANGE);
		createEAttribute(spotLightEClass, SPOT_LIGHT__SPREAD_ANGLE);

		apogyCommonTopologyAddonsPrimitivesFacadeEClass = createEClass(APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_VECTOR__VECTOR);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_VECTOR__POINT3D_POINT3D);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_VECTOR__DOUBLE_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_PLANE__VECTOR3D_VECTOR3D_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_AMBIENT_LIGHT__TUPLE3D);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_AMBIENT_LIGHT__BOOLEAN_TUPLE3D);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_DIRECTIONAL_LIGHT__TUPLE3D_TUPLE3D);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_DIRECTIONAL_LIGHT__BOOLEAN_TUPLE3D_TUPLE3D);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_POINT_LIGHT__TUPLE3D);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_POINT_LIGHT__TUPLE3D_FLOAT);
		createEOperation(apogyCommonTopologyAddonsPrimitivesFacadeEClass, APOGY_COMMON_TOPOLOGY_ADDONS_PRIMITIVES_FACADE___CREATE_SPOT_LIGHT__TUPLE3D_FLOAT_FLOAT);

		// Create data types
		point3dEDataType = createEDataType(POINT3D);
		vector3dEDataType = createEDataType(VECTOR3D);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonTopologyPackage theApogyCommonTopologyPackage = (ApogyCommonTopologyPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyPackage.eNS_URI);
		ApogyCommonMathPackage theApogyCommonMathPackage = (ApogyCommonMathPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonMathPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		vectorEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getNode());
		pickVectorEClass.getESuperTypes().add(this.getVector());
		planeEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getNode());
		wayPointEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getAggregateGroupNode());
		labelEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getNode());
		spherePrimitiveEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getAggregateGroupNode());
		lightEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getLeaf());
		ambientLightEClass.getESuperTypes().add(this.getLight());
		directionalLightEClass.getESuperTypes().add(this.getLight());
		pointLightEClass.getESuperTypes().add(this.getLight());
		spotLightEClass.getESuperTypes().add(this.getLight());

		// Initialize classes, features, and operations; add parameters
		initEClass(vectorEClass, Vector.class, "Vector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVector_Coordinates(), theApogyCommonMathPackage.getTuple3d(), null, "coordinates", null, 0, 1, Vector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVector_CurrentLength(), theEcorePackage.getEDouble(), "currentLength", null, 0, 1, Vector.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getVector__SetLength__double(), null, "setLength", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "length", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(pickVectorEClass, PickVector.class, "PickVector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPickVector_IntersectionDistance(), theEcorePackage.getEDouble(), "intersectionDistance", "-1.0", 0, 1, PickVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPickVector_IntersectedNode(), theApogyCommonTopologyPackage.getNode(), null, "intersectedNode", null, 0, 1, PickVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPickVector_RelativeIntersectionPosition(), this.getPoint3d(), "relativeIntersectionPosition", null, 0, 1, PickVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPickVector_AbsoluteIntersectionPosition(), this.getPoint3d(), "absoluteIntersectionPosition", null, 0, 1, PickVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPickVector_NodeTypesInIntersection(), theEcorePackage.getEClass(), null, "nodeTypesInIntersection", null, 0, -1, PickVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPickVector_NodeTypesToExcludeFromIntersection(), theEcorePackage.getEClass(), null, "nodeTypesToExcludeFromIntersection", null, 0, -1, PickVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getPickVector__IsNodeIncludedInIntersection__Node(), theEcorePackage.getEBoolean(), "isNodeIncludedInIntersection", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonTopologyPackage.getNode(), "node", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(planeEClass, Plane.class, "Plane", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPlane_V0(), theApogyCommonMathPackage.getTuple3d(), null, "v0", null, 0, 1, Plane.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPlane_V1(), theApogyCommonMathPackage.getTuple3d(), null, "v1", null, 0, 1, Plane.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlane_Width(), theEcorePackage.getEDouble(), "width", null, 0, 1, Plane.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPlane_Height(), theEcorePackage.getEDouble(), "height", null, 0, 1, Plane.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(wayPointEClass, WayPoint.class, "WayPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(labelEClass, Label.class, "Label", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(spherePrimitiveEClass, SpherePrimitive.class, "SpherePrimitive", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSpherePrimitive_Radius(), theEcorePackage.getEDouble(), "radius", null, 0, 1, SpherePrimitive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(lightEClass, Light.class, "Light", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLight_Enabled(), theEcorePackage.getEBoolean(), "enabled", "false", 0, 1, Light.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLight_Color(), theApogyCommonMathPackage.getTuple3d(), null, "color", null, 0, 1, Light.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ambientLightEClass, AmbientLight.class, "AmbientLight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(directionalLightEClass, DirectionalLight.class, "DirectionalLight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDirectionalLight_Direction(), theApogyCommonMathPackage.getTuple3d(), null, "direction", null, 0, 1, DirectionalLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pointLightEClass, PointLight.class, "PointLight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPointLight_Radius(), theEcorePackage.getEFloat(), "radius", "10.0", 0, 1, PointLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(spotLightEClass, SpotLight.class, "SpotLight", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSpotLight_SpotRange(), theEcorePackage.getEFloat(), "spotRange", "10.0", 0, 1, SpotLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSpotLight_SpreadAngle(), theEcorePackage.getEFloat(), "spreadAngle", "0.52", 0, 1, SpotLight.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(apogyCommonTopologyAddonsPrimitivesFacadeEClass, ApogyCommonTopologyAddonsPrimitivesFacade.class, "ApogyCommonTopologyAddonsPrimitivesFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreateVector__Vector(), this.getVector(), "createVector", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVector(), "vector", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreateVector__Point3d_Point3d(), this.getVector(), "createVector", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPoint3d(), "p0", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPoint3d(), "p1", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreateVector__double_double_double(), this.getVector(), "createVector", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "x", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "y", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "z", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreatePlane__Vector3d_Vector3d_double_double(), this.getPlane(), "createPlane", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVector3d(), "v0", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVector3d(), "v1", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "width", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "height", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreateAmbientLight__Tuple3d(), this.getAmbientLight(), "createAmbientLight", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "color", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreateAmbientLight__boolean_Tuple3d(), this.getAmbientLight(), "createAmbientLight", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "lightOn", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "color", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreateDirectionalLight__Tuple3d_Tuple3d(), this.getDirectionalLight(), "createDirectionalLight", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "color", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "direction", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreateDirectionalLight__boolean_Tuple3d_Tuple3d(), this.getDirectionalLight(), "createDirectionalLight", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEBoolean(), "lightOn", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "color", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "direction", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreatePointLight__Tuple3d(), this.getPointLight(), "createPointLight", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "color", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreatePointLight__Tuple3d_float(), this.getPointLight(), "createPointLight", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "color", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEFloat(), "radius", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyAddonsPrimitivesFacade__CreateSpotLight__Tuple3d_float_float(), this.getSpotLight(), "createSpotLight", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonMathPackage.getTuple3d(), "color", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEFloat(), "spreadAngle", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEFloat(), "spotRange", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Initialize data types
		initEDataType(point3dEDataType, Point3d.class, "Point3d", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(vector3dEDataType, Vector3d.class, "Vector3d", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "documentation", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca),\n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "prefix", "ApogyCommonTopologyAddonsPrimitives",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "modelName", "ApogyCommonTopologyAddonsPrimitives",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.common.topology.addons.primitives/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.common.topology.addons.primitives.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.common.topology.addons"
		   });	
		addAnnotation
		  (vectorEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nNode that defines a vector by its length and rotation angles. When all rotation are zero, the vector points along the X-Axis."
		   });	
		addAnnotation
		  (getVector__SetLength__double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nChanges the length of the vector."
		   });	
		addAnnotation
		  ((getVector__SetLength__double()).getEParameters().get(0), 
		   source, 
		   new String[] {
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (getVector_CurrentLength(), 
		   source, 
		   new String[] {
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (pickVectorEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA Vector than support intersections."
		   });	
		addAnnotation
		  (getPickVector__IsNodeIncludedInIntersection__Node(), 
		   source, 
		   new String[] {
			 "documentation", "Returns whether or not the specified node should be considered when processing intersection."
		   });	
		addAnnotation
		  (getPickVector_IntersectionDistance(), 
		   source, 
		   new String[] {
			 "documentation", "The current shortest intersection distance between\nthe ray and a node of one of the included type.",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (getPickVector_IntersectedNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe node to which the intersected geometry belongs."
		   });	
		addAnnotation
		  (planeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA plane."
		   });	
		addAnnotation
		  (spherePrimitiveEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA sphere."
		   });	
		addAnnotation
		  (getSpherePrimitive_Radius(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe radius of the sphere.",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (lightEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nBase class of all lights."
		   });	
		addAnnotation
		  (getLight_Enabled(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not the light in enabled."
		   });	
		addAnnotation
		  (getLight_Color(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe color of the light."
		   });	
		addAnnotation
		  (ambientLightEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA light that illuminates all object uniformly."
		   });	
		addAnnotation
		  (directionalLightEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nThe DirectionalLight object specifies a light source that shines in along a given direction."
		   });	
		addAnnotation
		  (getDirectionalLight_Direction(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe direction vector of the light."
		   });	
		addAnnotation
		  (pointLightEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nThe PointLight object specifies an attenuated light source at a fixed point in space that radiates light equally in all directions away from the light source."
		   });	
		addAnnotation
		  (getPointLight_Radius(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe radius beyond which the light is attenuated.",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (spotLightEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nRepresents a spot light. A spot light emits a cone of light from a position and along the +Z axis direction. It can be used to fake torch lights or car\'s lights."
		   });	
		addAnnotation
		  (getSpotLight_SpotRange(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe range beyond which the light is attenuated.",
			 "apogy_units", "m"
		   });	
		addAnnotation
		  (getSpotLight_SpreadAngle(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe light cone spread angle.",
			 "apogy_units", "rad"
		   });
	}

} //ApogyCommonTopologyAddonsPrimitivesPackageImpl
