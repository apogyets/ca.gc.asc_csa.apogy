package ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.core.environment.orbit.earth.GroundStation;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.ApogyCoreEnvironmentOrbitEarthUIPackage;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.GroundStationWorldWindLayer;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ground Station World Wind Layer</b></em>'.
 * <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.orbit.earth.ui.impl.GroundStationWorldWindLayerImpl#getReferedGroundStation <em>Refered Ground Station</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GroundStationWorldWindLayerImpl extends AbstractGroundStationWorldWindLayerImpl implements GroundStationWorldWindLayer 
{
	/**
	 * The cached value of the '{@link #getReferedGroundStation() <em>Refered Ground Station</em>}' reference.
	 * <!-- begin-user-doc -->	 * <!-- end-user-doc -->	 * @see #getReferedGroundStation()
	 * @generated
	 * @ordered
	 */
	protected GroundStation referedGroundStation;

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected GroundStationWorldWindLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentOrbitEarthUIPackage.Literals.GROUND_STATION_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->	 * <!-- end-user-doc -->	 * @generated
	 */
	public GroundStation getReferedGroundStation() {
		if (referedGroundStation != null && referedGroundStation.eIsProxy()) {
			InternalEObject oldReferedGroundStation = (InternalEObject)referedGroundStation;
			referedGroundStation = (GroundStation)eResolveProxy(oldReferedGroundStation);
			if (referedGroundStation != oldReferedGroundStation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCoreEnvironmentOrbitEarthUIPackage.GROUND_STATION_WORLD_WIND_LAYER__REFERED_GROUND_STATION, oldReferedGroundStation, referedGroundStation));
			}
		}
		return referedGroundStation;
	}

	/**
	 * <!-- begin-user-doc -->	 * <!-- end-user-doc -->	 * @generated
	 */
	public GroundStation basicGetReferedGroundStation() {
		return referedGroundStation;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */

	public void setReferedGroundStation(GroundStation newReferedGroundStation) 
	{
		setReferedGroundStationGen(newReferedGroundStation);
		setGroundStation(newReferedGroundStation);
	}

	/**
	 * <!-- begin-user-doc -->	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setReferedGroundStationGen(GroundStation newReferedGroundStation) {
		GroundStation oldReferedGroundStation = referedGroundStation;
		referedGroundStation = newReferedGroundStation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreEnvironmentOrbitEarthUIPackage.GROUND_STATION_WORLD_WIND_LAYER__REFERED_GROUND_STATION, oldReferedGroundStation, referedGroundStation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.GROUND_STATION_WORLD_WIND_LAYER__REFERED_GROUND_STATION:
				if (resolve) return getReferedGroundStation();
				return basicGetReferedGroundStation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.GROUND_STATION_WORLD_WIND_LAYER__REFERED_GROUND_STATION:
				setReferedGroundStation((GroundStation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.GROUND_STATION_WORLD_WIND_LAYER__REFERED_GROUND_STATION:
				setReferedGroundStation((GroundStation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreEnvironmentOrbitEarthUIPackage.GROUND_STATION_WORLD_WIND_LAYER__REFERED_GROUND_STATION:
				return referedGroundStation != null;
		}
		return super.eIsSet(featureID);
	}
		
} //GroundStationWorldWindLayerImpl
