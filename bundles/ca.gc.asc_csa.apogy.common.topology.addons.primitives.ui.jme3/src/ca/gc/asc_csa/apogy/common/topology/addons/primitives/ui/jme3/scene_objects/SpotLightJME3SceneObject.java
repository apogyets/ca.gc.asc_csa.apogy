package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.concurrent.Callable;

import javax.vecmath.Color3f;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.swt.graphics.RGB;

import com.jme3.app.state.AbstractAppState;
import com.jme3.asset.AssetManager;
import com.jme3.light.SpotLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.jme3.JME3PrimitivesUtilities;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.scene_objects.SpotLightSceneObject;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.Activator;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;

public class SpotLightJME3SceneObject extends DefaultJME3SceneObject<ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight> implements SpotLightSceneObject 
{
	public static ColorRGBA DEFAULT_LIGHT_CONE_COLOR = new ColorRGBA(1f, 1f, 1.0f, 1.0f);
	
	protected ColorRGBA fovColor = new ColorRGBA(0f, 1f, 0.0f, 0.25f);
	protected RGB rgb = new  RGB(1, 1, 1);	
	
	public static final float INNER_TO_OUTER_SPREAD_RATIO = 1.8f;
	
	private MeshPresentationMode meshPresentationMode = MeshPresentationMode.WIREFRAME;
	protected boolean lightConeVisible = false;
	private float previousAxisLength = 1.0f;
	private boolean axisVisible = true;
	
	private Adapter adapter;	
	private UpdateAppState updateAppState = null;
	public static float DEFAULT_POINT_LIGHT_RADIUS = 100.0f;
		
	private AssetManager assetManager;
	
	private Geometry fovGeometry = null;	
	private Geometry axisGeometry = null;	
	
	protected com.jme3.scene.Node lightConeNode;
	
	private SpotLight spotLight;
	
	public SpotLightJME3SceneObject(ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight topologyNode, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(topologyNode, jme3RenderEngineDelegate);
														
		this.assetManager = jme3Application.getAssetManager();
		
		jme3Application.getStateManager().attach(getUpdateAppState());
		
		// Creates the 3DAxis.
		axisGeometry = JME3Utilities.createAxis3D(1.0f, assetManager);
		
		updateGeometry();
				
		topologyNode.eAdapters().add(getAdapter());				
	}		
	
	
	@Override
	public void setColor(RGB rgb) 
	{
		this.rgb = rgb;
		fovColor = JME3Utilities.convertToColorRGBA(rgb);
		updateGeometry();
	}
	
	@Override
	public RGB getColor() 
	{	
		return rgb;
	}
	
	@Override
	public void dispose() 
	{
		if(getTopologyNode() != null)
		{
			getTopologyNode().eAdapters().remove(getAdapter());
		}
		
		jme3Application.getStateManager().detach(getUpdateAppState());
		
		if(spotLight != null)
		{
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{
					jme3Application.getRootNode().removeLight(spotLight);
					return null;
				}
			});
		}
		super.dispose();
	}
	
	public void setSpotSpreadAngle(final float spreadAngle)
	{
		Logger.INSTANCE.log(Activator.ID, this, "Setting Spot Spread Angle<" + Math.toDegrees(spreadAngle) + "> degrees...", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				getSpotLight().setSpotInnerAngle(spreadAngle);
				getSpotLight().setSpotOuterAngle(spreadAngle * INNER_TO_OUTER_SPREAD_RATIO);
				return null;
			}
		});
	}
	
	public void setSpotRange(final float spotRange)
	{
		Logger.INSTANCE.log(Activator.ID, this, "Setting Spot Range <" + spotRange + "> meters...", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				getSpotLight().setSpotRange(spotRange);						
				return null;
			}
		});
	}
	
	public void setLightColor(Color3f color)
	{
		final ColorRGBA lightColor = JME3Utilities.convertToColorRGBA(color);
		
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				getSpotLight().setColor(lightColor);								
				return null;
			}
		});
	}
	
	@Override
	public MeshPresentationMode getPresentationMode() 
	{		
		return meshPresentationMode;
	}

	@Override
	public void setPresentationMode(final MeshPresentationMode mode) 
	{		
		this.meshPresentationMode = mode;

		if(fovGeometry != null && fovGeometry.getMaterial() != null)
		{
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{			
					internalSetPresentationMode(mode);
					
					return null;
				}
			});
		}
		
	}

	@Override
	public void setLightConeVisible(boolean visible) 
	{		
		Logger.INSTANCE.log(Activator.ID, this, "Set Light Cone Visible <" + visible + ">", EventSeverity.OK);
		
		this.lightConeVisible = visible;
		
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				if (!lightConeVisible && getAttachmentNode().hasChild(getLightConeNode())) 
				{
					getAttachmentNode().detachChild(getLightConeNode());
				} 
				else if (lightConeVisible && !getAttachmentNode().hasChild(getLightConeNode())) 
				{
					getAttachmentNode().attachChild(getLightConeNode());
				}
				return null;
			}
		});			
	}

	@Override
	public boolean isLightConeVisible() 
	{		
		return lightConeVisible;
	}

	@Override
	public void setAxisVisible(boolean visible) 
	{	
		Logger.INSTANCE.log(Activator.ID, this, "Setting axis visible to <" + visible + ">.", EventSeverity.INFO);
		this.axisVisible = visible;
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				if(axisVisible)
				{
					getAttachmentNode().attachChild(axisGeometry);
				}
				else
				{
					getAttachmentNode().detachChild(axisGeometry);
				}
				return null;
			}
		});	
	}

	@Override
	public void setAxisLength(double length) 
	{
		if(length > 0)
		{
			Logger.INSTANCE.log(Activator.ID, this, "Setting axis length to <" + length + ">.", EventSeverity.INFO);
			
			jme3Application.enqueue(new Callable<Object>() 
			{
					@Override
					public Object call() throws Exception 
					{
						try
						{
							float scale = (float) Math.abs(length) / previousAxisLength;
							
							// Scales existing axis.
							if(axisGeometry != null ) axisGeometry.scale(scale);
							
							previousAxisLength = (float) length;
						}
						catch(Throwable t)
						{
							Logger.INSTANCE.log(Activator.ID, this, "Failed to setAxisLength(" + length + ")!", EventSeverity.ERROR, t);
						}
						
						return null;
					}
			});			
		}
		else
		{
			Logger.INSTANCE.log(Activator.ID, this, "Setting axis length to <" + length + "> failed : Length must be greater than zero !", EventSeverity.ERROR);
		}
	}
	
	private void setLightEnabled(boolean enabled)
	{
		if(spotLight != null)
		{
			Logger.INSTANCE.log(Activator.ID, this, "Setting Light Enabled <" + enabled + ">.", EventSeverity.INFO);
			
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{
					if(enabled)
					{
						if(!jme3Application.getRootNode().getChildren().contains(getSpotLight()))
						{
							jme3Application.getRootNode().addLight(getSpotLight());
						}
					}
					else
					{
						jme3Application.getRootNode().removeLight(getSpotLight());	
					}
					
					return null;
				}
			});
		}	
	}
	
	private void updateGeometry()
	{
		Job job = new Job("PointLightJME3SceneObject : Updating Geometry.")
		{
			@Override
			protected IStatus run(IProgressMonitor monitor) 
			{				
				jme3Application.enqueue(new Callable<Object>() 
				{
					@Override
					public Object call() throws Exception 
					{	
						if(spotLight == null) getSpotLight();
												
						if(getTopologyNode().isEnabled())
						{
							jme3Application.getRootNode().addLight(spotLight);
						}
						else
						{
							jme3Application.getRootNode().removeLight(spotLight);	
						}
						
						// Removes previous geometry if applicable.
						if(fovGeometry != null)
						{
							getLightConeNode().detachChild(fovGeometry);			
						}
						
						// Creates new geometry.
						float maxRange = getTopologyNode().getSpotRange();						
						Mesh mesh = JME3PrimitivesUtilities.createTruncatedCone(getTopologyNode().getSpreadAngle(), 0.01f, maxRange, 32);
						
						if(getTopologyNode().getNodeId() != null) fovGeometry = new Geometry(getTopologyNode().getNodeId(), mesh);
						else fovGeometry = new Geometry("?", mesh);		
						fovGeometry.setMaterial(createMaterial());
						
						// Attaches new geometry.
						getLightConeNode().attachChild(fovGeometry);														
						
						// Sets the presentation mode.
						internalSetPresentationMode(meshPresentationMode);											
						
						return null;
					}
				});
				return Status.OK_STATUS;
			}
		};
		job.schedule();	
		
	}
	
	private SpotLight getSpotLight()
	{
		if(spotLight == null)
		{
			spotLight = new SpotLight();		
			spotLight.setColor(ColorRGBA.White);
			spotLight.setSpotInnerAngle(getTopologyNode().getSpreadAngle());
			spotLight.setSpotOuterAngle(getTopologyNode().getSpreadAngle() * INNER_TO_OUTER_SPREAD_RATIO);
			spotLight.setSpotRange(getTopologyNode().getSpotRange());
			
			Vector3f lightPosition = new Vector3f();
			if(getTopologyNode() != null)
			{
				Matrix4d m = ApogyCommonTopologyFacade.INSTANCE.expressNodeInRootFrame(getTopologyNode());
				Vector3d position = new Vector3d();
				m.get(position);
								
				lightPosition = new Vector3f((float) position.x, (float) position.y, (float) position.z);
			}
			spotLight.setPosition(lightPosition);	
			
			if(getTopologyNode().getNodeId() != null) spotLight.setName(getTopologyNode().getNodeId());
		}
		
		return spotLight;
	}	
			
	private Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
			@Override
			public void notifyChanged(Notification msg) 
			{			
				if(msg.getNotifier() instanceof ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight)
				{
					int featureID =  msg.getFeatureID(ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight.class);
										
					switch (featureID) 
					{
						case ApogyCommonTopologyAddonsPrimitivesPackage.LIGHT__ENABLED:						
							setLightEnabled(msg.getNewBooleanValue());
						break;
						
						case ApogyCommonTopologyAddonsPrimitivesPackage.LIGHT__COLOR:
							if(msg.getNewValue() instanceof Color3f)
							{
								setLightColor((Color3f) msg.getNewValue());
							}
						break;		
						
						case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPOT_RANGE:
							if(msg.getNewFloatValue() >= 0)
							{
								setSpotRange(msg.getNewFloatValue());
								updateGeometry();
							}
						break;
						
						case ApogyCommonTopologyAddonsPrimitivesPackage.SPOT_LIGHT__SPREAD_ANGLE:
							if(msg.getNewFloatValue() >= 0)
							{
								setSpotSpreadAngle(msg.getNewFloatValue());
								updateGeometry();
							}
						break;																

						default:
						break;
					}
				}
			}	
			};
		}
		return adapter;
	}
	
	protected UpdateAppState getUpdateAppState() 
	{
		if(updateAppState == null)
		{
			updateAppState = new UpdateAppState();
		}
		return updateAppState;
	}
	
	private class UpdateAppState extends AbstractAppState
	{
		private Node root = ApogyCommonTopologyFacade.INSTANCE.findRoot(getTopologyNode());	
		
		public UpdateAppState() {			
		}
		
		@Override
		public void update(float tpf) 
		{			
			
			Matrix4d newPose = ApogyCommonTopologyFacade.INSTANCE.expressInFrame(getTopologyNode(), root);									
			if(newPose != null)
			{
				// Computes the new position
				Vector3d position = new Vector3d();
				newPose.get(position);									
														
				// Computes the new orientation.
				Vector3d orientation = new Vector3d(0, 0, 1);
				Matrix3d rotation = new Matrix3d();
				newPose.get(rotation);									
				rotation.transform(orientation);																				
				orientation.normalize();
				
				getSpotLight().setPosition(new Vector3f((float) position.x, (float) position.y, (float) position.z));
				getSpotLight().setDirection(new Vector3f((float) orientation.x, (float) orientation.y, (float) orientation.z));																												
			}	
			
			super.update(tpf);
		}
	}

	private void internalSetPresentationMode(MeshPresentationMode mode)
	{
		if(fovGeometry != null && fovGeometry.getMesh() != null)
		{
			Mesh mesh = fovGeometry.getMesh();
			switch (mode.getValue()) 
			{							
				case MeshPresentationMode.SURFACE_VALUE:
					fovGeometry.getMaterial().getAdditionalRenderState().setWireframe(false);
					if(mesh != null) mesh.setMode(com.jme3.scene.Mesh.Mode.Triangles);
					break;
				case MeshPresentationMode.WIREFRAME_VALUE:
					fovGeometry.getMaterial().getAdditionalRenderState().setWireframe(true);
					if(mesh != null) mesh.setMode(com.jme3.scene.Mesh.Mode.Triangles);
					break;
				case MeshPresentationMode.POINTS_VALUE:
					fovGeometry.getMaterial().getAdditionalRenderState().setWireframe(false);
					if(mesh != null) mesh.setMode(com.jme3.scene.Mesh.Mode.Points);					
					break;
					
				default:
					break;
			}	
		}
	}
	
	/**
	 * Return the n ode to which the FOV representation should be attached.
	 * @return The node.
	 */
	protected com.jme3.scene.Node getLightConeNode()
	{
		if (lightConeNode == null) 
		{
			String name = null;
			if(getTopologyNode() != null && getTopologyNode().getNodeId() != null)
			{
				name = getTopologyNode().getNodeId();
			}
			else
			{
				name = getClass().getSimpleName();
			}		
			name += "_fov";
			
			lightConeNode = new com.jme3.scene.Node(name);	
			
			// Attaches the FOV node to the attachment node. 
			getAttachmentNode().attachChild(lightConeNode);
		}
		return lightConeNode;
	}
	
	private Material createMaterial()
	{
		Material mat = new Material(assetManager,  "Common/MatDefs/Light/Lighting.j3md");	
		
		if(getColor() != null)
		{
			mat.setColor("Diffuse", JME3Utilities.convertToColorRGBA(getColor()));
			mat.setColor("Ambient", JME3Utilities.convertToColorRGBA(getColor()));
			mat.setColor("Specular", JME3Utilities.convertToColorRGBA(getColor()));
		}
		else
		{
			mat.setColor("Diffuse", DEFAULT_LIGHT_CONE_COLOR.clone());
			mat.setColor("Ambient", DEFAULT_LIGHT_CONE_COLOR.clone());
			mat.setColor("Specular", DEFAULT_LIGHT_CONE_COLOR.clone());
		}
				
		mat.setFloat("Shininess",64f); 	
		mat.setBoolean("UseMaterialColors",true);    	
		mat.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
		
		return mat;
	}
}
