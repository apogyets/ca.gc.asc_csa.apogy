package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.swt.graphics.RGB;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.shape.Sphere;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpherePrimitive;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.jme3.Activator;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;

public class SpherePrimitiveJME3SceneObject extends DefaultJME3SceneObject<SpherePrimitive> 
{
	public static int Z_SAMPLES = 100;
	public static int RADIAL_SAMPLES = 36;
	
	private Adapter sphereAdapter;
		
	private Geometry sphereGeometry = null;
	
	private AssetManager assetManager;
	
	public SpherePrimitiveJME3SceneObject(SpherePrimitive topologyNode, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(topologyNode, jme3RenderEngineDelegate);
		
		this.assetManager = jme3Application.getAssetManager();
		
		Job job = new Job("SpherePrimitiveJME3SceneObject : Initialize Geometry.")
		{
			@Override
			protected IStatus run(IProgressMonitor monitor) 
			{	
				jme3Application.enqueue(new Callable<Object>() 
				{
					@Override
					public Object call() throws Exception 
					{												
						createGeometry();					
						return null;
					}
				});
				return Status.OK_STATUS;
			}
		};
		job.schedule();	
		
		
				
		topologyNode.eAdapters().add(getSphereAdapter());									
	}
	
	@Override
	public List<Geometry> getGeometries() 
	{		
		List<Geometry> geometries = new ArrayList<Geometry>();
		geometries.add(sphereGeometry);
		return geometries;
	}
	
	private void createGeometry()
	{
		sphereGeometry = createSphereGeometry();
		getAttachmentNode().attachChild(sphereGeometry);		
	}
	
	private Geometry createSphereGeometry()
	{
		Mesh mesh = createMesh();				
		Geometry geometry = new Geometry("Sphere", mesh);
		geometry.setMaterial(createMaterial());				
		return geometry;		
	}
	
	private Mesh createMesh()
	{		
		Sphere sphere = new Sphere(Z_SAMPLES, RADIAL_SAMPLES, (float) getTopologyNode().getRadius());			
		return sphere;
	}
	
	@Override
	public void setColor(RGB rgb) 
	{
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				try
				{					
					if(sphereGeometry != null)
					{
						Material mat = createMaterial();
						mat.setColor("Diffuse", JME3Utilities.convertToColorRGBA(rgb));
						mat.setColor("Ambient", JME3Utilities.convertToColorRGBA(rgb));
						mat.setColor("Specular", JME3Utilities.convertToColorRGBA(rgb)); 			
						sphereGeometry.setMaterial(mat);
					}
				}
				catch(Throwable t)
				{
					Logger.INSTANCE.log(Activator.ID, this, "Failed to set color to <" + rgb + ">!", EventSeverity.ERROR, t);
				}
				
				return null;
			}
		});
		
	}
	
	private void updateGeometry()
	{
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{										
				if(sphereGeometry != null) getAttachmentNode().detachChild(sphereGeometry);
				
				// Create new geometry
				sphereGeometry = createSphereGeometry();
				getAttachmentNode().attachChild(sphereGeometry);
				
				return null;
			}	
		});		
	}	
	
	private Material createMaterial()
	{
		Material mat = new Material(assetManager,  "Common/MatDefs/Light/Lighting.j3md");	
				
		if(getColor() != null)
		{
			mat.setColor("Diffuse", JME3Utilities.convertToColorRGBA(getColor()));
			mat.setColor("Ambient", JME3Utilities.convertToColorRGBA(getColor()));
			mat.setColor("Specular", JME3Utilities.convertToColorRGBA(getColor()));
		}
		else
		{
			mat.setColor("Diffuse", ColorRGBA.White);
			mat.setColor("Ambient", ColorRGBA.White);
			mat.setColor("Specular",ColorRGBA.White);
		}
				
		mat.setFloat("Shininess",64f); 	
		mat.setBoolean("UseMaterialColors",true);    		
		
		return mat;
	}
	
	private Adapter getSphereAdapter() 
	{
		if (sphereAdapter == null) 
		{
			sphereAdapter = new Adapter() 
			{				
				@Override
				public Notifier getTarget() 
				{
					return null;
				}

				@Override
				public boolean isAdapterForType(Object type) 
				{
					return false;
				}

				@Override
				public void notifyChanged(Notification notification) 
				{
					if (notification.getFeatureID(SpherePrimitive.class) == ApogyCommonTopologyAddonsPrimitivesPackage.SPHERE_PRIMITIVE__RADIUS) 
					{									
						updateGeometry();
					}
				}

				@Override
				public void setTarget(Notifier newTarget) 
				{
				}

			};
		}

		return sphereAdapter;
	}
	
	
}
