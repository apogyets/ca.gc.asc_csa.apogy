package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;

public class EarthAtmosphereWorksiteOriginComposite extends Composite 
{
	private EarthAtmosphereWorksite earthSurfaceWorksite;
	
	private DataBindingContext m_bindingContext;
	
	private Text txtLatitudevalue;
	private Text txtLongitudevalue;
	private Text txtAltitudevalue;
	
	public EarthAtmosphereWorksiteOriginComposite(Composite parent, int style) 
	{
		super(parent, SWT.NO_BACKGROUND);	
		setLayout(new GridLayout(3, false));
		
		Label lblLatitude = new Label(this, SWT.NONE);
		lblLatitude.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLatitude.setText("Latitude:");
		
		txtLatitudevalue = new Text(this, SWT.NONE);
		txtLatitudevalue.setText("0.0");
		GridData gd_txtLatitudevalue = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtLatitudevalue.widthHint = 150;
		gd_txtLatitudevalue.minimumWidth = 150;
		txtLatitudevalue.setLayoutData(gd_txtLatitudevalue);
		txtLatitudevalue.setToolTipText("Latitude of the origin of the worksite, in degrees.Latitude north of the equator are positive while those south of the equator are negative.");
		
		Label lblDegres = new Label(this, SWT.NONE);
		lblDegres.setText("degrees");
		
		Label lblLongitude = new Label(this, SWT.NONE);
		lblLongitude.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblLongitude.setText("Longitude:");
		
		txtLongitudevalue = new Text(this, SWT.NONE);
		txtLongitudevalue.setText("0.0");
		GridData gd_txtLongitudevalue = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtLongitudevalue.minimumWidth = 150;
		gd_txtLongitudevalue.widthHint = 150;
		txtLongitudevalue.setLayoutData(gd_txtLongitudevalue);
		txtLongitudevalue.setToolTipText("Longitude of the origin of the worksite, in degrees. Longitude east of Greenwich, UK  are positive while those west of Greenwich are negative.");
		
		Label lblDegres_1 = new Label(this, SWT.NONE);
		lblDegres_1.setText("degrees");
		
		Label lblAltitude = new Label(this, SWT.NONE);
		lblAltitude.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAltitude.setText("Altitude:");
		
		txtAltitudevalue = new Text(this, SWT.NONE);
		txtAltitudevalue.setText("0.0");
		GridData gd_txtAltitudevalue = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_txtAltitudevalue.widthHint = 150;
		gd_txtAltitudevalue.minimumWidth = 150;
		txtAltitudevalue.setLayoutData(gd_txtAltitudevalue);
		txtAltitudevalue.setToolTipText("Height above the Earth's sea level, in meters.");
		
		Label lblMeters = new Label(this, SWT.NONE);
		lblMeters.setText("meters");
		
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public EarthAtmosphereWorksite getEarthAtmosphereWorksite() 
	{
		return earthSurfaceWorksite;
	}

	public void setEarthAtmosphereWorksite(EarthAtmosphereWorksite earthSurfaceWorksite) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.earthSurfaceWorksite = earthSurfaceWorksite;
		
		if(earthSurfaceWorksite != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		/* Latitude Value. */
		IObservableValue<Double> observeLatitude = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyEarthEnvironmentPackage.Literals.EARTH_WORKSITE__GEOGRAPHICAL_COORDINATES, ApogyEarthEnvironmentPackage.Literals.GEOGRAPHIC_COORDINATES__LATITUDE)).observe(getEarthAtmosphereWorksite());
		IObservableValue<String> observeLatitudeLabelValueText = WidgetProperties.text(SWT.Modify).observe(txtLatitudevalue);

		bindingContext.bindValue(observeLatitudeLabelValueText,
								 observeLatitude, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Math.toRadians(Double.parseDouble((String) fromObject));
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											Double degrees =  Math.toDegrees((Double) fromObject);
											return degrees.toString();
										}

									}));
		
		/* Longitude Value. */
		IObservableValue<Double> observeLongitude = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyEarthEnvironmentPackage.Literals.EARTH_WORKSITE__GEOGRAPHICAL_COORDINATES, ApogyEarthEnvironmentPackage.Literals.GEOGRAPHIC_COORDINATES__LONGITUDE)).observe(getEarthAtmosphereWorksite());
		IObservableValue<String> observeLongitudeLabelValueText = WidgetProperties.text(SWT.Modify).observe(txtLongitudevalue);

		bindingContext.bindValue(observeLongitudeLabelValueText,
								 observeLongitude, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Math.toRadians(Double.parseDouble((String) fromObject));
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											Double degrees =  Math.toDegrees((Double) fromObject);
											return degrees.toString();
										}

									}));
		
		/* Altitude Value. */
		IObservableValue<Double> observeAltitude = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyEarthEnvironmentPackage.Literals.EARTH_WORKSITE__GEOGRAPHICAL_COORDINATES, ApogyEarthEnvironmentPackage.Literals.GEOGRAPHIC_COORDINATES__ELEVATION)).observe(getEarthAtmosphereWorksite());
		IObservableValue<String> observeAltitudeLabelValueText = WidgetProperties.text(SWT.Modify).observe(txtAltitudevalue);

		bindingContext.bindValue(observeAltitudeLabelValueText,
								 observeAltitude, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{																						
											return ((Double) fromObject).toString();
										}
									}));
		
		
		return bindingContext;
	}
}
