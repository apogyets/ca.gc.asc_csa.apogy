/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;

import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.ApogyEarthAtmosphereEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.ApogyEarthAtmosphereEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksiteWizardPagesProvider;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthAtmosphereEnvironmentUIPackageImpl extends EPackageImpl implements ApogyEarthAtmosphereEnvironmentUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthAtmosphereWorksiteWizardPagesProviderEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.ApogyEarthAtmosphereEnvironmentUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyEarthAtmosphereEnvironmentUIPackageImpl() {
		super(eNS_URI, ApogyEarthAtmosphereEnvironmentUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyEarthAtmosphereEnvironmentUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyEarthAtmosphereEnvironmentUIPackage init() {
		if (isInited) return (ApogyEarthAtmosphereEnvironmentUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthAtmosphereEnvironmentUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyEarthAtmosphereEnvironmentUIPackageImpl theApogyEarthAtmosphereEnvironmentUIPackage = (ApogyEarthAtmosphereEnvironmentUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyEarthAtmosphereEnvironmentUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyEarthAtmosphereEnvironmentUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonEMFUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyEarthAtmosphereEnvironmentUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyEarthAtmosphereEnvironmentUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyEarthAtmosphereEnvironmentUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyEarthAtmosphereEnvironmentUIPackage.eNS_URI, theApogyEarthAtmosphereEnvironmentUIPackage);
		return theApogyEarthAtmosphereEnvironmentUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthAtmosphereWorksiteWizardPagesProvider() {
		return earthAtmosphereWorksiteWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthAtmosphereEnvironmentUIFactory getApogyEarthAtmosphereEnvironmentUIFactory() {
		return (ApogyEarthAtmosphereEnvironmentUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		earthAtmosphereWorksiteWizardPagesProviderEClass = createEClass(EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonEMFUIPackage theApogyCommonEMFUIPackage = (ApogyCommonEMFUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		earthAtmosphereWorksiteWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(earthAtmosphereWorksiteWizardPagesProviderEClass, EarthAtmosphereWorksiteWizardPagesProvider.class, "EarthAtmosphereWorksiteWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "documentation", "******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n * Contributors:\n    Pierre Allard - initial API and implementation\n\nSPDX-License-Identifier: EPL-1.0\n*****************************************************************************",
			 "prefix", "ApogyEarthAtmosphereEnvironmentUI",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyEarthAtmosphereEnvironmentUI",
			 "complianceLevel", "6.0",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.core.environment.earth.atmosphere"
		   });
	}

} //ApogyEarthAtmosphereEnvironmentUIPackageImpl
