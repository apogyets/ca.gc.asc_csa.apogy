/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.ApogyEarthAtmosphereEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.EarthAtmosphereWorksiteWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Atmosphere Worksite Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EarthAtmosphereWorksiteWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements EarthAtmosphereWorksiteWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthAtmosphereWorksiteWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthAtmosphereEnvironmentUIPackage.Literals.EARTH_ATMOSPHERE_WORKSITE_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		EarthAtmosphereWorksite worksite = ApogyEarthAtmosphereFacade.INSTANCE.createAndInitializeDefaultCSAEarthAtmosphereWorksite();
		
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			
			String name = (String) mapBasedEClassSettings.getUserDataMap().get("name");
			worksite.setName(name);
		}
		
		return worksite;
	}			
} //EarthAtmosphereWorksiteWizardPagesProviderImpl
