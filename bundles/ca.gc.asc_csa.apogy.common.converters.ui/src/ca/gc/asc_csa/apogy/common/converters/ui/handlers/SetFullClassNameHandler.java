package ca.gc.asc_csa.apogy.common.converters.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;

import ca.gc.asc_csa.apogy.common.converters.ui.composites.ConvertersUIConstants.ClassNameDisplayMode;
import ca.gc.asc_csa.apogy.common.converters.ui.part.ConvertersPart;

public class SetFullClassNameHandler 
{
	@CanExecute
	public boolean canExecute(MPart part)
	{
		return true;
	}
	
	@Execute
	public void execute(MPart part, final MToolItem item)
	{
		if (part.getObject() instanceof ConvertersPart)
		{
			ConvertersPart convertersPart = (ConvertersPart) part.getObject();
			if(item.isSelected())
			{
				convertersPart.setClassNameDisplayMode(ClassNameDisplayMode.FULLY_QUALIFIED_CLASS_NAME);
			}
			else
			{
				convertersPart.setClassNameDisplayMode(ClassNameDisplayMode.SIMPLE_CLASS_NAME);
			}
		}
	}
}
