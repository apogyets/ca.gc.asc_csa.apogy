package ca.gc.asc_csa.apogy.common.converters.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

public class ConvertersUIConstants 
{
	/**
	 * Defines the class name display modes.
	 * @author pallard
	 *
	 */
	public enum ClassNameDisplayMode 
	{
		FULLY_QUALIFIED_CLASS_NAME, SIMPLE_CLASS_NAME
	}
}
