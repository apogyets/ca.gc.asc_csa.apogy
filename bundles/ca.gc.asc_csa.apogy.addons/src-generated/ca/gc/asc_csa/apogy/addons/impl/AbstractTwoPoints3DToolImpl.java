package ca.gc.asc_csa.apogy.addons.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.lang.reflect.InvocationTargetException;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodePath;
import ca.gc.asc_csa.apogy.common.topology.util.NodeRelativePoseListener;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Two Points3 DTool</b></em>'.
 * <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getFromAbsolutePosition <em>From Absolute Position</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getFromRelativePosition <em>From Relative Position</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getFromNode <em>From Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getFromNodeNodePath <em>From Node Node Path</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#isFromNodeLock <em>From Node Lock</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getToAbsolutePosition <em>To Absolute Position</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getToRelativePosition <em>To Relative Position</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getToNode <em>To Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getToNodeNodePath <em>To Node Node Path</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#isToNodeLock <em>To Node Lock</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractTwoPoints3DToolImpl#getDistance <em>Distance</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractTwoPoints3DToolImpl extends Simple3DToolImpl implements AbstractTwoPoints3DTool 
{		
	private NodeRelativePoseListener nodeRelativePoseListener;
		
	/**
	 * The cached value of the '{@link #getFromAbsolutePosition() <em>From Absolute Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getFromAbsolutePosition()
	 * @generated
	 * @ordered
	 */
	protected Tuple3d fromAbsolutePosition;

	/**
	 * The cached value of the '{@link #getFromRelativePosition() <em>From Relative Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getFromRelativePosition()
	 * @generated
	 * @ordered
	 */
	protected Tuple3d fromRelativePosition;

	/**
	 * The cached value of the '{@link #getFromNode() <em>From Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getFromNode()
	 * @generated
	 * @ordered
	 */
	protected Node fromNode;

	/**
	 * The cached value of the '{@link #getFromNodeNodePath() <em>From Node Node Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getFromNodeNodePath()
	 * @generated
	 * @ordered
	 */
	protected NodePath fromNodeNodePath;

	/**
	 * The default value of the '{@link #isFromNodeLock() <em>From Node Lock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #isFromNodeLock()
	 * @generated
	 * @ordered
	 */
	protected static final boolean FROM_NODE_LOCK_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isFromNodeLock() <em>From Node Lock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #isFromNodeLock()
	 * @generated
	 * @ordered
	 */
	protected boolean fromNodeLock = FROM_NODE_LOCK_EDEFAULT;

	/**
	 * The cached value of the '{@link #getToAbsolutePosition() <em>To Absolute Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getToAbsolutePosition()
	 * @generated
	 * @ordered
	 */
	protected Tuple3d toAbsolutePosition;

	/**
	 * The cached value of the '{@link #getToRelativePosition() <em>To Relative Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getToRelativePosition()
	 * @generated
	 * @ordered
	 */
	protected Tuple3d toRelativePosition;

	/**
	 * The cached value of the '{@link #getToNode() <em>To Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getToNode()
	 * @generated
	 * @ordered
	 */
	protected Node toNode;

	/**
	 * The cached value of the '{@link #getToNodeNodePath() <em>To Node Node Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getToNodeNodePath()
	 * @generated
	 * @ordered
	 */
	protected NodePath toNodeNodePath;

	/**
	 * The default value of the '{@link #isToNodeLock() <em>To Node Lock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #isToNodeLock()
	 * @generated
	 * @ordered
	 */
	protected static final boolean TO_NODE_LOCK_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isToNodeLock() <em>To Node Lock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #isToNodeLock()
	 * @generated
	 * @ordered
	 */
	protected boolean toNodeLock = TO_NODE_LOCK_EDEFAULT;

	/**
	 * The default value of the '{@link #getDistance() <em>Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getDistance()
	 * @generated
	 * @ordered
	 */
	protected static final double DISTANCE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getDistance() <em>Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getDistance()
	 * @generated
	 * @ordered
	 */
	protected double distance = DISTANCE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected AbstractTwoPoints3DToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Tuple3d getFromAbsolutePosition() {
		return fromAbsolutePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NotificationChain basicSetFromAbsolutePosition(Tuple3d newFromAbsolutePosition, NotificationChain msgs) {
		Tuple3d oldFromAbsolutePosition = fromAbsolutePosition;
		fromAbsolutePosition = newFromAbsolutePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION, oldFromAbsolutePosition, newFromAbsolutePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setFromAbsolutePosition(Tuple3d newFromAbsolutePosition) {
		if (newFromAbsolutePosition != fromAbsolutePosition) {
			NotificationChain msgs = null;
			if (fromAbsolutePosition != null)
				msgs = ((InternalEObject)fromAbsolutePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION, null, msgs);
			if (newFromAbsolutePosition != null)
				msgs = ((InternalEObject)newFromAbsolutePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION, null, msgs);
			msgs = basicSetFromAbsolutePosition(newFromAbsolutePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION, newFromAbsolutePosition, newFromAbsolutePosition));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Tuple3d getFromRelativePosition() {
		return fromRelativePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NotificationChain basicSetFromRelativePosition(Tuple3d newFromRelativePosition, NotificationChain msgs) {
		Tuple3d oldFromRelativePosition = fromRelativePosition;
		fromRelativePosition = newFromRelativePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION, oldFromRelativePosition, newFromRelativePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setFromRelativePosition(Tuple3d newFromRelativePosition)
	{
		setFromRelativePositionGen(newFromRelativePosition);
		updateFromAbsolutePosition();
		updateDistance();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setFromRelativePositionGen(Tuple3d newFromRelativePosition) {
		if (newFromRelativePosition != fromRelativePosition) {
			NotificationChain msgs = null;
			if (fromRelativePosition != null)
				msgs = ((InternalEObject)fromRelativePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION, null, msgs);
			if (newFromRelativePosition != null)
				msgs = ((InternalEObject)newFromRelativePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION, null, msgs);
			msgs = basicSetFromRelativePosition(newFromRelativePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION, newFromRelativePosition, newFromRelativePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Node getFromNode() {
		if (fromNode != null && fromNode.eIsProxy()) {
			InternalEObject oldFromNode = (InternalEObject)fromNode;
			fromNode = (Node)eResolveProxy(oldFromNode);
			if (fromNode != oldFromNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE, oldFromNode, fromNode));
			}
		}
		return fromNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Node basicGetFromNode() {
		return fromNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setFromNode(Node newFromNode) 
	{		
		setFromNodeGen(newFromNode);
		updateFromAbsolutePosition();
		updateDistance();
		getNodeRelativePoseListener().setFromNode(newFromNode);		
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setFromNodeGen(Node newFromNode) {
		Node oldFromNode = fromNode;
		fromNode = newFromNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE, oldFromNode, fromNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NodePath getFromNodeNodePath() {
		return fromNodeNodePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NotificationChain basicSetFromNodeNodePath(NodePath newFromNodeNodePath, NotificationChain msgs) {
		NodePath oldFromNodeNodePath = fromNodeNodePath;
		fromNodeNodePath = newFromNodeNodePath;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH, oldFromNodeNodePath, newFromNodeNodePath);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setFromNodeNodePath(NodePath newFromNodeNodePath) {
		if (newFromNodeNodePath != fromNodeNodePath) {
			NotificationChain msgs = null;
			if (fromNodeNodePath != null)
				msgs = ((InternalEObject)fromNodeNodePath).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH, null, msgs);
			if (newFromNodeNodePath != null)
				msgs = ((InternalEObject)newFromNodeNodePath).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH, null, msgs);
			msgs = basicSetFromNodeNodePath(newFromNodeNodePath, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH, newFromNodeNodePath, newFromNodeNodePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public boolean isFromNodeLock() {
		return fromNodeLock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setFromNodeLock(boolean newFromNodeLock) {
		boolean oldFromNodeLock = fromNodeLock;
		fromNodeLock = newFromNodeLock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK, oldFromNodeLock, fromNodeLock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Tuple3d getToAbsolutePosition() {
		return toAbsolutePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NotificationChain basicSetToAbsolutePosition(Tuple3d newToAbsolutePosition, NotificationChain msgs) {
		Tuple3d oldToAbsolutePosition = toAbsolutePosition;
		toAbsolutePosition = newToAbsolutePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION, oldToAbsolutePosition, newToAbsolutePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setToAbsolutePosition(Tuple3d newToAbsolutePosition) {
		if (newToAbsolutePosition != toAbsolutePosition) {
			NotificationChain msgs = null;
			if (toAbsolutePosition != null)
				msgs = ((InternalEObject)toAbsolutePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION, null, msgs);
			if (newToAbsolutePosition != null)
				msgs = ((InternalEObject)newToAbsolutePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION, null, msgs);
			msgs = basicSetToAbsolutePosition(newToAbsolutePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION, newToAbsolutePosition, newToAbsolutePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Tuple3d getToRelativePosition() {
		return toRelativePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NotificationChain basicSetToRelativePosition(Tuple3d newToRelativePosition, NotificationChain msgs) {
		Tuple3d oldToRelativePosition = toRelativePosition;
		toRelativePosition = newToRelativePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION, oldToRelativePosition, newToRelativePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setToRelativePosition(Tuple3d newToRelativePosition) 
	{
		setToRelativePositionGen(newToRelativePosition);
		updateToAbsolutePosition();
		updateDistance();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setToRelativePositionGen(Tuple3d newToRelativePosition) {
		if (newToRelativePosition != toRelativePosition) {
			NotificationChain msgs = null;
			if (toRelativePosition != null)
				msgs = ((InternalEObject)toRelativePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION, null, msgs);
			if (newToRelativePosition != null)
				msgs = ((InternalEObject)newToRelativePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION, null, msgs);
			msgs = basicSetToRelativePosition(newToRelativePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION, newToRelativePosition, newToRelativePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Node getToNode() {
		if (toNode != null && toNode.eIsProxy()) {
			InternalEObject oldToNode = (InternalEObject)toNode;
			toNode = (Node)eResolveProxy(oldToNode);
			if (toNode != oldToNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, oldToNode, toNode));
			}
		}
		return toNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Node basicGetToNode() {
		return toNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setToNode(Node newToNode) 
	{
		setToNodeGen(newToNode);
		updateToAbsolutePosition();
		updateDistance();
		getNodeRelativePoseListener().setToNode(newToNode);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setToNodeGen(Node newToNode) {
		Node oldToNode = toNode;
		toNode = newToNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, oldToNode, toNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NodePath getToNodeNodePath() {
		return toNodeNodePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NotificationChain basicSetToNodeNodePath(NodePath newToNodeNodePath, NotificationChain msgs) {
		NodePath oldToNodeNodePath = toNodeNodePath;
		toNodeNodePath = newToNodeNodePath;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH, oldToNodeNodePath, newToNodeNodePath);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setToNodeNodePath(NodePath newToNodeNodePath) 
	{
		if (newToNodeNodePath != toNodeNodePath) {
			NotificationChain msgs = null;
			if (toNodeNodePath != null)
				msgs = ((InternalEObject)toNodeNodePath).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH, null, msgs);
			if (newToNodeNodePath != null)
				msgs = ((InternalEObject)newToNodeNodePath).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH, null, msgs);
			msgs = basicSetToNodeNodePath(newToNodeNodePath, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH, newToNodeNodePath, newToNodeNodePath));
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public boolean isToNodeLock() {
		return toNodeLock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setToNodeLock(boolean newToNodeLock) {
		boolean oldToNodeLock = toNodeLock;
		toNodeLock = newToNodeLock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK, oldToNodeLock, toNodeLock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public double getDistance() {
		return distance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setDistance(double newDistance) {
		double oldDistance = distance;
		distance = newDistance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE, oldDistance, distance));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public abstract void pointsRelativePoseChanged(Matrix4d newPose);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION:
				return basicSetFromAbsolutePosition(null, msgs);
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION:
				return basicSetFromRelativePosition(null, msgs);
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH:
				return basicSetFromNodeNodePath(null, msgs);
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION:
				return basicSetToAbsolutePosition(null, msgs);
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION:
				return basicSetToRelativePosition(null, msgs);
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH:
				return basicSetToNodeNodePath(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	@Override
	public void dispose() 
	{
		if(nodeRelativePoseListener != null)
		{
			nodeRelativePoseListener.dispose();			
			nodeRelativePoseListener = null;
		}
		
		super.dispose();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION:
				return getFromAbsolutePosition();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION:
				return getFromRelativePosition();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE:
				if (resolve) return getFromNode();
				return basicGetFromNode();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH:
				return getFromNodeNodePath();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK:
				return isFromNodeLock();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION:
				return getToAbsolutePosition();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION:
				return getToRelativePosition();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE:
				if (resolve) return getToNode();
				return basicGetToNode();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH:
				return getToNodeNodePath();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK:
				return isToNodeLock();
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE:
				return getDistance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION:
				setFromAbsolutePosition((Tuple3d)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION:
				setFromRelativePosition((Tuple3d)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE:
				setFromNode((Node)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH:
				setFromNodeNodePath((NodePath)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK:
				setFromNodeLock((Boolean)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION:
				setToAbsolutePosition((Tuple3d)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION:
				setToRelativePosition((Tuple3d)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE:
				setToNode((Node)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH:
				setToNodeNodePath((NodePath)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK:
				setToNodeLock((Boolean)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE:
				setDistance((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION:
				setFromAbsolutePosition((Tuple3d)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION:
				setFromRelativePosition((Tuple3d)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE:
				setFromNode((Node)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH:
				setFromNodeNodePath((NodePath)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK:
				setFromNodeLock(FROM_NODE_LOCK_EDEFAULT);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION:
				setToAbsolutePosition((Tuple3d)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION:
				setToRelativePosition((Tuple3d)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE:
				setToNode((Node)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH:
				setToNodeNodePath((NodePath)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK:
				setToNodeLock(TO_NODE_LOCK_EDEFAULT);
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE:
				setDistance(DISTANCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION:
				return fromAbsolutePosition != null;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION:
				return fromRelativePosition != null;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE:
				return fromNode != null;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH:
				return fromNodeNodePath != null;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK:
				return fromNodeLock != FROM_NODE_LOCK_EDEFAULT;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION:
				return toAbsolutePosition != null;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION:
				return toRelativePosition != null;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE:
				return toNode != null;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH:
				return toNodeNodePath != null;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK:
				return toNodeLock != TO_NODE_LOCK_EDEFAULT;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE:
				return distance != DISTANCE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL___POINTS_RELATIVE_POSE_CHANGED__MATRIX4D:
				pointsRelativePoseChanged((Matrix4d)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fromNodeLock: ");
		result.append(fromNodeLock);
		result.append(", toNodeLock: ");
		result.append(toNodeLock);
		result.append(", distance: ");
		result.append(distance);
		result.append(')');
		return result.toString();
	}

	@Override
	public void setRootNode(Node newRootNode) 
	{	
		super.setRootNode(newRootNode);
		
		if(getToNodeNodePath() != null)
		{
			if(!isToNodeLock())
			{
				Node to = ApogyCommonTopologyFacade.INSTANCE.resolveNode(getRootNode(), getToNodeNodePath());						
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, to);
			}
		}
		
		if(getFromNodeNodePath() != null)
		{
			if(!isFromNodeLock())
			{
				Node from = ApogyCommonTopologyFacade.INSTANCE.resolveNode(getRootNode(), getFromNodeNodePath());
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE, from);
			}
		}
	}
	
	@Override
	public void variablesInstantiated() 
	{
		if(getToNodeNodePath() != null)
		{
			if(!isToNodeLock())
			{
				Node to = ApogyCommonTopologyFacade.INSTANCE.resolveNode(getRootNode(), getToNodeNodePath());
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, to);
			}
		}
		
		if(getFromNodeNodePath() != null)
		{
			if(!isFromNodeLock())
			{
				Node from = ApogyCommonTopologyFacade.INSTANCE.resolveNode(getRootNode(), getFromNodeNodePath());
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE, from);
			}
		}
	}
	
	@Override
	public void variablesCleared() 
	{
		// Nothing to do.
	}
	
	/*
	 * Updates the absolute position of the FROM node.
	 */
	protected void updateFromAbsolutePosition()
	{
		// Updates From Node Absolute Position.
		if(getFromNode() != null)
		{
			Matrix4d m = ApogyCommonTopologyFacade.INSTANCE.expressNodeInRootFrame(getFromNode());
			Point3d from = new Point3d();
			if(getFromRelativePosition() != null)
			{
				from = new Point3d(getFromRelativePosition().asTuple3d());
			}
			m.transform(from);
			
			// Updates the FromAbsolutePosition.
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION, ApogyCommonMathFacade.INSTANCE.createTuple3d(from));			
		}
	}
	
	/*
	 * Updates the absolute position of the TO node.
	 */
	protected void updateToAbsolutePosition()
	{
		// Updates To Node Absolute Position.
		if(getToNode() != null)
		{
			Matrix4d m = ApogyCommonTopologyFacade.INSTANCE.expressNodeInRootFrame(getToNode());
			Point3d to = new Point3d();
			if(getToRelativePosition() != null)
			{
				to = new Point3d(getToRelativePosition().asTuple3d());
			}
			m.transform(to);
			
			// Updates the ToAbsolutePosition.
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION, ApogyCommonMathFacade.INSTANCE.createTuple3d(to));				
		}
	}
	
	protected void updateDistance()
	{
		double distance = 0.0;
		if(getFromAbsolutePosition() != null && getToAbsolutePosition() != null)
		{
			Point3d from = new Point3d(getFromAbsolutePosition().asTuple3d());
			Point3d to = new Point3d(getToAbsolutePosition().asTuple3d());
			distance = to.distance(from);			
		}		
		
		// Updates the distance.
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE, distance);								
	}
	
	private NodeRelativePoseListener getNodeRelativePoseListener() 
	{
		if(nodeRelativePoseListener == null)
		{
			nodeRelativePoseListener = new NodeRelativePoseListener()
			{
				@Override
				public void relativePoseChanged(Matrix4d newPose) 
				{
					if(!isDisposed())
					{
						// Updates both node absolute position.
						updateFromAbsolutePosition();
						updateToAbsolutePosition();
						
						// Updates distance
						updateDistance();
						
						pointsRelativePoseChanged(newPose);
					}
				}
			};
		}
		return nodeRelativePoseListener;
	}
} //AbstractTwoPoints3DToolImpl
