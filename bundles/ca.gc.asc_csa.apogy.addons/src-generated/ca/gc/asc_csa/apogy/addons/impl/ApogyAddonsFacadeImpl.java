/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.addons.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsFacade;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsFactory;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.SimpleTool;
import ca.gc.asc_csa.apogy.addons.SimpleToolList;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.FeatureOfInterestList;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.FeaturesOfInterestMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.SurfaceWorksite;
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ApogyAddonsFacadeImpl extends MinimalEObjectImpl.Container implements ApogyAddonsFacade 
{
	private static ApogyAddonsFacade instance = null;

	public static ApogyAddonsFacade getInstance() {
		if (instance == null) {
			instance = new ApogyAddonsFacadeImpl();
		}
		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogyAddonsFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsPackage.Literals.APOGY_ADDONS_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void deleteTool(SimpleTool simpleTool) 
	{				
		// Calls the dispose method.
		simpleTool.dispose();
		
		// Removes the tool from the list.
		ApogyCommonTransactionFacade.INSTANCE.basicRemove(simpleTool.getToolList(), ApogyAddonsPackage.Literals.SIMPLE_TOOL_LIST__SIMPLE_TOOLS, simpleTool);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public Collection<FeatureOfInterestList> getAllFeatureOfInterestLists(InvocatorSession invocatorSession) 
	{
		List<FeatureOfInterestList> list = new ArrayList<FeatureOfInterestList>();
		if(invocatorSession != null)
		{
			if(invocatorSession.getEnvironment() instanceof ApogyEnvironment)
			{
				ApogyEnvironment apogyEnvironment = (ApogyEnvironment) invocatorSession.getEnvironment();
				if(apogyEnvironment.getActiveWorksite() instanceof SurfaceWorksite)
				{
					SurfaceWorksite surfaceWorksite = (SurfaceWorksite) apogyEnvironment.getActiveWorksite();
					if(surfaceWorksite.getMapsList() != null)
					{
						for(Map map :surfaceWorksite.getMapsList().getMaps())
						{
							for(AbstractMapLayer layer : map.getLayers())
							{
								if(layer instanceof FeaturesOfInterestMapLayer)
								{
									FeaturesOfInterestMapLayer featuresOfInterestMapLayer = (FeaturesOfInterestMapLayer) layer;
									if(!list.contains(featuresOfInterestMapLayer.getFeatures()))
									{
										list.add(featuresOfInterestMapLayer.getFeatures());
									}
								}
							}
						}
					}
				}
			}
		}
		
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public SimpleToolList getSimpleToolList() 
	{
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		if(invocatorSession != null && invocatorSession.getToolsList() != null)
		{
			for(AbstractToolsListContainer atlc : invocatorSession.getToolsList().getToolsListContainers())
			{
				if(atlc instanceof SimpleToolList)
				{
					return (SimpleToolList) atlc;
				}
			}
		}
		
		SimpleToolList simpleToolList = ApogyAddonsFactory.eINSTANCE.createSimpleToolList();
		ApogyCommonTransactionFacade.INSTANCE.basicAdd(invocatorSession.getToolsList(), ApogyCoreInvocatorPackage.Literals.TOOLS_LIST__TOOLS_LIST_CONTAINERS, simpleToolList);
		
		return simpleToolList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyAddonsPackage.APOGY_ADDONS_FACADE___DELETE_TOOL__SIMPLETOOL:
				deleteTool((SimpleTool)arguments.get(0));
				return null;
			case ApogyAddonsPackage.APOGY_ADDONS_FACADE___GET_ALL_FEATURE_OF_INTEREST_LISTS__INVOCATORSESSION:
				return getAllFeatureOfInterestLists((InvocatorSession)arguments.get(0));
			case ApogyAddonsPackage.APOGY_ADDONS_FACADE___GET_SIMPLE_TOOL_LIST:
				return getSimpleToolList();
		}
		return super.eInvoke(operationID, arguments);
	}

} //ApogyAddonsFacadeImpl
