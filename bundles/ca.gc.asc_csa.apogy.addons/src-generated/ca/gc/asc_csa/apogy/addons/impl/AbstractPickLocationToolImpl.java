/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.addons.impl;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsFactory;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.AbstractPickLocationToolNode;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodePath;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Geometry Placement Tool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractPickLocationToolImpl#getRelativeIntersectionNormal <em>Relative Intersection Normal</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractPickLocationToolImpl#getSelectedRelativePosition <em>Selected Relative Position</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractPickLocationToolImpl#getSelectedNode <em>Selected Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractPickLocationToolImpl#getSelectedNodeNodePath <em>Selected Node Node Path</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.impl.AbstractPickLocationToolImpl#getAbstractPickLocationToolNode <em>Abstract Pick Location Tool Node</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AbstractPickLocationToolImpl extends Simple3DToolImpl implements AbstractPickLocationTool 
{
	/**
	 * The cached value of the '{@link #getRelativeIntersectionNormal() <em>Relative Intersection Normal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelativeIntersectionNormal()
	 * @generated
	 * @ordered
	 */
	protected Tuple3d relativeIntersectionNormal;

	/**
	 * The cached value of the '{@link #getSelectedRelativePosition() <em>Selected Relative Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectedRelativePosition()
	 * @generated
	 * @ordered
	 */
	protected Tuple3d selectedRelativePosition;

	/**
	 * The cached value of the '{@link #getSelectedNode() <em>Selected Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectedNode()
	 * @generated
	 * @ordered
	 */
	protected Node selectedNode;

	/**
	 * The cached value of the '{@link #getSelectedNodeNodePath() <em>Selected Node Node Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectedNodeNodePath()
	 * @generated
	 * @ordered
	 */
	protected NodePath selectedNodeNodePath;

	/**
	 * The cached value of the '{@link #getAbstractPickLocationToolNode() <em>Abstract Pick Location Tool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractPickLocationToolNode()
	 * @generated
	 * @ordered
	 */
	protected AbstractPickLocationToolNode abstractPickLocationToolNode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractPickLocationToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tuple3d getRelativeIntersectionNormal() {
		return relativeIntersectionNormal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRelativeIntersectionNormal(Tuple3d newRelativeIntersectionNormal, NotificationChain msgs) {
		Tuple3d oldRelativeIntersectionNormal = relativeIntersectionNormal;
		relativeIntersectionNormal = newRelativeIntersectionNormal;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL, oldRelativeIntersectionNormal, newRelativeIntersectionNormal);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelativeIntersectionNormal(Tuple3d newRelativeIntersectionNormal) {
		if (newRelativeIntersectionNormal != relativeIntersectionNormal) {
			NotificationChain msgs = null;
			if (relativeIntersectionNormal != null)
				msgs = ((InternalEObject)relativeIntersectionNormal).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL, null, msgs);
			if (newRelativeIntersectionNormal != null)
				msgs = ((InternalEObject)newRelativeIntersectionNormal).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL, null, msgs);
			msgs = basicSetRelativeIntersectionNormal(newRelativeIntersectionNormal, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL, newRelativeIntersectionNormal, newRelativeIntersectionNormal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Tuple3d getSelectedRelativePosition() {
		return selectedRelativePosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectedRelativePosition(Tuple3d newSelectedRelativePosition, NotificationChain msgs) {
		Tuple3d oldSelectedRelativePosition = selectedRelativePosition;
		selectedRelativePosition = newSelectedRelativePosition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION, oldSelectedRelativePosition, newSelectedRelativePosition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectedRelativePosition(Tuple3d newSelectedRelativePosition) {
		if (newSelectedRelativePosition != selectedRelativePosition) {
			NotificationChain msgs = null;
			if (selectedRelativePosition != null)
				msgs = ((InternalEObject)selectedRelativePosition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION, null, msgs);
			if (newSelectedRelativePosition != null)
				msgs = ((InternalEObject)newSelectedRelativePosition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION, null, msgs);
			msgs = basicSetSelectedRelativePosition(newSelectedRelativePosition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION, newSelectedRelativePosition, newSelectedRelativePosition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getSelectedNode() {
		if (selectedNode != null && selectedNode.eIsProxy()) {
			InternalEObject oldSelectedNode = (InternalEObject)selectedNode;
			selectedNode = (Node)eResolveProxy(oldSelectedNode);
			if (selectedNode != oldSelectedNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE, oldSelectedNode, selectedNode));
			}
		}
		return selectedNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetSelectedNode() {
		return selectedNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectedNode(Node newSelectedNode) {
		Node oldSelectedNode = selectedNode;
		selectedNode = newSelectedNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE, oldSelectedNode, selectedNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodePath getSelectedNodeNodePath() {
		return selectedNodeNodePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSelectedNodeNodePath(NodePath newSelectedNodeNodePath, NotificationChain msgs) {
		NodePath oldSelectedNodeNodePath = selectedNodeNodePath;
		selectedNodeNodePath = newSelectedNodeNodePath;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH, oldSelectedNodeNodePath, newSelectedNodeNodePath);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectedNodeNodePath(NodePath newSelectedNodeNodePath) {
		if (newSelectedNodeNodePath != selectedNodeNodePath) {
			NotificationChain msgs = null;
			if (selectedNodeNodePath != null)
				msgs = ((InternalEObject)selectedNodeNodePath).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH, null, msgs);
			if (newSelectedNodeNodePath != null)
				msgs = ((InternalEObject)newSelectedNodeNodePath).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH, null, msgs);
			msgs = basicSetSelectedNodeNodePath(newSelectedNodeNodePath, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH, newSelectedNodeNodePath, newSelectedNodeNodePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractPickLocationToolNode getAbstractPickLocationToolNode() {
		if (abstractPickLocationToolNode != null && abstractPickLocationToolNode.eIsProxy()) {
			InternalEObject oldAbstractPickLocationToolNode = (InternalEObject)abstractPickLocationToolNode;
			abstractPickLocationToolNode = (AbstractPickLocationToolNode)eResolveProxy(oldAbstractPickLocationToolNode);
			if (abstractPickLocationToolNode != oldAbstractPickLocationToolNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE, oldAbstractPickLocationToolNode, abstractPickLocationToolNode));
			}
		}
		return abstractPickLocationToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractPickLocationToolNode basicGetAbstractPickLocationToolNode() {
		return abstractPickLocationToolNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractPickLocationToolNode(AbstractPickLocationToolNode newAbstractPickLocationToolNode, NotificationChain msgs) {
		AbstractPickLocationToolNode oldAbstractPickLocationToolNode = abstractPickLocationToolNode;
		abstractPickLocationToolNode = newAbstractPickLocationToolNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE, oldAbstractPickLocationToolNode, newAbstractPickLocationToolNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractPickLocationToolNode(AbstractPickLocationToolNode newAbstractPickLocationToolNode) {
		if (newAbstractPickLocationToolNode != abstractPickLocationToolNode) {
			NotificationChain msgs = null;
			if (abstractPickLocationToolNode != null)
				msgs = ((InternalEObject)abstractPickLocationToolNode).eInverseRemove(this, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE, AbstractPickLocationToolNode.class, msgs);
			if (newAbstractPickLocationToolNode != null)
				msgs = ((InternalEObject)newAbstractPickLocationToolNode).eInverseAdd(this, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE, AbstractPickLocationToolNode.class, msgs);
			msgs = basicSetAbstractPickLocationToolNode(newAbstractPickLocationToolNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE, newAbstractPickLocationToolNode, newAbstractPickLocationToolNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				if (abstractPickLocationToolNode != null)
					msgs = ((InternalEObject)abstractPickLocationToolNode).eInverseRemove(this, ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL_NODE__ABSTRACT_PICK_LOCATION_TOOL_NODE, AbstractPickLocationToolNode.class, msgs);
				return basicSetAbstractPickLocationToolNode((AbstractPickLocationToolNode)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL:
				return basicSetRelativeIntersectionNormal(null, msgs);
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION:
				return basicSetSelectedRelativePosition(null, msgs);
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH:
				return basicSetSelectedNodeNodePath(null, msgs);
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				return basicSetAbstractPickLocationToolNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL:
				return getRelativeIntersectionNormal();
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION:
				return getSelectedRelativePosition();
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE:
				if (resolve) return getSelectedNode();
				return basicGetSelectedNode();
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH:
				return getSelectedNodeNodePath();
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				if (resolve) return getAbstractPickLocationToolNode();
				return basicGetAbstractPickLocationToolNode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL:
				setRelativeIntersectionNormal((Tuple3d)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION:
				setSelectedRelativePosition((Tuple3d)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE:
				setSelectedNode((Node)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH:
				setSelectedNodeNodePath((NodePath)newValue);
				return;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				setAbstractPickLocationToolNode((AbstractPickLocationToolNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL:
				setRelativeIntersectionNormal((Tuple3d)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION:
				setSelectedRelativePosition((Tuple3d)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE:
				setSelectedNode((Node)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH:
				setSelectedNodeNodePath((NodePath)null);
				return;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				setAbstractPickLocationToolNode((AbstractPickLocationToolNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL:
				return relativeIntersectionNormal != null;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION:
				return selectedRelativePosition != null;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE:
				return selectedNode != null;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH:
				return selectedNodeNodePath != null;
			case ApogyAddonsPackage.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE:
				return abstractPickLocationToolNode != null;
		}
		return super.eIsSet(featureID);
	}

	@Override
	public void initialise() 
	{	
		super.initialise();
		
		// First, initialize the GeometryPlacementToolNode.
		AbstractPickLocationToolNode toolNode = ApogyAddonsFactory.eINSTANCE.createAbstractPickLocationToolNode();		
		{
			toolNode.setDescription("Node associated with the Abstract Pick Location Tool named <" + getName() + ">");
			toolNode.setNodeId("ABSTRACT_PICK_LOCATION_TOOL_" +  getName().replaceAll(" ", "_"));
		}
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE, toolNode);	
					
		// Attaches the tool node to the new to Node.
		attachPickLocationToolNode();			
	}
	
	@Override
	public void setVisible(boolean newVisible) 
	{		
		super.setVisible(newVisible);
		setPickLocationToolNodeVisibility(newVisible);
	}
	
	@Override
	public void setRootNode(Node newRootNode) 
	{	
		super.setRootNode(newRootNode);
		
		initializeNodeAndPosition();		
		
		// Update tool node visibility,
		setPickLocationToolNodeVisibility(isVisible());
	}
	
	@Override
	public void variablesInstantiated() 
	{		
		super.variablesInstantiated();

		initializeNodeAndPosition();		
	}
	
	@Override
	public void selectionChanged(NodeSelection nodeSelection) 
	{
		if(!isDisposed())
		{
			Node node = nodeSelection.getSelectedNode();
		
			// Updates selected node.			
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE, node);							
								
			// Updates TO Node Path				
			Node root = ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
			NodePath nodePath = ApogyCommonTopologyFacade.INSTANCE.createNodePath(root, node);												
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE_NODE_PATH, nodePath);			
			
			// Updates relative position.
			Tuple3d relativePosition = null;
			if(nodeSelection.getRelativeIntersectionPoint() != null)
			{
				relativePosition = ApogyCommonMathFacade.INSTANCE.createTuple3d(nodeSelection.getRelativeIntersectionPoint());
			}									
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_RELATIVE_POSITION, relativePosition);							

			// Updates relative normal.
			Tuple3d relativeNormal = null;
			if(nodeSelection.getRelativeIntersectionNormal() != null)
			{
				relativeNormal = ApogyCommonMathFacade.INSTANCE.createTuple3d(nodeSelection.getRelativeIntersectionNormal().x,nodeSelection.getRelativeIntersectionNormal().y, nodeSelection.getRelativeIntersectionNormal().z);
			}									
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL__RELATIVE_INTERSECTION_NORMAL, relativeNormal);											
			
			// Detach tool node from previous selection.
			detachPickLocationToolNode();
			
			// Updates the tool node transformation.
			if(getAbstractPickLocationToolNode() != null)
			{
				Matrix4d matrix = new Matrix4d();
				matrix.setIdentity();
				matrix.setTranslation(new Vector3d(relativePosition.asTuple3d()));
				
				// TODO : Update the orientation ?
				
				getAbstractPickLocationToolNode().setTransformation(matrix);
			}
			
			// Attaches the tool node to the new selected node.
			attachPickLocationToolNode();
		}
	}
	
	@Override
	public void dispose() 
	{
		super.dispose();		
		
		if(abstractPickLocationToolNode != null)
		{
			if(abstractPickLocationToolNode.getParent() instanceof GroupNode)
			{				
				GroupNode parent = (GroupNode) abstractPickLocationToolNode.getParent();										
				parent.getChildren().remove(abstractPickLocationToolNode);
				abstractPickLocationToolNode.setParent(null);
			}				
		}				
		
		// Clears the tool node.
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL__ABSTRACT_PICK_LOCATION_TOOL_NODE, null);				
	}
	
	protected void initializeNodeAndPosition()
	{
		// Resolve selected node from Node Path.
		if(getSelectedNodeNodePath() != null)
		{
			Node node = ApogyCommonTopologyFacade.INSTANCE.resolveNode(getRootNode(), getSelectedNodeNodePath());						
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsPackage.Literals.ABSTRACT_PICK_LOCATION_TOOL__SELECTED_NODE, node);
		}
				
		// Attaches the tool node to the new to Node.
		attachPickLocationToolNode();	
	}
			
	protected void setPickLocationToolNodeVisibility(boolean visible)
	{
		if(getAbstractPickLocationToolNode() != null)
		{
			NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(getAbstractPickLocationToolNode());
			if(nodePresentation != null)
			{
				// Toggle visibility flag.
				nodePresentation.setVisible(visible);
			}
		}
	}
	
	protected void attachPickLocationToolNode()
	{
		if(getAbstractPickLocationToolNode() != null)
		{
			// First, detach node from previous node.
			detachPickLocationToolNode();
			
			if(getSelectedNode() != null)
			{
				if(getSelectedNode() instanceof GroupNode)
				{
					GroupNode groupNode = (GroupNode) getSelectedNode();
					groupNode.getChildren().add(getAbstractPickLocationToolNode());
					
					if(getSelectedRelativePosition() != null)
					{
						getAbstractPickLocationToolNode().setPosition(EcoreUtil.copy(getSelectedRelativePosition()));
					}
				}
				else if (getSelectedNode().getParent() instanceof GroupNode)				
				{
					GroupNode groupNode = (GroupNode) getSelectedNode().getParent();
					groupNode.getChildren().add(getAbstractPickLocationToolNode());
					
					if(getSelectedRelativePosition() != null)
					{
						getAbstractPickLocationToolNode().setPosition(EcoreUtil.copy(getSelectedRelativePosition()));
					}
				}
			}		
		}
	}
	
	protected void detachPickLocationToolNode()
	{
		if(getAbstractPickLocationToolNode() != null && getAbstractPickLocationToolNode().getParent() instanceof GroupNode)
		{
			GroupNode parent = (GroupNode) getAbstractPickLocationToolNode().getParent();
			parent.getChildren().remove(getAbstractPickLocationToolNode());			
		}
	}
} //AbstractGeometryPlacementToolImpl
