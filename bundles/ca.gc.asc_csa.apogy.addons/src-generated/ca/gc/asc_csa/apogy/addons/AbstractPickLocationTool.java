/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.addons;

import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodePath;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Geometry Placement Tool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A tool that allows a user to pick a location.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getRelativeIntersectionNormal <em>Relative Intersection Normal</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getSelectedRelativePosition <em>Selected Relative Position</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getSelectedNode <em>Selected Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getSelectedNodeNodePath <em>Selected Node Node Path</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getAbstractPickLocationToolNode <em>Abstract Pick Location Tool Node</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getAbstractPickLocationTool()
 * @model
 * @generated
 */
public interface AbstractPickLocationTool extends Simple3DTool {
	/**
	 * Returns the value of the '<em><b>Relative Intersection Normal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relative Intersection Normal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relative Intersection Normal</em>' containment reference.
	 * @see #setRelativeIntersectionNormal(Tuple3d)
	 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getAbstractPickLocationTool_RelativeIntersectionNormal()
	 * @model containment="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' children='false' property='None'"
	 * @generated
	 */
	Tuple3d getRelativeIntersectionNormal();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getRelativeIntersectionNormal <em>Relative Intersection Normal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relative Intersection Normal</em>' containment reference.
	 * @see #getRelativeIntersectionNormal()
	 * @generated
	 */
	void setRelativeIntersectionNormal(Tuple3d value);

	/**
	 * Returns the value of the '<em><b>Selected Relative Position</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Relative Position</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Relative Position</em>' containment reference.
	 * @see #setSelectedRelativePosition(Tuple3d)
	 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getAbstractPickLocationTool_SelectedRelativePosition()
	 * @model containment="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' children='false' property='None'"
	 * @generated
	 */
	Tuple3d getSelectedRelativePosition();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getSelectedRelativePosition <em>Selected Relative Position</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected Relative Position</em>' containment reference.
	 * @see #getSelectedRelativePosition()
	 * @generated
	 */
	void setSelectedRelativePosition(Tuple3d value);

	/**
	 * Returns the value of the '<em><b>Selected Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Node</em>' reference.
	 * @see #setSelectedNode(Node)
	 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getAbstractPickLocationTool_SelectedNode()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' children='false' property='Readonly'"
	 * @generated
	 */
	Node getSelectedNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getSelectedNode <em>Selected Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected Node</em>' reference.
	 * @see #getSelectedNode()
	 * @generated
	 */
	void setSelectedNode(Node value);

	/**
	 * Returns the value of the '<em><b>Selected Node Node Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selected Node Node Path</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selected Node Node Path</em>' containment reference.
	 * @see #setSelectedNodeNodePath(NodePath)
	 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getAbstractPickLocationTool_SelectedNodeNodePath()
	 * @model containment="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='false' children='false' property='None'"
	 * @generated
	 */
	NodePath getSelectedNodeNodePath();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getSelectedNodeNodePath <em>Selected Node Node Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selected Node Node Path</em>' containment reference.
	 * @see #getSelectedNodeNodePath()
	 * @generated
	 */
	void setSelectedNodeNodePath(NodePath value);

	/**
	 * Returns the value of the '<em><b>Abstract Pick Location Tool Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationToolNode#getAbstractPickLocationToolNode <em>Abstract Pick Location Tool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  Topology Node associated with the tool.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Abstract Pick Location Tool Node</em>' reference.
	 * @see #setAbstractPickLocationToolNode(AbstractPickLocationToolNode)
	 * @see ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage#getAbstractPickLocationTool_AbstractPickLocationToolNode()
	 * @see ca.gc.asc_csa.apogy.addons.AbstractPickLocationToolNode#getAbstractPickLocationToolNode
	 * @model opposite="abstractPickLocationToolNode" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' children='true' property='None'"
	 * @generated
	 */
	AbstractPickLocationToolNode getAbstractPickLocationToolNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.AbstractPickLocationTool#getAbstractPickLocationToolNode <em>Abstract Pick Location Tool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Pick Location Tool Node</em>' reference.
	 * @see #getAbstractPickLocationToolNode()
	 * @generated
	 */
	void setAbstractPickLocationToolNode(AbstractPickLocationToolNode value);

} // AbstractGeometryPlacementTool
