/**
 * Canadian Space Agency / Agence spatiale canadienne 2016 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.sourcelookup.AbstractSourceLookupDirector;
import org.eclipse.debug.core.sourcelookup.ISourceContainer;
import org.eclipse.debug.core.sourcelookup.containers.ProjectSourceContainer;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.wst.jsdt.debug.internal.core.Constants;
import org.eclipse.wst.jsdt.debug.internal.core.launching.ILaunchConstants;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.ProgramRuntimeState;
import ca.gc.asc_csa.apogy.core.invocator.impl.AbstractProgramRuntimeImpl;
import ca.gc.asc_csa.apogy.core.programs.javascript.Activator;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram;
import ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgramRuntime;
import ca.gc.asc_csa.apogy.core.programs.javascript.RhinoDebuggerFrontend;
import ca.gc.asc_csa.apogy.core.programs.javascript.ScriptExecutor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Script Program Runtime</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JavaScriptProgramRuntimeImpl extends AbstractProgramRuntimeImpl implements JavaScriptProgramRuntime {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaScriptProgramRuntimeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreJavaScriptProgramsPackage.Literals.JAVA_SCRIPT_PROGRAM_RUNTIME;
	}

	@Override
	public void init() 
	{		
		/** Set to initialized */
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
		ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
		ProgramRuntimeState.INITIALIZED, true);
	}

	@Override
	public void terminate() 
	{		
		/** If running */
		if (getState() == ProgramRuntimeState.RUNNING || getState() == ProgramRuntimeState.RUNNING_SUSPENDED) 
		{
			/** Set to running_terminated */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
					ProgramRuntimeState.RUNNING_TERMINATED, true);
		} 
		else 
		{
			/** Set to terminated */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.TERMINATED,
					true);
		}
	}

	@Override
	public void resume() 
	{	
		System.out.println(getState());
		
		if(getState() != ProgramRuntimeState.TERMINATED && (getState() != ProgramRuntimeState.RUNNING))
		{
			if(getState() == ProgramRuntimeState.INITIALIZED)
			{
				/** Set to running */
				ApogyCommonTransactionFacade.INSTANCE.basicSet(JavaScriptProgramRuntimeImpl.this,
						ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.RUNNING,
						true);	
				
				// Start the executor in a thread.
				Job job = new Job("Executing Java Script <" + getProgram().getName() + ">.") 
				{
					@Override
					protected IStatus run(IProgressMonitor monitor) 
					{
						try 
						{			
							JavaScriptProgram javaScriptProgram = (JavaScriptProgram) getProgram();
							// ApogyRhinoDebuggerFrontend debug = new ApogyRhinoDebuggerFrontend(javaScriptProgram.getName());
							ScriptExecutor.execute(javaScriptProgram);
						} 
						catch (Exception e) 
						{
							Logger.INSTANCE.log(Activator.ID, "Error occured during resume() : ", EventSeverity.ERROR, e);
						}
						return Status.OK_STATUS;
					}
				};
				job.schedule();
			}
		}
	}

	@Override
	public void suspend() 
	{
		/** If running */
		if (getState() == ProgramRuntimeState.RUNNING) 
		{
			/** Set to running_suspended */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
					ProgramRuntimeState.RUNNING_SUSPENDED, true);
		} 
		else if (getState() != ProgramRuntimeState.RUNNING_TERMINATED)
		{
			/** Set to suspended */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.SUSPENDED,
					true);
		}
	}

	@Override
	public void stepInto() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stepOver() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stepReturn() {
		// TODO Auto-generated method stub
		
	}

	class ApogyRhinoDebuggerFrontend implements RhinoDebuggerFrontend 
	{
		private static final String LAUNCH_CONFIGURATION_PREFIX = "apogy";
		

		// Source: org.eclipse.wst.jsdt.debug.core/plugin.xml
		private static final String REMOTE_JS_SOURCE_LOCATOR_ID = "org.eclipse.wst.jsdt.debug.core.sourceLocator";

		// Source:
		// org.eclipse.wst.jsdt.debug.internal.rhino.jsdi.connect.RhinoAttachingConnector.id
		private static final String RHINO_ATTACHING_CONNECTOR_ID = "rhino.attaching.connector";

		private String projectName;

		public ApogyRhinoDebuggerFrontend(String projectName) {
			this.projectName = projectName;
		}

		@Override
		public void start(String address, int port) throws CoreException {
			ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
			ILaunchConfigurationType launchConfigurationType = manager.getLaunchConfigurationType(Constants.LAUNCH_CONFIG_ID);

			// Create launch configuration
			ILaunchConfigurationWorkingCopy remoteJavaScriptLaunchConfiguration = launchConfigurationType.newInstance(null, manager.generateLaunchConfigurationName(LAUNCH_CONFIGURATION_PREFIX));

			// Set container id
			remoteJavaScriptLaunchConfiguration.setAttribute(ILaunchConstants.CONNECTOR_ID, RHINO_ATTACHING_CONNECTOR_ID);

			// Set source locator
			AbstractSourceLookupDirector sourceLocator = (AbstractSourceLookupDirector) DebugPlugin.getDefault().getLaunchManager().newSourceLocator(REMOTE_JS_SOURCE_LOCATOR_ID);
			ProjectSourceContainer projectSourceContainer = new ProjectSourceContainer(ResourcesPlugin.getWorkspace().getRoot().getProject(projectName), false);
			sourceLocator.setSourceContainers(new ISourceContainer[] { projectSourceContainer });
			remoteJavaScriptLaunchConfiguration.setAttribute(ILaunchConfiguration.ATTR_SOURCE_LOCATOR_MEMENTO, sourceLocator.getMemento());

			// Set address
			Map<String, String> arguments = new HashMap<>();
			arguments.put(ILaunchConstants.HOST, address);
			arguments.put(ILaunchConstants.PORT, Integer.toString(port));
			remoteJavaScriptLaunchConfiguration.setAttribute(ILaunchConstants.ARGUMENT_MAP, arguments);

			// Launch
			remoteJavaScriptLaunchConfiguration.launch(ILaunchManager.DEBUG_MODE, null);

			// We don't have to save the launch configuration to use it, but in
			// case you want to see it from the LaunchConfigurationDialog, you
			// can save it with the following statement:
			// remoteJavaScriptLaunchConfiguration.doSave()
		}
	}

	/**
	 * Gets the name of the project of a JavaScriptProgram. The scriptPath
	 * attribute is in the format /<project name>/path/to/script.js . This
	 * function extracts the first part.
	 * 
	 * @return The project name or null if the script path has not the right
	 *         format
	 */
	private static String getProjectNameOfJavaScriptProgram(JavaScriptProgram program) {
		String scriptPath = program.getScriptPath();
		if (scriptPath == null) {
			return null;
		}

		File file = new File(scriptPath);
		while (file.getParentFile() != null && file.getParentFile().getParentFile() != null) {
			file = file.getParentFile();
		}
		return file.getName();
	}
	
} //JavaScriptProgramRuntimeImpl
