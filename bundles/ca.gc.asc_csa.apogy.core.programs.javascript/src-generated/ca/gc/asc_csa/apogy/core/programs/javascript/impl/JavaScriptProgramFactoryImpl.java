/**
 * Canadian Space Agency / Agence spatiale canadienne 2016 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.impl;

import org.eclipse.emf.ecore.EClass;

import ca.gc.asc_csa.apogy.core.invocator.AbstractProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ProgramSettings;
import ca.gc.asc_csa.apogy.core.invocator.impl.ProgramFactoryImpl;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsFactory;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgramFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Script Program Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JavaScriptProgramFactoryImpl extends ProgramFactoryImpl implements JavaScriptProgramFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaScriptProgramFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreJavaScriptProgramsPackage.Literals.JAVA_SCRIPT_PROGRAM_FACTORY;
	}

	/**
	 * @generated_NOT
	 */
	@Override
	public Program createProgram() 
	{
		return ApogyCoreJavaScriptProgramsFactoryImpl.eINSTANCE.createJavaScriptProgram();
	}

	@Override
	public AbstractProgramRuntime createProgramRuntime(Program program, ProgramSettings settings) 
	{	
		AbstractProgramRuntime runtime =  ApogyCoreJavaScriptProgramsFactory.eINSTANCE.createJavaScriptProgramRuntime();
		runtime.setProgram(program);
		return runtime;
	}
	
	@Override
	public void applySettings(Program program, ProgramSettings settings) 
	{
		// Nothing to do for now.
	}
} //JavaScriptProgramFactoryImpl
