/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator.ui;

import java.util.HashMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Program UI Factories Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * 
 * Registry to get the factory for UI elements of a Program EClass
 * 
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getProgramUIFactoriesMap <em>Program UI Factories Map</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage#getProgramUIFactoriesRegistry()
 * @model
 * @generated
 */
public interface ProgramUIFactoriesRegistry extends EObject 
{
	/**
	 * @generated_NOT
	 */
	public ProgramUIFactoriesRegistry INSTANCE = ProgramUIFactoriesRegistryImpl.getInstance();

	
	/**
	 * Returns the value of the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</b></em>' attribute.
	 * The default value is <code>"ca.gc.asc_csa.apogy.core.invocator.ui.programUIFactoryProvider"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage#getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID()
	 * @model default="ca.gc.asc_csa.apogy.core.invocator.ui.programUIFactoryProvider" unique="false" transient="true" changeable="false"
	 * @generated
	 */
	String getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID();

	/**
	 * Returns the value of the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</b></em>' attribute.
	 * The default value is <code>"programUIFactoryEClass"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage#getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID()
	 * @model default="programUIFactoryEClass" unique="false" transient="true" changeable="false"
	 * @generated
	 */
	String getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID();

	/**
	 * Returns the value of the '<em><b>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</b></em>' attribute.
	 * The default value is <code>"factory"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage#getProgramUIFactoriesRegistry_PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID()
	 * @model default="factory" unique="false" transient="true" changeable="false"
	 * @generated
	 */
	String getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID();

	/**
	 * Returns the value of the '<em><b>Program UI Factories Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Program UI Factories Map</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Program UI Factories Map</em>' attribute.
	 * @see #setProgramUIFactoriesMap(HashMap)
	 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage#getProgramUIFactoriesRegistry_ProgramUIFactoriesMap()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.core.invocator.ui.HashMap<org.eclipse.emf.ecore.EClass, ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory>"
	 * @generated
	 */
	HashMap<EClass, ProgramUIFactory> getProgramUIFactoriesMap();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry#getProgramUIFactoriesMap <em>Program UI Factories Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Program UI Factories Map</em>' attribute.
	 * @see #getProgramUIFactoriesMap()
	 * @generated
	 */
	void setProgramUIFactoriesMap(HashMap<EClass, ProgramUIFactory> value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Gets the factory corresponding to the EClass
	 * <!-- end-model-doc -->
	 * @model unique="false" eClassUnique="false"
	 * @generated
	 */
	ProgramUIFactory getFactory(EClass eClass);

} // ProgramUIFactoriesRegistry
