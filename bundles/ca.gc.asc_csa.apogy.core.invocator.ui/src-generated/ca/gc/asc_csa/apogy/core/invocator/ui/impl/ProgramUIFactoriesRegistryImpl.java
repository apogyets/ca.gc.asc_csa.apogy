/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.emf.Activator;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage;
import ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactoriesRegistry;
import ca.gc.asc_csa.apogy.core.invocator.ui.ProgramUIFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Program UI Factories Registry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl#getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoriesRegistryImpl#getProgramUIFactoriesMap <em>Program UI Factories Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProgramUIFactoriesRegistryImpl extends MinimalEObjectImpl.Container implements ProgramUIFactoriesRegistry 
{
	
	/**
	 * @generated_NOT
	 */
	private static ProgramUIFactoriesRegistry instance = null;

	/**
	 * @generated_NOT
	 */
	public static ProgramUIFactoriesRegistry getInstance() {
		if (instance == null) {
			instance = new ProgramUIFactoriesRegistryImpl();
		}
		return instance;
	}
	
	/**
	 * The default value of the '{@link #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID() <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID()
	 * @generated
	 * @ordered
	 */
	protected static final String PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID_EDEFAULT = "ca.gc.asc_csa.apogy.core.invocator.ui.programUIFactoryProvider";

	/**
	 * The cached value of the '{@link #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID() <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS POINT ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID()
	 * @generated
	 * @ordered
	 */
	protected String prograM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID = PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID() <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID()
	 * @generated
	 * @ordered
	 */
	protected static final String PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID_EDEFAULT = "programUIFactoryEClass";

	/**
	 * The cached value of the '{@link #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID() <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS ECLASS ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID()
	 * @generated
	 * @ordered
	 */
	protected String prograM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID = PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID() <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID()
	 * @generated
	 * @ordered
	 */
	protected static final String PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID_EDEFAULT = "factory";

	/**
	 * The cached value of the '{@link #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID() <em>PROGRAM FACTORY PROVIDER CONTRIBUTORS FACTORY ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID()
	 * @generated
	 * @ordered
	 */
	protected String prograM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID = PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProgramUIFactoriesMap() <em>Program UI Factories Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProgramUIFactoriesMap()
	 * @generated
	 * @ordered
	 */
	protected HashMap<EClass, ProgramUIFactory> programUIFactoriesMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProgramUIFactoriesRegistryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorUIPackage.Literals.PROGRAM_UI_FACTORIES_REGISTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID() {
		return prograM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID() {
		return prograM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID() {
		return prograM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public HashMap<EClass, ProgramUIFactory> getProgramUIFactoriesMap() 
	{
		HashMap<EClass, ProgramUIFactory> map = getProgramUIFactoriesMapGen();
		
		if (programUIFactoriesMap == null)
		{
			map = new HashMap<EClass, ProgramUIFactory>(); 
			
			IExtensionPoint extensionPoint = Platform.getExtensionRegistry().getExtensionPoint(getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID());

			IConfigurationElement[] contributors = extensionPoint.getConfigurationElements();
			
			for (int i = 0; i < contributors.length; i++) 
			{
				IConfigurationElement contributor = contributors[i];
				try 
				{
					String eClassStr = contributor.getAttribute(getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID());
					EClass eClass = ApogyCommonEMFFacade.INSTANCE.getEClass(eClassStr);
					ProgramUIFactory programFactory = (ProgramUIFactory) contributor.createExecutableExtension(getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID());
					map.put(eClass, programFactory);
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
					Logger.INSTANCE.log(Activator.ID, this,
							"Failed to load contributed ProgramUIFactory from <" + contributor.getClass().getName() + ">",
							EventSeverity.ERROR, e);
				}
			}
			setProgramUIFactoriesMap(map);
		}		
		
		return map;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HashMap<EClass, ProgramUIFactory> getProgramUIFactoriesMapGen() {
		return programUIFactoriesMap;
	}	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProgramUIFactoriesMap(HashMap<EClass, ProgramUIFactory> newProgramUIFactoriesMap) {
		HashMap<EClass, ProgramUIFactory> oldProgramUIFactoriesMap = programUIFactoriesMap;
		programUIFactoriesMap = newProgramUIFactoriesMap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_UI_FACTORIES_MAP, oldProgramUIFactoriesMap, programUIFactoriesMap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public ProgramUIFactory getFactory(EClass eClass) 
	{
		return getProgramUIFactoriesMap().get(eClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID:
				return getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID();
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID:
				return getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID();
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID:
				return getPROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID();
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_UI_FACTORIES_MAP:
				return getProgramUIFactoriesMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_UI_FACTORIES_MAP:
				setProgramUIFactoriesMap((HashMap<EClass, ProgramUIFactory>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_UI_FACTORIES_MAP:
				setProgramUIFactoriesMap((HashMap<EClass, ProgramUIFactory>)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID:
				return PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID_EDEFAULT == null ? prograM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID != null : !PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID_EDEFAULT.equals(prograM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID);
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID:
				return PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID_EDEFAULT == null ? prograM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID != null : !PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID_EDEFAULT.equals(prograM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID);
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID:
				return PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID_EDEFAULT == null ? prograM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID != null : !PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID_EDEFAULT.equals(prograM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID);
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY__PROGRAM_UI_FACTORIES_MAP:
				return programUIFactoriesMap != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORIES_REGISTRY___GET_FACTORY__ECLASS:
				return getFactory((EClass)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID: ");
		result.append(prograM_FACTORY_PROVIDER_CONTRIBUTORS_POINT_ID);
		result.append(", PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID: ");
		result.append(prograM_FACTORY_PROVIDER_CONTRIBUTORS_ECLASS_ID);
		result.append(", PROGRAM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID: ");
		result.append(prograM_FACTORY_PROVIDER_CONTRIBUTORS_FACTORY_ID);
		result.append(", programUIFactoriesMap: ");
		result.append(programUIFactoriesMap);
		result.append(')');
		return result.toString();
	}

} //ProgramUIFactoriesRegistryImpl
