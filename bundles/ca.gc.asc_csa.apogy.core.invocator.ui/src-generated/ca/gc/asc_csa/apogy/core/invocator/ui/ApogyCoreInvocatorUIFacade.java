package ca.gc.asc_csa.apogy.core.invocator.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.List;

import java.util.SortedSet;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.Context;
import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.Variable;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;
import ca.gc.asc_csa.apogy.core.invocator.ui.impl.ApogyCoreInvocatorUIFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * 
 * Utilities
 * 
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage#getApogyCoreInvocatorUIFacade()
 * @model
 * @generated
 */
public interface ApogyCoreInvocatorUIFacade extends EObject {

	public static ApogyCoreInvocatorUIFacade INSTANCE = ApogyCoreInvocatorUIFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Copies the Initialization Data.
	 * @param source Reference to the source context.
	 * @param destination Reference to the destination context.
	 * <!-- end-model-doc -->
	 * @model exceptions="ca.gc.asc_csa.apogy.common.emf.Exception" sourceUnique="false" destinationUnique="false"
	 * @generated
	 */
	void copyInitializationData(Context source, Context destination) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Copies the Initialization Data.
	 * @param source Reference to the source implementation.
	 * @param destination Reference to the destination implementation.
	 * @param command Reference to the stack command.
	 * <!-- end-model-doc -->
	 * @model exceptions="ca.gc.asc_csa.apogy.common.emf.Exception" sourceUnique="false" destinationUnique="false" commandDataType="ca.gc.asc_csa.apogy.core.invocator.ui.CompoundCommand" commandUnique="false"
	 * @generated
	 */
	void copyInitializationData(AbstractTypeImplementation source, AbstractTypeImplementation destination, CompoundCommand command) throws Exception;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Initializes the active session.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void initSession();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Disposes the active session.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void disposeSession();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Adds the variable to the specified list using a transaction.
	 * @param variablesList List of variables.
	 * @param variable Variable to add.
	 * @see ApogyCoreInvocatorFacade#addVariable(VariablesList variablesList, Variable variable)
	 * <!-- end-model-doc -->
	 * @model variablesListUnique="false" variableUnique="false"
	 * @generated
	 */
	void addVariable(VariablesList variablesList, Variable variable);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Delete the variables from the specified list using a transaction.
	 * @param variablesList List of variables.
	 * @param variables Variables to remove.
	 * @see ApogyCoreInvocatorFacade#deleteVariable(VariablesList variablesList, Variable variable)
	 * <!-- end-model-doc -->
	 * @model variablesListUnique="false" variablesDataType="ca.gc.asc_csa.apogy.core.invocator.ui.ListVariables" variablesUnique="false"
	 * @generated
	 */
	void deleteVariables(VariablesList variablesList, List<Variable> variables);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Saves a specified EObject to a part's Persisted State. The Resource Set used is the one associated with the active session.
	 * @param mPart The part.
	 * @param persistedStateKey The persisted state key under which the eObject will be saved.
	 * @param eObject The EObject to save.
	 * <!-- end-model-doc -->
	 * @model mPartDataType="ca.gc.asc_csa.apogy.core.invocator.ui.MPart" mPartUnique="false" persistedStateKeyUnique="false" eObjectUnique="false"
	 * @generated
	 */
	void saveToPersistedState(MPart mPart, String persistedStateKey, EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * Reads an EObject from a part's Persisted State. . The Resource Set used is the one associated with the active session.
	 * @param mPart The part.
	 * @param persistedStateKey The persisted state key under which the eObject is stored.
	 * @return The EObject read, null if none could be read.
	 * <!-- end-model-doc -->
	 * @model unique="false" mPartDataType="ca.gc.asc_csa.apogy.core.invocator.ui.MPart" mPartUnique="false" persistedStateKeyUnique="false"
	 * @generated
	 */
	EObject readFromPersistedState(MPart mPart, String persistedStateKey);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Sorts a list of types based on their name.
	 * @param types List of Type to be sorted.
	 * @return The Type sorted by name.
	 * <!-- end-model-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.core.invocator.ui.SortedSet<ca.gc.asc_csa.apogy.core.invocator.Type>" unique="false" typesDataType="ca.gc.asc_csa.apogy.core.invocator.ui.ListTypes" typesUnique="false"
	 * @generated
	 */
	SortedSet<Type> sortTypeByName(List<Type> types);
} // ApogyCoreInvocatorUIFacade
