package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import java.io.File;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.converters.ChainedConverter;
import ca.gc.asc_csa.apogy.common.converters.graphs.ApogyCommonConvertersGraphsFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResultValue;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallResult;

public class AbstractResultValueDetailsComposite extends Composite 
{
	private AbstractResultValue resultValue;
	
	private Composite groupExport;
	private Button exportButton;
	
	private Composite compositeDetails;
	
	public AbstractResultValueDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2,false));
		
		// Details
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_compositeDetails.widthHint = 398;
		gd_compositeDetails.minimumWidth = 398;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
		
		// Export.
		groupExport = new Composite(this, SWT.NONE);		
		groupExport.setLayout(new GridLayout(1,false));
		GridData gd_groupExport = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		groupExport.setLayoutData(gd_groupExport);
		
		exportButton = new Button(groupExport, SWT.PUSH);
		exportButton.setText("Export to File...");
		exportButton.setEnabled(false);
		exportButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	public AbstractResultValue getAbstractResultValue() {
		return resultValue;
	}

	public void setAbstractResultValue(AbstractResultValue resultValue) 
	{
		this.resultValue = resultValue;
					
		if(!isDisposed())
		{		
						
			// Dispose of the compositeDetails children.
			for (Control control : compositeDetails.getChildren()) 
			{
				control.dispose();
			}
			
			if(resultValue != null)
			{
				Object value = ApogyCoreInvocatorFacade.INSTANCE.getResultValue(resultValue.getResult());
				
				if(value instanceof EObject)
				{
					// TODO
					EObject eObject = (EObject) value;
																				
					// VView viewModel = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createDefaultViewModel(eObject);
					ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, eObject);
					
					// TODO
					// exportButton.setEnabled(canBeExportedToFile(eObject));
					
				}
				else if(value != null)
				{
					exportButton.setEnabled(false);
					createValueLabel(compositeDetails, resultValue);								
					compositeDetails.layout();
				}
				else
				{
					exportButton.setEnabled(false);
					ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, null, "No data to display");	
				}					
			}
		}
	}
	
	protected void createValueLabel(Composite parent, AbstractResultValue resultValue)	
	{
		parent.setLayout(new GridLayout(1,false));
		
		Object value = ApogyCoreInvocatorFacade.INSTANCE.getResultValue(resultValue.getResult());
		
		Composite baseComposite = new Composite(parent, SWT.NONE);		
		baseComposite.setLayout(new GridLayout(3, false));
		baseComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		
		Label resultLabel = new Label(baseComposite, SWT.NONE);
		resultLabel.setText("Result : ");
				
		Text valueLabel = new Text(baseComposite, SWT.NONE);
		valueLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));		
		valueLabel.setText(value.toString());	
		valueLabel.setEditable(false);
		
		Label unitsLabel = new Label(baseComposite, SWT.NONE);
		
		if(resultValue.getResult() instanceof OperationCallResult)
		{
			OperationCallResult operationCallResult = (OperationCallResult) resultValue.getResult();
			if(operationCallResult.getOperationCall() != null)
			{
				String units = ApogyCommonEMFFacade.INSTANCE.getEngineeringUnitsAsString(operationCallResult.getOperationCall().getEOperation());
				if(units != null) unitsLabel.setText(units);
			}
			else
			{
				unitsLabel.setText("No units applicable.");
			}
		}		
	}
	
	protected boolean canBeExportedToFile(EObject eObject)
	{
		Object object = ApogyCommonConvertersFacade.INSTANCE.convert(eObject, File.class);				
		return (object != null);
	}
}
