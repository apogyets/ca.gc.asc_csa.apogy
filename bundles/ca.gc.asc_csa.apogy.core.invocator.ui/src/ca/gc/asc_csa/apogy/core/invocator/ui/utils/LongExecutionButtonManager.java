package ca.gc.asc_csa.apogy.core.invocator.ui.utils;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.progress.UIJob;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.core.invocator.ui.Activator;

public abstract class LongExecutionButtonManager 
{		
	protected Button managedButton = null;
	protected boolean notifyOnCompletion = true;
	protected String operationDescription = null;	
	
	public LongExecutionButtonManager(Button managedButton, String operationDescription, boolean notifyOnCompletion)
	{
		this.managedButton = managedButton;
		this.operationDescription = operationDescription;
		this.notifyOnCompletion = notifyOnCompletion;
		
		managedButton.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				managedButton.setEnabled(false);
				Logger.INSTANCE.log(Activator.ID, "Launching execution of " + operationDescription, EventSeverity.INFO);
				
				Job job = new Job(operationDescription)
				{						
					@Override
					protected IStatus run(IProgressMonitor monitor) 
					{								
						try
						{
							execute();
							
							postExecuteWithSuccess();
							
							if(notifyOnCompletion)
							{						
								Display.getDefault().asyncExec(new Runnable() 
								{													
									@Override
									public void run() 
									{
										try
										{
											String message = getExecutionSuccessMessage();									
											Logger.INSTANCE.log(Activator.ID, message, EventSeverity.OK);
											MessageDialog.openInformation(managedButton.getDisplay().getActiveShell(), operationDescription + " Success", message);
										}
										catch (Exception e) 
										{
											e.printStackTrace();
										}
									}
								});							
							}
						}
						catch (final Exception e) 
						{						
							postExecuteWithError(e);
							
							Display.getDefault().asyncExec(new Runnable() {													
								@Override
								public void run() 
								{
									String message = getExecutionErrorMessage(e);
																						
									postExecuteWithError(e);
									
									Logger.INSTANCE.log(Activator.ID, message, EventSeverity.ERROR);
									MessageDialog.openError(managedButton.getDisplay().getActiveShell(), "Error", message);		
								}
							});
						}												
						
						return Status.OK_STATUS;
					}			
				};
				job.setPriority(Job.LONG);			
				job.schedule();									
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
			
	/**
	 * Method that return the message to display and log for successful execution.
	 * @return
	 */
	public String getExecutionSuccessMessage()
	{
		String message = operationDescription + " : Completed sucessfully.";
		return message;
	}
	
	/**
	 * Method that return the message to display and log for failed execution.
	 * @return
	 */
	public String getExecutionErrorMessage(Throwable t)
	{
		String message = "The following error occured during " + operationDescription + " : " + t.toString();
		return message;
	}
	
	
	/**
	 * Method called after the execute() has completed with success. It is called before the Message Dialog is shown. 
	 */
	public void postExecuteWithSuccess()
	{
		// Starts UI job that wait for the execution to complete.
		UIJob uiJob = new UIJob(operationDescription + "postExecuteWithSuccess()")
		{
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) 
			{
				try
				{
					if(!managedButton.isDisposed())
					{
						managedButton.setEnabled(true);
					}
				}
				catch(Throwable t)
				{
					t.printStackTrace();
				}
				return Status.OK_STATUS;
			}
		};
		uiJob.schedule();				
	}
	
	/**
	 * Method called after the execute() has completed with error. It is called before the Error Message Dialog is shown. 	
	 */
	public void postExecuteWithError(Throwable t)
	{
		// Starts UI job that wait for the execution to complete.
		UIJob uiJob = new UIJob(operationDescription + "postExecuteWithError(t)")
		{
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) 
			{
				try
				{
					if(!managedButton.isDisposed())
					{
						managedButton.setEnabled(true);
					}
				}
				catch(Throwable t)
				{
					t.printStackTrace();
				}
				return Status.OK_STATUS;
			}
		};
		uiJob.schedule();		
	}
	
	public abstract void execute() throws Exception;
		
}
