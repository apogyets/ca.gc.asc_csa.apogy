package ca.gc.asc_csa.apogy.core.invocator.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.viewers.ViewerSupport;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypesRegistry;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;

public class TypesRegistryComposite extends Composite {
	private DataBindingContext m_bindingContext;
	private TableViewer viewer;
	private WritableValue<TypesRegistry> typesRegistryBinder = new WritableValue<>();
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	public TypesRegistryComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
	
		viewer = new TableViewer(this, SWT.BORDER | SWT.SINGLE | SWT.FULL_SELECTION | SWT.V_SCROLL);
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		table.setLinesVisible(true);
		ColumnViewerToolTipSupport.enableFor(viewer);
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				newSelection(event.getSelection());
			}
		});
			
		TableViewerColumn tableViewerColumnItem_Name = new TableViewerColumn(viewer, SWT.NONE);		
		TableColumn trclmnName = tableViewerColumnItem_Name.getColumn();
		trclmnName.setText("Name");
		trclmnName.setWidth(250);
		
		TableViewerColumn tableViewerColumn_Type = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn tblclmnType = tableViewerColumn_Type.getColumn();
		tblclmnType.setWidth(450);
		tblclmnType.setText("Type");
		
		TableViewerColumn tableViewerColumn_Description = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn tblclmnDescription = tableViewerColumn_Description.getColumn();
		tblclmnDescription.setWidth(400);
		tblclmnDescription.setText("Description");
		
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setLabelProvider(new CustomLabelProvider(adapterFactory));
		viewer.setInput(ApogyCoreInvocatorUIFacade.INSTANCE.sortTypeByName(TypesRegistry.INSTANCE.getTypes()).toArray());
		
		// m_bindingContext = customInitDataBindings();
		
		addDisposeListener(new DisposeListener() 
		{
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();	
			}
		});
	}		
	
	/**
	 * This method is invoked a when a new selection is selected.
	 */
	protected void newSelection(ISelection selection) 
	{
	}

	/**
	 * Returns the selected {@link Type}.
	 * 
	 * @return Reference to the list of {@link Type}.
	 */
	public Type getSelectedType() {		
		return (Type) (((IStructuredSelection)viewer.getSelection()).getFirstElement());
	}

	protected DataBindingContext customInitDataBindings() {
		
		DataBindingContext bindingContext = new DataBindingContext();
		
		/*
		 * Bind variables list.
		 */
		@SuppressWarnings("unchecked")
		IObservableList<?> typesListObserveList = EMFProperties
				.list(ApogyCoreInvocatorPackage.Literals.TYPES_REGISTRY__TYPES)
				.observeDetail(typesRegistryBinder);

		ViewerSupport.bind(viewer, typesListObserveList,
				EMFProperties.value(ApogyCommonEMFPackage.Literals.NAMED__NAME), EMFProperties.value(ApogyCommonEMFPackage.Literals.DESCRIBED__DESCRIPTION));
	
		typesRegistryBinder.setValue(TypesRegistry.INSTANCE);
		
		return bindingContext;
	}
	
	/**
	 * 
	 * Label Provider.
	 *
	 */
	private class CustomLabelProvider extends
			AdapterFactoryLabelProvider implements ITableLabelProvider,
			ITableColorProvider {
				
		private final static int NAME_COLUMN = 0;		
		private final static int TYPE_COLUMN = 1;
		private final static int DESCRIPTION_COLUMN = 2;
		
		
		public CustomLabelProvider(	AdapterFactory adapterFactory) 
		{
			super(adapterFactory);
		}
		
		@Override
		public String getColumnText(Object object, int columnIndex) {
			String str = "<undefined>";

			switch (columnIndex) 
			{
				case NAME_COLUMN:				
					if(object instanceof Type)
					{
						Type type = (Type) object;
						str = type.getName();
					}
					break;
	
				case TYPE_COLUMN:
					str = "";
					if(object instanceof Type)
					{
						Type type = (Type) object;
						if(type.getInterfaceClass() != null)
						{
							str = type.getInterfaceClass().getInstanceTypeName();							
						}
						else
						{
							str = "null";
						}
					}
					break;
				
				case DESCRIPTION_COLUMN:				
					if(object instanceof Described)
					{
						Described described = (Described) object;
						str = described.getDescription();
					}
					break;
							
					default:
					break;
			}

			return str;
		}
		
		@Override
		public Image getColumnImage(Object object, int columnIndex) 
		{
			Image image = null;
			switch (columnIndex) 
			{
				case NAME_COLUMN:				
					image = super.getColumnImage(object, columnIndex);
					break;				
							
					default:
					break;
			}
			
			return image;
		}
	}
}