package ca.gc.asc_csa.apogy.core.invocator.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import ca.gc.asc_csa.apogy.core.invocator.Context;
import ca.gc.asc_csa.apogy.core.invocator.Environment;

public class ContextsDefinitionComposite extends ScrolledComposite {
	private DataBindingContext m_bindingContext;

	private FormToolkit toolkit = new FormToolkit(Display.getCurrent());

	private Environment environment;

	private ContextsListComposite contextsListComposite;

	private VariableImplementationsComposite variableImplementationsComposite;

	public ContextsDefinitionComposite(Composite parent, int style, Context context, Environment environment) {
		this(parent, style);
		setEnvironment(environment);
	}

	/**
	 * Creates the parentComposite.
	 * 
	 * @param parent
	 * @param style
	 */
	public ContextsDefinitionComposite(Composite parent, int style) {
		super(parent, style);

		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				toolkit.dispose();
				contextsListComposite.dispose();
				variableImplementationsComposite.dispose();
				if (m_bindingContext != null) {
					m_bindingContext.dispose();
					m_bindingContext = null;
				}
			}
		});
		setLayout(new FillLayout());
		setExpandHorizontal(true);
		setExpandVertical(true);

		Composite composite = new Composite(this, SWT.None);
		composite.setLayout(new GridLayout(2, false));

		Section sctnContexts = toolkit.createSection(composite, Section.EXPANDED | Section.TITLE_BAR);
		GridData gd_sctnContexts = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_sctnContexts.minimumWidth = 200;
		gd_sctnContexts.widthHint = 211;
		sctnContexts.setLayoutData(gd_sctnContexts);
		toolkit.paintBordersFor(sctnContexts);
		sctnContexts.setText("Contexts");

		contextsListComposite = new ContextsListComposite(sctnContexts, SWT.NONE) {
			@Override
			protected void newSelection(ISelection selection) {
				variableImplementationsComposite.setContext(contextsListComposite.getSelectedContext());
				ContextsDefinitionComposite.this.newSelection(selection);
			}

		};
		toolkit.adapt(contextsListComposite);
		toolkit.paintBordersFor(contextsListComposite);
		sctnContexts.setClient(contextsListComposite);

		Section sctnVariableImplementations = toolkit.createSection(composite, Section.TITLE_BAR | Section.EXPANDED);
		GridData gd_sctnVariableImplementations = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_sctnVariableImplementations.widthHint = 300;
		gd_sctnVariableImplementations.minimumWidth = 300;
		sctnVariableImplementations.setLayoutData(gd_sctnVariableImplementations);
		toolkit.paintBordersFor(sctnVariableImplementations);
		sctnVariableImplementations.setText("Implementation");

		variableImplementationsComposite = new VariableImplementationsComposite(sctnVariableImplementations, SWT.NONE) {
			@Override
			protected void newSelection(ISelection selection) {
				ContextsDefinitionComposite.this.newSelection(selection);
			}
		};
		toolkit.adapt(variableImplementationsComposite);
		toolkit.paintBordersFor(variableImplementationsComposite);
		sctnVariableImplementations.setClient(variableImplementationsComposite);

		/**
		 * Perform a layout otherwise the VariableImplementation Content is not
		 * displayed without resize.
		 */
		composite.layout(true, true);

		setContent(composite);
		setMinSize(composite.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}

	/**
	 * This method is called when a new selection is made in the parentComposite.
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(ISelection selection) {
	}

	/**
	 * Binds the {@link Environment} with the parentComposite.
	 * 
	 * @param environment
	 *            Reference to the environment.
	 */
	public void setEnvironment(Environment environment) {
		setEnvironment(environment, true);
	}

	/**
	 * Sets the {@link Environment}.
	 * 
	 * @param environment
	 *            Reference to the environment.
	 * @param update
	 *            If true then data bindings are created.
	 */
	private void setEnvironment(Environment environment, boolean update) {
		this.environment = environment;
		if (update) {
			if (m_bindingContext != null) {
				m_bindingContext.dispose();
				m_bindingContext = null;
			}

			if (environment != null) {
				m_bindingContext = initDataBindings();
			}
		}
	}

	/**
	 * Use this to prevent Window Pro Builder code analysis to fail with the
	 * complex data bindings code. Invokes
	 * {@link ContextsDefinitionComposite#initDataBindingsCustom()}.
	 * 
	 * @return Reference to the data bindings context.
	 * @see ContextsDefinitionComposite#initDataBindingsCustom()
	 */
	private DataBindingContext initDataBindings() {
		return initDataBindingsCustom();
	}

	/**
	 * Creates and returns the data bindings context that takes care of the
	 * Variables viewer and the Type Members viewer.
	 * 
	 * @return Reference to the data bindings context.
	 */
	private DataBindingContext initDataBindingsCustom() {
		DataBindingContext bindingContext = new DataBindingContext();

		contextsListComposite.setEnvironment(environment);
		variableImplementationsComposite.setContext(contextsListComposite.getSelectedContext());

		return bindingContext;
	}
}