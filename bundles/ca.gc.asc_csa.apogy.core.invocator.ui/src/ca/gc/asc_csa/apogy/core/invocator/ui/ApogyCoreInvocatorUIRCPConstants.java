/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 	 	 Regent L'Archeveque, 
 * 	 	 Sebastien Gemme 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator.ui;

public class ApogyCoreInvocatorUIRCPConstants {
	
	/**
	 * Parts IDs for selectionBasedParts
	 */
	public static final String PART__PROGRAM_ARGUMENTS__ID = "ca.gc.asc_csa.apogy.core.invocator.ui.part.programArgumentsPart";
	public static final String PART__SESSION_EDITOR__ID = "ca.gc.asc_csa.apogy.core.invocator.ui.part.sessionEditor";
	public static final String PART__VARIABLE_RUNTIME__ID = "ca.gc.asc_csa.apogy.core.invocator.ui.part.variableRuntimePart";
	public static final String PART__PROGRAM__ID = "ca.gc.asc_csa.apogy.core.invocator.ui.part.program";
	public static final String PART__SCRIPT_BASED_PROGRAMS_LIST__ID = "ca.gc.asc_csa.apogy.core.invocator.ui.part.scriptBasedProgramsList";
	public static final String PART__VARIABLES_LIST__ID = "ca.gc.asc_csa.apogy.core.invocator.ui.part.variablesList";
	
	/**
	 * IDs for RunProgramRuntimeCommand
	 */
	public static final String COMMAND__RUN_PROGRAM_RUNTIME__ID = "ca.gc.asc_csa.apogy.core.invocator.ui.command.runProgramRuntime";
	public static final String COMMAND_PARAMETER__RUN_PROGRAM_RUNTIME_STEP_BY_STEP__ID = "ca.gc.asc_csa.apogy.core.invocator.ui.commandparameter.stepByStep";

	/**
	 * ID for MapBaseSettings
	 */
	public static final String NAME_ID = "name";
	public static final String TYPE_ID = "type";
			
}
