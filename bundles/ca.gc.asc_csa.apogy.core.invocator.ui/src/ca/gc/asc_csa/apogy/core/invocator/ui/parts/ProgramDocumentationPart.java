package ca.gc.asc_csa.apogy.core.invocator.ui.parts;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractDocumentationPart;
import ca.gc.asc_csa.apogy.core.invocator.Argument;
import ca.gc.asc_csa.apogy.core.invocator.OperationCall;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIRCPConstants;

public class ProgramDocumentationPart extends AbstractDocumentationPart {

	@Override
	protected List<String> getSelectionListenerPartIds() {
		List<String> ids = new ArrayList<String>();
		
		ids.add(ApogyCoreInvocatorUIRCPConstants.PART__SCRIPT_BASED_PROGRAMS_LIST__ID);
		ids.add(ApogyCoreInvocatorUIRCPConstants.PART__PROGRAM__ID);
		ids.add(ApogyCoreInvocatorUIRCPConstants.PART__PROGRAM_ARGUMENTS__ID);
		
		return ids;
	}
	
	@Override
	protected String getCustomDoc(EObject eObject) 
	{		
		if(eObject instanceof OperationCall && ((OperationCall)eObject).getEOperation() != null)
		{
			return ApogyCommonEMFFacade.INSTANCE.getDocumentation(((OperationCall)eObject).getEOperation());			
		}
		else if(eObject instanceof Argument)
		{
			Argument argument = (Argument) eObject;
			return ApogyCommonEMFFacade.INSTANCE.getDocumentation(argument.getEParameter());
		}
		
		return super.getCustomDoc(eObject);
	}

	
}