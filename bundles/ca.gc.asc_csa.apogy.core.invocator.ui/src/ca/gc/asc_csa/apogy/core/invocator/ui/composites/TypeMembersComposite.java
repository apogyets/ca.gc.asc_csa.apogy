/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;

public class TypeMembersComposite extends Composite 
{
	private Type type;
	
	private FormToolkit formToolkit = new FormToolkit(Display.getCurrent());
	private TypeMembersListComposite typeMembersListComposite;
	private TypeMemberOverviewComposite typeMemberOverviewComposite;
	
	public TypeMembersComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(1, false));
		
		ScrolledForm topScrolledForm = formToolkit.createScrolledForm(this);
		topScrolledForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		topScrolledForm.setShowFocusedControl(true);
		formToolkit.paintBordersFor(topScrolledForm);
		topScrolledForm.setText(null);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 1;
			topScrolledForm.getBody().setLayout(tableWrapLayout);
		}
		
		// Member List
		Section membersListSection = formToolkit.createSection(topScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnFeaturesList = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnFeaturesList.valign = TableWrapData.FILL;		
		twd_sctnFeaturesList.grabVertical = true;
		twd_sctnFeaturesList.grabHorizontal = true;
		membersListSection.setLayoutData(twd_sctnFeaturesList);
		formToolkit.paintBordersFor(membersListSection);
		membersListSection.setText("Members");
		
		typeMembersListComposite = new TypeMembersListComposite(membersListSection, SWT.NONE)
		{
			@Override
			protected void newTypeMemberSelected(TypeMember selectedTypeMember) 
			{
				if(typeMemberOverviewComposite != null)
				{
					typeMemberOverviewComposite.setTypeMember(selectedTypeMember);
				}
			}
		};
				
		formToolkit.adapt(typeMembersListComposite);
		formToolkit.paintBordersFor(typeMembersListComposite);
		membersListSection.setClient(typeMembersListComposite);
		
		
		// Member Overview		
		Section memberOverviewSection = formToolkit.createSection(topScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_memberOverviewSection = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_memberOverviewSection.valign = TableWrapData.FILL;		
		twd_memberOverviewSection.grabVertical = true;
		twd_memberOverviewSection.grabHorizontal = true;
		memberOverviewSection.setLayoutData(twd_memberOverviewSection);
		formToolkit.paintBordersFor(memberOverviewSection);
		memberOverviewSection.setText("Member Overview");
		
		typeMemberOverviewComposite = new TypeMemberOverviewComposite(memberOverviewSection, SWT.NONE);
				
		formToolkit.adapt(typeMemberOverviewComposite);
		formToolkit.paintBordersFor(typeMemberOverviewComposite);
		memberOverviewSection.setClient(typeMemberOverviewComposite);				
	}

	public Type getType() {
		return type;
	}

	public void setType(Type newType) 
	{
		this.type = newType;
		
		if(typeMembersListComposite != null && !typeMembersListComposite.isDisposed())
		{
			typeMembersListComposite.setType(newType);
		}
		
		if(typeMemberOverviewComposite != null && !typeMemberOverviewComposite.isDisposed())
		{
			if(newType != null && newType.getMembers().size() > 0)
			{
				typeMemberOverviewComposite.setTypeMember(newType.getMembers().get(0));
			}
			else
			{
				typeMemberOverviewComposite.setTypeMember(null);			
			}
		}
	}
}
