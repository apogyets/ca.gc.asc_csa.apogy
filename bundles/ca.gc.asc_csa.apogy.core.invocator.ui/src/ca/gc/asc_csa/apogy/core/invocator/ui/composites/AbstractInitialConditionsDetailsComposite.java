package ca.gc.asc_csa.apogy.core.invocator.ui.composites;

import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.invocator.AbstractInitialConditions;

public class AbstractInitialConditionsDetailsComposite extends Composite 
{
	private AbstractInitialConditions abstractInitialConditions;
	
	private Composite compositeDetails;
	
	public AbstractInitialConditionsDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.widthHint = 398;
		gd_compositeDetails.minimumWidth = 398;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public AbstractInitialConditions getAbstractInitialConditions() {
		return abstractInitialConditions;
	}

	public void setAbstractInitialConditions(AbstractInitialConditions newAbstractInitialConditions) 
	{
		this.abstractInitialConditions = newAbstractInitialConditions;
		
		// Update Details EMFForm.
		if(newAbstractInitialConditions != null)
		{
			VView viewModel = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createDefaultViewModel(newAbstractInitialConditions.getAbstractInitializationData());
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newAbstractInitialConditions.getAbstractInitializationData(), viewModel);
		}
		else
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, null);
		}
	}
}
