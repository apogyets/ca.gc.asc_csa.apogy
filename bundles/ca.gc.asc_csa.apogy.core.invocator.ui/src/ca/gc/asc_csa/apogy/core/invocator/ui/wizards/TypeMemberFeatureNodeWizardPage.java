/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator.ui.wizards;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureSpecifier;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureTreeNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractRootNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.TreeRootNode;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.Type;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;

public class TypeMemberFeatureNodeWizardPage extends WizardPage
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.invocator.ui.wizards.TypeMemberFeatureNodeWizardPage";
	
	private Type parentType;	
	private TypeMember typeMember;	
		
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	private Button btnSelect;
	private Button btnClear;
	
	private Text txtSelectedAbstractFeatureTreeNode;

	
	private AbstractFeatureTreeNode selectedAbstractFeatureTreeNode = null;
	
	public TypeMemberFeatureNodeWizardPage(Type parentType, TypeMember typeMember)
	{
		super(WIZARD_PAGE_ID);			
		setTitle("Member mapping to System's feature");
		setDescription("Select the feature of the parent system to which this member should be mapped to. Use the NEW button to create the Feature Node that point to the required feature.");	
		
		this.parentType = parentType;
		this.typeMember = typeMember;
	}

	@Override
	public void createControl(Composite parent1) 
	{		
		Composite container = new Composite(parent1, SWT.None);
		container.setLayout(new GridLayout(2, false));
		setControl(container);		
		setPageComplete(true);
		
		if(parentType != null && parentType.getInterfaceClass() != null)
		{
			treeViewer = new TreeViewer(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION );
			tree = treeViewer.getTree();
			GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
			gd_tree.widthHint = 200;
			gd_tree.minimumWidth = 200;
			tree.setLayoutData(gd_tree);
			tree.setLinesVisible(true);
			
			treeViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
			treeViewer.setLabelProvider(new CustomAdapterFactoryLabelProvider(adapterFactory));
			treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
			{						
				@Override
				public void selectionChanged(SelectionChangedEvent event) 
				{
					AbstractFeatureTreeNode selectedNode = (AbstractFeatureTreeNode)((IStructuredSelection) event.getSelection()).getFirstElement();				
					setSelectedAbstractFeatureTreeNode(selectedNode);			
					
					if(selectedNode != null)
					{
						btnDelete.setEnabled(true);
					}
					else
					{
						btnDelete.setEnabled(false);
					}
					
					if(selectedNode instanceof AbstractFeatureSpecifier)
					{
						AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) selectedNode;
						if(isCompatibleWithFeatureSuperClass(abstractFeatureSpecifier))
						{
							btnSelect.setEnabled(true);
						}
						else
						{
							btnSelect.setEnabled(false);
						}
					}
				}
			});
			
			// Buttons.
			Composite composite = new Composite(container, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			composite.setLayout(new GridLayout(1, false));	
			
			btnNew = new Button(composite, SWT.NONE);
			btnNew.setSize(74, 29);
			btnNew.setText("New");
			btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnNew.setEnabled(true);
			btnNew.addListener(SWT.Selection, new Listener() 
			{		
				@Override
				public void handleEvent(Event event) 
				{				
					if (event.type == SWT.Selection) 
					{	
						AbstractFeatureTreeNode parent = null;
						
						// If no node has been selected yet.
						if(getSelectedAbstractFeatureTreeNode() == null)
						{
							if(typeMember.getTypeFeatureRootNode() == null)
							{
								TreeRootNode treeRoot = ApogyCommonEMFFactory.eINSTANCE.createTreeRootNode();
								treeRoot.setSourceClass(parentType.getInterfaceClass());								
								ApogyCommonTransactionFacade.INSTANCE.basicSet(typeMember, ApogyCoreInvocatorPackage.Literals.TYPE_MEMBER__TYPE_FEATURE_ROOT_NODE, treeRoot, true);	
								
							}
							
							parent = typeMember.getTypeFeatureRootNode();
						}
						else
						{
							parent = getSelectedAbstractFeatureTreeNode();
						}
						
						MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();
						settings.getUserDataMap().put("parent", parent);		
						
						ApogyEObjectWizard wizard = new ApogyEObjectWizard(ApogyCommonEMFPackage.Literals.ABSTRACT_FEATURE_TREE_NODE__CHILDREN, parent, settings, ApogyCommonEMFPackage.Literals.TREE_FEATURE_NODE); 
						WizardDialog dialog = new WizardDialog(getShell(), wizard);
						dialog.open();
						
						// Forces the viewer to refresh its input.
						if(!treeViewer.isBusy())
						{					
							treeViewer.setInput(typeMember.getTypeFeatureRootNode());
							
							if(wizard.getCreatedEObject() != null)
							{
								treeViewer.setSelection(new StructuredSelection(wizard.getCreatedEObject()));
							}
						}					
					}
				}
			});
					
			btnDelete = new Button(composite, SWT.NONE);
			btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnDelete.setSize(74, 29);
			btnDelete.setText("Delete");
			btnDelete.setEnabled(false);
			btnDelete.addSelectionListener(new SelectionAdapter() 
			{
				@Override
				public void widgetSelected(SelectionEvent event) 
				{
					String deleteMessage = "";			
					
					Iterator<AbstractFeatureTreeNode> treeNodeIterator = getSelectedAbstractFeatureTreeNodes().iterator();
					while (treeNodeIterator.hasNext()) 
					{
						// TODO
						AbstractFeatureTreeNode treeNode = treeNodeIterator.next();
						
						if(treeNode instanceof AbstractFeatureSpecifier)
						{
							AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) treeNode;
							
							if( abstractFeatureSpecifier.getStructuralFeature() != null)
							{
								deleteMessage = abstractFeatureSpecifier.getStructuralFeature().getName();
							}
						}					
	
						if (treeNodeIterator.hasNext()) 
						{
							deleteMessage = deleteMessage + ", ";
						}
					}
	
					MessageDialog dialog = new MessageDialog(null, "Delete the selected Features", null,
							"Are you sure to delete these Features: " + deleteMessage, MessageDialog.QUESTION,
							new String[] { "Yes", "No" }, 1);
					int result = dialog.open();
					if (result == 0) 
					{
						List<AbstractFeatureTreeNode> listOfNodesToRemove = getSelectedAbstractFeatureTreeNodes();
						
						for(AbstractFeatureTreeNode nodeToRemove : listOfNodesToRemove)
						{
							if(!(nodeToRemove instanceof AbstractRootNode))
							{
								ApogyCommonTransactionFacade.INSTANCE.basicRemove(nodeToRemove.getParent(), ApogyCommonEMFPackage.Literals.ABSTRACT_FEATURE_TREE_NODE__CHILDREN, nodeToRemove);
							}
						}										
					}
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(typeMember.getTypeFeatureRootNode());
					}					
				}
			});		
			
			Composite bottom = new Composite(container, SWT.NONE);
			bottom.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
			bottom.setLayout(new GridLayout(4, false));	
							
			btnSelect = new Button(bottom, SWT.NONE);
			btnSelect.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			btnSelect.setSize(74, 29);
			btnSelect.setText("Select");
			btnSelect.setEnabled(false);
			btnSelect.addSelectionListener(new SelectionListener() 
			{				
				@Override
				public void widgetSelected(SelectionEvent arg0) 
				{			
					if(txtSelectedAbstractFeatureTreeNode != null && !txtSelectedAbstractFeatureTreeNode.isDisposed())
					{
						if(getSelectedAbstractFeatureTreeNode() != null)
						{
							txtSelectedAbstractFeatureTreeNode.setText(ApogyCommonEMFFacade.INSTANCE.getAncestriesString(getSelectedAbstractFeatureTreeNode()));
						}
						else
						{
							txtSelectedAbstractFeatureTreeNode.setText("");
						}
					}
					
					validate();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {					
				}
			});		
			
			btnClear = new Button(bottom, SWT.NONE);
			btnClear.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
			btnClear.setSize(74, 29);
			btnClear.setText("Clear");
			btnClear.setEnabled(true);
			btnClear.addSelectionListener(new SelectionListener() 
			{				
				@Override
				public void widgetSelected(SelectionEvent arg0) 
				{			
					ApogyCommonTransactionFacade.INSTANCE.basicSet(typeMember, ApogyCoreInvocatorPackage.Literals.TYPE_MEMBER__TYPE_FEATURE_ROOT_NODE, null, true);
					if(!treeViewer.isBusy()) treeViewer.refresh();					
					txtSelectedAbstractFeatureTreeNode.setText("");
					validate();
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {										
				}
			});		
			
			
			// Feature
			Label lblFeature = new Label(bottom, SWT.NONE);
			lblFeature.setText("Feature : ");
			lblFeature.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));

			txtSelectedAbstractFeatureTreeNode = new Text(bottom, SWT.BORDER);
			txtSelectedAbstractFeatureTreeNode.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			txtSelectedAbstractFeatureTreeNode.setEditable(false);
		}				
	}
	
	public AbstractFeatureTreeNode getSelectedAbstractFeatureTreeNode() {
		return selectedAbstractFeatureTreeNode;
	}

	public void setSelectedAbstractFeatureTreeNode(AbstractFeatureTreeNode selectedAbstractFeatureTreeNode) 
	{
		this.selectedAbstractFeatureTreeNode = selectedAbstractFeatureTreeNode;		
	}

	@SuppressWarnings("unchecked")
	public List<AbstractFeatureTreeNode> getSelectedAbstractFeatureTreeNodes()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}	
	
	protected void validate()
	{
		setErrorMessage(null);	
		if(typeMember.getTypeFeatureRootNode() != null)
		{
			// Verifies that the parent type does not have another member mapped to the same attribute.
			EReference typeMemberEReference = getEReference(typeMember.getTypeFeatureRootNode());			
			
			for(TypeMember member : parentType.getMembers())
			{
				if(member != typeMember)
				{
					if(member.getTypeFeatureRootNode() != null)
					{
						EReference leafEReference = getEReference(member.getTypeFeatureRootNode());
																		
						if(typeMemberEReference == leafEReference)
						{
							setErrorMessage("Another type member <" + member.getName() + "> in this system is mapped to " + ApogyCommonEMFFacade.INSTANCE.getAncestriesString(getSelectedAbstractFeatureTreeNode()));
						}
					}
				}
			}
		}		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private EReference getEReference(TreeRootNode treeRootNode)
	{
		if(treeRootNode != null)
		{
			AbstractFeatureNode abstractFeatureNode = getLeafRecursive(treeRootNode);
			if(abstractFeatureNode instanceof AbstractFeatureSpecifier)
			{
				AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) abstractFeatureNode;
				if(abstractFeatureSpecifier.getStructuralFeature() instanceof EReference)
				{
					return (EReference) abstractFeatureSpecifier.getStructuralFeature();					
				}			
			}			
		}
		
		return null;
	}
	
	private AbstractFeatureNode getLeafRecursive(AbstractFeatureNode root)
	{
		if(root instanceof AbstractFeatureTreeNode)
		{
			AbstractFeatureTreeNode abstractFeatureTreeNode = (AbstractFeatureTreeNode) root;
			if(abstractFeatureTreeNode.getChildren().size() == 0)
			{
				return abstractFeatureTreeNode;
			}
			else
			{
				return getLeafRecursive(abstractFeatureTreeNode.getChildren().get(0));
			}
		}
		else 
		{
			return root;
		}
	}
	
	private class CustomAdapterFactoryLabelProvider extends AdapterFactoryLabelProvider
	{
		public CustomAdapterFactoryLabelProvider(AdapterFactory adapterFactory) 
		{
			super(adapterFactory);		
		}
		
		@Override
		public Image getImage(Object object) 
		{
			if(typeMember.getMemberType().getInterfaceClass().getInstanceClass() != null)
			{			
				if(object instanceof AbstractFeatureSpecifier)
				{
					AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) object;
					
					if(isCompatibleWithFeatureSuperClass(abstractFeatureSpecifier))
					{
						return super.getImage(object);
					}
					else
					{
						return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE);
					}
				}				
			}
			return super.getImage(object);
		}		
	}		
	
	@SuppressWarnings({ "rawtypes" })
	private boolean isCompatibleWithFeatureSuperClass(AbstractFeatureSpecifier abstractFeatureSpecifier)
	{		
		if(abstractFeatureSpecifier != null && abstractFeatureSpecifier.getStructuralFeature() != null)
		{
			EStructuralFeature eStructuralFeature = abstractFeatureSpecifier.getStructuralFeature();
			if(eStructuralFeature instanceof EReference)
			{
				EReference eReference = (EReference) eStructuralFeature;
				Class referrencedClass = eReference.getEType().getInstanceClass();
											
				return typeMember.getMemberType().getInterfaceClass().getInstanceClass().isAssignableFrom(referrencedClass);
			}						
		}
		return false;
	}
}
