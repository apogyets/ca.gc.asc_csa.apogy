/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;

import javax.vecmath.Color3f;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Airspace World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 *  An AbstractWorldWindLayer representing volume in the atmosphere.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getLowerAltitude <em>Lower Altitude</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getUpperAltitude <em>Upper Altitude</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getGeographicCoordinatesList <em>Geographic Coordinates List</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getUrl <em>Url</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getColor <em>Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getOpacity <em>Opacity</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer()
 * @model
 * @generated
 */
public interface AirspaceWorldWindLayer extends AbstractWorldWindLayer {
	/**
	 * Returns the value of the '<em><b>Lower Altitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The lower altitude limit, relative to Mean Sea Level. A negative value has the airspace follow the ground.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Lower Altitude</em>' attribute.
	 * @see #setLowerAltitude(double)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer_LowerAltitude()
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	double getLowerAltitude();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getLowerAltitude <em>Lower Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Altitude</em>' attribute.
	 * @see #getLowerAltitude()
	 * @generated
	 */
	void setLowerAltitude(double value);

	/**
	 * Returns the value of the '<em><b>Upper Altitude</b></em>' attribute.
	 * The default value is <code>"10000"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The upper altitude limit, relative to Mean Sea Level.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Upper Altitude</em>' attribute.
	 * @see #setUpperAltitude(double)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer_UpperAltitude()
	 * @model default="10000" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='m'"
	 * @generated
	 */
	double getUpperAltitude();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getUpperAltitude <em>Upper Altitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Altitude</em>' attribute.
	 * @see #getUpperAltitude()
	 * @generated
	 */
	void setUpperAltitude(double value);

	/**
	 * Returns the value of the '<em><b>Geographic Coordinates List</b></em>' containment reference list.
	 * The list contents are of type {@link ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The list of geographical coordinates representing the outline of the Airspace.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Geographic Coordinates List</em>' containment reference list.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer_GeographicCoordinatesList()
	 * @model containment="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true'"
	 * @generated
	 */
	EList<GeographicCoordinates> getGeographicCoordinatesList();

	/**
	 * Returns the value of the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * URL to a CSV formated file that contains the geographical coordinates defining the airspace contours.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Url</em>' attribute.
	 * @see #setUrl(String)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer_Url()
	 * @model unique="false"
	 * @generated
	 */
	String getUrl();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getUrl <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url</em>' attribute.
	 * @see #getUrl()
	 * @generated
	 */
	void setUrl(String value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * The default value is <code>"0.0,1.0,0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The color of the airspace.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(Color3f)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer_Color()
	 * @model default="0.0,1.0,0.0" unique="false" dataType="ca.gc.asc_csa.apogy.core.environment.earth.ui.Color3f"
	 * @generated
	 */
	Color3f getColor();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(Color3f value);

	/**
	 * Returns the value of the '<em><b>Opacity</b></em>' attribute.
	 * The default value is <code>"1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The opacity. 1 if fully opaque, 0 is fully transparent.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Opacity</em>' attribute.
	 * @see #setOpacity(double)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getAirspaceWorldWindLayer_Opacity()
	 * @model default="1.0" unique="false"
	 * @generated
	 */
	double getOpacity();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer#getOpacity <em>Opacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opacity</em>' attribute.
	 * @see #getOpacity()
	 * @generated
	 */
	void setOpacity(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Returns whether or not a specified GeographicCoordinates is inside the Airspace.
	 * @param coordinates The GeographicCoordinates.
	 * @return True if the coordinates are inside, false otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" coordinatesUnique="false"
	 * @generated
	 */
	boolean isCoordinatesInside(GeographicCoordinates coordinates);

} // AirspaceWorldWindLayer
