/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.*;
import gov.nasa.worldwind.WorldWindow;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.jface.viewers.ISelection;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesPathWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.GeographicCoordinatesWorldWindLayer;
import gov.nasa.worldwind.layers.RenderableLayer;
import javax.vecmath.Color3f;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthEnvironmentUIFactoryImpl extends EFactoryImpl implements ApogyEarthEnvironmentUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyEarthEnvironmentUIFactory init() {
		try {
			ApogyEarthEnvironmentUIFactory theApogyEarthEnvironmentUIFactory = (ApogyEarthEnvironmentUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyEarthEnvironmentUIPackage.eNS_URI);
			if (theApogyEarthEnvironmentUIFactory != null) {
				return theApogyEarthEnvironmentUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyEarthEnvironmentUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthEnvironmentUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyEarthEnvironmentUIPackage.EARTH_UI_FACADE: return createEarthUIFacade();
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_LIST: return createEarthViewConfigurationList();
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION: return createEarthViewConfiguration();
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_REFERENCE: return createEarthViewConfigurationReference();
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_WORLD_WIND_LAYER: return createGeographicCoordinatesWorldWindLayer();
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER: return createEarthSurfaceLocationWorldWindLayer();
			case ApogyEarthEnvironmentUIPackage.GEOGRAPHIC_COORDINATES_PATH_WORLD_WIND_LAYER: return createGeographicCoordinatesPathWorldWindLayer();
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER: return createAirspaceWorldWindLayer();
			case ApogyEarthEnvironmentUIPackage.ABSTRACT_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: return createAbstractWorldWindLayerWizardPagesProvider();
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER_WIZARD_PAGES_PROVIDER: return createEarthSurfaceLocationWorldWindLayerWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyEarthEnvironmentUIPackage.COLOR3F:
				return createColor3fFromString(eDataType, initialValue);
			case ApogyEarthEnvironmentUIPackage.RENDERABLE_LAYER:
				return createRenderableLayerFromString(eDataType, initialValue);
			case ApogyEarthEnvironmentUIPackage.ISELECTION:
				return createISelectionFromString(eDataType, initialValue);
			case ApogyEarthEnvironmentUIPackage.WORLD_WINDOW:
				return createWorldWindowFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyEarthEnvironmentUIPackage.COLOR3F:
				return convertColor3fToString(eDataType, instanceValue);
			case ApogyEarthEnvironmentUIPackage.RENDERABLE_LAYER:
				return convertRenderableLayerToString(eDataType, instanceValue);
			case ApogyEarthEnvironmentUIPackage.ISELECTION:
				return convertISelectionToString(eDataType, instanceValue);
			case ApogyEarthEnvironmentUIPackage.WORLD_WINDOW:
				return convertWorldWindowToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthUIFacade createEarthUIFacade() {
		EarthUIFacadeImpl earthUIFacade = new EarthUIFacadeImpl();
		return earthUIFacade;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthViewConfigurationList createEarthViewConfigurationList() {
		EarthViewConfigurationListImpl earthViewConfigurationList = new EarthViewConfigurationListImpl();
		return earthViewConfigurationList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthViewConfiguration createEarthViewConfiguration() {
		EarthViewConfigurationImpl earthViewConfiguration = new EarthViewConfigurationImpl();
		return earthViewConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthViewConfigurationReference createEarthViewConfigurationReference() {
		EarthViewConfigurationReferenceImpl earthViewConfigurationReference = new EarthViewConfigurationReferenceImpl();
		return earthViewConfigurationReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeographicCoordinatesWorldWindLayer createGeographicCoordinatesWorldWindLayer() {
		GeographicCoordinatesWorldWindLayerImpl geographicCoordinatesWorldWindLayer = new GeographicCoordinatesWorldWindLayerImpl();
		return geographicCoordinatesWorldWindLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthSurfaceLocationWorldWindLayer createEarthSurfaceLocationWorldWindLayer() {
		EarthSurfaceLocationWorldWindLayerImpl earthSurfaceLocationWorldWindLayer = new EarthSurfaceLocationWorldWindLayerImpl();
		return earthSurfaceLocationWorldWindLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeographicCoordinatesPathWorldWindLayer createGeographicCoordinatesPathWorldWindLayer() {
		GeographicCoordinatesPathWorldWindLayerImpl geographicCoordinatesPathWorldWindLayer = new GeographicCoordinatesPathWorldWindLayerImpl();
		return geographicCoordinatesPathWorldWindLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AirspaceWorldWindLayer createAirspaceWorldWindLayer() {
		AirspaceWorldWindLayerImpl airspaceWorldWindLayer = new AirspaceWorldWindLayerImpl();
		return airspaceWorldWindLayer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractWorldWindLayerWizardPagesProvider createAbstractWorldWindLayerWizardPagesProvider() {
		AbstractWorldWindLayerWizardPagesProviderImpl abstractWorldWindLayerWizardPagesProvider = new AbstractWorldWindLayerWizardPagesProviderImpl();
		return abstractWorldWindLayerWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthSurfaceLocationWorldWindLayerWizardPagesProvider createEarthSurfaceLocationWorldWindLayerWizardPagesProvider() {
		EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl earthSurfaceLocationWorldWindLayerWizardPagesProvider = new EarthSurfaceLocationWorldWindLayerWizardPagesProviderImpl();
		return earthSurfaceLocationWorldWindLayerWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public Color3f createColor3fFromString(EDataType eDataType, String initialValue) {
		  Color3f color3f = new Color3f();
		  
		  String[] values = initialValue.split(",");
		  
		  color3f.x = Float.parseFloat(values[0]);
		  color3f.y = Float.parseFloat(values[1]);
		  color3f.z = Float.parseFloat(values[2]);

		  return color3f;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public String convertColor3fToString(EDataType eDataType, Object instanceValue) {
		  Color3f color3f = (Color3f) instanceValue;		 
		  String string = color3f.x + "," + color3f.y + "," + color3f.z;		  
		  return string;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RenderableLayer createRenderableLayerFromString(EDataType eDataType, String initialValue) {
		return (RenderableLayer)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRenderableLayerToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ISelection createISelectionFromString(EDataType eDataType, String initialValue) {
		return (ISelection)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertISelectionToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WorldWindow createWorldWindowFromString(EDataType eDataType, String initialValue) {
		return (WorldWindow)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertWorldWindowToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthEnvironmentUIPackage getApogyEarthEnvironmentUIPackage() {
		return (ApogyEarthEnvironmentUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyEarthEnvironmentUIPackage getPackage() {
		return ApogyEarthEnvironmentUIPackage.eINSTANCE;
	}

} //ApogyEarthEnvironmentUIFactoryImpl
