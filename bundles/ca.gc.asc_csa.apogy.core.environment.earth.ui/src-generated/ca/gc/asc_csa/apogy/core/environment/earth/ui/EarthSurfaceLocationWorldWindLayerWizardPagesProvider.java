/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Surface Location World Wind Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * WizardPagesProvider used to create EarthSurfaceLocationWorldWindLayer.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage#getEarthSurfaceLocationWorldWindLayerWizardPagesProvider()
 * @model
 * @generated
 */
public interface EarthSurfaceLocationWorldWindLayerWizardPagesProvider extends AbstractWorldWindLayerWizardPagesProvider {
} // EarthSurfaceLocationWorldWindLayerWizardPagesProvider
