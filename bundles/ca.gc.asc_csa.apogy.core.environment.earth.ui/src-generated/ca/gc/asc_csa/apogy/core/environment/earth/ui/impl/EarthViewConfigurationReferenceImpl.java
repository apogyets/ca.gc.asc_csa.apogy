/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationReference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth View Configuration Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthViewConfigurationReferenceImpl#getEarthViewConfiguration <em>Earth View Configuration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthViewConfigurationReferenceImpl extends MinimalEObjectImpl.Container implements EarthViewConfigurationReference {
	/**
	 * The cached value of the '{@link #getEarthViewConfiguration() <em>Earth View Configuration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarthViewConfiguration()
	 * @generated
	 * @ordered
	 */
	protected EarthViewConfiguration earthViewConfiguration;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthViewConfigurationReferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.EARTH_VIEW_CONFIGURATION_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthViewConfiguration getEarthViewConfiguration() {
		if (earthViewConfiguration != null && earthViewConfiguration.eIsProxy()) {
			InternalEObject oldEarthViewConfiguration = (InternalEObject)earthViewConfiguration;
			earthViewConfiguration = (EarthViewConfiguration)eResolveProxy(oldEarthViewConfiguration);
			if (earthViewConfiguration != oldEarthViewConfiguration) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION, oldEarthViewConfiguration, earthViewConfiguration));
			}
		}
		return earthViewConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthViewConfiguration basicGetEarthViewConfiguration() {
		return earthViewConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarthViewConfiguration(EarthViewConfiguration newEarthViewConfiguration) {
		EarthViewConfiguration oldEarthViewConfiguration = earthViewConfiguration;
		earthViewConfiguration = newEarthViewConfiguration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION, oldEarthViewConfiguration, earthViewConfiguration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION:
				if (resolve) return getEarthViewConfiguration();
				return basicGetEarthViewConfiguration();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION:
				setEarthViewConfiguration((EarthViewConfiguration)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION:
				setEarthViewConfiguration((EarthViewConfiguration)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_VIEW_CONFIGURATION_REFERENCE__EARTH_VIEW_CONFIGURATION:
				return earthViewConfiguration != null;
		}
		return super.eIsSet(featureID);
	}

} //EarthViewConfigurationReferenceImpl
