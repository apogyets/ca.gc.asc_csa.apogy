/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.impl;

import java.awt.Color;
import java.awt.Font;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthSurfaceLocation;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthSurfaceLocationWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.utils.MultiEObjectsAdapter;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.AnnotationAttributes;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.GlobeAnnotation;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.SurfaceCircle;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Surface Location World Wind Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerImpl#getEarthSurfaceLocation <em>Earth Surface Location</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.ui.impl.EarthSurfaceLocationWorldWindLayerImpl#getTargetRadius <em>Target Radius</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthSurfaceLocationWorldWindLayerImpl extends AbstractWorldWindLayerImpl implements EarthSurfaceLocationWorldWindLayer 
{
	private MultiEObjectsAdapter earthSurfaceLocationAdapter= null;
	
	/**
	 * The cached value of the '{@link #getEarthSurfaceLocation() <em>Earth Surface Location</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarthSurfaceLocation()
	 * @generated
	 * @ordered
	 */
	protected EarthSurfaceLocation earthSurfaceLocation;

	/**
	 * The default value of the '{@link #getTargetRadius() <em>Target Radius</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetRadius()
	 * @generated
	 * @ordered
	 */
	protected static final double TARGET_RADIUS_EDEFAULT = 50.0;

	/**
	 * The cached value of the '{@link #getTargetRadius() <em>Target Radius</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetRadius()
	 * @generated
	 * @ordered
	 */
	protected double targetRadius = TARGET_RADIUS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	protected EarthSurfaceLocationWorldWindLayerImpl() 
	{
		super();
		eAdapters().add(getEarthSurfaceLocationAdapter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentUIPackage.Literals.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthSurfaceLocation getEarthSurfaceLocation() {
		return earthSurfaceLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEarthSurfaceLocation(EarthSurfaceLocation newEarthSurfaceLocation, NotificationChain msgs) {
		EarthSurfaceLocation oldEarthSurfaceLocation = earthSurfaceLocation;
		earthSurfaceLocation = newEarthSurfaceLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION, oldEarthSurfaceLocation, newEarthSurfaceLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setEarthSurfaceLocation(EarthSurfaceLocation newEarthSurfaceLocation)
	{
		if(getEarthSurfaceLocation() != null)
		{
			getEarthSurfaceLocation().eAdapters().remove(getEarthSurfaceLocationAdapter());
		}
		
		setEarthSurfaceLocationGen(newEarthSurfaceLocation);
		
		if(newEarthSurfaceLocation != null)
		{
			newEarthSurfaceLocation.eAdapters().add(getEarthSurfaceLocationAdapter());
		}
		
		updateRenderableLayer();		
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarthSurfaceLocationGen(EarthSurfaceLocation newEarthSurfaceLocation) {
		if (newEarthSurfaceLocation != earthSurfaceLocation) {
			NotificationChain msgs = null;
			if (earthSurfaceLocation != null)
				msgs = ((InternalEObject)earthSurfaceLocation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION, null, msgs);
			if (newEarthSurfaceLocation != null)
				msgs = ((InternalEObject)newEarthSurfaceLocation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION, null, msgs);
			msgs = basicSetEarthSurfaceLocation(newEarthSurfaceLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION, newEarthSurfaceLocation, newEarthSurfaceLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTargetRadius() {
		return targetRadius;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setTargetRadius(double newTargetRadius)
	{		
		setTargetRadiusGen(newTargetRadius);
		
		if(isAutoUpdateEnabled())
		{
			try 
			{
				update();
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetRadiusGen(double newTargetRadius) {
		double oldTargetRadius = targetRadius;
		targetRadius = newTargetRadius;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS, oldTargetRadius, targetRadius));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION:
				return basicSetEarthSurfaceLocation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	@Override
	public boolean getDefaultAutoUpdateEnabled() {
		return true;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION:
				return getEarthSurfaceLocation();
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS:
				return getTargetRadius();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION:
				setEarthSurfaceLocation((EarthSurfaceLocation)newValue);
				return;
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS:
				setTargetRadius((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION:
				setEarthSurfaceLocation((EarthSurfaceLocation)null);
				return;
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS:
				setTargetRadius(TARGET_RADIUS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__EARTH_SURFACE_LOCATION:
				return earthSurfaceLocation != null;
			case ApogyEarthEnvironmentUIPackage.EARTH_SURFACE_LOCATION_WORLD_WIND_LAYER__TARGET_RADIUS:
				return targetRadius != TARGET_RADIUS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (targetRadius: ");
		result.append(targetRadius);
		result.append(')');
		return result.toString();
	}

	@Override
	public void dispose() 
	{
		// Unregister listeners.
		if(getEarthSurfaceLocation() != null) getEarthSurfaceLocation().eAdapters().remove(getEarthSurfaceLocationAdapter());
		getEarthSurfaceLocationAdapter().unregisterFromAllObjects();
					
		super.dispose();
	}
	
	@Override
	protected void updateRenderableLayer() 
	{
		if(!isUpdating())
		{
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, true, true);
			
			RenderableLayer layer = getRenderableLayer();
			layer.removeAllRenderables();
			
			if(isVisible() && !isDisposed())
			{
				addRenderable(layer);								
			}
			getRenderableLayer().firePropertyChange(AVKey.LAYER, null, this);
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyCorePackage.Literals.UPDATABLE__UPDATING, false, true);		
		}
	}

	protected void addRenderable(RenderableLayer layer)
	{
		try
		{
			if(getEarthSurfaceLocation() != null)
			{
				GeographicCoordinates coord = getEarthSurfaceLocation();
				
				Angle latitude = Angle.fromRadiansLatitude(coord.getLatitude());
		        Angle longitude = Angle.fromRadiansLongitude(coord.getLongitude());
		        double elevation = coord.getElevation();
		        Position position = new Position(latitude, longitude, elevation);
		        
		        BasicShapeAttributes attributes = new BasicShapeAttributes();
		        attributes.setDrawInterior(true);
		        
		        Material mat = new Material(Color.CYAN);
		        attributes.setInteriorMaterial(mat);
		        SurfaceCircle surfaceCircle = new SurfaceCircle(attributes, position, getTargetRadius() * 1000.0, 24);	    
		        surfaceCircle.setVisible(true);
		        layer.addRenderable(surfaceCircle);
		        
		        // Adds the name as text
		        if(getEarthSurfaceLocation().getName() != null)
		        {
		        	// Creates an annotation.
		            GlobeAnnotation annotation = new GlobeAnnotation(getEarthSurfaceLocation().getName(), position);
		            
		            AnnotationAttributes annotationAttributes = new AnnotationAttributes();
		            annotationAttributes.setCornerRadius(0);
		            annotationAttributes.setVisible(true);
		            
		            Font font = annotationAttributes.getFont();	               
		            font = font.deriveFont(Font.BOLD, 16.0f);
		            annotationAttributes.setFont(font);
		            
		            Color transparent = new Color(0, 0f, 1f, 0.3f);
		            annotationAttributes.setBackgroundColor(transparent);
		            annotationAttributes.setTextColor(Color.YELLOW);		            
		            
		            annotation.setAttributes(annotationAttributes);
		            layer.addRenderable(annotation);
		        }
			}		
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
	private MultiEObjectsAdapter getEarthSurfaceLocationAdapter() 
	{
		if(earthSurfaceLocationAdapter == null)
		{
			earthSurfaceLocationAdapter = new MultiEObjectsAdapter()
			{
				@Override
				public void registerToEObject(EObject eObject) 
				{
					if(eObject instanceof EarthSurfaceLocation)
					{
						EarthSurfaceLocation newEarthSurfaceLocation = (EarthSurfaceLocation) eObject;
						newEarthSurfaceLocation.eAdapters().add(getEarthSurfaceLocationAdapter());						
					}
					else
					{
						super.registerToEObject(eObject);
					}
				}
				
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof EarthSurfaceLocation)
					{
						int featureId = msg.getFeatureID(EarthSurfaceLocation.class);
						switch (featureId) 
						{
							case ApogyEarthEnvironmentPackage.EARTH_SURFACE_LOCATION__NAME:
							case ApogyEarthEnvironmentPackage.EARTH_SURFACE_LOCATION__ELEVATION:
							case ApogyEarthEnvironmentPackage.EARTH_SURFACE_LOCATION__LATITUDE:
							case ApogyEarthEnvironmentPackage.EARTH_SURFACE_LOCATION__LONGITUDE:							
								if(isAutoUpdateEnabled()) updateRenderableLayer();								
							break;

							default:
							break;
						}
					}
					else if(msg.getNotifier() instanceof GeographicCoordinates)
					{
						if(isAutoUpdateEnabled()) updateRenderableLayer();
					}
				}				
			};
		}
		return earthSurfaceLocationAdapter;
	}
} //EarthSurfaceLocationWorldWindLayerImpl
