/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui;

public class ApogyEarthEnvironmentUIRCPConstants 
{
	/**
	 * Camera view Part
	 */
	public static final String PART__EARTH_VIEW__ID = "ca.gc.asc_csa.apogy.core.environment.earth.ui.earthview";
	public static final String TOOL_BAR__EARTH_VIEW__ID = "ca.gc.asc_csa.apogy.core.environment.earth.ui.toolbar.earthView";
	
	/** ToolItems */
	public static final String HANDLED_TOOL_ITEM__ADD_EARTH_VIEW_CONFIGURATION__ID = "ca.gc.asc_csa.apogy.core.environment.earth.ui.handledtoolitem.addaearthviewconfiguration";
	public static final String HANDLED_TOOL_ITEM__DELETE_EARTH_VIEW_CONFIGURATION__ID = "ca.gc.asc_csa.apogy.core.environment.earth.ui.handledtoolitem.deleteaearthviewconfiguration";

	
	/**
	 * Commands
	 */
	public static final String COMMAND__UPDATE_EARTH_VIEW_TOOLBAR__ID = "ca.gc.asc_csa.apogy.core.environment.earth.ui.command.updateearthviewtoolbar";

}
