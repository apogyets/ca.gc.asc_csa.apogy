/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.utils;


import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Color3f;

import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.Polyline;

public class WorldWindUtils 
{
	
	public static gov.nasa.worldwind.geom.Position convertToPosition(GeographicCoordinates geographicCoordinates)
	{
		// Create latitude, longitude and elevation values.
        Angle latitude = Angle.fromRadiansLatitude(geographicCoordinates.getLatitude());
        Angle longitude = Angle.fromRadiansLongitude(geographicCoordinates.getLongitude());
        double elevation = geographicCoordinates.getElevation();
        	            
        // Creates and adds a position in the linked list.
        Position position = new Position(latitude, longitude, elevation);
        
        return position;
	}
	
	public static List<Polyline> createPolyLineFromGeographicCoordinatesListNoWrapAround(List<GeographicCoordinates> coordinatesList)
	{
		List<Polyline> polylines = new ArrayList<Polyline>();
		LinkedList<Position> positions = new LinkedList<Position>();
		
		for(GeographicCoordinates coord : coordinatesList)
		{
			// Creates and adds a position in the linked list.
            Position position = convertToPosition(coord);
            
            if(positions.size() > 0)
			{
				// If we are going from -180 to + 180
				double currentLongitude = position.longitude.getDegrees();
				double lastLongitude = positions.getLast().longitude.getDegrees();
				
				// If there is a change of sign.
				if(Math.signum(currentLongitude) != Math.signum(lastLongitude))					  
				{
					// If the change of sign does not happen around zero.
					if(Math.abs(currentLongitude) > 90 && (Math.abs(lastLongitude) > 90))
					{
						// Create a new line segment with the current position.
						Polyline polyline = new Polyline(positions);
						polylines.add(polyline);
						
						// Clears the position and start a new line.
						positions.clear();													
					}
				}

			}			
			positions.add(position);
		}
		
		// Adds the last line being processed.
		if(!positions.isEmpty())
		{
			Polyline polyline = new Polyline(positions);
			polylines.add(polyline);
		}
		
		return polylines;
	}
	
	public static Color convertFrom(Color3f color3f)
	{
		return new Color(color3f.x, color3f.y, color3f.z);
		
	}
}
