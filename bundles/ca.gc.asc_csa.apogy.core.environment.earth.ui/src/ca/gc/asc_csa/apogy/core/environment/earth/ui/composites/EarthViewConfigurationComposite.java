package ca.gc.asc_csa.apogy.core.environment.earth.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.AbstractWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfiguration;

public class EarthViewConfigurationComposite extends Composite 
{
	private EarthViewConfiguration earthViewConfiguration;

	private AbstractWorldWindLayerListComposite abstractWorldWindLayerListComposite;
	private AbstractWorldWindLayerOverviewComposite abstractWorldWindLayerOverviewComposite;
	private AbstractWorldWindLayerDetailsComposite abstractWorldWindLayerDetailsComposite;
	

	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public EarthViewConfigurationComposite(Composite parent, int style) 
	{
		super(parent, style);

		setLayout(new GridLayout(1, true));
		
		ScrolledForm scrldfrm = formToolkit.createScrolledForm(this);
		scrldfrm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrm);
		scrldfrm.setText("");
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			tableWrapLayout.makeColumnsEqualWidth = false;			
			scrldfrm.getBody().setLayout(tableWrapLayout);
		}
		
		// Layers
		Section sctnLayerList = formToolkit.createSection(scrldfrm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnLayerList = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 3, 1);
		twd_sctnLayerList.valign = TableWrapData.FILL;		
		twd_sctnLayerList.grabVertical = true;
		twd_sctnLayerList.grabHorizontal = true;
		sctnLayerList.setLayoutData(twd_sctnLayerList);
		formToolkit.paintBordersFor(sctnLayerList);
		sctnLayerList.setText("Earth View Layers");
		
		abstractWorldWindLayerListComposite = new AbstractWorldWindLayerListComposite(sctnLayerList, SWT.NONE)
		{
			@Override
			protected void newAbstractWorldWindLayerSelected(AbstractWorldWindLayer newAbstractWorldWindLayer) 
			{	
				abstractWorldWindLayerOverviewComposite.setAbstractWorldWindLayer(newAbstractWorldWindLayer);	
				abstractWorldWindLayerDetailsComposite.setAbstractWorldWindLayer(newAbstractWorldWindLayer);
			}
		};						
		
		formToolkit.adapt(abstractWorldWindLayerListComposite);
		formToolkit.paintBordersFor(abstractWorldWindLayerListComposite);
		sctnLayerList.setClient(abstractWorldWindLayerListComposite);
		
		// Layer Overview.
		Section sctnLayerOverview = formToolkit.createSection(scrldfrm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);		
		TableWrapData twd_sctnLayerOverview = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnLayerOverview.valign = TableWrapData.FILL;
		twd_sctnLayerOverview.grabVertical = true;
		twd_sctnLayerOverview.grabHorizontal = true;
		sctnLayerOverview.setLayoutData(twd_sctnLayerOverview);		
		formToolkit.paintBordersFor(sctnLayerOverview);
		sctnLayerOverview.setText("Layer Overview");
		
		abstractWorldWindLayerOverviewComposite = new AbstractWorldWindLayerOverviewComposite(sctnLayerOverview, SWT.NONE);
		formToolkit.adapt(abstractWorldWindLayerOverviewComposite);
		formToolkit.paintBordersFor(abstractWorldWindLayerOverviewComposite);
		sctnLayerOverview.setClient(abstractWorldWindLayerOverviewComposite);	
		
		// Layer Details
		Section sctnLayerDetails = formToolkit.createSection(scrldfrm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);		
		TableWrapData twd_sctnLayerDetails = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnLayerDetails.valign = TableWrapData.FILL;
		twd_sctnLayerDetails.grabVertical = true;
		twd_sctnLayerDetails.grabHorizontal = true;
		sctnLayerDetails.setLayoutData(twd_sctnLayerDetails);		
		formToolkit.paintBordersFor(sctnLayerDetails);
		sctnLayerDetails.setText("Layer Details");
		
		abstractWorldWindLayerDetailsComposite = new AbstractWorldWindLayerDetailsComposite(sctnLayerDetails, SWT.NONE);
		formToolkit.adapt(abstractWorldWindLayerDetailsComposite);
		formToolkit.paintBordersFor(abstractWorldWindLayerDetailsComposite);
		sctnLayerDetails.setClient(abstractWorldWindLayerDetailsComposite);		
	}

	public EarthViewConfiguration getEarthViewConfiguration() {
		return earthViewConfiguration;
	}

	public void setEarthViewConfiguration(EarthViewConfiguration earthViewConfiguration) 
	{
		this.earthViewConfiguration = earthViewConfiguration;
		
		abstractWorldWindLayerListComposite.setEarthViewConfiguration(earthViewConfiguration);
		abstractWorldWindLayerOverviewComposite.setAbstractWorldWindLayer(null);
		abstractWorldWindLayerDetailsComposite.setAbstractWorldWindLayer(null);
	}
}
