/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthUIFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.EarthViewConfigurationList;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.parts.EarthViewPart;

public class AddEarthViewConfigurationHandler {

	@CanExecute
	public boolean canExecute(MPart part)
	{
		if (part.getObject() instanceof EarthViewPart)
		{
			EarthViewConfigurationList cameraViewConfigurationList = EarthUIFacade.INSTANCE.getActiveEarthViewConfigurationList();
			return cameraViewConfigurationList != null;
		}
		return false;
	}
	
	@Execute
	public void execute(MPart part, final MToolItem item)
	{
		if (part.getObject() instanceof EarthViewPart)
		{			
			EarthViewConfigurationList cameraViewConfigurationList = EarthUIFacade.INSTANCE.getActiveEarthViewConfigurationList();
			if(cameraViewConfigurationList != null)
			{
				NamedSetting namedSettings = ApogyCommonEMFUIFactory.eINSTANCE.createNamedSetting();
				namedSettings.setParent(cameraViewConfigurationList);
				namedSettings.setContainingFeature(ApogyEarthEnvironmentUIPackage.Literals.EARTH_VIEW_CONFIGURATION_LIST__EARTH_VIEW_CONFIGURATIONS);
				ApogyEObjectWizard wizard = new ApogyEObjectWizard(ApogyEarthEnvironmentUIPackage.Literals.EARTH_VIEW_CONFIGURATION_LIST__EARTH_VIEW_CONFIGURATIONS,
												cameraViewConfigurationList, namedSettings, null) 
				{
					@Override
					public boolean performFinish() 
					{
						boolean value = super.performFinish();					
						return value;
					}
				};
				WizardDialog dialog = new WizardDialog(Display.getDefault().getActiveShell(), wizard);
				dialog.open();				
			}			
		}		
	}
}