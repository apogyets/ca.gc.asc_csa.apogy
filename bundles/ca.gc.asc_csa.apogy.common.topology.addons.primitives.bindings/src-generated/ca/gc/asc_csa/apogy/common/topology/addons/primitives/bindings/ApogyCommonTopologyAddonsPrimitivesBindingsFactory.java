/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage
 * @generated
 */
public interface ApogyCommonTopologyAddonsPrimitivesBindingsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCommonTopologyAddonsPrimitivesBindingsFactory eINSTANCE = ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.impl.ApogyCommonTopologyAddonsPrimitivesBindingsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Light Enablement Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Light Enablement Binding</em>'.
	 * @generated
	 */
	LightEnablementBinding createLightEnablementBinding();

	/**
	 * Returns a new object of class '<em>Point Light Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Point Light Binding</em>'.
	 * @generated
	 */
	PointLightBinding createPointLightBinding();

	/**
	 * Returns a new object of class '<em>Spot Light Binding</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Spot Light Binding</em>'.
	 * @generated
	 */
	SpotLightBinding createSpotLightBinding();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyCommonTopologyAddonsPrimitivesBindingsPackage getApogyCommonTopologyAddonsPrimitivesBindingsPackage();

} //ApogyCommonTopologyAddonsPrimitivesBindingsFactory
