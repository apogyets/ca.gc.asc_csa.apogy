/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.SpotLight;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Spot Light Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Binding that binds all parameters of a SpotLight to another SpotLight.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding#getSpotLight <em>Spot Light</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage#getSpotLightBinding()
 * @model
 * @generated
 */
public interface SpotLightBinding extends AbstractTopologyBinding {
	/**
	 * Returns the value of the '<em><b>Spot Light</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Spot Light</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The Spot Light being controlled by the binding.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Spot Light</em>' reference.
	 * @see #setSpotLight(SpotLight)
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage#getSpotLightBinding_SpotLight()
	 * @model
	 * @generated
	 */
	SpotLight getSpotLight();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.SpotLightBinding#getSpotLight <em>Spot Light</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Spot Light</em>' reference.
	 * @see #getSpotLight()
	 * @generated
	 */
	void setSpotLight(SpotLight value);

} // SpotLightBinding
