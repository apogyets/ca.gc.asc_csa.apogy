package ca.gc.asc_csa.apogy.addons.ui.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.addons.ui.*;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.AbstractTwoPoints3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.addons.ui.FeatureOfInterestPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Ruler3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Ruler3dToolNodePresentation;
import ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolNodePresentation;
import ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage
 * @generated
 */
public class ApogyAddonsUISwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected static ApogyAddonsUIPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyAddonsUISwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyAddonsUIPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyAddonsUIPackage.RULER3D_TOOL_NODE_PRESENTATION: {
				Ruler3dToolNodePresentation ruler3dToolNodePresentation = (Ruler3dToolNodePresentation)theEObject;
				T result = caseRuler3dToolNodePresentation(ruler3dToolNodePresentation);
				if (result == null) result = caseNodePresentation(ruler3dToolNodePresentation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.TRAJECTORY3_DTOOL_NODE_PRESENTATION: {
				Trajectory3DToolNodePresentation trajectory3DToolNodePresentation = (Trajectory3DToolNodePresentation)theEObject;
				T result = caseTrajectory3DToolNodePresentation(trajectory3DToolNodePresentation);
				if (result == null) result = caseNodePresentation(trajectory3DToolNodePresentation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.ABSTRACT_TOOL_ECLASS_SETTINGS: {
				AbstractToolEClassSettings abstractToolEClassSettings = (AbstractToolEClassSettings)theEObject;
				T result = caseAbstractToolEClassSettings(abstractToolEClassSettings);
				if (result == null) result = caseMapBasedEClassSettings(abstractToolEClassSettings);
				if (result == null) result = caseEClassSettings(abstractToolEClassSettings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER: {
				AbstractToolWizardPagesProvider abstractToolWizardPagesProvider = (AbstractToolWizardPagesProvider)theEObject;
				T result = caseAbstractToolWizardPagesProvider(abstractToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(abstractToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(abstractToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.SIMPLE_TOOL_WIZARD_PAGES_PROVIDER: {
				SimpleToolWizardPagesProvider simpleToolWizardPagesProvider = (SimpleToolWizardPagesProvider)theEObject;
				T result = caseSimpleToolWizardPagesProvider(simpleToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(simpleToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(simpleToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(simpleToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER: {
				Simple3DToolWizardPagesProvider simple3DToolWizardPagesProvider = (Simple3DToolWizardPagesProvider)theEObject;
				T result = caseSimple3DToolWizardPagesProvider(simple3DToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(simple3DToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(simple3DToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(simple3DToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(simple3DToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER: {
				FeatureOfInterestPickingToolWizardPagesProvider featureOfInterestPickingToolWizardPagesProvider = (FeatureOfInterestPickingToolWizardPagesProvider)theEObject;
				T result = caseFeatureOfInterestPickingToolWizardPagesProvider(featureOfInterestPickingToolWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(featureOfInterestPickingToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(featureOfInterestPickingToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(featureOfInterestPickingToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(featureOfInterestPickingToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(featureOfInterestPickingToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER: {
				AbstractTwoPoints3DToolWizardPagesProvider abstractTwoPoints3DToolWizardPagesProvider = (AbstractTwoPoints3DToolWizardPagesProvider)theEObject;
				T result = caseAbstractTwoPoints3DToolWizardPagesProvider(abstractTwoPoints3DToolWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(abstractTwoPoints3DToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(abstractTwoPoints3DToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(abstractTwoPoints3DToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(abstractTwoPoints3DToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(abstractTwoPoints3DToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.RULER3_DTOOL_WIZARD_PAGES_PROVIDER: {
				Ruler3DToolWizardPagesProvider ruler3DToolWizardPagesProvider = (Ruler3DToolWizardPagesProvider)theEObject;
				T result = caseRuler3DToolWizardPagesProvider(ruler3DToolWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(ruler3DToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(ruler3DToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(ruler3DToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(ruler3DToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(ruler3DToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER: {
				Trajectory3DToolWizardPagesProvider trajectory3DToolWizardPagesProvider = (Trajectory3DToolWizardPagesProvider)theEObject;
				T result = caseTrajectory3DToolWizardPagesProvider(trajectory3DToolWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(trajectory3DToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(trajectory3DToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(trajectory3DToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(trajectory3DToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(trajectory3DToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER: {
				TrajectoryPickingToolWizardPagesProvider trajectoryPickingToolWizardPagesProvider = (TrajectoryPickingToolWizardPagesProvider)theEObject;
				T result = caseTrajectoryPickingToolWizardPagesProvider(trajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(trajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(trajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(trajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(trajectoryPickingToolWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(trajectoryPickingToolWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyAddonsUIPackage.ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER: {
				AbstractPickLocationToollWizardPagesProvider abstractPickLocationToollWizardPagesProvider = (AbstractPickLocationToollWizardPagesProvider)theEObject;
				T result = caseAbstractPickLocationToollWizardPagesProvider(abstractPickLocationToollWizardPagesProvider);
				if (result == null) result = caseSimple3DToolWizardPagesProvider(abstractPickLocationToollWizardPagesProvider);
				if (result == null) result = caseSimpleToolWizardPagesProvider(abstractPickLocationToollWizardPagesProvider);
				if (result == null) result = caseAbstractToolWizardPagesProvider(abstractPickLocationToollWizardPagesProvider);
				if (result == null) result = caseNamedDescribedWizardPagesProvider(abstractPickLocationToollWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(abstractPickLocationToollWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ruler3d Tool Node Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ruler3d Tool Node Presentation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuler3dToolNodePresentation(Ruler3dToolNodePresentation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trajectory3 DTool Node Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trajectory3 DTool Node Presentation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrajectory3DToolNodePresentation(Trajectory3DToolNodePresentation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tool EClass Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tool EClass Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractToolEClassSettings(AbstractToolEClassSettings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractToolWizardPagesProvider(AbstractToolWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleToolWizardPagesProvider(SimpleToolWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple3 DTool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimple3DToolWizardPagesProvider(Simple3DToolWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Of Interest Picking Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Of Interest Picking Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureOfInterestPickingToolWizardPagesProvider(FeatureOfInterestPickingToolWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Two Points3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Two Points3 DTool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractTwoPoints3DToolWizardPagesProvider(AbstractTwoPoints3DToolWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ruler3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ruler3 DTool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuler3DToolWizardPagesProvider(Ruler3DToolWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trajectory3 DTool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trajectory3 DTool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrajectory3DToolWizardPagesProvider(Trajectory3DToolWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrajectoryPickingToolWizardPagesProvider(TrajectoryPickingToolWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Pick Location Tooll Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Pick Location Tooll Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractPickLocationToollWizardPagesProvider(AbstractPickLocationToollWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Presentation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodePresentation(NodePresentation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EClass Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EClass Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEClassSettings(EClassSettings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Map Based EClass Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Map Based EClass Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMapBasedEClassSettings(MapBasedEClassSettings object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ApogyAddonsUISwitch
