package ca.gc.asc_csa.apogy.addons.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc --> * <!-- begin-model-doc -->
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca),
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyAddonsUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation' modelName='ApogyAddonsUI' complianceLevel='6.0' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.addons.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.addons.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.addons'"
 * @generated
 */
public interface ApogyAddonsUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.addons.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	ApogyAddonsUIPackage eINSTANCE = ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Ruler3dToolNodePresentationImpl <em>Ruler3d Tool Node Presentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Ruler3dToolNodePresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getRuler3dToolNodePresentation()
	 * @generated
	 */
	int RULER3D_TOOL_NODE_PRESENTATION = 0;

	/**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__NODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__NODE;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__COLOR = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__COLOR;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__VISIBLE;

	/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__SHADOW_MODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SHADOW_MODE;

	/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

	/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__ID_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ID_VISIBLE;

	/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

	/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__CENTROID = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__CENTROID;

	/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__MIN = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__MAX = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MAX;

	/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__XRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__XRANGE;

	/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__YRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__YRANGE;

	/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__ZRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ZRANGE;

	/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION__SCENE_OBJECT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SCENE_OBJECT;

	/**
	 * The number of structural features of the '<em>Ruler3d Tool Node Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ruler3d Tool Node Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3D_TOOL_NODE_PRESENTATION_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Trajectory3DToolNodePresentationImpl <em>Trajectory3 DTool Node Presentation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Trajectory3DToolNodePresentationImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getTrajectory3DToolNodePresentation()
	 * @generated
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION = 1;

	/**
	 * The feature id for the '<em><b>Topology Presentation Set</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__TOPOLOGY_PRESENTATION_SET;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__NODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__NODE;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__COLOR = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__COLOR;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__VISIBLE;

	/**
	 * The feature id for the '<em><b>Shadow Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__SHADOW_MODE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SHADOW_MODE;

	/**
	 * The feature id for the '<em><b>Use In Bounding Calculation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__USE_IN_BOUNDING_CALCULATION;

	/**
	 * The feature id for the '<em><b>Id Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__ID_VISIBLE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ID_VISIBLE;

	/**
	 * The feature id for the '<em><b>Enable Texture Projection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ENABLE_TEXTURE_PROJECTION;

	/**
	 * The feature id for the '<em><b>Centroid</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__CENTROID = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__CENTROID;

	/**
	 * The feature id for the '<em><b>Min</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__MIN = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MIN;

	/**
	 * The feature id for the '<em><b>Max</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__MAX = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__MAX;

	/**
	 * The feature id for the '<em><b>XRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__XRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__XRANGE;

	/**
	 * The feature id for the '<em><b>YRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__YRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__YRANGE;

	/**
	 * The feature id for the '<em><b>ZRange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__ZRANGE = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__ZRANGE;

	/**
	 * The feature id for the '<em><b>Scene Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION__SCENE_OBJECT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION__SCENE_OBJECT;

	/**
	 * The number of structural features of the '<em>Trajectory3 DTool Node Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION_FEATURE_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Trajectory3 DTool Node Presentation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_NODE_PRESENTATION_OPERATION_COUNT = ApogyCommonTopologyUIPackage.NODE_PRESENTATION_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.AbstractToolEClassSettingsImpl <em>Abstract Tool EClass Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.AbstractToolEClassSettingsImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getAbstractToolEClassSettings()
	 * @generated
	 */
	int ABSTRACT_TOOL_ECLASS_SETTINGS = 2;

	/**
	 * The feature id for the '<em><b>User Data Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_ECLASS_SETTINGS__USER_DATA_MAP = ApogyCommonEMFUIPackage.MAP_BASED_ECLASS_SETTINGS__USER_DATA_MAP;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_ECLASS_SETTINGS__NAME = ApogyCommonEMFUIPackage.MAP_BASED_ECLASS_SETTINGS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_ECLASS_SETTINGS__DESCRIPTION = ApogyCommonEMFUIPackage.MAP_BASED_ECLASS_SETTINGS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Abstract Tool EClass Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_ECLASS_SETTINGS_FEATURE_COUNT = ApogyCommonEMFUIPackage.MAP_BASED_ECLASS_SETTINGS_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Abstract Tool EClass Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_ECLASS_SETTINGS_OPERATION_COUNT = ApogyCommonEMFUIPackage.MAP_BASED_ECLASS_SETTINGS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.AbstractToolWizardPagesProviderImpl <em>Abstract Tool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.AbstractToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getAbstractToolWizardPagesProvider()
	 * @generated
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER = 3;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Abstract Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Abstract Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.SimpleToolWizardPagesProviderImpl <em>Simple Tool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.SimpleToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getSimpleToolWizardPagesProvider()
	 * @generated
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER = 4;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Simple Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Simple Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Simple3DToolWizardPagesProviderImpl <em>Simple3 DTool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Simple3DToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getSimple3DToolWizardPagesProvider()
	 * @generated
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER = 5;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Simple3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Simple3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = SIMPLE_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.FeatureOfInterestPickingToolWizardPagesProviderImpl <em>Feature Of Interest Picking Tool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.FeatureOfInterestPickingToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getFeatureOfInterestPickingToolWizardPagesProvider()
	 * @generated
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER = 6;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER__PAGES = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Feature Of Interest Picking Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Feature Of Interest Picking Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.AbstractTwoPoints3DToolWizardPagesProviderImpl <em>Abstract Two Points3 DTool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.AbstractTwoPoints3DToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getAbstractTwoPoints3DToolWizardPagesProvider()
	 * @generated
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER = 7;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Abstract Two Points3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Abstract Two Points3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Ruler3DToolWizardPagesProviderImpl <em>Ruler3 DTool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Ruler3DToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getRuler3DToolWizardPagesProvider()
	 * @generated
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER = 8;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Ruler3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Ruler3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int RULER3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Trajectory3DToolWizardPagesProviderImpl <em>Trajectory3 DTool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Trajectory3DToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getTrajectory3DToolWizardPagesProvider()
	 * @generated
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER = 9;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Trajectory3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Trajectory3 DTool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.TrajectoryPickingToolWizardPagesProviderImpl <em>Trajectory Picking Tool Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.TrajectoryPickingToolWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getTrajectoryPickingToolWizardPagesProvider()
	 * @generated
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER = 10;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER__PAGES = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER__EOBJECT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Trajectory Picking Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Trajectory Picking Tool Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.AbstractPickLocationToollWizardPagesProviderImpl <em>Abstract Pick Location Tooll Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.AbstractPickLocationToollWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getAbstractPickLocationToollWizardPagesProvider()
	 * @generated
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER = 11;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER__PAGES = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER__EOBJECT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Abstract Pick Location Tooll Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Abstract Pick Location Tooll Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.Ruler3dToolNodePresentation <em>Ruler3d Tool Node Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Ruler3d Tool Node Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.Ruler3dToolNodePresentation
	 * @generated
	 */
	EClass getRuler3dToolNodePresentation();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolNodePresentation <em>Trajectory3 DTool Node Presentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Trajectory3 DTool Node Presentation</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolNodePresentation
	 * @generated
	 */
	EClass getTrajectory3DToolNodePresentation();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings <em>Abstract Tool EClass Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Abstract Tool EClass Settings</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings
	 * @generated
	 */
	EClass getAbstractToolEClassSettings();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings#getName()
	 * @see #getAbstractToolEClassSettings()
	 * @generated
	 */
	EAttribute getAbstractToolEClassSettings_Name();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings#getDescription()
	 * @see #getAbstractToolEClassSettings()
	 * @generated
	 */
	EAttribute getAbstractToolEClassSettings_Description();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider <em>Abstract Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Abstract Tool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider
	 * @generated
	 */
	EClass getAbstractToolWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider <em>Simple Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Simple Tool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider
	 * @generated
	 */
	EClass getSimpleToolWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider <em>Simple3 DTool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Simple3 DTool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider
	 * @generated
	 */
	EClass getSimple3DToolWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.FeatureOfInterestPickingToolWizardPagesProvider <em>Feature Of Interest Picking Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Feature Of Interest Picking Tool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.FeatureOfInterestPickingToolWizardPagesProvider
	 * @generated
	 */
	EClass getFeatureOfInterestPickingToolWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.AbstractTwoPoints3DToolWizardPagesProvider <em>Abstract Two Points3 DTool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Abstract Two Points3 DTool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.AbstractTwoPoints3DToolWizardPagesProvider
	 * @generated
	 */
	EClass getAbstractTwoPoints3DToolWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.Ruler3DToolWizardPagesProvider <em>Ruler3 DTool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Ruler3 DTool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.Ruler3DToolWizardPagesProvider
	 * @generated
	 */
	EClass getRuler3DToolWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolWizardPagesProvider <em>Trajectory3 DTool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Trajectory3 DTool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolWizardPagesProvider
	 * @generated
	 */
	EClass getTrajectory3DToolWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider <em>Trajectory Picking Tool Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Trajectory Picking Tool Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider
	 * @generated
	 */
	EClass getTrajectoryPickingToolWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ui.AbstractPickLocationToollWizardPagesProvider <em>Abstract Pick Location Tooll Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Abstract Pick Location Tooll Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ui.AbstractPickLocationToollWizardPagesProvider
	 * @generated
	 */
	EClass getAbstractPickLocationToollWizardPagesProvider();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyAddonsUIFactory getApogyAddonsUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Ruler3dToolNodePresentationImpl <em>Ruler3d Tool Node Presentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Ruler3dToolNodePresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getRuler3dToolNodePresentation()
		 * @generated
		 */
		EClass RULER3D_TOOL_NODE_PRESENTATION = eINSTANCE.getRuler3dToolNodePresentation();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Trajectory3DToolNodePresentationImpl <em>Trajectory3 DTool Node Presentation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Trajectory3DToolNodePresentationImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getTrajectory3DToolNodePresentation()
		 * @generated
		 */
		EClass TRAJECTORY3_DTOOL_NODE_PRESENTATION = eINSTANCE.getTrajectory3DToolNodePresentation();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.AbstractToolEClassSettingsImpl <em>Abstract Tool EClass Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.AbstractToolEClassSettingsImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getAbstractToolEClassSettings()
		 * @generated
		 */
		EClass ABSTRACT_TOOL_ECLASS_SETTINGS = eINSTANCE.getAbstractToolEClassSettings();
		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute ABSTRACT_TOOL_ECLASS_SETTINGS__NAME = eINSTANCE.getAbstractToolEClassSettings_Name();
		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute ABSTRACT_TOOL_ECLASS_SETTINGS__DESCRIPTION = eINSTANCE.getAbstractToolEClassSettings_Description();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.AbstractToolWizardPagesProviderImpl <em>Abstract Tool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.AbstractToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getAbstractToolWizardPagesProvider()
		 * @generated
		 */
		EClass ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getAbstractToolWizardPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.SimpleToolWizardPagesProviderImpl <em>Simple Tool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.SimpleToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getSimpleToolWizardPagesProvider()
		 * @generated
		 */
		EClass SIMPLE_TOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getSimpleToolWizardPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Simple3DToolWizardPagesProviderImpl <em>Simple3 DTool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Simple3DToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getSimple3DToolWizardPagesProvider()
		 * @generated
		 */
		EClass SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getSimple3DToolWizardPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.FeatureOfInterestPickingToolWizardPagesProviderImpl <em>Feature Of Interest Picking Tool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.FeatureOfInterestPickingToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getFeatureOfInterestPickingToolWizardPagesProvider()
		 * @generated
		 */
		EClass FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getFeatureOfInterestPickingToolWizardPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.AbstractTwoPoints3DToolWizardPagesProviderImpl <em>Abstract Two Points3 DTool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.AbstractTwoPoints3DToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getAbstractTwoPoints3DToolWizardPagesProvider()
		 * @generated
		 */
		EClass ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getAbstractTwoPoints3DToolWizardPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Ruler3DToolWizardPagesProviderImpl <em>Ruler3 DTool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Ruler3DToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getRuler3DToolWizardPagesProvider()
		 * @generated
		 */
		EClass RULER3_DTOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getRuler3DToolWizardPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.Trajectory3DToolWizardPagesProviderImpl <em>Trajectory3 DTool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.Trajectory3DToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getTrajectory3DToolWizardPagesProvider()
		 * @generated
		 */
		EClass TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getTrajectory3DToolWizardPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.TrajectoryPickingToolWizardPagesProviderImpl <em>Trajectory Picking Tool Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.TrajectoryPickingToolWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getTrajectoryPickingToolWizardPagesProvider()
		 * @generated
		 */
		EClass TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER = eINSTANCE.getTrajectoryPickingToolWizardPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ui.impl.AbstractPickLocationToollWizardPagesProviderImpl <em>Abstract Pick Location Tooll Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.AbstractPickLocationToollWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.ui.impl.ApogyAddonsUIPackageImpl#getAbstractPickLocationToollWizardPagesProvider()
		 * @generated
		 */
		EClass ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER = eINSTANCE.getAbstractPickLocationToollWizardPagesProvider();

	}

} //ApogyAddonsUIPackage
