package ca.gc.asc_csa.apogy.addons.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.addons.ui.AbstractPickLocationToollWizardPagesProvider;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.AbstractTwoPoints3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIFactory;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage;
import ca.gc.asc_csa.apogy.addons.ui.FeatureOfInterestPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Ruler3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Ruler3dToolNodePresentation;
import ca.gc.asc_csa.apogy.addons.ui.Simple3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.SimpleToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolNodePresentation;
import ca.gc.asc_csa.apogy.addons.ui.Trajectory3DToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.ui.TrajectoryPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyAddonsUIPackageImpl extends EPackageImpl implements ApogyAddonsUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass ruler3dToolNodePresentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass trajectory3DToolNodePresentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass abstractToolEClassSettingsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass abstractToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass simpleToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass simple3DToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass featureOfInterestPickingToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass abstractTwoPoints3DToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass ruler3DToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass trajectory3DToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass trajectoryPickingToolWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass abstractPickLocationToollWizardPagesProviderEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyAddonsUIPackageImpl() {
		super(eNS_URI, ApogyAddonsUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyAddonsUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyAddonsUIPackage init() {
		if (isInited) return (ApogyAddonsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyAddonsUIPackageImpl theApogyAddonsUIPackage = (ApogyAddonsUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyAddonsUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyAddonsUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonTopologyUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyAddonsUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyAddonsUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyAddonsUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyAddonsUIPackage.eNS_URI, theApogyAddonsUIPackage);
		return theApogyAddonsUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getRuler3dToolNodePresentation() {
		return ruler3dToolNodePresentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getTrajectory3DToolNodePresentation() {
		return trajectory3DToolNodePresentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAbstractToolEClassSettings() {
		return abstractToolEClassSettingsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getAbstractToolEClassSettings_Name() {
		return (EAttribute)abstractToolEClassSettingsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getAbstractToolEClassSettings_Description() {
		return (EAttribute)abstractToolEClassSettingsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAbstractToolWizardPagesProvider() {
		return abstractToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getSimpleToolWizardPagesProvider() {
		return simpleToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getSimple3DToolWizardPagesProvider() {
		return simple3DToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getFeatureOfInterestPickingToolWizardPagesProvider() {
		return featureOfInterestPickingToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAbstractTwoPoints3DToolWizardPagesProvider() {
		return abstractTwoPoints3DToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getRuler3DToolWizardPagesProvider() {
		return ruler3DToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getTrajectory3DToolWizardPagesProvider() {
		return trajectory3DToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getTrajectoryPickingToolWizardPagesProvider() {
		return trajectoryPickingToolWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAbstractPickLocationToollWizardPagesProvider() {
		return abstractPickLocationToollWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyAddonsUIFactory getApogyAddonsUIFactory() {
		return (ApogyAddonsUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		ruler3dToolNodePresentationEClass = createEClass(RULER3D_TOOL_NODE_PRESENTATION);

		trajectory3DToolNodePresentationEClass = createEClass(TRAJECTORY3_DTOOL_NODE_PRESENTATION);

		abstractToolEClassSettingsEClass = createEClass(ABSTRACT_TOOL_ECLASS_SETTINGS);
		createEAttribute(abstractToolEClassSettingsEClass, ABSTRACT_TOOL_ECLASS_SETTINGS__NAME);
		createEAttribute(abstractToolEClassSettingsEClass, ABSTRACT_TOOL_ECLASS_SETTINGS__DESCRIPTION);

		abstractToolWizardPagesProviderEClass = createEClass(ABSTRACT_TOOL_WIZARD_PAGES_PROVIDER);

		simpleToolWizardPagesProviderEClass = createEClass(SIMPLE_TOOL_WIZARD_PAGES_PROVIDER);

		simple3DToolWizardPagesProviderEClass = createEClass(SIMPLE3_DTOOL_WIZARD_PAGES_PROVIDER);

		featureOfInterestPickingToolWizardPagesProviderEClass = createEClass(FEATURE_OF_INTEREST_PICKING_TOOL_WIZARD_PAGES_PROVIDER);

		abstractTwoPoints3DToolWizardPagesProviderEClass = createEClass(ABSTRACT_TWO_POINTS3_DTOOL_WIZARD_PAGES_PROVIDER);

		ruler3DToolWizardPagesProviderEClass = createEClass(RULER3_DTOOL_WIZARD_PAGES_PROVIDER);

		trajectory3DToolWizardPagesProviderEClass = createEClass(TRAJECTORY3_DTOOL_WIZARD_PAGES_PROVIDER);

		trajectoryPickingToolWizardPagesProviderEClass = createEClass(TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER);

		abstractPickLocationToollWizardPagesProviderEClass = createEClass(ABSTRACT_PICK_LOCATION_TOOLL_WIZARD_PAGES_PROVIDER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonTopologyUIPackage theApogyCommonTopologyUIPackage = (ApogyCommonTopologyUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyUIPackage.eNS_URI);
		ApogyCommonEMFUIPackage theApogyCommonEMFUIPackage = (ApogyCommonEMFUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUIPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		ruler3dToolNodePresentationEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodePresentation());
		trajectory3DToolNodePresentationEClass.getESuperTypes().add(theApogyCommonTopologyUIPackage.getNodePresentation());
		abstractToolEClassSettingsEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getMapBasedEClassSettings());
		abstractToolWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonEMFUIPackage.getNamedDescribedWizardPagesProvider());
		simpleToolWizardPagesProviderEClass.getESuperTypes().add(this.getAbstractToolWizardPagesProvider());
		simple3DToolWizardPagesProviderEClass.getESuperTypes().add(this.getSimpleToolWizardPagesProvider());
		featureOfInterestPickingToolWizardPagesProviderEClass.getESuperTypes().add(this.getSimple3DToolWizardPagesProvider());
		abstractTwoPoints3DToolWizardPagesProviderEClass.getESuperTypes().add(this.getSimple3DToolWizardPagesProvider());
		ruler3DToolWizardPagesProviderEClass.getESuperTypes().add(this.getSimple3DToolWizardPagesProvider());
		trajectory3DToolWizardPagesProviderEClass.getESuperTypes().add(this.getSimple3DToolWizardPagesProvider());
		trajectoryPickingToolWizardPagesProviderEClass.getESuperTypes().add(this.getSimple3DToolWizardPagesProvider());
		abstractPickLocationToollWizardPagesProviderEClass.getESuperTypes().add(this.getSimple3DToolWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(ruler3dToolNodePresentationEClass, Ruler3dToolNodePresentation.class, "Ruler3dToolNodePresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(trajectory3DToolNodePresentationEClass, Trajectory3DToolNodePresentation.class, "Trajectory3DToolNodePresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractToolEClassSettingsEClass, AbstractToolEClassSettings.class, "AbstractToolEClassSettings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractToolEClassSettings_Name(), theEcorePackage.getEString(), "name", null, 0, 1, AbstractToolEClassSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractToolEClassSettings_Description(), theEcorePackage.getEString(), "description", null, 0, 1, AbstractToolEClassSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractToolWizardPagesProviderEClass, AbstractToolWizardPagesProvider.class, "AbstractToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(simpleToolWizardPagesProviderEClass, SimpleToolWizardPagesProvider.class, "SimpleToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(simple3DToolWizardPagesProviderEClass, Simple3DToolWizardPagesProvider.class, "Simple3DToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(featureOfInterestPickingToolWizardPagesProviderEClass, FeatureOfInterestPickingToolWizardPagesProvider.class, "FeatureOfInterestPickingToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractTwoPoints3DToolWizardPagesProviderEClass, AbstractTwoPoints3DToolWizardPagesProvider.class, "AbstractTwoPoints3DToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ruler3DToolWizardPagesProviderEClass, Ruler3DToolWizardPagesProvider.class, "Ruler3DToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(trajectory3DToolWizardPagesProviderEClass, Trajectory3DToolWizardPagesProvider.class, "Trajectory3DToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(trajectoryPickingToolWizardPagesProviderEClass, TrajectoryPickingToolWizardPagesProvider.class, "TrajectoryPickingToolWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractPickLocationToollWizardPagesProviderEClass, AbstractPickLocationToollWizardPagesProvider.class, "AbstractPickLocationToollWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //ApogyAddonsUIPackageImpl
