/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
@GenModel(prefix="ApogyAddonsUI",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
		  copyrightText="Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
    Pierre Allard (Pierre.Allard@canada.ca), 
    Regent L'Archeveque (Regent.Larcheveque@canada.ca),
    Sebastien Gemme (Sebastien.Gemme@canada.ca),
    Canadian Space Agency (CSA) - Initial API and implementation",
		  modelName="ApogyAddonsUI",
		  complianceLevel="6.0",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.addons.ui/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.addons.ui.edit/src-generated")

package ca.gc.asc_csa.apogy.addons.ui

import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation

class Ruler3dToolNodePresentation extends NodePresentation
{
	
}

class Trajectory3DToolNodePresentation extends NodePresentation
{
	
}

// Wizard Support.
class AbstractToolEClassSettings extends MapBasedEClassSettings
{
	String name
	String description
}


// Wizard support for AbstractTool
class AbstractToolWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

// Wizard support for SimpleTool
class SimpleToolWizardPagesProvider extends AbstractToolWizardPagesProvider
{	
}

// Wizard support for Simple3DTool
class Simple3DToolWizardPagesProvider extends SimpleToolWizardPagesProvider
{	
}

// Wizard support for FeatureOfInterestPickingTool
class FeatureOfInterestPickingToolWizardPagesProvider extends Simple3DToolWizardPagesProvider
{	
}

// Wizard support for AbstractTwoPoints3DTool
class AbstractTwoPoints3DToolWizardPagesProvider extends Simple3DToolWizardPagesProvider
{	
}

// Wizard support for Ruler3DTool
class Ruler3DToolWizardPagesProvider extends Simple3DToolWizardPagesProvider
{	
}

// Wizard support for Trajectory3DTool.
class Trajectory3DToolWizardPagesProvider extends Simple3DToolWizardPagesProvider
{	
}

// Wizard support for TrajectoryPickingToolNode
class TrajectoryPickingToolWizardPagesProvider extends Simple3DToolWizardPagesProvider
{	
}

// Wizard support for AbstractPickLocationTool
class AbstractPickLocationToollWizardPagesProvider extends Simple3DToolWizardPagesProvider
{	
}