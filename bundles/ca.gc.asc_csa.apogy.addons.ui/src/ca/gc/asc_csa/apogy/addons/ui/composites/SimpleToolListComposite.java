package ca.gc.asc_csa.apogy.addons.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsFacade;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.SimpleTool;
import ca.gc.asc_csa.apogy.addons.SimpleToolList;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings;
import ca.gc.asc_csa.apogy.addons.ui.Activator;
import ca.gc.asc_csa.apogy.addons.ui.ApogyAddonsUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

public class SimpleToolListComposite extends Composite 
{
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);	
	private DataBindingContext m_bindingContext;
	
	private TableViewer viewer;	
	private Button btnNew;
	private Button btnDelete;

	private SimpleToolList simpleToolList;
	
	private Adapter adapter;
	
	public SimpleToolListComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		// Table Viewer
		viewer = new TableViewer(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		Table table = viewer.getTable();
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 2);
		gd_table.minimumWidth = 600;
		gd_table.widthHint = 600;
		table.setLayoutData(gd_table);
		table.setLinesVisible(true);
		ColumnViewerToolTipSupport.enableFor(viewer);
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newSimpleToolSelected((SimpleTool)((IStructuredSelection) event.getSelection()).getFirstElement());
			}
		});

		TableViewerColumn tableViewerColumnItem_Name = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn trclmnItemName = tableViewerColumnItem_Name.getColumn();
		trclmnItemName.setWidth(200);		
		
		viewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory)
		{
			@Override
			public Object[] getElements(Object object) 
			{
				if(object instanceof SimpleToolList)
				{
					return ((SimpleToolList) object).getSimpleTools().toArray();
				}
				return null;
			}
		});
		viewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));

		btnNew = new Button(composite, SWT.NONE);		
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.addListener(SWT.Selection, new Listener() 
		{
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{										
					AbstractToolEClassSettings settings = ApogyAddonsUIFactory.eINSTANCE.createAbstractToolEClassSettings();					
					settings.setName(ApogyCommonEMFFacade.INSTANCE.getDefaultName(getSimpleToolList(), null, ApogyAddonsPackage.Literals.SIMPLE_TOOL_LIST__SIMPLE_TOOLS));
					
					Wizard wizard = new ApogyEObjectWizard(ApogyAddonsPackage.Literals.SIMPLE_TOOL_LIST__SIMPLE_TOOLS, getSimpleToolList(), settings, ApogyAddonsPackage.Literals.SIMPLE_TOOL); 
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
					
					// Forces the viewer to refresh its input.
					if(!viewer.isBusy())
					{					
						viewer.setInput(getSimpleToolList());
					}					
				}
			}
		});


		Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setSize(64, 2);

		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String toolsToDeleteMessage = "";

				Iterator<SimpleTool> projects = getSelectedSimpleTools().iterator();
				while (projects.hasNext()) 
				{
					SimpleTool tool = projects.next();
					toolsToDeleteMessage = toolsToDeleteMessage + tool.getName();

					if (projects.hasNext()) 
					{
						toolsToDeleteMessage = toolsToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected tools", null,
						"Are you sure to delete these tools: " + toolsToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (SimpleTool tool :  getSelectedSimpleTools()) 
					{
						try 
						{
							ApogyAddonsFacade.INSTANCE.deleteTool(tool);
						} 
						catch (Exception e)	
						{
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the tool <"+ tool.getName() + ">",
									EventSeverity.ERROR, e);
						}
					}
				}
				
			}
		});
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");		
		
		setSimpleToolList(ApogyAddonsFacade.INSTANCE.getSimpleToolList());
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	public SimpleToolList getSimpleToolList() {
		return simpleToolList;
	}

	public void setSimpleToolList(SimpleToolList simpleToolList) 
	{				
		if(this.simpleToolList != null)
		{
			simpleToolList.eAdapters().remove(getAdapter());
		}
				
		this.simpleToolList = simpleToolList;
		
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		if(simpleToolList != null)
		{						
			m_bindingContext = customInitDataBindings();
		
			simpleToolList.eAdapters().add(getAdapter());
			
			viewer.setInput(simpleToolList);
			
			if(!viewer.isBusy())
			{
				Table table = viewer.getTable();
				for(int i = 0; i < table.getColumnCount(); i++)
				{
					table.getColumn(i).pack();
				}
				viewer.refresh();
			}
		}
		else
		{
			//viewer.setInput(null);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SimpleTool> getSelectedSimpleTools()
	{
		return ((IStructuredSelection) viewer.getSelection()).toList();
	}
	
	/**
	 * This method is invoked a when a new selection is selected.
	 */
	protected void newSimpleToolSelected(SimpleTool simpleTool) {
	}

	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(viewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}

	private Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof SimpleToolList)
					{
						// forces the viewer to update.						
						setSimpleToolList(getSimpleToolList());
					}
				}
			};
		}
		return adapter;
	}
	
	
}
