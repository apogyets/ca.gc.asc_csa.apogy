package ca.gc.asc_csa.apogy.addons.ui.parts;

import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.SimpleToolList;
import ca.gc.asc_csa.apogy.addons.ui.composites.SimpleToolsComposite;
import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

public class SimpleToolsPart extends AbstractSessionBasedPart
{
	
	private SimpleToolsComposite simpleToolsComposite;
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		simpleToolsComposite.setSimpleToolList(resolveSimpleToolList());
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{						
		simpleToolsComposite = new SimpleToolsComposite(parent, SWT.NONE);
	}

	protected SimpleToolList resolveSimpleToolList()
	{
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		
		SimpleToolList simpleToolList = null;
		if(invocatorSession != null)
		{			
			Iterator<AbstractToolsListContainer> it = invocatorSession.getToolsList().getToolsListContainers().iterator();
			
			while(it.hasNext() && simpleToolList == null)
			{
				AbstractToolsListContainer list = it.next();
				if(list instanceof SimpleToolList)
				{
					simpleToolList = (SimpleToolList) list;
				}
			}
		}
		
		return simpleToolList;
	}
}
