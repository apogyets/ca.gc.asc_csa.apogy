package ca.gc.asc_csa.apogy.addons.ui.composites;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import ca.gc.asc_csa.apogy.core.FeatureOfInterestList;

public class ListOfFeatureOfInterestListComposite extends Composite 
{
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);	
	private Collection<FeatureOfInterestList> list = new ArrayList<FeatureOfInterestList>();
	
	private TableViewer viewer;
	
	public ListOfFeatureOfInterestListComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(1, false));
		
		// Table Viewer
		viewer = new TableViewer(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		Table table = viewer.getTable();
		table.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2);
		gd_table.minimumWidth = 300;
		gd_table.widthHint = 300;
		table.setLayoutData(gd_table);
		table.setLinesVisible(true);
		ColumnViewerToolTipSupport.enableFor(viewer);
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newFeatureOfInterestListSelected((FeatureOfInterestList)((IStructuredSelection) event.getSelection()).getFirstElement());
			}
		});

		TableViewerColumn tableViewerColumnItem_Name = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn trclmnItemName = tableViewerColumnItem_Name.getColumn();
		trclmnItemName.setText("Feature Of Interest List");
		trclmnItemName.setWidth(400);		
		
		viewer.setContentProvider(new ArrayContentProvider());		
		viewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		viewer.setInput(list);
	}

	@Override
	public void dispose() 
	{	
		super.dispose();
	}

	public Collection<FeatureOfInterestList> getList() {
		return list;
	}

	public void setList(Collection<FeatureOfInterestList> newList) 
	{
		this.list.clear();
		
		if(newList != null)
		{
			this.list.addAll(newList);
		}
		
		viewer.setInput(list);
	}

	protected void newFeatureOfInterestListSelected(FeatureOfInterestList featureOfInterestList)
	{		
	}
	
}
