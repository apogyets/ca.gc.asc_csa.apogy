/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.provider;


import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.VisibilityPass;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EarthOrbitModelPassToolItemProvider extends EarthOrbitModelToolItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModelPassToolItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLookAheadPeriodPropertyDescriptor(object);
			addSpacecraftsVisibilitySetPropertyDescriptor(object);
			addDisplayedPassPropertyDescriptor(object);
			addEarthOrbitModelPassToolNodePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Look Ahead Period feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLookAheadPeriodPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EarthOrbitModelPassTool_lookAheadPeriod_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EarthOrbitModelPassTool_lookAheadPeriod_feature", "_UI_EarthOrbitModelPassTool_type"),
				 ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Spacecrafts Visibility Set feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSpacecraftsVisibilitySetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EarthOrbitModelPassTool_spacecraftsVisibilitySet_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EarthOrbitModelPassTool_spacecraftsVisibilitySet_feature", "_UI_EarthOrbitModelPassTool_type"),
				 ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET,
				 false,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Displayed Pass feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayedPassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EarthOrbitModelPassTool_displayedPass_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EarthOrbitModelPassTool_displayedPass_feature", "_UI_EarthOrbitModelPassTool_type"),
				 ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS,
				 false,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Earth Orbit Model Pass Tool Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEarthOrbitModelPassToolNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_EarthOrbitModelPassTool_earthOrbitModelPassToolNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_EarthOrbitModelPassTool_earthOrbitModelPassToolNode_feature", "_UI_EarthOrbitModelPassTool_type"),
				 ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE,
				 false,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET);
			childrenFeatures.add(ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS);
			childrenFeatures.add(ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns EarthOrbitModelPassTool.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/EarthOrbitModelPassTool"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@Override
	public String getText(Object object) 	
	{
		EarthOrbitModelPassTool tool = (EarthOrbitModelPassTool) object;
		
		String label = null;
		if(tool.getName() != null && tool.getName().length() > 0)
		{
			label = tool.getName();
		}
		else
		{
			label = getString("_UI_EarthOrbitModelPassTool_type"); 
		}
		
		String simple3DToolText = getSimple3DToolText(tool);		
		if(tool.isAutoUpdateEnabled())
		{
			if(simple3DToolText != null && simple3DToolText.length() > 0) 
			{
				simple3DToolText += ", auto update";	
			}
			else
			{
				simple3DToolText += "auto update";
			}
		}
		
		if(tool.isUpdating())
		{
			if(simple3DToolText != null && simple3DToolText.length() > 0) 
			{
				simple3DToolText += ", updating...";	
			}
			else
			{
				simple3DToolText += "updating...";
			}
		}
		
		label += "(" + simple3DToolText;
		
		VisibilityPass pass = tool.getDisplayedPass();
		if(pass!= null)
		{
			if(simple3DToolText != null && simple3DToolText.length() > 0) label += ", ";								
			
			label += ApogyCommonEMFFacade.INSTANCE.format(pass.getStartTime());
		}
		label += ")";
		
		return label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(EarthOrbitModelPassTool.class)) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD:
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS:
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
