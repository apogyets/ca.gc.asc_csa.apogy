/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import ca.gc.asc_csa.apogy.common.topology.ui.ApogyCommonTopologyUIPackage;
import org.eclipse.emf.ecore.EAttribute;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca),
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCoreTopologyUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation' modelName='ApogyCoreTopologyUI' modelDirectory='/ca.gc.asc_csa.apogy.core.topology.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.topology.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.core.topology'"
 * @generated
 */
public interface ApogyCoreTopologyUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.topology.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCoreTopologyUIPackage eINSTANCE = ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIFacadeImpl <em>Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIFacadeImpl
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl#getApogyCoreTopologyUIFacade()
	 * @generated
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE = 0;

	/**
	 * The feature id for the '<em><b>Edited Apogy System Assembly Root</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT = 0;

	/**
	 * The number of structural features of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Set Visibility</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE___SET_VISIBILITY__VARIABLE_BOOLEAN = 0;

	/**
	 * The operation id for the '<em>Toggle Visibility</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE___TOGGLE_VISIBILITY__VARIABLE = 1;

	/**
	 * The operation id for the '<em>Set Presentation Mode</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__VARIABLE_MESHPRESENTATIONMODE = 2;

	/**
	 * The operation id for the '<em>Set Visibility</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE___SET_VISIBILITY__ABSTRACTTYPEIMPLEMENTATION_BOOLEAN = 3;

	/**
	 * The operation id for the '<em>Toggle Visibility</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE___TOGGLE_VISIBILITY__ABSTRACTTYPEIMPLEMENTATION = 4;

	/**
	 * The operation id for the '<em>Set Presentation Mode</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__ABSTRACTTYPEIMPLEMENTATION_MESHPRESENTATIONMODE = 5;

	/**
	 * The operation id for the '<em>Set Presentation Mode</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__NODE_MESHPRESENTATIONMODE = 6;

	/**
	 * The number of operations of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_CORE_TOPOLOGY_UI_FACADE_OPERATION_COUNT = 7;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.impl.AttachedViewPointPagesProviderImpl <em>Attached View Point Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.AttachedViewPointPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl#getAttachedViewPointPagesProvider()
	 * @generated
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER = 2;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology <em>Displayed Topology</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl#getDisplayedTopology()
	 * @generated
	 */
	int DISPLAYED_TOPOLOGY = 3;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.impl.DisplayedTopologyChoiceImpl <em>Displayed Topology Choice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.DisplayedTopologyChoiceImpl
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl#getDisplayedTopologyChoice()
	 * @generated
	 */
	int DISPLAYED_TOPOLOGY_CHOICE = 1;


	/**
	 * The feature id for the '<em><b>Displayed Topology</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAYED_TOPOLOGY_CHOICE__DISPLAYED_TOPOLOGY = 0;

	/**
	 * The number of structural features of the '<em>Displayed Topology Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAYED_TOPOLOGY_CHOICE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Displayed Topology Choice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAYED_TOPOLOGY_CHOICE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER__PAGES = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Attached View Point Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Attached View Point Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACHED_VIEW_POINT_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyUIPackage.ABSTRACT_VIEW_POINT_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade <em>Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade
	 * @generated
	 */
	EClass getApogyCoreTopologyUIFacade();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#getEditedApogySystemAssemblyRoot <em>Edited Apogy System Assembly Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Edited Apogy System Assembly Root</em>'.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#getEditedApogySystemAssemblyRoot()
	 * @see #getApogyCoreTopologyUIFacade()
	 * @generated
	 */
	EReference getApogyCoreTopologyUIFacade_EditedApogySystemAssemblyRoot();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setVisibility(ca.gc.asc_csa.apogy.core.invocator.Variable, boolean) <em>Set Visibility</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Visibility</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setVisibility(ca.gc.asc_csa.apogy.core.invocator.Variable, boolean)
	 * @generated
	 */
	EOperation getApogyCoreTopologyUIFacade__SetVisibility__Variable_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#toggleVisibility(ca.gc.asc_csa.apogy.core.invocator.Variable) <em>Toggle Visibility</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Toggle Visibility</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#toggleVisibility(ca.gc.asc_csa.apogy.core.invocator.Variable)
	 * @generated
	 */
	EOperation getApogyCoreTopologyUIFacade__ToggleVisibility__Variable();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setPresentationMode(ca.gc.asc_csa.apogy.core.invocator.Variable, ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode) <em>Set Presentation Mode</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Presentation Mode</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setPresentationMode(ca.gc.asc_csa.apogy.core.invocator.Variable, ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode)
	 * @generated
	 */
	EOperation getApogyCoreTopologyUIFacade__SetPresentationMode__Variable_MeshPresentationMode();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setVisibility(ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation, boolean) <em>Set Visibility</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Visibility</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setVisibility(ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation, boolean)
	 * @generated
	 */
	EOperation getApogyCoreTopologyUIFacade__SetVisibility__AbstractTypeImplementation_boolean();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#toggleVisibility(ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation) <em>Toggle Visibility</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Toggle Visibility</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#toggleVisibility(ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation)
	 * @generated
	 */
	EOperation getApogyCoreTopologyUIFacade__ToggleVisibility__AbstractTypeImplementation();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setPresentationMode(ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation, ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode) <em>Set Presentation Mode</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Presentation Mode</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setPresentationMode(ca.gc.asc_csa.apogy.core.invocator.AbstractTypeImplementation, ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode)
	 * @generated
	 */
	EOperation getApogyCoreTopologyUIFacade__SetPresentationMode__AbstractTypeImplementation_MeshPresentationMode();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setPresentationMode(ca.gc.asc_csa.apogy.common.topology.Node, ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode) <em>Set Presentation Mode</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Presentation Mode</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade#setPresentationMode(ca.gc.asc_csa.apogy.common.topology.Node, ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode)
	 * @generated
	 */
	EOperation getApogyCoreTopologyUIFacade__SetPresentationMode__Node_MeshPresentationMode();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.topology.ui.AttachedViewPointPagesProvider <em>Attached View Point Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attached View Point Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.AttachedViewPointPagesProvider
	 * @generated
	 */
	EClass getAttachedViewPointPagesProvider();

	/**
	 * Returns the meta object for enum '{@link ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology <em>Displayed Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Displayed Topology</em>'.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology
	 * @generated
	 */
	EEnum getDisplayedTopology();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopologyChoice <em>Displayed Topology Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Displayed Topology Choice</em>'.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopologyChoice
	 * @generated
	 */
	EClass getDisplayedTopologyChoice();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopologyChoice#getDisplayedTopology <em>Displayed Topology</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Displayed Topology</em>'.
	 * @see ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopologyChoice#getDisplayedTopology()
	 * @see #getDisplayedTopologyChoice()
	 * @generated
	 */
	EAttribute getDisplayedTopologyChoice_DisplayedTopology();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCoreTopologyUIFactory getApogyCoreTopologyUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIFacadeImpl <em>Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIFacadeImpl
		 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl#getApogyCoreTopologyUIFacade()
		 * @generated
		 */
		EClass APOGY_CORE_TOPOLOGY_UI_FACADE = eINSTANCE.getApogyCoreTopologyUIFacade();
		/**
		 * The meta object literal for the '<em><b>Edited Apogy System Assembly Root</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT = eINSTANCE.getApogyCoreTopologyUIFacade_EditedApogySystemAssemblyRoot();
		/**
		 * The meta object literal for the '<em><b>Set Visibility</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_CORE_TOPOLOGY_UI_FACADE___SET_VISIBILITY__VARIABLE_BOOLEAN = eINSTANCE.getApogyCoreTopologyUIFacade__SetVisibility__Variable_boolean();
		/**
		 * The meta object literal for the '<em><b>Toggle Visibility</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_CORE_TOPOLOGY_UI_FACADE___TOGGLE_VISIBILITY__VARIABLE = eINSTANCE.getApogyCoreTopologyUIFacade__ToggleVisibility__Variable();
		/**
		 * The meta object literal for the '<em><b>Set Presentation Mode</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__VARIABLE_MESHPRESENTATIONMODE = eINSTANCE.getApogyCoreTopologyUIFacade__SetPresentationMode__Variable_MeshPresentationMode();
		/**
		 * The meta object literal for the '<em><b>Set Visibility</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_CORE_TOPOLOGY_UI_FACADE___SET_VISIBILITY__ABSTRACTTYPEIMPLEMENTATION_BOOLEAN = eINSTANCE.getApogyCoreTopologyUIFacade__SetVisibility__AbstractTypeImplementation_boolean();
		/**
		 * The meta object literal for the '<em><b>Toggle Visibility</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_CORE_TOPOLOGY_UI_FACADE___TOGGLE_VISIBILITY__ABSTRACTTYPEIMPLEMENTATION = eINSTANCE.getApogyCoreTopologyUIFacade__ToggleVisibility__AbstractTypeImplementation();
		/**
		 * The meta object literal for the '<em><b>Set Presentation Mode</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__ABSTRACTTYPEIMPLEMENTATION_MESHPRESENTATIONMODE = eINSTANCE.getApogyCoreTopologyUIFacade__SetPresentationMode__AbstractTypeImplementation_MeshPresentationMode();
		/**
		 * The meta object literal for the '<em><b>Set Presentation Mode</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_CORE_TOPOLOGY_UI_FACADE___SET_PRESENTATION_MODE__NODE_MESHPRESENTATIONMODE = eINSTANCE.getApogyCoreTopologyUIFacade__SetPresentationMode__Node_MeshPresentationMode();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.impl.AttachedViewPointPagesProviderImpl <em>Attached View Point Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.AttachedViewPointPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl#getAttachedViewPointPagesProvider()
		 * @generated
		 */
		EClass ATTACHED_VIEW_POINT_PAGES_PROVIDER = eINSTANCE.getAttachedViewPointPagesProvider();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology <em>Displayed Topology</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology
		 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl#getDisplayedTopology()
		 * @generated
		 */
		EEnum DISPLAYED_TOPOLOGY = eINSTANCE.getDisplayedTopology();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.topology.ui.impl.DisplayedTopologyChoiceImpl <em>Displayed Topology Choice</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.DisplayedTopologyChoiceImpl
		 * @see ca.gc.asc_csa.apogy.core.topology.ui.impl.ApogyCoreTopologyUIPackageImpl#getDisplayedTopologyChoice()
		 * @generated
		 */
		EClass DISPLAYED_TOPOLOGY_CHOICE = eINSTANCE.getDisplayedTopologyChoice();
		/**
		 * The meta object literal for the '<em><b>Displayed Topology</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISPLAYED_TOPOLOGY_CHOICE__DISPLAYED_TOPOLOGY = eINSTANCE.getDisplayedTopologyChoice_DisplayedTopology();

	}

} //ApogyCoreTopologyUIPackage
