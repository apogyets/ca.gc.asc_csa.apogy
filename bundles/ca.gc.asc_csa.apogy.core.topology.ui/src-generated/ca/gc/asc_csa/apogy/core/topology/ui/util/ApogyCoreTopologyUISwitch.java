/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.ui.AbstractViewPointPagesProvider;
import ca.gc.asc_csa.apogy.core.topology.ui.*;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage;
import ca.gc.asc_csa.apogy.core.topology.ui.AttachedViewPointPagesProvider;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage
 * @generated
 */
public class ApogyCoreTopologyUISwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ApogyCoreTopologyUIPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCoreTopologyUISwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyCoreTopologyUIPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE: {
				ApogyCoreTopologyUIFacade apogyCoreTopologyUIFacade = (ApogyCoreTopologyUIFacade)theEObject;
				T result = caseApogyCoreTopologyUIFacade(apogyCoreTopologyUIFacade);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY_CHOICE: {
				DisplayedTopologyChoice displayedTopologyChoice = (DisplayedTopologyChoice)theEObject;
				T result = caseDisplayedTopologyChoice(displayedTopologyChoice);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCoreTopologyUIPackage.ATTACHED_VIEW_POINT_PAGES_PROVIDER: {
				AttachedViewPointPagesProvider attachedViewPointPagesProvider = (AttachedViewPointPagesProvider)theEObject;
				T result = caseAttachedViewPointPagesProvider(attachedViewPointPagesProvider);
				if (result == null) result = caseAbstractViewPointPagesProvider(attachedViewPointPagesProvider);
				if (result == null) result = caseWizardPagesProvider(attachedViewPointPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApogyCoreTopologyUIFacade(ApogyCoreTopologyUIFacade object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Displayed Topology Choice</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Displayed Topology Choice</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDisplayedTopologyChoice(DisplayedTopologyChoice object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attached View Point Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attached View Point Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttachedViewPointPagesProvider(AttachedViewPointPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract View Point Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract View Point Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractViewPointPagesProvider(AbstractViewPointPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ApogyCoreTopologyUISwitch
