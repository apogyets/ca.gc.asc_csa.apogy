/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui.impl;

import ca.gc.asc_csa.apogy.core.topology.ui.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFactory;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage;
import ca.gc.asc_csa.apogy.core.topology.ui.AttachedViewPointPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCoreTopologyUIFactoryImpl extends EFactoryImpl implements ApogyCoreTopologyUIFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ApogyCoreTopologyUIFactory init() {
		try {
			ApogyCoreTopologyUIFactory theApogyCoreTopologyUIFactory = (ApogyCoreTopologyUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCoreTopologyUIPackage.eNS_URI);
			if (theApogyCoreTopologyUIFactory != null) {
				return theApogyCoreTopologyUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCoreTopologyUIFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCoreTopologyUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE: return createApogyCoreTopologyUIFacade();
			case ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY_CHOICE: return createDisplayedTopologyChoice();
			case ApogyCoreTopologyUIPackage.ATTACHED_VIEW_POINT_PAGES_PROVIDER: return createAttachedViewPointPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY:
				return createDisplayedTopologyFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ApogyCoreTopologyUIPackage.DISPLAYED_TOPOLOGY:
				return convertDisplayedTopologyToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCoreTopologyUIFacade createApogyCoreTopologyUIFacade() {
		ApogyCoreTopologyUIFacadeImpl apogyCoreTopologyUIFacade = new ApogyCoreTopologyUIFacadeImpl();
		return apogyCoreTopologyUIFacade;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisplayedTopologyChoice createDisplayedTopologyChoice() {
		DisplayedTopologyChoiceImpl displayedTopologyChoice = new DisplayedTopologyChoiceImpl();
		return displayedTopologyChoice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttachedViewPointPagesProvider createAttachedViewPointPagesProvider() {
		AttachedViewPointPagesProviderImpl attachedViewPointPagesProvider = new AttachedViewPointPagesProviderImpl();
		return attachedViewPointPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DisplayedTopology createDisplayedTopologyFromString(EDataType eDataType, String initialValue) {
		DisplayedTopology result = DisplayedTopology.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDisplayedTopologyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCoreTopologyUIPackage getApogyCoreTopologyUIPackage() {
		return (ApogyCoreTopologyUIPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ApogyCoreTopologyUIPackage getPackage() {
		return ApogyCoreTopologyUIPackage.eINSTANCE;
	}

} //ApogyCoreTopologyUIFactoryImpl
