/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.topology.ui;

import ca.gc.asc_csa.apogy.common.topology.ui.AbstractViewPointPagesProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attached View Point Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage#getAttachedViewPointPagesProvider()
 * @model
 * @generated
 */
public interface AttachedViewPointPagesProvider extends AbstractViewPointPagesProvider {
} // AttachedViewPointPagesProvider
