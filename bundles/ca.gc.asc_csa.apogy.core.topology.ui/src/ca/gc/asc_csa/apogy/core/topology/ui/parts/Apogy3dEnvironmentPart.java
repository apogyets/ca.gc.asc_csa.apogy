/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * 		 Regent L'Archeveque
 * 		 Olivier L. Larouche
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.GraphicsContext;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.common.topology.ui.TopologyPresentationSet;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.core.ResultNode;
import ca.gc.asc_csa.apogy.core.invocator.AbstractResult;
import ca.gc.asc_csa.apogy.core.ui.ResultNodePresentation;

public class Apogy3dEnvironmentPart extends AbstractApogy3dPart 
{		
	// FIXME: Add List<AbstractResult> selection. List results = ApogyCommonConvertersUIFacade.INSTANCE.convert(selection, AbstractResult.class);	
				
	@Override
	public void userPostConstruct(MPart mPart) 
	{	
		super.userPostConstruct(mPart);				
		reloadToolBarAndMenus();			
	}
	
	@Override
	protected JME3RenderEngineDelegate createJME3RenderEngineDelegate() {
		return new JME3RenderEngineDelegate();
	}

	/*
	 * Highlights the list of results in the renderer.
	 */
	public void show(List<AbstractResult> results) {
		if (getTopologyViewer() != null && getTopologyViewer().getInput() instanceof GraphicsContext) {
			GraphicsContext graphicsContext = (GraphicsContext) getTopologyViewer().getInput();
			TopologyPresentationSet presentationTopologySet = graphicsContext.getTopologyPresentationSet();

			List<Node> nodes = presentationTopologySet.getNodes();
			Map<AbstractResult, ResultNodePresentation> resultToPresentationMap = new HashMap<AbstractResult, ResultNodePresentation>();

			for (Node node : nodes) {
				if (node instanceof ResultNode) {
					ResultNode resultNode = (ResultNode) node;
					NodePresentation nodePresentation = presentationTopologySet.getPresentationNode(node);
					if (nodePresentation instanceof ResultNodePresentation) {
						ResultNodePresentation resultNodePresentation = (ResultNodePresentation) nodePresentation;
						resultToPresentationMap.put(resultNode.getResult(), resultNodePresentation);
					}
				}
			}

			// Set invisible all ResultNode
			for (ResultNodePresentation resultNodePresentation : resultToPresentationMap.values()) {
				resultNodePresentation.setVisible(false);
			}

			// Set visible the selected ones.
			for (AbstractResult abstractResult : results) {
				ResultNodePresentation resultNodePresentation = resultToPresentationMap.get(abstractResult);
				if (resultNodePresentation != null)
					resultNodePresentation.setVisible(true);
			}
		}
	}
}