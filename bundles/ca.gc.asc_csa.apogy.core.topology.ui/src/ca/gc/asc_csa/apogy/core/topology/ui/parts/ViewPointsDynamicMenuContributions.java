/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.ui.di.AboutToHide;
import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.emf.common.util.EList;

import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.topology.ArbitraryViewPoint;
import ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint;
import ca.gc.asc_csa.apogy.core.environment.AbstractApogyEnvironmentItem;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.ViewPointList;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.Environment;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;

public class ViewPointsDynamicMenuContributions 
{
	
	@Inject
	private EModelService modelService;
	
	@AboutToShow
	public void aboutToShow(List<MMenuElement> items)
	{
		// First, get the list of view point from the active session.
		ViewPointList viewPointList = resolveViewPointList();
		
		if(viewPointList != null)
		{
			populate(items);	
		}
	}
	
	@AboutToHide
	public void aboutToHide(List<MMenuElement> items)
	{		
	}
	
	protected void populate(List<MMenuElement> items)
	{
		// Remove existing menu items
		for (MMenuElement child : items)
		{
			child.setToBeRendered(false);
			child.setVisible(false);			
		}
		items.clear();
				
		// Gets the list of view point available.
		ViewPointList viewPointList = resolveViewPointList();
				
		for(AbstractViewPoint viewPoint : viewPointList.getViewPoints())
		{		
			MDirectMenuItem dynamicItem = modelService.createModelElement(MDirectMenuItem.class);
	        dynamicItem.setLabel(viewPoint.getName());
	        dynamicItem.setContributionURI("bundleclass://ca.gc.asc_csa.apogy.core.topology.ui/ca.gc.asc_csa.apogy.core.topology.ui.handlers.SetActiveViewPointHandler");
	        dynamicItem.setContributorURI("platform:/plugin/ca.gc.asc_csa.apogy.core.topology.ui");
	        dynamicItem.setIconURI("platform:/plugin/ca.gc.asc_csa.apogy.core.topology.ui/icons/view_point_16.gif");
	        dynamicItem.setToBeRendered(true);
	        
	        dynamicItem.getTransientData().put("viewpoint", viewPoint);
	
	        items.add(dynamicItem);
		}		
	}		
	
	protected ViewPointList resolveViewPointList()
	{
		ViewPointList viewPointList = null;
		
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		if(invocatorSession != null)
		{
			Environment env = invocatorSession.getEnvironment();
			if(env instanceof ApogyEnvironment)
			{
				ApogyEnvironment apogyEnvironment = (ApogyEnvironment) env;
				EList<AbstractApogyEnvironmentItem> items = apogyEnvironment.getEnvironmentItems();
				for(AbstractApogyEnvironmentItem item : items)
				{
					if(item instanceof ViewPointList)
					{
						viewPointList = (ViewPointList) item;
					}
				}				
			}
		}
		
		return viewPointList;
	}
	
	protected String getIconURIForAbstractViewPoint(AbstractViewPoint abstractViewPoint)
	{
		String uri = null;
		
		if(abstractViewPoint instanceof ArbitraryViewPoint)
		{
			uri = "platform:/plugin/ca.gc.asc_csa.apogy.common.topology.edit/icons/full/obj16/ArbitraryViewPoint.gif";
		}
		else if(abstractViewPoint instanceof AttachedViewPoint)
		{
			uri = "platform:/plugin/ca.gc.asc_csa.apogy.common.topology.edit/icons/full/obj16/AttachedViewPoint.gif";
		}		
		
		return uri;
	}
}
