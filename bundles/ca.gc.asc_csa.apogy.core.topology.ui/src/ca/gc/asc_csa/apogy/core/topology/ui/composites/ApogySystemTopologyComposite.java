/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.composites;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.TopologyTreeEditingComposite;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology;
import ca.gc.asc_csa.apogy.core.topology.ui.internal.ApogySystem3dUtils;

public class ApogySystemTopologyComposite extends Composite 
{
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	protected ApogySystem apogySystem;
	protected DisplayedTopology displayedTopology = DisplayedTopology.ASSEMBLY;
	
	protected Node systemRoot;
	protected Node assemblyRoot;						
	
	private TopologyTreeEditingComposite systemTopologyTreeEditingComposite;
	private TopologyTreeEditingComposite assemblyTopologyTreeEditingComposite;
			
	protected final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public ApogySystemTopologyComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
				
		ScrolledForm scrolledForm = formToolkit.createScrolledForm(this);
		scrolledForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrolledForm);		
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 1;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrolledForm.getBody().setLayout(tableWrapLayout);
		}
				
		// Display settings.
		Section sctnDisplaySettings = formToolkit.createSection(scrolledForm.getBody(),Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDisplaySettings = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnDisplaySettings.valign = TableWrapData.FILL;
		twd_sctnDisplaySettings.grabVertical = true;
		twd_sctnDisplaySettings.grabHorizontal = true;
		sctnDisplaySettings.setLayoutData(twd_sctnDisplaySettings);
		formToolkit.paintBordersFor(sctnDisplaySettings);
		sctnDisplaySettings.setText("3D Display Settings");
		
		Composite displaySettingsComposite = new Composite(sctnDisplaySettings, SWT.NONE);
		displaySettingsComposite.setLayout(new GridLayout(4, false));
		GridData gd_displaySettingsComposite = new GridData(SWT.FILL, SWT.TOP, true, false);
		displaySettingsComposite.setLayoutData(gd_displaySettingsComposite);
				
		formToolkit.adapt(displaySettingsComposite);
		formToolkit.paintBordersFor(displaySettingsComposite);
		sctnDisplaySettings.setClient(displaySettingsComposite);
			
		Label lblDisplayedTopologyChoice = new Label(displaySettingsComposite, SWT.NONE);
		lblDisplayedTopologyChoice.setText("Displayed Topology");
		
		ComboViewer comboViewerDisplayedTopology = new ComboViewer(displaySettingsComposite, SWT.NONE);		
		comboViewerDisplayedTopology.setContentProvider(new ArrayContentProvider());
		comboViewerDisplayedTopology.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		comboViewerDisplayedTopology.setInput(DisplayedTopology.values());
		comboViewerDisplayedTopology.setSelection(new StructuredSelection(displayedTopology), true);
		comboViewerDisplayedTopology.addSelectionChangedListener(new ISelectionChangedListener() 
		{		
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				DisplayedTopology displayedTopology = (DisplayedTopology) comboViewerDisplayedTopology.getStructuredSelection().getFirstElement();
				setDisplayedTopology(displayedTopology);
			}
		});				
			
		Label cadNodeDisplayMode = new Label(displaySettingsComposite, SWT.NONE);
		cadNodeDisplayMode.setText("Sub-System Display Mode:");
		
		ComboViewer comboSubSystemPresentationMode = new ComboViewer(displaySettingsComposite, SWT.NONE);		
		comboSubSystemPresentationMode.setContentProvider(new ArrayContentProvider());
		comboSubSystemPresentationMode.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		comboSubSystemPresentationMode.setInput(MeshPresentationMode.values());
		comboSubSystemPresentationMode.setSelection(new StructuredSelection(MeshPresentationMode.SURFACE), true);
		comboSubSystemPresentationMode.addSelectionChangedListener(new ISelectionChangedListener() 
		{		
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				MeshPresentationMode meshPresentationMode = (MeshPresentationMode) comboSubSystemPresentationMode.getStructuredSelection().getFirstElement();
				setSubSystemDisplayMode(meshPresentationMode);
			}
		});				
		
		// System Topology
		Section sctnSystemTopology = formToolkit.createSection(scrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDemlist = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnDemlist.valign = TableWrapData.FILL;
		twd_sctnDemlist.grabVertical = true;
		twd_sctnDemlist.grabHorizontal = true;
		sctnSystemTopology.setLayoutData(twd_sctnDemlist);
		formToolkit.paintBordersFor(sctnSystemTopology);
		sctnSystemTopology.setText("System");

		systemTopologyTreeEditingComposite = new TopologyTreeEditingComposite(sctnSystemTopology, SWT.NONE, true)
		{
			@Override
			protected void nodeSelected(Node node) 
			{
				ApogySystemTopologyComposite.this.nodeSelected(node);
			}
		};				
		formToolkit.adapt(systemTopologyTreeEditingComposite);
		formToolkit.paintBordersFor(systemTopologyTreeEditingComposite);
		sctnSystemTopology.setClient(systemTopologyTreeEditingComposite);
						
		// Assembly Topology
		Section sctnAssemblyTopology = formToolkit.createSection(scrolledForm.getBody(),Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnAssemblyTopology = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 1, 1);
		twd_sctnAssemblyTopology.valign = TableWrapData.FILL;
		twd_sctnAssemblyTopology.grabVertical = true;
		twd_sctnAssemblyTopology.grabHorizontal = true;
		sctnAssemblyTopology.setLayoutData(twd_sctnAssemblyTopology);
		formToolkit.paintBordersFor(sctnAssemblyTopology);
		sctnAssemblyTopology.setText("Assembly");
				
		assemblyTopologyTreeEditingComposite = new TopologyTreeEditingComposite(sctnAssemblyTopology, SWT.NONE, false)
		{
			@Override
			protected void nodeSelected(Node node) 
			{
				ApogySystemTopologyComposite.this.nodeSelected(node);
			}
		};				
		formToolkit.adapt(assemblyTopologyTreeEditingComposite);
		formToolkit.paintBordersFor(assemblyTopologyTreeEditingComposite);
		sctnAssemblyTopology.setClient(assemblyTopologyTreeEditingComposite);						
	}

	public ApogySystem getApogySystem() 
	{
		return apogySystem;
	}

	public void setApogySystem(ApogySystem newApogySystem) 
	{
		// Cleanup previous
		if(this.apogySystem != newApogySystem && this.apogySystem != null)
		{
			// TODO.
		}		
		
		this.apogySystem = newApogySystem;
		
		if(newApogySystem != null)
		{
			// Get the system root topology.
			if(newApogySystem.getTopologyRoot() != null)
			{
				setSystemRoot(newApogySystem.getTopologyRoot().getOriginNode());
			}
			else 
			{
				setSystemRoot(null);
			}			
		}
		else
		{
			setSystemRoot(null);
			setAssemblyRoot(null);
		}
	}
	
	public Node getAssemblyRoot() 
	{
		return assemblyRoot;
	}

	public void setAssemblyRoot(Node newNode) 
	{		
		this.assemblyRoot = newNode;		
		assemblyTopologyTreeEditingComposite.setRoot(newNode);		
	}
	
	public void selectNode(Node selectedNode)
	{
		if(assemblyTopologyTreeEditingComposite != null && !assemblyTopologyTreeEditingComposite.isDisposed())
		{
			assemblyTopologyTreeEditingComposite.selectNode(selectedNode);
		}
		
		nodeSelected(selectedNode);
	}
		
	/**
	 * Method called when the topology to be displayed choice is changed. To be overloaded.
	 * @param newDisplayedTopology The new topology display choice.
	 */
	protected void newDisplayedTopologySelected(DisplayedTopology newDisplayedTopology)
	{		
	}
	
	protected Node getSystemRoot() 
	{
		return systemRoot;
	}

	protected void setSystemRoot(Node newSystemRoot) 
	{
		this.systemRoot = newSystemRoot;
		
		systemTopologyTreeEditingComposite.setRoot(newSystemRoot);
	}
	
	protected void nodeSelected(Node node)
	{		
	}
	
	private void setDisplayedTopology(DisplayedTopology newDisplayedTopology)
	{
		this.displayedTopology = newDisplayedTopology;			
		newDisplayedTopologySelected(newDisplayedTopology);
	}
	
	private void setSubSystemDisplayMode(MeshPresentationMode newMeshPresentationMode)
	{
		ApogySystem3dUtils.setSubSystemMode(getAssemblyRoot(), newMeshPresentationMode);
	}
}
