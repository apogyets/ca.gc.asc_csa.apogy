/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.parts.BindingsSetPart;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIRCPConstants;

public class ApogySystemBindingsPart extends BindingsSetPart
{
	private ApogySystem apogySystem;
		
	@Override
	protected void setCompositeContents(EObject eObject) 
	{
		if(eObject == null || eObject instanceof ApogySystem)
		{
			setApogySystem((ApogySystem) eObject);			
		}		
	}
	
	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() 
	{
		HashMap<String, ISelectionListener> selectionProvidersIdsToSelectionListeners = new HashMap<String, ISelectionListener>();

		// Listens to the Apogy System File Editor
		selectionProvidersIdsToSelectionListeners.put(ApogyCoreTopologyUIRCPConstants.PART__APOGY_SYSTEM_FILE_EDITOR_ID, new ISelectionListener() {
			
			@Override
			public void selectionChanged(MPart mPart, Object object) 
			{				
				if(object == null || object instanceof EObject)
				{
					setEObject((EObject) object);
				}				
			}
		});

		return selectionProvidersIdsToSelectionListeners;
	}
	
	private void setApogySystem(ApogySystem newApogySystem)
	{
		this.apogySystem = newApogySystem;
		
		if(newApogySystem != null)
		{		
			setBindingsSet(newApogySystem.getBindingSet());			
		}
		else
		{
			setBindingsSet(null);
		}
	}

	@Override
	public Collection<Node> getAvailableNodes() 
	{
		if(apogySystem != null && apogySystem.getTopologyRoot() != null && apogySystem.getTopologyRoot().getOriginNode() != null)
		{
			return ApogyCommonTopologyFacade.INSTANCE.findNodesByType(ApogyCommonTopologyPackage.Literals.NODE, apogySystem.getTopologyRoot().getOriginNode());
		}
		else
		{
			return new ArrayList<Node>();
		}
	}
}
