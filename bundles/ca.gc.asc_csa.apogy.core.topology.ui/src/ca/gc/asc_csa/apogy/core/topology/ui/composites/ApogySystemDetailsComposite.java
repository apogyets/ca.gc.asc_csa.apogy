/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.EClassSelectionComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.ui.composites.TypeTypeMemberSelectionComposite;

public class ApogySystemDetailsComposite extends Composite 
{	
	protected ApogySystem apogySystem;
	
	private Text txtApogySystemName;
	private Text txtApogySystemDescription;
	private EClassSelectionComposite comboApogySystemInterfaceClass;
	private EClassSelectionComposite comboApogySystemApiAdapterClass;
	private TypeTypeMemberSelectionComposite poseProviderSelectionComposite;
	private Button btnPoseProviderClear;
	
	private DataBindingContext m_bindingContext;
		
	public ApogySystemDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(3, false));
		
		// Name
		Label lblName = new Label(this, SWT.NONE);
		lblName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblName.setText("Name :");
		
		txtApogySystemName = new Text(this, SWT.BORDER);
		txtApogySystemName.setText("N/A");
		txtApogySystemName.setEditable(true);
		GridData gd_txtApogySystemName = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_txtApogySystemName.widthHint = 450;
		gd_txtApogySystemName.minimumWidth = 450;
		txtApogySystemName.setLayoutData(gd_txtApogySystemName);
		txtApogySystemName.setToolTipText("Name of this Apogy System.");
		
		// Description
		Label lblDescription = new Label(this, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblDescription.setText("Description :");
		
		txtApogySystemDescription = new Text(this, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
		txtApogySystemDescription.setText("N/A");
		txtApogySystemDescription.setEditable(true);
		GridData gd_txtApogySystemDescription = new GridData(SWT.FILL, SWT.TOP, true, true, 2, 1);
		gd_txtApogySystemDescription.widthHint = 450;
		gd_txtApogySystemDescription.minimumWidth = 450;		
		gd_txtApogySystemDescription.heightHint = 150;
		gd_txtApogySystemDescription.minimumHeight = 150;
		txtApogySystemDescription.setLayoutData(gd_txtApogySystemDescription);
		txtApogySystemDescription.setToolTipText("Description of this Apogy System.");
		
		// Interface Class.
		Label lblInterfaceClass = new Label(this, SWT.NONE);
		lblInterfaceClass.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblInterfaceClass.setText("Interface Class:");
		
		comboApogySystemInterfaceClass = new EClassSelectionComposite(this, SWT.NONE);
		comboApogySystemInterfaceClass.setToolTipText("The interface class this Apogy System is bing used for.");
		GridData gd_comboApogySystemInterfaceClass = new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1);
		gd_comboApogySystemInterfaceClass.minimumWidth = 450;
		gd_comboApogySystemInterfaceClass.widthHint = 450;
		comboApogySystemInterfaceClass.setLayoutData(gd_comboApogySystemInterfaceClass);
				
		// API Adapter Class.
		Label lblApiAdapterClass = new Label(this, SWT.NONE);
		lblApiAdapterClass.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblApiAdapterClass.setText("API Adapter Class:");
		
		comboApogySystemApiAdapterClass = new EClassSelectionComposite(this, SWT.NONE, ApogyCorePackage.Literals.APOGY_SYSTEM_API_ADAPTER);
		comboApogySystemApiAdapterClass.setToolTipText("The interface class this Apogy System is bing used for.");
		GridData gd_comboApogySystemApiAdapterClass = new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1);
		gd_comboApogySystemApiAdapterClass.minimumWidth = 450;
		gd_comboApogySystemApiAdapterClass.widthHint = 450;
		comboApogySystemApiAdapterClass.setLayoutData(gd_comboApogySystemApiAdapterClass);
		
		// Pose Provider
		Label lblPoseProvider = new Label(this, SWT.NONE);
		lblPoseProvider.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPoseProvider.setText("Pose Provider :");
	
		poseProviderSelectionComposite = new TypeTypeMemberSelectionComposite(this, SWT.NONE);
		GridData gd_typeTypeMemberSelectionComposite = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
		gd_typeTypeMemberSelectionComposite.minimumWidth = 450;
		gd_typeTypeMemberSelectionComposite.widthHint = 450;
		poseProviderSelectionComposite.setLayoutData(gd_typeTypeMemberSelectionComposite);
		poseProviderSelectionComposite.getComboViewer().getCombo().setToolTipText("The type member that provides the pose information for this system.");
		
		btnPoseProviderClear = new Button(this, SWT.PUSH);
		btnPoseProviderClear.setText("This");
		btnPoseProviderClear.setToolTipText("Set this system as the pose provider.");
		btnPoseProviderClear.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				poseProviderSelectionComposite.select(null);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) 
			{				
			}
		});
		
		// Dispose listener.
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();			
			}
		});
	}

	public ApogySystem getApogySystem() 
	{
		return apogySystem;
	}

	public void setApogySystem(ApogySystem newApogySystem) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.apogySystem = newApogySystem;				
		
		if(newApogySystem != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}		
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		// Name Value.
		IObservableValue<Double> observeName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCommonEMFPackage.Literals.NAMED__NAME)).observe(getApogySystem());
		IObservableValue<String> observeNameText = WidgetProperties.text(SWT.Modify).observe(txtApogySystemName);

		bindingContext.bindValue(observeNameText,
								 observeName, 
								 new UpdateValueStrategy(),
								 new UpdateValueStrategy());
		
		
		// Description.
		IObservableValue<Double> observeDescription = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  									FeaturePath.fromList(ApogyCommonEMFPackage.Literals.DESCRIBED__DESCRIPTION)).observe(getApogySystem());
		IObservableValue<String> observeDescriptionText = WidgetProperties.text(SWT.Modify).observe(txtApogySystemDescription);
		
		bindingContext.bindValue(observeDescriptionText,
								 observeDescription, 
								 new UpdateValueStrategy(),
								 new UpdateValueStrategy());

		// Interface class.		
		IObservableValue<EClass> observeInterfaceClass = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  										 FeaturePath.fromList(ApogyCoreInvocatorPackage.Literals.TYPE__INTERFACE_CLASS)).observe(getApogySystem());

		IObservableValue<?> observeInterfaceClassComboViewer = ViewerProperties.singleSelection().observe(comboApogySystemInterfaceClass.getComboViewer());
		bindingContext.bindValue(observeInterfaceClassComboViewer, 
								 observeInterfaceClass,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		// Api Adapter Class
		IObservableValue<EClass> observeApiAdapterClass = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
					 FeaturePath.fromList(ApogyCoreInvocatorPackage.Literals.TYPE__TYPE_API_ADAPTER_CLASS)).observe(getApogySystem());

		IObservableValue<?> observeApiAdapterClassComboViewer = ViewerProperties.singleSelection().observe(comboApogySystemApiAdapterClass.getComboViewer());
		bindingContext.bindValue(observeApiAdapterClassComboViewer, 
				observeApiAdapterClass,
				new UpdateValueStrategy(), 
				new UpdateValueStrategy());
		
		// Pose Provider	
		poseProviderSelectionComposite.setType(getApogySystem());
		
		IObservableValue<EClass> observePoseProvider = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				 FeaturePath.fromList(ApogyCorePackage.Literals.APOGY_SYSTEM__POSE_PROVIDER)).observe(getApogySystem());

		IObservableValue<?> observePoseProviderComboViewer = ViewerProperties.singleSelection().observe(poseProviderSelectionComposite.getComboViewer());
				bindingContext.bindValue(observePoseProviderComboViewer, 
				observePoseProvider,
				new UpdateValueStrategy(), 
				new UpdateValueStrategy());
	
		
		return bindingContext;
	}
}
