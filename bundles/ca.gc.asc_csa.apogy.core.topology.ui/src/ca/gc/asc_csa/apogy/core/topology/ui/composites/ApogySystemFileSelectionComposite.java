/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.composites;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.core.ApogyCoreFacade;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.ui.wizards.NewApogySystemWizard;

public class ApogySystemFileSelectionComposite extends Composite 
{
	protected ApogySystem apogySystem;
	protected String apogySystemFilePath;
	
	protected Label lblApogySystemFilePath;
	protected Button btnOpen;
	protected Button btnNew;
	protected Button btnSave;
	
	private EContentAdapter systemAdapter;

	public ApogySystemFileSelectionComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(5, false));
		
		Label fileLabel = new Label(this, SWT.NONE);
		fileLabel.setText("Apogy System File :");
		
		lblApogySystemFilePath = new Label(this, SWT.BORDER);
		lblApogySystemFilePath.setText("");
		lblApogySystemFilePath.setToolTipText("");
		GridData gd_lblApogySystemFilePath = new GridData(SWT.FILL, SWT.CENTER, true, false);
		lblApogySystemFilePath.setLayoutData(gd_lblApogySystemFilePath);
		
		btnOpen = new Button(this, SWT.PUSH);
		btnOpen.setText("Open");
		btnOpen.setToolTipText("Opens an existing Apogy System file.");
		GridData gd_btnOpen = new GridData(SWT.CENTER, SWT.TOP, false,false);
		btnOpen.setLayoutData(gd_btnOpen);
		btnOpen.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				// Get file selection from user.
				FileDialog fd = new FileDialog(getShell(), SWT.OPEN);
		        fd.setText("Select an ApogySystem File");
		        String[] filterExt = { "*.ss" };
		        fd.setFilterExtensions(filterExt);
		        
		        String filePath = fd.open();				
		        if(filePath != null)
		        {
					// Tries to load the file.
					try 
					{
						setApogySystem(ApogyCoreFacade.INSTANCE.loadApogySystemFromFile(filePath));
						setApogySystemFilePath(filePath);
					} 
					catch (Exception e) 
					{
						// TODO : Log event + pop-up.						
						setApogySystem(null);
						setApogySystemFilePath(null);
						e.printStackTrace();
					}
		        }
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {				
			}
		});
		
		btnNew = new Button(this, SWT.PUSH);
		btnNew.setText("New");
		btnNew.setToolTipText("Creates a new Apogy System file.");
		GridData gd_btnNew = new GridData(SWT.CENTER, SWT.TOP, false,false);
		btnNew.setLayoutData(gd_btnNew);
		btnNew.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				// Call the creation wizard.
				NewApogySystemWizard newApogySystemWizard = new NewApogySystemWizard();
				int result = new WizardDialog(ApogySystemFileSelectionComposite.this.getShell(), newApogySystemWizard).open();
						
				if(result == WizardDialog.OK)
				{
					if(newApogySystemWizard.getApogySystem() != null)
					{
						setApogySystem(newApogySystemWizard.getApogySystem());
						
						setApogySystemFilePath(newApogySystemWizard.getFileAbsolutePath());
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {				
			}
		});
		
		btnSave = new Button(this, SWT.PUSH);
		btnSave.setText("Save");
		btnSave.setToolTipText("Saves the Apogy System file.");
		GridData gd_btnSave = new GridData(SWT.CENTER, SWT.TOP, false,false);
		btnSave.setLayoutData(gd_btnSave);
		btnSave.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				try 
				{
					ApogySystemFileSelectionComposite.this.save();
				} catch (Exception e) 
				{
					// TODO : Add pop-up.
					e.printStackTrace();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {				
			}
		});
		
	}
	
	public ApogySystem getApogySystem() {
		return apogySystem;
	}

	
	
	public String getApogySystemFilePath() {
		return apogySystemFilePath;
	}

	public void save() throws Exception
	{	
		if(getApogySystem() != null && getApogySystemFilePath() != null)
		{
			ApogyCoreFacade.INSTANCE.saveApogySystemToFile(getApogySystem() ,getApogySystemFilePath());
		}
		
		setFileIsDirty(false);
	}
	
	private EContentAdapter getSystemAdapter() 
	{
		if(systemAdapter == null)
		{
			systemAdapter = new EContentAdapter()
			{
				@Override
				public void notifyChanged(Notification notification) 
				{				
					super.notifyChanged(notification);
					setFileIsDirty(true);
				}
			};
		}
		return systemAdapter;
	}
	
	private void setApogySystem(final ApogySystem newApogySystem)
	{
		if(this.apogySystem != null)
		{
			this.apogySystem.eAdapters().add(getSystemAdapter());
		}
		
		this.apogySystem = newApogySystem;
		
		if(newApogySystem != null)
		{
			newApogySystem.eAdapters().add(getSystemAdapter());
		}
		
		setFileIsDirty(false);
		
		// Notify sub-classes.
		apogySystemOpened(newApogySystem);
	}

	private void setApogySystemFilePath(final String apogySystemFilePath)
	{
		this.apogySystemFilePath = apogySystemFilePath;		
		getDisplay().asyncExec(new Runnable() {
			
			@Override
			public void run() 
			{
				if(lblApogySystemFilePath != null && !lblApogySystemFilePath.isDisposed())
				{
					if(apogySystemFilePath != null)
					{
						lblApogySystemFilePath.setText(apogySystemFilePath);
						lblApogySystemFilePath.setToolTipText(apogySystemFilePath);
					}
					else
					{
						lblApogySystemFilePath.setText("");
						lblApogySystemFilePath.setToolTipText("");
					}
				}
				
			}
		});
						
		apogySystemPathSet(apogySystemFilePath);
	}
	
	/**
	 * Method called when an ApogySystem is opened. To be overloaded.
	 * @param newApogySystem The ApogySystem loaded, can be null.
	 */
	protected void apogySystemOpened(ApogySystem newApogySystem)
	{		
	}
	
	protected void apogySystemPathSet(String newApogySystemPath)
	{		
	}
	
	protected void setFileIsDirty(boolean dirty)
	{		
	}
}
