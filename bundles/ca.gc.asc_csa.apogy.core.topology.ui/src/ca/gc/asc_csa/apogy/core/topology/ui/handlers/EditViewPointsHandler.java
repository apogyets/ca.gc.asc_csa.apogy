/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation 
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.common.util.EList;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.AbstractApogyEnvironmentItem;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.ViewPointList;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.Environment;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.topology.ui.dialogs.EditViewPointListDialog;
import ca.gc.asc_csa.apogy.core.topology.ui.parts.AbstractApogy3dPart;

public class EditViewPointsHandler 
{
	@CanExecute
	public boolean canExecute(MPart part){
		return ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null;
	}
	
	@Execute
	public void execute(MPart part)
	{
		final Shell shell = Display.getCurrent().getActiveShell();
		EditViewPointListDialog dialog = new EditViewPointListDialog(shell, resolveViewPointList())
		{
			@Override
			protected void newViewPointSelected(AbstractViewPoint viewPoint) 
			{				
				if(viewPoint != null)
				{
					if (part.getObject() instanceof AbstractApogy3dPart)
					{
						AbstractApogy3dPart apogy3dPart = (AbstractApogy3dPart) part.getObject();											
						apogy3dPart.setActiveViewPoint(viewPoint);					
					}
				}
			}
		};
		dialog.open();
	}
	
	protected ViewPointList resolveViewPointList()
	{
		ViewPointList viewPointList = null;
		
		InvocatorSession invocatorSession = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession();
		if(invocatorSession != null)
		{
			Environment env = invocatorSession.getEnvironment();
			if(env instanceof ApogyEnvironment)
			{
				ApogyEnvironment apogyEnvironment = (ApogyEnvironment) env;
				EList<AbstractApogyEnvironmentItem> items = apogyEnvironment.getEnvironmentItems();
				for(AbstractApogyEnvironmentItem item : items)
				{
					if(item instanceof ViewPointList)
					{
						viewPointList = (ViewPointList) item;
					}
				}	
				
				// If no ViewPointList has been found, create one.
				if(viewPointList == null)
				{
					viewPointList = ApogyCoreEnvironmentFactory.eINSTANCE.createViewPointList();
					
					// Adds the list of view points to the current environment.
					ApogyCommonTransactionFacade.INSTANCE.basicAdd(env, ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__ENVIRONMENT_ITEMS, viewPointList);					
				}
			}
		}
		
		
		return viewPointList;
	}
}