package ca.gc.asc_csa.apogy.core.topology.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.core.environment.ViewPointList;
import ca.gc.asc_csa.apogy.core.topology.ui.composites.ViewPointEditComposite;

public class EditViewPointListDialog extends Dialog 
{
	private ViewPointList viewPointList;
	
	private ViewPointEditComposite viewPointEditComposite;
	
	
	public EditViewPointListDialog(Shell parentShell) 
	{
		super(parentShell);		
	}
	
	public EditViewPointListDialog(Shell parentShell, ViewPointList viewPointList) 
	{
		super(parentShell);	
		this.viewPointList = viewPointList;
	}	
	
	@Override
	protected void configureShell(Shell newShell) 
	{	
		super.configureShell(newShell);
		newShell.setText("Add / Remove / Edit View Points");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) 
	{	
		Composite area = (Composite) super.createDialogArea(parent);
		
	    Composite container = new Composite(area, SWT.NONE);	    
	    container.setLayoutData(new GridData(GridData.FILL_BOTH));
	    container.setLayout(new GridLayout(1, false));
	    
	    viewPointEditComposite = new ViewPointEditComposite(container, SWT.NONE)
	    {
	    	@Override
	    	protected void newViewPointSelected(AbstractViewPoint viewPoint) 
	    	{
	    		EditViewPointListDialog.this.newViewPointSelected(viewPoint);
	    	}
	    };
	    viewPointEditComposite.setViewPointList(getViewPointList());
	    viewPointEditComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	    	    	    
	    return area;
	}

	public ViewPointList getViewPointList() 
	{
		return viewPointList;
	}

	public void setViewPointList(ViewPointList viewPointList) 
	{				
		this.viewPointList = viewPointList;
		
		if(viewPointEditComposite != null && !viewPointEditComposite.isDisposed())
		{
			viewPointEditComposite.setViewPointList(viewPointList);			
		}
	}		
	
	protected void newViewPointSelected(AbstractViewPoint viewPoint) 
	{		
	}
}
