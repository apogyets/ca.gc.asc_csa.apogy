/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;

import ca.gc.asc_csa.apogy.core.topology.ui.parts.ApogySystemFileEditorPart;

public class SaveApogySystemFileHandler  {

	@CanExecute
	public boolean canExecute(MPart part)
	{
		if (part.getObject() instanceof ApogySystemFileEditorPart)
		{
			ApogySystemFileEditorPart apogySystemFileEditorPart = (ApogySystemFileEditorPart) part.getObject();
			
			return apogySystemFileEditorPart.getApogySystem() != null && apogySystemFileEditorPart.getApogySystemFilePath() != null;
		}
		else
		{
			return false;
		}		
	}
	
	@Execute
	public void execute(MPart part, final MToolItem item)
	{
		if (part.getObject() instanceof ApogySystemFileEditorPart)
		{
			ApogySystemFileEditorPart apogySystemFileEditorPart = (ApogySystemFileEditorPart) part.getObject();
			
			apogySystemFileEditorPart.saveToFile();
		}
	}

}
