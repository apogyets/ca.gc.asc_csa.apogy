/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFactory;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIPackage;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIRCPConstants;
import ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopology;
import ca.gc.asc_csa.apogy.core.topology.ui.DisplayedTopologyChoice;
import ca.gc.asc_csa.apogy.core.topology.ui.composites.ApogySystemTopologyComposite;

public class ApogySystemTopologyEditorPart extends AbstractEObjectSelectionPart
{
	private ApogySystemTopologyComposite apogySystemTopologyComposite;
	private Adapter editedApogySystemAssemblyRootAdapter = null;
	
	@Override
	public void userPostConstruct(MPart mPart) 
	{
		ApogyCoreTopologyUIFacade.INSTANCE.eAdapters().add(getEditedApogySystemAssemblyRootAdapter());		
		super.userPostConstruct(mPart);
	}
	
	@Override
	public void userPreDestroy(MPart mPart) 
	{
		ApogyCoreTopologyUIFacade.INSTANCE.eAdapters().remove(getEditedApogySystemAssemblyRootAdapter());
		super.userPreDestroy(mPart);
	}
	
	@Override
	protected void setCompositeContents(EObject eObject) 
	{
		if(eObject instanceof ApogySystem)
		{
			apogySystemTopologyComposite.setApogySystem((ApogySystem) eObject);
		}
		else if(eObject instanceof NodeSelection)
		{
			NodeSelection nodeSelection = (NodeSelection) eObject;
			apogySystemTopologyComposite.selectNode(nodeSelection.getSelectedNode());
		}				
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		apogySystemTopologyComposite = new ApogySystemTopologyComposite(parent, SWT.BORDER)
		{
			@Override
			protected void nodeSelected(Node node) 
			{
				selectionService.setSelection(node);
			}
			
			@Override
			protected void newDisplayedTopologySelected(DisplayedTopology newDisplayedTopology) 
			{
				DisplayedTopologyChoice displayedTopologyChoice = ApogyCoreTopologyUIFactory.eINSTANCE.createDisplayedTopologyChoice();
				displayedTopologyChoice.setDisplayedTopology(newDisplayedTopology);
				selectionService.setSelection(displayedTopologyChoice);
			}
		};
		
		apogySystemTopologyComposite.setAssemblyRoot(ApogyCoreTopologyUIFacade.INSTANCE.getEditedApogySystemAssemblyRoot());
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() 
	{
		HashMap<String, ISelectionListener> selectionProvidersIdsToSelectionListeners = new HashMap<String, ISelectionListener>();

		// Selection from System 3D viewer.
		selectionProvidersIdsToSelectionListeners.put(ApogyCoreTopologyUIRCPConstants.PART__APOGY_SYSTEM_3D__ID, new ISelectionListener() {
			
			@Override
			public void selectionChanged(MPart mPart, Object object) 
			{				
				if(object == null || object instanceof EObject)
				{
					setEObject((EObject) object);
				}				
			}
		});

		// Listens to the Apogy System File Editor
		selectionProvidersIdsToSelectionListeners.put(ApogyCoreTopologyUIRCPConstants.PART__APOGY_SYSTEM_FILE_EDITOR_ID, new ISelectionListener() {
			
			@Override
			public void selectionChanged(MPart mPart, Object object) 
			{				
				if(object == null || object instanceof EObject)
				{
					setEObject((EObject) object);
				}				
			}
		});		
		
		return selectionProvidersIdsToSelectionListeners;
	}

	protected Adapter getEditedApogySystemAssemblyRootAdapter() 
	{
		if(editedApogySystemAssemblyRootAdapter == null)
		{
			editedApogySystemAssemblyRootAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ApogyCoreTopologyUIFacade)
					{
						int featureId = msg.getFeatureID(ApogyCoreTopologyUIFacade.class);
						if(featureId == ApogyCoreTopologyUIPackage.APOGY_CORE_TOPOLOGY_UI_FACADE__EDITED_APOGY_SYSTEM_ASSEMBLY_ROOT)
						{							
							if(apogySystemTopologyComposite != null && !apogySystemTopologyComposite.isDisposed())
							{
								Node root = ApogyCoreTopologyUIFacade.INSTANCE.getEditedApogySystemAssemblyRoot();
								apogySystemTopologyComposite.setAssemblyRoot(root);
							}
						}
					}
				}
			};
		}
		return editedApogySystemAssemblyRootAdapter;
	}
}
