/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.parts;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.progress.UIJob;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.TreeRootNode;
import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractPart;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsList;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsSet;
import ca.gc.asc_csa.apogy.common.topology.bindings.FeatureRootsList;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCoreFactory;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.AssemblyLinksList;
import ca.gc.asc_csa.apogy.core.ConnectionPointsList;
import ca.gc.asc_csa.apogy.core.TopologyRoot;
import ca.gc.asc_csa.apogy.core.topology.ui.ApogyCoreTopologyUIFacade;
import ca.gc.asc_csa.apogy.core.topology.ui.composites.ApogySystemEditingComposite;
import ca.gc.asc_csa.apogy.core.topology.ui.internal.ApogySystem3dUtils;

public class ApogySystemFileEditorPart extends AbstractPart
{
	private ApogySystemEditingComposite apogySystemEditingComposite;
	
	private boolean dirty = false;	
	private String fileName = null;
	
	private ApogySystem apogySystem;
	private String apogySystemFilePath;
	
	
	public ApogySystem getApogySystem()
	{
		return apogySystem;
	}

	public String getApogySystemFilePath() 
	{
		return apogySystemFilePath;
	}

	public void saveToFile()
	{		
		try 
		{
			apogySystemEditingComposite.save();
		} 
		catch (Exception e) 
		{		
			e.printStackTrace();
		}
	}
	
	@Override
	public void userPostConstruct(MPart mPart) {
		super.userPostConstruct(mPart);	
	}
	
	@Override
	public void userPreDestroy(MPart mPart) 
	{
		// Clear the assembly topology.
		ApogyCoreTopologyUIFacade.INSTANCE.setEditedApogySystemAssemblyRoot(null);
		
		super.userPreDestroy(mPart);
	}
	
	@Override
	protected EObject getInitializeObject() 
	{	
		return null;
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{
		apogySystemEditingComposite = new ApogySystemEditingComposite(parent, style)
		{
			@Override
			protected void setFileIsDirty(boolean newDirty) 
			{				
				if(newDirty)
				{
					// Update the assembly root.
					if(getApogySystem() != null)
					{
						UIJob updateJob = new UIJob("Update Assembly Topology")
						{
							@Override
							public IStatus runInUIThread(IProgressMonitor arg0) 
							{
								try
								{
									ApogySystem as = getApogySystem();
									if(as != null)
									{										
										ApogyCoreTopologyUIFacade.INSTANCE.setEditedApogySystemAssemblyRoot(ApogySystem3dUtils.assembleSubSystem(as));
									}
								}
								catch (Exception e) 
								{
									e.printStackTrace();							
								}
								return Status.OK_STATUS;
							}
						};		
						updateJob.schedule();
					}
				}
				
				dirty = newDirty;
				
				// Update the dirty file asterix.
				mPart.setLabel(getPartDisplayedName());
			}
			
			@Override
			public void setApogySystem(ca.gc.asc_csa.apogy.core.ApogySystem newApogySystem) 
			{
				apogySystem = newApogySystem;
				
				fixApogySystem(newApogySystem);
				
				super.setApogySystem(newApogySystem);
				
				// Raise a selection containing the Apogy System.
				selectionService.setSelection(newApogySystem);
				
				// Create the system assembly topology and raise a selection.
				if(newApogySystem != null)
				{
					ApogyCoreTopologyUIFacade.INSTANCE.setEditedApogySystemAssemblyRoot(ApogySystem3dUtils.assembleSubSystem(newApogySystem));
				}				
			}
			
			@Override
			protected void apogySystemPathSet(String newApogySystemPath) 
			{				
				apogySystemFilePath = newApogySystemPath;
				
				if(newApogySystemPath != null)
				{
					fileName = newApogySystemPath;
				}
				else
				{
					fileName = null;
				}
			
				// Update displayed name.
				mPart.setLabel(getPartDisplayedName());
			}
		};
	}

		
	@Override
	protected void setCompositeContent(EObject eObject) 
	{
		if(apogySystemEditingComposite != null && !apogySystemEditingComposite.isDisposed())
		{
			apogySystemEditingComposite.setApogySystem((ApogySystem) eObject);
		}
	}

	@Override
	protected void createNoContentComposite(Composite parent, int style) 
	{
		createContentComposite( parent, style);
	}
	
	private String getPartDisplayedName()
	{
		String label = "";
				
		if(fileName != null)
		{
			label += fileName.substring(fileName.lastIndexOf(File.separator) + 1);
			
			if(dirty)
			{
				label = "*" + label;
			}
		}
		else
		{
			label += "System Details";
		}
		
		return label;
	}
	
	private void fixApogySystem(ApogySystem apogySystem)
	{
		if(apogySystem != null)
		{
			if(apogySystem.getAssemblyLinksList() == null)
			{
				AssemblyLinksList assemblyLinksList =  ApogyCoreFactory.eINSTANCE.createAssemblyLinksList();
				ApogyCommonTransactionFacade.INSTANCE.basicSet(apogySystem, ApogyCorePackage.Literals.APOGY_SYSTEM__ASSEMBLY_LINKS_LIST, assemblyLinksList);
			}
			
			if(apogySystem.getBindingSet() == null)
			{
				BindingsSet bindingsSet = ApogyCommonTopologyBindingsFactory.eINSTANCE.createBindingsSet();
				bindingsSet.setFeatureRootsList(ApogyCommonTopologyBindingsFactory.eINSTANCE.createFeatureRootsList());
				bindingsSet.setBindingsList(ApogyCommonTopologyBindingsFactory.eINSTANCE.createBindingsList());
				
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(apogySystem, ApogyCorePackage.Literals.APOGY_SYSTEM__BINDING_SET, bindingsSet);
			}
			else
			{
				if(apogySystem.getBindingSet().getFeatureRootsList() == null)
				{
					FeatureRootsList featureRootsList = ApogyCommonTopologyBindingsFactory.eINSTANCE.createFeatureRootsList();
					
					TreeRootNode treeRoot = ApogyCommonEMFFactory.eINSTANCE.createTreeRootNode();
					treeRoot.setSourceClass(apogySystem.getInterfaceClass());
					
					featureRootsList.getFeatureRoots().add(treeRoot);
					ApogyCommonTransactionFacade.INSTANCE.basicSet(apogySystem.getBindingSet(), ApogyCommonTopologyBindingsPackage.Literals.BINDINGS_SET__FEATURE_ROOTS_LIST, featureRootsList);
				}
				
				if(apogySystem.getBindingSet().getFeatureRootsList().getFeatureRoots().isEmpty())
				{
					if(apogySystem.getInterfaceClass() != null)
					{
						TreeRootNode treeRoot = ApogyCommonEMFFactory.eINSTANCE.createTreeRootNode();
						treeRoot.setSourceClass(apogySystem.getInterfaceClass());
						
						ApogyCommonTransactionFacade.INSTANCE.basicAdd(apogySystem.getBindingSet().getFeatureRootsList(), 
																	  ApogyCommonTopologyBindingsPackage.Literals.FEATURE_ROOTS_LIST__FEATURE_ROOTS, 
																	  treeRoot);
					}
				}
				
				if(apogySystem.getBindingSet().getBindingsList() == null)
				{
					BindingsList bindingsList = ApogyCommonTopologyBindingsFactory.eINSTANCE.createBindingsList();
					ApogyCommonTransactionFacade.INSTANCE.basicSet(apogySystem.getBindingSet(), ApogyCommonTopologyBindingsPackage.Literals.BINDINGS_SET__BINDINGS_LIST, bindingsList);
				}
			}
			
			
			if(apogySystem.getConnectionPointsList() == null)
			{
				ConnectionPointsList connectionPointsList = ApogyCoreFactory.eINSTANCE.createConnectionPointsList();
				ApogyCommonTransactionFacade.INSTANCE.basicSet(apogySystem, ApogyCorePackage.Literals.APOGY_SYSTEM__CONNECTION_POINTS_LIST, connectionPointsList);
			}
			
			if(apogySystem.getTopologyRoot() == null)
			{
				TopologyRoot topologyRoot = ApogyCoreFactory.eINSTANCE.createTopologyRoot();
				ApogyCommonTransactionFacade.INSTANCE.basicSet(apogySystem, ApogyCorePackage.Literals.APOGY_SYSTEM__TOPOLOGY_ROOT, topologyRoot);
			}
		}
	}
		
}
