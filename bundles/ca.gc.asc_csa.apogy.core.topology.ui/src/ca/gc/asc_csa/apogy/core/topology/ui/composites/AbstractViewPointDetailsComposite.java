/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.topology.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;

public class AbstractViewPointDetailsComposite extends Composite 
{
	private AbstractViewPoint abstractViewPoint;
		
	private Composite compositeDetails;
	
	public AbstractViewPointDetailsComposite(Composite parent, int style) 
	{		
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.minimumWidth = 398;
		gd_compositeDetails.widthHint = 398;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));		
	}

	public AbstractViewPoint getAbstractViewPoint() {
		return abstractViewPoint;
	}

	public void setAbstractViewPoint(AbstractViewPoint newAbstractViewPoint) 
	{
		this.abstractViewPoint = newAbstractViewPoint;
	
		// Update Details EMFForm.
		if(newAbstractViewPoint != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newAbstractViewPoint);
		}
	}		
}
