/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui;

import ca.gc.asc_csa.apogy.common.topology.Node;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Orbit Model Pass Tool Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Specialized Node that represents the EarthOrbitModelPassTool in the topology.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode#getEarthOrbitModelPassTool <em>Earth Orbit Model Pass Tool</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelPassToolNode()
 * @model
 * @generated
 */
public interface EarthOrbitModelPassToolNode extends Node {
	/**
	 * Returns the value of the '<em><b>Earth Orbit Model Pass Tool</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getEarthOrbitModelPassToolNode <em>Earth Orbit Model Pass Tool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Earth Orbit Model Pass Tool</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Earth Orbit Model Pass Tool</em>' reference.
	 * @see #setEarthOrbitModelPassTool(EarthOrbitModelPassTool)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelPassToolNode_EarthOrbitModelPassTool()
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getEarthOrbitModelPassToolNode
	 * @model opposite="earthOrbitModelPassToolNode" transient="true"
	 * @generated
	 */
	EarthOrbitModelPassTool getEarthOrbitModelPassTool();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode#getEarthOrbitModelPassTool <em>Earth Orbit Model Pass Tool</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earth Orbit Model Pass Tool</em>' reference.
	 * @see #getEarthOrbitModelPassTool()
	 * @generated
	 */
	void setEarthOrbitModelPassTool(EarthOrbitModelPassTool value);

} // EarthOrbitModelPassToolNode
