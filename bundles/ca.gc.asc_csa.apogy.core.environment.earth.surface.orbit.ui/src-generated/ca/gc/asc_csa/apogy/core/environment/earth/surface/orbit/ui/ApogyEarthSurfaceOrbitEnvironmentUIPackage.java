/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * ******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque,
 *     Sebastien Gemme
 * 
 * SPDX-License-Identifier: EPL-1.0
 * *****************************************************************************
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyEarthSurfaceOrbitEnvironmentUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyEarthSurfaceOrbitEnvironmentUI' complianceLevel='6.0' suppressGenModelAnnotations='false' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit'"
 * @generated
 */
public interface ApogyEarthSurfaceOrbitEnvironmentUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyEarthSurfaceOrbitEnvironmentUIPackage eINSTANCE = ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl <em>Earth Orbit Model Tool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitModelTool()
	 * @generated
	 */
	int EARTH_ORBIT_MODEL_TOOL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__NAME = ApogyAddonsPackage.SIMPLE3_DTOOL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__DESCRIPTION = ApogyAddonsPackage.SIMPLE3_DTOOL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tool List</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__TOOL_LIST = ApogyAddonsPackage.SIMPLE3_DTOOL__TOOL_LIST;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__ACTIVE = ApogyAddonsPackage.SIMPLE3_DTOOL__ACTIVE;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__DISPOSED = ApogyAddonsPackage.SIMPLE3_DTOOL__DISPOSED;

	/**
	 * The feature id for the '<em><b>Initialized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__INITIALIZED = ApogyAddonsPackage.SIMPLE3_DTOOL__INITIALIZED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__VISIBLE = ApogyAddonsPackage.SIMPLE3_DTOOL__VISIBLE;

	/**
	 * The feature id for the '<em><b>Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__ROOT_NODE = ApogyAddonsPackage.SIMPLE3_DTOOL__ROOT_NODE;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__UPDATING = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Earth Orbit Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Active Time Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Active Earth Surface Worksite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Earth Orbit Model Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___INITIALISE = ApogyAddonsPackage.SIMPLE3_DTOOL___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___DISPOSE = ApogyAddonsPackage.SIMPLE3_DTOOL___DISPOSE;

	/**
	 * The operation id for the '<em>Variables Instantiated</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___VARIABLES_INSTANTIATED = ApogyAddonsPackage.SIMPLE3_DTOOL___VARIABLES_INSTANTIATED;

	/**
	 * The operation id for the '<em>Variables Cleared</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___VARIABLES_CLEARED = ApogyAddonsPackage.SIMPLE3_DTOOL___VARIABLES_CLEARED;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___SELECTION_CHANGED__NODESELECTION = ApogyAddonsPackage.SIMPLE3_DTOOL___SELECTION_CHANGED__NODESELECTION;

	/**
	 * The operation id for the '<em>Mouse Button Clicked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON = ApogyAddonsPackage.SIMPLE3_DTOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___GET_DEFAULT_AUTO_UPDATE_ENABLED = ApogyAddonsPackage.SIMPLE3_DTOOL_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___UPDATE = ApogyAddonsPackage.SIMPLE3_DTOOL_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Update Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL___UPDATE_TIME__DATE = ApogyAddonsPackage.SIMPLE3_DTOOL_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Earth Orbit Model Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_TOOL_OPERATION_COUNT = ApogyAddonsPackage.SIMPLE3_DTOOL_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolImpl <em>Earth Orbiting Spacecraft Location Tool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitingSpacecraftLocationTool()
	 * @generated
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__NAME = EARTH_ORBIT_MODEL_TOOL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__DESCRIPTION = EARTH_ORBIT_MODEL_TOOL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tool List</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__TOOL_LIST = EARTH_ORBIT_MODEL_TOOL__TOOL_LIST;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__ACTIVE = EARTH_ORBIT_MODEL_TOOL__ACTIVE;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__DISPOSED = EARTH_ORBIT_MODEL_TOOL__DISPOSED;

	/**
	 * The feature id for the '<em><b>Initialized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__INITIALIZED = EARTH_ORBIT_MODEL_TOOL__INITIALIZED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__VISIBLE = EARTH_ORBIT_MODEL_TOOL__VISIBLE;

	/**
	 * The feature id for the '<em><b>Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__ROOT_NODE = EARTH_ORBIT_MODEL_TOOL__ROOT_NODE;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__UPDATING = EARTH_ORBIT_MODEL_TOOL__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__AUTO_UPDATE_ENABLED = EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Earth Orbit Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBIT_MODEL = EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL;

	/**
	 * The feature id for the '<em><b>Active Time Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__ACTIVE_TIME_SOURCE = EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE;

	/**
	 * The feature id for the '<em><b>Active Earth Surface Worksite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE = EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE;

	/**
	 * The feature id for the '<em><b>Show Vector</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Spacecraft Position</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Earth Orbiting Spacecraft Location Tool Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Earth Orbiting Spacecraft Location Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_FEATURE_COUNT = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___INITIALISE = EARTH_ORBIT_MODEL_TOOL___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___DISPOSE = EARTH_ORBIT_MODEL_TOOL___DISPOSE;

	/**
	 * The operation id for the '<em>Variables Instantiated</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___VARIABLES_INSTANTIATED = EARTH_ORBIT_MODEL_TOOL___VARIABLES_INSTANTIATED;

	/**
	 * The operation id for the '<em>Variables Cleared</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___VARIABLES_CLEARED = EARTH_ORBIT_MODEL_TOOL___VARIABLES_CLEARED;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___SELECTION_CHANGED__NODESELECTION = EARTH_ORBIT_MODEL_TOOL___SELECTION_CHANGED__NODESELECTION;

	/**
	 * The operation id for the '<em>Mouse Button Clicked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON = EARTH_ORBIT_MODEL_TOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___GET_DEFAULT_AUTO_UPDATE_ENABLED = EARTH_ORBIT_MODEL_TOOL___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___UPDATE = EARTH_ORBIT_MODEL_TOOL___UPDATE;

	/**
	 * The operation id for the '<em>Update Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL___UPDATE_TIME__DATE = EARTH_ORBIT_MODEL_TOOL___UPDATE_TIME__DATE;

	/**
	 * The number of operations of the '<em>Earth Orbiting Spacecraft Location Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_OPERATION_COUNT = EARTH_ORBIT_MODEL_TOOL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolNodeImpl <em>Earth Orbiting Spacecraft Location Tool Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolNodeImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitingSpacecraftLocationToolNode()
	 * @generated
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE = 2;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__PARENT = ApogyCommonTopologyPackage.NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__DESCRIPTION = ApogyCommonTopologyPackage.NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__NODE_ID = ApogyCommonTopologyPackage.NODE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Earth Orbiting Spacecraft Location Tool</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL = ApogyCommonTopologyPackage.NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Earth Orbiting Spacecraft Location Tool Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE_FEATURE_COUNT = ApogyCommonTopologyPackage.NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Accept</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE___ACCEPT__INODEVISITOR = ApogyCommonTopologyPackage.NODE___ACCEPT__INODEVISITOR;

	/**
	 * The number of operations of the '<em>Earth Orbiting Spacecraft Location Tool Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE_OPERATION_COUNT = ApogyCommonTopologyPackage.NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl <em>Earth Orbit Model Pass Tool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitModelPassTool()
	 * @generated
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__NAME = EARTH_ORBIT_MODEL_TOOL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__DESCRIPTION = EARTH_ORBIT_MODEL_TOOL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tool List</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__TOOL_LIST = EARTH_ORBIT_MODEL_TOOL__TOOL_LIST;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__ACTIVE = EARTH_ORBIT_MODEL_TOOL__ACTIVE;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__DISPOSED = EARTH_ORBIT_MODEL_TOOL__DISPOSED;

	/**
	 * The feature id for the '<em><b>Initialized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__INITIALIZED = EARTH_ORBIT_MODEL_TOOL__INITIALIZED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__VISIBLE = EARTH_ORBIT_MODEL_TOOL__VISIBLE;

	/**
	 * The feature id for the '<em><b>Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__ROOT_NODE = EARTH_ORBIT_MODEL_TOOL__ROOT_NODE;

	/**
	 * The feature id for the '<em><b>Updating</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__UPDATING = EARTH_ORBIT_MODEL_TOOL__UPDATING;

	/**
	 * The feature id for the '<em><b>Auto Update Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__AUTO_UPDATE_ENABLED = EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED;

	/**
	 * The feature id for the '<em><b>Earth Orbit Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL = EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL;

	/**
	 * The feature id for the '<em><b>Active Time Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__ACTIVE_TIME_SOURCE = EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE;

	/**
	 * The feature id for the '<em><b>Active Earth Surface Worksite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE = EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE;

	/**
	 * The feature id for the '<em><b>Look Ahead Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Last Passes Update Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Spacecrafts Visibility Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Displayed Pass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Earth Orbit Model Pass Tool Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Earth Orbit Model Pass Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_FEATURE_COUNT = EARTH_ORBIT_MODEL_TOOL_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___INITIALISE = EARTH_ORBIT_MODEL_TOOL___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___DISPOSE = EARTH_ORBIT_MODEL_TOOL___DISPOSE;

	/**
	 * The operation id for the '<em>Variables Instantiated</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___VARIABLES_INSTANTIATED = EARTH_ORBIT_MODEL_TOOL___VARIABLES_INSTANTIATED;

	/**
	 * The operation id for the '<em>Variables Cleared</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___VARIABLES_CLEARED = EARTH_ORBIT_MODEL_TOOL___VARIABLES_CLEARED;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___SELECTION_CHANGED__NODESELECTION = EARTH_ORBIT_MODEL_TOOL___SELECTION_CHANGED__NODESELECTION;

	/**
	 * The operation id for the '<em>Mouse Button Clicked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON = EARTH_ORBIT_MODEL_TOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON;

	/**
	 * The operation id for the '<em>Get Default Auto Update Enabled</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___GET_DEFAULT_AUTO_UPDATE_ENABLED = EARTH_ORBIT_MODEL_TOOL___GET_DEFAULT_AUTO_UPDATE_ENABLED;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___UPDATE = EARTH_ORBIT_MODEL_TOOL___UPDATE;

	/**
	 * The operation id for the '<em>Update Time</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL___UPDATE_TIME__DATE = EARTH_ORBIT_MODEL_TOOL___UPDATE_TIME__DATE;

	/**
	 * The number of operations of the '<em>Earth Orbit Model Pass Tool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_OPERATION_COUNT = EARTH_ORBIT_MODEL_TOOL_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolNodeImpl <em>Earth Orbit Model Pass Tool Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolNodeImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitModelPassToolNode()
	 * @generated
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_NODE = 4;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_NODE__PARENT = ApogyCommonTopologyPackage.NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_NODE__DESCRIPTION = ApogyCommonTopologyPackage.NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_NODE__NODE_ID = ApogyCommonTopologyPackage.NODE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Earth Orbit Model Pass Tool</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL = ApogyCommonTopologyPackage.NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Earth Orbit Model Pass Tool Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_NODE_FEATURE_COUNT = ApogyCommonTopologyPackage.NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Accept</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_NODE___ACCEPT__INODEVISITOR = ApogyCommonTopologyPackage.NODE___ACCEPT__INODEVISITOR;

	/**
	 * The number of operations of the '<em>Earth Orbit Model Pass Tool Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ORBIT_MODEL_PASS_TOOL_NODE_OPERATION_COUNT = ApogyCommonTopologyPackage.NODE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool <em>Earth Orbit Model Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Orbit Model Tool</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool
	 * @generated
	 */
	EClass getEarthOrbitModelTool();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getEarthOrbitModel <em>Earth Orbit Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Earth Orbit Model</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getEarthOrbitModel()
	 * @see #getEarthOrbitModelTool()
	 * @generated
	 */
	EReference getEarthOrbitModelTool_EarthOrbitModel();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getActiveTimeSource <em>Active Time Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Active Time Source</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getActiveTimeSource()
	 * @see #getEarthOrbitModelTool()
	 * @generated
	 */
	EReference getEarthOrbitModelTool_ActiveTimeSource();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getActiveEarthSurfaceWorksite <em>Active Earth Surface Worksite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Active Earth Surface Worksite</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#getActiveEarthSurfaceWorksite()
	 * @see #getEarthOrbitModelTool()
	 * @generated
	 */
	EReference getEarthOrbitModelTool_ActiveEarthSurfaceWorksite();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#updateTime(java.util.Date) <em>Update Time</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Time</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool#updateTime(java.util.Date)
	 * @generated
	 */
	EOperation getEarthOrbitModelTool__UpdateTime__Date();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool <em>Earth Orbiting Spacecraft Location Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Orbiting Spacecraft Location Tool</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool
	 * @generated
	 */
	EClass getEarthOrbitingSpacecraftLocationTool();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#isShowVector <em>Show Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Show Vector</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#isShowVector()
	 * @see #getEarthOrbitingSpacecraftLocationTool()
	 * @generated
	 */
	EAttribute getEarthOrbitingSpacecraftLocationTool_ShowVector();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getSpacecraftPosition <em>Spacecraft Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Spacecraft Position</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getSpacecraftPosition()
	 * @see #getEarthOrbitingSpacecraftLocationTool()
	 * @generated
	 */
	EReference getEarthOrbitingSpacecraftLocationTool_SpacecraftPosition();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getEarthOrbitingSpacecraftLocationToolNode <em>Earth Orbiting Spacecraft Location Tool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Earth Orbiting Spacecraft Location Tool Node</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool#getEarthOrbitingSpacecraftLocationToolNode()
	 * @see #getEarthOrbitingSpacecraftLocationTool()
	 * @generated
	 */
	EReference getEarthOrbitingSpacecraftLocationTool_EarthOrbitingSpacecraftLocationToolNode();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode <em>Earth Orbiting Spacecraft Location Tool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Orbiting Spacecraft Location Tool Node</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode
	 * @generated
	 */
	EClass getEarthOrbitingSpacecraftLocationToolNode();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode#getEarthOrbitingSpacecraftLocationTool <em>Earth Orbiting Spacecraft Location Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Earth Orbiting Spacecraft Location Tool</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode#getEarthOrbitingSpacecraftLocationTool()
	 * @see #getEarthOrbitingSpacecraftLocationToolNode()
	 * @generated
	 */
	EReference getEarthOrbitingSpacecraftLocationToolNode_EarthOrbitingSpacecraftLocationTool();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool <em>Earth Orbit Model Pass Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Orbit Model Pass Tool</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool
	 * @generated
	 */
	EClass getEarthOrbitModelPassTool();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getLookAheadPeriod <em>Look Ahead Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Look Ahead Period</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getLookAheadPeriod()
	 * @see #getEarthOrbitModelPassTool()
	 * @generated
	 */
	EAttribute getEarthOrbitModelPassTool_LookAheadPeriod();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getLastPassesUpdateTime <em>Last Passes Update Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Passes Update Time</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getLastPassesUpdateTime()
	 * @see #getEarthOrbitModelPassTool()
	 * @generated
	 */
	EAttribute getEarthOrbitModelPassTool_LastPassesUpdateTime();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getSpacecraftsVisibilitySet <em>Spacecrafts Visibility Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Spacecrafts Visibility Set</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getSpacecraftsVisibilitySet()
	 * @see #getEarthOrbitModelPassTool()
	 * @generated
	 */
	EReference getEarthOrbitModelPassTool_SpacecraftsVisibilitySet();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getDisplayedPass <em>Displayed Pass</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Displayed Pass</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getDisplayedPass()
	 * @see #getEarthOrbitModelPassTool()
	 * @generated
	 */
	EReference getEarthOrbitModelPassTool_DisplayedPass();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getEarthOrbitModelPassToolNode <em>Earth Orbit Model Pass Tool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Earth Orbit Model Pass Tool Node</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getEarthOrbitModelPassToolNode()
	 * @see #getEarthOrbitModelPassTool()
	 * @generated
	 */
	EReference getEarthOrbitModelPassTool_EarthOrbitModelPassToolNode();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode <em>Earth Orbit Model Pass Tool Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Orbit Model Pass Tool Node</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode
	 * @generated
	 */
	EClass getEarthOrbitModelPassToolNode();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode#getEarthOrbitModelPassTool <em>Earth Orbit Model Pass Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Earth Orbit Model Pass Tool</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode#getEarthOrbitModelPassTool()
	 * @see #getEarthOrbitModelPassToolNode()
	 * @generated
	 */
	EReference getEarthOrbitModelPassToolNode_EarthOrbitModelPassTool();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyEarthSurfaceOrbitEnvironmentUIFactory getApogyEarthSurfaceOrbitEnvironmentUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl <em>Earth Orbit Model Tool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitModelTool()
		 * @generated
		 */
		EClass EARTH_ORBIT_MODEL_TOOL = eINSTANCE.getEarthOrbitModelTool();

		/**
		 * The meta object literal for the '<em><b>Earth Orbit Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL = eINSTANCE.getEarthOrbitModelTool_EarthOrbitModel();

		/**
		 * The meta object literal for the '<em><b>Active Time Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE = eINSTANCE.getEarthOrbitModelTool_ActiveTimeSource();

		/**
		 * The meta object literal for the '<em><b>Active Earth Surface Worksite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE = eINSTANCE.getEarthOrbitModelTool_ActiveEarthSurfaceWorksite();

		/**
		 * The meta object literal for the '<em><b>Update Time</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EARTH_ORBIT_MODEL_TOOL___UPDATE_TIME__DATE = eINSTANCE.getEarthOrbitModelTool__UpdateTime__Date();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolImpl <em>Earth Orbiting Spacecraft Location Tool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitingSpacecraftLocationTool()
		 * @generated
		 */
		EClass EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL = eINSTANCE.getEarthOrbitingSpacecraftLocationTool();

		/**
		 * The meta object literal for the '<em><b>Show Vector</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR = eINSTANCE.getEarthOrbitingSpacecraftLocationTool_ShowVector();

		/**
		 * The meta object literal for the '<em><b>Spacecraft Position</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION = eINSTANCE.getEarthOrbitingSpacecraftLocationTool_SpacecraftPosition();

		/**
		 * The meta object literal for the '<em><b>Earth Orbiting Spacecraft Location Tool Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE = eINSTANCE.getEarthOrbitingSpacecraftLocationTool_EarthOrbitingSpacecraftLocationToolNode();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolNodeImpl <em>Earth Orbiting Spacecraft Location Tool Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitingSpacecraftLocationToolNodeImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitingSpacecraftLocationToolNode()
		 * @generated
		 */
		EClass EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE = eINSTANCE.getEarthOrbitingSpacecraftLocationToolNode();

		/**
		 * The meta object literal for the '<em><b>Earth Orbiting Spacecraft Location Tool</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL = eINSTANCE.getEarthOrbitingSpacecraftLocationToolNode_EarthOrbitingSpacecraftLocationTool();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl <em>Earth Orbit Model Pass Tool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitModelPassTool()
		 * @generated
		 */
		EClass EARTH_ORBIT_MODEL_PASS_TOOL = eINSTANCE.getEarthOrbitModelPassTool();

		/**
		 * The meta object literal for the '<em><b>Look Ahead Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD = eINSTANCE.getEarthOrbitModelPassTool_LookAheadPeriod();

		/**
		 * The meta object literal for the '<em><b>Last Passes Update Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME = eINSTANCE.getEarthOrbitModelPassTool_LastPassesUpdateTime();

		/**
		 * The meta object literal for the '<em><b>Spacecrafts Visibility Set</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET = eINSTANCE.getEarthOrbitModelPassTool_SpacecraftsVisibilitySet();

		/**
		 * The meta object literal for the '<em><b>Displayed Pass</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS = eINSTANCE.getEarthOrbitModelPassTool_DisplayedPass();

		/**
		 * The meta object literal for the '<em><b>Earth Orbit Model Pass Tool Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE = eINSTANCE.getEarthOrbitModelPassTool_EarthOrbitModelPassToolNode();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolNodeImpl <em>Earth Orbit Model Pass Tool Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolNodeImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl#getEarthOrbitModelPassToolNode()
		 * @generated
		 */
		EClass EARTH_ORBIT_MODEL_PASS_TOOL_NODE = eINSTANCE.getEarthOrbitModelPassToolNode();

		/**
		 * The meta object literal for the '<em><b>Earth Orbit Model Pass Tool</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL = eINSTANCE.getEarthOrbitModelPassToolNode_EarthOrbitModelPassTool();

	}

} //ApogyEarthSurfaceOrbitEnvironmentUIPackage
