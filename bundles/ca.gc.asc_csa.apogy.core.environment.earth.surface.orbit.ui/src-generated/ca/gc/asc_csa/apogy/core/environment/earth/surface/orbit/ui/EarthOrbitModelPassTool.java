/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui;

import ca.gc.asc_csa.apogy.core.environment.orbit.earth.SpacecraftsVisibilitySet;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.VisibilityPass;
import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Orbit Model Pass Tool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Tool used to display a spacecraft passes as viewed from a surface worksite.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getLookAheadPeriod <em>Look Ahead Period</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getLastPassesUpdateTime <em>Last Passes Update Time</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getSpacecraftsVisibilitySet <em>Spacecrafts Visibility Set</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getDisplayedPass <em>Displayed Pass</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getEarthOrbitModelPassToolNode <em>Earth Orbit Model Pass Tool Node</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelPassTool()
 * @model
 * @generated
 */
public interface EarthOrbitModelPassTool extends EarthOrbitModelTool {
	/**
	 * Returns the value of the '<em><b>Look Ahead Period</b></em>' attribute.
	 * The default value is <code>"43200"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Look Ahead period use to decide when passes must be recomputed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Look Ahead Period</em>' attribute.
	 * @see #setLookAheadPeriod(long)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelPassTool_LookAheadPeriod()
	 * @model default="43200" unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Editable' apogy_units='s'"
	 * @generated
	 */
	long getLookAheadPeriod();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getLookAheadPeriod <em>Look Ahead Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Look Ahead Period</em>' attribute.
	 * @see #getLookAheadPeriod()
	 * @generated
	 */
	void setLookAheadPeriod(long value);

	/**
	 * Returns the value of the '<em><b>Last Passes Update Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Passes Update Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Passes Update Time</em>' attribute.
	 * @see #setLastPassesUpdateTime(Date)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelPassTool_LastPassesUpdateTime()
	 * @model unique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='None'"
	 * @generated
	 */
	Date getLastPassesUpdateTime();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getLastPassesUpdateTime <em>Last Passes Update Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Passes Update Time</em>' attribute.
	 * @see #getLastPassesUpdateTime()
	 * @generated
	 */
	void setLastPassesUpdateTime(Date value);

	/**
	 * Returns the value of the '<em><b>Spacecrafts Visibility Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The SpacecraftsVisibilitySet containing the passes.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Spacecrafts Visibility Set</em>' reference.
	 * @see #setSpacecraftsVisibilitySet(SpacecraftsVisibilitySet)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelPassTool_SpacecraftsVisibilitySet()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Readonly' children='true'"
	 * @generated
	 */
	SpacecraftsVisibilitySet getSpacecraftsVisibilitySet();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getSpacecraftsVisibilitySet <em>Spacecrafts Visibility Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Spacecrafts Visibility Set</em>' reference.
	 * @see #getSpacecraftsVisibilitySet()
	 * @generated
	 */
	void setSpacecraftsVisibilitySet(SpacecraftsVisibilitySet value);

	/**
	 * Returns the value of the '<em><b>Displayed Pass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The pass currently displayed. Can be null.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Displayed Pass</em>' reference.
	 * @see #setDisplayedPass(VisibilityPass)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelPassTool_DisplayedPass()
	 * @model transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel property='Readonly' children='true' notify='true'"
	 * @generated
	 */
	VisibilityPass getDisplayedPass();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getDisplayedPass <em>Displayed Pass</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Displayed Pass</em>' reference.
	 * @see #getDisplayedPass()
	 * @generated
	 */
	void setDisplayedPass(VisibilityPass value);

	/**
	 * Returns the value of the '<em><b>Earth Orbit Model Pass Tool Node</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode#getEarthOrbitModelPassTool <em>Earth Orbit Model Pass Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Topology Node associated with the tool.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Earth Orbit Model Pass Tool Node</em>' reference.
	 * @see #setEarthOrbitModelPassToolNode(EarthOrbitModelPassToolNode)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#getEarthOrbitModelPassTool_EarthOrbitModelPassToolNode()
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode#getEarthOrbitModelPassTool
	 * @model opposite="earthOrbitModelPassTool" transient="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel notify='true' children='true' property='Readonly'"
	 * @generated
	 */
	EarthOrbitModelPassToolNode getEarthOrbitModelPassToolNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool#getEarthOrbitModelPassToolNode <em>Earth Orbit Model Pass Tool Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earth Orbit Model Pass Tool Node</em>' reference.
	 * @see #getEarthOrbitModelPassToolNode()
	 * @generated
	 */
	void setEarthOrbitModelPassToolNode(EarthOrbitModelPassToolNode value);

} // EarthOrbitModelPassTool
