/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.addons.impl.Simple3DToolImpl;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.Updatable;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.EarthOrbitModel;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Orbit Model Tool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl#isUpdating <em>Updating</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl#isAutoUpdateEnabled <em>Auto Update Enabled</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl#getEarthOrbitModel <em>Earth Orbit Model</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl#getActiveTimeSource <em>Active Time Source</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelToolImpl#getActiveEarthSurfaceWorksite <em>Active Earth Surface Worksite</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class EarthOrbitModelToolImpl extends Simple3DToolImpl implements EarthOrbitModelTool 
{			
	private Adapter activeEarthSurfaceWorksiteAdapter;
	private Adapter activeTimeSourceAdapter;
	private Adapter timeSourceAdapter;
	
	/**
	 * The default value of the '{@link #isUpdating() <em>Updating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUpdating()
	 * @generated
	 * @ordered
	 */
	protected static final boolean UPDATING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUpdating() <em>Updating</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUpdating()
	 * @generated
	 * @ordered
	 */
	protected boolean updating = UPDATING_EDEFAULT;

	/**
	 * The default value of the '{@link #isAutoUpdateEnabled() <em>Auto Update Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAutoUpdateEnabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean AUTO_UPDATE_ENABLED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAutoUpdateEnabled() <em>Auto Update Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAutoUpdateEnabled()
	 * @generated
	 * @ordered
	 */
	protected boolean autoUpdateEnabled = AUTO_UPDATE_ENABLED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEarthOrbitModel() <em>Earth Orbit Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarthOrbitModel()
	 * @generated
	 * @ordered
	 */
	protected EarthOrbitModel earthOrbitModel;

	/**
	 * The cached value of the '{@link #getActiveTimeSource() <em>Active Time Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveTimeSource()
	 * @generated
	 * @ordered
	 */
	protected TimeSource activeTimeSource;

	/**
	 * The cached value of the '{@link #getActiveEarthSurfaceWorksite() <em>Active Earth Surface Worksite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveEarthSurfaceWorksite()
	 * @generated
	 * @ordered
	 */
	protected EarthSurfaceWorksite activeEarthSurfaceWorksite;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthOrbitModelToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_TOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUpdating() {
		return updating;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpdating(boolean newUpdating) {
		boolean oldUpdating = updating;
		updating = newUpdating;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__UPDATING, oldUpdating, updating));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAutoUpdateEnabled() {
		return autoUpdateEnabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAutoUpdateEnabled(boolean newAutoUpdateEnabled) {
		boolean oldAutoUpdateEnabled = autoUpdateEnabled;
		autoUpdateEnabled = newAutoUpdateEnabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED, oldAutoUpdateEnabled, autoUpdateEnabled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModel getEarthOrbitModel() {
		if (earthOrbitModel != null && earthOrbitModel.eIsProxy()) {
			InternalEObject oldEarthOrbitModel = (InternalEObject)earthOrbitModel;
			earthOrbitModel = (EarthOrbitModel)eResolveProxy(oldEarthOrbitModel);
			if (earthOrbitModel != oldEarthOrbitModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL, oldEarthOrbitModel, earthOrbitModel));
			}
		}
		return earthOrbitModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModel basicGetEarthOrbitModel() {
		return earthOrbitModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarthOrbitModel(EarthOrbitModel newEarthOrbitModel) {
		EarthOrbitModel oldEarthOrbitModel = earthOrbitModel;
		earthOrbitModel = newEarthOrbitModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL, oldEarthOrbitModel, earthOrbitModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeSource getActiveTimeSource() {
		if (activeTimeSource != null && activeTimeSource.eIsProxy()) {
			InternalEObject oldActiveTimeSource = (InternalEObject)activeTimeSource;
			activeTimeSource = (TimeSource)eResolveProxy(oldActiveTimeSource);
			if (activeTimeSource != oldActiveTimeSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE, oldActiveTimeSource, activeTimeSource));
			}
		}
		return activeTimeSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeSource basicGetActiveTimeSource() {
		return activeTimeSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setActiveTimeSource(TimeSource newActiveTimeSource)
	{
		// Unregister from previous time source.
		if(getActiveTimeSource() != null) getActiveTimeSource().eAdapters().remove(getTimeSourceAdapter());
		
		setActiveTimeSourceGen(newActiveTimeSource);
		
		// Register to new time source.
		if(newActiveTimeSource != null) newActiveTimeSource.eAdapters().add(getTimeSourceAdapter());

	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveTimeSourceGen(TimeSource newActiveTimeSource) {
		TimeSource oldActiveTimeSource = activeTimeSource;
		activeTimeSource = newActiveTimeSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE, oldActiveTimeSource, activeTimeSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthSurfaceWorksite getActiveEarthSurfaceWorksite() {
		if (activeEarthSurfaceWorksite != null && activeEarthSurfaceWorksite.eIsProxy()) {
			InternalEObject oldActiveEarthSurfaceWorksite = (InternalEObject)activeEarthSurfaceWorksite;
			activeEarthSurfaceWorksite = (EarthSurfaceWorksite)eResolveProxy(oldActiveEarthSurfaceWorksite);
			if (activeEarthSurfaceWorksite != oldActiveEarthSurfaceWorksite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE, oldActiveEarthSurfaceWorksite, activeEarthSurfaceWorksite));
			}
		}
		return activeEarthSurfaceWorksite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthSurfaceWorksite basicGetActiveEarthSurfaceWorksite() {
		return activeEarthSurfaceWorksite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveEarthSurfaceWorksite(EarthSurfaceWorksite newActiveEarthSurfaceWorksite) {
		EarthSurfaceWorksite oldActiveEarthSurfaceWorksite = activeEarthSurfaceWorksite;
		activeEarthSurfaceWorksite = newActiveEarthSurfaceWorksite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE, oldActiveEarthSurfaceWorksite, activeEarthSurfaceWorksite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void updateTime(Date newTime) 
	{
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean getDefaultAutoUpdateEnabled() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void update() throws Exception 
	{
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__UPDATING:
				return isUpdating();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED:
				return isAutoUpdateEnabled();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL:
				if (resolve) return getEarthOrbitModel();
				return basicGetEarthOrbitModel();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE:
				if (resolve) return getActiveTimeSource();
				return basicGetActiveTimeSource();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE:
				if (resolve) return getActiveEarthSurfaceWorksite();
				return basicGetActiveEarthSurfaceWorksite();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__UPDATING:
				setUpdating((Boolean)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED:
				setAutoUpdateEnabled((Boolean)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL:
				setEarthOrbitModel((EarthOrbitModel)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE:
				setActiveTimeSource((TimeSource)newValue);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE:
				setActiveEarthSurfaceWorksite((EarthSurfaceWorksite)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__UPDATING:
				setUpdating(UPDATING_EDEFAULT);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED:
				setAutoUpdateEnabled(AUTO_UPDATE_ENABLED_EDEFAULT);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL:
				setEarthOrbitModel((EarthOrbitModel)null);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE:
				setActiveTimeSource((TimeSource)null);
				return;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE:
				setActiveEarthSurfaceWorksite((EarthSurfaceWorksite)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__UPDATING:
				return updating != UPDATING_EDEFAULT;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED:
				return autoUpdateEnabled != AUTO_UPDATE_ENABLED_EDEFAULT;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL:
				return earthOrbitModel != null;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE:
				return activeTimeSource != null;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE:
				return activeEarthSurfaceWorksite != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Updatable.class) {
			switch (derivedFeatureID) {
				case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__UPDATING: return ApogyCorePackage.UPDATABLE__UPDATING;
				case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED: return ApogyCorePackage.UPDATABLE__AUTO_UPDATE_ENABLED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Updatable.class) {
			switch (baseFeatureID) {
				case ApogyCorePackage.UPDATABLE__UPDATING: return ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__UPDATING;
				case ApogyCorePackage.UPDATABLE__AUTO_UPDATE_ENABLED: return ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL__AUTO_UPDATE_ENABLED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == Updatable.class) {
			switch (baseOperationID) {
				case ApogyCorePackage.UPDATABLE___GET_DEFAULT_AUTO_UPDATE_ENABLED: return ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL___GET_DEFAULT_AUTO_UPDATE_ENABLED;
				case ApogyCorePackage.UPDATABLE___UPDATE: return ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL___UPDATE;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL___UPDATE_TIME__DATE:
				updateTime((Date)arguments.get(0));
				return null;
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL___GET_DEFAULT_AUTO_UPDATE_ENABLED:
				return getDefaultAutoUpdateEnabled();
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL___UPDATE:
				try {
					update();
					return null;
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (updating: ");
		result.append(updating);
		result.append(", autoUpdateEnabled: ");
		result.append(autoUpdateEnabled);
		result.append(')');
		return result.toString();
	}

	@Override
	public void initialise() 
	{	
		super.initialise();
				
		// Updates the active EarthSurfaceWorksite.
		updateActiveEarthSurfaceWorksite(ApogyEarthSurfaceEnvironmentFacade.INSTANCE.getActiveEarthSurfaceWorksite());
				
		// Register adapter used to respond to changes in active EarthSurfaceWorksite.
		ApogyEarthSurfaceEnvironmentFacade.INSTANCE.eAdapters().add(getActiveEarthSurfaceWorksiteAdapter());
		
		// Set the initial TimeSource.
		setActiveTimeSource(ApogyCoreEnvironmentFacade.INSTANCE.getActiveTimeSource());
			
		// Register adapter used to respond to changes in active ApogyEnvironment.
		ApogyCoreEnvironmentFacade.INSTANCE.eAdapters().add(getActiveTimeSourceAdapter()); 	
	}
	
	@Override
	public void dispose() 
	{
		// Unregister from the active time source.
		if(getActiveTimeSource() != null) getActiveTimeSource().eAdapters().remove(getTimeSourceAdapter());
		
		// Un-Register adapter used to respond to changes in active EarthSurfaceWorksite.
		ApogyEarthSurfaceEnvironmentFacade.INSTANCE.eAdapters().remove(getActiveEarthSurfaceWorksiteAdapter());
		
		// Un-Register adapter used to respond to changes in active ApogyEnvironment.
		ApogyCoreEnvironmentFacade.INSTANCE.eAdapters().remove(getActiveTimeSourceAdapter()); 				
		
		super.dispose();
	}
	
	private void updateActiveEarthSurfaceWorksite(EarthSurfaceWorksite newEarthSurfaceWorksite)
	{		
		if(getActiveEarthSurfaceWorksite() != newEarthSurfaceWorksite)
		{
			// Detach node from old sky.
			// detachToolNode();
			
			// Updates active worksite.
			ApogyCommonTransactionFacade.INSTANCE.basicSet(EarthOrbitModelToolImpl.this, 
														   ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE, 
														   ApogyEarthSurfaceEnvironmentFacade.INSTANCE.getActiveEarthSurfaceWorksite(), true);
			// setActiveEarthSurfaceWorksite(ApogyEarthSurfaceEnvironmentFacade.INSTANCE.getActiveEarthSurfaceWorksite());
			
			// Attaches node to new sky
			// attachToolNode();
		}
	}
	
	/**
	 * Return the Adapter used to monitor the currently active EarthSurfaceWorksite.
	 * @return The adapter, a singleton.
	 */
	private Adapter getActiveEarthSurfaceWorksiteAdapter() 
	{
		if(activeEarthSurfaceWorksiteAdapter == null)
		{
			activeEarthSurfaceWorksiteAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ApogyEarthSurfaceEnvironmentFacade)
					{
						int featureId = msg.getFeatureID(ApogyEarthSurfaceEnvironmentFacade.class);
						switch (featureId) 
						{
							case ApogyEarthSurfaceEnvironmentPackage.APOGY_EARTH_SURFACE_ENVIRONMENT_FACADE__ACTIVE_EARTH_SURFACE_WORKSITE:
							{
								updateActiveEarthSurfaceWorksite(ApogyEarthSurfaceEnvironmentFacade.INSTANCE.getActiveEarthSurfaceWorksite());
							}							
							break;

							default:
							break;
						}
					}
				}
			};
		}
		return activeEarthSurfaceWorksiteAdapter;
	}

	private Adapter getActiveTimeSourceAdapter() 
	{	
		if(activeTimeSourceAdapter == null)
		{
			activeTimeSourceAdapter = new AdapterImpl()
			{
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ApogyCoreEnvironmentFacade)
					{
						int featureId = msg.getFeatureID(ApogyCoreEnvironmentFacade.class);
						switch (featureId) 
						{
							case ApogyCoreEnvironmentPackage.APOGY_CORE_ENVIRONMENT_FACADE__ACTIVE_TIME_SOURCE:
							{
								// Update TimeSource.
								TimeSource newTimeSource = (TimeSource) msg.getNewValue();
								
								ApogyCommonTransactionFacade.INSTANCE.basicSet(EarthOrbitModelToolImpl.this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE, newTimeSource);
								// setActiveTimeSource(newTimeSource);
							}							
							break;

							default:
							break;
						}
					}
				}
			};
		}
		return activeTimeSourceAdapter;
	}

	private Adapter getTimeSourceAdapter() 
	{
		if(timeSourceAdapter == null)
		{
			timeSourceAdapter = new AdapterImpl()
			{
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof TimeSource)
					{
						int featureId = msg.getFeatureID(TimeSource.class);
						switch (featureId) 
						{
							case ApogyCommonEMFPackage.TIME_SOURCE__TIME:
							{
								Date newTime = (Date) msg.getNewValue();
								if(newTime != null) updateTime(newTime);
							}							
							break;

							default:
							break;
						}
					}
				}
			};
		}
		return timeSourceAdapter;
	}

	
} //EarthOrbitModelToolImpl
