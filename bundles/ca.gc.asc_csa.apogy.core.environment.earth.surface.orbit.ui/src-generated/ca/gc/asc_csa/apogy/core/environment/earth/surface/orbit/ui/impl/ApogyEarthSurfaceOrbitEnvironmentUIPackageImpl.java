/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode;
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.ApogyCoreEnvironmentOrbitEarthPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl extends EPackageImpl implements ApogyEarthSurfaceOrbitEnvironmentUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthOrbitModelToolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthOrbitingSpacecraftLocationToolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthOrbitingSpacecraftLocationToolNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthOrbitModelPassToolEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthOrbitModelPassToolNodeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl() {
		super(eNS_URI, ApogyEarthSurfaceOrbitEnvironmentUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyEarthSurfaceOrbitEnvironmentUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyEarthSurfaceOrbitEnvironmentUIPackage init() {
		if (isInited) return (ApogyEarthSurfaceOrbitEnvironmentUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthSurfaceOrbitEnvironmentUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl theApogyEarthSurfaceOrbitEnvironmentUIPackage = (ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyAddonsPackage.eINSTANCE.eClass();
		ApogyCoreEnvironmentOrbitEarthPackage.eINSTANCE.eClass();
		ApogyEarthSurfaceEnvironmentPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyEarthSurfaceOrbitEnvironmentUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyEarthSurfaceOrbitEnvironmentUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyEarthSurfaceOrbitEnvironmentUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyEarthSurfaceOrbitEnvironmentUIPackage.eNS_URI, theApogyEarthSurfaceOrbitEnvironmentUIPackage);
		return theApogyEarthSurfaceOrbitEnvironmentUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthOrbitModelTool() {
		return earthOrbitModelToolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitModelTool_EarthOrbitModel() {
		return (EReference)earthOrbitModelToolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitModelTool_ActiveTimeSource() {
		return (EReference)earthOrbitModelToolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitModelTool_ActiveEarthSurfaceWorksite() {
		return (EReference)earthOrbitModelToolEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEarthOrbitModelTool__UpdateTime__Date() {
		return earthOrbitModelToolEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthOrbitingSpacecraftLocationTool() {
		return earthOrbitingSpacecraftLocationToolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEarthOrbitingSpacecraftLocationTool_ShowVector() {
		return (EAttribute)earthOrbitingSpacecraftLocationToolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitingSpacecraftLocationTool_SpacecraftPosition() {
		return (EReference)earthOrbitingSpacecraftLocationToolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitingSpacecraftLocationTool_EarthOrbitingSpacecraftLocationToolNode() {
		return (EReference)earthOrbitingSpacecraftLocationToolEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthOrbitingSpacecraftLocationToolNode() {
		return earthOrbitingSpacecraftLocationToolNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitingSpacecraftLocationToolNode_EarthOrbitingSpacecraftLocationTool() {
		return (EReference)earthOrbitingSpacecraftLocationToolNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthOrbitModelPassTool() {
		return earthOrbitModelPassToolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEarthOrbitModelPassTool_LookAheadPeriod() {
		return (EAttribute)earthOrbitModelPassToolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEarthOrbitModelPassTool_LastPassesUpdateTime() {
		return (EAttribute)earthOrbitModelPassToolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitModelPassTool_SpacecraftsVisibilitySet() {
		return (EReference)earthOrbitModelPassToolEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitModelPassTool_DisplayedPass() {
		return (EReference)earthOrbitModelPassToolEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitModelPassTool_EarthOrbitModelPassToolNode() {
		return (EReference)earthOrbitModelPassToolEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthOrbitModelPassToolNode() {
		return earthOrbitModelPassToolNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthOrbitModelPassToolNode_EarthOrbitModelPassTool() {
		return (EReference)earthOrbitModelPassToolNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthSurfaceOrbitEnvironmentUIFactory getApogyEarthSurfaceOrbitEnvironmentUIFactory() {
		return (ApogyEarthSurfaceOrbitEnvironmentUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		earthOrbitModelToolEClass = createEClass(EARTH_ORBIT_MODEL_TOOL);
		createEReference(earthOrbitModelToolEClass, EARTH_ORBIT_MODEL_TOOL__EARTH_ORBIT_MODEL);
		createEReference(earthOrbitModelToolEClass, EARTH_ORBIT_MODEL_TOOL__ACTIVE_TIME_SOURCE);
		createEReference(earthOrbitModelToolEClass, EARTH_ORBIT_MODEL_TOOL__ACTIVE_EARTH_SURFACE_WORKSITE);
		createEOperation(earthOrbitModelToolEClass, EARTH_ORBIT_MODEL_TOOL___UPDATE_TIME__DATE);

		earthOrbitingSpacecraftLocationToolEClass = createEClass(EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL);
		createEAttribute(earthOrbitingSpacecraftLocationToolEClass, EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR);
		createEReference(earthOrbitingSpacecraftLocationToolEClass, EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION);
		createEReference(earthOrbitingSpacecraftLocationToolEClass, EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE);

		earthOrbitingSpacecraftLocationToolNodeEClass = createEClass(EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE);
		createEReference(earthOrbitingSpacecraftLocationToolNodeEClass, EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL);

		earthOrbitModelPassToolEClass = createEClass(EARTH_ORBIT_MODEL_PASS_TOOL);
		createEAttribute(earthOrbitModelPassToolEClass, EARTH_ORBIT_MODEL_PASS_TOOL__LOOK_AHEAD_PERIOD);
		createEAttribute(earthOrbitModelPassToolEClass, EARTH_ORBIT_MODEL_PASS_TOOL__LAST_PASSES_UPDATE_TIME);
		createEReference(earthOrbitModelPassToolEClass, EARTH_ORBIT_MODEL_PASS_TOOL__SPACECRAFTS_VISIBILITY_SET);
		createEReference(earthOrbitModelPassToolEClass, EARTH_ORBIT_MODEL_PASS_TOOL__DISPLAYED_PASS);
		createEReference(earthOrbitModelPassToolEClass, EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE);

		earthOrbitModelPassToolNodeEClass = createEClass(EARTH_ORBIT_MODEL_PASS_TOOL_NODE);
		createEReference(earthOrbitModelPassToolNodeEClass, EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyAddonsPackage theApogyAddonsPackage = (ApogyAddonsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsPackage.eNS_URI);
		ApogyCorePackage theApogyCorePackage = (ApogyCorePackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCorePackage.eNS_URI);
		ApogyCoreEnvironmentOrbitEarthPackage theApogyCoreEnvironmentOrbitEarthPackage = (ApogyCoreEnvironmentOrbitEarthPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCoreEnvironmentOrbitEarthPackage.eNS_URI);
		ApogyCommonEMFPackage theApogyCommonEMFPackage = (ApogyCommonEMFPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFPackage.eNS_URI);
		ApogyEarthSurfaceEnvironmentPackage theApogyEarthSurfaceEnvironmentPackage = (ApogyEarthSurfaceEnvironmentPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthSurfaceEnvironmentPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyEarthEnvironmentPackage theApogyEarthEnvironmentPackage = (ApogyEarthEnvironmentPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthEnvironmentPackage.eNS_URI);
		ApogyCommonTopologyPackage theApogyCommonTopologyPackage = (ApogyCommonTopologyPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		earthOrbitModelToolEClass.getESuperTypes().add(theApogyAddonsPackage.getSimple3DTool());
		earthOrbitModelToolEClass.getESuperTypes().add(theApogyCorePackage.getUpdatable());
		earthOrbitingSpacecraftLocationToolEClass.getESuperTypes().add(this.getEarthOrbitModelTool());
		earthOrbitingSpacecraftLocationToolNodeEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getNode());
		earthOrbitModelPassToolEClass.getESuperTypes().add(this.getEarthOrbitModelTool());
		earthOrbitModelPassToolNodeEClass.getESuperTypes().add(theApogyCommonTopologyPackage.getNode());

		// Initialize classes, features, and operations; add parameters
		initEClass(earthOrbitModelToolEClass, EarthOrbitModelTool.class, "EarthOrbitModelTool", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthOrbitModelTool_EarthOrbitModel(), theApogyCoreEnvironmentOrbitEarthPackage.getEarthOrbitModel(), null, "earthOrbitModel", null, 0, 1, EarthOrbitModelTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEarthOrbitModelTool_ActiveTimeSource(), theApogyCommonEMFPackage.getTimeSource(), null, "activeTimeSource", null, 0, 1, EarthOrbitModelTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEarthOrbitModelTool_ActiveEarthSurfaceWorksite(), theApogyEarthSurfaceEnvironmentPackage.getEarthSurfaceWorksite(), null, "activeEarthSurfaceWorksite", null, 0, 1, EarthOrbitModelTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getEarthOrbitModelTool__UpdateTime__Date(), null, "updateTime", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDate(), "newTime", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(earthOrbitingSpacecraftLocationToolEClass, EarthOrbitingSpacecraftLocationTool.class, "EarthOrbitingSpacecraftLocationTool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEarthOrbitingSpacecraftLocationTool_ShowVector(), theEcorePackage.getEBoolean(), "showVector", "true", 0, 1, EarthOrbitingSpacecraftLocationTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEarthOrbitingSpacecraftLocationTool_SpacecraftPosition(), theApogyEarthEnvironmentPackage.getHorizontalCoordinates(), null, "spacecraftPosition", null, 0, 1, EarthOrbitingSpacecraftLocationTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEarthOrbitingSpacecraftLocationTool_EarthOrbitingSpacecraftLocationToolNode(), this.getEarthOrbitingSpacecraftLocationToolNode(), this.getEarthOrbitingSpacecraftLocationToolNode_EarthOrbitingSpacecraftLocationTool(), "earthOrbitingSpacecraftLocationToolNode", null, 0, 1, EarthOrbitingSpacecraftLocationTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(earthOrbitingSpacecraftLocationToolNodeEClass, EarthOrbitingSpacecraftLocationToolNode.class, "EarthOrbitingSpacecraftLocationToolNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthOrbitingSpacecraftLocationToolNode_EarthOrbitingSpacecraftLocationTool(), this.getEarthOrbitingSpacecraftLocationTool(), this.getEarthOrbitingSpacecraftLocationTool_EarthOrbitingSpacecraftLocationToolNode(), "earthOrbitingSpacecraftLocationTool", null, 0, 1, EarthOrbitingSpacecraftLocationToolNode.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(earthOrbitModelPassToolEClass, EarthOrbitModelPassTool.class, "EarthOrbitModelPassTool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEarthOrbitModelPassTool_LookAheadPeriod(), theEcorePackage.getELong(), "lookAheadPeriod", "43200", 0, 1, EarthOrbitModelPassTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEarthOrbitModelPassTool_LastPassesUpdateTime(), theEcorePackage.getEDate(), "lastPassesUpdateTime", null, 0, 1, EarthOrbitModelPassTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEarthOrbitModelPassTool_SpacecraftsVisibilitySet(), theApogyCoreEnvironmentOrbitEarthPackage.getSpacecraftsVisibilitySet(), null, "spacecraftsVisibilitySet", null, 0, 1, EarthOrbitModelPassTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEarthOrbitModelPassTool_DisplayedPass(), theApogyCoreEnvironmentOrbitEarthPackage.getVisibilityPass(), null, "displayedPass", null, 0, 1, EarthOrbitModelPassTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEarthOrbitModelPassTool_EarthOrbitModelPassToolNode(), this.getEarthOrbitModelPassToolNode(), this.getEarthOrbitModelPassToolNode_EarthOrbitModelPassTool(), "earthOrbitModelPassToolNode", null, 0, 1, EarthOrbitModelPassTool.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(earthOrbitModelPassToolNodeEClass, EarthOrbitModelPassToolNode.class, "EarthOrbitModelPassToolNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthOrbitModelPassToolNode_EarthOrbitModelPassTool(), this.getEarthOrbitModelPassTool(), this.getEarthOrbitModelPassTool_EarthOrbitModelPassToolNode(), "earthOrbitModelPassTool", null, 0, 1, EarthOrbitModelPassToolNode.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "documentation", "******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n * Contributors:\n    Pierre Allard - initial API and implementation\n    Regent L\'Archeveque,\n    Sebastien Gemme\n\nSPDX-License-Identifier: EPL-1.0\n*****************************************************************************",
			 "prefix", "ApogyEarthSurfaceOrbitEnvironmentUI",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyEarthSurfaceOrbitEnvironmentUI",
			 "complianceLevel", "6.0",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit"
		   });	
		addAnnotation
		  (earthOrbitModelToolEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAbstract base class for tools displaying properties of EarthOrbitModel."
		   });	
		addAnnotation
		  (getEarthOrbitModelTool__UpdateTime__Date(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMethod called when the active time source time changes."
		   });	
		addAnnotation
		  (getEarthOrbitModelTool_EarthOrbitModel(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe orbit model defining the orbit of the spacecraft.",
			 "property", "Editable",
			 "children", "true"
		   });	
		addAnnotation
		  (getEarthOrbitModelTool_ActiveTimeSource(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe time source currently used to compute the spacecraft location.",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getEarthOrbitModelTool_ActiveEarthSurfaceWorksite(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe worksite for which to compute the the spacecraft location.",
			 "property", "Editable"
		   });	
		addAnnotation
		  (earthOrbitingSpacecraftLocationToolEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nTool used to display a spacecraft location as viewed from a surface worksite."
		   });	
		addAnnotation
		  (getEarthOrbitingSpacecraftLocationTool_ShowVector(), 
		   source, 
		   new String[] {
			 "documentation", "*\nWhether or not to show the vector from the worksite origin to the spacecraft."
		   });	
		addAnnotation
		  (getEarthOrbitingSpacecraftLocationTool_SpacecraftPosition(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe spacecraft location relative to the worksite.",
			 "property", "Readonly",
			 "children", "true",
			 "notify", "true"
		   });	
		addAnnotation
		  (getEarthOrbitingSpacecraftLocationTool_EarthOrbitingSpacecraftLocationToolNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe node representing this tool in the topology."
		   });	
		addAnnotation
		  (earthOrbitingSpacecraftLocationToolNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecialized Node that represents the EarthOrbitModelPassTool in the topology."
		   });	
		addAnnotation
		  (earthOrbitModelPassToolEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nTool used to display a spacecraft passes as viewed from a surface worksite."
		   });	
		addAnnotation
		  (getEarthOrbitModelPassTool_LookAheadPeriod(), 
		   source, 
		   new String[] {
			 "documentation", "*\nLook Ahead period use to decide when passes must be recomputed.",
			 "property", "Editable",
			 "apogy_units", "s"
		   });	
		addAnnotation
		  (getEarthOrbitModelPassTool_LastPassesUpdateTime(), 
		   source, 
		   new String[] {
			 "property", "None"
		   });	
		addAnnotation
		  (getEarthOrbitModelPassTool_SpacecraftsVisibilitySet(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe SpacecraftsVisibilitySet containing the passes.",
			 "property", "Readonly",
			 "children", "true"
		   });	
		addAnnotation
		  (getEarthOrbitModelPassTool_DisplayedPass(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe pass currently displayed. Can be null.",
			 "property", "Readonly",
			 "children", "true",
			 "notify", "true"
		   });	
		addAnnotation
		  (getEarthOrbitModelPassTool_EarthOrbitModelPassToolNode(), 
		   source, 
		   new String[] {
			 "documentation", "*\nTopology Node associated with the tool.",
			 "notify", "true",
			 "children", "true",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (earthOrbitModelPassToolNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecialized Node that represents the EarthOrbitModelPassTool in the topology."
		   });
	}

} //ApogyEarthSurfaceOrbitEnvironmentUIPackageImpl
