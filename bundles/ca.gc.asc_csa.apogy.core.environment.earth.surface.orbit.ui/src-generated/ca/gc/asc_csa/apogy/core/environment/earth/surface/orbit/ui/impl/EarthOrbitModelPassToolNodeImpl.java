/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl;

import ca.gc.asc_csa.apogy.common.topology.impl.NodeImpl;

import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitModelPassToolNode;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Orbit Model Pass Tool Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.impl.EarthOrbitModelPassToolNodeImpl#getEarthOrbitModelPassTool <em>Earth Orbit Model Pass Tool</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthOrbitModelPassToolNodeImpl extends NodeImpl implements EarthOrbitModelPassToolNode {
	/**
	 * The cached value of the '{@link #getEarthOrbitModelPassTool() <em>Earth Orbit Model Pass Tool</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEarthOrbitModelPassTool()
	 * @generated
	 * @ordered
	 */
	protected EarthOrbitModelPassTool earthOrbitModelPassTool;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthOrbitModelPassToolNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthSurfaceOrbitEnvironmentUIPackage.Literals.EARTH_ORBIT_MODEL_PASS_TOOL_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModelPassTool getEarthOrbitModelPassTool() {
		if (earthOrbitModelPassTool != null && earthOrbitModelPassTool.eIsProxy()) {
			InternalEObject oldEarthOrbitModelPassTool = (InternalEObject)earthOrbitModelPassTool;
			earthOrbitModelPassTool = (EarthOrbitModelPassTool)eResolveProxy(oldEarthOrbitModelPassTool);
			if (earthOrbitModelPassTool != oldEarthOrbitModelPassTool) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL, oldEarthOrbitModelPassTool, earthOrbitModelPassTool));
			}
		}
		return earthOrbitModelPassTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthOrbitModelPassTool basicGetEarthOrbitModelPassTool() {
		return earthOrbitModelPassTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEarthOrbitModelPassTool(EarthOrbitModelPassTool newEarthOrbitModelPassTool, NotificationChain msgs) {
		EarthOrbitModelPassTool oldEarthOrbitModelPassTool = earthOrbitModelPassTool;
		earthOrbitModelPassTool = newEarthOrbitModelPassTool;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL, oldEarthOrbitModelPassTool, newEarthOrbitModelPassTool);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEarthOrbitModelPassTool(EarthOrbitModelPassTool newEarthOrbitModelPassTool) {
		if (newEarthOrbitModelPassTool != earthOrbitModelPassTool) {
			NotificationChain msgs = null;
			if (earthOrbitModelPassTool != null)
				msgs = ((InternalEObject)earthOrbitModelPassTool).eInverseRemove(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE, EarthOrbitModelPassTool.class, msgs);
			if (newEarthOrbitModelPassTool != null)
				msgs = ((InternalEObject)newEarthOrbitModelPassTool).eInverseAdd(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE, EarthOrbitModelPassTool.class, msgs);
			msgs = basicSetEarthOrbitModelPassTool(newEarthOrbitModelPassTool, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL, newEarthOrbitModelPassTool, newEarthOrbitModelPassTool));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL:
				if (earthOrbitModelPassTool != null)
					msgs = ((InternalEObject)earthOrbitModelPassTool).eInverseRemove(this, ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL__EARTH_ORBIT_MODEL_PASS_TOOL_NODE, EarthOrbitModelPassTool.class, msgs);
				return basicSetEarthOrbitModelPassTool((EarthOrbitModelPassTool)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL:
				return basicSetEarthOrbitModelPassTool(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL:
				if (resolve) return getEarthOrbitModelPassTool();
				return basicGetEarthOrbitModelPassTool();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL:
				setEarthOrbitModelPassTool((EarthOrbitModelPassTool)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL:
				setEarthOrbitModelPassTool((EarthOrbitModelPassTool)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE__EARTH_ORBIT_MODEL_PASS_TOOL:
				return earthOrbitModelPassTool != null;
		}
		return super.eIsSet(featureID);
	}

} //EarthOrbitModelPassToolNodeImpl
