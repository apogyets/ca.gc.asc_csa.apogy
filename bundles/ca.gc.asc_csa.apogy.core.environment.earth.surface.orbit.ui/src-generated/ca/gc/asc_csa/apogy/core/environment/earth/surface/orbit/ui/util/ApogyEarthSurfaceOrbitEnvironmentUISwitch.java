/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.util;

import ca.gc.asc_csa.apogy.addons.AbstractTool;
import ca.gc.asc_csa.apogy.addons.Simple3DTool;
import ca.gc.asc_csa.apogy.addons.SimpleTool;

import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;

import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.core.Updatable;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage
 * @generated
 */
public class ApogyEarthSurfaceOrbitEnvironmentUISwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ApogyEarthSurfaceOrbitEnvironmentUIPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthSurfaceOrbitEnvironmentUISwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyEarthSurfaceOrbitEnvironmentUIPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_TOOL: {
				EarthOrbitModelTool earthOrbitModelTool = (EarthOrbitModelTool)theEObject;
				T result = caseEarthOrbitModelTool(earthOrbitModelTool);
				if (result == null) result = caseSimple3DTool(earthOrbitModelTool);
				if (result == null) result = caseUpdatable(earthOrbitModelTool);
				if (result == null) result = caseSimpleTool(earthOrbitModelTool);
				if (result == null) result = caseAbstractTool(earthOrbitModelTool);
				if (result == null) result = caseNamed(earthOrbitModelTool);
				if (result == null) result = caseDescribed(earthOrbitModelTool);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL: {
				EarthOrbitingSpacecraftLocationTool earthOrbitingSpacecraftLocationTool = (EarthOrbitingSpacecraftLocationTool)theEObject;
				T result = caseEarthOrbitingSpacecraftLocationTool(earthOrbitingSpacecraftLocationTool);
				if (result == null) result = caseEarthOrbitModelTool(earthOrbitingSpacecraftLocationTool);
				if (result == null) result = caseSimple3DTool(earthOrbitingSpacecraftLocationTool);
				if (result == null) result = caseUpdatable(earthOrbitingSpacecraftLocationTool);
				if (result == null) result = caseSimpleTool(earthOrbitingSpacecraftLocationTool);
				if (result == null) result = caseAbstractTool(earthOrbitingSpacecraftLocationTool);
				if (result == null) result = caseNamed(earthOrbitingSpacecraftLocationTool);
				if (result == null) result = caseDescribed(earthOrbitingSpacecraftLocationTool);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE: {
				EarthOrbitingSpacecraftLocationToolNode earthOrbitingSpacecraftLocationToolNode = (EarthOrbitingSpacecraftLocationToolNode)theEObject;
				T result = caseEarthOrbitingSpacecraftLocationToolNode(earthOrbitingSpacecraftLocationToolNode);
				if (result == null) result = caseNode(earthOrbitingSpacecraftLocationToolNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL: {
				EarthOrbitModelPassTool earthOrbitModelPassTool = (EarthOrbitModelPassTool)theEObject;
				T result = caseEarthOrbitModelPassTool(earthOrbitModelPassTool);
				if (result == null) result = caseEarthOrbitModelTool(earthOrbitModelPassTool);
				if (result == null) result = caseSimple3DTool(earthOrbitModelPassTool);
				if (result == null) result = caseUpdatable(earthOrbitModelPassTool);
				if (result == null) result = caseSimpleTool(earthOrbitModelPassTool);
				if (result == null) result = caseAbstractTool(earthOrbitModelPassTool);
				if (result == null) result = caseNamed(earthOrbitModelPassTool);
				if (result == null) result = caseDescribed(earthOrbitModelPassTool);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBIT_MODEL_PASS_TOOL_NODE: {
				EarthOrbitModelPassToolNode earthOrbitModelPassToolNode = (EarthOrbitModelPassToolNode)theEObject;
				T result = caseEarthOrbitModelPassToolNode(earthOrbitModelPassToolNode);
				if (result == null) result = caseNode(earthOrbitModelPassToolNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Orbit Model Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Orbit Model Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthOrbitModelTool(EarthOrbitModelTool object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Orbiting Spacecraft Location Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Orbiting Spacecraft Location Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthOrbitingSpacecraftLocationTool(EarthOrbitingSpacecraftLocationTool object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Orbiting Spacecraft Location Tool Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Orbiting Spacecraft Location Tool Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthOrbitingSpacecraftLocationToolNode(EarthOrbitingSpacecraftLocationToolNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Orbit Model Pass Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Orbit Model Pass Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthOrbitModelPassTool(EarthOrbitModelPassTool object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Earth Orbit Model Pass Tool Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Earth Orbit Model Pass Tool Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEarthOrbitModelPassToolNode(EarthOrbitModelPassToolNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Described</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescribed(Described object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractTool(AbstractTool object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Tool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Tool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleTool(SimpleTool object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple3 DTool</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple3 DTool</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimple3DTool(Simple3DTool object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Updatable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Updatable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUpdatable(Updatable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNode(Node object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ApogyEarthSurfaceOrbitEnvironmentUISwitch
