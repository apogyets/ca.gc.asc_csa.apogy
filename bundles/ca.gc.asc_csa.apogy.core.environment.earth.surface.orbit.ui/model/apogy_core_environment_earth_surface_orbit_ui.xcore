/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Pierre Allard - initial API and implementation
 *     Regent L'Archeveque, 
 *     Sebastien Gemme 
 *        
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
@GenModel(prefix="ApogyEarthSurfaceOrbitEnvironmentUI",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
          copyrightText="*******************************************************************************
Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
     Pierre Allard - initial API and implementation
        
SPDX-License-Identifier: EPL-1.0    
*******************************************************************************",
		  modelName="ApogyEarthSurfaceOrbitEnvironmentUI",
		  complianceLevel="6.0",
		  suppressGenModelAnnotations="false",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui/src-generated")
@GenModel(editDirectory= "/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.edit/src-generated")
//@GenModel(testsDirectory="/ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.tests/src-generated")

package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui

import ca.gc.asc_csa.apogy.addons.Simple3DTool
import ca.gc.asc_csa.apogy.common.emf.TimeSource
import ca.gc.asc_csa.apogy.common.topology.Node
import ca.gc.asc_csa.apogy.core.Updatable

import ca.gc.asc_csa.apogy.core.environment.earth.HorizontalCoordinates
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.EarthOrbitModel
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.SpacecraftsVisibilitySet
import ca.gc.asc_csa.apogy.core.environment.orbit.earth.VisibilityPass
import org.eclipse.emf.ecore.EDate

/**
 * Abstract base class for tools displaying properties of EarthOrbitModel.
 */
abstract class EarthOrbitModelTool extends Simple3DTool, Updatable
{		
	/**
	 * The orbit model defining the orbit of the spacecraft.
	 */
	@GenModel(property="Editable", children="true")	
	refers transient EarthOrbitModel earthOrbitModel
	
	/**
	 * The time source currently used to compute the spacecraft location.
	 */
	@GenModel(property="Editable")	
	refers transient TimeSource activeTimeSource
	
	/**
	 * The worksite for which to compute the the spacecraft location.
	 */
	@GenModel(property="Editable")			
	refers transient EarthSurfaceWorksite activeEarthSurfaceWorksite
	
	/**
	 * Method called when the active time source time changes.
	 */
	op void updateTime(Date newTime)
}


/**
 * Tool used to display a spacecraft location as viewed from a surface worksite.
 */
class EarthOrbitingSpacecraftLocationTool extends EarthOrbitModelTool
{
	
	/**
	 * Whether or not to show the vector from the worksite origin to the spacecraft.
	 */
	boolean showVector = "true"
	
	/**
	 * The spacecraft location relative to the worksite.
	 */
	@GenModel(property="Readonly", children="true", notify="true")	
	refers transient HorizontalCoordinates spacecraftPosition
	
	/**
	 * The node representing this tool in the topology.
	 */
	refers transient EarthOrbitingSpacecraftLocationToolNode earthOrbitingSpacecraftLocationToolNode opposite earthOrbitingSpacecraftLocationTool
}

/**
 * Specialized Node that represents the EarthOrbitModelPassTool in the topology.
 */
class EarthOrbitingSpacecraftLocationToolNode extends Node
{
	refers transient EarthOrbitingSpacecraftLocationTool earthOrbitingSpacecraftLocationTool opposite earthOrbitingSpacecraftLocationToolNode
}

/**
 * Tool used to display a spacecraft passes as viewed from a surface worksite.
 */
class EarthOrbitModelPassTool extends EarthOrbitModelTool
{
	/**
	 * Look Ahead period use to decide when passes must be recomputed.
	 */
	@GenModel(property="Editable", apogy_units="s")	
	long lookAheadPeriod = "43200"
	
	@GenModel(property="None")	
	EDate lastPassesUpdateTime
	
	
	/**
	 * The SpacecraftsVisibilitySet containing the passes.
	 */
	@GenModel(property="Readonly",children="true")	
	refers transient SpacecraftsVisibilitySet spacecraftsVisibilitySet
		
	/**
	 * The pass currently displayed. Can be null.
	 */	
	@GenModel(property="Readonly",children="true", notify="true")		 
	refers transient VisibilityPass displayedPass
		
	/**
	 * Topology Node associated with the tool.
	 */
	@GenModel(notify="true", children="true", property="Readonly")	
	refers transient EarthOrbitModelPassToolNode earthOrbitModelPassToolNode opposite earthOrbitModelPassTool	
}

/**
 * Specialized Node that represents the EarthOrbitModelPassTool in the topology.
 */
class EarthOrbitModelPassToolNode extends Node
{
	refers transient EarthOrbitModelPassTool earthOrbitModelPassTool opposite earthOrbitModelPassToolNode
}