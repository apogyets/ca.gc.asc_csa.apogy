package ca.gc.asc_csa.apogy.common.topology.ui.viewer.actions;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.resource.ImageDescriptor;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.TopologyViewerProvider;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.internal.IConstants;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.internal.PluginImages;


public class EnableHighSpeedMotionAction extends Action 
{
	/**
	 * The {@link TopologyView} on which we want to enable/disble high speed motion.
	 */
	private TopologyViewerProvider provider;
	
	public EnableHighSpeedMotionAction(TopologyViewerProvider provider) 
	{
		super("High Speed Motion", IAction.AS_CHECK_BOX);
		if (provider == null) 
		{
			throw new IllegalArgumentException("Provider is null");
		}

		// The image to assign to this action.
		ImageDescriptor highSpeedMotion = PluginImages.getImageDescriptor(IConstants.IMG_ELCL_ENABLE_HIGH_SPEED_MOTION);

		setImageDescriptor(highSpeedMotion);

		this.provider = provider;
	}

	@Override
	public void run() 
	{
		if (provider.getTopologyViewer() != null) 
		{
			provider.getTopologyViewer().setHighSpeedMotionEnabled(isChecked());
		}
	}
}
