package ca.gc.asc_csa.apogy.common.geometry.data3d.las;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.io.IOException;

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.common.geometry.data3d.las.impl.ApogyCommonGeometryData3DLASFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>LAS Facade</b></em>'.
 * <!-- end-user-doc --> *
 *
 * @see ca.gc.asc_csa.apogy.common.geometry.data3d.las.ApogyCommonGeometryData3DLASPackage#getApogyCommonGeometryData3DLASFacade()
 * @model
 * @generated
 */
public interface ApogyCommonGeometryData3DLASFacade extends EObject {

	public static final ApogyCommonGeometryData3DLASFacade INSTANCE = ApogyCommonGeometryData3DLASFacadeImpl.getInstance();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @model unique="false" exceptions="ca.gc.asc_csa.apogy.common.geometry.data3d.las.IOException" formatIDUnique="false" dataUnique="false"
	 * @generated
	 */
	LASPoint createPoint(int formatID, byte[] data) throws IOException;

} // ApogyCommonGeometryData3DLASFacade
