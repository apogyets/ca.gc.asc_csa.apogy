/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;

import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;

import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;

import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyEarthAtmosphereEnvironmentPackageImpl extends EPackageImpl implements ApogyEarthAtmosphereEnvironmentPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthAtmosphereWorksiteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass earthAtmosphereWorksiteNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass apogyEarthAtmosphereFacadeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyEarthAtmosphereEnvironmentPackageImpl() {
		super(eNS_URI, ApogyEarthAtmosphereEnvironmentFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyEarthAtmosphereEnvironmentPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyEarthAtmosphereEnvironmentPackage init() {
		if (isInited) return (ApogyEarthAtmosphereEnvironmentPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthAtmosphereEnvironmentPackage.eNS_URI);

		// Obtain or create and register package
		ApogyEarthAtmosphereEnvironmentPackageImpl theApogyEarthAtmosphereEnvironmentPackage = (ApogyEarthAtmosphereEnvironmentPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyEarthAtmosphereEnvironmentPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyEarthAtmosphereEnvironmentPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyEarthSurfaceEnvironmentPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyEarthAtmosphereEnvironmentPackage.createPackageContents();

		// Initialize created meta-data
		theApogyEarthAtmosphereEnvironmentPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyEarthAtmosphereEnvironmentPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyEarthAtmosphereEnvironmentPackage.eNS_URI, theApogyEarthAtmosphereEnvironmentPackage);
		return theApogyEarthAtmosphereEnvironmentPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthAtmosphereWorksite() {
		return earthAtmosphereWorksiteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthAtmosphereWorksite_EarthSky() {
		return (EReference)earthAtmosphereWorksiteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEarthAtmosphereWorksiteNode() {
		return earthAtmosphereWorksiteNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEarthAtmosphereWorksiteNode_SkyTransformNode() {
		return (EReference)earthAtmosphereWorksiteNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApogyEarthAtmosphereFacade() {
		return apogyEarthAtmosphereFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApogyEarthAtmosphereFacade_ActiveEarthAtmosphereWorksite() {
		return (EReference)apogyEarthAtmosphereFacadeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyEarthAtmosphereFacade__CreateAndInitializeDefaultCSAEarthAtmosphereWorksite() {
		return apogyEarthAtmosphereFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyEarthAtmosphereEnvironmentFactory getApogyEarthAtmosphereEnvironmentFactory() {
		return (ApogyEarthAtmosphereEnvironmentFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		earthAtmosphereWorksiteEClass = createEClass(EARTH_ATMOSPHERE_WORKSITE);
		createEReference(earthAtmosphereWorksiteEClass, EARTH_ATMOSPHERE_WORKSITE__EARTH_SKY);

		earthAtmosphereWorksiteNodeEClass = createEClass(EARTH_ATMOSPHERE_WORKSITE_NODE);
		createEReference(earthAtmosphereWorksiteNodeEClass, EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE);

		apogyEarthAtmosphereFacadeEClass = createEClass(APOGY_EARTH_ATMOSPHERE_FACADE);
		createEReference(apogyEarthAtmosphereFacadeEClass, APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE);
		createEOperation(apogyEarthAtmosphereFacadeEClass, APOGY_EARTH_ATMOSPHERE_FACADE___CREATE_AND_INITIALIZE_DEFAULT_CSA_EARTH_ATMOSPHERE_WORKSITE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyEarthEnvironmentPackage theApogyEarthEnvironmentPackage = (ApogyEarthEnvironmentPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthEnvironmentPackage.eNS_URI);
		ApogyEarthSurfaceEnvironmentPackage theApogyEarthSurfaceEnvironmentPackage = (ApogyEarthSurfaceEnvironmentPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyEarthSurfaceEnvironmentPackage.eNS_URI);
		ApogyCoreEnvironmentPackage theApogyCoreEnvironmentPackage = (ApogyCoreEnvironmentPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCoreEnvironmentPackage.eNS_URI);
		ApogyCommonTopologyPackage theApogyCommonTopologyPackage = (ApogyCommonTopologyPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		earthAtmosphereWorksiteEClass.getESuperTypes().add(theApogyEarthEnvironmentPackage.getEarthWorksite());
		earthAtmosphereWorksiteNodeEClass.getESuperTypes().add(theApogyCoreEnvironmentPackage.getWorksiteNode());

		// Initialize classes, features, and operations; add parameters
		initEClass(earthAtmosphereWorksiteEClass, EarthAtmosphereWorksite.class, "EarthAtmosphereWorksite", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthAtmosphereWorksite_EarthSky(), theApogyEarthSurfaceEnvironmentPackage.getEarthSky(), null, "earthSky", null, 1, 1, EarthAtmosphereWorksite.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(earthAtmosphereWorksiteNodeEClass, EarthAtmosphereWorksiteNode.class, "EarthAtmosphereWorksiteNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEarthAtmosphereWorksiteNode_SkyTransformNode(), theApogyCommonTopologyPackage.getTransformNode(), null, "skyTransformNode", null, 0, 1, EarthAtmosphereWorksiteNode.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(apogyEarthAtmosphereFacadeEClass, ApogyEarthAtmosphereFacade.class, "ApogyEarthAtmosphereFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getApogyEarthAtmosphereFacade_ActiveEarthAtmosphereWorksite(), this.getEarthAtmosphereWorksite(), null, "activeEarthAtmosphereWorksite", null, 0, 1, ApogyEarthAtmosphereFacade.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getApogyEarthAtmosphereFacade__CreateAndInitializeDefaultCSAEarthAtmosphereWorksite(), this.getEarthAtmosphereWorksite(), "createAndInitializeDefaultCSAEarthAtmosphereWorksite", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "documentation", "******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n * Contributors:\n    Pierre Allard - initial API and implementation\n\nSPDX-License-Identifier: EPL-1.0\n*****************************************************************************",
			 "prefix", "ApogyEarthAtmosphereEnvironment",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************",
			 "modelName", "ApogyEarthAtmosphereEnvironment",
			 "complianceLevel", "6.0",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.core.environment.earth"
		   });	
		addAnnotation
		  (earthAtmosphereWorksiteEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nDefines a worksite above the surface, in the atmosphere, below 100km altitude."
		   });	
		addAnnotation
		  (getEarthAtmosphereWorksite_EarthSky(), 
		   source, 
		   new String[] {
			 "documentation", "The EarthSky associated with the worksite,"
		   });	
		addAnnotation
		  (earthAtmosphereWorksiteNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "SurfaceWorksiteNode specialized for the Earth Surface."
		   });	
		addAnnotation
		  (getApogyEarthAtmosphereFacade__CreateAndInitializeDefaultCSAEarthAtmosphereWorksite(), 
		   source, 
		   new String[] {
			 "documentation", "Create an empty EarthAtmosphereWorksite with the CSA Mars Yard coordinates."
		   });	
		addAnnotation
		  (getApogyEarthAtmosphereFacade_ActiveEarthAtmosphereWorksite(), 
		   source, 
		   new String[] {
			 "documentation", "Refers to the active EarthSurfaceWorksite. May be null."
		   });
	}

} //ApogyEarthAtmosphereEnvironmentPackageImpl
