/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere;

import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * ******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *  * Contributors:
 *     Pierre Allard - initial API and implementation
 * 
 * SPDX-License-Identifier: EPL-1.0
 * *****************************************************************************
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyEarthAtmosphereEnvironment' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='*******************************************************************************\nCopyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency \nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n     Pierre Allard - initial API and implementation\n        \nSPDX-License-Identifier: EPL-1.0    \n*******************************************************************************' modelName='ApogyEarthAtmosphereEnvironment' complianceLevel='6.0' suppressGenModelAnnotations='false' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.core.environment.earth'"
 * @generated
 */
public interface ApogyEarthAtmosphereEnvironmentPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "atmosphere";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.environment.earth.atmosphere";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "atmosphere";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyEarthAtmosphereEnvironmentPackage eINSTANCE = ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereEnvironmentPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteImpl <em>Earth Atmosphere Worksite</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereEnvironmentPackageImpl#getEarthAtmosphereWorksite()
	 * @generated
	 */
	int EARTH_ATMOSPHERE_WORKSITE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE__NAME = ApogyEarthEnvironmentPackage.EARTH_WORKSITE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE__DESCRIPTION = ApogyEarthEnvironmentPackage.EARTH_WORKSITE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE__TIME = ApogyEarthEnvironmentPackage.EARTH_WORKSITE__TIME;

	/**
	 * The feature id for the '<em><b>Worksites List</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE__WORKSITES_LIST = ApogyEarthEnvironmentPackage.EARTH_WORKSITE__WORKSITES_LIST;

	/**
	 * The feature id for the '<em><b>Worksite Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE__WORKSITE_NODE = ApogyEarthEnvironmentPackage.EARTH_WORKSITE__WORKSITE_NODE;

	/**
	 * The feature id for the '<em><b>Sky</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE__SKY = ApogyEarthEnvironmentPackage.EARTH_WORKSITE__SKY;

	/**
	 * The feature id for the '<em><b>Geographical Coordinates</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE__GEOGRAPHICAL_COORDINATES = ApogyEarthEnvironmentPackage.EARTH_WORKSITE__GEOGRAPHICAL_COORDINATES;

	/**
	 * The feature id for the '<em><b>Earth Sky</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE__EARTH_SKY = ApogyEarthEnvironmentPackage.EARTH_WORKSITE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Earth Atmosphere Worksite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_FEATURE_COUNT = ApogyEarthEnvironmentPackage.EARTH_WORKSITE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Earth Atmosphere Worksite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_OPERATION_COUNT = ApogyEarthEnvironmentPackage.EARTH_WORKSITE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteNodeImpl <em>Earth Atmosphere Worksite Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteNodeImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereEnvironmentPackageImpl#getEarthAtmosphereWorksiteNode()
	 * @generated
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE = 1;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE__PARENT = ApogyCoreEnvironmentPackage.WORKSITE_NODE__PARENT;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE__DESCRIPTION = ApogyCoreEnvironmentPackage.WORKSITE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Node Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE__NODE_ID = ApogyCoreEnvironmentPackage.WORKSITE_NODE__NODE_ID;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE__CHILDREN = ApogyCoreEnvironmentPackage.WORKSITE_NODE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Aggregated Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE__AGGREGATED_CHILDREN = ApogyCoreEnvironmentPackage.WORKSITE_NODE__AGGREGATED_CHILDREN;

	/**
	 * The feature id for the '<em><b>Worksite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE__WORKSITE = ApogyCoreEnvironmentPackage.WORKSITE_NODE__WORKSITE;

	/**
	 * The feature id for the '<em><b>Sky Transform Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE = ApogyCoreEnvironmentPackage.WORKSITE_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Earth Atmosphere Worksite Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE_FEATURE_COUNT = ApogyCoreEnvironmentPackage.WORKSITE_NODE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Accept</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE___ACCEPT__INODEVISITOR = ApogyCoreEnvironmentPackage.WORKSITE_NODE___ACCEPT__INODEVISITOR;

	/**
	 * The number of operations of the '<em>Earth Atmosphere Worksite Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EARTH_ATMOSPHERE_WORKSITE_NODE_OPERATION_COUNT = ApogyCoreEnvironmentPackage.WORKSITE_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereFacadeImpl <em>Apogy Earth Atmosphere Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereFacadeImpl
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereEnvironmentPackageImpl#getApogyEarthAtmosphereFacade()
	 * @generated
	 */
	int APOGY_EARTH_ATMOSPHERE_FACADE = 2;

	/**
	 * The feature id for the '<em><b>Active Earth Atmosphere Worksite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE = 0;

	/**
	 * The number of structural features of the '<em>Apogy Earth Atmosphere Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_EARTH_ATMOSPHERE_FACADE_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Create And Initialize Default CSA Earth Atmosphere Worksite</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_EARTH_ATMOSPHERE_FACADE___CREATE_AND_INITIALIZE_DEFAULT_CSA_EARTH_ATMOSPHERE_WORKSITE = 0;

	/**
	 * The number of operations of the '<em>Apogy Earth Atmosphere Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_EARTH_ATMOSPHERE_FACADE_OPERATION_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite <em>Earth Atmosphere Worksite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Atmosphere Worksite</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite
	 * @generated
	 */
	EClass getEarthAtmosphereWorksite();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite#getEarthSky <em>Earth Sky</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Earth Sky</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite#getEarthSky()
	 * @see #getEarthAtmosphereWorksite()
	 * @generated
	 */
	EReference getEarthAtmosphereWorksite_EarthSky();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode <em>Earth Atmosphere Worksite Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Earth Atmosphere Worksite Node</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode
	 * @generated
	 */
	EClass getEarthAtmosphereWorksiteNode();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode#getSkyTransformNode <em>Sky Transform Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sky Transform Node</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode#getSkyTransformNode()
	 * @see #getEarthAtmosphereWorksiteNode()
	 * @generated
	 */
	EReference getEarthAtmosphereWorksiteNode_SkyTransformNode();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade <em>Apogy Earth Atmosphere Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apogy Earth Atmosphere Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade
	 * @generated
	 */
	EClass getApogyEarthAtmosphereFacade();

	/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade#getActiveEarthAtmosphereWorksite <em>Active Earth Atmosphere Worksite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Active Earth Atmosphere Worksite</em>'.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade#getActiveEarthAtmosphereWorksite()
	 * @see #getApogyEarthAtmosphereFacade()
	 * @generated
	 */
	EReference getApogyEarthAtmosphereFacade_ActiveEarthAtmosphereWorksite();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade#createAndInitializeDefaultCSAEarthAtmosphereWorksite() <em>Create And Initialize Default CSA Earth Atmosphere Worksite</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create And Initialize Default CSA Earth Atmosphere Worksite</em>' operation.
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade#createAndInitializeDefaultCSAEarthAtmosphereWorksite()
	 * @generated
	 */
	EOperation getApogyEarthAtmosphereFacade__CreateAndInitializeDefaultCSAEarthAtmosphereWorksite();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyEarthAtmosphereEnvironmentFactory getApogyEarthAtmosphereEnvironmentFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteImpl <em>Earth Atmosphere Worksite</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereEnvironmentPackageImpl#getEarthAtmosphereWorksite()
		 * @generated
		 */
		EClass EARTH_ATMOSPHERE_WORKSITE = eINSTANCE.getEarthAtmosphereWorksite();

		/**
		 * The meta object literal for the '<em><b>Earth Sky</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ATMOSPHERE_WORKSITE__EARTH_SKY = eINSTANCE.getEarthAtmosphereWorksite_EarthSky();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteNodeImpl <em>Earth Atmosphere Worksite Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteNodeImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereEnvironmentPackageImpl#getEarthAtmosphereWorksiteNode()
		 * @generated
		 */
		EClass EARTH_ATMOSPHERE_WORKSITE_NODE = eINSTANCE.getEarthAtmosphereWorksiteNode();

		/**
		 * The meta object literal for the '<em><b>Sky Transform Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EARTH_ATMOSPHERE_WORKSITE_NODE__SKY_TRANSFORM_NODE = eINSTANCE.getEarthAtmosphereWorksiteNode_SkyTransformNode();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereFacadeImpl <em>Apogy Earth Atmosphere Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereFacadeImpl
		 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereEnvironmentPackageImpl#getApogyEarthAtmosphereFacade()
		 * @generated
		 */
		EClass APOGY_EARTH_ATMOSPHERE_FACADE = eINSTANCE.getApogyEarthAtmosphereFacade();

		/**
		 * The meta object literal for the '<em><b>Active Earth Atmosphere Worksite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE = eINSTANCE.getApogyEarthAtmosphereFacade_ActiveEarthAtmosphereWorksite();

		/**
		 * The meta object literal for the '<em><b>Create And Initialize Default CSA Earth Atmosphere Worksite</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_EARTH_ATMOSPHERE_FACADE___CREATE_AND_INITIALIZE_DEFAULT_CSA_EARTH_ATMOSPHERE_WORKSITE = eINSTANCE.getApogyEarthAtmosphereFacade__CreateAndInitializeDefaultCSAEarthAtmosphereWorksite();

	}

} //ApogyEarthAtmosphereEnvironmentPackage
