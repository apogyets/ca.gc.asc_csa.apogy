/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apogy Earth Atmosphere Facade</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.ApogyEarthAtmosphereFacadeImpl#getActiveEarthAtmosphereWorksite <em>Active Earth Atmosphere Worksite</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ApogyEarthAtmosphereFacadeImpl extends MinimalEObjectImpl.Container implements ApogyEarthAtmosphereFacade 
{	
	private Adapter activeWorksiteAdapter = null;
	private static ApogyEarthAtmosphereFacade instance = null;
	
	public static ApogyEarthAtmosphereFacade getInstance() 
	{
		if (instance == null) 
		{
			instance = new ApogyEarthAtmosphereFacadeImpl();
		}
		return instance;
	}		
	
	/**
	 * The cached value of the '{@link #getActiveEarthAtmosphereWorksite() <em>Active Earth Atmosphere Worksite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActiveEarthAtmosphereWorksite()
	 * @generated
	 * @ordered
	 */
	protected EarthAtmosphereWorksite activeEarthAtmosphereWorksite;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	protected ApogyEarthAtmosphereFacadeImpl() 
	{
		super();
		
		// Register to the ApogyCoreEnvironmentFacade for changes in the active worksite.
		ApogyCoreEnvironmentFacade.INSTANCE.eAdapters().add(getWorksiteAdapter());		
		
		if(ApogyCoreEnvironmentFacade.INSTANCE.getActiveWorksite() instanceof EarthAtmosphereWorksite)
		{
			setActiveEarthAtmosphereWorksite((EarthAtmosphereWorksite) ApogyCoreEnvironmentFacade.INSTANCE.getActiveWorksite());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthAtmosphereEnvironmentPackage.Literals.APOGY_EARTH_ATMOSPHERE_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthAtmosphereWorksite getActiveEarthAtmosphereWorksite() {
		if (activeEarthAtmosphereWorksite != null && activeEarthAtmosphereWorksite.eIsProxy()) {
			InternalEObject oldActiveEarthAtmosphereWorksite = (InternalEObject)activeEarthAtmosphereWorksite;
			activeEarthAtmosphereWorksite = (EarthAtmosphereWorksite)eResolveProxy(oldActiveEarthAtmosphereWorksite);
			if (activeEarthAtmosphereWorksite != oldActiveEarthAtmosphereWorksite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyEarthAtmosphereEnvironmentPackage.APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE, oldActiveEarthAtmosphereWorksite, activeEarthAtmosphereWorksite));
			}
		}
		return activeEarthAtmosphereWorksite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthAtmosphereWorksite basicGetActiveEarthAtmosphereWorksite() {
		return activeEarthAtmosphereWorksite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActiveEarthAtmosphereWorksite(EarthAtmosphereWorksite newActiveEarthAtmosphereWorksite) {
		EarthAtmosphereWorksite oldActiveEarthAtmosphereWorksite = activeEarthAtmosphereWorksite;
		activeEarthAtmosphereWorksite = newActiveEarthAtmosphereWorksite;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyEarthAtmosphereEnvironmentPackage.APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE, oldActiveEarthAtmosphereWorksite, activeEarthAtmosphereWorksite));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthAtmosphereWorksite createAndInitializeDefaultCSAEarthAtmosphereWorksite() 
	{
		EarthAtmosphereWorksite worksite = null;
	
		try
		{
			Date now = new Date();
			
			// Initialise the worksite.
			 worksite = ApogyEarthAtmosphereEnvironmentFactory.eINSTANCE.createEarthAtmosphereWorksite();
			worksite.setName("CSA AT");
			worksite.setDescription("The CSA Default Worksite.");
			
			// Sets the coordinates.
			worksite.setGeographicalCoordinates( ApogyEarthSurfaceEnvironmentFacade.INSTANCE.getMarsYardGeographicalCoordinates());						
					
			// Creates the Earth Sky.
			EarthSky earthSky = ApogyEarthSurfaceEnvironmentFacade.INSTANCE.createEarthSky(worksite.getGeographicalCoordinates());						
			
			// Sets the worksite sky.
			worksite.setSky(earthSky);	
			earthSky.setWorksite(worksite);
			
			// Sets time stamp.		
			worksite.getEarthSky().setTime(now);					
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
				
		return worksite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE:
				if (resolve) return getActiveEarthAtmosphereWorksite();
				return basicGetActiveEarthAtmosphereWorksite();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE:
				setActiveEarthAtmosphereWorksite((EarthAtmosphereWorksite)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE:
				setActiveEarthAtmosphereWorksite((EarthAtmosphereWorksite)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.APOGY_EARTH_ATMOSPHERE_FACADE__ACTIVE_EARTH_ATMOSPHERE_WORKSITE:
				return activeEarthAtmosphereWorksite != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyEarthAtmosphereEnvironmentPackage.APOGY_EARTH_ATMOSPHERE_FACADE___CREATE_AND_INITIALIZE_DEFAULT_CSA_EARTH_ATMOSPHERE_WORKSITE:
				return createAndInitializeDefaultCSAEarthAtmosphereWorksite();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Adapter that listens to the active AbstractWorksite and updates the active EarthSurfaceWorksite.
	 * @return The adapter.
	 */
	private Adapter getWorksiteAdapter()
	{
		if(activeWorksiteAdapter == null)
		{
			activeWorksiteAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ApogyCoreEnvironmentFacade)
					{
						int featureID = msg.getFeatureID(ApogyCoreEnvironmentFacade.class);
						
						switch (featureID) 
						{
							case ApogyCoreEnvironmentPackage.APOGY_CORE_ENVIRONMENT_FACADE__ACTIVE_WORKSITE:
							{		
								if(msg.getNewValue() == null)
								{
									setActiveEarthAtmosphereWorksite(null);
								}
								else if(msg.getNewValue() instanceof EarthAtmosphereWorksite)
								{
									setActiveEarthAtmosphereWorksite((EarthAtmosphereWorksite) msg.getNewValue());
								}								
							}
							break;
	
							default:
							break;
						}
					}
				}
			};
		}
		return activeWorksiteAdapter;
	}
	
} //ApogyEarthAtmosphereFacadeImpl
