/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere;

import ca.gc.asc_csa.apogy.core.environment.earth.EarthWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Atmosphere Worksite</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Defines a worksite above the surface, in the atmosphere, below 100km altitude.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite#getEarthSky <em>Earth Sky</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage#getEarthAtmosphereWorksite()
 * @model
 * @generated
 */
public interface EarthAtmosphereWorksite extends EarthWorksite {
	/**
	 * Returns the value of the '<em><b>Earth Sky</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The EarthSky associated with the worksite,
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Earth Sky</em>' reference.
	 * @see #setEarthSky(EarthSky)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage#getEarthAtmosphereWorksite_EarthSky()
	 * @model required="true" transient="true" volatile="true" derived="true"
	 * @generated
	 */
	EarthSky getEarthSky();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite#getEarthSky <em>Earth Sky</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Earth Sky</em>' reference.
	 * @see #getEarthSky()
	 * @generated
	 */
	void setEarthSky(EarthSky value);

} // EarthAtmosphereWorksite
