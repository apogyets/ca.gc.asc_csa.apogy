/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere;

import ca.gc.asc_csa.apogy.common.topology.TransformNode;

import ca.gc.asc_csa.apogy.core.environment.WorksiteNode;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Earth Atmosphere Worksite Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * SurfaceWorksiteNode specialized for the Earth Surface.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode#getSkyTransformNode <em>Sky Transform Node</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage#getEarthAtmosphereWorksiteNode()
 * @model
 * @generated
 */
public interface EarthAtmosphereWorksiteNode extends WorksiteNode {
	/**
	 * Returns the value of the '<em><b>Sky Transform Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sky Transform Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sky Transform Node</em>' reference.
	 * @see #setSkyTransformNode(TransformNode)
	 * @see ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage#getEarthAtmosphereWorksiteNode_SkyTransformNode()
	 * @model transient="true"
	 * @generated
	 */
	TransformNode getSkyTransformNode();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode#getSkyTransformNode <em>Sky Transform Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sky Transform Node</em>' reference.
	 * @see #getSkyTransformNode()
	 * @generated
	 */
	void setSkyTransformNode(TransformNode value);

} // EarthAtmosphereWorksiteNode
