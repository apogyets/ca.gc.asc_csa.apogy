/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *      Pierre Allard - initial API and implementation
 *         
 * SPDX-License-Identifier: EPL-1.0    
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import ca.gc.asc_csa.apogy.core.environment.Sky;
import ca.gc.asc_csa.apogy.core.environment.WorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ApogyEarthAtmosphereEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.earth.impl.EarthWorksiteImpl;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Atmosphere Worksite</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.impl.EarthAtmosphereWorksiteImpl#getEarthSky <em>Earth Sky</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EarthAtmosphereWorksiteImpl extends EarthWorksiteImpl implements EarthAtmosphereWorksite {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EarthAtmosphereWorksiteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthAtmosphereEnvironmentPackage.Literals.EARTH_ATMOSPHERE_WORKSITE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthSky getEarthSky() 
	{
		  EarthSky es = getEarthSkyGen();
		  
		  if(es == null)
		  {
			  es = ApogyEarthSurfaceEnvironmentFactory.eINSTANCE.createEarthSky();		  
			  setSky(es);
		  }
		  
		  return es;	
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EarthSky getEarthSkyGen() {
		EarthSky earthSky = basicGetEarthSky();
		return earthSky != null && earthSky.eIsProxy() ? (EarthSky)eResolveProxy((InternalEObject)earthSky) : earthSky;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthSky basicGetEarthSky() {
		return (EarthSky) super.getSky();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setEarthSky(EarthSky newEarthSky) 
	{
		super.setSky(newEarthSky);
	}

	@Override
	public void setSky(Sky newSky) 
	{
	  // Removes topology from previous Sky if applicable.
	  if(this.getSky() != null)
	  {
		  if(this.getWorksiteNode() instanceof EarthAtmosphereWorksiteNode)
		  {
			  EarthAtmosphereWorksiteNode earthSurfaceWorksiteNode = (EarthAtmosphereWorksiteNode) this.getWorksiteNode();			  			  			  
			  earthSurfaceWorksiteNode.getSkyTransformNode().getChildren().remove(this.getSky().getSkyNode());
			  earthSurfaceWorksiteNode.getChildren().remove(this.getSky().getSkyNode());
		  } 		  
	  }
	  	  	  
	  // Updates sky
	  super.setSky(newSky);
	  
	  // Adds topology from new Sky Sky if applicable.
	  if(newSky != null) 
	  {		  
		  if(this.getWorksiteNode() instanceof EarthAtmosphereWorksiteNode)
		  {
			  EarthAtmosphereWorksiteNode earthSurfaceWorksiteNode = (EarthAtmosphereWorksiteNode) this.getWorksiteNode();				 
			  earthSurfaceWorksiteNode.getSkyTransformNode().getChildren().add(newSky.getSkyNode());
			  
			  // Should not have to do this..
			  //newSky.getSkyNode().setParent(earthSurfaceWorksiteNode.getSkyTransformNode());
		  }		  
	  }	
	}
		
	@Override
	public WorksiteNode getWorksiteNode() 
	{
		  if(!(super.getWorksiteNode() instanceof EarthAtmosphereWorksiteNode))
		  {
				worksiteNode = ApogyEarthAtmosphereEnvironmentFactory.eINSTANCE.createEarthAtmosphereWorksiteNode();
				worksiteNode.setWorksite(this); 
				worksiteNode.setNodeId(this.getName().replaceAll(" ", "_"));
				worksiteNode.setDescription("Root Node of the Worksite");
		  }
			
		  return worksiteNode;
	}
	  
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE__EARTH_SKY:
				if (resolve) return getEarthSky();
				return basicGetEarthSky();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE__EARTH_SKY:
				setEarthSky((EarthSky)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE__EARTH_SKY:
				setEarthSky((EarthSky)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyEarthAtmosphereEnvironmentPackage.EARTH_ATMOSPHERE_WORKSITE__EARTH_SKY:
				return basicGetEarthSky() != null;
		}
		return super.eIsSet(featureID);
	}

} //EarthAtmosphereWorksiteImpl
