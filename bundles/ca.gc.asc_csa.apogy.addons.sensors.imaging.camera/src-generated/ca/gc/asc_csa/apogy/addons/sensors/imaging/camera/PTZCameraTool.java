/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera;

import javax.vecmath.Color3f;
import javax.vecmath.Point2d;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PTZ Camera Tool</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * An overlay that displays a graduated cross-hair representing the horizontal and vertical Field Of View (FOV) of the
 * camera and allows the user to specified an area in the image for the camera to pan + tilt + zoom to.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool#getSelectionBoxColor <em>Selection Box Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool#getUserSelectionCorner0 <em>User Selection Corner0</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool#getUserSelectionCorner1 <em>User Selection Corner1</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage#getPTZCameraTool()
 * @model abstract="true"
 * @generated
 */
public interface PTZCameraTool extends FOVOverlay, CameraTool {
	/**
	 * Returns the value of the '<em><b>Selection Box Color</b></em>' attribute.
	 * The default value is <code>"0.0,0.0,1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Color to be used to represent the user selection.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Selection Box Color</em>' attribute.
	 * @see #setSelectionBoxColor(Color3f)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage#getPTZCameraTool_SelectionBoxColor()
	 * @model default="0.0,0.0,1.0" unique="false" dataType="ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.Color3f"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel propertyCategory='OVERLAY_PROPERTIES'"
	 * @generated
	 */
	Color3f getSelectionBoxColor();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool#getSelectionBoxColor <em>Selection Box Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection Box Color</em>' attribute.
	 * @see #getSelectionBoxColor()
	 * @generated
	 */
	void setSelectionBoxColor(Color3f value);

	/**
	 * Returns the value of the '<em><b>User Selection Corner0</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Selection Corner0</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * First corner of the user selection. This represent the azimuth, elevation of the corner, denied using the FOV coordinates defined in the overlay.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>User Selection Corner0</em>' attribute.
	 * @see #setUserSelectionCorner0(Point2d)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage#getPTZCameraTool_UserSelectionCorner0()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.Point2d" transient="true"
	 * @generated
	 */
	Point2d getUserSelectionCorner0();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool#getUserSelectionCorner0 <em>User Selection Corner0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Selection Corner0</em>' attribute.
	 * @see #getUserSelectionCorner0()
	 * @generated
	 */
	void setUserSelectionCorner0(Point2d value);

	/**
	 * Returns the value of the '<em><b>User Selection Corner1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Selection Corner1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Second corner of the user selection. This represent the azimuth, elevation of the corner, denied using the FOV coordinates defined in the overlay.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>User Selection Corner1</em>' attribute.
	 * @see #setUserSelectionCorner1(Point2d)
	 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage#getPTZCameraTool_UserSelectionCorner1()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.Point2d" transient="true"
	 * @generated
	 */
	Point2d getUserSelectionCorner1();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool#getUserSelectionCorner1 <em>User Selection Corner1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Selection Corner1</em>' attribute.
	 * @see #getUserSelectionCorner1()
	 * @generated
	 */
	void setUserSelectionCorner1(Point2d value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method called when the user completes it selection in the image. This method should be overriden by sub classes.
	 * @param panIncrement The pan increment to center the camera to the user selected area.
	 * @param tiltIncrement The tilt increment to center the camera to the user selected area.
	 * @param newHorizontalFOV The horizontal FOV angle represented by the user selected area.
	 * @param newVerticalFOV The vertical FOV angle represented by the user selected area.
	 * <!-- end-model-doc -->
	 * @model panIncrementUnique="false"
	 *        panIncrementAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" tiltIncrementUnique="false"
	 *        tiltIncrementAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" newHorizontalFOVUnique="false"
	 *        newHorizontalFOVAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'" newVerticalFOVUnique="false"
	 *        newVerticalFOVAnnotation="http://www.eclipse.org/emf/2002/GenModel apogy_units='rad'"
	 * @generated
	 */
	void commandPTZ(double panIncrement, double tiltIncrement, double newHorizontalFOV, double newVerticalFOV);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * Method that clears the current user selection.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void clearUserSelection();

} // PTZCameraTool
