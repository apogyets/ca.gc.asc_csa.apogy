package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.impl.CameraViewUtilitiesImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Camera View Utilities</b></em>'.
 * <!-- end-user-doc --> *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage#getCameraViewUtilities()
 * @model
 * @generated
 */
public interface CameraViewUtilities extends EObject 
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Return the identifier associated with a given CameraViewConfiguration.
	 * @param cameraViewConfiguration The given CameraViewConfiguration.
	 * @return The identifier, null if none is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" cameraViewConfigurationUnique="false"
	 * @generated
	 */
	String getCameraViewConfigurationIdentifier(CameraViewConfiguration cameraViewConfiguration);

	public static CameraViewUtilities INSTANCE = CameraViewUtilitiesImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Return the CameraViewConfiguration (in the Active Session) with the specified identifier.
	 * @param identifier The CameraViewConfiguration identifier.
	 * @return The CameraViewConfiguration with the specified identifier, null if non is found.
	 * <!-- end-model-doc -->
	 * @model unique="false" identifierUnique="false"
	 * @generated
	 */
	CameraViewConfiguration getActiveCameraViewConfiguration(String identifier);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Return the CameraViewConfigurationList in the Active Session.
	 * @return The CameraViewConfigurationList in the Active Session, null if none is found.
	 * <!-- end-model-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	CameraViewConfigurationList getActiveCameraViewConfigurationList();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Adds a Camera Tool from a given Camera View Configuration.
	 * @param cameraViewConfiguration The CameraViewConfiguration to which to add the CameraTool.
	 * @param cameraTool The camera Tool to add.
	 * <!-- end-model-doc -->
	 * @model cameraViewConfigurationUnique="false" cameraToolUnique="false"
	 * @generated
	 */
	void addCameraTool(CameraViewConfiguration cameraViewConfiguration, CameraTool cameraTool);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Removes cleanly a Camera Tool from a given Camera View Configuration.
	 * @param cameraViewConfiguration The CameraViewConfiguration from which to remove the CameraTool.
	 * @param cameraTool The camera Tool to remove.
	 * <!-- end-model-doc -->
	 * @model cameraViewConfigurationUnique="false" cameraToolUnique="false"
	 * @generated
	 */
	void removeCameraTool(CameraViewConfiguration cameraViewConfiguration, CameraTool cameraTool);

} // CameraViewUtilities
