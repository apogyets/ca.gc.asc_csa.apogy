/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.impl;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;

import javax.vecmath.Color3f;
import javax.vecmath.Point2d;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.swt.graphics.Point;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.AbstractCamera;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.AzimuthDirection;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ElevationDirection;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraFactory;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraTool;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraToolList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.OverlayAlignment;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.PTZCameraTool;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesFactory;
import ca.gc.asc_csa.apogy.common.images.EImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PTZ Camera Tool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.impl.PTZCameraToolImpl#getCameraToolList <em>Camera Tool List</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.impl.PTZCameraToolImpl#getSelectionBoxColor <em>Selection Box Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.impl.PTZCameraToolImpl#getUserSelectionCorner0 <em>User Selection Corner0</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.impl.PTZCameraToolImpl#getUserSelectionCorner1 <em>User Selection Corner1</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class PTZCameraToolImpl extends FOVOverlayImpl implements PTZCameraTool 
{
	/**
	 * The default value of the '{@link #getSelectionBoxColor() <em>Selection Box Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionBoxColor()
	 * @generated
	 * @ordered
	 */
	protected static final Color3f SELECTION_BOX_COLOR_EDEFAULT = (Color3f)ApogyAddonsSensorsImagingCameraFactory.eINSTANCE.createFromString(ApogyAddonsSensorsImagingCameraPackage.eINSTANCE.getColor3f(), "0.0,0.0,1.0");

	/**
	 * The cached value of the '{@link #getSelectionBoxColor() <em>Selection Box Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionBoxColor()
	 * @generated
	 * @ordered
	 */
	protected Color3f selectionBoxColor = SELECTION_BOX_COLOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getUserSelectionCorner0() <em>User Selection Corner0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserSelectionCorner0()
	 * @generated
	 * @ordered
	 */
	protected static final Point2d USER_SELECTION_CORNER0_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUserSelectionCorner0() <em>User Selection Corner0</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserSelectionCorner0()
	 * @generated
	 * @ordered
	 */
	protected Point2d userSelectionCorner0 = USER_SELECTION_CORNER0_EDEFAULT;

	/**
	 * The default value of the '{@link #getUserSelectionCorner1() <em>User Selection Corner1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserSelectionCorner1()
	 * @generated
	 * @ordered
	 */
	protected static final Point2d USER_SELECTION_CORNER1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUserSelectionCorner1() <em>User Selection Corner1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserSelectionCorner1()
	 * @generated
	 * @ordered
	 */
	protected Point2d userSelectionCorner1 = USER_SELECTION_CORNER1_EDEFAULT;

	private ImageSnapshot imageSnapshot;
		
	/*protected Point2d areaPoint0 = null;
	protected Point2d areaPoint1 = null;*/
	protected int clickCount = 0;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PTZCameraToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsImagingCameraPackage.Literals.PTZ_CAMERA_TOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CameraToolList getCameraToolList() {
		if (eContainerFeatureID() != ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST) return null;
		return (CameraToolList)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CameraToolList basicGetCameraToolList() {
		if (eContainerFeatureID() != ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST) return null;
		return (CameraToolList)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCameraToolList(CameraToolList newCameraToolList, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCameraToolList, ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCameraToolList(CameraToolList newCameraToolList) {
		if (newCameraToolList != eInternalContainer() || (eContainerFeatureID() != ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST && newCameraToolList != null)) {
			if (EcoreUtil.isAncestor(this, newCameraToolList))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCameraToolList != null)
				msgs = ((InternalEObject)newCameraToolList).eInverseAdd(this, ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL_LIST__TOOLS, CameraToolList.class, msgs);
			msgs = basicSetCameraToolList(newCameraToolList, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST, newCameraToolList, newCameraToolList));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Color3f getSelectionBoxColor() {
		return selectionBoxColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionBoxColor(Color3f newSelectionBoxColor) {
		Color3f oldSelectionBoxColor = selectionBoxColor;
		selectionBoxColor = newSelectionBoxColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__SELECTION_BOX_COLOR, oldSelectionBoxColor, selectionBoxColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Point2d getUserSelectionCorner0() {
		return userSelectionCorner0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserSelectionCorner0(Point2d newUserSelectionCorner0) {
		Point2d oldUserSelectionCorner0 = userSelectionCorner0;
		userSelectionCorner0 = newUserSelectionCorner0;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0, oldUserSelectionCorner0, userSelectionCorner0));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Point2d getUserSelectionCorner1() {
		return userSelectionCorner1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserSelectionCorner1(Point2d newUserSelectionCorner1) {
		Point2d oldUserSelectionCorner1 = userSelectionCorner1;
		userSelectionCorner1 = newUserSelectionCorner1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1, oldUserSelectionCorner1, userSelectionCorner1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public abstract void commandPTZ(double panIncrement, double tiltIncrement, double newHorizontalFOV, double newVerticalFOV);	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void clearUserSelection() 
	{
		// Clears both corners.
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsImagingCameraPackage.Literals.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0, null);
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsImagingCameraPackage.Literals.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1, null);		
		clickCount = 0;
		
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void initializeCamera(AbstractCamera camera) 
	{
		// TODO: implement this method		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void updateImageSnapshot(ImageSnapshot imageSnapshot) 
	{
		this.imageSnapshot = imageSnapshot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void mouseMoved(AbstractEImage cameraImage, int mouseButton, int x, int y) 
	{
		if(mouseButton == 1)
		{
			if(getUserSelectionCorner0() != null)
			{
				double h = imageSnapshot.convertToHorizontalAngle(x);
				double v = imageSnapshot.convertToVerticalAngle(y);
				
				if(getAzimuthDirection() == AzimuthDirection.POSITIVE_TOWARD_RIGHT)
				{
					h = -h;
				}
				
				if(getElevationDirection() == ElevationDirection.POSITIVE_DOWN)
				{
					v = -v;
				}
					
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsImagingCameraPackage.Literals.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1, new Point2d(h, v));							
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void positionSelected(AbstractEImage cameraImage, int mouseButton, int x, int y) 
	{				
		switch (mouseButton) 
		{					
			// LEFT Button
			case 1:	
			{
				if(clickCount == 0)
				{
					// Clears corner 1					
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsImagingCameraPackage.Literals.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0, null);					
					
					double h = imageSnapshot.convertToHorizontalAngle(x);
					double v = imageSnapshot.convertToVerticalAngle(y);
					
					if(getAzimuthDirection() == AzimuthDirection.POSITIVE_TOWARD_RIGHT)
					{
						h = -h;
					}
					
					if(getElevationDirection() == ElevationDirection.POSITIVE_DOWN)
					{
						v = -v;
					}
										
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsImagingCameraPackage.Literals.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0, new Point2d(h, v));
				}
				else
				{
					double h = imageSnapshot.convertToHorizontalAngle(x);
					double v = imageSnapshot.convertToVerticalAngle(y);
					
					if(getAzimuthDirection() == AzimuthDirection.POSITIVE_TOWARD_RIGHT)
					{
						h = -h;
					}
					
					if(getElevationDirection() == ElevationDirection.POSITIVE_DOWN)
					{
						v = -v;
					}
										
					ApogyCommonTransactionFacade.INSTANCE.basicSet(this, ApogyAddonsSensorsImagingCameraPackage.Literals.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1, new Point2d(h, v));					
					
				}
				
				if(clickCount == 0)
				{
					clickCount = 1;
				}
				else
				{
					clickCount = 0;
				}
				
			}
			break;
			
			// MIDDLE Button
			case 2:
				if(getUserSelectionCorner0() != null && getUserSelectionCorner1() != null)
				{
					commandPTZ();
				}
			break;
			
			// RIGHT Button
			case 3:
			{
				clearUserSelection();
			}
			break;

			default:
			break;
		}				
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCameraToolList((CameraToolList)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST:
				return basicSetCameraToolList(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST:
				return eInternalContainer().eInverseRemove(this, ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL_LIST__TOOLS, CameraToolList.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST:
				if (resolve) return getCameraToolList();
				return basicGetCameraToolList();
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__SELECTION_BOX_COLOR:
				return getSelectionBoxColor();
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0:
				return getUserSelectionCorner0();
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1:
				return getUserSelectionCorner1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST:
				setCameraToolList((CameraToolList)newValue);
				return;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__SELECTION_BOX_COLOR:
				setSelectionBoxColor((Color3f)newValue);
				return;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0:
				setUserSelectionCorner0((Point2d)newValue);
				return;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1:
				setUserSelectionCorner1((Point2d)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST:
				setCameraToolList((CameraToolList)null);
				return;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__SELECTION_BOX_COLOR:
				setSelectionBoxColor(SELECTION_BOX_COLOR_EDEFAULT);
				return;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0:
				setUserSelectionCorner0(USER_SELECTION_CORNER0_EDEFAULT);
				return;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1:
				setUserSelectionCorner1(USER_SELECTION_CORNER1_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST:
				return basicGetCameraToolList() != null;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__SELECTION_BOX_COLOR:
				return SELECTION_BOX_COLOR_EDEFAULT == null ? selectionBoxColor != null : !SELECTION_BOX_COLOR_EDEFAULT.equals(selectionBoxColor);
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER0:
				return USER_SELECTION_CORNER0_EDEFAULT == null ? userSelectionCorner0 != null : !USER_SELECTION_CORNER0_EDEFAULT.equals(userSelectionCorner0);
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__USER_SELECTION_CORNER1:
				return USER_SELECTION_CORNER1_EDEFAULT == null ? userSelectionCorner1 != null : !USER_SELECTION_CORNER1_EDEFAULT.equals(userSelectionCorner1);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == CameraTool.class) {
			switch (derivedFeatureID) {
				case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST: return ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL__CAMERA_TOOL_LIST;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == CameraTool.class) {
			switch (baseFeatureID) {
				case ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL__CAMERA_TOOL_LIST: return ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL__CAMERA_TOOL_LIST;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == CameraTool.class) {
			switch (baseOperationID) {
				case ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL___INITIALIZE_CAMERA__ABSTRACTCAMERA: return ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___INITIALIZE_CAMERA__ABSTRACTCAMERA;
				case ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL___UPDATE_IMAGE_SNAPSHOT__IMAGESNAPSHOT: return ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___UPDATE_IMAGE_SNAPSHOT__IMAGESNAPSHOT;
				case ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL___DISPOSE: return ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___DISPOSE;
				case ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL___MOUSE_MOVED__ABSTRACTEIMAGE_INT_INT_INT: return ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___MOUSE_MOVED__ABSTRACTEIMAGE_INT_INT_INT;
				case ApogyAddonsSensorsImagingCameraPackage.CAMERA_TOOL___POSITION_SELECTED__ABSTRACTEIMAGE_INT_INT_INT: return ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___POSITION_SELECTED__ABSTRACTEIMAGE_INT_INT_INT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___CLEAR_USER_SELECTION:
				clearUserSelection();
				return null;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___COMMAND_PTZ__DOUBLE_DOUBLE_DOUBLE_DOUBLE:
				commandPTZ((Double)arguments.get(0), (Double)arguments.get(1), (Double)arguments.get(2), (Double)arguments.get(3));
				return null;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___INITIALIZE_CAMERA__ABSTRACTCAMERA:
				initializeCamera((AbstractCamera)arguments.get(0));
				return null;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___UPDATE_IMAGE_SNAPSHOT__IMAGESNAPSHOT:
				updateImageSnapshot((ImageSnapshot)arguments.get(0));
				return null;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___MOUSE_MOVED__ABSTRACTEIMAGE_INT_INT_INT:
				mouseMoved((AbstractEImage)arguments.get(0), (Integer)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3));
				return null;
			case ApogyAddonsSensorsImagingCameraPackage.PTZ_CAMERA_TOOL___POSITION_SELECTED__ABSTRACTEIMAGE_INT_INT_INT:
				positionSelected((AbstractEImage)arguments.get(0), (Integer)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (selectionBoxColor: ");
		result.append(selectionBoxColor);
		result.append(", userSelectionCorner0: ");
		result.append(userSelectionCorner0);
		result.append(", userSelectionCorner1: ");
		result.append(userSelectionCorner1);
		result.append(')');
		return result.toString();
	}

	@Override
	public AbstractEImage applyOverlay(AbstractCamera camera, AbstractEImage cameraImage,OverlayAlignment overlayAlignment, int overlayWidth, int overlayHeight) 
	{	
		AbstractEImage fovImage =  super.applyOverlay(camera, cameraImage, overlayAlignment, overlayWidth, overlayHeight);
		AbstractEImage userSelectedAreaImage =  getUserSelectionAreaImage(camera, cameraImage);
		
		AbstractEImage result = EImagesUtilities.INSTANCE.applyOverlay(fovImage, userSelectedAreaImage, true); 
		
		return result;
	}
	
	protected AbstractEImage getUserSelectionAreaImage(AbstractCamera camera, AbstractEImage cameraImage)
	{
		int height = cameraImage.getHeight();
		int width = cameraImage.getWidth();
				
		AbstractEImage areaImage = EImagesUtilities.INSTANCE.createTransparentImage(width, height);
		
		BufferedImage bufferedImage = areaImage.asBufferedImage();			
		Graphics2D g2d = bufferedImage.createGraphics();  
		
		g2d.setColor(getColorOfSelectionBox());	
		
		Point p0 = null;
		Point p1 = null;
		if(getUserSelectionCorner0() != null)
		{
			p0 = getImageLocation(camera, cameraImage, getUserSelectionCorner0().x, getUserSelectionCorner0().y);				
			g2d.drawOval(p0.x-1, p0.y-1, 3, 3);
		}
		
		if(getUserSelectionCorner1() != null)
		{
			p1 = getImageLocation(camera, cameraImage, getUserSelectionCorner1().x, getUserSelectionCorner1().y);
			g2d.drawOval(p1.x-1, p1.y-1, 3, 3);
		}
		
		if((getUserSelectionCorner0() != null) && (getUserSelectionCorner1() != null))
		{						
			g2d.setStroke(new BasicStroke(1));
					
			int x0 = p0.x;
			int x1 = p1.x;		
			int y0 = p0.y;
			int y1 = p1.y;
					
			g2d.drawLine(x0, y0, x1, y0);
			g2d.drawLine(x1, y0, x1, y1);
			g2d.drawLine(x1, y1, x0, y1);
			g2d.drawLine(x0, y1, x0, y0);
								
			g2d.dispose();
			
			EImage resultImage = ApogyCommonImagesFactory.eINSTANCE.createEImage();
			resultImage.setImageContent(bufferedImage);
			
			return resultImage;
		}
		else
		{
			return areaImage;
		}				
	}
	
	protected Color getColorOfSelectionBox()
	{
		 if(getSelectionBoxColor() != null)
		 {
			  return new Color(getSelectionBoxColor().getX(), getSelectionBoxColor().getY(), getSelectionBoxColor().getZ());
		 }
		 else
		 {
			  return Color.BLUE;
		 }
	}
	
	private void commandPTZ()
	{
		if((getUserSelectionCorner0() != null) && (getUserSelectionCorner1() != null))
		{
			double areaPoint0HAngle = getUserSelectionCorner0().x;
			double areaPoint0VAngle = getUserSelectionCorner0().y;

			double areaPoint1HAngle = getUserSelectionCorner1().x;
			double areaPoint1VAngle = getUserSelectionCorner1().y;
						
			double panIncrement = (areaPoint0HAngle + areaPoint1HAngle) / 2.0;
			double tiltIncrement = (areaPoint0VAngle + areaPoint1VAngle) / 2.0;
			
			double newHorizontalFOV = Math.abs(areaPoint1HAngle - areaPoint0HAngle);
			double newVerticalFOV = Math.abs(areaPoint1VAngle - areaPoint0VAngle);
			
			commandPTZ(panIncrement, tiltIncrement, newHorizontalFOV, newVerticalFOV);			
		}
	}
			
} //PTZCameraToolImpl
