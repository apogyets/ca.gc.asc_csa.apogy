package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.RescaleFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.RescaleFilterComposite;

public class RescaleFilterWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.RescaleFilterWizardPage";
		
	private RescaleFilter rescaleFilter;
	private RescaleFilterComposite rescaleFilterComposite;
		
	private DataBindingContext m_bindingContext;
	
	public RescaleFilterWizardPage(RescaleFilter rescaleFilter) 
	{
		super(WIZARD_PAGE_ID);
		this.rescaleFilter = rescaleFilter;
			
		setTitle("Rescale Filter.");
		setDescription("Select filter settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		rescaleFilterComposite = new RescaleFilterComposite(top, SWT.NONE)
		{
			@Override
			protected void newExposureSelected(double exposure)
			{
				validate();
			}			
		};
		rescaleFilterComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		rescaleFilterComposite.setRescaleFilter(rescaleFilter);
				
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(rescaleFilter.getScale() < 0)
		{
			setErrorMessage("Invalid scale specified ! Must be equal or greater than zero.");
		}				
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}
