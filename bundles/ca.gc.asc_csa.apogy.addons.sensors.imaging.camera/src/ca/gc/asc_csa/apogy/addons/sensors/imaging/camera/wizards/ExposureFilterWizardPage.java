package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ExposureFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.ExposureFilterComposite;

public class ExposureFilterWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.ExposureFilterWizardPage";
		
	private ExposureFilter exposureFilter;
	private ExposureFilterComposite exposureFilterComposite;
		
	private DataBindingContext m_bindingContext;
	
	public ExposureFilterWizardPage(ExposureFilter exposureFilter) 
	{
		super(WIZARD_PAGE_ID);
		this.exposureFilter = exposureFilter;
			
		setTitle("Exposure Filter.");
		setDescription("Select filter settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		exposureFilterComposite = new ExposureFilterComposite(top, SWT.NONE)
		{
			@Override
			protected void newExposureSelected(double exposure)
			{
				validate();
			}			
		};
		exposureFilterComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		exposureFilterComposite.setExposureFilter(exposureFilter);
				
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(exposureFilter.getExposure() < 0)
		{
			setErrorMessage("Invalid exposure specified ! Must be equal or greater than zero.");
		}				
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}
