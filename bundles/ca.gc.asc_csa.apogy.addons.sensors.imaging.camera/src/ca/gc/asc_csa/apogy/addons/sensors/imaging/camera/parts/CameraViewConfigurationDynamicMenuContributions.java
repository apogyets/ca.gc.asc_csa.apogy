package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.parts;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.ui.di.AboutToHide;
import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewUtilities;
import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.topology.ArbitraryViewPoint;
import ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint;

public class CameraViewConfigurationDynamicMenuContributions 
{
	
	@Inject
	private EModelService modelService;
	
	@AboutToShow
	public void aboutToShow(List<MMenuElement> items)
	{
		// First, get the list of view point from the active session.
		CameraViewConfigurationList cameraViewConfigurationList = CameraViewUtilities.INSTANCE.getActiveCameraViewConfigurationList();
		
		if(cameraViewConfigurationList != null)
		{
			populate(items);	
		}
	}
	
	@AboutToHide
	public void aboutToHide(List<MMenuElement> items)
	{		
	}
	
	protected void populate(List<MMenuElement> items)
	{
		// Remove existing menu items
		for (MMenuElement child : items)
		{
			child.setToBeRendered(false);
			child.setVisible(false);			
		}
		items.clear();
				
		// Gets the list of Camera View Configuration available.
		CameraViewConfigurationList cameraViewConfigurationList = CameraViewUtilities.INSTANCE.getActiveCameraViewConfigurationList();
				
		for(CameraViewConfiguration cameraViewConfiguration : cameraViewConfigurationList.getCameraViewConfigurations())
		{
			MDirectMenuItem dynamicItem = modelService.createModelElement(MDirectMenuItem.class);
	        dynamicItem.setLabel(cameraViewConfiguration.getName());
	        dynamicItem.setContributionURI("bundleclass://ca.gc.asc_csa.apogy.addons.sensors.imaging.camera/ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handlers.SetActiveCameraViewConfigurationHandler");
	        dynamicItem.setContributorURI("platform:/plugin/ca.gc.asc_csa.apogy.addons.sensors.imaging.camera");	        
	        dynamicItem.setToBeRendered(true);
	        
	        dynamicItem.getTransientData().put("cameraViewConfiguration", cameraViewConfiguration);
	
	        items.add(dynamicItem);
		}		
	}		
	
	protected String getIconURIForAbstractViewPoint(AbstractViewPoint abstractViewPoint)
	{
		String uri = null;
		
		if(abstractViewPoint instanceof ArbitraryViewPoint)
		{
			uri = "platform:/plugin/ca.gc.asc_csa.apogy.common.topology.edit/icons/full/obj16/ArbitraryViewPoint.gif";
		}
		else if(abstractViewPoint instanceof AttachedViewPoint)
		{
			uri = "platform:/plugin/ca.gc.asc_csa.apogy.common.topology.edit/icons/full/obj16/AttachedViewPoint.gif";
		}		
		
		return uri;
	}
}
