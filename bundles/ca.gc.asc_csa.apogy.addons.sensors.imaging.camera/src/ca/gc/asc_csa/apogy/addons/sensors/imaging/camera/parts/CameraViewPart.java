package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.parts;

import java.util.Collections;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraFactory;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationReference;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewUtilities;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraRCPConstants;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.CameraViewComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.parts.ToolBarMenuPart;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.ui.Activator;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

@SuppressWarnings("restriction")
public class CameraViewPart extends AbstractSessionBasedPart implements ToolBarMenuPart 
{
	public static final String CameraViewConfigurationID = "CameraViewConfiguration";
	protected String partName = "Camera View";
	
	private CameraViewConfiguration cameraViewConfiguration;
	
	@Inject
	ECommandService commandService;

	@Inject
	EHandlerService handlerService;

	@Override
	@PostConstruct
	public void createPartControl(Composite parent, MPerspective perspective) {
		super.createPartControl(parent, perspective);
	}

	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		CameraViewComposite cameraViewComposite = ((CameraViewComposite) getActualComposite());
		cameraViewComposite.setCameraViewConfigurationList(CameraViewUtilities.INSTANCE.getActiveCameraViewConfigurationList());
		
		if(invocatorSession != null)
		{
			CameraViewConfigurationReference ref = (CameraViewConfigurationReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, CameraViewConfigurationID);
			if(ref != null)
			{
				cameraViewConfiguration = ref.getCameraViewConfiguration();	
			}
			else
			{
				Logger.INSTANCE.log(Activator.ID, this, "No Camera View Configuration could be read back, setting it to null. ", EventSeverity.WARNING);
			}
											
			cameraViewComposite.setCameraViewConfiguration(cameraViewConfiguration);			
		}
		
		//-clearPersistedState 
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		new CameraViewComposite(parent, style) 
		{
			@Override
			protected void newCameraViewConfigurationSelected(CameraViewConfiguration newCameraViewConfiguration) 
			{
				cameraViewConfiguration = newCameraViewConfiguration;								
				selectionService.setSelection(cameraViewConfiguration);
				
				if(cameraViewConfiguration != null)
				{
					mPart.setLabel(partName + " - " + cameraViewConfiguration.getName());
				}
				
				ParameterizedCommand command = commandService.createCommand(ApogyAddonsSensorsImagingCameraRCPConstants.COMMAND__UPDATE_CAMERA_VIEW_TOOLBAR__ID, Collections.emptyMap());
				handlerService.executeHandler(command);								
			}

			@Override
			protected void newTabFolderSelection(String parameterID) {
				HashMap<String, Object> parameters = new HashMap<>(1);
				parameters.put(ca.gc.asc_csa.apogy.common.ui.ApogyCommonUIRCPConstants.COMMAND_PARAMETER__OPEN_TOOL_BAR_MENU_ID__ID,
						parameterID);

				ParameterizedCommand command = commandService.createCommand(
						ca.gc.asc_csa.apogy.common.ui.ApogyCommonUIRCPConstants.COMMAND__OPEN_TOOL_BAR_MENU__ID, parameters);
				handlerService.executeHandler(command);
			}
		};
	}
	
	@Override
	public void userPostConstruct(MPart mPart) 
	{	
		super.userPostConstruct(mPart);
		reloadToolBarAndMenus();
	}
	
	@Override
	public void userPersistState(MPart mPart) 
	{
		try
		{			
			if(cameraViewConfiguration != null)
			{		
				CameraViewConfigurationReference ref = ApogyAddonsSensorsImagingCameraFactory.eINSTANCE.createCameraViewConfigurationReference();
				ref.setCameraViewConfiguration(cameraViewConfiguration);
				ApogyCoreInvocatorUIFacade.INSTANCE.saveToPersistedState(mPart, CameraViewConfigurationID, ref);
			}		
			
			// Reset part name
			mPart.setLabel(partName);
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
	}
	
	public void setDisplayRectifiedImage(boolean value) {
		ApogyCommonTransactionFacade.INSTANCE.basicSet(
				((CameraViewComposite) getActualComposite()).getSelectedCameraViewConfiguration(),
				ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION__DISPLAY_RECTIFIED_IMAGE,
				value);
	}

	public void setImageAutoSaveEnable(boolean value) {
		ApogyCommonTransactionFacade.INSTANCE.basicSet(
				((CameraViewComposite) getActualComposite()).getSelectedCameraViewConfiguration(),
				ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION__IMAGE_AUTO_SAVE_ENABLE,
				value);
	}

	public void setSaveImageWithOverlays(boolean value) {
		ApogyCommonTransactionFacade.INSTANCE.basicSet(
				((CameraViewComposite) getActualComposite()).getSelectedCameraViewConfiguration(),
				ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION__SAVE_IMAGE_WITH_OVERLAYS,
				value);
	}

	@Override
	public void toolItemSelectedChanged(String parameterID, boolean selected) {
		((CameraViewComposite) getActualComposite()).updatetoolbarMenuComposite(parameterID, selected);
	}

	@Override
	public boolean canExecute() {
		return getActualComposite() instanceof CameraViewComposite && ((CameraViewComposite)getActualComposite()).getSelectedCameraViewConfiguration() != null;
	}

}
