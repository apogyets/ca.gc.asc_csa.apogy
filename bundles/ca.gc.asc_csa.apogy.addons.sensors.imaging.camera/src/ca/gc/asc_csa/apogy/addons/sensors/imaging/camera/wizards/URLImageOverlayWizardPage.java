package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.URLImageOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.URLImageOverlayComposite;

public class URLImageOverlayWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.RescaleFilterWizardPage";
		
	private URLImageOverlay urlImageOverlay;
	private URLImageOverlayComposite urlImageOverlayComposite;
		
	private DataBindingContext m_bindingContext;
	
	public URLImageOverlayWizardPage(URLImageOverlay urlImageOverlay) 
	{
		super(WIZARD_PAGE_ID);
		this.urlImageOverlay = urlImageOverlay;
			
		setTitle("URL Image Overlay.");
		setDescription("Select the image URL and the image resize policy.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		urlImageOverlayComposite = new URLImageOverlayComposite(top, SWT.NONE)
		{
			@Override
			protected void newURLSelected(String newURL) 
			{
				validate();
			}
		};
		urlImageOverlayComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		urlImageOverlayComposite.setUrlImageOverlay(urlImageOverlay);
				
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(urlImageOverlay.getUrl() == null)
		{
			setErrorMessage("No image URL specified !");
		}				
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}
