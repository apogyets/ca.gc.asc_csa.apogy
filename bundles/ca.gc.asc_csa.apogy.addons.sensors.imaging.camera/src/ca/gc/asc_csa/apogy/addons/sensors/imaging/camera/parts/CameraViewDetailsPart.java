package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraRCPConstants;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.parts.AbstractFormPropertiesPart;

public class CameraViewDetailsPart extends AbstractFormPropertiesPart {

	@Override
	protected boolean isReadOnly() {
		return false;
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		HashMap<String, ISelectionListener> map = new HashMap<>();

		map.put(ApogyAddonsSensorsImagingCameraRCPConstants.PART__CAMERA_VIEW__ID, DEFAULT_LISTENER);

		return map;
	}
}
