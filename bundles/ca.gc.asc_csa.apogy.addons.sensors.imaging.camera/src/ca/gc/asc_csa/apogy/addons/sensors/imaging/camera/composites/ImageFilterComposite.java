package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFilter;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesFactory;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.URLEImage;
import ca.gc.asc_csa.apogy.common.images.ui.composites.ImageDisplayComposite;

public class ImageFilterComposite extends Composite 
{
	private ImageFilter imageFilter;
	private Adapter imageFilterAdapter;
	
	private ImageDisplayComposite originalImage;
	private ImageDisplayComposite filteredImage;
	
	private AbstractEImage unfilteredImage = null;
	
	public ImageFilterComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		Label lblOriginalImage = new Label(this, SWT.NONE);
		lblOriginalImage.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblOriginalImage.setText("Original Image:");
		
		Label lblFilteredImage = new Label(this, SWT.NONE);
		lblFilteredImage.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblFilteredImage.setText("Filtered Image:");
		
		originalImage = new ImageDisplayComposite(this, SWT.BORDER);
		GridData gd_originalImage = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_originalImage.heightHint = 180;
		gd_originalImage.minimumHeight = 180;
		gd_originalImage.minimumWidth = 320;
		gd_originalImage.widthHint = 320;
		originalImage.setLayoutData(gd_originalImage);
			
		
		filteredImage = new ImageDisplayComposite(this, SWT.BORDER);
		GridData gd_filteredImage = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_filteredImage.widthHint = 320;
		gd_filteredImage.minimumWidth = 320;
		gd_filteredImage.minimumHeight = 180;
		gd_filteredImage.heightHint = 180;
		filteredImage.setLayoutData(gd_filteredImage);
	}

	public ImageFilter getImageFilter() {
		return imageFilter;
	}

	public void setImageFilter(ImageFilter imageFilter) 
	{
		if(this.imageFilter != null)
		{
			this.imageFilter.eAdapters().remove(getImageFilterAdapter());
		}
		
		this.imageFilter = imageFilter;
		
		if(imageFilter != null)
		{			
			updateImage();
			
			// Register listener.
			imageFilter.eAdapters().add(getImageFilterAdapter());
		}
	}
	
	private void updateImage()
	{
		// Updates preview.
		if(!isDisposed())
		{
			getDisplay().asyncExec(new Runnable() 
			{			
				@Override
				public void run() 
				{	
					try
					{
						// Update image.
						AbstractEImage filtered = imageFilter.filter(null, getUnfilteredImage());
						
						if(filtered != null)
						{
							filteredImage.setImageData(EImagesUtilities.INSTANCE.convertToImageData(filtered.asBufferedImage()));
							filteredImage.fitImage();
						}
						
						originalImage.setImageData(EImagesUtilities.INSTANCE.convertToImageData(getUnfilteredImage().asBufferedImage()));
						originalImage.fitImage();	
					}
					catch (Exception e) 
					{
						// TODO
					}									
				}
			});	
		}
	}
	
	private AbstractEImage getUnfilteredImage() 
	{
		if(unfilteredImage == null)
		{
			URLEImage urlImage = ApogyCommonImagesFactory.eINSTANCE.createURLEImage();
			urlImage.setUrl("platform:/plugin/ca.gc.asc_csa.apogy.addons.sensors.imaging.camera/images/unfiltered_image.jpg");		  		  
			unfilteredImage = urlImage;
		}
		return unfilteredImage;
	}

	private Adapter getImageFilterAdapter()
	{
		if(imageFilterAdapter == null)
		{
			imageFilterAdapter = new EContentAdapter()
			{
				@Override
				public void notifyChanged(Notification notification) 
				{
					// Update result image.
					updateImage();
					
					super.notifyChanged(notification);
				}
			};
		}
		
		return imageFilterAdapter;
	}
	
	
	
}
