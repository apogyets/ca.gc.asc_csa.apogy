package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import java.awt.GraphicsEnvironment;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Spinner;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AbstractTextOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.OverlayAlignment;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;

public class AbstractTextOverlayComposite extends Composite 
{
	public static final String PREVIEW_TEXT = "Test 1.234";
	public static final int PREVIEW_IMG_WIDTH = 1200;
	public static final int PREVIEW_IMG_HEIGHT = 400;
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private AbstractTextOverlay abstractTextOverlay;
	
	private	ComboViewer fontComboViewer;
	private Combo fontCombo;
	private Spinner fontSizeSpinner;	
	private Color3fComposite fontColorComposite;
	
	private ComboViewer overlayAlignmentComboViewer;
	private Combo overlayAlignmentCombo;
	private Spinner horizontalOffsetSpinner;
	private Spinner verticalOffsetSpinner;

	private DataBindingContext m_bindingContext;
	
	public AbstractTextOverlayComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(2, false));
		
		// Alignment
		Group grpPositionOffsets = new Group(this, SWT.NONE);
		grpPositionOffsets.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpPositionOffsets.setText("Position");
		grpPositionOffsets.setLayout(new GridLayout(4, false));
		
		Label lblAlignment = new Label(grpPositionOffsets, SWT.NONE);
		lblAlignment.setText("Overlay Alignment :");
		
		overlayAlignmentComboViewer =  new ComboViewer(grpPositionOffsets, SWT.NONE);
		overlayAlignmentCombo = overlayAlignmentComboViewer.getCombo();
		GridData gd_overlayAlignmentCombo = new GridData(SWT.LEFT, SWT.CENTER, true, false, 3, 1);
		gd_overlayAlignmentCombo.widthHint = 250;
		gd_overlayAlignmentCombo.minimumWidth = 250;
		overlayAlignmentCombo.setLayoutData(gd_overlayAlignmentCombo);		
		overlayAlignmentComboViewer.setContentProvider(new ArrayContentProvider());
		overlayAlignmentComboViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		overlayAlignmentComboViewer.setInput(OverlayAlignment.values());		
		
		Label lblHorizontalOffset = new Label(grpPositionOffsets, SWT.NONE);
		lblHorizontalOffset.setText("Horizontal Offset:");
		
		horizontalOffsetSpinner = new Spinner(grpPositionOffsets, SWT.BORDER);
		GridData gd_horizontalOffsetSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_horizontalOffsetSpinner.widthHint = 50;
		gd_horizontalOffsetSpinner.minimumWidth = 50;
		horizontalOffsetSpinner.setLayoutData(gd_horizontalOffsetSpinner);
		
		Label lblVerticalOffset = new Label(grpPositionOffsets, SWT.NONE);
		lblVerticalOffset.setText("Vertical Offset:");
		
		verticalOffsetSpinner = new Spinner(grpPositionOffsets, SWT.BORDER);
		GridData gd_verticalOffsetSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_verticalOffsetSpinner.minimumWidth = 50;
		gd_verticalOffsetSpinner.widthHint = 50;
		verticalOffsetSpinner.setLayoutData(gd_verticalOffsetSpinner);		
		
		
		// Font
		Group grpFontSettings = new Group(this, SWT.NONE);
		grpFontSettings.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		grpFontSettings.setText("Font Settings");
		grpFontSettings.setLayout(new GridLayout(4, false));
		
		Label lblFont = new Label(grpFontSettings, SWT.NONE);
		lblFont.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFont.setText("Font:");
		
		fontComboViewer = new ComboViewer(grpFontSettings, SWT.NONE);
		fontCombo = fontComboViewer.getCombo();
		GridData gd_fontCombo = new GridData(SWT.FILL, SWT.CENTER, true, false, 3, 1);
		gd_fontCombo.widthHint = 250;
		gd_fontCombo.minimumWidth = 250;
		fontCombo.setLayoutData(gd_fontCombo);
		fontComboViewer.setContentProvider(ArrayContentProvider.getInstance());
		fontComboViewer.setInput(getAvailableFonts());
		fontComboViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				if(event.getSelection().isEmpty())
				{					
				}
				else if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					if(iStructuredSelection.getFirstElement() instanceof String)
					{
						String fontName = (String) iStructuredSelection.getFirstElement();
						if(abstractTextOverlay != null)
						{
							if(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getAbstractTextOverlay()) == null)
							{
								ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(getAbstractTextOverlay());
							}							
								
							ApogyCommonTransactionFacade.INSTANCE.basicSet(getAbstractTextOverlay(), ApogyAddonsSensorsImagingCameraPackage.Literals.ABSTRACT_TEXT_OVERLAY__FONT_NAME, fontName);
						}
					}
					else
					{						
					}
				}
								
				newSelection(event.getSelection());				
			}
		});
		
		Label lblFontSize = new Label(grpFontSettings, SWT.NONE);
		lblFontSize.setText("Font Size:");
		
		fontSizeSpinner = new Spinner(grpFontSettings, SWT.BORDER);
		GridData gd_fontSizeSpinner = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_fontSizeSpinner.minimumWidth = 50;
		gd_fontSizeSpinner.widthHint = 50;
		fontSizeSpinner.setLayoutData(gd_fontSizeSpinner);		
		
		Label lblTextColor = new Label(grpFontSettings, SWT.NONE);
		lblTextColor.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblTextColor.setText("Text Color:");
		
		fontColorComposite = new Color3fComposite(grpFontSettings, SWT.NONE);		
		fontColorComposite.setFeature(ApogyAddonsSensorsImagingCameraPackage.Literals.ABSTRACT_TEXT_OVERLAY__TEXT_COLOR);	
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public AbstractTextOverlay getAbstractTextOverlay() {
		return abstractTextOverlay;
	}

	public void setAbstractTextOverlay(AbstractTextOverlay abstractTextOverlay) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.abstractTextOverlay = abstractTextOverlay;
		
		if(abstractTextOverlay != null)
		{
			m_bindingContext = customInitDataBindings();						
		}
	}
	
	protected void newSelection(ISelection selection)
	{		
	}
	
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		// Initialize font color.
		fontColorComposite.setOwner(getAbstractTextOverlay());
		
		// Initialize font name
		
		IObservableValue<Double> observeFontName = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractTextOverlay), 
				  										FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.ABSTRACT_TEXT_OVERLAY__FONT_NAME)).observe(abstractTextOverlay);
		IObservableValue<?> observeFontComboViewer = WidgetProperties.selection().observe(fontComboViewer.getCombo());
		
		bindingContext.bindValue(observeFontComboViewer,
								 observeFontName, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		
		/* Font size Value. */
		IObservableValue<Double> observeFontSize = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractTextOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.ABSTRACT_TEXT_OVERLAY__FONT_SIZE)).observe(abstractTextOverlay);
		IObservableValue<String> observeFontSizeSpinner = WidgetProperties.selection().observe(fontSizeSpinner);

		bindingContext.bindValue(observeFontSizeSpinner,
								 observeFontSize, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
				
		
		/* Alignment.*/
		IObservableValue<Double> observeAlignment = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractTextOverlay),
																				  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_OVERLAY__OVERLAY_ALIGNMENT)).observe(abstractTextOverlay);
		
		IObservableValue<?> observeAlignmentCombViewer = ViewerProperties.singleSelection().observe(overlayAlignmentComboViewer);
		bindingContext.bindValue(observeAlignmentCombViewer, 
								 observeAlignment,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		/* Horizontal Offset Value. */
		IObservableValue<Double> observeHorizontalOffset = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractTextOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.ABSTRACT_TEXT_OVERLAY__HORIZONTAL_OFFSET)).observe(abstractTextOverlay);
		IObservableValue<String> observeHorizontalOffsetSpinner = WidgetProperties.selection().observe(horizontalOffsetSpinner);

		bindingContext.bindValue(observeHorizontalOffsetSpinner,
								 observeHorizontalOffset, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		/* Vertical Offset Value. */
		IObservableValue<Double> observeVerticalOffset = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(abstractTextOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.ABSTRACT_TEXT_OVERLAY__VERTICAL_OFFSET)).observe(abstractTextOverlay);
		IObservableValue<String> observeVerticalOffsetSpinner = WidgetProperties.selection().observe(verticalOffsetSpinner);

		bindingContext.bindValue(observeVerticalOffsetSpinner,
								 observeVerticalOffset, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		return bindingContext;
	}
	
	protected List<String> getAvailableFonts()
	{
		List<String> fonts = new ArrayList<String>();
		
		String[] fontNames = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
	   	for ( int i = 0; i < fontNames.length; i++ )
	   	{
	   		fonts.add(fontNames[i]);
	    }
		
		return fonts;
	}
}
