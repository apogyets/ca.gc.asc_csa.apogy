package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageSizePolicy;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.URLImageOverlay;
import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesFactory;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.URLEImage;
import ca.gc.asc_csa.apogy.common.images.ui.composites.ImageDisplayComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.URLSelectionComposite;

public class URLImageOverlayComposite extends Composite 
{
	private URLImageOverlay urlImageOverlay;
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private ComboViewer imageSizePolicyComboViewer;
	private URLSelectionComposite urlSelectionComposite;
	
	private Label imageWidthValue;
	private Label imageHeightValue;
	private ImageDisplayComposite imageDisplayComposite;	
	
	private DataBindingContext m_bindingContext;
	
	public URLImageOverlayComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(2, false));
		
		Label lblImageSizePolicy = new Label(this, SWT.NONE);
		lblImageSizePolicy.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblImageSizePolicy.setText("Image Resize Policy :");
	
		imageSizePolicyComboViewer = new ComboViewer(this, SWT.NONE);				
		GridData gd_imageSizePolicyComboViewer = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_imageSizePolicyComboViewer.minimumWidth = 200;
		gd_imageSizePolicyComboViewer.widthHint = 200;
		imageSizePolicyComboViewer.getCombo().setLayoutData(gd_imageSizePolicyComboViewer);
		imageSizePolicyComboViewer.setContentProvider(new ArrayContentProvider());
		imageSizePolicyComboViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		imageSizePolicyComboViewer.setInput(ImageSizePolicy.values());

		urlSelectionComposite = new URLSelectionComposite(this, SWT.None, new String[]{"*.gif", "*.jpg", "*.jpeg"}, true, true, true)
		{
			@Override
			protected void urlStringSelected(String newURLString) 
			{		
				if(urlImageOverlay != null)
				{
					if(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(urlImageOverlay) == null)
					{
						ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(urlImageOverlay);
					}
					ApogyCommonTransactionFacade.INSTANCE.basicSet(urlImageOverlay, ApogyAddonsSensorsImagingCameraPackage.Literals.URL_IMAGE_OVERLAY__URL, newURLString);
					
					URLImageOverlayComposite.this.updateImagePreview(newURLString);
					
					newURLSelected(newURLString);
				}
			}
		};
		urlSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		
		Group grpImagePreview = new Group(this, SWT.BORDER);
		grpImagePreview.setText("Image Preview");
		grpImagePreview.setLayout(new GridLayout(2,false));
		grpImagePreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));		
		
		Composite imageSizeComposite = new Composite(grpImagePreview, SWT.NONE);		
		imageSizeComposite.setLayout(new GridLayout(2, false));
		imageSizeComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		
		Label lblimageWidthInMeters = new Label(imageSizeComposite, SWT.NONE);		
		lblimageWidthInMeters.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblimageWidthInMeters.setText("Image width (pixels):");
		
		imageWidthValue = new Label(imageSizeComposite, SWT.BORDER);
		GridData gd_imageWidthValue = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_imageWidthValue.minimumWidth = 100;
		gd_imageWidthValue.widthHint = 100;
		imageWidthValue.setLayoutData(gd_imageWidthValue);		
		
		Label lblimageHeightInMeters = new Label(imageSizeComposite, SWT.NONE);
		lblimageHeightInMeters.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblimageHeightInMeters.setText("Image height (pixels):");
		
		imageHeightValue = new Label(imageSizeComposite, SWT.BORDER);
		GridData gd_imageHeightValue = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_imageHeightValue.minimumWidth = 100;
		gd_imageHeightValue.widthHint = 100;		
		imageHeightValue.setLayoutData(gd_imageHeightValue);
		new Label(grpImagePreview, SWT.NONE);
		
		
		imageDisplayComposite = new ImageDisplayComposite(grpImagePreview, SWT.BORDER);
		GridData gd_imageDisplayComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gd_imageDisplayComposite.heightHint = 200;
		gd_imageDisplayComposite.minimumHeight = 200;
		gd_imageDisplayComposite.widthHint = 200;
		gd_imageDisplayComposite.minimumWidth = 200;
		imageDisplayComposite.setLayoutData(gd_imageDisplayComposite);
		imageDisplayComposite.addListener(SWT.Resize, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				imageDisplayComposite.fitImage();
			}
		});		
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public URLImageOverlay getUrlImageOverlay() 
	{
		return urlImageOverlay;
	}

	public void setUrlImageOverlay(URLImageOverlay urlImageOverlay) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.urlImageOverlay = urlImageOverlay;
		
		if(urlImageOverlay != null)
		{
			if(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(urlImageOverlay) == null)
			{
				ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(urlImageOverlay);
			}
			
			m_bindingContext = customInitDataBindings();
		}
	}
	
	protected void newURLSelected(String newURL)
	{		
	}
	
	protected void updateImagePreview(String urlString)
	{
		try
		{
			// Update preview image.
			URLEImage urlEImage = ApogyCommonImagesFactory.eINSTANCE.createURLEImage();
			urlEImage.setUrl(urlString);			
			
			imageWidthValue.setText(Integer.toString(urlEImage.getWidth()));
			imageHeightValue.setText(Integer.toString(urlEImage.getHeight()));
				
			ImageData imageData = EImagesUtilities.INSTANCE.convertToImageData(urlEImage.asBufferedImage());
										
			imageDisplayComposite.setImageData(imageData);
			imageDisplayComposite.fitImage();
		}
		catch(Exception e)
		{
			imageDisplayComposite.setImageData(null);			
			imageWidthValue.setText("0");
			imageHeightValue.setText("0");
			
			e.printStackTrace();										
		}
	}
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		if(urlImageOverlay.getUrl() != null)
		{
			urlSelectionComposite.setUrlString(urlImageOverlay.getUrl());
		}
		
		/* Image Resize Policy.*/
		IObservableValue<Double> observeImageSizePolicy = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(),
																				  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.URL_IMAGE_OVERLAY__IMAGE_SIZE_POLICY)).observe(urlImageOverlay);
		
		IObservableValue<?> observeImageSizePolicyCombViewer = ViewerProperties.singleSelection().observe(imageSizePolicyComboViewer);
		bindingContext.bindValue(observeImageSizePolicyCombViewer, 
								 observeImageSizePolicy,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		return bindingContext;
	}
}
