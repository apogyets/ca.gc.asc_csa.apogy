package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.AbstractTextOverlay;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.AbstractTextOverlayComposite;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.DrawnCameraOverlayPreviewComposite;

public class AbstractTextOverlayWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.AbstractTextOverlayWizardPage";
		
	private AbstractTextOverlay abstractTextOverlay;
	
	private AbstractTextOverlayComposite abstractTextOverlayComposite; 
	private DrawnCameraOverlayPreviewComposite drawnCameraOverlayPreviewComposite;
	
	private DataBindingContext m_bindingContext;
	
	public AbstractTextOverlayWizardPage(AbstractTextOverlay abstractTextOverlay) 
	{
		super(WIZARD_PAGE_ID);
		this.abstractTextOverlay = abstractTextOverlay;
			
		setTitle("Text Overlay.");
		setDescription("Select the text settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		Group grpGeneral = new Group(top, SWT.NONE);
		grpGeneral.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpGeneral.setText("General");
		grpGeneral.setLayout(new GridLayout(1, false));
		
		abstractTextOverlayComposite = new AbstractTextOverlayComposite(grpGeneral, SWT.NONE)
		{
			protected void newSelection(ISelection selection) 
			{
				validate();
			}
		};
		abstractTextOverlayComposite.setAbstractTextOverlay(abstractTextOverlay);
		abstractTextOverlayComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				
		// Preview
		Group grpPreview = new Group(top, SWT.BORDER);
		grpPreview.setLayout(new GridLayout(1, false));
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpPreview.setText("Preview");

		drawnCameraOverlayPreviewComposite = new DrawnCameraOverlayPreviewComposite(grpPreview, SWT.BORDER);
		GridData gd_imagePreviewComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imagePreviewComposite.minimumWidth = 160;
		gd_imagePreviewComposite.minimumHeight = 320;
		drawnCameraOverlayPreviewComposite.setLayoutData(gd_imagePreviewComposite);		
		drawnCameraOverlayPreviewComposite.setDrawnCameraOverlay(abstractTextOverlay);
		
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(abstractTextOverlay.getFontName() == null)
		{
			setErrorMessage("No font selected !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}
