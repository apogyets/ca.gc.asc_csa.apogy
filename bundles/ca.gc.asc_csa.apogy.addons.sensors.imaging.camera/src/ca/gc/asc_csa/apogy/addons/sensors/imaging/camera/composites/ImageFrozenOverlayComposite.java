package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFrozenOverlay;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class ImageFrozenOverlayComposite extends Composite 
{
	private ImageFrozenOverlay imageFrozenOverlay;
	
	private Text expectedRefreshPeriodTxt;
	private Text frozenMessageTxt;
	
	private AbstractTextOverlayComposite abstractTextOverlayComposite;
	private DrawnCameraOverlayPreviewComposite drawnCameraOverlayPreviewComposite;
	
	private DataBindingContext m_bindingContext;
	
	public ImageFrozenOverlayComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(1, false));
		
		Group grpGeneral = new Group(this, SWT.NONE);
		grpGeneral.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpGeneral.setText("General");
		grpGeneral.setLayout(new GridLayout(2, false));
		
		abstractTextOverlayComposite = new AbstractTextOverlayComposite(grpGeneral, SWT.NONE);				
		abstractTextOverlayComposite.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
		
		Group grpFrozen = new Group(this, SWT.NONE);
		grpFrozen.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpFrozen.setText("Frozen Settings");
		grpFrozen.setLayout(new GridLayout(4, false));
		
		Label lblNumberFormat = new Label(grpFrozen, SWT.NONE);
		lblNumberFormat.setText("Expected Image Refresh Period (s) :");
						
		expectedRefreshPeriodTxt = new Text(grpFrozen, SWT.BORDER);
		GridData gd_numberFormatTxt = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_numberFormatTxt.minimumWidth = 100;
		gd_numberFormatTxt.widthHint = 100;
		expectedRefreshPeriodTxt.setLayoutData(gd_numberFormatTxt);
		expectedRefreshPeriodTxt.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				try
				{
					double expectedImageUpdatePeriod = Double.parseDouble(expectedRefreshPeriodTxt.getText());
					newExpectedImageUpdatePeriodSelected(expectedImageUpdatePeriod);
				}
				catch (Exception ex) 
				{					
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		
		Label lblFrozenMessage = new Label(grpFrozen, SWT.NONE);
		lblFrozenMessage.setText("Frozen Message :");
		
		frozenMessageTxt = new Text(grpFrozen, SWT.BORDER);
		gd_numberFormatTxt.minimumWidth = 200;
		gd_numberFormatTxt.widthHint = 200;
		GridData gd_frozenMessageTxt = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_frozenMessageTxt.minimumWidth = 200;
		gd_frozenMessageTxt.widthHint = 200;
		frozenMessageTxt.setLayoutData(gd_frozenMessageTxt);
		frozenMessageTxt.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				newFrozenMessageSelected(frozenMessageTxt.getText());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
		});
		
		// Preview
		Group grpPreview = new Group(this, SWT.NONE);
		grpPreview.setLayout(new GridLayout(1, false));
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1));
		grpPreview.setText("Preview");
		
		drawnCameraOverlayPreviewComposite = new DrawnCameraOverlayPreviewComposite(grpPreview, SWT.BORDER);
		GridData gd_imagePreviewComposite = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_imagePreviewComposite.minimumWidth = 160;
		gd_imagePreviewComposite.minimumHeight = 320;
		drawnCameraOverlayPreviewComposite.setLayoutData(gd_imagePreviewComposite);	
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public ImageFrozenOverlay getImageFrozenOverlay() 
	{
		return imageFrozenOverlay;
	}

	public void setImageFrozenOverlay(ImageFrozenOverlay imageFrozenOverlay) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.imageFrozenOverlay = imageFrozenOverlay;
		
		if(imageFrozenOverlay != null)
		{
			abstractTextOverlayComposite.setAbstractTextOverlay(imageFrozenOverlay);
			drawnCameraOverlayPreviewComposite.setDrawnCameraOverlay(imageFrozenOverlay);
			m_bindingContext = initDataBindingsCustom();
		}
	}
	
	protected void newExpectedImageUpdatePeriodSelected(double expectedImageUpdatePeriod)
	{		
	}
	
	protected void newFrozenMessageSelected(String frozenMessage)
	{	
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
				
		/* Name Value. */
		IObservableValue<Double> observeNumberFormat = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(imageFrozenOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.IMAGE_FROZEN_OVERLAY__EXPECTED_IMAGE_UPDATE_PERIOD)).observe(imageFrozenOverlay);
		IObservableValue<String> observeNumberFormatText = WidgetProperties.text(SWT.Modify).observe(expectedRefreshPeriodTxt);

		bindingContext.bindValue(observeNumberFormatText,
								observeNumberFormat, 
								new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{				
										double tmp = imageFrozenOverlay.getExpectedImageUpdatePeriod();
										try
										{
											tmp = Double.parseDouble((String) fromObject); 
										}
										catch (Throwable t) 
										{
										}
										
										return tmp;										
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		
		/* Frozen Message Value. */
		IObservableValue<Double> observeFrozenMessage = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(imageFrozenOverlay), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.IMAGE_FROZEN_OVERLAY__FROZEN_MESSAGE)).observe(imageFrozenOverlay);
		IObservableValue<String> observeFrozenMessageText = WidgetProperties.text(SWT.Modify).observe(frozenMessageTxt);

		bindingContext.bindValue(observeFrozenMessageText,
								 observeFrozenMessage, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
	
		return bindingContext;
	}
}
