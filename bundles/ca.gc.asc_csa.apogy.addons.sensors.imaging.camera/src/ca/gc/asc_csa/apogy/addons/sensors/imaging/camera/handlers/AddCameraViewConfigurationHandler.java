package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Display;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationList;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewUtilities;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.parts.CameraViewPart;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;

public class AddCameraViewConfigurationHandler {

	@CanExecute
	public boolean canExecute(MPart part)
	{
		if (part.getObject() instanceof CameraViewPart)
		{
			CameraViewConfigurationList cameraViewConfigurationList = CameraViewUtilities.INSTANCE.getActiveCameraViewConfigurationList();
			return cameraViewConfigurationList != null;
		}
		return false;
	}
	
	@Execute
	public void execute(MPart part, final MToolItem item)
	{
		if (part.getObject() instanceof CameraViewPart)
		{			
			CameraViewConfigurationList cameraViewConfigurationList = CameraViewUtilities.INSTANCE.getActiveCameraViewConfigurationList();
			if(cameraViewConfigurationList != null)
			{
				NamedSetting namedSettings = ApogyCommonEMFUIFactory.eINSTANCE.createNamedSetting();
				namedSettings.setParent(cameraViewConfigurationList);
				namedSettings.setContainingFeature(ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION_LIST__CAMERA_VIEW_CONFIGURATIONS);
				ApogyEObjectWizard wizard = new ApogyEObjectWizard(ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION_LIST__CAMERA_VIEW_CONFIGURATIONS,
												cameraViewConfigurationList, namedSettings, null) 
				{
					@Override
					public boolean performFinish() 
					{
						boolean value = super.performFinish();					
						return value;
					}
				};
				WizardDialog dialog = new WizardDialog(Display.getDefault().getActiveShell(), wizard);
				dialog.open();				
			}
			
		}		
	}
}