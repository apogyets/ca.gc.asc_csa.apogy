package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.handlers;

import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBar;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBarElement;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraRCPConstants;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.CameraViewComposite;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.parts.CameraViewPart;

public class UpdateCameraViewToolbarHandler {

	@Inject
	EModelService modelService;

	@CanExecute
	public boolean canExecute(MApplication app) {
		MPart part = (MPart) modelService.find(ApogyAddonsSensorsImagingCameraRCPConstants.PART__CAMERA_VIEW__ID, app);

		if (part != null && part.getObject() instanceof CameraViewPart) {
			return modelService.find(ApogyAddonsSensorsImagingCameraRCPConstants.TOOL_BAR__CAMERA_VIEW__ID, part) == null;
		}
		return false;
	}

	@Execute
	public void execute(MTrimmedWindow window) {
		MPart part = (MPart) modelService.find(ApogyAddonsSensorsImagingCameraRCPConstants.PART__CAMERA_VIEW__ID, window);

		if (part.getObject() instanceof CameraViewPart) {
			CameraViewPart viewPart = (CameraViewPart) part.getObject();
			if (viewPart.getActualComposite() instanceof CameraViewComposite) {
				CameraViewConfiguration config = ((CameraViewComposite) viewPart.getActualComposite())
						.getSelectedCameraViewConfiguration();

				if (config != null) {
					MToolBar toolBar = part.getToolbar();

					for (MToolBarElement element : toolBar.getChildren()) 
					{
						if (element.getElementId().equals(ApogyAddonsSensorsImagingCameraRCPConstants.HANDLED_TOOL_ITEM__DISPLAY_RECTIFIED_IMAGE__ID)) {
							((MToolItem) element).setSelected(config.isDisplayRectifiedImage());
						}
						if (element.getElementId().equals(ApogyAddonsSensorsImagingCameraRCPConstants.HANDLED_TOOL_ITEM__IMAGE_AUTO_SAVE__ID)) {
							((MToolItem) element).setSelected(config.isImageAutoSaveEnable());
						}
						if (element.getElementId().equals(ApogyAddonsSensorsImagingCameraRCPConstants.HANDLED_TOOL_ITEM__SAVE_IMAGE_WITH_OVERLAYS__ID)) {
							((MToolItem) element).setSelected(config.isSaveImageWithOverlays());
						}
						if (element.getElementId().equals(ApogyAddonsSensorsImagingCameraRCPConstants.HANDLED_TOOL_ITEM__DELETE_CAMERA_VIEW_CONFIGURATION__ID)) {
							((MToolItem) element).setEnabled(config != null);
						}						
					}
				}
			}
		}
	}
}