package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.RescaleFilter;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class RescaleFilterComposite extends Composite 
{
	private RescaleFilter rescaleFilter;
	
	private ImageFilterComposite imageFilterComposite;
	private Text scaleText;
	
	private DataBindingContext m_bindingContext;
	
	public RescaleFilterComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Group grpSettings = new Group(this, SWT.NONE);
		grpSettings.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSettings.setText("Settings");
		grpSettings.setLayout(new GridLayout(2, false));
		
		Label lblContrast = new Label(grpSettings, SWT.NONE);
		lblContrast.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblContrast.setText("Scale :");
		
		scaleText = new Text(grpSettings, SWT.BORDER);
		scaleText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		scaleText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) 
			{				
				try
				{
					newExposureSelected(Double.parseDouble(scaleText.getText()));
				}
				catch (Exception ex) 
				{					
				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{				
				try
				{
					newExposureSelected(Double.parseDouble(scaleText.getText()));
				}
				catch (Exception ex) 
				{					
				}
			}
		});	
		
		Group grpPreview = new Group(this, SWT.NONE);
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpPreview.setText("Preview");
		grpPreview.setLayout(new GridLayout(1, false));
		
		imageFilterComposite = new ImageFilterComposite(grpPreview, SWT.NONE);	
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public RescaleFilter getRescaleFilter() 
	{
		return rescaleFilter;
	}

	public void setRescaleFilter(RescaleFilter rescaleFilter) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.rescaleFilter = rescaleFilter;
		
		if(rescaleFilter != null)
		{
			m_bindingContext = customInitDataBindings();
		}
		
		imageFilterComposite.setImageFilter(rescaleFilter);
	}	
	
	protected void newExposureSelected(double contrast)
	{	
	}
	
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Contrast Value. */
		IObservableValue<Double> observeScale = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(rescaleFilter), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.RESCALE_FILTER__SCALE)).observe(rescaleFilter);
		IObservableValue<String> observeScaleText = WidgetProperties.text(SWT.Modify).observe(scaleText);

		bindingContext.bindValue(observeScaleText,
								 observeScale, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{						
										double tmp  =  rescaleFilter.getScale();
										try
										{
											tmp  = Double.parseDouble((String) fromObject);										
										}
										catch(Throwable t)
										{											
										}	
										
										return tmp;
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
	
		return bindingContext;
	}
}
