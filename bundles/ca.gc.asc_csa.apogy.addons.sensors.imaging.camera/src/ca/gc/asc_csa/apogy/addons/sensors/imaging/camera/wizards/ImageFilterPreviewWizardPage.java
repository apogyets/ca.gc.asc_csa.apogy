package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ImageFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.ImageFilterComposite;

public class ImageFilterPreviewWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.GainFilterWizardPage";
		
	private ImageFilter imageFilter;
	private ImageFilterComposite imageFilterComposite;
		
	private DataBindingContext m_bindingContext;
	
	public ImageFilterPreviewWizardPage(ImageFilter imageFilter) 
	{
		super(WIZARD_PAGE_ID);
		this.imageFilter = imageFilter;
			
		setTitle("Image Filter.");
		setDescription("Filter Effect Preview.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		imageFilterComposite = new ImageFilterComposite(top, SWT.NONE);
		imageFilterComposite.setImageFilter(imageFilter);
		
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		validate();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);				
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}
