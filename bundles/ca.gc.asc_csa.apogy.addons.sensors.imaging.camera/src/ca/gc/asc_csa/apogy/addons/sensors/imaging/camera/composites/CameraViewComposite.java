package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfiguration;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.CameraViewConfigurationList;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;

public class CameraViewComposite extends Composite 
{
	private final String DETAILS_ID = "Details";
	private final String OVERLAYS_ID = "Overlays";
	private final String FILTERS_ID = "Filters";
	private final String TOOLS_ID = "Tools";

	private CameraViewConfiguration cameraViewConfiguration= null;
	
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private CameraViewConfigurationComposite cameraComposite;

	private Composite toolbarMenuComposite;
	private CTabFolder tabFolder;
	private Map<String, CTabItem> tabItemMap;

	private CameraViewConfigurationList configurationList;

	public CameraViewComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(1, false));
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{	
				if (tabFolder != null && !tabFolder.isDisposed()) {
					for (Control control : tabFolder.getChildren()) {
						control.dispose();
					}
				}
			}
		});

		cameraComposite = new CameraViewConfigurationComposite(this, SWT.None);
		cameraComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));		
	}	
	
	public void setCameraViewConfiguration(CameraViewConfiguration cameraViewConfiguration)
	{	
		this.cameraViewConfiguration = cameraViewConfiguration;
		
		// Update view
		cameraComposite.setCameraViewConfiguration(cameraViewConfiguration);
				
		// Update tool bar if applicable
		if (toolbarMenuComposite != null && !toolbarMenuComposite.isDisposed()) {
			if (tabFolder != null && !tabFolder.isDisposed()) {
				for (Control control : tabFolder.getChildren()) {
					control.dispose();
				}
			}
			toolbarMenuComposite.dispose();

			for (Object key : tabItemMap.keySet()) {
				if (tabItemMap.get(key) == tabFolder.getSelection()) {
					updatetoolbarMenuComposite((String) key, true);
				}
			}
		}
		
		// Notify there is a new selection.
		newCameraViewConfigurationSelected(cameraViewConfiguration);
	}
	
	protected void newCameraViewConfigurationSelected(CameraViewConfiguration newCameraViewConfiguration) {

	}

	/**
	 * Updates the toolBarMenu.
	 * 
	 * @param parameterID
	 *            ID of the parameter to display
	 * @param selected
	 *            id true, the selected tabItem will change. Otherwise, if false
	 *            the composite will be disposed.
	 */
	public void updatetoolbarMenuComposite(String parameterID, boolean selected) {
		if (selected) {
			if (toolbarMenuComposite == null || toolbarMenuComposite.isDisposed()) {
				/**
				 * Composite
				 */
				toolbarMenuComposite = new Composite(this, SWT.None);
				toolbarMenuComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
				toolbarMenuComposite.setLayout(new FillLayout(SWT.HORIZONTAL));
				// Limits the height of the toolBarMenu at 1/3 of the total height of the composite.
				toolbarMenuComposite.addListener(SWT.Resize, new Listener() {
					@Override
					public void handleEvent(Event event) {
						if (toolbarMenuComposite
								.getSize().y > (int) (CameraViewComposite.this.getBounds().height / 3)) {
							int diff = toolbarMenuComposite.getSize().y
									- (int) (CameraViewComposite.this.getBounds().height / 3);
							Rectangle bounds = toolbarMenuComposite.getBounds();
							toolbarMenuComposite.setBounds(bounds.x, bounds.y + diff, bounds.width,
									bounds.height - diff);
							cameraComposite.setSize(cameraComposite.getSize().x, cameraComposite.getSize().y + diff);
						}
					}
				});

				/**
				 * Folder
				 */
				tabFolder = new CTabFolder(toolbarMenuComposite, SWT.BORDER);
				tabFolder.setLayout(new FillLayout());
				tabFolder.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						for (Object key : tabItemMap.keySet()) {
							if (tabItemMap.get(key) == ((CTabFolder) e.getSource()).getSelection()) {
								newTabFolderSelection((String) key);
							}
						}
					}
				});

				/**
				 * Details
				 */
				CTabItem tbtmDetails = new CTabItem(tabFolder, SWT.NONE);
				tbtmDetails.setText("Details");
				ScrolledComposite emfFormsScrolledComposite = new ScrolledComposite(tabFolder,
						SWT.H_SCROLL | SWT.V_SCROLL);
				emfFormsScrolledComposite.setExpandHorizontal(true);
				emfFormsScrolledComposite.setExpandVertical(true);
				Composite emfFormsDetails = new Composite(emfFormsScrolledComposite, SWT.None);
				emfFormsDetails.setBackground(getDisplay().getSystemColor(SWT.COLOR_WHITE));
				ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(emfFormsDetails,
						getSelectedCameraViewConfiguration());
				emfFormsScrolledComposite.setContent(emfFormsDetails);
				emfFormsScrolledComposite.setMinSize(emfFormsDetails.computeSize(SWT.DEFAULT, SWT.DEFAULT));
				tbtmDetails.setControl(emfFormsScrolledComposite);

				/**
				 * Overlays
				 */
				CTabItem tbtmOverlays = new CTabItem(tabFolder, SWT.NONE);
				tbtmOverlays.setText("Overlays");
				CameraImageAnnotationListComposite overlayComposite = new CameraImageAnnotationListComposite(tabFolder,
						SWT.NONE);
				overlayComposite.setCameraViewConfiguration(getSelectedCameraViewConfiguration(),
						ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION__OVERLAY_LIST);
				tbtmOverlays.setControl(overlayComposite);

				/**
				 * Filters
				 */
				CTabItem tbtmFilters = new CTabItem(tabFolder, SWT.NONE);
				tbtmFilters.setText("Filters");
				FiltersListComposite filtersComposite = new FiltersListComposite(tabFolder, SWT.NONE);
				filtersComposite.setCameraViewConfiguration(getSelectedCameraViewConfiguration());
				tbtmFilters.setControl(filtersComposite);

				/**
				 * Tools
				 */
				CTabItem tbtmTools = new CTabItem(tabFolder, SWT.NONE);
				tbtmTools.setText("Tools");
				CameraImageAnnotationListComposite toolsComposite = new CameraImageAnnotationListComposite(tabFolder,
						SWT.NONE);
				toolsComposite.setCameraViewConfiguration(getSelectedCameraViewConfiguration(),
						ApogyAddonsSensorsImagingCameraPackage.Literals.CAMERA_VIEW_CONFIGURATION__TOOL_LIST);
				tbtmTools.setControl(toolsComposite);

				tabItemMap = new HashMap<String, CTabItem>();
				tabItemMap.put(DETAILS_ID, tbtmDetails);
				tabItemMap.put(FILTERS_ID, tbtmFilters);
				tabItemMap.put(OVERLAYS_ID, tbtmOverlays);
				tabItemMap.put(TOOLS_ID, tbtmTools);

				layout();
			}
			tabFolder.setSelection(tabItemMap.get(parameterID));
		}
		/** Otherwise close the menu toolBar */
		else {
			/**
			 * Dispose
			 */
			if (!toolbarMenuComposite.isDisposed()) {
				for (Control control : tabFolder.getChildren()) {
					control.dispose();
				}
				toolbarMenuComposite.dispose();
			}

			layout();
		}
	}

	protected void newTabFolderSelection(String parameterID) 
	{
	}

	public CameraViewConfiguration getSelectedCameraViewConfiguration() 
	{
		return this.cameraViewConfiguration;
	}

	public void setCameraViewConfigurationList(CameraViewConfigurationList cameraViewConfigurationList) {
		this.configurationList = cameraViewConfigurationList;
	}


	protected IContentProvider getContentProvider() {
		return new AdapterFactoryContentProvider(adapterFactory) {
			@Override
			public Object[] getElements(Object object) {
				return configurationList.getCameraViewConfigurations().toArray();
			}

			@Override
			public Object[] getChildren(Object object) {
				return null;
			}
		};
	}

	protected IBaseLabelProvider getLabelProvider() {
		return new AdapterFactoryLabelProvider(adapterFactory) {
			@Override
			public String getText(Object object) {
				if (object instanceof CameraViewConfiguration) {
					return ((CameraViewConfiguration) object).getName();
				}
				return "";
			}

			@Override
			public Image getImage(Object object) {
				return null;
			}
		};
	}
}
