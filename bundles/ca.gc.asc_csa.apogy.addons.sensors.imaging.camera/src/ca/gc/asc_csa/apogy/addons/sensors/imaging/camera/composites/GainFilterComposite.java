package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.ApogyAddonsSensorsImagingCameraPackage;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.GainFilter;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class GainFilterComposite extends Composite 
{
	private GainFilter gainFilter;
	
	private ImageFilterComposite imageFilterComposite;
	private Text gainText;
	private Text biasText;
	
	private DataBindingContext m_bindingContext;
	
	public GainFilterComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		Group grpSettings = new Group(this, SWT.NONE);
		grpSettings.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		grpSettings.setText("Settings");
		grpSettings.setLayout(new GridLayout(4, false));
		
		Label lblContrast = new Label(grpSettings, SWT.NONE);
		lblContrast.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblContrast.setText("Gain:");
		
		gainText = new Text(grpSettings, SWT.BORDER);
		gainText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		gainText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				try
				{
					newGainSelected(Double.parseDouble(gainText.getText()));
				}
				catch (Exception ex) 
				{					
				}
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				try
				{
					newGainSelected(Double.parseDouble(gainText.getText()));
				}
				catch (Exception ex) 
				{					
				}
				
			}
		});
		
		Label lblBrightness = new Label(grpSettings, SWT.NONE);
		lblBrightness.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblBrightness.setText("Bias:");
		
		biasText = new Text(grpSettings, SWT.NONE);
		biasText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		biasText.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				try
				{
					newBiasSelected(Double.parseDouble(biasText.getText()));
				}
				catch (Exception ex) 
				{					
				}				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				try
				{
					newBiasSelected(Double.parseDouble(biasText.getText()));
				}
				catch (Exception ex) 
				{					
				}				
			}
		});
		
		Group grpPreview = new Group(this, SWT.NONE);
		grpPreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpPreview.setText("Preview");
		grpPreview.setLayout(new GridLayout(1, false));
		
		imageFilterComposite = new ImageFilterComposite(grpPreview, SWT.NONE);	
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public GainFilter getGainFilter() 
	{
		return gainFilter;
	}

	public void setGainFilter(GainFilter gainFilter) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.gainFilter = gainFilter;
		
		if(gainFilter != null)
		{
			m_bindingContext = customInitDataBindings();
		}
		
		imageFilterComposite.setImageFilter(gainFilter);
	}	
	
	protected void newGainSelected(double contrast)
	{	
	}
	
	protected void newBiasSelected(double brightness)
	{		
	}
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Contrast Value. */
		IObservableValue<Double> observeGain = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(gainFilter), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.GAIN_FILTER__GAIN)).observe(gainFilter);
		IObservableValue<String> observeGainText = WidgetProperties.text(SWT.Modify).observe(gainText);

		bindingContext.bindValue(observeGainText,
								 observeGain, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{				
										double tmp = gainFilter.getGain();
										try
										{
											tmp = Double.parseDouble((String) fromObject); 
										}
										catch (Throwable t) 
										{
										}
										
										return tmp;										
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		/* Brightness Value. */
		IObservableValue<Double> observeBias = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(gainFilter), 
																	  FeaturePath.fromList(ApogyAddonsSensorsImagingCameraPackage.Literals.GAIN_FILTER__BIAS)).observe(gainFilter);
		IObservableValue<String> observeBiasText = WidgetProperties.text(SWT.Modify).observe(biasText);

		bindingContext.bindValue(observeBiasText,
								 observeBias, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{							
										double tmp = gainFilter.getBias();
										try
										{
											tmp = Double.parseDouble((String) fromObject); 
										}
										catch (Throwable t) 
										{
										}
										
										return tmp;												
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		return bindingContext;
	}
}
