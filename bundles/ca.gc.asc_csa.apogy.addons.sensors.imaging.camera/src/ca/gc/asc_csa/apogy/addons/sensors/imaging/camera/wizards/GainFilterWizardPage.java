package ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.GainFilter;
import ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.composites.GainFilterComposite;

public class GainFilterWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.imaging.camera.wizards.GainFilterWizardPage";
		
	private GainFilter gainFilter;
	private GainFilterComposite gainFilterComposite;
		
	private DataBindingContext m_bindingContext;
	
	public GainFilterWizardPage(GainFilter gainFilter) 
	{
		super(WIZARD_PAGE_ID);
		this.gainFilter = gainFilter;
			
		setTitle("Gain Filter.");
		setDescription("Select filter settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));		
		
		gainFilterComposite = new GainFilterComposite(top, SWT.NONE)
		{
			@Override
			protected void newGainSelected(double contrast)
			{
				validate();
			}
			@Override
			protected void newBiasSelected(double brightness)
			{
				validate();
			}
		};
		gainFilterComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		gainFilterComposite.setGainFilter(gainFilter);
				
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);		
						
		if(gainFilter.getGain() < 0)
		{
			setErrorMessage("Invalid gain specified ! Must be equal or greater than zero.");
		}
		
		if(gainFilter.getBias() < 0)
		{
			setErrorMessage("Invalid bias specified ! Must be equal or greater than zero.");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
	
		return bindingContext;
	}
}
