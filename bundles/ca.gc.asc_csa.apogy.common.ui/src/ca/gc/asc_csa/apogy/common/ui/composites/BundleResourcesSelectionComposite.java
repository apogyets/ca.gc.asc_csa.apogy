package ca.gc.asc_csa.apogy.common.ui.composites;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;
import org.osgi.framework.Bundle;

import ca.gc.asc_csa.apogy.common.ui.provider.ApogyCommonUiEditPlugin;

/**
 * Composite providing a tree navigation of resources in installed plugins.
 * @author pallard
 *
 */
public class BundleResourcesSelectionComposite extends Composite 
{
	private Tree tree;
	private TreeViewer treeViewer;
	private Label lblPluginvalue;
	private Label lblPathvalue;
	
	private String[] fileExtensions;
	
	private String selectedPluginSymbolicName;
	private String selectedPath;
	
	public BundleResourcesSelectionComposite(Composite parent, int style)
	{
		this(parent, style, null);
	}
	
	/**
	 * @param parent The parent Composite.
	 * @param style The style.
	 * @param fileExtensions An array containing the file extensions to use as filters. For example new String[]{"*.txt","*.gif"}
	 */
	public BundleResourcesSelectionComposite(Composite parent, int style, String[] fileExtensions) 
	{
		super(parent, style);
		this.fileExtensions = fileExtensions;
		
		setLayout(new GridLayout(2, false));
				
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gd_tree.widthHint = 300;
		gd_tree.minimumWidth = 300;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		
		Label lblPlugin = new Label(this, SWT.NONE);
		lblPlugin.setText("Plugin:");
		
		lblPluginvalue = new Label(this, SWT.BORDER);
		GridData gd_lblPluginvalue = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_lblPluginvalue.minimumWidth = 100;
		gd_lblPluginvalue.widthHint = 100;
		lblPluginvalue.setLayoutData(gd_lblPluginvalue);
		
		Label lblPath = new Label(this, SWT.NONE);
		lblPath.setText("Path:");
		
		lblPathvalue = new Label(this, SWT.BORDER);
		GridData gd_lblPathvalue = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_lblPathvalue.minimumWidth = 100;
		gd_lblPathvalue.widthHint = 100;
		lblPathvalue.setLayoutData(gd_lblPathvalue);
		
		treeViewer.setContentProvider(getContentProvider());
		treeViewer.setLabelProvider(getLabelProvider());
		
		
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{		
				AbstractElement abstractElement = (AbstractElement) ((IStructuredSelection)event.getSelection()).getFirstElement();
				
				if(abstractElement != null)
				{
					AbstractElement root = abstractElement.getRoot();
					if(root instanceof PluginElement)
					{
						selectedPluginSymbolicName = root.name;
						lblPluginvalue.setText(selectedPluginSymbolicName);					
					}
					
					if(abstractElement instanceof FileElement)
					{
						selectedPath = ((FileElement) abstractElement).getPath();
						lblPathvalue.setText(selectedPath);
					}
					
					newSelection();		
				}
			}
		});
		
		treeViewer.setInput(getElements(fileExtensions));
	}
	
	protected LabelProvider getLabelProvider(){
		return (LabelProvider) new ResourceLabelProvider();
	}
	
	protected ITreeContentProvider getContentProvider(){
		return (ITreeContentProvider) new ResourceContentProvider();
	}
	
	public String[] getFileExtensions() {
		return fileExtensions;
	}

	public void setFileExtensions(String[] fileExtensions) 
	{
		this.fileExtensions = fileExtensions;
		
		// Update viewer.
		treeViewer.setInput(getElements(fileExtensions));
	}	
	
	public String getSelectedPluginSymbolicName() {
		return selectedPluginSymbolicName;
	}

	public String getSelectedPath() {
		return selectedPath;
	}

	/*
	 * Method called when the user make a selection. Can be overloaded. 
	 */
	protected void newSelection()
	{		
	}

	private class ResourceContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@SuppressWarnings("rawtypes")
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof Collection)
			{								
				return ((Collection) inputElement).toArray();
			}			
			else if(inputElement instanceof AbstractElement)
			{
				return ((AbstractElement) inputElement).children.toArray();		
			}
					
			return null;
		}

		@SuppressWarnings("rawtypes")
		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof Collection)
			{								
				return ((Collection) parentElement).toArray();
			}			
			else if(parentElement instanceof AbstractElement)
			{
				return ((AbstractElement) parentElement).children.toArray();		
			}
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@SuppressWarnings("rawtypes")
		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof Collection)
			{
				return !((Collection) element).isEmpty();
			}		
			else if(element instanceof AbstractElement)
			{
				return !((AbstractElement) element).children.isEmpty();
			}
			else
			{
				return false;
			}
		}			
	}

	public class ResourceLabelProvider extends LabelProvider
	{
		ImageDescriptor pluginImageDesc = getImageDescriptor("/icons/plugin.gif");
		ImageDescriptor folderImageDesc = getImageDescriptor("/icons/folder.gif");
		ImageDescriptor fileImageDesc = getImageDescriptor("/icons/file.gif");
		
		private Image pluginImage = pluginImageDesc.createImage();
		private Image folderImage = folderImageDesc.createImage();
		private Image fileImage = fileImageDesc.createImage();
		
		@Override
		public Image getImage(Object element) 
		{
			if(element instanceof PluginElement)
			{
				return pluginImage;
			}
			else if(element instanceof FolderElement)
			{
				return folderImage;
			}
			else if(element instanceof FileElement)
			{
				return fileImage;
			}
			else
			{
				return null;
			}
		}

		@Override
		public String getText(Object element) 
		{
			String str = null;
			if(element instanceof AbstractElement)
			{
				str = ((AbstractElement) element).name;
			}
			
			return str;
		}		
		
		@Override
		public void dispose() {
	
			pluginImage.dispose();
			folderImage.dispose();
			fileImage.dispose();
			
			super.dispose();
		}
		
		/**
		 * Returns an image descriptor for the image file at the given
		 * plug-in relative path
		 *
		 * @param path the path
		 * @return the image descriptor
		 */
		private ImageDescriptor getImageDescriptor(String path) 
		{		
			Bundle bundle = ApogyCommonUiEditPlugin.getPlugin().getBundle();
			URL url = FileLocator.find(bundle, new Path(path), null);				
	        return ImageDescriptor.createFromURL(url);
		}
	}
	
	protected List<PluginElement> getElements(String[] fileExtensions)
	{
		List<PluginElement> elements = new ArrayList<PluginElement>();
		Bundle[] bundles = ApogyCommonUiEditPlugin.getPlugin().getBundle().getBundleContext().getBundles();
		
		for(Bundle bundle : bundles)
		{
			PluginElement pluginElement = new PluginElement();
			pluginElement.name = bundle.getSymbolicName();
			
			for(String fileExtension : fileExtensions)
			{
				Enumeration<URL> urls = bundle.findEntries("/", fileExtension, true);
				
				if(urls != null)
				{
					while(urls.hasMoreElements())
					{
						URL url = urls.nextElement();												
						String[] folders = url.getPath().split(File.separator);						
						
						AbstractElement parent = pluginElement;
						for(int i = 0; i < folders.length; i++)
						{
							String folder = folders[i];
							if(folder.length() > 0)
							{
								if(i+1 >= folders.length)
								{
									FileElement file = new FileElement();
									file.name = folder;
									parent.children.add(file);
									file.parent = parent;																		
								}
								else
								{
									// Checks if folder exist.
									AbstractElement child = parent.getChildByName(folder);
									if(child == null)
									{
										child = new FolderElement();
										child.name = folder;
										
										parent.children.add(child);
										child.parent = parent;
									}								
									
									parent = child;
								}
							}
						}							
					}
				}
			}
			
			if(pluginElement.children.size() > 0)
			{
				elements.add(pluginElement);
			}
		}
		
		return elements;
	}
	
	protected class AbstractElement
	{
		public String name;
		public AbstractElement parent;
		public List<AbstractElement> children = new ArrayList<AbstractElement>();
		
		public AbstractElement getChildByName(String name)
		{
			AbstractElement result = null;
			Iterator<AbstractElement> it = children.iterator();
			while(it.hasNext() && result==null)
			{
				AbstractElement child = it.next();
				if(child.name.compareTo(name) == 0)
				{
					result = child;
				}
			}
			return result;
		}
		
		public AbstractElement getRoot()
		{
			AbstractElement currentParent = this;
			while(currentParent.parent != null)
			{				
				currentParent = currentParent.parent;
			}
			
			return currentParent;
		}		
	}
	
	protected class PluginElement extends AbstractElement
	{		
	}
	
	protected class FolderElement extends AbstractElement
	{		
	}
	
	protected class FileElement extends AbstractElement
	{		
		public String getPath()
		{
			String path = File.separator + name;
			AbstractElement currentParent = this;
			while(!(currentParent.parent instanceof PluginElement))
			{
				path = File.separator  + currentParent.parent.name + path;
				currentParent = currentParent.parent;
			}
						
			return path;
		}
	}
}
