package ca.gc.asc_csa.apogy.common.ui.expressions;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.ui.views.properties.PropertySheet;

import ca.gc.asc_csa.apogy.common.ui.EclipseUiUtilities;

public class TabbedPropertySheetPropertyTester extends PropertyTester {

	public static final String PROPERTY_NAMESPACE = "ca.gc.asc_csa.apogy.common.ui.expressions.TabbedPropertySheetPropertyTester";
	public static final String PROPERTY_ACTIVE_SECTION = "activeSection";
	
	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		
		boolean result = false;		
		if (PROPERTY_ACTIVE_SECTION.equals(property)){
			PropertySheet propertySheet = (PropertySheet) EclipseUiUtilities.findView(EclipseUiUtilities.PROPERTY_SHEET_ID);
			if (propertySheet != null){
				result = EclipseUiUtilities.getActiveTabbedPropertySheetSection(propertySheet, (String) expectedValue) != null;
			}
		}	
		return result;
	}
}