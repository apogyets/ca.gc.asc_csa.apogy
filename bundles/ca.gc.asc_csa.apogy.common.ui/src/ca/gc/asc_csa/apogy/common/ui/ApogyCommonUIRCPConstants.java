package ca.gc.asc_csa.apogy.common.ui;

public class ApogyCommonUIRCPConstants {

	/**
	 * Open toolBar menu
	 */
	public static final String COMMAND__OPEN_TOOL_BAR_MENU__ID = "ca.gc.asc_csa.apogy.common.ui.command.openToolBarMenu";
	public static final String COMMAND_PARAMETER__OPEN_TOOL_BAR_MENU_ID__ID = "ca.gc.asc_csa.apogy.common.ui.opentoolbarmenu.commandparameter.ID";

	
}
