package ca.gc.asc_csa.apogy.common.ui.composites;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

public class NoContentComposite extends Composite {
	/**
	 * Create the parentComposite.
	 * @param parent
	 * @param style
	 */
	public NoContentComposite(Composite parent, int style) {
		super(parent, style);
	
		createContent(parent);
	}

	protected String getMessage() {
		return "No displayable content";
	}
	
	protected void createContent(Composite parent){
		GridLayout gridLayout_arguments = new GridLayout(1, false);
		gridLayout_arguments.marginWidth = 0;
		gridLayout_arguments.marginHeight = 0;
		setLayout(gridLayout_arguments);
		
		Label lblNoActiveSession = new Label(this, SWT.NONE);
		lblNoActiveSession.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		lblNoActiveSession.setText(getMessage());
	}
}