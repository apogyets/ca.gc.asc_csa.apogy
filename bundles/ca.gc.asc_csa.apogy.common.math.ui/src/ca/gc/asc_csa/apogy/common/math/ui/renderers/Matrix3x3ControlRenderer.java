package ca.gc.asc_csa.apogy.common.math.ui.renderers;

import javax.inject.Inject;

import org.eclipse.core.databinding.Binding;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.core.swt.SimpleControlSWTControlSWTRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.math.Activator;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFactory;
import ca.gc.asc_csa.apogy.common.math.Matrix3x3;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Matrix3x3Composite;
import ca.gc.asc_csa.apogy.common.math.ui.composites.RotationMatrixComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class Matrix3x3ControlRenderer extends SimpleControlSWTControlSWTRenderer {

	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	@Inject
	public Matrix3x3ControlRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService,
			EMFFormsDatabinding emfFormsDatabinding, EMFFormsLabelProvider emfFormsLabelProvider,
			VTViewTemplateProvider vtViewTemplateProvider) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}

	@Override
	protected Binding[] createBindings(Control control) throws DatabindingFailedException {
		return new Binding[0];
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Control createSWTControl(Composite parent) throws DatabindingFailedException {
		/** Get the matrix*/
		Matrix3x3 matrix = (Matrix3x3) getValue();
		/** Initialize the matrix if needed */
		if(matrix == null){
			matrix = ApogyCommonMathFactory.eINSTANCE.createMatrix3x3();
			
			getModelValue().setValue(matrix);
		}

		Composite composite = new Composite(parent, SWT.BORDER);
		composite.setLayout(GridLayoutFactory.fillDefaults().create());

		/** Rotation matrix */
		RotationMatrixComposite rotationComposite = new RotationMatrixComposite(composite, SWT.None,
				ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(matrix));
		rotationComposite.setMatrix3x3(matrix);
		rotationComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		/** Complete matrix */
		Section section = toolkit.createSection(composite, Section.TITLE_BAR | Section.TWISTIE);
		toolkit.adapt(section);
		section.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		section.setExpanded(false);
		section.setText("Complete");

		Matrix3x3Composite matrixComposite = new Matrix3x3Composite(section, SWT.None,
				ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(matrix));
		matrixComposite.setMatrix3x3(matrix);
		matrixComposite.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		section.setClient(matrixComposite);
		
		return composite;
	}

	@Override
	protected String getUnsetText() {
		return "Unset";
	}

	protected Object getValue() {
		try {
			Object obj = getModelValue().getValue();
			return obj;
		} catch (DatabindingFailedException e) {
			Logger.INSTANCE.log(Activator.PLUGIN_ID, " error setting the RGB value. ", EventSeverity.ERROR);
		}
		return null;
	}
	
	@Override
	protected void dispose() {
		toolkit.dispose();
		super.dispose();
	}
}
