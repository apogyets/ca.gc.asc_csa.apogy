package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.parts;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractPart;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSky;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.EarthSkyComposite;

public class EarthSkyPart extends AbstractPart
{
	private Adapter adapter;
	private EarthSkyComposite earthSkyComposite = null;

	public EarthSkyPart() 
	{
		// Register to changes in the active Earth Surface Worksite.
		ApogyEarthSurfaceEnvironmentFacade.INSTANCE.eAdapters().add(getApogyEarthSurfaceEnvironmentFacadeAdapter());
	}
	
	@Override
	protected EObject getInitializeObject() 
	{			
		EarthSky earthSky = null;
		
		EarthSurfaceWorksite earthSurfaceWorksite = ApogyEarthSurfaceEnvironmentFacade.INSTANCE.getActiveEarthSurfaceWorksite();
		if(earthSurfaceWorksite != null)
		{
			earthSky = earthSurfaceWorksite.getEarthSky();
		}
		
		return earthSky;
	}
	
	@Override
	protected void setCompositeContent(EObject eObject) 
	{	
		if(eObject instanceof EarthSky)
		{			
			earthSkyComposite.setEarthSky((EarthSky) eObject);
		}
		else
		{
			setEObject(null);
		}
	}

	@Override
	protected void createNoContentComposite(Composite parent, int style) 
	{		
		new NoContentComposite(parent, SWT.None){
			@Override
			protected String getMessage() {
				return "No active Earth Sky.";
			}		
		};	
	}	

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		earthSkyComposite = new EarthSkyComposite(parent, style);			
	}

	@Override
	public void dispose() 
	{
		// Un-Register from changes in the active Earth Surface Worksite.
		ApogyEarthSurfaceEnvironmentFacade.INSTANCE.eAdapters().remove(getApogyEarthSurfaceEnvironmentFacadeAdapter());	
		super.dispose();
	}
	
	private Adapter getApogyEarthSurfaceEnvironmentFacadeAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{			
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ApogyEarthSurfaceEnvironmentFacade)
					{
						int featureID = msg.getFeatureID(ApogyEarthSurfaceEnvironmentFacade.class);
						switch (featureID) 
						{
							case ApogyEarthSurfaceEnvironmentPackage.APOGY_EARTH_SURFACE_ENVIRONMENT_FACADE__ACTIVE_EARTH_SURFACE_WORKSITE:
								if(msg.getNewValue() instanceof EarthSurfaceWorksite)
								{
									EarthSurfaceWorksite earthSurfaceWorksite = (EarthSurfaceWorksite) msg.getNewValue();
									setEObject(earthSurfaceWorksite.getEarthSky());
								}
								else
								{
									setEObject(null);
								}
								
							break;

						default:
							break;
						}
					}
				}	
			};
		}
		
		return adapter;
		
	}
}
