package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;

public class EarthSurfaceWorksiteDetailsComposite extends Composite 
{
	private EarthSurfaceWorksite earthSurfaceWorksite;
			
	private Composite compositeDetails;
	
	public EarthSurfaceWorksiteDetailsComposite(Composite parent, int style) 
	{
		super(parent, SWT.NONE);
		setLayout(new GridLayout(1,false));
				
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.widthHint = 318;
		gd_compositeDetails.minimumWidth = 318;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public EarthSurfaceWorksite getEarthSurfaceWorksite() {
		return earthSurfaceWorksite;
	}

	public void setEarthSurfaceWorksite(EarthSurfaceWorksite earthSurfaceWorksite) 
	{
		this.earthSurfaceWorksite = earthSurfaceWorksite;
			
		// Update Details EMFForm.
		if(earthSurfaceWorksite != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, earthSurfaceWorksite);
		}
	}		
}
