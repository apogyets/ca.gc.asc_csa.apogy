package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.parts;

import java.util.HashMap;

import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.AbstractEObjectSelectionPart;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.ApogyCoreEnvironmentSurfaceEarthUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.OtherLayersComposite;

public class OtherLayersPart extends AbstractEObjectSelectionPart 
{
	@Inject
	protected EPartService ePartService;

	private OtherLayersComposite otherLayersComposite;

	@Override
	protected EObject getInitializeObject() 
	{		
		if(super.getInitializeObject() == null)
		{
			eObject = getInitialMap();
		}
				
		return eObject;
	}
	
	@Override
	protected void setCompositeContents(EObject eObject) 
	{
		if(eObject instanceof Map)
		{
			Map map = (Map) eObject;
			otherLayersComposite.setMap(map);
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		
		otherLayersComposite = new OtherLayersComposite(parent, SWT.BORDER);
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() 
	{	
		HashMap<String, ISelectionListener> map = new HashMap<>();

		map.put(ApogyCoreEnvironmentSurfaceEarthUIRCPConstants.PART__MAPS__ID, new ISelectionListener() {
			@Override
			public void selectionChanged(MPart part, Object selection) 
			{
				if (selection instanceof Map) 
				{
					Map map = (Map) selection;
					setEObject(map);
				}
			}
		});

		return map;
	}

	private Map getInitialMap()
	{
		Map map = null;
		MPart part = ePartService.findPart(ApogyCoreEnvironmentSurfaceEarthUIRCPConstants.PART__MAPS__ID);
		if(part != null && part.getObject() instanceof MapsPart)
		{
			MapsPart mapsPart = (MapsPart) part.getObject();
			map = mapsPart.getSelectedMap();
		}
		return map;
	}
}
