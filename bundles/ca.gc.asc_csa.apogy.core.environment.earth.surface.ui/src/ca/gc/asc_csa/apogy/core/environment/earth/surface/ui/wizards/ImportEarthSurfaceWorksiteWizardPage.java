package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import ca.gc.asc_csa.apogy.core.environment.WorksitesList;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.EarthSurfaceWorksiteDetailsComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.EarthSurfaceWorksiteImportFromFileComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.EarthSurfaceWorksiteOriginComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites.EarthSurfaceWorksitesRegistryComposite;

public class ImportEarthSurfaceWorksiteWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.wizards.ImportEarthSurfaceWorksiteWizardPage";
		
	private TabFolder tabFolder;
	private TabItem tabItemFromRegistry;
	private TabItem tabItemFromFile;
	
	private EarthSurfaceWorksite selectedEarthSurfaceWorksite;
	private EarthSurfaceWorksitesRegistryComposite earthSurfaceWorksitesRegistryComposite;
	private EarthSurfaceWorksiteImportFromFileComposite earthSurfaceWorksiteImportFromFileComposite;	
	
	private EarthSurfaceWorksiteDetailsComposite earthSurfaceWorksiteDetailsComposite;
	private EarthSurfaceWorksiteOriginComposite earthSurfaceWorksiteOriginComposite;
	
	public ImportEarthSurfaceWorksiteWizardPage(WorksitesList worksitesList) 
	{
		super(WIZARD_PAGE_ID);
		
		
		setTitle("Import Earth Surface Worksite");
		setDescription("Select the Earth Surface Worksite to import.");
	}
	
	private void setSelectedEarthSurfaceWorksite(EarthSurfaceWorksite selectedEarthSurfaceWorksite)
	{
		this.selectedEarthSurfaceWorksite = selectedEarthSurfaceWorksite;
		
		earthSurfaceWorksiteDetailsComposite.setEarthSurfaceWorksite(selectedEarthSurfaceWorksite);
		earthSurfaceWorksiteOriginComposite.setEarthSurfaceWorksite(selectedEarthSurfaceWorksite);
		
		newSelectedEarthSurfaceWorksite(selectedEarthSurfaceWorksite);		
	}
	
	public EarthSurfaceWorksite getSelectedEarthSurfaceWorksite()
	{
		return selectedEarthSurfaceWorksite;
	}	
	
	protected void newSelectedEarthSurfaceWorksite(EarthSurfaceWorksite selectedEarthSurfaceWorksite) 
	{				
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, false));							
				
		// Tab on left end side
		tabFolder = new TabFolder(container, SWT.BORDER);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		tabFolder.addSelectionListener(new SelectionAdapter() 
		{
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent event) 
			{
				TabItem tabItem =  tabFolder.getSelection()[0];
				
				if(tabItem == tabItemFromRegistry)
				{
					setSelectedEarthSurfaceWorksite(earthSurfaceWorksitesRegistryComposite.getSelectedEarthSurfaceWorksite());
				}
				else if(tabItem == tabItemFromFile)
				{
					setSelectedEarthSurfaceWorksite(earthSurfaceWorksiteImportFromFileComposite.getSelectedEarthSurfaceWorksite());
				}
			}
		});
		tabFolder.pack();
		
		tabItemFromRegistry = new TabItem(tabFolder, SWT.NONE);			
		populateTabItemFromRegistry(tabFolder, tabItemFromRegistry);
			
		tabItemFromFile = new TabItem(tabFolder, SWT.NONE);			
		populateTabItemFromFile(tabFolder, tabItemFromFile);
							
		// Selected Worksite Overview on the right.
		Group overviewGroup = new Group(container, SWT.BORDER);
		overviewGroup.setText("Selected Worksite");
		overviewGroup.setLayout(new GridLayout(1,false));
		GridData gd_overviewGroup = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		overviewGroup.setLayoutData(gd_overviewGroup);
		
		Group worksiteOriginGroup = new Group(overviewGroup, SWT.BORDER);
		worksiteOriginGroup.setText("Worksite Origin");
		worksiteOriginGroup.setLayout(new GridLayout(1,false));
		GridData gd_worksiteOriginGroup = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2);
		worksiteOriginGroup.setLayoutData(gd_worksiteOriginGroup);

		earthSurfaceWorksiteOriginComposite = new EarthSurfaceWorksiteOriginComposite(worksiteOriginGroup, SWT.NONE);
		GridData gd_earthSurfaceWorksiteOriginComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		earthSurfaceWorksiteOriginComposite.setLayoutData(gd_earthSurfaceWorksiteOriginComposite);

		Group worksiteDetailsGroup = new Group(overviewGroup, SWT.BORDER);
		worksiteDetailsGroup.setText("Worksite Details");
		worksiteDetailsGroup.setLayout(new GridLayout(1,false));
		GridData gd_worksiteDetailsGroup = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2);
		worksiteDetailsGroup.setLayoutData(gd_worksiteDetailsGroup);
		
		earthSurfaceWorksiteDetailsComposite = new EarthSurfaceWorksiteDetailsComposite(worksiteDetailsGroup, SWT.NONE);
		GridData gd_earthSurfaceWorksiteDetailsComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		earthSurfaceWorksiteDetailsComposite.setLayoutData(gd_earthSurfaceWorksiteDetailsComposite);
							
		setControl(container);	
		
		setPageComplete(false);
	}		
	
	private void populateTabItemFromRegistry(final TabFolder tabFolder, final TabItem tabItem)
	{
		tabItem.setText("From Registry");
		
		Composite top = new Composite(tabFolder, SWT.NONE);
		top.setLayout(new GridLayout(1, false));
		
		earthSurfaceWorksitesRegistryComposite = new EarthSurfaceWorksitesRegistryComposite(top, SWT.NONE)
		{
			@Override
			protected void newEarthSurfaceWorksiteSelected(EarthSurfaceWorksite earthSurfaceWorksite) 
			{
				selectedEarthSurfaceWorksite = earthSurfaceWorksite;
				setPageComplete(selectedEarthSurfaceWorksite != null);
												
				setSelectedEarthSurfaceWorksite(selectedEarthSurfaceWorksite);
			}
		};
		
		GridData gd_earthSurfaceWorksitesRegistryComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		earthSurfaceWorksitesRegistryComposite.setLayoutData(gd_earthSurfaceWorksitesRegistryComposite);
		
		tabItem.setControl(top);
	}
	
	private void populateTabItemFromFile(final TabFolder tabFolder, final TabItem tabItem)
	{
		tabItem.setText("From File");
		
		Composite top = new Composite(tabFolder, SWT.NONE);
		top.setLayout(new GridLayout(1, false));
		
		earthSurfaceWorksiteImportFromFileComposite = new EarthSurfaceWorksiteImportFromFileComposite(top, SWT.NONE)
		{
			@Override
			protected void newEarthSurfaceWorksiteSelected(EarthSurfaceWorksite earthSurfaceWorksite) 
			{
				selectedEarthSurfaceWorksite = earthSurfaceWorksite;
				setPageComplete(selectedEarthSurfaceWorksite != null);
				
				setSelectedEarthSurfaceWorksite(selectedEarthSurfaceWorksite);
			}
		};
		GridData gd_earthSurfaceWorksiteImportFromFileComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		earthSurfaceWorksiteImportFromFileComposite.setLayoutData(gd_earthSurfaceWorksiteImportFromFileComposite);
						
		
		tabItem.setControl(top);
	}
}
