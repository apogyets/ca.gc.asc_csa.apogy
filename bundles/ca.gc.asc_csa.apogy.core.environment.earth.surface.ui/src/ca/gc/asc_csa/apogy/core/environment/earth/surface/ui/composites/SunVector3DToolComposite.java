package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.dialogs.NodeSelectionDialog;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.Color3fComposite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.ApogyCoreEnvironmentSurfaceEarthUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.SunVector3DTool;
import ca.gc.asc_csa.apogy.core.topology.ApogyCoreTopologyFacade;

public class SunVector3DToolComposite extends Composite 
{
	private SunVector3DTool sunVector3DTool;
	
	private Text txtVectorLength;
	private Color3fComposite vectorColor;
	private Color3fComposite sunIntensityColor;
	private Text txtEndPointRadius;
	
		
	private Text txtToNodeID;
	private Button btnSelectToNode;
	private Tuple3dComposite toRelativePositionComposite;
	private Button btnToLocked;
	
	private DataBindingContext m_bindingContext;
	
	public SunVector3DToolComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, true));
		
		Group grpSettings = new Group(this, SWT.NONE);
		grpSettings.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpSettings.setText("Settings");
		grpSettings.setLayout(new GridLayout(2, false));
		
		// Vector Color
		Label lblVectorColor  = new Label(grpSettings, SWT.NONE);
		lblVectorColor.setText("Vector Color:");
		
		vectorColor = new Color3fComposite(grpSettings, SWT.NONE, "", true, null, ApogyCoreEnvironmentSurfaceEarthUIPackage.Literals.SUN_VECTOR3_DTOOL__VECTOR_COLOR);
		GridData gd_vectorColor = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_vectorColor.widthHint = 100;
		gd_vectorColor.minimumWidth = 100;
		vectorColor.setLayoutData(gd_vectorColor);
		
		
		// Intensity Color
		Label lblSunIntensityColor  = new Label(grpSettings, SWT.NONE);
		lblSunIntensityColor.setText("Sun Intensity Level Color:");
		
		sunIntensityColor = new Color3fComposite(grpSettings, SWT.NONE, "", true, null, ApogyCoreEnvironmentSurfaceEarthUIPackage.Literals.SUN_VECTOR3_DTOOL__SUN_INTENSITY_LEVEL_COLOR);
		GridData gd_sunIntensityColor = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_sunIntensityColor.widthHint = 100;
		gd_sunIntensityColor.minimumWidth = 100;
		sunIntensityColor.setLayoutData(gd_sunIntensityColor);
		
		// Vector Lenght
		Label lblVectorLength  = new Label(grpSettings, SWT.NONE);
		lblVectorLength.setText("Vector Length (m):");
		
		txtVectorLength = new Text(grpSettings, SWT.BORDER);
		GridData gd_txtVectorLength = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtVectorLength.widthHint = 100;
		gd_txtVectorLength.minimumWidth = 100;
		txtVectorLength.setLayoutData(gd_txtVectorLength);
		
		// End Point Radius
		Label lblEndPointRadius  = new Label(grpSettings, SWT.NONE);
		lblEndPointRadius.setText("End Point Radius (m):");
		
		txtEndPointRadius = new Text(grpSettings, SWT.BORDER);				
		GridData gd_txtEndPointRadius = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtEndPointRadius.minimumWidth = 100;
		gd_txtEndPointRadius.widthHint = 100;
		txtEndPointRadius.setLayoutData(gd_txtEndPointRadius);
		
		// TO
		
		Group grpTo = new Group(this, SWT.NONE);
		grpTo.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpTo.setText("To Node");
		grpTo.setLayout(new GridLayout(3, false));	
		
		Label lblToNode = new Label(grpTo, SWT.NONE);
		lblToNode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblToNode.setText("Node:");
		
		txtToNodeID = new Text(grpTo, SWT.BORDER);
		txtToNodeID.setEditable(false);
		GridData gd_lblToNodeID = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblToNodeID.minimumWidth = 250;
		gd_lblToNodeID.widthHint = 250;
		txtToNodeID.setLayoutData(gd_lblToNodeID);
		
		btnSelectToNode = new Button(grpTo, SWT.NONE);
		btnSelectToNode.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnSelectToNode.setText("Select");
		btnSelectToNode.addSelectionListener(new SelectionListener() 
		{			
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				// Opens the Node Selection Dialog.
				NodeSelectionDialog nodeSelectionDialog = new NodeSelectionDialog(getShell(), getRootNode());
				
				if(nodeSelectionDialog.open() == Window.OK)		
				{
					Node toNode = nodeSelectionDialog.getSelectedNode();
					
					if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(getSunVector3DTool(), ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, toNode, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
					{
						ApogyCommonTransactionFacade.INSTANCE.basicSet(getSunVector3DTool(), ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE, toNode);
					}
					else
					{
						getSunVector3DTool().setToNode(toNode);
					}
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{			
			}
		});
		
		Label lblToRelativePosition = new Label(grpTo, SWT.NONE);
		lblToRelativePosition.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblToRelativePosition.setText("Relative Position (m):");
		
		toRelativePositionComposite = new Tuple3dComposite(grpTo, SWT.NONE, "0.000");
		toRelativePositionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
					
		Label lblFromNodeLocked = new Label(grpTo, SWT.NONE);
		lblFromNodeLocked.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFromNodeLocked.setText("Node Locked:");
		
		btnToLocked = new Button(grpTo, SWT.CHECK);		
		new Label(grpTo, SWT.NONE);
		
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}

	public SunVector3DTool getSunVector3DTool() {
		return sunVector3DTool;
	}

	public void setSunVector3DTool(SunVector3DTool sunVector3DTool) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.sunVector3DTool = sunVector3DTool;
		
		if(sunVector3DTool != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}	
	
	private Node getRootNode()
	{
		if(ApogyCoreTopologyFacade.INSTANCE.getApogyTopology() != null)
		{
			return ApogyCoreTopologyFacade.INSTANCE.getApogyTopology().getRootNode();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
			
		// Colors
		vectorColor.setOwner(getSunVector3DTool());
		sunIntensityColor.setOwner(getSunVector3DTool());
		
		
		// Vector Lenght
		IObservableValue<Double> observeVectorLength = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getSunVector3DTool()), 
				  											FeaturePath.fromList(ApogyCoreEnvironmentSurfaceEarthUIPackage.Literals.SUN_VECTOR3_DTOOL__VECTOR_LENGTH)).observe(getSunVector3DTool());
		IObservableValue<String> observeVectorLengthTxt = WidgetProperties.text(SWT.Modify).observe(txtVectorLength);
		
		bindingContext.bindValue(observeVectorLengthTxt,
				observeVectorLength, 
			 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
			 {																		
				@Override
				public Object convert(Object fromObject) 
				{										
					return Double.parseDouble((String) fromObject);
				}

				}), 
			 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
			 	{																		 											
					@Override
					public Object convert(Object fromObject) 
					{											
						return ((Double) fromObject).toString();
					}
				}));
		
		// End radius
		IObservableValue<Double> observeEndPointRadius = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getSunVector3DTool()), 
				  											FeaturePath.fromList(ApogyCoreEnvironmentSurfaceEarthUIPackage.Literals.SUN_VECTOR3_DTOOL__END_POINT_RADIUS)).observe(getSunVector3DTool());
		IObservableValue<String> observeEndPointRadiusTxt = WidgetProperties.text(SWT.Modify).observe(txtEndPointRadius);
		
		bindingContext.bindValue(observeEndPointRadiusTxt,
				observeEndPointRadius, 
			 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
			 {																		
				@Override
				public Object convert(Object fromObject) 
				{										
					return Double.parseDouble((String) fromObject);
				}

				}), 
			 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
			 	{																		 											
					@Override
					public Object convert(Object fromObject) 
					{											
						return ((Double) fromObject).toString();
					}
				}));		
		
		// To ID
		IObservableValue<Node> observeToNode = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getSunVector3DTool()), 
				  FeaturePath.fromList(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE)).observe(getSunVector3DTool());
	
		IObservableValue<String> observeToNodeLabel = WidgetProperties.text(SWT.Modify).observe(txtToNodeID);
		
		bindingContext.bindValue(observeToNodeLabel,
								observeToNode, 
								new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				 new UpdateValueStrategy().setConverter(new Converter(Node.class, String.class)
				 {																		
					@Override
					public Object convert(Object fromObject) 
					{			
						if(fromObject instanceof Node)
						{
							return ((Node) fromObject).getNodeId();
						}
						else
						{
							return "";
						}
					}
					}));
		
		
		// From To Lock Button
		IObservableValue<Double> observeToNodeLock = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(getSunVector3DTool()), 
				  FeaturePath.fromList(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK)).observe(getSunVector3DTool());
		
		IObservableValue<String> observeToNodeLockButton = WidgetProperties.selection().observe(btnToLocked);
		
		bindingContext.bindValue(observeToNodeLockButton,
								 observeToNodeLock, 
								 new UpdateValueStrategy(),	
								 new UpdateValueStrategy());
		
		// To Relative Position
		toRelativePositionComposite.setTuple3d(getSunVector3DTool().getToRelativePosition());	

								
		return bindingContext;
	}
}
