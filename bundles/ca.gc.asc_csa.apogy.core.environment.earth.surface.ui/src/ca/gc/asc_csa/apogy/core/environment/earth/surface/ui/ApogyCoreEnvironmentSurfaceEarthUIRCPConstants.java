package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui;

public class ApogyCoreEnvironmentSurfaceEarthUIRCPConstants 
{
	
	/**
	 * Part IDs for selectionBasedParts.
	 */
	public static final String PART__EARTH_SURFACE_WORKSITES__ID = "ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.part.earthsurfaceworksites";
	public static final String PART__MAPS__ID = "ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.part.maps";
}
