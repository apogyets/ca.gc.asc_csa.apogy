/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.ApogyCoreEnvironmentSurfaceEarthUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.EarthSurfaceWorksiteSettings;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.EarthSurfaceWorksiteWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.wizards.EarthSurfaceWorksiteOriginWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Earth Surface Worksite Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EarthSurfaceWorksiteWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements EarthSurfaceWorksiteWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthSurfaceWorksiteWizardPagesProviderImpl() 
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreEnvironmentSurfaceEarthUIPackage.Literals.EARTH_SURFACE_WORKSITE_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{

		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		EarthSurfaceWorksiteOriginWizardPage earthSurfaceWorksiteOriginWizardPage = new EarthSurfaceWorksiteOriginWizardPage((EarthSurfaceWorksite) eObject);
		list.add(earthSurfaceWorksiteOriginWizardPage);
		
		return list;	
	}
	
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{	
		EarthSurfaceWorksite worksite = ApogyEarthSurfaceEnvironmentFacade.INSTANCE.createEmptyEarthSurfaceWorksite();
		if(settings instanceof EarthSurfaceWorksiteSettings)
		{
			EarthSurfaceWorksiteSettings earthSurfaceWorksiteSettings = (EarthSurfaceWorksiteSettings) settings;
			worksite.setName(earthSurfaceWorksiteSettings.getName());
		}		
		
		return worksite;
	}
} //EarthSurfaceWorksiteWizardPagesProviderImpl
