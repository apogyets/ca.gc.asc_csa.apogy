package ca.gc.asc_csa.apogy.workspace.ui.toolcontrols;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.resources.IProject;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimBar;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimElement;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.workspace.ApogyWorkspaceFacade;
import ca.gc.asc_csa.apogy.workspace.ApogyWorkspacePackage;
import ca.gc.asc_csa.apogy.workspace.ui.ApogyWorkspaceRCPConstants;

public class ActiveProjectToolControl {
	
	private Label lblValue;
	private DataBindingContext bindingContext;
	
	@Inject
	EModelService modelService;
	
	@Inject
	MTrimmedWindow window;

	@PostConstruct
	public void createControls(Composite parent){
		
		MTrimBar trimBar = (MTrimBar)modelService.find(ApogyWorkspaceRCPConstants.TRIMBAR__BOTTOM__ID, window);
		List<String> unwantedItems = Arrays.asList("org.eclipse.ui.StatusLine", "org.eclipse.ui.HeapStatus", "org.eclipse.ui.ProgressBar");
		for(MTrimElement element : trimBar.getChildren()){
			if(unwantedItems.contains(element.getElementId())){
				element.setVisible(false);
				element.setToBeRendered(false);
			}
			
		}
		
		final Composite composite = new Composite(parent, SWT.None);
		composite.setLayout(new GridLayout(1, false));
		
		lblValue = new Label(composite, SWT.BORDER);
		GridData gd_labelValue = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_labelValue.widthHint = 300;
		gd_labelValue.minimumWidth = 350;
		lblValue.setLayoutData(gd_labelValue);
		
		initDataBindings();
	}

	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindings() {
		bindingContext = new DataBindingContext();
		
		/* Import Button Enabled. */
		IObservableValue<?> observeActiveProject = EMFProperties.value(ApogyWorkspacePackage.Literals.APOGY_WORKSPACE_FACADE__ACTIVE_PROJECT).observe(ApogyWorkspaceFacade.INSTANCE);
		IObservableValue<?> observeLabelValueText = WidgetProperties.text().observe(lblValue);
		
		bindingContext.bindValue(observeLabelValueText, observeActiveProject,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(IProject.class, String.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null ? ApogyWorkspaceFacade.INSTANCE.getActiveProject().getName()
										: "No active project";
							}
						}));
		//
		return bindingContext;
	}
	
	@PreDestroy
	public void dispose(){
		bindingContext.dispose();
	}
}