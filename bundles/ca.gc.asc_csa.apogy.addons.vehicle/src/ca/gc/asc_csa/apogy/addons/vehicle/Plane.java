package ca.gc.asc_csa.apogy.addons.vehicle;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

public class Plane 
{
	public Point3d point = null;
	public Vector3d normal = null;
	
	public Plane(Point3d point, Vector3d normal)
	{
		this.point = point;
		this.normal = normal;
	}
}
