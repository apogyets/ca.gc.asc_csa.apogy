package ca.gc.asc_csa.apogy.addons.vehicle;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

public class TerrainProfile 
{
	private SortedSet<Segment2D> segments = null;
	
	public TerrainProfile()
	{		
	}
	
	public TerrainProfile(List<Segment2D> segments)
	{
		getSegments().addAll(segments);
	}

	public SortedSet<Segment2D> getSegments() 
	{
		if(segments == null)
		{
			segments = new TreeSet<Segment2D>();
		}
		return segments;
	}	
}
