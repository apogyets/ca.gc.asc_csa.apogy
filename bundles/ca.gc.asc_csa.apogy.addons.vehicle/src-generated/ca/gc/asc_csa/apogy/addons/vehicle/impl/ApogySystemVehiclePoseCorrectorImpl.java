/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.vehicle.impl;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ApogySystemVehiclePoseCorrector;

import ca.gc.asc_csa.apogy.common.math.Matrix4x4;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.core.AbstractApogySystemPoseCorrector;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;
import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Apogy System Vehicle Pose Corrector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.vehicle.impl.ApogySystemVehiclePoseCorrectorImpl#getApogySystemApiAdapter <em>Apogy System Api Adapter</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.vehicle.impl.ApogySystemVehiclePoseCorrectorImpl#isEnabled <em>Enabled</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ApogySystemVehiclePoseCorrectorImpl extends VehiclePoseCorrectorImpl implements ApogySystemVehiclePoseCorrector {
	/**
	 * The default value of the '{@link #isEnabled() <em>Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ENABLED_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isEnabled() <em>Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnabled()
	 * @generated
	 * @ordered
	 */
	protected boolean enabled = ENABLED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogySystemVehiclePoseCorrectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsVehiclePackage.Literals.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogySystemApiAdapter getApogySystemApiAdapter() {
		if (eContainerFeatureID() != ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER) return null;
		return (ApogySystemApiAdapter)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogySystemApiAdapter basicGetApogySystemApiAdapter() {
		if (eContainerFeatureID() != ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER) return null;
		return (ApogySystemApiAdapter)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetApogySystemApiAdapter(ApogySystemApiAdapter newApogySystemApiAdapter, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newApogySystemApiAdapter, ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApogySystemApiAdapter(ApogySystemApiAdapter newApogySystemApiAdapter) {
		if (newApogySystemApiAdapter != eInternalContainer() || (eContainerFeatureID() != ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER && newApogySystemApiAdapter != null)) {
			if (EcoreUtil.isAncestor(this, newApogySystemApiAdapter))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newApogySystemApiAdapter != null)
				msgs = ((InternalEObject)newApogySystemApiAdapter).eInverseAdd(this, ApogyCorePackage.APOGY_SYSTEM_API_ADAPTER__POSE_CORRECTOR, ApogySystemApiAdapter.class, msgs);
			msgs = basicSetApogySystemApiAdapter(newApogySystemApiAdapter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER, newApogySystemApiAdapter, newApogySystemApiAdapter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnabled() {
		return enabled;
	}

	@Override
	public void setEnabled(boolean newEnabled) 
	{	
		boolean previousEnabled = isEnabled();
		   	 	  	  
		setEnabledGen(newEnabled);
		  
		// Forces an update if the corrector is being enabled.
		if(newEnabled && !previousEnabled)
		{
			try
			{
				updateMeshes();
				  
				// Upadtes pose.
				if(getApogySystemApiAdapter() != null)
				{
					Matrix4x4 correctedPose = applyCorrection(getApogySystemApiAdapter().getPoseTransform());
					getApogySystemApiAdapter().setPoseTransform(correctedPose);
				 }
			 }
			 catch(Throwable t)
			 {
				 t.printStackTrace();
			 }
		 }
	  }
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnabledGen(boolean newEnabled) {
		boolean oldEnabled = enabled;
		enabled = newEnabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__ENABLED, oldEnabled, enabled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetApogySystemApiAdapter((ApogySystemApiAdapter)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER:
				return basicSetApogySystemApiAdapter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER:
				return eInternalContainer().eInverseRemove(this, ApogyCorePackage.APOGY_SYSTEM_API_ADAPTER__POSE_CORRECTOR, ApogySystemApiAdapter.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER:
				if (resolve) return getApogySystemApiAdapter();
				return basicGetApogySystemApiAdapter();
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__ENABLED:
				return isEnabled();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER:
				setApogySystemApiAdapter((ApogySystemApiAdapter)newValue);
				return;
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__ENABLED:
				setEnabled((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER:
				setApogySystemApiAdapter((ApogySystemApiAdapter)null);
				return;
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__ENABLED:
				setEnabled(ENABLED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER:
				return basicGetApogySystemApiAdapter() != null;
			case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__ENABLED:
				return enabled != ENABLED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractApogySystemPoseCorrector.class) {
			switch (derivedFeatureID) {
				case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER: return ApogyCorePackage.ABSTRACT_APOGY_SYSTEM_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER;
				case ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__ENABLED: return ApogyCorePackage.ABSTRACT_APOGY_SYSTEM_POSE_CORRECTOR__ENABLED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AbstractApogySystemPoseCorrector.class) {
			switch (baseFeatureID) {
				case ApogyCorePackage.ABSTRACT_APOGY_SYSTEM_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER: return ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__APOGY_SYSTEM_API_ADAPTER;
				case ApogyCorePackage.ABSTRACT_APOGY_SYSTEM_POSE_CORRECTOR__ENABLED: return ApogyAddonsVehiclePackage.APOGY_SYSTEM_VEHICLE_POSE_CORRECTOR__ENABLED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (enabled: ");
		result.append(enabled);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public Node getSystemRootNode() 
	{	
		return getApogySystemApiAdapter().getApogySystem().getTopologyRoot().getOriginNode();
	}
	
	@Override
	public Matrix4x4 applyCorrection(Matrix4x4 originalPose) 
	{	
		if(isEnabled())
		{
			return super.applyCorrection(originalPose);
		}
		else
		{
			return originalPose;
		}
	}
	
} //ApogySystemVehiclePoseCorrectorImpl
