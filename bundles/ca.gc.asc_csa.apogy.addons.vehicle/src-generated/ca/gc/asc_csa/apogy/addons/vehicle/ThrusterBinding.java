package ca.gc.asc_csa.apogy.addons.vehicle;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Thruster Binding</b></em>'.
 * <!-- end-user-doc --> *
 * <!-- begin-model-doc -->
 * *
 * A topology binding used to bind a Thruster in the topology to one in a System.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.vehicle.ThrusterBinding#getThruster <em>Thruster</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage#getThrusterBinding()
 * @model
 * @generated
 */
public interface ThrusterBinding extends AbstractTopologyBinding
{
  /**
	 * Returns the value of the '<em><b>Thruster</b></em>' reference.
	 * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Thruster</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->	 * @return the value of the '<em>Thruster</em>' reference.
	 * @see #setThruster(Thruster)
	 * @see ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage#getThrusterBinding_Thruster()
	 * @model
	 * @generated
	 */
  Thruster getThruster();

  /**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.addons.vehicle.ThrusterBinding#getThruster <em>Thruster</em>}' reference.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @param value the new value of the '<em>Thruster</em>' reference.
	 * @see #getThruster()
	 * @generated
	 */
  void setThruster(Thruster value);

} // ThrusterBinding
