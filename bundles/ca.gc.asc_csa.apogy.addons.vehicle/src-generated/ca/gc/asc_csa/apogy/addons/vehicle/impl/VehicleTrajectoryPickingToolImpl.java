/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.vehicle.impl;

import java.lang.reflect.InvocationTargetException;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import ca.gc.asc_csa.apogy.addons.geometry.paths.ApogyAddonsGeometryPathsFactory;
import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath;
import ca.gc.asc_csa.apogy.addons.impl.TrajectoryPickingToolImpl;
import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.VehicleTrajectoryPickingTool;
import ca.gc.asc_csa.apogy.common.geometry.data.ApogyCommonGeometryDataPackage;
import ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DFacade;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates;
import ca.gc.asc_csa.apogy.common.topology.ui.NodeSelection;
import ca.gc.asc_csa.apogy.common.topology.ui.viewer.MouseButton;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.TypeApiAdapter;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vehicle Trajectory Picking Tool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.vehicle.impl.VehicleTrajectoryPickingToolImpl#getVehiculeVariableFeatureReference <em>Vehicule Variable Feature Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VehicleTrajectoryPickingToolImpl extends TrajectoryPickingToolImpl implements VehicleTrajectoryPickingTool {
	/**
	 * The cached value of the '{@link #getVehiculeVariableFeatureReference() <em>Vehicule Variable Feature Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVehiculeVariableFeatureReference()
	 * @generated
	 * @ordered
	 */
	protected VariableFeatureReference vehiculeVariableFeatureReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VehicleTrajectoryPickingToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsVehiclePackage.Literals.VEHICLE_TRAJECTORY_PICKING_TOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableFeatureReference getVehiculeVariableFeatureReference() {
		return vehiculeVariableFeatureReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVehiculeVariableFeatureReference(VariableFeatureReference newVehiculeVariableFeatureReference, NotificationChain msgs) {
		VariableFeatureReference oldVehiculeVariableFeatureReference = vehiculeVariableFeatureReference;
		vehiculeVariableFeatureReference = newVehiculeVariableFeatureReference;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE, oldVehiculeVariableFeatureReference, newVehiculeVariableFeatureReference);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVehiculeVariableFeatureReference(VariableFeatureReference newVehiculeVariableFeatureReference) {
		if (newVehiculeVariableFeatureReference != vehiculeVariableFeatureReference) {
			NotificationChain msgs = null;
			if (vehiculeVariableFeatureReference != null)
				msgs = ((InternalEObject)vehiculeVariableFeatureReference).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE, null, msgs);
			if (newVehiculeVariableFeatureReference != null)
				msgs = ((InternalEObject)newVehiculeVariableFeatureReference).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE, null, msgs);
			msgs = basicSetVehiculeVariableFeatureReference(newVehiculeVariableFeatureReference, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE, newVehiculeVariableFeatureReference, newVehiculeVariableFeatureReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public WayPointPath getLocalPath() 
	{
		if(getActivePath() != null)
		{			
			Matrix4d vehiculePose = getVehiculePose();
			Matrix4d inv = new Matrix4d(vehiculePose);
			inv.invert();
			
			WayPointPath localPath = ApogyAddonsGeometryPathsFactory.eINSTANCE.createWayPointPath();
			
			for(CartesianPositionCoordinates point : getActivePath().getPoints())
			{
				// Transform the point in the vehicule frame.
				Vector3d pointPosition = new Vector3d(point.asPoint3d());				
				
				Matrix4d pointPose = new Matrix4d();
				pointPose.setIdentity();
				pointPose.setTranslation(pointPosition);
				pointPose.invert();			
				pointPose.mul(vehiculePose);
				pointPose.invert();	
				
				Vector3d pointRelativePosition = new Vector3d();
				pointPose.get(pointRelativePosition);								
				
				CartesianPositionCoordinates localPoint = ApogyCommonGeometryData3DFacade.INSTANCE.createCartesianPositionCoordinates(pointRelativePosition.x, pointRelativePosition.y, pointRelativePosition.z);
				
				// Adds the local point into the path.
				localPath.getPoints().add(localPoint);
			}
			
			return localPath;
		}
		else
		{
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE:
				return basicSetVehiculeVariableFeatureReference(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE:
				return getVehiculeVariableFeatureReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE:
				setVehiculeVariableFeatureReference((VariableFeatureReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE:
				setVehiculeVariableFeatureReference((VariableFeatureReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL__VEHICULE_VARIABLE_FEATURE_REFERENCE:
				return vehiculeVariableFeatureReference != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyAddonsVehiclePackage.VEHICLE_TRAJECTORY_PICKING_TOOL___GET_LOCAL_PATH:
				return getLocalPath();
		}
		return super.eInvoke(operationID, arguments);
	}

	@Override
	public void setActive(boolean newActive) 
	{			
		try
		{
			super.setActive(newActive);
		}
		catch(Throwable t)
		{			
		}
		
		// Clears the path.
		if(newActive && getActivePath() != null)
		{
			ApogyCommonTransactionFacade.INSTANCE.basicRemove(getActivePath(), ApogyCommonGeometryDataPackage.Literals.COORDINATES_SET__POINTS, getActivePath().getPoints());
		}
	}
	
	@Override
	public void selectionChanged(NodeSelection nodeSelection) 
	{
		if(getActivePath() != null && getActivePath().getPoints().isEmpty())
		{
			// Adds the current position of the vehicule.
			ApogyCommonTransactionFacade.INSTANCE.basicAdd(getActivePath(), ApogyCommonGeometryDataPackage.Literals.COORDINATES_SET__POINTS, getVechiculeCurrentPosition());		
		}
		
		super.selectionChanged(nodeSelection);
	}
	
	@Override
	public void mouseButtonClicked(MouseButton mouseButtonClicked) 
	{
		if(mouseButtonClicked == MouseButton.RIGHT)
		{
			if(getActivePath() != null && getActivePath().getPoints().size() > 1)
			{
				CartesianPositionCoordinates coord = getActivePath().getPoints().get(getActivePath().getPoints().size() -1);
				ApogyCommonTransactionFacade.INSTANCE.basicRemove(getActivePath(), ApogyCommonGeometryDataPackage.Literals.COORDINATES_SET__POINTS, coord);
			}
		}
	}
	
	/**
	 * Gets the current pose of the specified VehiculeVariableFeatureReference.
	 * @return The matrix4d representing the vehicule pose.
	 */
	protected Matrix4d getVehiculePose()
	{
		Matrix4d m = new Matrix4d();
		m.setIdentity();
		
		if(getVehiculeVariableFeatureReference() != null)
		{
			TypeApiAdapter apiAdapter = ApogyCoreInvocatorFacade.INSTANCE.getTypeApiAdapter(getVehiculeVariableFeatureReference());
			if(apiAdapter instanceof ApogySystemApiAdapter)
			{
				ApogySystemApiAdapter apogySystemApiAdapter = (ApogySystemApiAdapter) apiAdapter;
				if(apogySystemApiAdapter.getPoseTransform() != null)
				{
					m = apogySystemApiAdapter.getPoseTransform().asMatrix4d();
				}
			}
		}
		
		return m;
	}
	
	protected CartesianPositionCoordinates getVechiculeCurrentPosition()
	{
		Matrix4d vehiclePose = getVehiculePose();
		Vector3d position = new Vector3d();
		vehiclePose.get(position);
		
		CartesianPositionCoordinates point = ApogyCommonGeometryData3DFacade.INSTANCE.createCartesianPositionCoordinates(position.x, position.y, position.z);

		return point;
	}
} //VehicleTrajectoryPickingToolImpl
