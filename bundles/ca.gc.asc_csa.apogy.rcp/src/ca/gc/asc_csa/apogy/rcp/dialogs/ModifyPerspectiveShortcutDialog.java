package ca.gc.asc_csa.apogy.rcp.dialogs;

import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.workbench.IResourceUtilities;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.rcp.ApogyRCPConstants;

public class ModifyPerspectiveShortcutDialog extends Dialog {

	private Text labelText;
	private Text toolTipText;
	private Text iconURIText;
	private Button checkBoxLabel;
	private Button checkBoxIcon;

	private MPerspective perspective;
	private IResourceUtilities<?> resourceUtilities;

	public ModifyPerspectiveShortcutDialog(Shell parentShell, MPerspective perspective,
			IResourceUtilities<?> resourceUtilities) {
		super(parentShell);
		setShellStyle(getShellStyle() | SWT.SHEET);
		this.perspective = perspective;
		this.resourceUtilities = resourceUtilities;
	}

	@Override
	protected void configureShell(Shell shell) {
		shell.setText("Modify perspective shortcut");
		super.configureShell(shell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = new Composite(parent, SWT.None);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		composite.setLayout(new GridLayout(4, false));

		/** Label for layout */
		Label label1 = new Label(composite, SWT.None);
		label1.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));

		Label labelDisplay = new Label(composite, SWT.None);
		labelDisplay.setText("Display");
		labelDisplay.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, true, 1, 1));

		/**
		 * Label of the perspective
		 */
		Label labelLabel = new Label(composite, SWT.None);
		labelLabel.setText("Label : ");
		labelLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));

		labelText = new Text(composite, SWT.SINGLE | SWT.BORDER);
		labelText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		if (perspective.getLabel() != null) {
			labelText.setText(perspective.getLabel());
		}

		checkBoxLabel = new Button(composite, SWT.CHECK);
		checkBoxLabel.setSelection(perspective.getPersistedState()
				.containsKey(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL)
				&& perspective.getPersistedState().get(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL)
						.equals("true"));
		checkBoxLabel.setLayoutData(GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.FILL).create());

		/**
		 * ToolTip of the perspective
		 */
		Label toolTipLabel = new Label(composite, SWT.None);
		toolTipLabel.setText("ToolTip : ");
		toolTipLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));

		toolTipText = new Text(composite, SWT.SINGLE | SWT.BORDER);
		toolTipText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		if (perspective.getTooltip() != null) {
			toolTipText.setText(perspective.getTooltip());
		}
		new Label(composite, SWT.None);

		/**
		 * Icon of the perspective
		 */
		Label iconURILabel = new Label(composite, SWT.None);
		iconURILabel.setText("Icon file : ");
		iconURILabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, true, 1, 1));

		iconURIText = new Text(composite, SWT.SINGLE | SWT.BORDER);
		iconURIText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		if (perspective.getIconURI() != null) {
			iconURIText.setText(perspective.getIconURI());
		}

		Button iconURIButton = new Button(composite, SWT.ICON_SEARCH);
		iconURIButton.setText("...");
		iconURIButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1));
		iconURIButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Wizard wizard = new Wizard() {
					IconURLDialog iconDialog;

					@Override
					public void addPages() {
						this.iconDialog = new IconURLDialog(ModifyPerspectiveShortcutDialog.this.perspective,
								resourceUtilities);
						addPage(this.iconDialog);
					}

					@Override
					public boolean performFinish() {
						perspective.setIconURI(this.iconDialog.getUrlString());
						iconURIText.setText(this.iconDialog.getUrlString());
						return true;
					}
				};
				WizardDialog dialog = new WizardDialog(getShell(), wizard);
				dialog.open();
			}
		});

		checkBoxIcon = new Button(composite, SWT.CHECK);
		checkBoxIcon.setSelection(perspective.getPersistedState()
				.containsKey(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON)
				&& perspective.getPersistedState().get(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON)
						.equals("true"));
		checkBoxIcon.setLayoutData(GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.FILL).create());

		return composite;
	}

	@Override
	protected void okPressed() {
		perspective.getPersistedState().put(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_LABEL,
				String.valueOf(checkBoxLabel.getSelection()));
		perspective.getPersistedState().put(ApogyRCPConstants.PERSISTED_STATE__MPERSPECTIVE__DISPLAY_ICON,
				String.valueOf(checkBoxIcon.getSelection()));

		perspective.setLabel(labelText.getText());
		perspective.setTooltip(toolTipText.getText());
		perspective.setIconURI(iconURIText.getText());

		super.okPressed();
	}

}
