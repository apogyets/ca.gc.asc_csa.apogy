package ca.gc.asc_csa.apogy.common.databinding.converters;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.NumberFormat;
import java.text.ParseException;

public class StringToLongConverter extends AbstractNumberConverter 
{
	public StringToLongConverter()
	{
		super(String.class, Long.class);
	}
	
	public StringToLongConverter(NumberFormat numberFormat)
	{
		super(String.class, Long.class);
		setNumberFormat(numberFormat);
	}
	
	public Object convert(Object fromObject) 
	{
		if(getNumberFormat() != null)
		{				
			try 
			{
				return new Long(getNumberFormat().parse( (String) fromObject).longValue());
			} 
			catch (ParseException e) 
			{			
				e.printStackTrace();
			}
		}
		return new Long((String)fromObject);		
	}

}
