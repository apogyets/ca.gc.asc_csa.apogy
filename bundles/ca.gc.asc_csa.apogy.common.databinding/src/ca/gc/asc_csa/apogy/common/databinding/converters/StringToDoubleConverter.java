package ca.gc.asc_csa.apogy.common.databinding.converters;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.NumberFormat;
import java.text.ParseException;

public class StringToDoubleConverter extends AbstractNumberConverter {

	public StringToDoubleConverter() {
		super(String.class, Double.class);
	}

	public StringToDoubleConverter(NumberFormat numberFormat) 
	{
		super(String.class, Double.class);
		setNumberFormat(numberFormat);
	}
	
	public Object convert(Object fromObject) 
	{
		if(getNumberFormat() != null)
		{				
			try 
			{
				return new Double(getNumberFormat().parse( (String) fromObject).doubleValue());
			} 
			catch (ParseException e) 
			{			
				e.printStackTrace();
			}
		}
		return new Double((String)fromObject);		
	}
}
