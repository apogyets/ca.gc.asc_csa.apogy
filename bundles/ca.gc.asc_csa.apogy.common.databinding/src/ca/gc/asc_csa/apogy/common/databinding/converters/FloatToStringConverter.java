package ca.gc.asc_csa.apogy.common.databinding.converters;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.NumberFormat;

public class FloatToStringConverter extends AbstractNumberConverter 
{
	public FloatToStringConverter()
	{
		super(Float.class, String.class);
	}
	
	public FloatToStringConverter(NumberFormat numberFormat)
	{
		super(Float.class, String.class);
		setNumberFormat(numberFormat);
	}
		
	public Object convert(Object fromObject) 
	{
		if(getNumberFormat() != null)
		{
			try
			{
				Float value = (Float) fromObject;
				return getNumberFormat().format(value);
			}
			catch(Exception e)
			{
				return fromObject.toString();
			}
		}
		else
		{
			return fromObject.toString();
		}
	}
}
