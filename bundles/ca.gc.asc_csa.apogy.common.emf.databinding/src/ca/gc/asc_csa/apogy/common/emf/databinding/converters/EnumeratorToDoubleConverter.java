package ca.gc.asc_csa.apogy.common.emf.databinding.converters;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.emf.common.util.Enumerator;

public class EnumeratorToDoubleConverter extends Converter 
{	
	public EnumeratorToDoubleConverter() 
	{
		super(Enumerator.class, Double.class);
	}
	
	@Override
	public Object convert(Object fromObject) 
	{
		try
		{
			Enumerator enumerator = (Enumerator) fromObject;			
			return new Double(enumerator.getValue());
		}
		catch(Throwable t)
		{
			t.printStackTrace();
			return 0.0;
		}
	}
}
