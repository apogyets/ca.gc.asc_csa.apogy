package ca.gc.asc_csa.apogy.common.emf.databinding;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.core.databinding.observable.value.ValueDiff;

public class DefaultValueDiff extends ValueDiff 
{
	private Object oldValue;
	private Object newValue;
	
	public DefaultValueDiff(Object oldValue, Object newValue)
	{
		this.oldValue = oldValue;
		this.newValue = newValue;
	}
	
	@Override
	public Object getOldValue() 
	{	
		return oldValue;
	}
	@Override
	public Object getNewValue() 
	{	
		return newValue;
	}
}
