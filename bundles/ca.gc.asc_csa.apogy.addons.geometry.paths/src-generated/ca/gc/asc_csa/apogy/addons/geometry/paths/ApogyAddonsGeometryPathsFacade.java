package ca.gc.asc_csa.apogy.addons.geometry.paths;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import ca.gc.asc_csa.apogy.addons.geometry.paths.impl.ApogyAddonsGeometryPathsFacadeImpl;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianAxis;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangularMesh;
import ca.gc.asc_csa.apogy.common.math.Matrix4x4;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc --> *
 *
 * @see ca.gc.asc_csa.apogy.addons.geometry.paths.ApogyAddonsGeometryPathsPackage#getApogyAddonsGeometryPathsFacade()
 * @model
 * @generated
 */
public interface ApogyAddonsGeometryPathsFacade extends EObject {

	public static ApogyAddonsGeometryPathsFacade INSTANCE = ApogyAddonsGeometryPathsFacadeImpl.getInstance();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Creates a WayPointPath from a WayPointPath. The WayPointPath created
	 * contains copies of all the points found in the specified WayPointPath.
	 * @param wayPointPath The original WayPointPath.
	 * @return A WayPointPath that contains copies of the point found in the original.
	 * <!-- end-model-doc -->
	 * @model unique="false" wayPointPathUnique="false"
	 * @generated
	 */
	WayPointPath createWayPointPath(WayPointPath wayPointPath);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Creates a WayPointPath using a list of CartesianPositionCoordinates.
	 * The WayPointPath created contains copies of all the points found in
	 * the specified list.
	 * @param points The list of CartesianPositionCoordinates.
	 * @return A WayPointPath that contains copies of the point found in the list.
	 * <!-- end-model-doc -->
	 * @model unique="false" pointsDataType="ca.gc.asc_csa.apogy.addons.geometry.paths.List<ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates>" pointsUnique="false" pointsMany="false"
	 * @generated
	 */
	WayPointPath createWayPointPath(List<CartesianPositionCoordinates> points);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * <!-- begin-model-doc -->
	 * *
	 * Creates a WayPointPath by finding the projection of each point of the original WayPointPath onto a specified mesh.
	 * If no projection is found, a copy of the original point is used. Note that point may be added at location where
	 * the WayPointPath projection crosses polygons edges.
	 * @param originalPath The original WayPointPath.
	 * @param mesh The mesh on which to find the projection.
	 * @param meshToPathTransform The 4X4 matrix expressing the transformation to bring the trajectory into the mesh reference frame.
	 * @param projectionAxis The axis (w.r.t the mesh reference frame) to use when finding the projection.
	 * @param heightOffset Distance by which the generated WayPointPath's point has to be shifted along the projection axis.
	 * <!-- end-model-doc -->
	 * @model unique="false" originalPathUnique="false" meshUnique="false" meshToPathTransformUnique="false" projectionAxisUnique="false" heightOffsetUnique="false"
	 * @generated
	 */
	WayPointPath projectOntoMesh(WayPointPath originalPath, CartesianTriangularMesh mesh, Matrix4x4 meshToPathTransform, CartesianAxis projectionAxis, double heightOffset);

} // ApogyAddonsGeometryPathsFacade
