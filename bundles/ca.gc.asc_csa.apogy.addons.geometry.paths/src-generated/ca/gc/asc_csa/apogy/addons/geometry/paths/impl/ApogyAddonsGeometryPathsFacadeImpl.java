package ca.gc.asc_csa.apogy.addons.geometry.paths.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector2d;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.addons.geometry.paths.ApogyAddonsGeometryPathsFacade;
import ca.gc.asc_csa.apogy.addons.geometry.paths.ApogyAddonsGeometryPathsFactory;
import ca.gc.asc_csa.apogy.addons.geometry.paths.ApogyAddonsGeometryPathsPackage;
import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath;
import ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DFacade;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianAxis;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPlane;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPolygon;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangle;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangularMesh;
import ca.gc.asc_csa.apogy.common.geometry.data3d.Geometry3DUtilities;
import ca.gc.asc_csa.apogy.common.math.Matrix4x4;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc --> *
 * @generated
 */
public class ApogyAddonsGeometryPathsFacadeImpl extends MinimalEObjectImpl.Container implements ApogyAddonsGeometryPathsFacade {
	
	private static ApogyAddonsGeometryPathsFacade instance = null;
	
	public static ApogyAddonsGeometryPathsFacade getInstance()
	{
		if (instance == null)
		{
			instance = new ApogyAddonsGeometryPathsFacadeImpl();
		}
		
		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected ApogyAddonsGeometryPathsFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsGeometryPathsPackage.Literals.APOGY_ADDONS_GEOMETRY_PATHS_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@Override
	public WayPointPath createWayPointPath(WayPointPath wayPointPath) {
		
		WayPointPath path = createWayPointPath(wayPointPath.getPoints());	
		
		// Copies the description.		
		path.setDescription(wayPointPath.getDescription());		
		
		return path;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@Override
	public WayPointPath createWayPointPath(List<CartesianPositionCoordinates> points)
	{
		WayPointPath path = ApogyAddonsGeometryPathsFactory.eINSTANCE.createWayPointPath();
				
		// Copies all of the points.
		Iterator <CartesianPositionCoordinates> it = points.iterator();
		while(it.hasNext())
		{
			CartesianPositionCoordinates wayPointCopy = ApogyCommonGeometryData3DFacade.INSTANCE.createCartesianPositionCoordinates(it.next());
			path.getPoints().add(wayPointCopy);
		}				
					
		return path;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public WayPointPath projectOntoMesh(WayPointPath originalPath, CartesianTriangularMesh mesh, Matrix4x4 meshToPathTransform, CartesianAxis projectionAxis, double heightOffset) 
	{
		// First, creates a transformed version of trajectory to bring it into the mesh frame.
		Matrix4d transform = meshToPathTransform.asMatrix4d();		
		
		WayPointPath transformedWayPointPath = ApogyAddonsGeometryPathsFactory.eINSTANCE.createWayPointPath();
		for(CartesianPositionCoordinates point : originalPath.getPoints())
		{
			transformedWayPointPath.getPoints().add(Geometry3DUtilities.createTransformedCartesianPositionCoordinates(transform, point));
		}
		
		WayPointPath temp = createWayPointPath(transformedWayPointPath);
		if(transformedWayPointPath.getPoints().size() >= 2)
		{
			CartesianPositionCoordinates p1 = null;
			CartesianPositionCoordinates p2 = null;
			for(int j = 1; j < transformedWayPointPath.getPoints().size(); j++)
			{
				// For each line segment in the WayPointPath, finds the polygons it intersect.
				p1 = transformedWayPointPath.getPoints().get(j-1);
				p2 = transformedWayPointPath.getPoints().get(j);
				
				TreeSet<CartesianPositionCoordinates> crossingPoints = getPolygonEdgeCrossingPoints(projectionAxis, p1, p2, mesh.getPolygons());
				if(!crossingPoints.isEmpty())
				{
					int startIndex = temp.getPoints().indexOf(p1);
					temp.getPoints().addAll(startIndex, crossingPoints);
				}				
			}
		}
				
		// For each point in the trajectory, find the intersection with the polygons in the mesh.
		CartesianPositionCoordinates[] projections = Geometry3DUtilities.getProjectionAlongAxisOnToPolygon(projectionAxis, temp.getPoints(), mesh.getPolygons());

		// Replace the point in the original by its projection if one was found.
		for(int i = 0; i < projections.length; i++)
		{
			CartesianPositionCoordinates projection = projections[i];
			if(projection != null)
			{
				CartesianPositionCoordinates point = temp.getPoints().get(i);
				point.setX(projection.getX());
				point.setY(projection.getY());
				point.setZ(projection.getZ());
				
				// Adds the height Offset to the projection.
				switch (projectionAxis.getValue()) 
				{
					case CartesianAxis.X_VALUE:
						point.setX(point.getX() + heightOffset);
					break;
					case CartesianAxis.Y_VALUE:
						point.setY(point.getY() + heightOffset);
					break;
					case CartesianAxis.Z_VALUE:
						point.setZ(point.getZ() + heightOffset);
					break;
	
					default:
					break;
				}
			}			
		}				
						
		// Transform the point back into the original frame of reference.		
		Matrix4d transformInverse = meshToPathTransform.asMatrix4d();	
		transformInverse.invert();
		
		WayPointPath result = ApogyAddonsGeometryPathsFactory.eINSTANCE.createWayPointPath();
		for(CartesianPositionCoordinates point : temp.getPoints())
		{
			result.getPoints().add(Geometry3DUtilities.createTransformedCartesianPositionCoordinates(transformInverse, point));
		}
		return result;
	}

	private TreeSet<CartesianPositionCoordinates> getPolygonEdgeCrossingPoints(CartesianAxis projectionAxis, CartesianPositionCoordinates p1, CartesianPositionCoordinates p2, Collection<CartesianTriangle> polygons)
	{
		List<CartesianPositionCoordinates> crossingPoints = new ArrayList<CartesianPositionCoordinates>();
		
		CartesianPlane plane = Geometry3DUtilities.getPerpendicularPlane(projectionAxis);
		for(CartesianPolygon polygon : polygons)
		{
			if(polygon.getVertices().size() > 2)
			{
				Vector2d u1 = Geometry3DUtilities.getVector2D(plane, p1);
				Vector2d u2 = Geometry3DUtilities.getVector2D(plane, p2);
					
				// Check to see if the line intersect at least one edge of the polygon.
				int i = 0;			
				while(i < polygon.getVertices().size())
				{								
					CartesianPositionCoordinates e1 = null;
					CartesianPositionCoordinates e2 = null;
					
					// Test all edges.
					if(i <  (polygon.getVertices().size() -1))
					{
						e1 = polygon.getVertices().get(i);
						e2 = polygon.getVertices().get(i+1);
					}
					else
					{
						e1 = polygon.getVertices().get( polygon.getVertices().size() -1);
						e2 = polygon.getVertices().get(0);
					}
													
					Vector2d v1 = Geometry3DUtilities.getVector2D(plane, e1);
					Vector2d v2 = Geometry3DUtilities.getVector2D(plane, e2);
					
					// Find the intersection point between the line and the edge.
					Vector2d intersect = Geometry3DUtilities.getLineIntersectionPoint(u1,u2,v1,v2);
					
					// Checks is the intersection point falls on the edge segment and the line segment.
					if(intersect != null)
					{
						double edgeLength = Geometry3DUtilities.getDistance(v1,v2);
						double lineLength = Geometry3DUtilities.getDistance(u1,u2);										
												
						if((Geometry3DUtilities.getDistance(intersect,v1) < edgeLength) && 
							(Geometry3DUtilities.getDistance(intersect,v1) > 0) && 
							(Geometry3DUtilities.getDistance(intersect,v2) < edgeLength) &&
							(Geometry3DUtilities.getDistance(intersect,v2) > 0) && 
							(Geometry3DUtilities.getDistance(intersect,u1) < lineLength) &&
							(Geometry3DUtilities.getDistance(intersect,u1) > 0) && 
							(Geometry3DUtilities.getDistance(intersect,u2) < lineLength) &&
							(Geometry3DUtilities.getDistance(intersect,u2) > 0))
						{
							Point3d intersectionPoint = new Point3d(intersect.x, intersect.y, 0);
							crossingPoints.add(Geometry3DUtilities.getProjectionAlongAxisOnToPolygon(projectionAxis, intersectionPoint, polygon));
						}
					}				
					i++;
				}
			}
		}
		
		// Sorts the resulting point by order of distance relative to the start of the segment.
		TreeSet<CartesianPositionCoordinates> sortedPoints = new TreeSet<>(new Comparator<CartesianPositionCoordinates>() 
		{
			@Override
			public int compare(CartesianPositionCoordinates arg0, CartesianPositionCoordinates arg1) 
			{
				double d0 =  p1.asPoint3d().distance(arg0.asPoint3d());
				double d1 =  p1.asPoint3d().distance(arg1.asPoint3d());
				
				if(d0 < d1)
				{
					return -1;
				}
				else if(d0 > d1)
				{
					return 1;
				}
				else
				{
					return 0;
				}
			}
		});
		sortedPoints.addAll(crossingPoints);
		
		return sortedPoints;
	}
	
//	private List<CartesianTriangle> getPolygonsIntersectectByLine(CartesianAxis projectionAxis, CartesianPositionCoordinates p1, CartesianPositionCoordinates p2, List<CartesianTriangle> polygons)
//	{
//		List<CartesianTriangle> intersectedPolygons = new ArrayList<CartesianTriangle>();
//		
//		CartesianPlane plane = Geometry3DUtilities.getPerpendicularPlane(projectionAxis);
//		for(CartesianPolygon polygon : polygons)
//		{
//			if(polygon.getVertices().size() > 2)
//			{
//				Vector2d u1 = Geometry3DUtilities.getVector2D(plane, p1);
//				Vector2d u2 = Geometry3DUtilities.getVector2D(plane, p2);
//					
//				// Check to see if the line intersect at least one edge of the polygon.
//				int i = 0;			
//				while(i < polygon.getVertices().size())
//				{								
//					CartesianPositionCoordinates e1 = null;
//					CartesianPositionCoordinates e2 = null;
//					
//					// Test all edges.
//					if(i <  (polygon.getVertices().size() -1))
//					{
//						e1 = polygon.getVertices().get(i);
//						e2 = polygon.getVertices().get(i+1);
//					}
//					else
//					{
//						e1 = polygon.getVertices().get( polygon.getVertices().size() -1);
//						e2 = polygon.getVertices().get(0);
//					}
//													
//					Vector2d v1 = Geometry3DUtilities.getVector2D(plane, e1);
//					Vector2d v2 = Geometry3DUtilities.getVector2D(plane, e2);
//					
//					// Find the intersection point between the line and the edge.
//					Vector2d intersect = Geometry3DUtilities.getLineIntersectionPoint(u1,u2,v1,v2);
//					
//					// Checks is the intersection point falls on the edge segment and the line segment.
//					if(intersect != null)
//					{
//						double edgeLength = Geometry3DUtilities.getDistance(v1,v2);
//						double lineLength = Geometry3DUtilities.getDistance(u1,u2);										
//						
//						
//						if((Geometry3DUtilities.getDistance(intersect,v1) < edgeLength) && 
//							(Geometry3DUtilities.getDistance(intersect,v1) > 0) && 
//							(Geometry3DUtilities.getDistance(intersect,v2) < edgeLength) &&
//							(Geometry3DUtilities.getDistance(intersect,v2) > 0) && 
//							(Geometry3DUtilities.getDistance(intersect,u1) < lineLength) &&
//							(Geometry3DUtilities.getDistance(intersect,u1) > 0) && 
//							(Geometry3DUtilities.getDistance(intersect,u2) < lineLength) &&
//							(Geometry3DUtilities.getDistance(intersect,u2) > 0))
//						{
//							intersectedPolygons.add(polygon);
//						}
//					}				
//					i++;
//				}
//			}
//		}
//		
//		return intersectedPolygons;
//	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyAddonsGeometryPathsPackage.APOGY_ADDONS_GEOMETRY_PATHS_FACADE___CREATE_WAY_POINT_PATH__WAYPOINTPATH:
				return createWayPointPath((WayPointPath)arguments.get(0));
			case ApogyAddonsGeometryPathsPackage.APOGY_ADDONS_GEOMETRY_PATHS_FACADE___CREATE_WAY_POINT_PATH__LIST:
				return createWayPointPath((List<CartesianPositionCoordinates>)arguments.get(0));
			case ApogyAddonsGeometryPathsPackage.APOGY_ADDONS_GEOMETRY_PATHS_FACADE___PROJECT_ONTO_MESH__WAYPOINTPATH_CARTESIANTRIANGULARMESH_MATRIX4X4_CARTESIANAXIS_DOUBLE:
				return projectOntoMesh((WayPointPath)arguments.get(0), (CartesianTriangularMesh)arguments.get(1), (Matrix4x4)arguments.get(2), (CartesianAxis)arguments.get(3), (Double)arguments.get(4));
		}
		return super.eInvoke(operationID, arguments);
	}
	
} //ApogyAddonsGeometryPathsFacadeImpl
