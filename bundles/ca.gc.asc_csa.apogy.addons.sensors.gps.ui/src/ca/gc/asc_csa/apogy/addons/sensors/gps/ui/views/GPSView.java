package ca.gc.asc_csa.apogy.addons.sensors.gps.ui.views;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import ca.gc.asc_csa.apogy.addons.sensors.gps.ui.viewers.GPSViewer;

public class GPSView extends ViewPart implements ISelectionListener {

	public static final String ID = "ca.gc.asc_csa.apogy.addons.sensors.gps.ui.views.GPSView";
//	private IMemento memento;
	private GPSViewer gpsViewer;

	public GPSView() {

	}

	@Override
	public void createPartControl(Composite parent) {
		// We register as a selection listener.
		getSite().getWorkbenchWindow().getSelectionService()
				.addSelectionListener(this);

		parent.setLayout(new GridLayout(1, true));

		gpsViewer = new GPSViewer(parent);

		parent.pack();

	}

	@Override
	public void setFocus() {

	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		gpsViewer.setSelection(selection);
	}

	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		init(site);
//		this.memento = memento;
	}

	@Override
	public void saveState(IMemento memento) {
		// IStructuredSelection sel = (IStructuredSelection) gpsViewer
		// .getSelection();
		// if (sel.isEmpty())
		// return;
		// IMemento selectionMemento = memento.createChild("selection");
		// Iterator<?> iter = sel.iterator();
		// while (iter.hasNext()) {
		// GPS gps = (GPS) iter.next();
		// selectionMemento.createChild("descriptor", gps.getConnection()
		// .toString());
		// }
	}

//	private void restoreState() {
//		IMemento selectionMemento = memento.getChild("selection");
//
//		IMemento gpsMemento = selectionMemento.getChild("descriptor");
//
//		if (gpsMemento != null) {
//			// Set selection to this gps.
//		}
//	}

}
