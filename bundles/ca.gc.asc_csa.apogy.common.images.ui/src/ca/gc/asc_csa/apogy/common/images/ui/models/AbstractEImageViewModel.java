package ca.gc.asc_csa.apogy.common.images.ui.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.spi.model.VFeaturePathDomainModelReference;
import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.emf.ecp.view.spi.model.VViewFactory;
import org.eclipse.emf.ecp.view.spi.model.VViewModelProperties;
import org.eclipse.emf.ecp.view.spi.provider.IViewProvider;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesPackage;
import ca.gc.asc_csa.apogy.common.images.ui.elements.AbstractEImageVElement;

public class AbstractEImageViewModel implements IViewProvider {
	
	private enum PropertyType	
	{
		NONE, READONLY, EDITABLE
	}

	@Override
	public double canProvideViewModel(EObject eObject, VViewModelProperties properties) {
		if (eObject instanceof AbstractEImage) {
			return 10;
		}
		return NOT_APPLICABLE;
	}

	@Override
	public VView provideViewModel(EObject eObject, VViewModelProperties properties) {		
		EClass eClass = eObject.eClass();
		
		VView vView = VViewFactory.eINSTANCE.createView();
		vView.setRootEClass(eClass);
		vView.setVisible(true);
		
		List<VControl> vControls = new ArrayList<>();
		
		// Adds all attributes			
		for(EAttribute attribute : eClass.getEAllAttributes())
		{
			if(attribute != ApogyCommonImagesPackage.Literals.EIMAGE__IMAGE_CONTENT){
				VControl vControl = createVControl(attribute);
				if(vControl != null) vControls.add(vControl);
			}
		}
		
		// Adds all References		
		for(EReference eReference : eClass.getEAllReferences())
		{
			VControl vControl = createVControl(eReference);
			if(vControl != null) vControls.add(vControl);
		}
		
		SortedSet<VControl> sortedVControls = sortVControlAlphabetically(vControls);		
		vView.getChildren().addAll(sortedVControls);
		
		/** Add the element */
		AbstractEImageVElement element = new AbstractEImageVElement();
		vView.getChildren().add(element);
				
		return vView;
	}
	
	/**
	 * Sorts a list of VControl alphabetically.
	 * @param attributes The list of VControl. 
	 * @return The VControl sorted alphabetically.
	 */
	private SortedSet<VControl> sortVControlAlphabetically(List<VControl> vcontrols)
	{
		TreeSet<VControl> treeSet = new TreeSet<VControl>(new Comparator<VControl>() 
		{
			@Override
			public int compare(VControl arg0, VControl arg1) 
			{
				return arg0.getLabel().compareTo(arg1.getLabel());
			}
		});
						
		treeSet.addAll(vcontrols);
		
		return treeSet;
	}
	
	/**
	 * Creates a VControl for a given attribute.
	 * @param attribute The attribute
	 * @return The VCOntrol, null if the attribute GenModel property is NONE.
	 */
	private VControl createVControl(EAttribute attribute)
	{
		VControl vControl = null;		
		String property = getAnnotationDetail(attribute, "property");		
		PropertyType properType = getPropertyType(property);
		switch (properType) 
		{
			case READONLY:
			{
				vControl = VViewFactory.eINSTANCE.createControl();
				vControl.setLabel(attribute.getName());							
				vControl.setReadonly(true);
				
				VFeaturePathDomainModelReference ref1 = VViewFactory.eINSTANCE.createFeaturePathDomainModelReference();
				ref1.setDomainModelEFeature(attribute);
				vControl.setDomainModelReference(ref1);
			}
			break;
			
			case EDITABLE:
			{
				vControl = VViewFactory.eINSTANCE.createControl();
				vControl.setLabel(attribute.getName());

		
				VFeaturePathDomainModelReference ref1 = VViewFactory.eINSTANCE.createFeaturePathDomainModelReference();
				ref1.setDomainModelEFeature(attribute);
				vControl.setDomainModelReference(ref1);
			}
			break;

			default:
			break;
		}		
		
		return vControl;
	}
	
	/**
	 * Creates a VControl for a given reference.
	 * @param eReference The reference
	 * @return The VCOntrol, null if the reference GenModel property is NONE.
	 */
	private VControl createVControl(EReference eReference)
	{
		VControl vControl = null;	
		String property = getAnnotationDetail(eReference, "property");	
		PropertyType properType = getPropertyType(property);
		switch (properType) 
		{
			case READONLY:
			{
				vControl = VViewFactory.eINSTANCE.createControl();
				vControl.setLabel(eReference.getName());							
				vControl.setReadonly(true);
				
				VFeaturePathDomainModelReference ref1 = VViewFactory.eINSTANCE.createFeaturePathDomainModelReference();
				ref1.setDomainModelEFeature(eReference);
				vControl.setDomainModelReference(ref1);
			}
			break;
			
			case EDITABLE:
			{
				vControl = VViewFactory.eINSTANCE.createControl();
				vControl.setLabel(eReference.getName());
		
				VFeaturePathDomainModelReference ref1 = VViewFactory.eINSTANCE.createFeaturePathDomainModelReference();
				ref1.setDomainModelEFeature(eReference);
				vControl.setDomainModelReference(ref1);
			}
			break;

			default:
			break;
		}		
		
		return vControl;
	}
	
	/**
	 * Extracts the GenModel property type from a property string. 
	 * @param propertyString The property string.
	 * @return The property type. If propertyString is null EDITABLE is returned. If the property string is empty, NONE is returned. 
	 */
	private PropertyType getPropertyType(String propertyString)
	{
		if(propertyString == null)
		{
			return PropertyType.EDITABLE;					
		}
		else if(propertyString.contains("Editable"))
		{
			return PropertyType.EDITABLE;	
		}
		else if(propertyString.contains("None"))
		{
			return PropertyType.NONE;
		}
		else if(propertyString.contains("Readonly"))
		{
			return PropertyType.READONLY;
		}
		return PropertyType.NONE;
	}
	
	/**
	 * Returns the string value found in the EAnnotation GenModel for a given
	 * ETypedElement and key.
	 * 
	 * @param eTypedElement
	 *            The eTypedElement.
	 * @param key
	 *            The key of the details in the annotation.
	 * @return The value string found, or null if none could be extracted.
	 */
	protected String getAnnotationDetail(ETypedElement eTypedElement, String key) 
	{
		EAnnotation annotation = eTypedElement.getEAnnotation("http://www.eclipse.org/emf/2002/GenModel");
		if (annotation != null) {
			EMap<String, String> map = annotation.getDetails();
			if (map != null)
				return map.get(key);
		}
		return null;
	}

}
