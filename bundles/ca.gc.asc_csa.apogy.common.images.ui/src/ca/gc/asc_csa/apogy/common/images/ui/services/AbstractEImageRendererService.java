package ca.gc.asc_csa.apogy.common.images.ui.services;

import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VContainedElement;
import org.eclipse.emf.ecp.view.spi.model.VElement;
import org.eclipse.emfforms.spi.swt.core.AbstractSWTRenderer;
import org.eclipse.emfforms.spi.swt.core.di.EMFFormsDIRendererService;

import ca.gc.asc_csa.apogy.common.images.ui.elements.AbstractEImageVElement;
import ca.gc.asc_csa.apogy.common.images.ui.renderers.AbstractEImageVElementSWTRenderer;

public class AbstractEImageRendererService implements EMFFormsDIRendererService<VContainedElement> {
	@Override
	public double isApplicable(VElement vElement, ViewModelContext viewModelContext) {
		if (vElement instanceof AbstractEImageVElement) {
			return 10;
		}
		return NOT_APPLICABLE;
	}

	@Override
	public Class<? extends AbstractSWTRenderer<VContainedElement>> getRendererClass() {
		return AbstractEImageVElementSWTRenderer.class;
	}
}