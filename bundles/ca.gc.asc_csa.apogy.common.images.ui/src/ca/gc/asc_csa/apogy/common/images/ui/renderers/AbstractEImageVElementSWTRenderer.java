/**
 * Copyright (c) 2011-2013 EclipseSource Muenchen GmbH and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eugen Neufeld - initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.images.ui.renderers;

import javax.inject.Inject;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VElement;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers.AbstractCustomElementSWTRenderer;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.ui.elements.AbstractEImageVElement;

public class AbstractEImageVElementSWTRenderer extends AbstractCustomElementSWTRenderer {
	private ImageData imageData;

	@Inject
	public AbstractEImageVElementSWTRenderer(VElement element, ViewModelContext viewModelContext,
			ReportService reportService) {
		super((AbstractEImageVElement) element, viewModelContext, reportService);

		EObject object = viewModelContext.getDomainModel();
		if (object instanceof AbstractEImage) {
			AbstractEImage image = (AbstractEImage) object;
			imageData = EImagesUtilities.INSTANCE.convertToImageData(image.asBufferedImage());
		}
	}

	@Override
	protected Control createControl(Composite parent) {
		Label label = new Label(parent, SWT.None);
		label.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TRANSPARENT));
		if (imageData != null) {
			ImageData displayedImageData = (ImageData) imageData.clone();
			Image image = new Image(Display.getCurrent(), displayedImageData);
			label.setImage(image);
		}

		return label;
	}

	@Override
	protected String getLabelText() {
		return "Image";
	}

}
