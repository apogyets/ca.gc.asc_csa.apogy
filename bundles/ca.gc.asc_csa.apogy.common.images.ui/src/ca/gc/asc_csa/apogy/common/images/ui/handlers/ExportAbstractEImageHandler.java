package ca.gc.asc_csa.apogy.common.images.ui.handlers;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.swt.graphics.ImageData;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.ui.ImagesUiUtilities;

public class ExportAbstractEImageHandler 
{		
	@Execute
	public void execute(@Named("abstracteimage") AbstractEImage image)
	{
		if(image != null)
		{
			ImageData imageData = EImagesUtilities.INSTANCE.convertToImageData(image.asBufferedImage());
			ImagesUiUtilities.export(imageData);		
		}
	}
}
