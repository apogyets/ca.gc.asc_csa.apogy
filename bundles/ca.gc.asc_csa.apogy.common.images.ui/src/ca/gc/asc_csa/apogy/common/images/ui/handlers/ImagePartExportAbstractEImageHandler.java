package ca.gc.asc_csa.apogy.common.images.ui.handlers;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.ui.parts.ImagePart;

public class ImagePartExportAbstractEImageHandler 
{	
	@CanExecute
	public boolean canExecute(MPart part){
		ImagePart imagePart = (ImagePart) part.getObject();						
		AbstractEImage image = imagePart.getAbstractEImage();
		return image != null;
	}
	
	@Execute
	public void execute(MPart part)
	{
		if(part.getObject() instanceof ImagePart)
		{
			ImagePart imagePart = (ImagePart) part.getObject();						
			AbstractEImage image = imagePart.getAbstractEImage();
			
			ExportAbstractEImageHandler handler = new ExportAbstractEImageHandler();
			handler.execute(image);					
		}
	}
}
