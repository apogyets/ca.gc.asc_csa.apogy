/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.geometry.data3d.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.vecmath.Point3d;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DFacade;
import ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DFactory;
import ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianCoordinatesSet;
import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianPositionCoordinates;
import ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler;
import ca.gc.asc_csa.apogy.common.geometry.data3d.VoxelFilterType;
import ca.gc.asc_csa.apogy.common.processors.impl.ProcessorImpl;
import edu.wlu.cs.levy.CG.KDTree;
import edu.wlu.cs.levy.CG.KeyDuplicateException;
import edu.wlu.cs.levy.CG.KeySizeException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Infinite Height Voxel Resampler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.impl.InfiniteHeightVoxelResamplerImpl#getFilterType <em>Filter Type</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.impl.InfiniteHeightVoxelResamplerImpl#getMinimumNumberOfPointPerVoxel <em>Minimum Number Of Point Per Voxel</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.impl.InfiniteHeightVoxelResamplerImpl#getShortRangeLimit <em>Short Range Limit</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.impl.InfiniteHeightVoxelResamplerImpl#getShortRangeResolution <em>Short Range Resolution</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.impl.InfiniteHeightVoxelResamplerImpl#getLongRangeLimit <em>Long Range Limit</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.geometry.data3d.impl.InfiniteHeightVoxelResamplerImpl#getLongRangeResolution <em>Long Range Resolution</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InfiniteHeightVoxelResamplerImpl extends ProcessorImpl<CartesianCoordinatesSet, CartesianCoordinatesSet> implements InfiniteHeightVoxelResampler 
{
	private double[] queryBuffer1;
	private double[] queryBuffer2;
	
	/**
	 * The default value of the '{@link #getFilterType() <em>Filter Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterType()
	 * @generated
	 * @ordered
	 */
	protected static final VoxelFilterType FILTER_TYPE_EDEFAULT = VoxelFilterType.MEDIAN;

	/**
	 * The cached value of the '{@link #getFilterType() <em>Filter Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterType()
	 * @generated
	 * @ordered
	 */
	protected VoxelFilterType filterType = FILTER_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinimumNumberOfPointPerVoxel() <em>Minimum Number Of Point Per Voxel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumNumberOfPointPerVoxel()
	 * @generated
	 * @ordered
	 */
	protected static final int MINIMUM_NUMBER_OF_POINT_PER_VOXEL_EDEFAULT = 2;

	/**
	 * The cached value of the '{@link #getMinimumNumberOfPointPerVoxel() <em>Minimum Number Of Point Per Voxel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumNumberOfPointPerVoxel()
	 * @generated
	 * @ordered
	 */
	protected int minimumNumberOfPointPerVoxel = MINIMUM_NUMBER_OF_POINT_PER_VOXEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getShortRangeLimit() <em>Short Range Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortRangeLimit()
	 * @generated
	 * @ordered
	 */
	protected static final double SHORT_RANGE_LIMIT_EDEFAULT = 4.0;

	/**
	 * The cached value of the '{@link #getShortRangeLimit() <em>Short Range Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortRangeLimit()
	 * @generated
	 * @ordered
	 */
	protected double shortRangeLimit = SHORT_RANGE_LIMIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getShortRangeResolution() <em>Short Range Resolution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortRangeResolution()
	 * @generated
	 * @ordered
	 */
	protected static final double SHORT_RANGE_RESOLUTION_EDEFAULT = 0.05;

	/**
	 * The cached value of the '{@link #getShortRangeResolution() <em>Short Range Resolution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortRangeResolution()
	 * @generated
	 * @ordered
	 */
	protected double shortRangeResolution = SHORT_RANGE_RESOLUTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getLongRangeLimit() <em>Long Range Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongRangeLimit()
	 * @generated
	 * @ordered
	 */
	protected static final double LONG_RANGE_LIMIT_EDEFAULT = 4.0;

	/**
	 * The cached value of the '{@link #getLongRangeLimit() <em>Long Range Limit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongRangeLimit()
	 * @generated
	 * @ordered
	 */
	protected double longRangeLimit = LONG_RANGE_LIMIT_EDEFAULT;

	/**
	 * The default value of the '{@link #getLongRangeResolution() <em>Long Range Resolution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongRangeResolution()
	 * @generated
	 * @ordered
	 */
	protected static final double LONG_RANGE_RESOLUTION_EDEFAULT = 0.25;

	/**
	 * The cached value of the '{@link #getLongRangeResolution() <em>Long Range Resolution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLongRangeResolution()
	 * @generated
	 * @ordered
	 */
	protected double longRangeResolution = LONG_RANGE_RESOLUTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InfiniteHeightVoxelResamplerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonGeometryData3DPackage.Literals.INFINITE_HEIGHT_VOXEL_RESAMPLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * This is specialized for the more specific type known in this context.
	 * @generated
	 */
	@Override
	public void setInput(CartesianCoordinatesSet newInput) {
		super.setInput(newInput);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * This is specialized for the more specific type known in this context.
	 * @generated
	 */
	@Override
	public void setOutput(CartesianCoordinatesSet newOutput) {
		super.setOutput(newOutput);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoxelFilterType getFilterType() {
		return filterType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterType(VoxelFilterType newFilterType) {
		VoxelFilterType oldFilterType = filterType;
		filterType = newFilterType == null ? FILTER_TYPE_EDEFAULT : newFilterType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__FILTER_TYPE, oldFilterType, filterType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinimumNumberOfPointPerVoxel() {
		return minimumNumberOfPointPerVoxel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinimumNumberOfPointPerVoxel(int newMinimumNumberOfPointPerVoxel) {
		int oldMinimumNumberOfPointPerVoxel = minimumNumberOfPointPerVoxel;
		minimumNumberOfPointPerVoxel = newMinimumNumberOfPointPerVoxel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__MINIMUM_NUMBER_OF_POINT_PER_VOXEL, oldMinimumNumberOfPointPerVoxel, minimumNumberOfPointPerVoxel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getShortRangeLimit() {
		return shortRangeLimit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortRangeLimit(double newShortRangeLimit) {
		double oldShortRangeLimit = shortRangeLimit;
		shortRangeLimit = newShortRangeLimit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_LIMIT, oldShortRangeLimit, shortRangeLimit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getShortRangeResolution() {
		return shortRangeResolution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShortRangeResolution(double newShortRangeResolution) {
		double oldShortRangeResolution = shortRangeResolution;
		shortRangeResolution = newShortRangeResolution;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_RESOLUTION, oldShortRangeResolution, shortRangeResolution));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLongRangeLimit() {
		return longRangeLimit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLongRangeLimit(double newLongRangeLimit) {
		double oldLongRangeLimit = longRangeLimit;
		longRangeLimit = newLongRangeLimit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_LIMIT, oldLongRangeLimit, longRangeLimit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getLongRangeResolution() {
		return longRangeResolution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLongRangeResolution(double newLongRangeResolution) {
		double oldLongRangeResolution = longRangeResolution;
		longRangeResolution = newLongRangeResolution;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_RESOLUTION, oldLongRangeResolution, longRangeResolution));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__FILTER_TYPE:
				return getFilterType();
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__MINIMUM_NUMBER_OF_POINT_PER_VOXEL:
				return getMinimumNumberOfPointPerVoxel();
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_LIMIT:
				return getShortRangeLimit();
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_RESOLUTION:
				return getShortRangeResolution();
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_LIMIT:
				return getLongRangeLimit();
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_RESOLUTION:
				return getLongRangeResolution();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__FILTER_TYPE:
				setFilterType((VoxelFilterType)newValue);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__MINIMUM_NUMBER_OF_POINT_PER_VOXEL:
				setMinimumNumberOfPointPerVoxel((Integer)newValue);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_LIMIT:
				setShortRangeLimit((Double)newValue);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_RESOLUTION:
				setShortRangeResolution((Double)newValue);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_LIMIT:
				setLongRangeLimit((Double)newValue);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_RESOLUTION:
				setLongRangeResolution((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__FILTER_TYPE:
				setFilterType(FILTER_TYPE_EDEFAULT);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__MINIMUM_NUMBER_OF_POINT_PER_VOXEL:
				setMinimumNumberOfPointPerVoxel(MINIMUM_NUMBER_OF_POINT_PER_VOXEL_EDEFAULT);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_LIMIT:
				setShortRangeLimit(SHORT_RANGE_LIMIT_EDEFAULT);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_RESOLUTION:
				setShortRangeResolution(SHORT_RANGE_RESOLUTION_EDEFAULT);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_LIMIT:
				setLongRangeLimit(LONG_RANGE_LIMIT_EDEFAULT);
				return;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_RESOLUTION:
				setLongRangeResolution(LONG_RANGE_RESOLUTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__FILTER_TYPE:
				return filterType != FILTER_TYPE_EDEFAULT;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__MINIMUM_NUMBER_OF_POINT_PER_VOXEL:
				return minimumNumberOfPointPerVoxel != MINIMUM_NUMBER_OF_POINT_PER_VOXEL_EDEFAULT;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_LIMIT:
				return shortRangeLimit != SHORT_RANGE_LIMIT_EDEFAULT;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_RESOLUTION:
				return shortRangeResolution != SHORT_RANGE_RESOLUTION_EDEFAULT;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_LIMIT:
				return longRangeLimit != LONG_RANGE_LIMIT_EDEFAULT;
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_RESOLUTION:
				return longRangeResolution != LONG_RANGE_RESOLUTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (filterType: ");
		result.append(filterType);
		result.append(", minimumNumberOfPointPerVoxel: ");
		result.append(minimumNumberOfPointPerVoxel);
		result.append(", shortRangeLimit: ");
		result.append(shortRangeLimit);
		result.append(", shortRangeResolution: ");
		result.append(shortRangeResolution);
		result.append(", longRangeLimit: ");
		result.append(longRangeLimit);
		result.append(", longRangeResolution: ");
		result.append(longRangeResolution);
		result.append(')');
		return result.toString();
	}
	@Override
	public CartesianCoordinatesSet process(CartesianCoordinatesSet input) throws Exception 
	{
		// Create KDTree
		getProgressMonitor().subTask("Creating KDTree...");
		KDTree kdTree = createKDTree(input);
		
		// Filter short range.
		getProgressMonitor().subTask("Filtering short range with limit from <0> to <" + getShortRangeLimit() + "> at resolution <" + getShortRangeResolution() + ">...");
		List<Point3d> shortRange = applyShortRange(kdTree, input);
		
		// Filter long range.
		getProgressMonitor().subTask("Filtering long range with limit from <" + getShortRangeLimit() +"> to <" + getLongRangeLimit() + "> at resolution <" + getLongRangeResolution() + ">...");
		List<Point3d> longRange = applyLongRange(kdTree, input);
		
		List<Point3d> resampled = new ArrayList<Point3d>();
		resampled.addAll(shortRange);
		resampled.addAll(longRange);		
		
		// Create CartesianCoordinatesSet
		CartesianCoordinatesSet output = ApogyCommonGeometryData3DFactory.eINSTANCE.createCartesianCoordinatesSet();
		for(Point3d point : resampled)
		{
			CartesianPositionCoordinates p = ApogyCommonGeometryData3DFacade.INSTANCE.createCartesianPositionCoordinates(point.x, point.y, point.z);
			output.getPoints().add(p);
		}
			
		return output;
	}
	
	protected KDTree createKDTree(CartesianCoordinatesSet input)
	{
		KDTree kdTree = new KDTree(2);
		
		// We build the kd tree.
		int numberOfPoints = input.getPoints().size();
		for(int pid = 0; pid < numberOfPoints; pid++)		
		{			
			CartesianPositionCoordinates point = input.getPoints().get(pid);
			double key[] = new double[] {point.getX(), point.getY()};
			try 
			{
				kdTree.insert(key, pid);
			} 
			catch (KeySizeException e) 
			{
				// e.printStackTrace();
			} 
			catch (KeyDuplicateException e) 
			{
				// We simply log the exception.
				// Activator.logMessage(this, "Duplicate Key", FrameworkLogEntry.WARNING, e);
			}
		}
		
		return kdTree;
		
	}
		
	protected List<Point3d> findPointIdsWithinRadius(KDTree kdTree, CartesianCoordinatesSet input, double radius, Point3d point) 
	{
		List<Point3d> points = new ArrayList<Point3d>();
		
		// The lower bound.
		getQueryBuffer1()[0] = point.getX() - Math.abs(radius);
		getQueryBuffer1()[1] = point.getY() - Math.abs(radius);		

		// The upper bound.
		getQueryBuffer2()[0] = point.getX() + Math.abs(radius);
		getQueryBuffer2()[1] = point.getY() + Math.abs(radius);

		int[] indexes;

		try 
		{
			Object[] range = kdTree.range(getQueryBuffer1(), getQueryBuffer2());

			indexes = new int[range.length];

			for (int i = 0; i < range.length; i++) 
			{
				indexes[i] = (Integer) range[i];
			}
		} 
		catch (KeySizeException e) 
		{
			indexes = new int[0];
			e.printStackTrace();
		}

		for(int i = 0; i < indexes.length; i++)
		{
			points.add(input.getPoints().get(indexes[i]).asPoint3d());
		}
		
		return points;
	}
	
	protected double[] getQueryBuffer1() 
	{
		if (queryBuffer1 == null) {
			queryBuffer1 = new double[2];
		}
		return queryBuffer1;
	}

	protected double[] getQueryBuffer2() 
	{
		if (queryBuffer2 == null) {
			queryBuffer2 = new double[2];
		}
		return queryBuffer2;
	}

	
	protected List<Point3d> applyShortRange(KDTree kdTree, CartesianCoordinatesSet input)
	{				
		List<Point3d> points = new ArrayList<Point3d>();
		
		// Generate voxels grid.
		Set<Point3d> grid = generatePoints(0, getShortRangeLimit(), getShortRangeResolution());
		
		for(Point3d gridPoint : grid)
		{
			// Gets all neighbors
			List<Point3d> neighbors = findPointIdsWithinRadius(kdTree, input, getShortRangeResolution(), gridPoint);
									
			// Filter neighbors
			if(neighbors.size() >= getMinimumNumberOfPointPerVoxel())
			{
				points.add(new Point3d(gridPoint.x, gridPoint.y, applyFilter(neighbors)));
			}			
		}
		
		return points;
	}
	
	protected List<Point3d> applyLongRange(KDTree kdTree, CartesianCoordinatesSet input)
	{
		List<Point3d> points = new ArrayList<Point3d>();
		
		// Generate voxels grid.
		Set<Point3d> grid = generatePoints(getShortRangeLimit(), getLongRangeLimit(), getLongRangeResolution());		
		
		for(Point3d gridPoint : grid)
		{
			// Gets all neighbors
			List<Point3d> neighbors = findPointIdsWithinRadius(kdTree, input, getLongRangeResolution(), gridPoint);
									
			// Filter neighbors
			if(neighbors.size() >= getMinimumNumberOfPointPerVoxel())
			{
				points.add(new Point3d(gridPoint.x, gridPoint.y, applyFilter(neighbors)));
			}			
		}
		
		return points;
	}
	
	protected double applyFilter(Collection<Point3d> pointsInVoxel)
	{
		double result = 0;
		
		switch (getFilterType().getValue()) 
		{
			case VoxelFilterType.AVERAGE_VALUE:
				result = applyAverageFilter(pointsInVoxel);
			break;
			
			case VoxelFilterType.MEDIAN_VALUE:
				result = applyMedianFilter(pointsInVoxel);
			break;

			default:
			break;
		}
		
		return result;
	}
	
	protected double applyAverageFilter(Collection<Point3d> pointsInVoxel)
	{
		double result = 0;
		for(Point3d p : pointsInVoxel)
		{
			result += p.z;
		}
		
		result = result / pointsInVoxel.size();
		
		return result;
	}
	
	protected double applyMedianFilter(Collection<Point3d> pointsInVoxel)
	{		
		double result = 0.0;
		double[] values = new double[pointsInVoxel.size()];
		int i = 0;
		for(Point3d p : pointsInVoxel)
		{
			values[i] = p.z;
			i++;
		}
		
		Arrays.sort(values);
				
		
		if(values.length % 2 == 0)
		{
			int index1 = 0;	
			int index2 = 0;
			if(values.length == 2)
			{
				index1 = 0;
				index2 = 1;
			}
			else
			{
				// The number of points is even, average center values
				index1 = (int) (values.length * 0.5f) - 1;	
				index2 = index1 + 1;				
				result = ((values[index1] + values[index2]) * 0.5);
			}
			
			result = ((values[index1] + values[index2]) * 0.5);
		}
		else
		{
			// The number of points is odd, select center
			int middelIndex = (int) (values.length * 0.5f);		
			result =  values[middelIndex];
		}
		
		return result;
	}
	
	protected Set<Point3d> generatePoints(double minRadius, double maxRadius, double increment)
	{		
		List<Point3d> firstQuadrant = new ArrayList<Point3d>();
		
		Point3d center = new Point3d();
		double x = 0;
		double y = 0;

		// Generate the first quadrant.
		while(x <= maxRadius)
		{					
			y = 0;
			while(y <=  maxRadius)
			{
				Point3d point = new Point3d(x, y, 0);				
				double radius = point.distance(center);
				
				if(minRadius <= radius && radius <= maxRadius)
				{
					firstQuadrant.add(point);
				}								
				
				y += increment;
			}
			
			x += increment;
		}
				
		// Generate quadrant X negative, Y positive
		List<Point3d> secondQuadrant = new ArrayList<Point3d>();
		for(Point3d p : firstQuadrant)
		{
			Point3d point = new Point3d(-p.x, p.y, 0);
			secondQuadrant.add(point);
		}
		
		// Generate quadrant X negative, Y negative
		List<Point3d> thirdQuadrant = new ArrayList<Point3d>();
		for(Point3d p : firstQuadrant)
		{
			Point3d point = new Point3d(-p.x, -p.y, 0);
			thirdQuadrant.add(point);
		}
		
		// Generate quadrant X positive, Y negative
		List<Point3d> fourthQuadrant = new ArrayList<Point3d>();
		for(Point3d p : firstQuadrant)
		{
			Point3d point = new Point3d(p.x, -p.y, 0);
			fourthQuadrant.add(point);
		}
		
		List<Point3d> points = new ArrayList<Point3d>();
		points.addAll(firstQuadrant);
		points.addAll(secondQuadrant);
		points.addAll(thirdQuadrant);
		points.addAll(fourthQuadrant);
				
		// Removes duplicate
		TreeSet<Point3d> set = new TreeSet<Point3d>(new Point3dComparator());
		set.addAll(points);
				
		return set;
	}

	protected class Point3dComparator implements Comparator<Point3d>
	{
		@Override
		public int compare(Point3d o1, Point3d o2) 
		{
			if(o1.distance(o2) == 0)
			{
				return 0;
			}
			else
			{
				if(o1.x != o2.x)
				{
					if(o1.x > o2.x) return 1;
					else return -1;
				}
				else
				{
					if(o1.y != o2.y)
					{
						if (o1.y > o2.y) return 1;
						else return -1;
					}
					else
					{
						if (o1.z > o2.z) return 1;
						else return -1;
					}
				}
			}			
		}		
	}

} //InfiniteHeightVoxelResamplerImpl
