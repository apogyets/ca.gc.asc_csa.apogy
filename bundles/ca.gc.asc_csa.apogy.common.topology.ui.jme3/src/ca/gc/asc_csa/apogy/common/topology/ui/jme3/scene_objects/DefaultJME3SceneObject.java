package ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.vecmath.Point3d;
import javax.vecmath.Tuple3d;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.graphics.RGB;

import com.jme3.app.Application;
import com.jme3.asset.AssetManager;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.bounding.BoundingBox;
import com.jme3.bounding.BoundingSphere;
import com.jme3.bounding.BoundingVolume;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.control.BillboardControl;
import com.jme3.util.BufferUtils;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.ShadowMode;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.Activator;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Application;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3SceneObject;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;

public abstract class DefaultJME3SceneObject<T extends Node> implements JME3SceneObject 
{
	public static final float DEFAULT_FLAG_POLE_HEIGHT = 1.0f;	
	public static final float FLAG_SIZE_PER_PIXEL = 0.005f;
	public static final ColorRGBA FLAG_POLE_COLOR = ColorRGBA.Yellow;
	public static final ColorRGBA FLAG_BACKGROUND_COLOR = ColorRGBA.Black;
	public static final ColorRGBA FLAG_TEXT_COLOR = ColorRGBA.Yellow;

	private T topologyNode;
	private com.jme3.scene.Node root;
	private com.jme3.scene.Node attachmentNode;
	
	protected boolean busy = false;
		
	protected ShadowMode shadowMode = ShadowMode.INHERIT;	
	protected JME3RenderEngineDelegate jme3RenderEngineDelegate;
	protected JME3Application jme3Application = null;
	private AssetManager assetManager;	
	
	// ID Flag.
	private float idPoleHeight = DEFAULT_FLAG_POLE_HEIGHT;	
	private com.jme3.scene.Node pole = null;
	private com.jme3.scene.Node flag = null;	
	private BillboardControl billboardControl;
	private boolean idVisible = false;
	
	public DefaultJME3SceneObject(T topologyNode, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{		
		try
		{
			if (topologyNode == null || jme3RenderEngineDelegate == null) 
			{
				throw new IllegalArgumentException();
			}
	
			this.jme3Application = jme3RenderEngineDelegate.getJME3Application();
			this.topologyNode = topologyNode;				
			this.jme3Application.getAssetManager().registerLocator("/", FileLocator.class);
			this.jme3RenderEngineDelegate = jme3RenderEngineDelegate;
			this.assetManager = jme3Application.getAssetManager();
			
			setVisible(true);
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}
		
		Job job = new Job("DefaultJME3SceneObject : Updating Geometry") 
		{			
			@Override
			protected IStatus run(IProgressMonitor monitor) 
			{
				updateGeometry();					
				return Status.OK_STATUS;
			}
		};
		job.schedule();		
	}

	@Override
	public T getTopologyNode() {
		return topologyNode;
	}

	@Override
	public boolean isVisible() 
	{
		return getRoot().hasChild(getAttachmentNode());
	}

	@Override
	public void setVisible(boolean visible) 
	{
		// Logger.INSTANCE.log(Activator.ID, this, "Setting visibilty to <" + visible + ">...", EventSeverity.INFO);
		
		// Call this on viewer thread.
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				if (!visible && isVisible()) 
				{
					getRoot().detachChild(getAttachmentNode());
				} 
				else if (visible && !isVisible()) 
				{
					getRoot().attachChild(getAttachmentNode());
				}
				
				return null;
			}
		});		
	}

	@Override
	public void setShadowMode(ShadowMode shadowMode) 
	{			
		Logger.INSTANCE.log(Activator.ID, this, "Setting Shadow Mode to <" + shadowMode + ">...", EventSeverity.INFO);
		this.shadowMode = shadowMode;
		
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				if(getRoot() != null)
				{
					getRoot().setShadowMode(JME3Utilities.convertToJMEShadowMode(shadowMode));
				}
				return null;
			}
		});
	}
	
	@Override
	public ShadowMode getShadowMode() 
	{	
		return shadowMode;
	}
	
	@Override
	public Tuple3d getMin() {
		Tuple3d min = null;
		BoundingVolume bounds = getBounds();

		if (bounds instanceof BoundingSphere) {
			BoundingSphere boundingSphere = (BoundingSphere) bounds;
			Vector3f center = boundingSphere.getCenter();

			double[] minValues = new double[3];
			for (int i = 0; i < minValues.length; i++) {
				minValues[i] = center.get(i) - boundingSphere.getRadius();
			}

			min = new Point3d(minValues);
		} else if (bounds instanceof BoundingBox) {
			BoundingBox boundingBox = (BoundingBox) bounds;
			Vector3f vMin = new Vector3f();

			boundingBox.getMin(vMin);
			min = new Point3d(vMin.x, vMin.y, vMin.z);
		}

		return min;
	}

	@Override
	public Tuple3d getMax() {
		Tuple3d max = null;
		BoundingVolume bounds = getBounds();

		if (bounds instanceof BoundingSphere) {
			BoundingSphere boundingSphere = (BoundingSphere) bounds;
			Vector3f center = boundingSphere.getCenter();

			double[] minValues = new double[3];
			for (int i = 0; i < minValues.length; i++) {
				minValues[i] = center.get(i) + boundingSphere.getRadius();
			}

			max = new Point3d(minValues);
		} else if (bounds instanceof BoundingBox) {
			BoundingBox boundingBox = (BoundingBox) bounds;
			Vector3f vMax = new Vector3f();

			boundingBox.getMax(vMax);
			max = new Point3d(vMax.x, vMax.y, vMax.z);
		}

		return max;
	}

	@Override
	public Point3d getCentroid() {
		Vector3f center = getBounds().getCenter();
		return new Point3d(center.x, center.y, center.z);
	}

	@Override
	public com.jme3.scene.Node getRoot() 
	{
		if (root == null) 
		{
			String name = null;			
			if(getTopologyNode() != null && getTopologyNode().getNodeId() != null)
			{
				name = getTopologyNode().getNodeId();
			}
			else
			{
				name = getClass().getSimpleName();
			}			
			name += "_root";
			root = new com.jme3.scene.Node(name);
		}
		return root;
	}

	@Override
	public com.jme3.scene.Node getAttachmentNode() 
	{
		if (attachmentNode == null) 
		{
			String name = null;
			if(getTopologyNode() != null && getTopologyNode().getNodeId() != null)
			{
				name = getTopologyNode().getNodeId();
			}
			else
			{
				name = getClass().getSimpleName();
			}		
			name += "_attachement";
			
			attachmentNode = new com.jme3.scene.Node(name);					
		}
		return attachmentNode;
	}

	@Override
	public List<Geometry> getGeometries() {
		return new ArrayList<Geometry>();
	}

	@Override
	public BoundingVolume getBounds() {

		BoundingVolume bounds = null;

		for (Geometry geometry : getGeometries()) 
		{
			if(geometry != null)
			{
				if (bounds == null) 
				{
					bounds = geometry.getModelBound();
				} 
				else 
				{
					if(geometry.getModelBound() != null)
					{
						bounds.merge(geometry.getModelBound());
					}
				}
			}
		}

		return bounds;
	}

	@Override
	public Application getApplication() 
	{
		return jme3Application;
	}
	
	@Override
	public void dispose() 
	{		
		jme3Application.enqueue(new Callable<Object>() 
		{
				@Override
				public Object call() throws Exception 
				{
					// Logger.INSTANCE.log(Activator.ID, DefaultJME3SceneObject.this, "Being disposed of.", EventSeverity.INFO);				
					if(root != null)
					{
						if(root.getParent() != null)
						{							
							root.getParent().detachChild(root);
						}
						else
						{
							root.detachAllChildren();
						}			
					}
					
					// Clear references.
					topologyNode = null;
					root = null;
					attachmentNode = null;
					
					return null;
				}
		});							
	}
	
	@Override
	public void setIDPoleHeight(float idPoleHeight) 
	{
		if(idPoleHeight > 0)
		{
			this.idPoleHeight = idPoleHeight;
		}
		
	}
	
	@Override
	public void setIDVisible(final boolean visible) 
	{		
		this.idVisible = visible;
		updateGeometry();
	}
		
	
	@Override
	public RGB getColor() {
		return null;
	}

	@Override
	public void setColor(RGB rgb) {
	}

	@Override
	public void objectSelected() {
	}

	@Override
	public void setTransparency(float transparency) {
	}
	
	private void updateGeometry()
	{
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{					
				if(pole != null) getAttachmentNode().detachChild(pole);
				if(flag != null) getAttachmentNode().detachChild(flag);
				
				getBillboardControl().setSpatial(null);
				
				// Creates the pole.
				if(idVisible)
				{
					pole = createPole(idPoleHeight);
					getAttachmentNode().attachChild(pole);
					
					// Creates the flag
					flag = createFlag(getFlagText(), 16);
					pole.attachChild(flag);				
				}
				return null;
			}	
		});		
	}
	
	private com.jme3.scene.Node createPole(float poleHeight)
	{
		List<Vector3f> verticesList = new ArrayList<Vector3f>();
		List<Integer> indexesList = new ArrayList<Integer>();

		Vector3f p0 = new Vector3f(0, 0, 0);
		Vector3f p1 = new Vector3f(0, 0, poleHeight);
		verticesList.add(p0);
		verticesList.add(p1);
		indexesList.add(verticesList.indexOf(p0));
		indexesList.add(verticesList.indexOf(p1));	
		
		Mesh mesh = new Mesh();
		mesh.setMode(Mesh.Mode.Lines);		
		mesh.setBuffer( com.jme3.scene.VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(JME3Utilities.convertToFloatArray(verticesList)));
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Index, 2, BufferUtils.createIntBuffer(JME3Utilities.convertToIntArray(indexesList)));								
		mesh.updateBound();
		mesh.updateCounts();
		
		Geometry poleGeometry = new Geometry("Pole", mesh);
		
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", FLAG_POLE_COLOR.clone());        
        poleGeometry.setMaterial(mat);        
        		
        com.jme3.scene.Node node = new com.jme3.scene.Node("Pole.");
        node.setShadowMode(com.jme3.renderer.queue.RenderQueue.ShadowMode.Off);
        node.attachChild(poleGeometry);
		
		return node;				
	}
	
	private String getFlagText()
	{
		String text = null;
		
		if(getTopologyNode() != null)
		{
			text = getTopologyNode().getNodeId();
		}
		
		if(text == null || text.length() == 0)
		{
			text = "?";
		}
		
		return text;
	}
	
	private com.jme3.scene.Node createFlag(String text, int fontSize)
	{		
		com.jme3.scene.Node node = new com.jme3.scene.Node("Flag");	
		node.setShadowMode(com.jme3.renderer.queue.RenderQueue.ShadowMode.Off);
		
		com.jme3.scene.Node flagAttachmentPoint = new com.jme3.scene.Node("Flag Attachment Point");
		flagAttachmentPoint.setLocalTranslation(0, 0, idPoleHeight);		
		node.attachChild(flagAttachmentPoint);		
	
		// First, create the image that will hold the text.
		Font font = new Font("Serif", Font.BOLD, fontSize);				
		BufferedImage bufferedImage = createTextImage(text, font, 2);
				
		// Based on the image size, create the flag geometry.
		float flagWidth = FLAG_SIZE_PER_PIXEL * bufferedImage.getWidth();
		float flagHeight = FLAG_SIZE_PER_PIXEL * bufferedImage.getHeight();
		Mesh flagMesh = createFlagMesh(flagWidth, flagHeight);
	
		Geometry flagGeometry = new Geometry("Flag Geometry", flagMesh);		
		Material mat = JME3Utilities.createMaterial(bufferedImage, assetManager);         
        flagGeometry.setMaterial(mat);
                
        flagAttachmentPoint.addControl(getBillboardControl());     
        flagAttachmentPoint.attachChild(flagGeometry);	
        
		return node;
	}
	
	private Mesh createFlagMesh(float flagWidth, float flagHeight)
	{
		Vector3f [] vertices = new Vector3f[4];
		vertices[0] = new Vector3f(0, 0, 0);
		vertices[1] = new Vector3f(flagWidth, 0, 0);
		vertices[2] = new Vector3f(0, flagHeight, 0);	
		vertices[3] = new Vector3f(flagWidth, flagHeight, 0);		
		
		int [] indexes = { 2,0,1, 1,3,2 };
									
		Vector2f[] texCoord = new Vector2f[4];
		texCoord[0] = new Vector2f(0,0);
		texCoord[1] = new Vector2f(1,0);
		texCoord[2] = new Vector2f(0,1);
		texCoord[3] = new Vector2f(1,1);
		
		Mesh mesh = new Mesh();		
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Index,    3, BufferUtils.createIntBuffer(indexes));									
		mesh.updateBound();
		mesh.updateCounts();
		
		return mesh;
	}
	
	private BillboardControl getBillboardControl()
	{
		if(billboardControl == null)
		{
			billboardControl = new BillboardControl();			
			billboardControl.setAlignment(BillboardControl.Alignment.Screen);
		}
		
		return billboardControl;
	}
	
	private BufferedImage createTextImage(String text, Font font, int borderWidth) 
	{			
		Color textColor = JME3Utilities.convertToAWTColor(FLAG_TEXT_COLOR);
		Color backgroundColor = JME3Utilities.convertToAWTColor(FLAG_BACKGROUND_COLOR);
		AbstractEImage original = EImagesUtilities.INSTANCE.createTextImage(text, font, textColor, backgroundColor, borderWidth);
		
		int[] borderColor = JME3Utilities.convertToColorIntRGBA(FLAG_POLE_COLOR);         
        AbstractEImage imageWithBorder = EImagesUtilities.INSTANCE.addBorder(original, borderWidth, borderColor[0], borderColor[1], borderColor[2]);
                   
        return imageWithBorder.asBufferedImage();
    }
}
