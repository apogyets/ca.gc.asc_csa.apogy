package ca.gc.asc_csa.apogy.core.environment.ui.composites;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.emf.databinding.EMFProperties;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerSupport;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import ca.gc.asc_csa.apogy.common.databinding.converters.DateToStringConverter;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.ui.ApogyCoreEnvironmentUIRCPConstants;

@SuppressWarnings("restriction")
public class TimeSourceSelectionComposite extends Composite {
	
	private final String IMAGE_PATH = "icons/time_sources.gif";
	Image timeSourceImage;
	
	protected DataBindingContext m_bindingContext;
	protected ApogyEnvironment apogyEnvironment;
	protected Label currentTimeValueLabel;
	protected ComboViewer activeTimeSourceCombo;
	
	protected Button toggleTimeSourcePartButton;
	
	public TimeSourceSelectionComposite(Composite parent, int style, EHandlerService handlerService, ECommandService commandService) {
		super(parent, style);
		setLayout( new GridLayout(5, false));
		addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) {
					m_bindingContext.dispose();
				}
				if(timeSourceImage != null){
					timeSourceImage.dispose();
				}
			}
		});
		
		ImageDescriptor imageDescriptor = AbstractUIPlugin.imageDescriptorFromPlugin(Activator.ID, IMAGE_PATH);
		if(imageDescriptor != null){
			timeSourceImage = imageDescriptor.createImage();
		}

		toggleTimeSourcePartButton = new Button(this, SWT.TOGGLE);
		toggleTimeSourcePartButton.setImage(timeSourceImage);
		toggleTimeSourcePartButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Map<String, Object> map = new HashMap<>();
				map.put(ApogyCoreEnvironmentUIRCPConstants.COMMAND_PARAMETER__TOGGLE_ACTIVE_TIME_SOURCE__SHOW, Boolean.toString(toggleTimeSourcePartButton.getSelection()));
				ParameterizedCommand command = commandService.createCommand(ApogyCoreEnvironmentUIRCPConstants.COMMAND__TOGGLE_ACTIVE_TIME_SOURCE_PART__ID,
						map);
				handlerService.executeHandler(command);
			}
		});
	
		Label activeTimeSourceLabel = new Label(this, SWT.NONE);
		activeTimeSourceLabel.setText("Time Source :");
		activeTimeSourceLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		activeTimeSourceCombo = new ComboViewer(this, SWT.NONE);
		GridData gd_comboContext = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_comboContext.widthHint = 250;
		gd_comboContext.minimumWidth = 250;
		activeTimeSourceCombo.getCombo().setLayoutData(gd_comboContext);

		Label currentTimeLabel = new Label(this, SWT.NONE);
		currentTimeLabel.setText("Current Time : ");
		currentTimeLabel.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		currentTimeValueLabel = new Label(this, SWT.None);
		currentTimeValueLabel.setText("?");
		GridData currentTimeValueLabelGridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		currentTimeValueLabelGridData.widthHint = 50;
		currentTimeValueLabelGridData.minimumWidth = 250;
		currentTimeValueLabel.setLayoutData(currentTimeValueLabelGridData);

		initDataBindingsCustom();
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}
	
	public void setToggleTimeSourcePartButtonSelection(boolean selected){
		toggleTimeSourcePartButton.setSelection(selected);
	}

	@SuppressWarnings("unchecked")
	protected DataBindingContext initDataBindingsCustom() {
		DataBindingContext m_bindingContext = new DataBindingContext();

		/**
		 * Bind ApogyEnvironment current time to currentTimeLabel.
		 */
		IObservableValue<?> observeCurrentTimeLabelTextValue = WidgetProperties.text().observe(currentTimeValueLabel);
		IObservableValue<?> currentTimeObserveValue = EMFProperties.value(FeaturePath.fromList(
				ApogyCoreEnvironmentPackage.Literals.APOGY_CORE_ENVIRONMENT_FACADE__ACTIVE_APOGY_ENVIRONMENT,
				ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__ACTIVE_TIME_SOURCE,
				ApogyCommonEMFPackage.Literals.TIMED__TIME)).observe(ApogyCoreEnvironmentFacade.INSTANCE);
		m_bindingContext.bindValue(observeCurrentTimeLabelTextValue, currentTimeObserveValue,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE).setConverter(new DateToStringConverter(
						new SimpleDateFormat(ApogyCommonEMFFacade.INSTANCE.getDateFormatString()))));

		/** Data binding to get the name of the TimeSources for the combo box */
		IObservableList<?> invocatorFacadeEnvironmentContextsListContextsObserveValue = EMFProperties
				.list(FeaturePath.fromList(
						ApogyCoreEnvironmentPackage.Literals.APOGY_CORE_ENVIRONMENT_FACADE__ACTIVE_APOGY_ENVIRONMENT,
						ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__TIME_SOURCES_LIST,
						ApogyCoreEnvironmentPackage.Literals.TIME_SOURCES_LIST__TIME_SOURCES))
				.observe(ApogyCoreEnvironmentFacade.INSTANCE);		
		ViewerSupport.bind(activeTimeSourceCombo, invocatorFacadeEnvironmentContextsListContextsObserveValue,
				EMFProperties.value(ApogyCommonEMFPackage.Literals.NAMED__NAME));

		/**
		 * Data binding to set the value of the combo box to the active
		 * timeSource
		 */
		IObservableValue<?> observeComboContextSingleSelectionIndexObserveWidget = WidgetProperties
				.singleSelectionIndex().observe(activeTimeSourceCombo.getCombo());
		IObservableValue<?> invocatorFacadeEnvironmentActiveContextObserveValue = EMFEditProperties
				.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(),
						FeaturePath.fromList(
								ApogyCoreEnvironmentPackage.Literals.APOGY_CORE_ENVIRONMENT_FACADE__ACTIVE_APOGY_ENVIRONMENT,
								ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__ACTIVE_TIME_SOURCE))
				.observe(ApogyCoreEnvironmentFacade.INSTANCE);

		m_bindingContext.bindValue(observeComboContextSingleSelectionIndexObserveWidget,
				invocatorFacadeEnvironmentActiveContextObserveValue,
				new UpdateValueStrategy().setConverter(new Converter(Integer.class, TimeSource.class) {

					@Override
					public Object convert(Object fromObject) {
						if (fromObject != null && (Integer) fromObject != -1
								&& ApogyCoreEnvironmentFacade.INSTANCE.getActiveApogyEnvironment() != null) {
							if (!ApogyCoreEnvironmentFacade.INSTANCE.getActiveApogyEnvironment().getTimeSourcesList()
									.getTimeSources().get((Integer) fromObject).equals(null)) {
								return ApogyCoreEnvironmentFacade.INSTANCE.getActiveApogyEnvironment()
										.getTimeSourcesList().getTimeSources().get((Integer) fromObject);
							}
						}
						return null;
					}

				}),

				new UpdateValueStrategy().setConverter(new Converter(TimeSource.class, Integer.class) {
					@Override
					public Object convert(Object arg0) {
						for (int i = 0; i < activeTimeSourceCombo.getCombo().getItemCount(); i++) {
							if (arg0 != null && ((TimeSource) arg0).getName() != null && ((TimeSource) arg0).getName()
									.equals(activeTimeSourceCombo.getCombo().getItem(i))) {
								return i;
							}
						}
						return -1;
					}
				}));
		
		/**
		 * Bind Combo Box Enabled.
		 */
		IObservableValue<?> observeEnabledComboWidget = WidgetProperties.enabled()
				.observe(activeTimeSourceCombo.getCombo());
		m_bindingContext.bindValue(observeEnabledComboWidget, invocatorFacadeEnvironmentActiveContextObserveValue, null,
				new UpdateValueStrategy().setConverter(new Converter(TimeSource.class, Boolean.class) {
					@Override
					public Object convert(Object fromObject) {
						return ApogyCoreEnvironmentFacade.INSTANCE.getActiveApogyEnvironment() != null;
					}
				}));

		return m_bindingContext;
	}
}
