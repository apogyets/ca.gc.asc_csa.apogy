package ca.gc.asc_csa.apogy.core.environment.ui.toolcontrols;
/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.llarouche@canada.ca,
 *     Canadian Space Agency (CSA) - Initial API and implementation
 **/

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.environment.ui.composites.TimeSourceSelectionComposite;

@SuppressWarnings("restriction")
public class TimeSourceToolControl {
	
	private Composite composite;

	@Inject
	ECommandService commandService;
	
	@Inject
	EHandlerService handlerService;

	@PostConstruct
	public void createControls(Composite parent) {
		composite = new TimeSourceSelectionComposite(parent, SWT.None, handlerService, commandService);
	}

	/**
	 * Disposes the actual parentComposite.
	 */
	@PreDestroy
	public void dispose() {
		composite.dispose();
	}
	
	public Composite getComposite() {
		return composite;
	}

}
