package ca.gc.asc_csa.apogy.core.environment.ui.parts;

import java.util.HashMap;

import javax.inject.Inject;

import org.eclipse.core.commands.ParameterizedCommand;
import org.eclipse.e4.core.commands.ECommandService;
import org.eclipse.e4.core.commands.EHandlerService;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource;
import ca.gc.asc_csa.apogy.common.ui.composites.ClosableNoContentComposite;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.ui.ApogyCoreEnvironmentUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.ui.composites.ActiveTimeSourceComposite;

@SuppressWarnings("restriction")
public class ActiveTimeSourcePart extends AbstractApogyEnvironmentBasedPart {

	private ApogyEnvironment environment;
	private Adapter activeTimeSourceAdapter;
	private ActiveTimeSourceComposite ActiveTimeSourceComposite;

	@Inject
	EHandlerService handlerService;

	@Inject
	ECommandService commandService;

	@Override
	protected void setCompositeContent(EObject eObject) {
		if (this.environment != eObject) {
			newEnvironment((ApogyEnvironment) eObject);
		}

		if (ActiveTimeSourceComposite == null) {
			createContentComposite(getActualComposite().getParent().getParent(), SWT.None);
		}
		ActiveTimeSourceComposite.setTimeSource(((ApogyEnvironment) eObject).getActiveTimeSource());
	}

	@Override
	protected void newEnvironment(ApogyEnvironment environment) {
		if (this.environment != null) {
			this.environment.eAdapters().remove(getActiveTimeSourceAdapter());
		}

		this.environment = environment;

		if (this.environment != null) {
			this.environment.eAdapters().add(getActiveTimeSourceAdapter());
		}
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		Composite composite = new Composite(parent, SWT.BORDER);
		composite.setLayout(new GridLayout(1, false));

		Button btnClose = new Button(composite, SWT.None);
		ImageDescriptor image = AbstractUIPlugin.imageDescriptorFromPlugin("org.eclipse.ui",
				"icons/full/elcl16/close_view.png");
		btnClose.setImage(image.createImage());
		btnClose.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, false, 1, 1));
		btnClose.addSelectionListener(getListener());
		ActiveTimeSourceComposite = new ActiveTimeSourceComposite(composite, SWT.None);
		ActiveTimeSourceComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		if (environment != null) {
			ActiveTimeSourceComposite.setTimeSource(environment.getActiveTimeSource());
		}
	}

	private Adapter getActiveTimeSourceAdapter() {
		if (activeTimeSourceAdapter == null) {
			activeTimeSourceAdapter = new AdapterImpl() {
				@Override
				public void notifyChanged(Notification notification) {
					if (notification
							.getFeature() == ApogyCoreEnvironmentPackage.Literals.APOGY_ENVIRONMENT__ACTIVE_TIME_SOURCE) {
						if (notification.getNewValue() != null) {
							ActiveTimeSourceComposite.setTimeSource((TimeSource) notification.getNewValue());
							if (notification.getNewValue() instanceof SelectionBasedTimeSource) {
								((SelectionBasedTimeSource) notification.getNewValue())
										.setSelectionService(selectionService);
							}
						} else {
							ActiveTimeSourceComposite.setTimeSource(null);
						}
					}
				}
			};
		}
		return activeTimeSourceAdapter;
	}

	@Override
	protected void createNoContentComposite(Composite parent, int style) {
		if (ActiveTimeSourceComposite != null) {
			ActiveTimeSourceComposite.dispose();
		}

		new ClosableNoContentComposite(parent, SWT.BORDER) {
			@Override
			protected String getMessage() {
				return "No active environment";
			}

			@Override
			protected SelectionListener getSelectionListener() {
				return ActiveTimeSourcePart.this.getListener();
			}
		};
	}

	private SelectionListener getListener() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				HashMap<String, Object> parameters = new HashMap<>(1);
				parameters.put(ApogyCoreEnvironmentUIRCPConstants.COMMAND_PARAMETER__TOGGLE_ACTIVE_TIME_SOURCE__SHOW,
						"false");

				ParameterizedCommand command = commandService.createCommand(
						ApogyCoreEnvironmentUIRCPConstants.COMMAND__TOGGLE_ACTIVE_TIME_SOURCE_PART__ID, parameters);
				// Execute the command
				handlerService.executeHandler(command);
			}
		};
	}

	@Override
	protected void dispose() {
		if (this.environment != null) {
			this.environment.eAdapters().remove(getActiveTimeSourceAdapter());
		}
		super.dispose();
	}

}
