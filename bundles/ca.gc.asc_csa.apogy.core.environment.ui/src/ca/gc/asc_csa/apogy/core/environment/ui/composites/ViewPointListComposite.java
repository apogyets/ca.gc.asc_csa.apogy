package ca.gc.asc_csa.apogy.core.environment.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.ApogyCoreEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.ViewPointList;
import ca.gc.asc_csa.apogy.core.environment.ui.Activator;

public class ViewPointListComposite extends Composite 
{
	private ViewPointList viewPointList;
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private DataBindingContext m_bindingContext;
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	public ViewPointListComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new ViewPointContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newViewPointSelected((AbstractViewPoint)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		// Buttons.
		Composite compositeButtons = new Composite(this, SWT.NONE);
		compositeButtons.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		compositeButtons.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(compositeButtons, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{					
					MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();
					settings.getUserDataMap().put("name", ApogyCommonEMFFacade.INSTANCE.getDefaultName(getViewPointList(), null, ApogyCoreEnvironmentPackage.Literals.VIEW_POINT_LIST__VIEW_POINTS));					// 
										
					Wizard wizard = new ApogyEObjectWizard(ApogyCoreEnvironmentPackage.Literals.VIEW_POINT_LIST__VIEW_POINTS, getViewPointList(), settings, ApogyCommonTopologyPackage.Literals.ABSTRACT_VIEW_POINT);								
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getViewPointList());
					}
				}
			}
		});
				
		btnDelete = new Button(compositeButtons, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String viewPointsToDeleteMessage = "";

				Iterator<AbstractViewPoint> viewPointsToDelete = getSelectedViewPoints().iterator();
				while (viewPointsToDelete.hasNext()) 
				{
					AbstractViewPoint viewPoint = viewPointsToDelete.next();
					viewPointsToDeleteMessage = viewPointsToDeleteMessage + viewPoint.getName();

					if (viewPointsToDelete.hasNext()) 
					{
						viewPointsToDeleteMessage = viewPointsToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected View Points", null,
						"Are you sure to delete these View Points: " + viewPointsToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (AbstractViewPoint viewPoint :  getSelectedViewPoints()) 
					{
						try 
						{
							ApogyCommonTransactionFacade.INSTANCE.basicRemove(viewPoint.eContainer(), ApogyCoreEnvironmentPackage.Literals.VIEW_POINT_LIST__VIEW_POINTS, viewPoint);
						} 
						catch (Exception e)	
						{
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the View Points <"+ viewPoint.getName() + ">",
									EventSeverity.ERROR, e);
						}
					}
				}
				
				// Forces the viewer to refresh its input.
				if(!treeViewer.isBusy())
				{					
					treeViewer.setInput(getViewPointList());
				}					
			}
		});
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	public ViewPointList getViewPointList() {
		return viewPointList;
	}

	public void setViewPointList(ViewPointList viewPointList) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.viewPointList = viewPointList;
		if(viewPointList != null)
		{
			m_bindingContext = customInitDataBindings();
		}
		
		treeViewer.setInput(viewPointList);
	}

	public void setSelectedViewPoint(AbstractViewPoint viewPoint)
	{
		if(viewPoint != null)
		{
			treeViewer.setSelection(new StructuredSelection(viewPoint), true);
		}
		else
		{			
			treeViewer.setSelection(null);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<AbstractViewPoint> getSelectedViewPoints()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	protected void newViewPointSelected(AbstractViewPoint viewPoint)
	{		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
			
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) 
							{
								return (fromObject != null) && (getViewPointList().getViewPoints().size() > 0);
							}
						}));
		
		return bindingContext;
	}
	
	private class ViewPointContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() {						
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{
		}

		@Override
		public Object[] getElements(Object inputElement) 
		{
			if(inputElement instanceof ViewPointList)
			{
				return 	((ViewPointList) inputElement).getViewPoints().toArray();				
			}
			else
			{
				return null;
			}
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{
			return false;
		}	
	}
}
