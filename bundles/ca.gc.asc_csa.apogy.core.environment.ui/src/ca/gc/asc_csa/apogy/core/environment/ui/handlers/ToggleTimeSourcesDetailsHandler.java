package ca.gc.asc_csa.apogy.core.environment.ui.handlers;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.advanced.MPlaceholder;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.menu.MToolControl;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

import ca.gc.asc_csa.apogy.core.environment.ui.ApogyCoreEnvironmentUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.ui.composites.TimeSourceSelectionComposite;
import ca.gc.asc_csa.apogy.core.environment.ui.toolcontrols.TimeSourceToolControl;

public class ToggleTimeSourcesDetailsHandler {
	
	@Inject
	EModelService modelService;
	
	@Execute
	public void execute(
			@Named(ApogyCoreEnvironmentUIRCPConstants.COMMAND_PARAMETER__TOGGLE_ACTIVE_TIME_SOURCE__SHOW) String show,
			MApplication application) {
		MWindow window = (MWindow) modelService.find(ApogyCoreEnvironmentUIRCPConstants.MAIN_WINDOW__ID, application);

		/** Find the placeholder */
		MPlaceholder placeholder = (MPlaceholder) modelService
				.find(ApogyCoreEnvironmentUIRCPConstants.PLACEHOLDER__ACTIVE_TIME_SOURCE__ID, window);

		/** Show/hide the placeholder */
		boolean showBoolean = Boolean.parseBoolean(show);
		placeholder.setVisible(showBoolean);
		placeholder.setToBeRendered(showBoolean);

		/** Notify the part that will notify the composite */
		MToolControl toolControl = (MToolControl) modelService
				.find(ApogyCoreEnvironmentUIRCPConstants.TOOL_CONTROL__TIME_SOURCES__ID, window);
		((TimeSourceSelectionComposite) ((TimeSourceToolControl) toolControl.getObject()).getComposite())
				.setToggleTimeSourcePartButtonSelection(showBoolean);
	}

	@CanExecute
	public boolean canExecute(MApplication application) {
		MWindow window = (MWindow) modelService.find(ApogyCoreEnvironmentUIRCPConstants.MAIN_WINDOW__ID, application);

		if (window != null && modelService.find(ApogyCoreEnvironmentUIRCPConstants.PLACEHOLDER__ACTIVE_TIME_SOURCE__ID,
				window) != null) {
			return true;
		}
		return false;
	}
}
