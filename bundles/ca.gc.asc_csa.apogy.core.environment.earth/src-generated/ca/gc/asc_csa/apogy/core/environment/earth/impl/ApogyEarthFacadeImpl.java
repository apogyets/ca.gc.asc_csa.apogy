/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.core.ApogySystem;
import ca.gc.asc_csa.apogy.core.environment.ApogyEnvironment;
import ca.gc.asc_csa.apogy.core.environment.Moon;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthFacade;
import ca.gc.asc_csa.apogy.core.environment.earth.EarthSurfaceLocation;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.invocator.Environment;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Apogy
 * Earth Facade</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public class ApogyEarthFacadeImpl extends MinimalEObjectImpl.Container implements ApogyEarthFacade 
{
	private static ApogyEarthFacade instance = null;

	public static ApogyEarthFacade getInstance() {
		if (instance == null) {
			instance = new ApogyEarthFacadeImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogyEarthFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyEarthEnvironmentPackage.Literals.APOGY_EARTH_FACADE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public Tuple3d getMoonVector(Node node, Environment environment) 
	{
		if (environment instanceof ApogyEnvironment) 
		{
			ApogyEnvironment apogyEnvironment = (ApogyEnvironment) environment;

			// Search the active topology.			
			Node universeRoot = null;
			EList<Node> moons = ApogyCommonTopologyFacade.INSTANCE.findNodesByID(Moon.NODE_ID, universeRoot);
			
			if(moons.size() > 0)
			{
				Node moon = moons.get(0);
	
				Matrix4d matrix = ApogyCommonTopologyFacade.INSTANCE.expressInFrame(moon, node);
				Vector3d v = new Vector3d();
	
				matrix.get(v);
				v.normalize();
	
				return ApogyCommonMathFacade.INSTANCE.createTuple3d(v);
			}
			else
			{
				return null;
			}
		}

		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public GeographicCoordinates createGeographicCoordinates(double longitude, double latitude, double altitude) 
	{
		GeographicCoordinates coord = ApogyEarthEnvironmentFactory.eINSTANCE.createGeographicCoordinates();
		coord.setLongitude(longitude);
		coord.setLatitude(latitude);
		coord.setElevation(altitude);

		return coord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public EarthSurfaceLocation createEarthSurfaceLocation(String name, String description, double longitude, double latitude, double elevation) 
	{
		EarthSurfaceLocation loc = ApogyEarthEnvironmentFactory.eINSTANCE.createEarthSurfaceLocation();
		loc.setName(name);
		loc.setDescription(description);
		
		loc.setLatitude(latitude);
		loc.setLongitude(longitude);
		loc.setElevation(elevation);	
		
		return loc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public List<GeographicCoordinates> loadGeographicCoordinatesFromURL(String urlString) throws Exception 
	{
		List<GeographicCoordinates> coords = new ArrayList<GeographicCoordinates>();
		
		URL url = new URL(urlString);
		File tempFile = copyURLContent(url);
		BufferedReader reader = new BufferedReader(new FileReader(tempFile));

		String line = null;
		while ((line = reader.readLine()) != null) 
		{
			line = line.trim();
			GeographicCoordinates coord = parserCSVLine(line);
			coords.add(coord);
		}

		reader.close();

		
		return coords;
	}

	@Override
	public Tuple3d getMoonVector(ApogySystem apogySystem, String nodeID, Environment environment) {
		EList<Node> nodes = ApogyCommonTopologyFacade.INSTANCE.findNodesByID(nodeID,
				apogySystem.getTopologyRoot().getOriginNode());
		if (!nodes.isEmpty()) {
			Node node = nodes.get(0);
			return getMoonVector(node, environment);
		} else {
			return null;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyEarthEnvironmentPackage.APOGY_EARTH_FACADE___GET_MOON_VECTOR__APOGYSYSTEM_STRING_ENVIRONMENT:
				return getMoonVector((ApogySystem)arguments.get(0), (String)arguments.get(1), (Environment)arguments.get(2));
			case ApogyEarthEnvironmentPackage.APOGY_EARTH_FACADE___GET_MOON_VECTOR__NODE_ENVIRONMENT:
				return getMoonVector((Node)arguments.get(0), (Environment)arguments.get(1));
			case ApogyEarthEnvironmentPackage.APOGY_EARTH_FACADE___CREATE_GEOGRAPHIC_COORDINATES__DOUBLE_DOUBLE_DOUBLE:
				return createGeographicCoordinates((Double)arguments.get(0), (Double)arguments.get(1), (Double)arguments.get(2));
			case ApogyEarthEnvironmentPackage.APOGY_EARTH_FACADE___CREATE_EARTH_SURFACE_LOCATION__STRING_STRING_DOUBLE_DOUBLE_DOUBLE:
				return createEarthSurfaceLocation((String)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2), (Double)arguments.get(3), (Double)arguments.get(4));
			case ApogyEarthEnvironmentPackage.APOGY_EARTH_FACADE___LOAD_GEOGRAPHIC_COORDINATES_FROM_URL__STRING:
				try {
					return loadGeographicCoordinatesFromURL((String)arguments.get(0));
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
		}
		return super.eInvoke(operationID, arguments);
	}

	private Moon getActiveMoon()
	{
		return null;
	}
	
	private File copyURLContent(URL url) throws Exception {
		File tempFile = null;

		String fileName = getFileName(url);
		String fileExtension = getFileExtension(url);

		tempFile = File.createTempFile(fileName, fileExtension);

		url.openConnection();
		InputStream reader = url.openStream();

		FileOutputStream writer = new FileOutputStream(tempFile);
		byte[] buffer = new byte[153600];
		int bytesRead = 0;
		while ((bytesRead = reader.read(buffer)) > 0) {
			writer.write(buffer, 0, bytesRead);
			buffer = new byte[153600];
		}
		writer.close();
		reader.close();

		if (tempFile != null) {
			tempFile.deleteOnExit();
		}

		return tempFile;
	}
	
	private String getFileName(URL url) {
		String fileName = url.getFile();

		int startIndex = fileName.lastIndexOf(File.separator);
		int endIndex = fileName.lastIndexOf(".");
		if (startIndex > 0 && endIndex > 0) {
			fileName = fileName.substring(startIndex + 1, endIndex);
		}

		return fileName;
	}

	private String getFileExtension(URL url) {
		String fileExtension = url.getFile();

		int index = fileExtension.lastIndexOf(".");
		if (index > 0) {
			fileExtension = fileExtension.substring(index);
		}

		return fileExtension;
	}
	
	private GeographicCoordinates parserCSVLine(String line) throws Exception {
		GeographicCoordinates coords = null;

		String[] entries = line.split(",");

		if (entries.length < 3) {
			throw new Exception("Line <" + line + "> contains too few entries !");
		}
		
		double longitude = Math.toRadians(Double.parseDouble(entries[0]));
		double lattitude = Math.toRadians(Double.parseDouble(entries[1]));
		double altitude = Double.parseDouble(entries[2]);

		coords = ApogyEarthEnvironmentFactory.eINSTANCE.createGeographicCoordinates();
		coords.setElevation(altitude);
		coords.setLatitude(lattitude);
		coords.setLongitude(longitude);
		
		return coords;
	}

} // ApogyEarthFacadeImpl
