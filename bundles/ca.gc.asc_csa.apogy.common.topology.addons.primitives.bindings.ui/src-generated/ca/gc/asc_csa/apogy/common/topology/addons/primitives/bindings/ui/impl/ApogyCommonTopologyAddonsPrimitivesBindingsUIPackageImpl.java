/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.LightEnablementBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.PointLightBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.SpotLightBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl extends EPackageImpl implements ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass lightEnablementBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pointLightBindingWizardPagesProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass spotLightBindingWizardPagesProviderEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl() {
		super(eNS_URI, ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage init() {
		if (isInited) return (ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl theApogyCommonTopologyAddonsPrimitivesBindingsUIPackage = (ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonTopologyBindingsUIPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.eNS_URI, theApogyCommonTopologyAddonsPrimitivesBindingsUIPackage);
		return theApogyCommonTopologyAddonsPrimitivesBindingsUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLightEnablementBindingWizardPagesProvider() {
		return lightEnablementBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPointLightBindingWizardPagesProvider() {
		return pointLightBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSpotLightBindingWizardPagesProvider() {
		return spotLightBindingWizardPagesProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory getApogyCommonTopologyAddonsPrimitivesBindingsUIFactory() {
		return (ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		lightEnablementBindingWizardPagesProviderEClass = createEClass(LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER);

		pointLightBindingWizardPagesProviderEClass = createEClass(POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER);

		spotLightBindingWizardPagesProviderEClass = createEClass(SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonTopologyBindingsUIPackage theApogyCommonTopologyBindingsUIPackage = (ApogyCommonTopologyBindingsUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyBindingsUIPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		lightEnablementBindingWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyBindingsUIPackage.getAbstractTopologyBindingWizardPagesProvider());
		pointLightBindingWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyBindingsUIPackage.getAbstractTopologyBindingWizardPagesProvider());
		spotLightBindingWizardPagesProviderEClass.getESuperTypes().add(theApogyCommonTopologyBindingsUIPackage.getAbstractTopologyBindingWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(lightEnablementBindingWizardPagesProviderEClass, LightEnablementBindingWizardPagesProvider.class, "LightEnablementBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pointLightBindingWizardPagesProviderEClass, PointLightBindingWizardPagesProvider.class, "PointLightBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(spotLightBindingWizardPagesProviderEClass, SpotLightBindingWizardPagesProvider.class, "SpotLightBindingWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl
