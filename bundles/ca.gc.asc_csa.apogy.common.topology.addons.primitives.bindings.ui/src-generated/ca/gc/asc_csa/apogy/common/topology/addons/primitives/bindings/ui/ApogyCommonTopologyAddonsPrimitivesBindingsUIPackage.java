/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCommonTopologyAddonsPrimitivesBindingsUI' copyrightText='Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Canadian Space Agency (CSA) - Initial API and implementation' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' modelName='ApogyCommonTopologyAddonsPrimitivesBindingsUI' complianceLevel='6.0' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings'"
 * @generated
 */
public interface ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage eINSTANCE = ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.LightEnablementBindingWizardPagesProviderImpl <em>Light Enablement Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.LightEnablementBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl#getLightEnablementBindingWizardPagesProvider()
	 * @generated
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER = 0;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Light Enablement Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Light Enablement Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.PointLightBindingWizardPagesProviderImpl <em>Point Light Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.PointLightBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl#getPointLightBindingWizardPagesProvider()
	 * @generated
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER = 1;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Point Light Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Point Light Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.SpotLightBindingWizardPagesProviderImpl <em>Spot Light Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.SpotLightBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl#getSpotLightBindingWizardPagesProvider()
	 * @generated
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER = 2;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Spot Light Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Spot Light Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.LightEnablementBindingWizardPagesProvider <em>Light Enablement Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Light Enablement Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.LightEnablementBindingWizardPagesProvider
	 * @generated
	 */
	EClass getLightEnablementBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.PointLightBindingWizardPagesProvider <em>Point Light Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point Light Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.PointLightBindingWizardPagesProvider
	 * @generated
	 */
	EClass getPointLightBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.SpotLightBindingWizardPagesProvider <em>Spot Light Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Spot Light Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.SpotLightBindingWizardPagesProvider
	 * @generated
	 */
	EClass getSpotLightBindingWizardPagesProvider();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCommonTopologyAddonsPrimitivesBindingsUIFactory getApogyCommonTopologyAddonsPrimitivesBindingsUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.LightEnablementBindingWizardPagesProviderImpl <em>Light Enablement Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.LightEnablementBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl#getLightEnablementBindingWizardPagesProvider()
		 * @generated
		 */
		EClass LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getLightEnablementBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.PointLightBindingWizardPagesProviderImpl <em>Point Light Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.PointLightBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl#getPointLightBindingWizardPagesProvider()
		 * @generated
		 */
		EClass POINT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getPointLightBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.SpotLightBindingWizardPagesProviderImpl <em>Spot Light Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.SpotLightBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackageImpl#getSpotLightBindingWizardPagesProvider()
		 * @generated
		 */
		EClass SPOT_LIGHT_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getSpotLightBindingWizardPagesProvider();

	}

} //ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage
