/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.Light;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ApogyCommonTopologyAddonsPrimitivesBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.LightEnablementBinding;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.bindings.ui.LightEnablementBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.AbstractTopologyBindingWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.TopologyNodeSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Light Enablement Binding Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LightEnablementBindingWizardPagesProviderImpl extends AbstractTopologyBindingWizardPagesProviderImpl implements LightEnablementBindingWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LightEnablementBindingWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyAddonsPrimitivesBindingsUIPackage.Literals.LIGHT_ENABLEMENT_BINDING_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		LightEnablementBinding lightEnablementBinding = ApogyCommonTopologyAddonsPrimitivesBindingsFactory.eINSTANCE.createLightEnablementBinding();		
		return lightEnablementBinding;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
		
		// Add topology node selection page.
		List<Node> nodes = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			nodes = (List<Node>) mapBasedEClassSettings.getUserDataMap().get(TopologyUIBindingsConstants.NODES_LIST_USER_ID);
		}
		
		LightEnablementBinding binding = (LightEnablementBinding) eObject;		
		
		TopologyNodeSelectionWizardPage topologyNodeSelectionWizardPage = new TopologyNodeSelectionWizardPage("Select Light", 
				"Select the Light that this binding is controlling.", 
				binding, 
				ApogyCommonTopologyAddonsPrimitivesPackage.Literals.LIGHT, 
				nodes) 
		{	
			@Override
			protected void nodeSelected(Node nodeSelected) 
			{
				if(abstractTopologyBinding != null && nodeSelected instanceof Light)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((LightEnablementBinding) abstractTopologyBinding, ApogyCommonTopologyAddonsPrimitivesBindingsPackage.Literals.LIGHT_ENABLEMENT_BINDING__LIGHT, (Light) nodeSelected, true);
					
					setMessage("Node selected : " + nodeSelected.getNodeId());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("The selected Node is not a Light !");
				}
			}
		};
		
		list.add(topologyNodeSelectionWizardPage);
		
		return list;
	}	
} //LightEnablementBindingWizardPagesProviderImpl
