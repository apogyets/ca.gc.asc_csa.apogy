package ca.gc.asc_csa.apogy.addons.provider;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool;
import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFactory;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.addons.AbstractTwoPoints3DTool} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc --> * @generated
 */
public class AbstractTwoPoints3DToolItemProvider
	extends Simple3DToolItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public AbstractTwoPoints3DToolItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFromNodePropertyDescriptor(object);
			addFromNodeLockPropertyDescriptor(object);
			addToNodePropertyDescriptor(object);
			addToNodeLockPropertyDescriptor(object);
			addDistancePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the From Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void addFromNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractTwoPoints3DTool_fromNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractTwoPoints3DTool_fromNode_feature", "_UI_AbstractTwoPoints3DTool_type"),
				 ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE,
				 false,
				 false,
				 true,
				 null,
				 getString("_UI_FROM_NODEPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the From Node Lock feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void addFromNodeLockPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractTwoPoints3DTool_fromNodeLock_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractTwoPoints3DTool_fromNodeLock_feature", "_UI_AbstractTwoPoints3DTool_type"),
				 ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 getString("_UI_FROM_NODEPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the To Node feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void addToNodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractTwoPoints3DTool_toNode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractTwoPoints3DTool_toNode_feature", "_UI_AbstractTwoPoints3DTool_type"),
				 ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE,
				 false,
				 false,
				 true,
				 null,
				 getString("_UI_TO_NODEPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the To Node Lock feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void addToNodeLockPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractTwoPoints3DTool_toNodeLock_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractTwoPoints3DTool_toNodeLock_feature", "_UI_AbstractTwoPoints3DTool_type"),
				 ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 getString("_UI_TO_NODEPropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Distance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void addDistancePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AbstractTwoPoints3DTool_distance_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AbstractTwoPoints3DTool_distance_feature", "_UI_AbstractTwoPoints3DTool_type"),
				 ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE,
				 false,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION);
			childrenFeatures.add(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AbstractTwoPoints3DTool)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_AbstractTwoPoints3DTool_type") :
			getString("_UI_AbstractTwoPoints3DTool_type") + " " + label;
	}
	
	protected String getAbstractTwoPoints3DToolText(AbstractTwoPoints3DTool abstractTwoPoints3DTool)
	{
		String text = new String();
		
		// Adds locked status.
		if(abstractTwoPoints3DTool.isFromNodeLock())
		{
			text += "FROM locked";
			
			if(abstractTwoPoints3DTool.isToNodeLock()) 
			{
				text += " , TO locked ";
			}
		}
		else if(abstractTwoPoints3DTool.isToNodeLock())
		{
			text += "TO locked";
		}
								
		return text;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AbstractTwoPoints3DTool.class)) {
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE:
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_LOCK:
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION:
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION:
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE:
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_LOCK:
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__DISTANCE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION:
			case ApogyAddonsPackage.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION,
				 ApogyCommonMathFactory.eINSTANCE.createTuple3d()));

		newChildDescriptors.add
			(createChildParameter
				(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION,
				 ApogyCommonMathFactory.eINSTANCE.createTuple3d()));

		newChildDescriptors.add
			(createChildParameter
				(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH,
				 ApogyCommonTopologyFactory.eINSTANCE.createNodePath()));

		newChildDescriptors.add
			(createChildParameter
				(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION,
				 ApogyCommonMathFactory.eINSTANCE.createTuple3d()));

		newChildDescriptors.add
			(createChildParameter
				(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION,
				 ApogyCommonMathFactory.eINSTANCE.createTuple3d()));

		newChildDescriptors.add
			(createChildParameter
				(ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH,
				 ApogyCommonTopologyFactory.eINSTANCE.createNodePath()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_ABSOLUTE_POSITION ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_RELATIVE_POSITION ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_ABSOLUTE_POSITION ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_RELATIVE_POSITION ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__FROM_NODE_NODE_PATH ||
			childFeature == ApogyAddonsPackage.Literals.ABSTRACT_TWO_POINTS3_DTOOL__TO_NODE_NODE_PATH;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
