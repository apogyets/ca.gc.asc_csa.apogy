package ca.gc.asc_csa.apogy.addons.sensors.gps.state;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.addons.sensors.gps.GPS;
import ca.gc.asc_csa.apogy.addons.sensors.gps.GPSStatus;

public abstract class GPSState {

	private GPS gps;

	public GPSState(GPS gps) {

		if (gps == null) {
			throw new IllegalArgumentException();
		}

		this.gps = gps;
	}

	public GPS getGPS() {
		return gps;
	}

	public void start() throws IllegalStateException {
		throw new IllegalStateException();
	}

	public void stop() throws IllegalStateException {
		throw new IllegalStateException();
	}

	public void reset() throws IllegalStateException {
		getGPS().setStatus(GPSStatus.STOPPED);
		getGPS().setLastFailure(null);
	}

	public void updateGPS() throws IllegalStateException {
		throw new IllegalStateException();
	}

	public boolean isRunning() {
		return false;
	}

	public boolean reconnect() throws IllegalStateException {
		throw new IllegalStateException();
	}

	public void failure(Exception e) {
		getGPS().setLastFailure(e);
	}

}
