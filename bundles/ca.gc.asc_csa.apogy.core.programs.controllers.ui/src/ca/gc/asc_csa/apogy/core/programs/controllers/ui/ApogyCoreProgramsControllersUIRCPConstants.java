package ca.gc.asc_csa.apogy.core.programs.controllers.ui;

public class ApogyCoreProgramsControllersUIRCPConstants {

	/**
	 * PartIds for selectionBasedParts.
	 */
	public static final String PART__CONTROLLER_BINDINGS__ID = "ca.gc.asc_csa.apogy.core.programs.controllers.ui.part.controllerBindings";
	public static final String PART__CONTROLLER_CONFIGS__ID = "ca.gc.asc_csa.apogy.core.programs.controllers.ui.part.controllerConfigs";
}
