package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.ApogyCommonTopologyAddonsPrimitivesUIPackage
 * @generated
 */
public interface ApogyCommonTopologyAddonsPrimitivesUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	ApogyCommonTopologyAddonsPrimitivesUIFactory eINSTANCE = ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.impl.ApogyCommonTopologyAddonsPrimitivesUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Vector Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Vector Presentation</em>'.
	 * @generated
	 */
	VectorPresentation createVectorPresentation();

	/**
	 * Returns a new object of class '<em>Way Point Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Way Point Presentation</em>'.
	 * @generated
	 */
	WayPointPresentation createWayPointPresentation();

	/**
	 * Returns a new object of class '<em>Label Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Label Presentation</em>'.
	 * @generated
	 */
	LabelPresentation createLabelPresentation();

	/**
	 * Returns a new object of class '<em>Sphere Primitive Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Sphere Primitive Presentation</em>'.
	 * @generated
	 */
	SpherePrimitivePresentation createSpherePrimitivePresentation();

	/**
	 * Returns a new object of class '<em>Spot Light Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Spot Light Presentation</em>'.
	 * @generated
	 */
	SpotLightPresentation createSpotLightPresentation();

	/**
	 * Returns a new object of class '<em>Sphere Primitive Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sphere Primitive Wizard Pages Provider</em>'.
	 * @generated
	 */
	SpherePrimitiveWizardPagesProvider createSpherePrimitiveWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Light Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Light Wizard Pages Provider</em>'.
	 * @generated
	 */
	LightWizardPagesProvider createLightWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Ambient Light Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ambient Light Wizard Pages Provider</em>'.
	 * @generated
	 */
	AmbientLightWizardPagesProvider createAmbientLightWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Directional Light Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Directional Light Wizard Pages Provider</em>'.
	 * @generated
	 */
	DirectionalLightWizardPagesProvider createDirectionalLightWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Point Light Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Point Light Wizard Pages Provider</em>'.
	 * @generated
	 */
	PointLightWizardPagesProvider createPointLightWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Spot Light Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Spot Light Wizard Pages Provider</em>'.
	 * @generated
	 */
	SpotLightWizardPagesProvider createSpotLightWizardPagesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyCommonTopologyAddonsPrimitivesUIPackage getApogyCommonTopologyAddonsPrimitivesUIPackage();

} //ApogyCommonTopologyAddonsPrimitivesUIFactory
