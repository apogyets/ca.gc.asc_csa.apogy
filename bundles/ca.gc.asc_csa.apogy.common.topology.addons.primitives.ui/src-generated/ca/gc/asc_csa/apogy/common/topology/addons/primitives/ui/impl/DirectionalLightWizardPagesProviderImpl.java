/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ApogyCommonTopologyAddonsPrimitivesFactory;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.DirectionalLight;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.ApogyCommonTopologyAddonsPrimitivesUIPackage;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.DirectionalLightWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.wizards.DirectionalLightWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Directional Light Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DirectionalLightWizardPagesProviderImpl extends LightWizardPagesProviderImpl implements DirectionalLightWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DirectionalLightWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyAddonsPrimitivesUIPackage.Literals.DIRECTIONAL_LIGHT_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		DirectionalLight light = ApogyCommonTopologyAddonsPrimitivesFactory.eINSTANCE.createDirectionalLight();
		light.setColor(ApogyCommonMathFacade.INSTANCE.createTuple3d(1, 1, 1));
		light.setDirection(ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, -1));
		return light;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	

		list.add(new DirectionalLightWizardPage((DirectionalLight) eObject));
		
		return list;
	}
} //DirectionalLightWizardPagesProviderImpl
