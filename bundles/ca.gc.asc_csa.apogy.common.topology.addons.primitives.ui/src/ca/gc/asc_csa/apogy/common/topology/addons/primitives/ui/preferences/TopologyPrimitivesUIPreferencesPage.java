package ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.Activator;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;

public class TopologyPrimitivesUIPreferencesPage extends PreferencePage implements IWorkbenchPreferencePage
{	
	private BooleanFieldEditor circularSectorFOVBooleanFieldEditor;	
	private BooleanFieldEditor circularSectorFOVVisibilityBooleanFieldEditor;	
	private RadioGroupFieldEditor circularSectorFOVRadioGroupFieldEditor;	
	private ColorFieldEditor circularSectorFOVColorFieldEditor;	
	private BooleanFieldEditor circularSectorAxisVisibleBooleanFieldEditor;

	/**
	 * Create the preference page.
	 */
	public TopologyPrimitivesUIPreferencesPage() {
	}

	/**
	 * Create contents of the preference page.
	 * @param parent
	 */
	@Override
	public Control createContents(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, true));
		
		// Spot Light
		Group grpCircularSectorFov = new Group(container, SWT.NONE);
		grpCircularSectorFov.setLayout(new GridLayout(2, true));
		grpCircularSectorFov.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
		grpCircularSectorFov.setText("Spot Light");	
		
		Label circularSectorFOVVisibilityLabel = new Label(grpCircularSectorFov, SWT.NONE);
		circularSectorFOVVisibilityLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		circularSectorFOVVisibilityLabel.setText("Visibility");		
		circularSectorFOVBooleanFieldEditor = createBooleanFieldEditor(grpCircularSectorFov, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_VISIBILITY_ID, "");				

		Label circularSectorFOVFOVVisibilityLabel = new Label(grpCircularSectorFov, SWT.NONE);
		circularSectorFOVFOVVisibilityLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		circularSectorFOVFOVVisibilityLabel.setText("Light Cone Visibility");		
		circularSectorFOVVisibilityBooleanFieldEditor = createBooleanFieldEditor(grpCircularSectorFov, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_VISIBILITY_ID, "");				

		// Axis Visibility
		Label circularSectorAxisLabel = new Label(grpCircularSectorFov, SWT.NONE);
		circularSectorAxisLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		circularSectorAxisLabel.setText("Axis Visible");
		circularSectorAxisVisibleBooleanFieldEditor = createBooleanFieldEditor(grpCircularSectorFov, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_AXIS_VISIBLE_ID, "");

		
		// FOV Color
		circularSectorFOVColorFieldEditor = createColorFieldEditor(grpCircularSectorFov, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_COLOR_ID, "Default Color:");		

		Label circularSectorFOVModeLabel = new Label(grpCircularSectorFov, SWT.NONE);
		circularSectorFOVModeLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 1, 1));
		circularSectorFOVModeLabel.setText("Presentation Mode:");
		circularSectorFOVRadioGroupFieldEditor = createRadioGroupFieldEditor(grpCircularSectorFov, TopologyPrimitivesUIPreferencesConstants.DEFAULT_SPOT_LIGHT_LIGHT_CONE_PRESENTATION_MODE_ID, "");				

		return container;
	}
		
	/**
	 * Initialize the preference page.
	 */
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	public boolean performOk() 
	{
		storePreferences();			
		return super.performOk();
	}
	
	@Override
	protected void performApply() 
	{
		storePreferences();
		super.performApply();
	}
	
	@Override
	protected void performDefaults() 
	{		
		circularSectorFOVBooleanFieldEditor.loadDefault();

		circularSectorFOVVisibilityBooleanFieldEditor.loadDefault();
			
		circularSectorFOVRadioGroupFieldEditor.loadDefault();
		
		circularSectorFOVColorFieldEditor.loadDefault();
				
		circularSectorAxisVisibleBooleanFieldEditor.loadDefault();
		
		super.performDefaults();
	}
	
	private String[][] getModeLabelsAndValues()
	{
		String [][] labelAndValues = new String[MeshPresentationMode.VALUES.size()][2];
		
		int i = 0;
		for(MeshPresentationMode value : MeshPresentationMode.VALUES)
		{
			labelAndValues[i][0] = value.getName();
			labelAndValues[i][1] = Integer.toString(value.getValue());
			i++;
		}
		
		return labelAndValues;
	}
	
	private BooleanFieldEditor createBooleanFieldEditor(final Composite container, final String preferenceID, final String preferenceLabel)
	{
		BooleanFieldEditor editor = new BooleanFieldEditor(preferenceID, preferenceLabel, container);
		
		//Set the editor up to use this page	
		editor.setPreferenceStore(getPreferenceStore());
		editor.load();
		
		return editor;
	}
	
	private RadioGroupFieldEditor createRadioGroupFieldEditor(final Composite container, final String preferenceID, final String preferenceLabel)
	{
		Composite editorContainer = new Composite(container, SWT.NULL);
		editorContainer.setLayout(new GridLayout(1, true));
		
		RadioGroupFieldEditor editor = new RadioGroupFieldEditor(preferenceID, preferenceLabel, 1, getModeLabelsAndValues(), editorContainer, false);
		
		//Set the editor up to use this page	
		editor.setPreferenceStore(getPreferenceStore());
		editor.load();
		
		return editor;
	}
	
	private ColorFieldEditor createColorFieldEditor(final Composite container, final String preferenceID, final String preferenceLabel)
	{
		ColorFieldEditor colorEditor = new ColorFieldEditor(preferenceID, preferenceLabel, container);				

		//Set the editor up to use this page	
		colorEditor.setPreferenceStore(getPreferenceStore());
		colorEditor.load();

		return colorEditor;
	}

	private void storePreferences()
	{
		circularSectorFOVBooleanFieldEditor.store();
		
		circularSectorFOVVisibilityBooleanFieldEditor.store();

		circularSectorFOVRadioGroupFieldEditor.store();
		
		circularSectorFOVColorFieldEditor.store();
		
		circularSectorAxisVisibleBooleanFieldEditor.store();
	}

}
