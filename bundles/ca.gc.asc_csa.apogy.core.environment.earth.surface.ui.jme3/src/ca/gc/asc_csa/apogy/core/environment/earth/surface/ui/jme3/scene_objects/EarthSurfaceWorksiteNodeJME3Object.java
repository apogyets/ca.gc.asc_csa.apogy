package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.vecmath.Matrix4d;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.addons.primitives.ui.jme3.JME3PrimitivesUtilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;
import ca.gc.asc_csa.apogy.core.environment.Worksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ApogyEarthSurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksite;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.EarthSurfaceWorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.jme3.EarthSurfaceEnvironmentJMEConstants;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.jme3.EnvironmentUIJME3Utilities;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.scene_objects.EarthSurfaceWorksiteSceneObject;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.jme3.SurfaceEnvironmentJMEConstants;


public class EarthSurfaceWorksiteNodeJME3Object extends DefaultJME3SceneObject<EarthSurfaceWorksiteNode> implements EarthSurfaceWorksiteSceneObject
{					
	private static ColorRGBA HORIZON_COLOR = new ColorRGBA(0.0753f, 0.04414f, 0.015686f, 1.0f);
	
	private Adapter adapter;
	
	private boolean axisVisible = true;
	private float axisLength = 1.0f;
	
	private boolean planeVisible = true;
	private float gridSize = 10.0f;
	private float planeSize = 100.0f;	
	
	private boolean azimuthVisible = true;
	private boolean azimuthLinesVisible = true;
	private boolean elevationLinesVisible = true;
		
	private AssetManager assetManager;	
	
	private Geometry gridGeometry = null;
	//private Geometry planeGeometry = null;
	private Geometry axisGeometry = null;	
	private Geometry horizon;
	
	private Node skyNode = null;
	private Node azimuthDisplayNode = null;
	private Node azimuthDisplayCirclesNode = null;
	private Node elevationDisplayCirclesNode = null;
		
	public EarthSurfaceWorksiteNodeJME3Object(EarthSurfaceWorksiteNode node, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(node, jme3RenderEngineDelegate);
				
		this.assetManager = jme3Application.getAssetManager();		
		
		// Creates geometry.
		createGeometry();			
				
		// Listens for changes on the Worksite.
		node.eAdapters().add(getAdapter());
		if(node.getWorksite() instanceof EarthSurfaceWorksite)
		{
			EarthSurfaceWorksite earthSurfaceWorksite = (EarthSurfaceWorksite) node.getWorksite();
			earthSurfaceWorksite.eAdapters().add(getAdapter());			
			updateSkyTransform(earthSurfaceWorksite.getXAxisAzimuth());
		}					
	}
		
	@Override
	public List<Geometry> getGeometries() 
	{		
		List<Geometry> geometries = new ArrayList<Geometry>();
		geometries.add(gridGeometry);		
		geometries.add(axisGeometry);
		geometries.add(horizon);
		return geometries;
	}
	
	
	@Override
	public void setPlaneVisible(boolean newPlaneVisible)
	{		
		this.planeVisible = newPlaneVisible;
		
		Logger.INSTANCE.log(Activator.ID, this, "Setting Plane visibility to <" + newPlaneVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				if(gridGeometry != null) 
				{
					if(planeVisible)
					{
						getAttachmentNode().attachChild(gridGeometry);
					}
					else
					{
						getAttachmentNode().detachChild(gridGeometry);
					}	
				}
				
				return null;
			}
		});		
	}
	
	@Override
	public void setPlaneParameters(double newGridSize, double newPlaneSize)
	{
		Logger.INSTANCE.log(Activator.ID, this, "Setting Plane grid size to <" + newGridSize + "> and grid size to <" + newPlaneSize + ">", EventSeverity.INFO);
		
		if(newGridSize > 0 && newPlaneSize > 0)
		{
			this.gridSize = (float) newGridSize;
			this.planeSize = (float) newPlaneSize;		
			
			jme3Application.enqueue(new Callable<Object>() 
			{
				@Override
				public Object call() throws Exception 
				{									
					// Detach previous geometry.
					if(gridGeometry != null)
					{
						getAttachmentNode().detachChild(gridGeometry);
					}
					
					gridGeometry = createGridGeometry();
					
					if(planeVisible)
					{
						getAttachmentNode().attachChild(gridGeometry);
					}
					else
					{
						getAttachmentNode().detachChild(gridGeometry);
					}			
					
					return null;
				}
			});	
		}
		else
		{
			Logger.INSTANCE.log(Activator.ID, this, "Failed to set Plane grid size to <" + newGridSize + "> and grid size to <" + newPlaneSize + ">", EventSeverity.ERROR);

		}
	}

	@Override
	public void setAxisVisible(boolean newAxisVisible)
	{
		this.axisVisible = newAxisVisible;		
		
		Logger.INSTANCE.log(Activator.ID, this, "Setting Axis visibility to <" + newAxisVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				if(axisGeometry != null) 
				{
					if(axisVisible)
					{
						getAttachmentNode().attachChild(axisGeometry);
					}
					else
					{
						getAttachmentNode().detachChild(axisGeometry);
					}	
				}	
				else
				{
					Logger.INSTANCE.log(Activator.ID, this, "Failed to set Axis visibility to <" + newAxisVisible + ">", EventSeverity.ERROR);
				}
				
				return null;
			}
		});		
	}
	
	@Override
	public void setAxisLength(double newLength)
	{				
		this.axisLength = (float) newLength;
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{	
				Logger.INSTANCE.log(Activator.ID, this, "Setting Axis Length to <" + newLength + ">", EventSeverity.INFO);
				try
				{					
					if(axisGeometry != null)
					{
						getAttachmentNode().detachChild(axisGeometry);
					}
					
					// Updates axis geometry
					axisGeometry = JME3Utilities.createAxis3D(axisLength, assetManager);
					
					// If axis geometry is visible, attach the new geometry.
					if(axisVisible)
					{
						getAttachmentNode().attachChild(axisGeometry);
					}
				}
				catch(Throwable t)
				{
					Logger.INSTANCE.log(Activator.ID, this, "Failed to setAxisLength(" + newLength + ")!", EventSeverity.ERROR, t);
				}	
				
				return null;
			}
		});								
	}
	
	@Override
	public void setAzimuthVisible(boolean newAzimuthVisible) 	
	{
		this.azimuthVisible = newAzimuthVisible;				
		Logger.INSTANCE.log(Activator.ID, this, "Setting Azimuth visibility to <" + newAzimuthVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{		
				if(skyNode != null) 
				{
					if(azimuthVisible)
					{
						skyNode.attachChild(azimuthDisplayNode);
					}
					else
					{
						skyNode.detachChild(azimuthDisplayNode);
					}
				}
				else
				{
					Logger.INSTANCE.log(Activator.ID, this, "Failed to set Azimuth visibility to <" + newAzimuthVisible + ">", EventSeverity.ERROR);
				}
				
				return null;
			}	
		});	
	}

	@Override
	public void setElevationLinesVisible(boolean newElevationLinesVisible) 
	{
		this.elevationLinesVisible = newElevationLinesVisible;		
		
		Logger.INSTANCE.log(Activator.ID, this, "Setting Elevation Lines visibility to <" + newElevationLinesVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{		
				if(skyNode != null) 
				{
					if(elevationLinesVisible)
					{
						skyNode.attachChild(elevationDisplayCirclesNode);
					}
					else
					{
						skyNode.detachChild(elevationDisplayCirclesNode);
					}
				}
				else
				{
					Logger.INSTANCE.log(Activator.ID, this, "Failed to set Elevation Lines visibility to <" + newElevationLinesVisible + ">", EventSeverity.ERROR);
				}
				return null;
			}	
		});	
	}

	@Override
	public void setAzimuthLinesVisible(final boolean newAzimuthLinesVisible) 
	{
		this.azimuthLinesVisible = newAzimuthLinesVisible;
						
		Logger.INSTANCE.log(Activator.ID, this, "Setting Azimuth Lines visibility to <" + newAzimuthLinesVisible + ">", EventSeverity.INFO);
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{		
				if(skyNode != null) 
				{
					if(azimuthLinesVisible)
					{
						skyNode.attachChild(azimuthDisplayCirclesNode);
					}
					else
					{
						skyNode.detachChild(azimuthDisplayCirclesNode);
					}
				}
				else
				{
					Logger.INSTANCE.log(Activator.ID, this, "Failed to set Azimuth Lines visibility to <" + newAzimuthLinesVisible + ">", EventSeverity.ERROR);
				}
				return null;
			}	
		});		
	}
	
	private void createGeometry()
	{
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				if(horizon != null) getAttachmentNode().detachChild(horizon);
				if(gridGeometry != null) getAttachmentNode().detachChild(gridGeometry);				
				if(axisGeometry != null) getAttachmentNode().detachChild(axisGeometry);								
				if(skyNode != null)  getAttachmentNode().detachChild(skyNode);

				// Adds the Horizon;
				horizon = createHorizon();
				getAttachmentNode().attachChild(horizon);
				
				// Adds the axis
				axisGeometry = JME3Utilities.createAxis3D(axisLength, assetManager);				
				getAttachmentNode().attachChild(axisGeometry);
				
				// Adds the grid				
				gridGeometry = createGridGeometry();
				getAttachmentNode().attachChild(gridGeometry);
																	
				skyNode = createSkyNode();
				getAttachmentNode().attachChild(skyNode);
				
				return null;
			}	
		});		
	}
	
	private Node createSkyNode()
	{
		Node node =  new Node("Worksite Sky");			
									
		azimuthDisplayNode = EnvironmentUIJME3Utilities.createAzimuthDisplay(assetManager);
		node.attachChild(azimuthDisplayNode);
		
		// Azimuth Circle displays
		azimuthDisplayCirclesNode = EnvironmentUIJME3Utilities.createAzimuthCirclesDisplay(assetManager);
		node.attachChild(azimuthDisplayCirclesNode);
		
		// Elevation circle display
		elevationDisplayCirclesNode = EnvironmentUIJME3Utilities.createElevationCirclesDisplay(assetManager);
		node.attachChild(elevationDisplayCirclesNode);		
		
		Matrix4d m = new Matrix4d();
		m.setIdentity();

		if(getTopologyNode().getWorksite() instanceof EarthSurfaceWorksite)
		{
			EarthSurfaceWorksite earthSurfaceWorksite = (EarthSurfaceWorksite) getTopologyNode().getWorksite();					
			m.rotZ(-earthSurfaceWorksite.getXAxisAzimuth());			
		}
										
		node.setLocalTransform(JME3Utilities.createTransform(m));
		
		return node;
	}
	
	private void updateSkyTransform(double newXAxisAzimuthDegrees)
	{
		if(skyNode != null)
		{
			Matrix4d m = new Matrix4d();
			m.setIdentity();
			m.rotZ(-newXAxisAzimuthDegrees);	
			
			skyNode.setLocalTransform(JME3Utilities.createTransform(m));
		}
	}
	
	private Geometry createGridGeometry()
	{
		Mesh gridMesh = EnvironmentUIJME3Utilities.createGrid(gridSize, planeSize);
		
		Geometry geometry = new Geometry("Grid", gridMesh);
		
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", EarthSurfaceEnvironmentJMEConstants.DEFAULT_GRID_COLOR.clone());
        geometry.setMaterial(mat);
		
		return geometry;
	}	
	
	/**
	 * Returns the Spherical Cap (half-sphere used to represent the horizon and hide the sky).
	 * @return The geometry.
	 */
	private Geometry createHorizon()
	{		
		Mesh horizonMesh = JME3PrimitivesUtilities.createSphericalCap(SurfaceEnvironmentJMEConstants.HORIZON_RADIUS, (float) Math.toRadians(-90), 0f, 32);
		horizonMesh.updateBound();
		horizonMesh.updateCounts();
			
		Geometry geometry = new Geometry("Horizon", horizonMesh);			
		geometry.setMaterial(createHorizonMaterial());
		geometry.setShadowMode(ShadowMode.Off);
				
		return geometry;
	}
	
	/**
	 * Creates the material used on the horizon geometry.
	 * @return The Material.
	 */
	private Material createHorizonMaterial()
	{		
		Material mat = new Material(assetManager,  "Common/MatDefs/Misc/Unshaded.j3md");			
		mat.setColor("Color", HORIZON_COLOR.clone());				
		mat.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
		
		return mat;
	}
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof EarthSurfaceWorksiteNode)
					{
						// Worksite has changed.
						if(msg.getFeatureID(EarthSurfaceWorksiteNode.class) == ApogyEarthSurfaceEnvironmentPackage.EARTH_SURFACE_WORKSITE_NODE__WORKSITE)
						{
							// Unregister from old Worksite.
							if(msg.getOldValue() instanceof Worksite)
							{
								Worksite oldWorksite = (Worksite) msg.getOldValue();
								oldWorksite.eAdapters().remove(getAdapter());
							}
																					
							if(msg.getNewValue() instanceof Worksite)
							{
								Worksite newWorksite = (Worksite) msg.getNewValue();
								newWorksite.eAdapters().add(getAdapter());
								
								if(newWorksite instanceof EarthSurfaceWorksite)
								{
									EarthSurfaceWorksite earthSurfaceWorksite = (EarthSurfaceWorksite) newWorksite;
									updateSkyTransform(earthSurfaceWorksite.getXAxisAzimuth());
								}
							}							
						}												
					}
					else if(msg.getNotifier() instanceof EarthSurfaceWorksite)
					{
						if(msg.getFeatureID(EarthSurfaceWorksite.class) == ApogyEarthSurfaceEnvironmentPackage.EARTH_SURFACE_WORKSITE__XAXIS_AZIMUTH)
						{
							double newXAxisAzimuthDegrees = msg.getNewDoubleValue();
							updateSkyTransform(newXAxisAzimuthDegrees);
						}
					}
				}
			};
		}
		
		return adapter;
	}
}
