package ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.jme3;

import com.jme3.math.ColorRGBA;

import ca.gc.asc_csa.apogy.core.environment.surface.ui.jme3.SurfaceEnvironmentJMEConstants;

public class EarthSurfaceEnvironmentJMEConstants 
{
	public static final float SUN_AND_MOON_RADIUS     = SurfaceEnvironmentJMEConstants.CELESTIAL_SPHERE_RADIUS * 0.9f;
	
	
	public static final String DEGREE_STRING = 	"\u00b0";
	public static final ColorRGBA DEFAULT_GRID_COLOR = new ColorRGBA(0.2f, 0.2f, 0.2f, 1.0f);
	public static final ColorRGBA DEFAULT_AZIMUTH_LINES_COLOR = new ColorRGBA(0.0f, 1.0f, 0.0f, 1.0f);

	
	/** Azimuth display properties. */
	public static final float AZIMUTH_DISPLAY_RADIUS  = (float) (SurfaceEnvironmentJMEConstants.CELESTIAL_SPHERE_RADIUS * 0.75);	
	public static final float AZIMUTH_MAJOR_TICKS_HEIGHT = (float) (AZIMUTH_DISPLAY_RADIUS * Math.tan(Math.toRadians(2)));
	public static final float AZIMUTH_MIDDLE_TICKS_HEIGHT = AZIMUTH_MAJOR_TICKS_HEIGHT * 0.5f;
	public static final float AZIMUTH_MINOR_TICKS_HEIGHT = AZIMUTH_MAJOR_TICKS_HEIGHT * 0.25f;
	
	public static final int MAJOR_TICKS_FONT_SIZE = Math.round((float)(20 * (AZIMUTH_DISPLAY_RADIUS / 1000.0)));	
	public static final int MIDDLE_TICKS_FONT_SIZE = Math.round((float)(15 * (AZIMUTH_DISPLAY_RADIUS / 1000.0)));
	public static final int MINOR_TICKS_FONT_SIZE = Math.round((float)(15 * (AZIMUTH_DISPLAY_RADIUS / 1000.0)));
	
	public static final float AZIMUTH_MAJOR_TICK_ANGLE_INTERVAL_IN_DEG = 90.0f;
	public static final float AZIMUTH_MID_TICK_ANGLE_INTERVAL_IN_DEG = 45.0f;
	public static final float AZIMUTH_MINOR_TICK_ANGLE_INTERVAL_IN_DEG = 5.0f;
			
	/** Elevation circles display properties. */
	public static final float ELEVATION_DISPLAY_RADIUS  = (float) (SurfaceEnvironmentJMEConstants.CELESTIAL_SPHERE_RADIUS * 0.75f);
	public static final float ELEVATION_LINES_ANGLE_INTERVAL_IN_DEG = 5.0f;
	public static final float ELEVATION_AZIMUTH_INTERVAL_IN_DEG = 5.0f;
	public static final float ELEVATION_LABEL_AZIMUTH_INTERVAL_IN_DEG = 45.0f;
	
	/** Azimuth circles display properties. */	
	public static final float AZIMUTH_LINES_ANGLE_INTERVAL_IN_DEG = 10f;
	public static final float AZIMUTH_LABEL_AZIMUTH_INTERVAL_IN_DEG = 45.0f;
	public static final float AZIMUTH_ELEVATION_INTERVAL_IN_DEG = 5.0f;
	public static final float AZIMUTH_MIN_ELEVATION_ANGLE_IN_DEG = 1.5f;
	public static final float AZIMUTH_MAX_ELEVATION_ANGLE_IN_DEG = 90 - ELEVATION_AZIMUTH_INTERVAL_IN_DEG;
}
