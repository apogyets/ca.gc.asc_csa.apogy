package ca.gc.asc_csa.apogy.addons.sensors.imaging.converters;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.io.File;
import java.util.Date;

import ca.gc.asc_csa.apogy.addons.sensors.imaging.ImageSnapshot;
import ca.gc.asc_csa.apogy.common.converters.IConverter;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;

public class ImageSnapshotToIFileConverter implements IConverter {

	public boolean canConvert(Object arg0) 
	{		
		if(arg0 instanceof ImageSnapshot)
		{
			return (((ImageSnapshot) arg0).getImage() != null);
		}
		return false;
	}

	public Object convert(Object arg0) throws Exception 
	{		
		ImageSnapshot imageSnapshot = (ImageSnapshot) arg0;
		
		// Saves the image in a file in the tmp folder .
		String tmpFolder = System.getProperty("user.home") + File.separator + System.getProperty("java.io.tmpdir");
		Date now = new Date();				
				
		// Save as .jpg
		String fileName = null;
		try
		{			
			fileName = tmpFolder + File.separator + now.getTime() + ".jpg";										
			EImagesUtilities.INSTANCE.saveImageAsJPEG(fileName, imageSnapshot.getImage());				
			
			return new File(fileName);	
		}
		catch(Exception e)
		{
			File toDelete = new File(fileName);
			toDelete.delete();			
			e.printStackTrace();
			
			return null;
		}
	}

	public Class<?> getInputType() {
		return ImageSnapshot.class;
	}

	public Class<?> getOutputType() { 
		return  File.class;
	}
}
