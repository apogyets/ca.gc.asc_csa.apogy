/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
@GenModel(prefix="ApogyAddonsROSUI",
		  modelName="ApogyAddonsROSUI",
		  operationReflection="true",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
		  copyrightText="Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
    Pierre Allard (Pierre.Allard@canada.ca), 
    Regent L'Archeveque (Regent.Larcheveque@canada.ca),
    Sebastien Gemme (Sebastien.Gemme@canada.ca),
    Canadian Space Agency (CSA) - Initial API and implementation",
		  suppressGenModelAnnotations="false",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.addons.ros.ui/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.addons.ros.ui.edit/src-generated")

package ca.gc.asc_csa.apogy.addons.ros.ui

import ca.gc.asc_csa.apogy.addons.Simple3DTool
import ca.gc.asc_csa.apogy.addons.ros.ROSInterface
import ca.gc.asc_csa.apogy.addons.ros.ROSNode

/**
 * Tool used to display a ROS Transform into the 3D environment.
 */
class TFDisplay3DTool extends Simple3DTool, ROSInterface 
{
	contains ROSNode toolRosNode	
	
	/**
	 * Displays whether or not the tool if connected to its topic.
	 */
	@GenModel(notify="true", children="false", property="Readonly")
	boolean connected = "false"
	
	/**
	 * The name of the topic the provides the transform.
	 */
	String topicName = ""
		
	/**
	 * Starts tool and connected to the topic.
	 */
	op boolean start()
	
	/**
	 * Stops the tool and disconnects from the topic.
	 */
	op boolean stop()
	
}