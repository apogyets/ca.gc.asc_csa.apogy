/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.ros.ui;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca),
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyAddonsROSUI' modelName='ApogyAddonsROSUI' operationReflection='true' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation' suppressGenModelAnnotations='false' dynamicTemplates='true' templateDirectory='platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates' modelDirectory='/ca.gc.asc_csa.apogy.addons.ros.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.addons.ros.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.addons.ros'"
 * @generated
 */
public interface ApogyAddonsROSUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.addons.ros.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyAddonsROSUIPackage eINSTANCE = ca.gc.asc_csa.apogy.addons.ros.ui.impl.ApogyAddonsROSUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl <em>TF Display3 DTool</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.impl.ApogyAddonsROSUIPackageImpl#getTFDisplay3DTool()
	 * @generated
	 */
	int TF_DISPLAY3_DTOOL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__NAME = ApogyAddonsPackage.SIMPLE3_DTOOL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__DESCRIPTION = ApogyAddonsPackage.SIMPLE3_DTOOL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Tool List</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__TOOL_LIST = ApogyAddonsPackage.SIMPLE3_DTOOL__TOOL_LIST;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__ACTIVE = ApogyAddonsPackage.SIMPLE3_DTOOL__ACTIVE;

	/**
	 * The feature id for the '<em><b>Disposed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__DISPOSED = ApogyAddonsPackage.SIMPLE3_DTOOL__DISPOSED;

	/**
	 * The feature id for the '<em><b>Initialized</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__INITIALIZED = ApogyAddonsPackage.SIMPLE3_DTOOL__INITIALIZED;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__VISIBLE = ApogyAddonsPackage.SIMPLE3_DTOOL__VISIBLE;

	/**
	 * The feature id for the '<em><b>Root Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__ROOT_NODE = ApogyAddonsPackage.SIMPLE3_DTOOL__ROOT_NODE;

	/**
	 * The feature id for the '<em><b>Service Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__SERVICE_MANAGER = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Topic Launcher</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Publisher Manager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__NODE = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Tool Ros Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__TOOL_ROS_NODE = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Connected</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__CONNECTED = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Topic Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL__TOPIC_NAME = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>TF Display3 DTool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL_FEATURE_COUNT = ApogyAddonsPackage.SIMPLE3_DTOOL_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Initialise</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___INITIALISE = ApogyAddonsPackage.SIMPLE3_DTOOL___INITIALISE;

	/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___DISPOSE = ApogyAddonsPackage.SIMPLE3_DTOOL___DISPOSE;

	/**
	 * The operation id for the '<em>Variables Instantiated</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___VARIABLES_INSTANTIATED = ApogyAddonsPackage.SIMPLE3_DTOOL___VARIABLES_INSTANTIATED;

	/**
	 * The operation id for the '<em>Variables Cleared</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___VARIABLES_CLEARED = ApogyAddonsPackage.SIMPLE3_DTOOL___VARIABLES_CLEARED;

	/**
	 * The operation id for the '<em>Selection Changed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___SELECTION_CHANGED__NODESELECTION = ApogyAddonsPackage.SIMPLE3_DTOOL___SELECTION_CHANGED__NODESELECTION;

	/**
	 * The operation id for the '<em>Mouse Button Clicked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON = ApogyAddonsPackage.SIMPLE3_DTOOL___MOUSE_BUTTON_CLICKED__MOUSEBUTTON;

	/**
	 * The operation id for the '<em>Ros Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___ROS_INIT = ApogyAddonsPackage.SIMPLE3_DTOOL_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Start</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___START = ApogyAddonsPackage.SIMPLE3_DTOOL_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Stop</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL___STOP = ApogyAddonsPackage.SIMPLE3_DTOOL_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>TF Display3 DTool</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TF_DISPLAY3_DTOOL_OPERATION_COUNT = ApogyAddonsPackage.SIMPLE3_DTOOL_OPERATION_COUNT + 3;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool <em>TF Display3 DTool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>TF Display3 DTool</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool
	 * @generated
	 */
	EClass getTFDisplay3DTool();

	/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#getToolRosNode <em>Tool Ros Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Tool Ros Node</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#getToolRosNode()
	 * @see #getTFDisplay3DTool()
	 * @generated
	 */
	EReference getTFDisplay3DTool_ToolRosNode();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#isConnected <em>Connected</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Connected</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#isConnected()
	 * @see #getTFDisplay3DTool()
	 * @generated
	 */
	EAttribute getTFDisplay3DTool_Connected();

	/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#getTopicName <em>Topic Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Topic Name</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#getTopicName()
	 * @see #getTFDisplay3DTool()
	 * @generated
	 */
	EAttribute getTFDisplay3DTool_TopicName();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#start() <em>Start</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Start</em>' operation.
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#start()
	 * @generated
	 */
	EOperation getTFDisplay3DTool__Start();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#stop() <em>Stop</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Stop</em>' operation.
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool#stop()
	 * @generated
	 */
	EOperation getTFDisplay3DTool__Stop();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyAddonsROSUIFactory getApogyAddonsROSUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl <em>TF Display3 DTool</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl
		 * @see ca.gc.asc_csa.apogy.addons.ros.ui.impl.ApogyAddonsROSUIPackageImpl#getTFDisplay3DTool()
		 * @generated
		 */
		EClass TF_DISPLAY3_DTOOL = eINSTANCE.getTFDisplay3DTool();
		/**
		 * The meta object literal for the '<em><b>Tool Ros Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TF_DISPLAY3_DTOOL__TOOL_ROS_NODE = eINSTANCE.getTFDisplay3DTool_ToolRosNode();
		/**
		 * The meta object literal for the '<em><b>Connected</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TF_DISPLAY3_DTOOL__CONNECTED = eINSTANCE.getTFDisplay3DTool_Connected();
		/**
		 * The meta object literal for the '<em><b>Topic Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TF_DISPLAY3_DTOOL__TOPIC_NAME = eINSTANCE.getTFDisplay3DTool_TopicName();
		/**
		 * The meta object literal for the '<em><b>Start</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TF_DISPLAY3_DTOOL___START = eINSTANCE.getTFDisplay3DTool__Start();
		/**
		 * The meta object literal for the '<em><b>Stop</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TF_DISPLAY3_DTOOL___STOP = eINSTANCE.getTFDisplay3DTool__Stop();

	}

} //ApogyAddonsROSUIPackage
