/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.ros.ui.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSPackage;
import ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIFactory;
import ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIPackage;
import ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyAddonsROSUIPackageImpl extends EPackageImpl implements ApogyAddonsROSUIPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tfDisplay3DToolEClass = null;
	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyAddonsROSUIPackageImpl() {
		super(eNS_URI, ApogyAddonsROSUIFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyAddonsROSUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyAddonsROSUIPackage init() {
		if (isInited) return (ApogyAddonsROSUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsROSUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyAddonsROSUIPackageImpl theApogyAddonsROSUIPackage = (ApogyAddonsROSUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyAddonsROSUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyAddonsROSUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyAddonsPackage.eINSTANCE.eClass();
		ApogyAddonsROSPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyAddonsROSUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyAddonsROSUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyAddonsROSUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyAddonsROSUIPackage.eNS_URI, theApogyAddonsROSUIPackage);
		return theApogyAddonsROSUIPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTFDisplay3DTool() {
		return tfDisplay3DToolEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTFDisplay3DTool_ToolRosNode() {
		return (EReference)tfDisplay3DToolEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTFDisplay3DTool_Connected() {
		return (EAttribute)tfDisplay3DToolEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTFDisplay3DTool_TopicName() {
		return (EAttribute)tfDisplay3DToolEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTFDisplay3DTool__Start() {
		return tfDisplay3DToolEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTFDisplay3DTool__Stop() {
		return tfDisplay3DToolEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyAddonsROSUIFactory getApogyAddonsROSUIFactory() {
		return (ApogyAddonsROSUIFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tfDisplay3DToolEClass = createEClass(TF_DISPLAY3_DTOOL);
		createEReference(tfDisplay3DToolEClass, TF_DISPLAY3_DTOOL__TOOL_ROS_NODE);
		createEAttribute(tfDisplay3DToolEClass, TF_DISPLAY3_DTOOL__CONNECTED);
		createEAttribute(tfDisplay3DToolEClass, TF_DISPLAY3_DTOOL__TOPIC_NAME);
		createEOperation(tfDisplay3DToolEClass, TF_DISPLAY3_DTOOL___START);
		createEOperation(tfDisplay3DToolEClass, TF_DISPLAY3_DTOOL___STOP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyAddonsPackage theApogyAddonsPackage = (ApogyAddonsPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsPackage.eNS_URI);
		ApogyAddonsROSPackage theApogyAddonsROSPackage = (ApogyAddonsROSPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyAddonsROSPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tfDisplay3DToolEClass.getESuperTypes().add(theApogyAddonsPackage.getSimple3DTool());
		tfDisplay3DToolEClass.getESuperTypes().add(theApogyAddonsROSPackage.getROSInterface());

		// Initialize classes, features, and operations; add parameters
		initEClass(tfDisplay3DToolEClass, TFDisplay3DTool.class, "TFDisplay3DTool", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTFDisplay3DTool_ToolRosNode(), theApogyAddonsROSPackage.getROSNode(), null, "toolRosNode", null, 0, 1, TFDisplay3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTFDisplay3DTool_Connected(), theEcorePackage.getEBoolean(), "connected", "false", 0, 1, TFDisplay3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTFDisplay3DTool_TopicName(), theEcorePackage.getEString(), "topicName", "", 0, 1, TFDisplay3DTool.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getTFDisplay3DTool__Start(), theEcorePackage.getEBoolean(), "start", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getTFDisplay3DTool__Stop(), theEcorePackage.getEBoolean(), "stop", 0, 1, !IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "documentation", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca),\n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "prefix", "ApogyAddonsROSUI",
			 "modelName", "ApogyAddonsROSUI",
			 "operationReflection", "true",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "suppressGenModelAnnotations", "false",
			 "dynamicTemplates", "true",
			 "templateDirectory", "platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.addons.ros.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.addons.ros.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.addons.ros"
		   });	
		addAnnotation
		  (tfDisplay3DToolEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nTool used to display a ROS Transform into the 3D environment."
		   });	
		addAnnotation
		  (getTFDisplay3DTool__Start(), 
		   source, 
		   new String[] {
			 "documentation", "*\nStarts tool and connected to the topic."
		   });	
		addAnnotation
		  (getTFDisplay3DTool__Stop(), 
		   source, 
		   new String[] {
			 "documentation", "*\nStops the tool and disconnects from the topic."
		   });	
		addAnnotation
		  (getTFDisplay3DTool_Connected(), 
		   source, 
		   new String[] {
			 "documentation", "*\nDisplays whether or not the tool if connected to its topic.",
			 "notify", "true",
			 "children", "false",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getTFDisplay3DTool_TopicName(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe name of the topic the provides the transform."
		   });
	}

} //ApogyAddonsROSUIPackageImpl
