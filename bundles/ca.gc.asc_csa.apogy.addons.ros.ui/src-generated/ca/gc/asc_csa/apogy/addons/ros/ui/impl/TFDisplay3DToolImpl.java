/**
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.ros.ui.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.ros.message.MessageListener;

import ca.gc.asc_csa.apogy.addons.impl.Simple3DToolImpl;
import ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSFacade;
import ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSFactory;
import ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSPackage;
import ca.gc.asc_csa.apogy.addons.ros.ROSInterface;
import ca.gc.asc_csa.apogy.addons.ros.ROSNode;
import ca.gc.asc_csa.apogy.addons.ros.ROSPublisherManager;
import ca.gc.asc_csa.apogy.addons.ros.ROSServiceManager;
import ca.gc.asc_csa.apogy.addons.ros.ROSTopicLauncher;
import ca.gc.asc_csa.apogy.addons.ros.ui.Activator;
import ca.gc.asc_csa.apogy.addons.ros.ui.ApogyAddonsROSUIPackage;
import ca.gc.asc_csa.apogy.addons.ros.ui.TFDisplay3DTool;
import ca.gc.asc_csa.apogy.addons.ros.utilities.GeometryUtils;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.math.Matrix4x4;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.TransformNode;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import geometry_msgs.Transform;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>TF Display3 DTool</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl#getServiceManager <em>Service Manager</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl#getTopicLauncher <em>Topic Launcher</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl#getPublisherManager <em>Publisher Manager</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl#getNode <em>Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl#getToolRosNode <em>Tool Ros Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl#isConnected <em>Connected</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ui.impl.TFDisplay3DToolImpl#getTopicName <em>Topic Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TFDisplay3DToolImpl extends Simple3DToolImpl implements TFDisplay3DTool 
{
	private TransformListener transformListener;
	private TransformNode transformNode;
	
	/**
	 * The cached value of the '{@link #getServiceManager() <em>Service Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServiceManager()
	 * @generated
	 * @ordered
	 */
	protected ROSServiceManager serviceManager;
	/**
	 * The cached value of the '{@link #getTopicLauncher() <em>Topic Launcher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopicLauncher()
	 * @generated
	 * @ordered
	 */
	protected ROSTopicLauncher topicLauncher;
	/**
	 * The cached value of the '{@link #getPublisherManager() <em>Publisher Manager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPublisherManager()
	 * @generated
	 * @ordered
	 */
	protected ROSPublisherManager publisherManager;
	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected ROSNode node;
	/**
	 * The cached value of the '{@link #getToolRosNode() <em>Tool Ros Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToolRosNode()
	 * @generated
	 * @ordered
	 */
	protected ROSNode toolRosNode;
	/**
	 * The default value of the '{@link #isConnected() <em>Connected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConnected()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONNECTED_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isConnected() <em>Connected</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isConnected()
	 * @generated
	 * @ordered
	 */
	protected boolean connected = CONNECTED_EDEFAULT;
	/**
	 * The default value of the '{@link #getTopicName() <em>Topic Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopicName()
	 * @generated
	 * @ordered
	 */
	protected static final String TOPIC_NAME_EDEFAULT = "";
	/**
	 * The cached value of the '{@link #getTopicName() <em>Topic Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTopicName()
	 * @generated
	 * @ordered
	 */
	protected String topicName = TOPIC_NAME_EDEFAULT;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TFDisplay3DToolImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsROSUIPackage.Literals.TF_DISPLAY3_DTOOL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSServiceManager getServiceManager() {
		if (serviceManager != null && serviceManager.eIsProxy()) {
			InternalEObject oldServiceManager = (InternalEObject)serviceManager;
			serviceManager = (ROSServiceManager)eResolveProxy(oldServiceManager);
			if (serviceManager != oldServiceManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__SERVICE_MANAGER, oldServiceManager, serviceManager));
			}
		}
		return serviceManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSServiceManager basicGetServiceManager() {
		return serviceManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setServiceManager(ROSServiceManager newServiceManager) {
		ROSServiceManager oldServiceManager = serviceManager;
		serviceManager = newServiceManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__SERVICE_MANAGER, oldServiceManager, serviceManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSTopicLauncher getTopicLauncher() {
		if (topicLauncher != null && topicLauncher.eIsProxy()) {
			InternalEObject oldTopicLauncher = (InternalEObject)topicLauncher;
			topicLauncher = (ROSTopicLauncher)eResolveProxy(oldTopicLauncher);
			if (topicLauncher != oldTopicLauncher) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER, oldTopicLauncher, topicLauncher));
			}
		}
		return topicLauncher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSTopicLauncher basicGetTopicLauncher() {
		return topicLauncher;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopicLauncher(ROSTopicLauncher newTopicLauncher) {
		ROSTopicLauncher oldTopicLauncher = topicLauncher;
		topicLauncher = newTopicLauncher;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER, oldTopicLauncher, topicLauncher));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSPublisherManager getPublisherManager() {
		if (publisherManager != null && publisherManager.eIsProxy()) {
			InternalEObject oldPublisherManager = (InternalEObject)publisherManager;
			publisherManager = (ROSPublisherManager)eResolveProxy(oldPublisherManager);
			if (publisherManager != oldPublisherManager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER, oldPublisherManager, publisherManager));
			}
		}
		return publisherManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSPublisherManager basicGetPublisherManager() {
		return publisherManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublisherManager(ROSPublisherManager newPublisherManager) {
		ROSPublisherManager oldPublisherManager = publisherManager;
		publisherManager = newPublisherManager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER, oldPublisherManager, publisherManager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSNode getNode() {
		if (node != null && node.eIsProxy()) {
			InternalEObject oldNode = (InternalEObject)node;
			node = (ROSNode)eResolveProxy(oldNode);
			if (node != oldNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__NODE, oldNode, node));
			}
		}
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSNode basicGetNode() {
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNode(ROSNode newNode) {
		ROSNode oldNode = node;
		node = newNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__NODE, oldNode, node));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ROSNode getToolRosNode() {
		return toolRosNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetToolRosNode(ROSNode newToolRosNode, NotificationChain msgs) {
		ROSNode oldToolRosNode = toolRosNode;
		toolRosNode = newToolRosNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE, oldToolRosNode, newToolRosNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToolRosNode(ROSNode newToolRosNode) {
		if (newToolRosNode != toolRosNode) {
			NotificationChain msgs = null;
			if (toolRosNode != null)
				msgs = ((InternalEObject)toolRosNode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE, null, msgs);
			if (newToolRosNode != null)
				msgs = ((InternalEObject)newToolRosNode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE, null, msgs);
			msgs = basicSetToolRosNode(newToolRosNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE, newToolRosNode, newToolRosNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConnected(boolean newConnected) {
		boolean oldConnected = connected;
		connected = newConnected;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__CONNECTED, oldConnected, connected));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTopicName() {
		return topicName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTopicName(String newTopicName) {
		String oldTopicName = topicName;
		topicName = newTopicName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_NAME, oldTopicName, topicName));
	}
	
	@Override
	public void setActive(boolean newActive) {
		
		if(!isActive() && newActive)
		{
			start();
		}		
		else if(isActive() && !newActive)
		{
			stop();
		}
		super.setActive(newActive);
	}
	
	@Override
	public void setVisible(boolean newVisible) 
	{	
		super.setVisible(newVisible);
		
		NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(transformNode);
		if(nodePresentation != null)
		{
			// Toggle visibility flag.
			nodePresentation.setVisible(isVisible() && isActive());
		}
	}
	
	@Override
	public void setRootNode(Node newRootNode) 
	{		
		if(getRootNode() instanceof GroupNode)
		{
			GroupNode previousGroupNode = (GroupNode) getRootNode();
			previousGroupNode.getChildren().remove(getTransformNode());
		}
				
		super.setRootNode(newRootNode);
						
		if(newRootNode instanceof GroupNode)
		{
			GroupNode newGroupNode = (GroupNode) newRootNode;
			newGroupNode.getChildren().add(getTransformNode());
		}				
	}
	
	@Override
	public void dispose() 
	{
		// Detach the transform node from root.
		if(transformNode != null && transformNode.getParent() instanceof GroupNode)
		{
			((GroupNode) transformNode.getParent()).getChildren().remove(transformNode);
			transformNode.setParent(null);
			transformNode = null;
		}			
		
		super.dispose();
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean start() 
	{
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "start()", EventSeverity.INFO);		

		// Initialize the ROS node.
		ROSNode node = ApogyAddonsROSFactory.eINSTANCE.createROSNode();
		node.setNodeName( "/TFDisplayClient" + ApogyAddonsROSFacade.INSTANCE.getNodeNamePrefix());
		node.setEnableAutoRestartOnConnectionLost( false );

		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "RosNode Initializing...", EventSeverity.INFO);
		try 
		{
			node.initialize();
		} 
		catch (Exception e) 
		{
			throw new RuntimeException("Unable to initialize the TFDisplay3DTool ROS Client");
		}

		setNode( node );
		node.register( this, false );

		node.eAdapters().add( new AdapterImpl()
		{
			@Override
			public void notifyChanged( Notification msg )
			{
				if( msg.getFeatureID( ROSNode.class ) == ApogyAddonsROSPackage.ROS_NODE__CONNECTED )
				{
					boolean connected = msg.getNewBooleanValue();
					if( connected )
					{
						Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "Connected to the topic.", EventSeverity.INFO);
						
						// TODO : TRANSACTION
						setConnected(true);
					}
				}
			}
		} );

		node.start();
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean stop() 
	{
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "stop()", EventSeverity.INFO);		
		if( getNode() != null )
		{
			try
			{
				// Shuts down the ROS Node.
				getNode().shutdown();

				// Clears the ROS node.
				setNode( null );

			}
			catch( Throwable t )
			{
				t.printStackTrace();				
				Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "AbstractURManipulatorROS stop() failed!", EventSeverity.ERROR, t);
				
				// TODO : TRANSACTION
				setConnected(false);
				
				return false;
			}
		}
		
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "AbstractURManipulatorROS stop() completed.", EventSeverity.OK);
		
		// TODO : TRANSACTION
		setConnected(false);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void rosInit() 
	{
		Logger.INSTANCE.log(Activator.PLUGIN_ID, this, "rosInit() on topic <" + getTopicName() + ">..." , EventSeverity.INFO);
		
		// Topics Initialization.
		topicLauncher.createListener(getTopicName(), geometry_msgs.Transform._TYPE, new TransformListener(this));

		if( !topicLauncher.isRunning() ) topicLauncher.launch();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE:
				return basicSetToolRosNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__SERVICE_MANAGER:
				if (resolve) return getServiceManager();
				return basicGetServiceManager();
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER:
				if (resolve) return getTopicLauncher();
				return basicGetTopicLauncher();
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER:
				if (resolve) return getPublisherManager();
				return basicGetPublisherManager();
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__NODE:
				if (resolve) return getNode();
				return basicGetNode();
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE:
				return getToolRosNode();
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__CONNECTED:
				return isConnected();
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_NAME:
				return getTopicName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__SERVICE_MANAGER:
				setServiceManager((ROSServiceManager)newValue);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER:
				setTopicLauncher((ROSTopicLauncher)newValue);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER:
				setPublisherManager((ROSPublisherManager)newValue);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__NODE:
				setNode((ROSNode)newValue);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE:
				setToolRosNode((ROSNode)newValue);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__CONNECTED:
				setConnected((Boolean)newValue);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_NAME:
				setTopicName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__SERVICE_MANAGER:
				setServiceManager((ROSServiceManager)null);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER:
				setTopicLauncher((ROSTopicLauncher)null);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER:
				setPublisherManager((ROSPublisherManager)null);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__NODE:
				setNode((ROSNode)null);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE:
				setToolRosNode((ROSNode)null);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__CONNECTED:
				setConnected(CONNECTED_EDEFAULT);
				return;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_NAME:
				setTopicName(TOPIC_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__SERVICE_MANAGER:
				return serviceManager != null;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER:
				return topicLauncher != null;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER:
				return publisherManager != null;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__NODE:
				return node != null;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOOL_ROS_NODE:
				return toolRosNode != null;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__CONNECTED:
				return connected != CONNECTED_EDEFAULT;
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_NAME:
				return TOPIC_NAME_EDEFAULT == null ? topicName != null : !TOPIC_NAME_EDEFAULT.equals(topicName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ROSInterface.class) {
			switch (derivedFeatureID) {
				case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__SERVICE_MANAGER: return ApogyAddonsROSPackage.ROS_INTERFACE__SERVICE_MANAGER;
				case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER: return ApogyAddonsROSPackage.ROS_INTERFACE__TOPIC_LAUNCHER;
				case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER: return ApogyAddonsROSPackage.ROS_INTERFACE__PUBLISHER_MANAGER;
				case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__NODE: return ApogyAddonsROSPackage.ROS_INTERFACE__NODE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ROSInterface.class) {
			switch (baseFeatureID) {
				case ApogyAddonsROSPackage.ROS_INTERFACE__SERVICE_MANAGER: return ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__SERVICE_MANAGER;
				case ApogyAddonsROSPackage.ROS_INTERFACE__TOPIC_LAUNCHER: return ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__TOPIC_LAUNCHER;
				case ApogyAddonsROSPackage.ROS_INTERFACE__PUBLISHER_MANAGER: return ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__PUBLISHER_MANAGER;
				case ApogyAddonsROSPackage.ROS_INTERFACE__NODE: return ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL__NODE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == ROSInterface.class) {
			switch (baseOperationID) {
				case ApogyAddonsROSPackage.ROS_INTERFACE___ROS_INIT: return ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL___ROS_INIT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL___START:
				return start();
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL___STOP:
				return stop();
			case ApogyAddonsROSUIPackage.TF_DISPLAY3_DTOOL___ROS_INIT:
				rosInit();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (connected: ");
		result.append(connected);
		result.append(", topicName: ");
		result.append(topicName);
		result.append(')');
		return result.toString();
	}

	protected TransformNode getTransformNode() 
	{		
		if(transformNode == null)
		{
			transformNode = ApogyCommonTopologyFactory.eINSTANCE.createTransformNode();
			transformNode.setNodeId("TFDisplay3DTool");
			transformNode.setDescription("TF : <" + getTopicName() + ">");
			
			NodePresentation nodePresentation = ca.gc.asc_csa.apogy.common.topology.ui.Activator.getTopologyPresentationRegistry().getPresentationNode(transformNode);
			if(nodePresentation != null)
			{
				// Toggle visibility flag.
				nodePresentation.setVisible(isVisible() && isActive());
			}
		}
		
		return transformNode;
	}
	
	protected TransformListener getTransformListener() 
	{
		if(transformListener == null)
		{
			transformListener = new TransformListener(this);
		}
		return transformListener;
	}
	
	private class TransformListener implements MessageListener<geometry_msgs.Transform> 
	{
		private TFDisplay3DTool tfDisplay3DTool;
		
		public TransformListener(TFDisplay3DTool newTFDisplay3DTool) 
		{
			tfDisplay3DTool = newTFDisplay3DTool;
		}
		
		@Override
		public void onNewMessage(Transform msg) 
		{
			if(getTransformNode() != null && tfDisplay3DTool.isActive())
			{
				Matrix4x4 matrix = GeometryUtils.rosTransformToMatrix(msg);
				getTransformNode().setTransformation(matrix.asMatrix4d());
			}
		}		
	}
} //TFDisplay3DToolImpl
