package ca.gc.asc_csa.apogy.addons.ros.ui.composites;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.addons.ros.ApogyROSRegistry;
import ca.gc.asc_csa.apogy.addons.ros.ROSInterface;


public class ROSInterfacesComposite extends Composite 
{
	private FormToolkit formToolkit = new FormToolkit(Display.getCurrent());	
	
	private ApogyROSRegistry apogyROSRegistry;
	private Adapter apogyROSRegistryAdapter;
	
	private ROSInterfaceListComposite rosInterfaceListComposite;
	private ROSListenerListComposite rosListenerListComposite;
	private ROSServiceListComposite rosServiceListComposite;
	
	private ROSInterface selectedROSInterface = null;
	
	public ROSInterfacesComposite(Composite parent, int style) 
	{
		super(parent, style);
		
		addDisposeListener(new DisposeListener() 
		{
			public void widgetDisposed(DisposeEvent e) 
			{								
				if(apogyROSRegistry != null) apogyROSRegistry.eAdapters().add(getApogyROSRegistryAdapter());
				
				formToolkit.dispose();								
			}
		});
		setLayout(new GridLayout(1, true));
		
		ScrolledForm scrldfrmNewScrolledform = formToolkit.createScrolledForm(this);
		scrldfrmNewScrolledform.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		scrldfrmNewScrolledform.setShowFocusedControl(true);
		formToolkit.paintBordersFor(scrldfrmNewScrolledform);
		scrldfrmNewScrolledform.setText(null);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 2;
			scrldfrmNewScrolledform.getBody().setLayout(tableWrapLayout);
		}
		
		
		// ROS Interfaces Section.
		Section sctnROSInterfaces = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnROSInterfaces = new TableWrapData(TableWrapData.FILL, TableWrapData.FILL, 2, 1);
		twd_sctnROSInterfaces.grabHorizontal = true;
		twd_sctnROSInterfaces.valign = TableWrapData.FILL;		
		twd_sctnROSInterfaces.grabVertical = true;
		sctnROSInterfaces.setLayoutData(twd_sctnROSInterfaces);
		formToolkit.paintBordersFor(sctnROSInterfaces);
		sctnROSInterfaces.setText("ROS Interfaces");
		
		rosInterfaceListComposite = new ROSInterfaceListComposite(sctnROSInterfaces, SWT.NONE) 
		{
			@Override
			protected void newROSInterfaceSelected(ROSInterface rosInterface)
			{					
				updateSelectedROSInterface(rosInterface);								
			}
		};		
		
		formToolkit.adapt(rosInterfaceListComposite);
		formToolkit.paintBordersFor(rosInterfaceListComposite);
		sctnROSInterfaces.setClient(rosInterfaceListComposite);
	
		// Topics Section.
		Section sctnTopics = formToolkit.createSection(scrldfrmNewScrolledform.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnOrigin = new TableWrapData(TableWrapData.FILL, TableWrapData.FILL, 1, 1);
		twd_sctnOrigin.grabHorizontal = true;
		twd_sctnOrigin.align = TableWrapData.FILL | TableWrapData.FILL;
		twd_sctnOrigin.grabVertical = true;
		sctnTopics.setLayoutData(twd_sctnOrigin);
		formToolkit.paintBordersFor(sctnTopics);
		sctnTopics.setText("Topics");
		sctnTopics.setExpanded(true);
		
		rosListenerListComposite = new ROSListenerListComposite(sctnTopics, SWT.NONE);
		formToolkit.adapt(rosListenerListComposite);
		formToolkit.paintBordersFor(rosListenerListComposite);
		sctnTopics.setClient(rosListenerListComposite);
		
		
		// Services Section.
		Section sctnServices = formToolkit.createSection(scrldfrmNewScrolledform.getBody(),Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDetails = new TableWrapData(TableWrapData.FILL, TableWrapData.FILL, 1, 1);
		twd_sctnDetails.grabHorizontal = true;
		twd_sctnDetails.align = TableWrapData.FILL | TableWrapData.FILL;
		twd_sctnDetails.grabVertical = true;
		sctnServices.setLayoutData(twd_sctnDetails);
		formToolkit.paintBordersFor(sctnServices);
		sctnServices.setText("Services");
		sctnServices.setExpanded(true);
		
		rosServiceListComposite = new ROSServiceListComposite(sctnServices, SWT.NONE);	
		formToolkit.adapt(rosServiceListComposite);
		formToolkit.paintBordersFor(rosServiceListComposite);
		sctnServices.setClient(rosServiceListComposite);		
	}

	public ApogyROSRegistry getApogyROSRegistry() 
	{
		return apogyROSRegistry;
	}

	public void setApogyROSRegistry(ApogyROSRegistry apogyROSRegistry) 
	{
		if(this.apogyROSRegistry != null) apogyROSRegistry.eAdapters().remove(getApogyROSRegistryAdapter());
		
		if(!rosInterfaceListComposite.isDisposed()) 
		{
			if(apogyROSRegistry != null)
			{
				rosInterfaceListComposite.setRosInterfaceList(apogyROSRegistry.getRosInterfaceList());
			}
		}
		
		this.apogyROSRegistry = apogyROSRegistry;
		
		if(apogyROSRegistry != null) apogyROSRegistry.eAdapters().add(getApogyROSRegistryAdapter());
	}	
	
	public ROSInterface getSelectedROSInterface() 
	{
		return selectedROSInterface;
	}
	
	protected void updateSelectedROSInterface(ROSInterface rosInterface)
	{
		selectedROSInterface = rosInterface;
								
		if(rosListenerListComposite != null && !rosListenerListComposite.isDisposed())
		{
			rosListenerListComposite.setROSInterface(rosInterface);						
		}	
		
		if(rosServiceListComposite != null && !rosServiceListComposite.isDisposed())
		{			
			rosServiceListComposite.setROSInterface(rosInterface);
		}
	}
	
	protected Adapter getApogyROSRegistryAdapter()
	{
		if(apogyROSRegistryAdapter == null)
		{
			apogyROSRegistryAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					rosInterfaceListComposite.setRosInterfaceList(apogyROSRegistry.getRosInterfaceList());
				}
			};
		}
		
		return apogyROSRegistryAdapter;
	}
}
