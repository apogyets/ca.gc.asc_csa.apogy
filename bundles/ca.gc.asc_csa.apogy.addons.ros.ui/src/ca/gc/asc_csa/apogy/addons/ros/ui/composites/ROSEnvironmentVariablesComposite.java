package ca.gc.asc_csa.apogy.addons.ros.ui.composites;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSFacade;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;

public class ROSEnvironmentVariablesComposite extends Composite 
{
	public ROSEnvironmentVariablesComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		Label lblRosMasterUri = new Label(this, SWT.NONE);
		lblRosMasterUri.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRosMasterUri.setText("ROS Master URI:");
		
		Label lblRosMasterURIValue = new Label(this, SWT.BORDER);
		GridData gd_lblRosMasterURIValue = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblRosMasterURIValue.widthHint = 250;
		gd_lblRosMasterURIValue.minimumWidth = 250;
		lblRosMasterURIValue.setLayoutData(gd_lblRosMasterURIValue);
		lblRosMasterURIValue.setText(ApogyAddonsROSFacade.INSTANCE.getROSMasterURI());
		
		Label lblRosIp = new Label(this, SWT.NONE);
		lblRosIp.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRosIp.setText("ROS IP:");
		
		Label lblRosipvalue = new Label(this, SWT.BORDER);
		GridData gd_lblRosipvalue = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblRosipvalue.minimumWidth = 250;
		gd_lblRosipvalue.widthHint = 250;
		lblRosipvalue.setLayoutData(gd_lblRosipvalue);
		lblRosipvalue.setText(ApogyAddonsROSFacade.INSTANCE.getROSIp());
		
		Label lblRosHostName = new Label(this, SWT.NONE);
		lblRosHostName.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRosHostName.setText("ROS Host Name:");
		
		Label lblRoshostnamevalue = new Label(this, SWT.BORDER);
		GridData gd_lblRoshostnamevalue = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_lblRoshostnamevalue.widthHint = 250;
		gd_lblRoshostnamevalue.minimumWidth = 250;
		lblRoshostnamevalue.setLayoutData(gd_lblRoshostnamevalue);
		lblRoshostnamevalue.setText(ApogyAddonsROSFacade.INSTANCE.getROSHostname());
	}

}
