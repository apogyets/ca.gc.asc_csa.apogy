package ca.gc.asc_csa.apogy.addons.sensors.imaging.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.common.topology.ui.SceneObject;

public interface ImageSnapshotSceneObject extends SceneObject 
{		
	public MeshPresentationMode getPresentationMode();
	public void setPresentationMode(MeshPresentationMode presentationMode);
		
	public void setFOVVisible(boolean visible);	
	public boolean isFOVVisible();
	
	public void setAxisVisible(boolean visible);	
	public boolean isAxisVisible();
	
	public void setAxisLength(double length);
	public double getAxisLength();
	
	public boolean isImageProjectionEnabled();
	public void setImageProjectionEnabled(boolean enabled);
		
	public boolean isImageProjectionOnFOVEnabled(); 
	public void setImageProjectionOnFOVEnabled(boolean enabled); 
}
