package ca.gc.asc_csa.apogy.addons.ros.tools;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.Map;

import org.ros.EnvironmentVariables;
import org.ros.internal.message.GenerateInterfaces;

public class GenerateROSInterfaces {

	public static void main(String[] args) 
	{
		// Ensure that the Environment Variable ROS_PACKAGE_PATH refers to the location of the ROS package folder.
		
		System.out.println("Generating ROS Java Classes");
		
		Map<String, String> env = System.getenv();
		String rosPackagePath = env.get(EnvironmentVariables.ROS_PACKAGE_PATH);
		
		System.out.println("\t Source ROS Package         : <" + rosPackagePath + ">");
		System.out.println("\t Java Classes output folder : <" + args[0] + ">.");
		
		GenerateInterfaces.main(args);
		
		System.out.println("Generation Completed.");
	}

}
