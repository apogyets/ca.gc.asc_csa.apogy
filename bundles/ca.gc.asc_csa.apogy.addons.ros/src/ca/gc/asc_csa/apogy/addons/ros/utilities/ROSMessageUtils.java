package ca.gc.asc_csa.apogy.addons.ros.utilities;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.Date;

public class ROSMessageUtils 
{
	/**
	 * Converts a ROS Header Time to a Date.
	 * @param time The ROS Time.
	 * @return The associated Date.
	 */
	public static Date convertToDate(org.ros.message.Time time)
	{
		long milliseconds = Math.round(time.totalNsecs() * 0.001); 
		return new Date(milliseconds);
	}
}
