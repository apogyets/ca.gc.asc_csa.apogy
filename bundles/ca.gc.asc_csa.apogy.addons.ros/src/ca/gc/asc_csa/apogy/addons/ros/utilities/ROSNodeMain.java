package ca.gc.asc_csa.apogy.addons.ros.utilities;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.List;

import org.ros.internal.loader.CommandLineLoader;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.DefaultNodeMainExecutor;
import org.ros.node.NodeConfiguration;
import org.ros.node.NodeMain;
import org.ros.node.NodeMainExecutor;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import ca.gc.asc_csa.apogy.addons.ros.Activator;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

public class ROSNodeMain extends AbstractNodeMain
{
	private String nodeName;
	
	public ROSNodeMain ( String nodeName )
	{
		this.nodeName = nodeName;
	}
	
	@Override
	public GraphName getDefaultNodeName()
	{
		return GraphName.of(nodeName);
	}
	
	public void start ( )
	{
		try
		{
			// RosRun.main(this);
			
			// This is the code that was in the RosRun.main(Object obj) method that is not available in the indigo branch of rosJava.			
			CommandLineLoader loader = new CommandLineLoader(Lists.newArrayList(new String[] { this.getClass().getName() }));
			//String nodeClassName = loader.getNodeClassName();
			//System.out.println("Loading node class: " + loader.getNodeClassName());
			NodeConfiguration nodeConfiguration = loader.build();

			NodeMain nodeMain = null;

			nodeMain = (NodeMain) this;

			Preconditions.checkState(nodeMain != null);
			NodeMainExecutor nodeMainExecutor = DefaultNodeMainExecutor.newDefault();
			nodeMainExecutor.execute(nodeMain, nodeConfiguration);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Event called when the node is connected to the ROS master <br>
	 * <br>
	 * Start the subscriber handler and the service clients handler
	 */
	@Override
	public void onStart(ConnectedNode connectedNode) 
	{
		Logger.INSTANCE.log(Activator.ID, this, "Node connected", EventSeverity.INFO);
		
		for ( NodeStartedListener l : nodeStartedListeners )
		{
			l.nodeStarted(connectedNode);
		}
	}
	
	private List<NodeStartedListener> nodeStartedListeners = new ArrayList<NodeStartedListener>();
	
	public interface NodeStartedListener
	{
		public void nodeStarted(ConnectedNode connectedNode);
	}
	
	public void addNodeStartedListener(NodeStartedListener l)
	{
		nodeStartedListeners.add(l);
	}
}
