package ca.gc.asc_csa.apogy.addons.ros.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.HashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.ros.internal.message.Message;
import org.ros.namespace.GraphName;
import org.ros.node.ConnectedNode;

import ca.gc.asc_csa.apogy.addons.ros.Activator;
import ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSFactory;
import ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSPackage;
import ca.gc.asc_csa.apogy.addons.ros.ROSNode;
import ca.gc.asc_csa.apogy.addons.ros.ROSService;
import ca.gc.asc_csa.apogy.addons.ros.ROSServiceManager;
import ca.gc.asc_csa.apogy.addons.ros.ROSServiceState;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Manager</b></em>'.
 * <!-- end-user-doc --> * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.impl.ROSServiceManagerImpl#getServices <em>Services</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.impl.ROSServiceManagerImpl#getNode <em>Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.impl.ROSServiceManagerImpl#isRunning <em>Running</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ROSServiceManagerImpl extends MinimalEObjectImpl.Container implements ROSServiceManager
{
	public static long WAIT_TIME_BETWEEN_TRIES_MS = 2000;
	private HashMap<String, ServiceLaunchRunnable> serviceLaunchRunnableList = new HashMap<String, ServiceLaunchRunnable>();
	
	private Adapter adapter;
	
	private Job serviceMonitoringJob = null;
	
	/**
	 * The cached value of the '{@link #getServices() <em>Services</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getServices()
	 * @generated
	 * @ordered
	 */
	protected HashMap<String, ROSService<?, ?>> services;

	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected ROSNode node;

	/**
	 * The default value of the '{@link #isRunning() <em>Running</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #isRunning()
	 * @generated
	 * @ordered
	 */
	protected static final boolean RUNNING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRunning() <em>Running</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #isRunning()
	 * @generated
	 * @ordered
	 */
	protected boolean running = RUNNING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected ROSServiceManagerImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return ApogyAddonsROSPackage.Literals.ROS_SERVICE_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public HashMap<String, ROSService<?, ?>> getServices()
	{
		if ( services == null )
			services = new HashMap<String, ROSService<?, ?>>();
		return services;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setServices(HashMap<String, ROSService<?, ?>> newServices)
	{
		HashMap<String, ROSService<?, ?>> oldServices = services;
		services = newServices;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__SERVICES, oldServices, services));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ROSNode getNode()
	{
		if (node != null && node.eIsProxy()) {
			InternalEObject oldNode = (InternalEObject)node;
			node = (ROSNode)eResolveProxy(oldNode);
			if (node != oldNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__NODE, oldNode, node));
			}
		}
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ROSNode basicGetNode()
	{
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setNode(ROSNode newNode)
	{
		ROSNode oldNode = node;
		node = newNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__NODE, oldNode, node));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public boolean isRunning()
	{
		return running;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void setRunning(boolean newRunning)
	{
		boolean oldRunning = running;
		running = newRunning;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__RUNNING, oldRunning, running));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void createService(String serviceName, String serviceType)
	{
		createService(serviceName, serviceType, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void createService(String serviceName, String serviceType, boolean disconnectOnTimeout)
	{
		ROSService<Message, Message> service = ApogyAddonsROSFactory.eINSTANCE.createROSService();
		service.setServiceName(serviceName);
		service.setServiceType(serviceType);
		service.setDisconnectOnTimeout(disconnectOnTimeout);
		getServices().put(serviceName, service);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void createService(String serviceName, String serviceType, boolean disconnectOnTimeout, boolean enableAutoReconnect) 
	{
		ROSService<Message, Message> service = ApogyAddonsROSFactory.eINSTANCE.createROSService();
		service.setServiceName(serviceName);
		service.setServiceType(serviceType);
		service.setDisconnectOnTimeout(disconnectOnTimeout);
		service.setEnableAutoReconnect(enableAutoReconnect);
		getServices().put(serviceName, service);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@SuppressWarnings("unchecked")
	public <Request extends Message, Response extends Message> ROSService<Request, Response> getService(String serviceName)
	{
		if (getServices().containsKey(serviceName))
		{ 
			ROSService<Request, Response> service = (ROSService<Request, Response>)((Object)getServices().get(serviceName));			
			return service;			
		}		

		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public <Request extends Message, Response extends Message> Request createRequestMessage(String serviceName)
	{
		ROSService<Request,Response> service = getService(serviceName);
		if (service != null)
			return service.newRequestMessage();
		else
			return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public <Request extends Message, Response extends Message> Response callService(String serviceName, Request request)
	{
		return callService(serviceName, request, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public <Request extends Message, Response extends Message> Response callService(String serviceName, Request request, int timeout) throws Exception
	{
		ROSService<Request,Response> service = getService(serviceName);
		if ( service != null )
			return service.call(request, false, timeout);
		else
			return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public <Request extends Message, Response extends Message> Response callService(String serviceName)
	{
		return callService(serviceName, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public <Request extends Message, Response extends Message> Response callService(String serviceName, Request request, boolean enableLogging)
	{
		ROSService<Request,Response> service = getService(serviceName);
		if (service != null && service.isLaunched())
			return service.call(request, enableLogging);
		else
			return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public <Request extends Message, Response extends Message> Response callService(String serviceName, Request request, boolean enableLogging, int timeout) throws Exception 
	{		
		ROSService<Request,Response> service = getService(serviceName);
		if(service != null && service.isLaunched())
		{
			return service.call(request, enableLogging, timeout);
		}
		else 
		{
			throw new RuntimeException("Service <" + serviceName + "> is not running !");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public <Request extends Message, Response extends Message> Response callService(String serviceName, boolean enableLogging)
	{
		ROSService<Request,Response> service = getService(serviceName);
		Request request = createRequestMessage(serviceName);		

		if(service != null && service.isLaunched())
		{
			return service.call(request, enableLogging);
		}
		else
		{
			throw new RuntimeException("Service <" + serviceName + "> is not running !");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void launch()
	{
		new Thread ()
		{
			public void run()
			{
				String listenerListMsg = "";
				for(ROSService<?,?> service : getServices().values())
				{
					listenerListMsg += service.getServiceName() + "\n";
				}
				  
				Logger.INSTANCE.log(Activator.ID, this, "Starts launching the following services : \n" + listenerListMsg, EventSeverity.INFO);
				  
				
				for (ROSService<?,?> service : getServices().values())
				{
					try
					{
						Logger.INSTANCE.log(Activator.ID, this, "Launching service <" + service.getServiceName() + ">...", EventSeverity.INFO);
						
						// Listens for changes on service.
						service.eAdapters().add(getAdapter());
						
						// Does the service launching in a thread.
						ServiceLaunchRunnable topicLaunchRunnable = new ServiceLaunchRunnable(getNode(), service);
						serviceLaunchRunnableList.put(service.getServiceName(), topicLaunchRunnable);
						Thread thread = new Thread(topicLaunchRunnable);
						thread.start();
					} 					
					catch (Exception e)
					{
						Logger.INSTANCE.log(Activator.ID, this, "Could not launch service <" + service.getServiceName() + "> !", EventSeverity.ERROR, e);
						e.printStackTrace();
						// TODO getNode().setConnected(false);
					}
				}
				setRunning(true);
				Logger.INSTANCE.log(Activator.ID, this, "Services launch completed.", EventSeverity.OK);
			}
		}.start();
		
		// Start the monitoring job.
		getServiceStateMonitoring().cancel();
		getServiceStateMonitoring().schedule();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void stop()
	{
		// Stops all ServiceLaunchRunnable that still attempting to launch.
		for(ServiceLaunchRunnable serviceLaunchRunnable : serviceLaunchRunnableList.values())
		{
			serviceLaunchRunnable.stop();		
		}
		serviceLaunchRunnableList.clear();
		
		
		for (ROSService<?,?> service : getServices().values())
		{
			service.eAdapters().remove(getAdapter());
			service.stop();
			service.setServiceState(ROSServiceState.STOPPED);
		}
		
		getServiceStateMonitoring().cancel();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType)
	{
		switch (featureID) {
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__SERVICES:
				return getServices();
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__NODE:
				if (resolve) return getNode();
				return basicGetNode();
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__RUNNING:
				return isRunning();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue)
	{
		switch (featureID) {
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__SERVICES:
				setServices((HashMap<String, ROSService<?, ?>>)newValue);
				return;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__NODE:
				setNode((ROSNode)newValue);
				return;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__RUNNING:
				setRunning((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void eUnset(int featureID)
	{
		switch (featureID) {
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__SERVICES:
				setServices((HashMap<String, ROSService<?, ?>>)null);
				return;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__NODE:
				setNode((ROSNode)null);
				return;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__RUNNING:
				setRunning(RUNNING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID)
	{
		switch (featureID) {
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__SERVICES:
				return services != null;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__NODE:
				return node != null;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER__RUNNING:
				return running != RUNNING_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException
	{
		switch (operationID) {
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CREATE_SERVICE__STRING_STRING:
				createService((String)arguments.get(0), (String)arguments.get(1));
				return null;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CREATE_SERVICE__STRING_STRING_BOOLEAN:
				createService((String)arguments.get(0), (String)arguments.get(1), (Boolean)arguments.get(2));
				return null;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CREATE_SERVICE__STRING_STRING_BOOLEAN_BOOLEAN:
				createService((String)arguments.get(0), (String)arguments.get(1), (Boolean)arguments.get(2), (Boolean)arguments.get(3));
				return null;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___GET_SERVICE__STRING:
				return getService((String)arguments.get(0));
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CREATE_REQUEST_MESSAGE__STRING:
				return createRequestMessage((String)arguments.get(0));
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CALL_SERVICE__STRING_MESSAGE:
				return callService((String)arguments.get(0), (Message)arguments.get(1));
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CALL_SERVICE__STRING_MESSAGE_INT:
				try {
					return callService((String)arguments.get(0), (Message)arguments.get(1), (Integer)arguments.get(2));
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CALL_SERVICE__STRING:
				return callService((String)arguments.get(0));
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CALL_SERVICE__STRING_MESSAGE_BOOLEAN:
				return callService((String)arguments.get(0), (Message)arguments.get(1), (Boolean)arguments.get(2));
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CALL_SERVICE__STRING_MESSAGE_BOOLEAN_INT:
				try {
					return callService((String)arguments.get(0), (Message)arguments.get(1), (Boolean)arguments.get(2), (Integer)arguments.get(3));
				}
				catch (Throwable throwable) {
					throw new InvocationTargetException(throwable);
				}
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___CALL_SERVICE__STRING_BOOLEAN:
				return callService((String)arguments.get(0), (Boolean)arguments.get(1));
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___LAUNCH:
				launch();
				return null;
			case ApogyAddonsROSPackage.ROS_SERVICE_MANAGER___STOP:
				stop();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public String toString()
	{
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (services: ");
		result.append(services);
		result.append(", running: ");
		result.append(running);
		result.append(')');
		return result.toString();
	}
		
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ROSService)
					{
						ROSService<?,?> service = (ROSService<?,?>) msg.getNotifier();
												
						int featureID = msg.getFeatureID(ROSService.class);
						
						if(featureID == ApogyAddonsROSPackage.ROS_SERVICE__SERVICE_STATE)
						{
							ROSServiceState oldState = (ROSServiceState) msg.getOldValue();
							ROSServiceState newState = (ROSServiceState) msg.getNewValue();
							
							if(newState == ROSServiceState.FAILED && oldState == ROSServiceState.READY)
							{					
								Logger.INSTANCE.log(Activator.ID, this, "Service <" + service.getServiceName() + "> failed, attempting to re-connect...", EventSeverity.WARNING);
								
								// Stops previous ServiceLaunchRunnable.
								ServiceLaunchRunnable oldServiceLaunchRunnable = serviceLaunchRunnableList.get(service.getServiceName());
								
								if(oldServiceLaunchRunnable != null)
								{
									oldServiceLaunchRunnable.stop();
								}
								
								// Restart the service.																								
								ServiceLaunchRunnable newServiceLaunchRunnable = new ServiceLaunchRunnable(getNode(), service);
								serviceLaunchRunnableList.put(service.getServiceName(), newServiceLaunchRunnable);
								Thread thread = new Thread(newServiceLaunchRunnable);
								thread.start();
							}
						}
					}
				}	
			};
		}
		return adapter;
	}

	protected Job getServiceStateMonitoring()
	{
		if(serviceMonitoringJob == null)
		{
			serviceMonitoringJob = new Job("ROSServiceManager Service Monitoring") {
			
			@Override
			protected IStatus run(IProgressMonitor monitor) 
			{
				while(!monitor.isCanceled())
				{
					for(ROSService<?,?> service : getServices().values())
					{
						try
						{
							if(service.isEnableAutoReconnect())
							{
								if(service.getServiceState() == ROSServiceState.READY)
								{		
									try
									{
										ConnectedNode node = getNode().getConnectedNode();
										GraphName resolvedServiceName = node.resolveName(service.getServiceName());
									    URI uri = node.lookupServiceUri(resolvedServiceName);
									    if (uri == null) 
									    {
									    	service.setServiceState(ROSServiceState.FAILED);
									    }
									}
									catch (Throwable t) 
									{
										service.setServiceState(ROSServiceState.FAILED);							
									}
								}
							}
						}
						catch (Exception e) 
						{
							e.printStackTrace();			
						}
					}		
					
					try 
					{
						Thread.sleep(1000);
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
				
				return Status.OK_STATUS;
			}
		};
		
		serviceMonitoringJob.setPriority(Job.LONG);
		}
		
		return serviceMonitoringJob;
	}

	protected class ServiceLaunchRunnable implements Runnable
	{
		private boolean stopRequested = false;
		  
		private ROSNode rosNode = null;
		private ROSService<?,?> service = null;
		  
		  
		public ServiceLaunchRunnable(ROSNode rosNode, ROSService<?,?> service)
		{
			this.rosNode = rosNode;
			this.service = service;
		}
		  
		@Override
		public void run() 
		{		  
			boolean success = false;
			int tries = 0;
			while(!stopRequested && !success)
			{
				try			  			  
				{
					service.setServiceState(ROSServiceState.INITIALIZING);
					
					service.launch(rosNode);					
					success = true;
					
					Logger.INSTANCE.log(Activator.ID, this, "Service <" + service.getServiceName() + "> is running.", EventSeverity.OK);
				}
				catch (Exception e) 
				{		
					service.setServiceState(ROSServiceState.INITIALIZING);
					success = false;
					  		
					if(tries == 0)
					{
						Logger.INSTANCE.log(Activator.ID, this, "Failed to launch service <" + service.getServiceName() + ">, trying again...", EventSeverity.WARNING, e);
					}
				  	
					// Update number of tries.
					tries++;
					
					// Wait a bit before trying again.
					if(!stopRequested)
					{
						try 
						{
							Thread.sleep(WAIT_TIME_BETWEEN_TRIES_MS);
						} 
						catch (InterruptedException e1) 
						{					
							e1.printStackTrace();
						}
					}
					
					
				  }
			  }
		  }
		  
		  public void stop()
		  {
			  stopRequested = true;
		  }
	}
	
	
} //ROSServiceManagerImpl
