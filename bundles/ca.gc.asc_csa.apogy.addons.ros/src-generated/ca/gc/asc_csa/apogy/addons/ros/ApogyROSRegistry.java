/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.addons.ros;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.addons.ros.impl.ApogyROSRegistryImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Apogy ROS Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Class representing the list of all ROSInterfaces currently managed by Apogy.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.addons.ros.ApogyROSRegistry#getRosInterfaceList <em>Ros Interface List</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSPackage#getApogyROSRegistry()
 * @model
 * @generated
 */
public interface ApogyROSRegistry extends EObject 
{
	public ApogyROSRegistry INSTANCE = ApogyROSRegistryImpl.getInstance();

	/**
	 * Returns the value of the '<em><b>Ros Interface List</b></em>' reference list.
	 * The list contents are of type {@link ca.gc.asc_csa.apogy.addons.ros.ROSInterface}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The list off all the ROSInterface currently managed.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Ros Interface List</em>' reference list.
	 * @see ca.gc.asc_csa.apogy.addons.ros.ApogyAddonsROSPackage#getApogyROSRegistry_RosInterfaceList()
	 * @model transient="true"
	 * @generated
	 */
	EList<ROSInterface> getRosInterfaceList();

} // ApogyROSRegistry
