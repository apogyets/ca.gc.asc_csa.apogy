package ca.gc.asc_csa.apogy.common.converters.graphs;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.converters.IConverter;

public class ConverterEdge 
{
	private Class<?> from = null;
	private Class<?> to = null;
	private IConverter converter = null;
	
	public Class<?> getFrom() {
		return from;
	}

	public void setFrom(Class<?> from) {
		this.from = from;
	}

	public Class<?> getTo() {
		return to;
	}

	public void setTo(Class<?> to) {
		this.to = to;
	}

	public IConverter getConverter() {
		return converter;
	}

	public void setConverter(IConverter converter) {
		this.converter = converter;
	}
	
	public ConverterEdge(Class<?> from, Class<?> to, IConverter converter)
	{
		setFrom(from);
		setTo(to);
		setConverter(converter);
	}
}
