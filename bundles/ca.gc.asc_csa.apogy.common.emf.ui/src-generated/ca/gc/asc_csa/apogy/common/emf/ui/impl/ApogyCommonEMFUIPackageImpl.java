package ca.gc.asc_csa.apogy.common.emf.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.TreeFeatureNodeWizardPageProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCommonEMFUIPackageImpl extends EPackageImpl implements ApogyCommonEMFUIPackage
{
  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass apogyCommonEMFUIFacadeEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass selectionBasedTimeSourceEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eClassSettingsEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass mapBasedEClassSettingsEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass wizardPagesProviderEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass wizardPagesProviderRegistryEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass namedDescribedWizardPagesProviderEClass = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass namedSettingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass displayUnitsRegistryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eTypedElementToUnitsKeyValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eTypedElementToUnitsMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass unitsProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass unitsProviderParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass simpleUnitsProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eOperationEParametersUnitsProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eOperationEParametersUnitsProviderParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass decimalFormatRegistryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eTypedElementToFormatStringMapEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eTypedElementToFormatStringKeyValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass formatProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass formatProviderParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass simpleFormatProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eOperationEParametersFormatProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass eOperationEParametersFormatProviderParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass treeFeatureNodeWizardPageProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType hashMapEDataType = null;

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private EDataType colorEDataType = null;

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType listEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType wizardPageEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType mapEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType iWizardPageEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType compoundCommandEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType editingDomainEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType resourceSetEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType mPartEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType eSelectionServiceEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType imageEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType unitConverterMapEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType displayUnitsMapEDataType = null;

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType decimalFormatEDataType = null;

		/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
  private ApogyCommonEMFUIPackageImpl()
  {
		super(eNS_URI, ApogyCommonEMFUIFactory.eINSTANCE);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private static boolean isInited = false;

  /**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonEMFUIPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
  public static ApogyCommonEMFUIPackage init()
  {
		if (isInited) return (ApogyCommonEMFUIPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFUIPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonEMFUIPackageImpl theApogyCommonEMFUIPackage = (ApogyCommonEMFUIPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonEMFUIPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonEMFUIPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonEMFPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonEMFUIPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonEMFUIPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonEMFUIPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonEMFUIPackage.eNS_URI, theApogyCommonEMFUIPackage);
		return theApogyCommonEMFUIPackage;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getApogyCommonEMFUIFacade() {
		return apogyCommonEMFUIFacadeEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getApogyCommonEMFUIFacade_UnitConverterMap() {
		return (EAttribute)apogyCommonEMFUIFacadeEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__GetColorForRange__Ranges() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__GetDisplayUnits__ETypedElement() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__GetDisplayUnits__EOperation_EOperationEParametersUnitsProviderParameters() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__AddUnitsProviderToRegistry__ETypedElement_UnitsProvider() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__ConvertToNativeUnits__Number_Unit_Unit_EClassifier() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(4);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__GetDisplayFormat__ETypedElement() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(5);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__GetDisplayFormat__EOperation_EOperationEParametersFormatProviderParameters() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(6);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__AddFormatProviderToRegistry__ETypedElement_FormatProvider() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(7);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__Named() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(8);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__List() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(9);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__SaveToPersistedState__MPart_String_EObject_ResourceSet() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(10);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__ReadFromPersistedState__MPart_String_ResourceSet() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(11);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonEMFUIFacade__GetImage__EClass() {
		return apogyCommonEMFUIFacadeEClass.getEOperations().get(12);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getSelectionBasedTimeSource() {
		return selectionBasedTimeSourceEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getSelectionBasedTimeSource_Selection() {
		return (EReference)selectionBasedTimeSourceEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getSelectionBasedTimeSource_SelectionService() {
		return (EAttribute)selectionBasedTimeSourceEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getEClassSettings() {
		return eClassSettingsEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getMapBasedEClassSettings() {
		return mapBasedEClassSettingsEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getMapBasedEClassSettings_UserDataMap() {
		return (EAttribute)mapBasedEClassSettingsEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getWizardPagesProvider() {
		return wizardPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getWizardPagesProvider_Pages() {
		return (EAttribute)wizardPagesProviderEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getWizardPagesProvider_EObject() {
		return (EReference)wizardPagesProviderEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getWizardPagesProvider__GetPages__EClass_EClassSettings() {
		return wizardPagesProviderEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getWizardPagesProvider__CreateEObject__EClass_EClassSettings() {
		return wizardPagesProviderEClass.getEOperations().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getWizardPagesProvider__InstantiateWizardPages__EObject_EClassSettings() {
		return wizardPagesProviderEClass.getEOperations().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getWizardPagesProvider__GetPerformFinishCommands__EObject_EClassSettings_EditingDomain() {
		return wizardPagesProviderEClass.getEOperations().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getWizardPagesProvider__GetNextPage__IWizardPage() {
		return wizardPagesProviderEClass.getEOperations().get(4);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getWizardPagesProviderRegistry() {
		return wizardPagesProviderRegistryEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID() {
		return (EAttribute)wizardPagesProviderRegistryEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID() {
		return (EAttribute)wizardPagesProviderRegistryEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID() {
		return (EAttribute)wizardPagesProviderRegistryEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getWizardPagesProviderRegistry_WizardPagesMap() {
		return (EAttribute)wizardPagesProviderRegistryEClass.getEStructuralFeatures().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getWizardPagesProviderRegistry__GetProvider__EClass() {
		return wizardPagesProviderRegistryEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNamedDescribedWizardPagesProvider() {
		return namedDescribedWizardPagesProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNamedSetting() {
		return namedSettingEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getNamedSetting_Parent() {
		return (EReference)namedSettingEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getNamedSetting_ContainingFeature() {
		return (EReference)namedSettingEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getDisplayUnitsRegistry() {
		return displayUnitsRegistryEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getDisplayUnitsRegistry_EntriesMap() {
		return (EReference)displayUnitsRegistryEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getDisplayUnitsRegistry__Save() {
		return displayUnitsRegistryEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getETypedElementToUnitsKeyValue() {
		return eTypedElementToUnitsKeyValueEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getETypedElementToUnitsKeyValue_Key() {
		return (EReference)eTypedElementToUnitsKeyValueEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getETypedElementToUnitsKeyValue_Value() {
		return (EReference)eTypedElementToUnitsKeyValueEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getETypedElementToUnitsMap() {
		return eTypedElementToUnitsMapEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getETypedElementToUnitsMap_Entries() {
		return (EReference)eTypedElementToUnitsMapEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getUnitsProvider() {
		return unitsProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getUnitsProvider__GetProvidedUnit__UnitsProviderParameters() {
		return unitsProviderEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getUnitsProviderParameters() {
		return unitsProviderParametersEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getSimpleUnitsProvider() {
		return simpleUnitsProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getSimpleUnitsProvider_Unit() {
		return (EAttribute)simpleUnitsProviderEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getEOperationEParametersUnitsProvider() {
		return eOperationEParametersUnitsProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getEOperationEParametersUnitsProvider_Map() {
		return (EReference)eOperationEParametersUnitsProviderEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getEOperationEParametersUnitsProviderParameters() {
		return eOperationEParametersUnitsProviderParametersEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getEOperationEParametersUnitsProviderParameters_Param() {
		return (EReference)eOperationEParametersUnitsProviderParametersEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getDecimalFormatRegistry() {
		return decimalFormatRegistryEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getDecimalFormatRegistry_EntriesMap() {
		return (EReference)decimalFormatRegistryEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getDecimalFormatRegistry__Save() {
		return decimalFormatRegistryEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getETypedElementToFormatStringMap() {
		return eTypedElementToFormatStringMapEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getETypedElementToFormatStringMap_Entries() {
		return (EReference)eTypedElementToFormatStringMapEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getETypedElementToFormatStringKeyValue() {
		return eTypedElementToFormatStringKeyValueEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getETypedElementToFormatStringKeyValue_Key() {
		return (EReference)eTypedElementToFormatStringKeyValueEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getETypedElementToFormatStringKeyValue_Value() {
		return (EReference)eTypedElementToFormatStringKeyValueEClass.getEStructuralFeatures().get(1);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getFormatProvider() {
		return formatProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getFormatProvider__GetProvidedFormat__FormatProviderParameters() {
		return formatProviderEClass.getEOperations().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getFormatProviderParameters() {
		return formatProviderParametersEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getSimpleFormatProvider() {
		return simpleFormatProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getSimpleFormatProvider_FormatPattern() {
		return (EAttribute)simpleFormatProviderEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getEOperationEParametersFormatProvider() {
		return eOperationEParametersFormatProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getEOperationEParametersFormatProvider_Map() {
		return (EReference)eOperationEParametersFormatProviderEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getEOperationEParametersFormatProviderParameters() {
		return eOperationEParametersFormatProviderParametersEClass;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getEOperationEParametersFormatProviderParameters_Param() {
		return (EReference)eOperationEParametersFormatProviderParametersEClass.getEStructuralFeatures().get(0);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getTreeFeatureNodeWizardPageProvider() {
		return treeFeatureNodeWizardPageProviderEClass;
	}

		/**
	 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getHashMap() {
		return hashMapEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public EDataType getColor()
  {
		return colorEDataType;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getList() {
		return listEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getWizardPage() {
		return wizardPageEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getMap() {
		return mapEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getIWizardPage() {
		return iWizardPageEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getCompoundCommand() {
		return compoundCommandEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getEditingDomain() {
		return editingDomainEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getResourceSet() {
		return resourceSetEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getMPart() {
		return mPartEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getESelectionService() {
		return eSelectionServiceEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getImage() {
		return imageEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getUnitConverterMap() {
		return unitConverterMapEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getDisplayUnitsMap() {
		return displayUnitsMapEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getDecimalFormat() {
		return decimalFormatEDataType;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonEMFUIFactory getApogyCommonEMFUIFactory() {
		return (ApogyCommonEMFUIFactory)getEFactoryInstance();
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private boolean isCreated = false;

  /**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public void createPackageContents()
  {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		apogyCommonEMFUIFacadeEClass = createEClass(APOGY_COMMON_EMFUI_FACADE);
		createEAttribute(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE__UNIT_CONVERTER_MAP);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___GET_COLOR_FOR_RANGE__RANGES);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_UNITS__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_UNITS__EOPERATION_EOPERATIONEPARAMETERSUNITSPROVIDERPARAMETERS);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___ADD_UNITS_PROVIDER_TO_REGISTRY__ETYPEDELEMENT_UNITSPROVIDER);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___CONVERT_TO_NATIVE_UNITS__NUMBER_UNIT_UNIT_ECLASSIFIER);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_FORMAT__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_FORMAT__EOPERATION_EOPERATIONEPARAMETERSFORMATPROVIDERPARAMETERS);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___ADD_FORMAT_PROVIDER_TO_REGISTRY__ETYPEDELEMENT_FORMATPROVIDER);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___OPEN_DELETE_NAMED_DIALOG__NAMED);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___OPEN_DELETE_NAMED_DIALOG__LIST);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___SAVE_TO_PERSISTED_STATE__MPART_STRING_EOBJECT_RESOURCESET);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___READ_FROM_PERSISTED_STATE__MPART_STRING_RESOURCESET);
		createEOperation(apogyCommonEMFUIFacadeEClass, APOGY_COMMON_EMFUI_FACADE___GET_IMAGE__ECLASS);

		selectionBasedTimeSourceEClass = createEClass(SELECTION_BASED_TIME_SOURCE);
		createEReference(selectionBasedTimeSourceEClass, SELECTION_BASED_TIME_SOURCE__SELECTION);
		createEAttribute(selectionBasedTimeSourceEClass, SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE);

		eClassSettingsEClass = createEClass(ECLASS_SETTINGS);

		mapBasedEClassSettingsEClass = createEClass(MAP_BASED_ECLASS_SETTINGS);
		createEAttribute(mapBasedEClassSettingsEClass, MAP_BASED_ECLASS_SETTINGS__USER_DATA_MAP);

		wizardPagesProviderEClass = createEClass(WIZARD_PAGES_PROVIDER);
		createEAttribute(wizardPagesProviderEClass, WIZARD_PAGES_PROVIDER__PAGES);
		createEReference(wizardPagesProviderEClass, WIZARD_PAGES_PROVIDER__EOBJECT);
		createEOperation(wizardPagesProviderEClass, WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS);
		createEOperation(wizardPagesProviderEClass, WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS);
		createEOperation(wizardPagesProviderEClass, WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS);
		createEOperation(wizardPagesProviderEClass, WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN);
		createEOperation(wizardPagesProviderEClass, WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE);

		wizardPagesProviderRegistryEClass = createEClass(WIZARD_PAGES_PROVIDER_REGISTRY);
		createEAttribute(wizardPagesProviderRegistryEClass, WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID);
		createEAttribute(wizardPagesProviderRegistryEClass, WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID);
		createEAttribute(wizardPagesProviderRegistryEClass, WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID);
		createEAttribute(wizardPagesProviderRegistryEClass, WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_MAP);
		createEOperation(wizardPagesProviderRegistryEClass, WIZARD_PAGES_PROVIDER_REGISTRY___GET_PROVIDER__ECLASS);

		namedDescribedWizardPagesProviderEClass = createEClass(NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER);

		namedSettingEClass = createEClass(NAMED_SETTING);
		createEReference(namedSettingEClass, NAMED_SETTING__PARENT);
		createEReference(namedSettingEClass, NAMED_SETTING__CONTAINING_FEATURE);

		displayUnitsRegistryEClass = createEClass(DISPLAY_UNITS_REGISTRY);
		createEReference(displayUnitsRegistryEClass, DISPLAY_UNITS_REGISTRY__ENTRIES_MAP);
		createEOperation(displayUnitsRegistryEClass, DISPLAY_UNITS_REGISTRY___SAVE);

		eTypedElementToUnitsMapEClass = createEClass(ETYPED_ELEMENT_TO_UNITS_MAP);
		createEReference(eTypedElementToUnitsMapEClass, ETYPED_ELEMENT_TO_UNITS_MAP__ENTRIES);

		eTypedElementToUnitsKeyValueEClass = createEClass(ETYPED_ELEMENT_TO_UNITS_KEY_VALUE);
		createEReference(eTypedElementToUnitsKeyValueEClass, ETYPED_ELEMENT_TO_UNITS_KEY_VALUE__KEY);
		createEReference(eTypedElementToUnitsKeyValueEClass, ETYPED_ELEMENT_TO_UNITS_KEY_VALUE__VALUE);

		unitsProviderEClass = createEClass(UNITS_PROVIDER);
		createEOperation(unitsProviderEClass, UNITS_PROVIDER___GET_PROVIDED_UNIT__UNITSPROVIDERPARAMETERS);

		unitsProviderParametersEClass = createEClass(UNITS_PROVIDER_PARAMETERS);

		simpleUnitsProviderEClass = createEClass(SIMPLE_UNITS_PROVIDER);
		createEAttribute(simpleUnitsProviderEClass, SIMPLE_UNITS_PROVIDER__UNIT);

		eOperationEParametersUnitsProviderEClass = createEClass(EOPERATION_EPARAMETERS_UNITS_PROVIDER);
		createEReference(eOperationEParametersUnitsProviderEClass, EOPERATION_EPARAMETERS_UNITS_PROVIDER__MAP);

		eOperationEParametersUnitsProviderParametersEClass = createEClass(EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS);
		createEReference(eOperationEParametersUnitsProviderParametersEClass, EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS__PARAM);

		decimalFormatRegistryEClass = createEClass(DECIMAL_FORMAT_REGISTRY);
		createEReference(decimalFormatRegistryEClass, DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP);
		createEOperation(decimalFormatRegistryEClass, DECIMAL_FORMAT_REGISTRY___SAVE);

		eTypedElementToFormatStringMapEClass = createEClass(ETYPED_ELEMENT_TO_FORMAT_STRING_MAP);
		createEReference(eTypedElementToFormatStringMapEClass, ETYPED_ELEMENT_TO_FORMAT_STRING_MAP__ENTRIES);

		eTypedElementToFormatStringKeyValueEClass = createEClass(ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE);
		createEReference(eTypedElementToFormatStringKeyValueEClass, ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE__KEY);
		createEReference(eTypedElementToFormatStringKeyValueEClass, ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE__VALUE);

		formatProviderEClass = createEClass(FORMAT_PROVIDER);
		createEOperation(formatProviderEClass, FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS);

		formatProviderParametersEClass = createEClass(FORMAT_PROVIDER_PARAMETERS);

		simpleFormatProviderEClass = createEClass(SIMPLE_FORMAT_PROVIDER);
		createEAttribute(simpleFormatProviderEClass, SIMPLE_FORMAT_PROVIDER__FORMAT_PATTERN);

		eOperationEParametersFormatProviderEClass = createEClass(EOPERATION_EPARAMETERS_FORMAT_PROVIDER);
		createEReference(eOperationEParametersFormatProviderEClass, EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP);

		eOperationEParametersFormatProviderParametersEClass = createEClass(EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS);
		createEReference(eOperationEParametersFormatProviderParametersEClass, EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS__PARAM);

		treeFeatureNodeWizardPageProviderEClass = createEClass(TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER);

		// Create data types
		hashMapEDataType = createEDataType(HASH_MAP);
		colorEDataType = createEDataType(COLOR);
		listEDataType = createEDataType(LIST);
		wizardPageEDataType = createEDataType(WIZARD_PAGE);
		mapEDataType = createEDataType(MAP);
		iWizardPageEDataType = createEDataType(IWIZARD_PAGE);
		compoundCommandEDataType = createEDataType(COMPOUND_COMMAND);
		editingDomainEDataType = createEDataType(EDITING_DOMAIN);
		resourceSetEDataType = createEDataType(RESOURCE_SET);
		mPartEDataType = createEDataType(MPART);
		eSelectionServiceEDataType = createEDataType(ESELECTION_SERVICE);
		imageEDataType = createEDataType(IMAGE);
		unitConverterMapEDataType = createEDataType(UNIT_CONVERTER_MAP);
		displayUnitsMapEDataType = createEDataType(DISPLAY_UNITS_MAP);
		decimalFormatEDataType = createEDataType(DECIMAL_FORMAT);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  private boolean isInitialized = false;

  /**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public void initializePackageContents()
  {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ApogyCommonEMFPackage theApogyCommonEMFPackage = (ApogyCommonEMFPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters
		addETypeParameter(hashMapEDataType, "key");
		addETypeParameter(hashMapEDataType, "value");
		addETypeParameter(mapEDataType, "key");
		addETypeParameter(mapEDataType, "value");

		// Set bounds for type parameters

		// Add supertypes to classes
		selectionBasedTimeSourceEClass.getESuperTypes().add(theApogyCommonEMFPackage.getTimeSource());
		mapBasedEClassSettingsEClass.getESuperTypes().add(this.getEClassSettings());
		namedDescribedWizardPagesProviderEClass.getESuperTypes().add(this.getWizardPagesProvider());
		namedSettingEClass.getESuperTypes().add(this.getEClassSettings());
		simpleUnitsProviderEClass.getESuperTypes().add(this.getUnitsProvider());
		eOperationEParametersUnitsProviderEClass.getESuperTypes().add(this.getUnitsProvider());
		eOperationEParametersUnitsProviderParametersEClass.getESuperTypes().add(this.getUnitsProviderParameters());
		simpleFormatProviderEClass.getESuperTypes().add(this.getFormatProvider());
		eOperationEParametersFormatProviderEClass.getESuperTypes().add(this.getFormatProvider());
		eOperationEParametersFormatProviderParametersEClass.getESuperTypes().add(this.getFormatProviderParameters());
		treeFeatureNodeWizardPageProviderEClass.getESuperTypes().add(this.getWizardPagesProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(apogyCommonEMFUIFacadeEClass, ApogyCommonEMFUIFacade.class, "ApogyCommonEMFUIFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getApogyCommonEMFUIFacade_UnitConverterMap(), this.getUnitConverterMap(), "unitConverterMap", null, 0, 1, ApogyCommonEMFUIFacade.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getApogyCommonEMFUIFacade__GetColorForRange__Ranges(), this.getColor(), "getColorForRange", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonEMFPackage.getRanges(), "range", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__GetDisplayUnits__ETypedElement(), theApogyCommonEMFPackage.getUnit(), "getDisplayUnits", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__GetDisplayUnits__EOperation_EOperationEParametersUnitsProviderParameters(), theApogyCommonEMFPackage.getUnit(), "getDisplayUnits", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEOperation(), "eOperation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEOperationEParametersUnitsProviderParameters(), "parameters", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__AddUnitsProviderToRegistry__ETypedElement_UnitsProvider(), null, "addUnitsProviderToRegistry", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getUnitsProvider(), "provider", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__ConvertToNativeUnits__Number_Unit_Unit_EClassifier(), theApogyCommonEMFPackage.getNumber(), "convertToNativeUnits", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonEMFPackage.getNumber(), "number", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonEMFPackage.getUnit(), "nativeUnits", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonEMFPackage.getUnit(), "displayUnits", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClassifier(), "numberType", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__GetDisplayFormat__ETypedElement(), this.getDecimalFormat(), "getDisplayFormat", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__GetDisplayFormat__EOperation_EOperationEParametersFormatProviderParameters(), this.getDecimalFormat(), "getDisplayFormat", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEOperation(), "eOperation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEOperationEParametersFormatProviderParameters(), "parameters", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__AddFormatProviderToRegistry__ETypedElement_FormatProvider(), null, "addFormatProviderToRegistry", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFormatProvider(), "provider", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__Named(), null, "openDeleteNamedDialog", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theApogyCommonEMFPackage.getNamed(), "named", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__List(), null, "openDeleteNamedDialog", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getList(), "namedList", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__SaveToPersistedState__MPart_String_EObject_ResourceSet(), null, "saveToPersistedState", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPart(), "mPart", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "persistedStateKey", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResourceSet(), "resourceSet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__ReadFromPersistedState__MPart_String_ResourceSet(), theEcorePackage.getEObject(), "readFromPersistedState", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPart(), "mPart", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "persistedStateKey", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResourceSet(), "resourceSet", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFUIFacade__GetImage__EClass(), this.getImage(), "getImage", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "eClass", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(selectionBasedTimeSourceEClass, SelectionBasedTimeSource.class, "SelectionBasedTimeSource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSelectionBasedTimeSource_Selection(), theApogyCommonEMFPackage.getTimed(), null, "selection", null, 0, 1, SelectionBasedTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSelectionBasedTimeSource_SelectionService(), this.getESelectionService(), "selectionService", null, 0, 1, SelectionBasedTimeSource.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eClassSettingsEClass, EClassSettings.class, "EClassSettings", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mapBasedEClassSettingsEClass, MapBasedEClassSettings.class, "MapBasedEClassSettings", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		EGenericType g1 = createEGenericType(this.getHashMap());
		EGenericType g2 = createEGenericType(theEcorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(theEcorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		initEAttribute(getMapBasedEClassSettings_UserDataMap(), g1, "userDataMap", null, 0, 1, MapBasedEClassSettings.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(wizardPagesProviderEClass, WizardPagesProvider.class, "WizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWizardPagesProvider_Pages(), this.getWizardPage(), "pages", null, 0, -1, WizardPagesProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWizardPagesProvider_EObject(), theEcorePackage.getEObject(), null, "eObject", null, 0, 1, WizardPagesProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getWizardPagesProvider__GetPages__EClass_EClassSettings(), this.getWizardPage(), "getPages", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "eClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEClassSettings(), "settings", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getWizardPagesProvider__CreateEObject__EClass_EClassSettings(), theEcorePackage.getEObject(), "createEObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "eClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEClassSettings(), "settings", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getWizardPagesProvider__InstantiateWizardPages__EObject_EClassSettings(), this.getWizardPage(), "instantiateWizardPages", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEClassSettings(), "settings", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getWizardPagesProvider__GetPerformFinishCommands__EObject_EClassSettings_EditingDomain(), this.getCompoundCommand(), "getPerformFinishCommands", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEClassSettings(), "settings", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEditingDomain(), "editingDomain", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getWizardPagesProvider__GetNextPage__IWizardPage(), this.getIWizardPage(), "getNextPage", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getIWizardPage(), "page", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(wizardPagesProviderRegistryEClass, WizardPagesProviderRegistry.class, "WizardPagesProviderRegistry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID(), theEcorePackage.getEString(), "WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID", "ca.gc.asc_csa.apogy.common.emf.ui.wizardPagesProvider", 0, 1, WizardPagesProviderRegistry.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID(), theEcorePackage.getEString(), "WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID", "eClass", 0, 1, WizardPagesProviderRegistry.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID(), theEcorePackage.getEString(), "WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID", "provider", 0, 1, WizardPagesProviderRegistry.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getMap());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(this.getWizardPagesProvider());
		g1.getETypeArguments().add(g2);
		initEAttribute(getWizardPagesProviderRegistry_WizardPagesMap(), g1, "wizardPagesMap", null, 0, 1, WizardPagesProviderRegistry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getWizardPagesProviderRegistry__GetProvider__EClass(), this.getWizardPagesProvider(), "getProvider", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "eClass", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(namedDescribedWizardPagesProviderEClass, NamedDescribedWizardPagesProvider.class, "NamedDescribedWizardPagesProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(namedSettingEClass, NamedSetting.class, "NamedSetting", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNamedSetting_Parent(), theEcorePackage.getEObject(), null, "parent", null, 0, 1, NamedSetting.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNamedSetting_ContainingFeature(), theEcorePackage.getETypedElement(), null, "containingFeature", null, 0, 1, NamedSetting.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(displayUnitsRegistryEClass, DisplayUnitsRegistry.class, "DisplayUnitsRegistry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDisplayUnitsRegistry_EntriesMap(), this.getETypedElementToUnitsMap(), null, "entriesMap", null, 0, 1, DisplayUnitsRegistry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getDisplayUnitsRegistry__Save(), null, "save", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(eTypedElementToUnitsMapEClass, ETypedElementToUnitsMap.class, "ETypedElementToUnitsMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getETypedElementToUnitsMap_Entries(), this.getETypedElementToUnitsKeyValue(), null, "entries", null, 0, -1, ETypedElementToUnitsMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eTypedElementToUnitsKeyValueEClass, Map.Entry.class, "ETypedElementToUnitsKeyValue", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getETypedElementToUnitsKeyValue_Key(), theEcorePackage.getETypedElement(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getETypedElementToUnitsKeyValue_Value(), this.getUnitsProvider(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unitsProviderEClass, UnitsProvider.class, "UnitsProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getUnitsProvider__GetProvidedUnit__UnitsProviderParameters(), theApogyCommonEMFPackage.getUnit(), "getProvidedUnit", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getUnitsProviderParameters(), "parameters", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(unitsProviderParametersEClass, UnitsProviderParameters.class, "UnitsProviderParameters", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(simpleUnitsProviderEClass, SimpleUnitsProvider.class, "SimpleUnitsProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimpleUnitsProvider_Unit(), theApogyCommonEMFPackage.getUnit(), "unit", null, 0, 1, SimpleUnitsProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eOperationEParametersUnitsProviderEClass, EOperationEParametersUnitsProvider.class, "EOperationEParametersUnitsProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEOperationEParametersUnitsProvider_Map(), this.getETypedElementToUnitsMap(), null, "map", null, 0, 1, EOperationEParametersUnitsProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eOperationEParametersUnitsProviderParametersEClass, EOperationEParametersUnitsProviderParameters.class, "EOperationEParametersUnitsProviderParameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEOperationEParametersUnitsProviderParameters_Param(), theEcorePackage.getEParameter(), null, "param", null, 0, 1, EOperationEParametersUnitsProviderParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(decimalFormatRegistryEClass, DecimalFormatRegistry.class, "DecimalFormatRegistry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDecimalFormatRegistry_EntriesMap(), this.getETypedElementToFormatStringMap(), null, "entriesMap", null, 0, 1, DecimalFormatRegistry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getDecimalFormatRegistry__Save(), null, "save", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(eTypedElementToFormatStringMapEClass, ETypedElementToFormatStringMap.class, "ETypedElementToFormatStringMap", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getETypedElementToFormatStringMap_Entries(), this.getETypedElementToFormatStringKeyValue(), null, "entries", null, 0, -1, ETypedElementToFormatStringMap.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eTypedElementToFormatStringKeyValueEClass, Map.Entry.class, "ETypedElementToFormatStringKeyValue", !IS_ABSTRACT, !IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);
		initEReference(getETypedElementToFormatStringKeyValue_Key(), theEcorePackage.getETypedElement(), null, "key", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getETypedElementToFormatStringKeyValue_Value(), this.getFormatProvider(), null, "value", null, 0, 1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(formatProviderEClass, FormatProvider.class, "FormatProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getFormatProvider__GetProvidedFormat__FormatProviderParameters(), this.getDecimalFormat(), "getProvidedFormat", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getFormatProviderParameters(), "parameters", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(formatProviderParametersEClass, FormatProviderParameters.class, "FormatProviderParameters", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(simpleFormatProviderEClass, SimpleFormatProvider.class, "SimpleFormatProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimpleFormatProvider_FormatPattern(), theEcorePackage.getEString(), "formatPattern", null, 0, 1, SimpleFormatProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eOperationEParametersFormatProviderEClass, EOperationEParametersFormatProvider.class, "EOperationEParametersFormatProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEOperationEParametersFormatProvider_Map(), this.getETypedElementToFormatStringMap(), null, "map", null, 0, 1, EOperationEParametersFormatProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eOperationEParametersFormatProviderParametersEClass, EOperationEParametersFormatProviderParameters.class, "EOperationEParametersFormatProviderParameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEOperationEParametersFormatProviderParameters_Param(), theEcorePackage.getEParameter(), null, "param", null, 0, 1, EOperationEParametersFormatProviderParameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(treeFeatureNodeWizardPageProviderEClass, TreeFeatureNodeWizardPageProvider.class, "TreeFeatureNodeWizardPageProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize data types
		initEDataType(hashMapEDataType, HashMap.class, "HashMap", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(colorEDataType, Color.class, "Color", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listEDataType, List.class, "List", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<? extends ca.gc.asc_csa.apogy.common.emf.Named>");
		initEDataType(wizardPageEDataType, WizardPage.class, "WizardPage", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(mapEDataType, Map.class, "Map", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iWizardPageEDataType, IWizardPage.class, "IWizardPage", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(compoundCommandEDataType, CompoundCommand.class, "CompoundCommand", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(editingDomainEDataType, EditingDomain.class, "EditingDomain", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(resourceSetEDataType, ResourceSet.class, "ResourceSet", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(mPartEDataType, MPart.class, "MPart", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eSelectionServiceEDataType, ESelectionService.class, "ESelectionService", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(imageEDataType, Image.class, "Image", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(unitConverterMapEDataType, TreeMap.class, "UnitConverterMap", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.TreeMap<javax.measure.converter.UnitConverter, java.lang.String>");
		initEDataType(displayUnitsMapEDataType, HashMap.class, "DisplayUnitsMap", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.HashMap<org.eclipse.emf.ecore.ETypedElement, ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider>");
		initEDataType(decimalFormatEDataType, DecimalFormat.class, "DecimalFormat", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

		/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "documentation", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca),\n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "prefix", "ApogyCommonEMFUI",
			 "copyrightText", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "childCreationExtenders", "true",
			 "suppressGenModelAnnotations", "false",
			 "modelName", "ApogyCommonEMFUI",
			 "publicConstructors", "true",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.common.emf.ui/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.common.emf.ui.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.common.emf"
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__GetColorForRange__Ranges(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the color currently associated with the specified\nRanges. Can be null."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__GetDisplayUnits__ETypedElement(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the {@link Unit} to be used for display for a specified {@link ETypedElement}.\n@param eTypedElement reference to the ETypedElement.\n@return the display {@link Unit}, or the native {@link Unit} if none found."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__GetDisplayUnits__EOperation_EOperationEParametersUnitsProviderParameters(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the {@link Unit} to be used for display for a specified {@link ETypedElement}.\n@param eOperation reference to the {@link EOperation}.\n@param parameter Parameters to get the units.\n@return the display Unit, or the native {@link Unit} if none found."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__AddUnitsProviderToRegistry__ETypedElement_UnitsProvider(), 
		   source, 
		   new String[] {
			 "documentation", "Adds a unit to the registry and saves the preferences.\n@param eTypedElement reference to the {@link ETypedElement} to provide a unit.\n@param provider reference to the {@link UnitsProvider}."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__ConvertToNativeUnits__Number_Unit_Unit_EClassifier(), 
		   source, 
		   new String[] {
			 "documentation", "Converts the specified number from the display units to the native units.\nThis method will also return the right type of number.\n@param number reference to the number to convert.\n@param nativeUnits {@link Unit} to convert to.\n@param displatUnits {@link Unit} to convert from.\n@param numberType the {@link EClassifier} of the number to return.\n@return the number value converted to the native units."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__GetDisplayFormat__ETypedElement(), 
		   source, 
		   new String[] {
			 "documentation", "***********************************************\nFormats\n**********************************************"
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__GetDisplayFormat__EOperation_EOperationEParametersFormatProviderParameters(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the {@link DecimalFormat} to be used for display for a specified {@link EOperation}.\n@param eOperation reference to the {@link EOperation}.\n@param parameter Parameters to get the format.\n@return the display {@link DecimalFormat}, or the native format if none found."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__AddFormatProviderToRegistry__ETypedElement_FormatProvider(), 
		   source, 
		   new String[] {
			 "documentation", "Adds a {@link DecimalFormat} to the registry and saves the preferences.\n@param eTypedElement reference to the {@link ETypedElement} to provide a {@link DecimalFormat}.\n@param provider reference to the {@link FormatProvider}."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__Named(), 
		   source, 
		   new String[] {
			 "documentation", "***********************************************\nNamed Dialog\n**********************************************"
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__List(), 
		   source, 
		   new String[] {
			 "documentation", "Opens a dialog to delete a {@link List} of {@link Named} objects.\n@param namedList Reference to the {@link List}"
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__SaveToPersistedState__MPart_String_EObject_ResourceSet(), 
		   source, 
		   new String[] {
			 "documentation", "***********************************************\nPersisted states\n**********************************************"
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__ReadFromPersistedState__MPart_String_ResourceSet(), 
		   source, 
		   new String[] {
			 "documentation", "Reads an EObject from a part\'s Persisted State.\n@param mPart The part.\n@param persistedStateKey The persisted state key under which the eObject is stored.\n@param resourceSet The ResourceSet containing all references used by the EObject. Cannot be null.\n@return The EObject read, null if none could be read."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade__GetImage__EClass(), 
		   source, 
		   new String[] {
			 "documentation", "Tries to retrieve the image that would represent an instance of an {@link EClass}.\n@param eClass reference to the {@link EClass}.\n@return the image if found."
		   });	
		addAnnotation
		  (getApogyCommonEMFUIFacade_UnitConverterMap(), 
		   source, 
		   new String[] {
			 "documentation", "***********************************************\nUnits\n**********************************************"
		   });	
		addAnnotation
		  (selectionBasedTimeSourceEClass, 
		   source, 
		   new String[] {
			 "documentation", "Time source that provides time based on a selection."
		   });	
		addAnnotation
		  (eClassSettingsEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAbstract class used to pass user settings to WizardPagesProvider."
		   });	
		addAnnotation
		  (mapBasedEClassSettingsEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecialization of EClassSettings that provide a key-value storage of user settings."
		   });	
		addAnnotation
		  (getMapBasedEClassSettings_UserDataMap(), 
		   source, 
		   new String[] {
			 "documentation", "*\nMap that stores user data."
		   });	
		addAnnotation
		  (wizardPagesProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA class providing Wizard support for the creation of EObjects.\nIt is used to provide customized EObject creation, Wizard page and post creation Commands."
		   });	
		addAnnotation
		  (getWizardPagesProvider__GetPages__EClass_EClassSettings(), 
		   source, 
		   new String[] {
			 "documentation", "Gets the pages needed to create or modify an {@link EObject}.\nIt is not recommended to override this method.\n@param eClass the {@link EClass} of the {@link EObject} to create.\n@param settings the {@link EClassSettings} needed to create the {@link EObject} or to instantiate the {@link IWizardPage}s.\n@return An array of the {@link WizardPage}s."
		   });	
		addAnnotation
		  (getWizardPagesProvider__CreateEObject__EClass_EClassSettings(), 
		   source, 
		   new String[] {
			 "documentation", "Creates the {@link EObject} that is modified in the {@link WizardPage}s.\n@param eClass The {@link EClass} of the {@link EObject} to create.\n@param settings If needed, the {@link EClassSettings} needed to create the {@link EObject}.\n@return Reference to the {@link EObject} created."
		   });	
		addAnnotation
		  (getWizardPagesProvider__InstantiateWizardPages__EObject_EClassSettings(), 
		   source, 
		   new String[] {
			 "documentation", "Instantiate the {@link IWizardPage}.\n@param eObject reference to the {@link EObject} modified in the pages.\n@param settings If needed, the {@link EClassSettings} needed to instantiate the {@link WizardPage}.\n@return List of pages"
		   });	
		addAnnotation
		  (getWizardPagesProvider__GetPerformFinishCommands__EObject_EClassSettings_EditingDomain(), 
		   source, 
		   new String[] {
			 "documentation", "Gets a {@link CompoundCommand} that need to be executed when the user presses performFinish() on the wizard.\n@param eObject reference to the {@link EObject} modified in the pages.\n@param settings settings used in the creation of the eObject and in the wizard pages.\n@param editingDomain the {@link EditingDomain} of the container of the eObject. Used to create the commands.\n@return {@link CompoundCommand}"
		   });	
		addAnnotation
		  (getWizardPagesProvider__GetNextPage__IWizardPage(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the successor of the given page.\n@param page The {@link IWizardPage}.\n@return the next {@link IWizardPage}, or null if none."
		   });	
		addAnnotation
		  (wizardPagesProviderRegistryEClass, 
		   source, 
		   new String[] {
			 "documentation", "Registry to get the factory of a Program EClass"
		   });	
		addAnnotation
		  (getWizardPagesProviderRegistry__GetProvider__EClass(), 
		   source, 
		   new String[] {
			 "documentation", "Gets the {@link WizardPagesProvider}s corresponding to the EClass and it\'s superclasses."
		   });	
		addAnnotation
		  (displayUnitsRegistryEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nUnits preferences registry\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getDisplayUnitsRegistry__Save(), 
		   source, 
		   new String[] {
			 "documentation", " Saves the registry in the preference."
		   });	
		addAnnotation
		  (eTypedElementToUnitsMapEClass, 
		   source, 
		   new String[] {
			 "documentation", "Units Map"
		   });	
		addAnnotation
		  (eTypedElementToUnitsKeyValueEClass, 
		   source, 
		   new String[] {
			 "documentation", " Units map entries"
		   });	
		addAnnotation
		  (unitsProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "Units provider. This interface is used to define how to get a unit using parameters."
		   });	
		addAnnotation
		  (unitsProviderParametersEClass, 
		   source, 
		   new String[] {
			 "documentation", " Parameters for an unitProvider."
		   });	
		addAnnotation
		  (simpleUnitsProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", " Simple units provider. This type of provider will return the referenced unit."
		   });	
		addAnnotation
		  (eOperationEParametersUnitsProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", " EParameters units provider.\nThis type of provider will return the referenced unit of a parameter using the parameter."
		   });	
		addAnnotation
		  (decimalFormatRegistryEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nDecimal formats preferences registry\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getDecimalFormatRegistry__Save(), 
		   source, 
		   new String[] {
			 "documentation", " Saves the registry in the preference."
		   });	
		addAnnotation
		  (eTypedElementToFormatStringMapEClass, 
		   source, 
		   new String[] {
			 "documentation", "Format Map"
		   });	
		addAnnotation
		  (eTypedElementToFormatStringKeyValueEClass, 
		   source, 
		   new String[] {
			 "documentation", " Format map entries"
		   });	
		addAnnotation
		  (formatProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "Format provider. This interface is used to define how to get a format using parameters."
		   });	
		addAnnotation
		  (formatProviderParametersEClass, 
		   source, 
		   new String[] {
			 "documentation", " Parameters for a formatProvider."
		   });	
		addAnnotation
		  (simpleFormatProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", " Simple format provider. This type of provider will return the referenced format."
		   });	
		addAnnotation
		  (eOperationEParametersFormatProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", " EParameters format provider.\nThis type of provider will return the referenced format of a parameter using the parameter."
		   });	
		addAnnotation
		  (treeFeatureNodeWizardPageProviderEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nPage provider used to create TreeFeatureNode."
		   });
	}

} //ApogyCommonEMFUIPackageImpl
