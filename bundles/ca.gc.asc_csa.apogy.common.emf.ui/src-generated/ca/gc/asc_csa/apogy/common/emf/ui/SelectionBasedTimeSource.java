/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui;

import org.eclipse.e4.ui.workbench.modeling.ESelectionService;

import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.Timed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Selection Based Time Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Time source that provides time based on a selection.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource#getSelection <em>Selection</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource#getSelectionService <em>Selection Service</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getSelectionBasedTimeSource()
 * @model
 * @generated
 */
public interface SelectionBasedTimeSource extends TimeSource {
	/**
	 * Returns the value of the '<em><b>Selection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection</em>' reference.
	 * @see #setSelection(Timed)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getSelectionBasedTimeSource_Selection()
	 * @model
	 * @generated
	 */
	Timed getSelection();

	/**
	 * Returns the value of the '<em><b>Selection Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection Service</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection Service</em>' attribute.
	 * @see #setSelectionService(ESelectionService)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getSelectionBasedTimeSource_SelectionService()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.common.emf.ui.ESelectionService" transient="true"
	 * @generated
	 */
	ESelectionService getSelectionService();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource#getSelectionService <em>Selection Service</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection Service</em>' attribute.
	 * @see #getSelectionService()
	 * @generated
	 */
	void setSelectionService(ESelectionService value);

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource#getSelection <em>Selection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection</em>' reference.
	 * @see #getSelection()
	 * @generated
	 */
	void setSelection(Timed value);

} // SelectionBasedTimeSource
