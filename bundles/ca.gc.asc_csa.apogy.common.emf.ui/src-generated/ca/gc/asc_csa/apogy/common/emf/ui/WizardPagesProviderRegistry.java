/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wizard Pages Provider Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Registry to get the factory of a Program EClass
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWizardPagesMap <em>Wizard Pages Map</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getWizardPagesProviderRegistry()
 * @model
 * @generated
 */
public interface WizardPagesProviderRegistry extends EObject {
	
	/**
	 * @generated_NOT
	 */
	public WizardPagesProviderRegistry INSTANCE = WizardPagesProviderRegistryImpl.getInstance();
	
	/**
	 * Returns the value of the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</b></em>' attribute.
	 * The default value is <code>"ca.gc.asc_csa.apogy.common.emf.ui.wizardPagesProvider"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID()
	 * @model default="ca.gc.asc_csa.apogy.common.emf.ui.wizardPagesProvider" unique="false" transient="true" changeable="false"
	 * @generated
	 */
	String getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID();

	/**
	 * Returns the value of the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</b></em>' attribute.
	 * The default value is <code>"eClass"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID()
	 * @model default="eClass" unique="false" transient="true" changeable="false"
	 * @generated
	 */
	String getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID();

	/**
	 * Returns the value of the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</b></em>' attribute.
	 * The default value is <code>"provider"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</em>' attribute.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID()
	 * @model default="provider" unique="false" transient="true" changeable="false"
	 * @generated
	 */
	String getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID();

	/**
	 * Returns the value of the '<em><b>Wizard Pages Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wizard Pages Map</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wizard Pages Map</em>' attribute.
	 * @see #setWizardPagesMap(Map)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getWizardPagesProviderRegistry_WizardPagesMap()
	 * @model unique="false" dataType="ca.gc.asc_csa.apogy.common.emf.ui.Map<org.eclipse.emf.ecore.EClass, ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider>"
	 * @generated
	 */
	Map<EClass, WizardPagesProvider> getWizardPagesMap();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWizardPagesMap <em>Wizard Pages Map</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wizard Pages Map</em>' attribute.
	 * @see #getWizardPagesMap()
	 * @generated
	 */
	void setWizardPagesMap(Map<EClass, WizardPagesProvider> value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Gets the {@link WizardPagesProvider}s corresponding to the EClass and it's superclasses.
	 * <!-- end-model-doc -->
	 * @model unique="false" eClassUnique="false"
	 * @generated
	 */
	WizardPagesProvider getProvider(EClass eClass);

} // WizardPagesProviderRegistry
