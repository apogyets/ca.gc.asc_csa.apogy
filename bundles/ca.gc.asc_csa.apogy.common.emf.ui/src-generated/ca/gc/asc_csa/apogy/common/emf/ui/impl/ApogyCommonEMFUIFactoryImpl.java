package ca.gc.asc_csa.apogy.common.emf.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.measure.converter.UnitConverter;
import javax.measure.unit.Unit;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.ui.*;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCommonEMFUIFactoryImpl extends EFactoryImpl implements ApogyCommonEMFUIFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public static ApogyCommonEMFUIFactory init()
  {
		try {
			ApogyCommonEMFUIFactory theApogyCommonEMFUIFactory = (ApogyCommonEMFUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyCommonEMFUIPackage.eNS_URI);
			if (theApogyCommonEMFUIFactory != null) {
				return theApogyCommonEMFUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyCommonEMFUIFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ApogyCommonEMFUIFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE: return createApogyCommonEMFUIFacade();
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE: return createSelectionBasedTimeSource();
			case ApogyCommonEMFUIPackage.MAP_BASED_ECLASS_SETTINGS: return createMapBasedEClassSettings();
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER: return createWizardPagesProvider();
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY: return createWizardPagesProviderRegistry();
			case ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER: return createNamedDescribedWizardPagesProvider();
			case ApogyCommonEMFUIPackage.NAMED_SETTING: return createNamedSetting();
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY: return createDisplayUnitsRegistry();
			case ApogyCommonEMFUIPackage.ETYPED_ELEMENT_TO_UNITS_MAP: return createETypedElementToUnitsMap();
			case ApogyCommonEMFUIPackage.ETYPED_ELEMENT_TO_UNITS_KEY_VALUE: return (EObject)createETypedElementToUnitsKeyValue();
			case ApogyCommonEMFUIPackage.SIMPLE_UNITS_PROVIDER: return createSimpleUnitsProvider();
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_UNITS_PROVIDER: return createEOperationEParametersUnitsProvider();
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS: return createEOperationEParametersUnitsProviderParameters();
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY: return createDecimalFormatRegistry();
			case ApogyCommonEMFUIPackage.ETYPED_ELEMENT_TO_FORMAT_STRING_MAP: return createETypedElementToFormatStringMap();
			case ApogyCommonEMFUIPackage.ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE: return (EObject)createETypedElementToFormatStringKeyValue();
			case ApogyCommonEMFUIPackage.SIMPLE_FORMAT_PROVIDER: return createSimpleFormatProvider();
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER: return createEOperationEParametersFormatProvider();
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS: return createEOperationEParametersFormatProviderParameters();
			case ApogyCommonEMFUIPackage.TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER: return createTreeFeatureNodeWizardPageProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
		switch (eDataType.getClassifierID()) {
			case ApogyCommonEMFUIPackage.HASH_MAP:
				return createHashMapFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.COLOR:
				return createColorFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.LIST:
				return createListFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.WIZARD_PAGE:
				return createWizardPageFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.MAP:
				return createMapFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.IWIZARD_PAGE:
				return createIWizardPageFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.COMPOUND_COMMAND:
				return createCompoundCommandFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.EDITING_DOMAIN:
				return createEditingDomainFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.RESOURCE_SET:
				return createResourceSetFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.MPART:
				return createMPartFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.ESELECTION_SERVICE:
				return createESelectionServiceFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.IMAGE:
				return createImageFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.UNIT_CONVERTER_MAP:
				return createUnitConverterMapFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_MAP:
				return createDisplayUnitsMapFromString(eDataType, initialValue);
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT:
				return createDecimalFormatFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
		switch (eDataType.getClassifierID()) {
			case ApogyCommonEMFUIPackage.HASH_MAP:
				return convertHashMapToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.COLOR:
				return convertColorToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.LIST:
				return convertListToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.WIZARD_PAGE:
				return convertWizardPageToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.MAP:
				return convertMapToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.IWIZARD_PAGE:
				return convertIWizardPageToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.COMPOUND_COMMAND:
				return convertCompoundCommandToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.EDITING_DOMAIN:
				return convertEditingDomainToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.RESOURCE_SET:
				return convertResourceSetToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.MPART:
				return convertMPartToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.ESELECTION_SERVICE:
				return convertESelectionServiceToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.IMAGE:
				return convertImageToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.UNIT_CONVERTER_MAP:
				return convertUnitConverterMapToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_MAP:
				return convertDisplayUnitsMapToString(eDataType, instanceValue);
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT:
				return convertDecimalFormatToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonEMFUIFacade createApogyCommonEMFUIFacade() {
		ApogyCommonEMFUIFacadeImpl apogyCommonEMFUIFacade = new ApogyCommonEMFUIFacadeImpl();
		return apogyCommonEMFUIFacade;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public SelectionBasedTimeSource createSelectionBasedTimeSource() {
		SelectionBasedTimeSourceImpl selectionBasedTimeSource = new SelectionBasedTimeSourceImpl();
		return selectionBasedTimeSource;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public MapBasedEClassSettings createMapBasedEClassSettings() {
		MapBasedEClassSettingsImpl mapBasedEClassSettings = new MapBasedEClassSettingsImpl();
		return mapBasedEClassSettings;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public WizardPagesProvider createWizardPagesProvider() {
		WizardPagesProviderImpl wizardPagesProvider = new WizardPagesProviderImpl();
		return wizardPagesProvider;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public WizardPagesProviderRegistry createWizardPagesProviderRegistry() {
		WizardPagesProviderRegistryImpl wizardPagesProviderRegistry = new WizardPagesProviderRegistryImpl();
		return wizardPagesProviderRegistry;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NamedDescribedWizardPagesProvider createNamedDescribedWizardPagesProvider() {
		NamedDescribedWizardPagesProviderImpl namedDescribedWizardPagesProvider = new NamedDescribedWizardPagesProviderImpl();
		return namedDescribedWizardPagesProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public NamedSetting createNamedSetting() {
		NamedSettingImpl namedSetting = new NamedSettingImpl();
		return namedSetting;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public DisplayUnitsRegistry createDisplayUnitsRegistry() {
		DisplayUnitsRegistryImpl displayUnitsRegistry = new DisplayUnitsRegistryImpl();
		return displayUnitsRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Map.Entry<ETypedElement, UnitsProvider> createETypedElementToUnitsKeyValue() {
		ETypedElementToUnitsKeyValueImpl eTypedElementToUnitsKeyValue = new ETypedElementToUnitsKeyValueImpl();
		return eTypedElementToUnitsKeyValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ETypedElementToUnitsMap createETypedElementToUnitsMap() {
		ETypedElementToUnitsMapImpl eTypedElementToUnitsMap = new ETypedElementToUnitsMapImpl();
		return eTypedElementToUnitsMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public SimpleUnitsProvider createSimpleUnitsProvider() {
		SimpleUnitsProviderImpl simpleUnitsProvider = new SimpleUnitsProviderImpl();
		return simpleUnitsProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperationEParametersUnitsProvider createEOperationEParametersUnitsProvider() {
		EOperationEParametersUnitsProviderImpl eOperationEParametersUnitsProvider = new EOperationEParametersUnitsProviderImpl();
		return eOperationEParametersUnitsProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperationEParametersUnitsProviderParameters createEOperationEParametersUnitsProviderParameters() {
		EOperationEParametersUnitsProviderParametersImpl eOperationEParametersUnitsProviderParameters = new EOperationEParametersUnitsProviderParametersImpl();
		return eOperationEParametersUnitsProviderParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public DecimalFormatRegistry createDecimalFormatRegistry() {
		DecimalFormatRegistryImpl decimalFormatRegistry = new DecimalFormatRegistryImpl();
		return decimalFormatRegistry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ETypedElementToFormatStringMap createETypedElementToFormatStringMap() {
		ETypedElementToFormatStringMapImpl eTypedElementToFormatStringMap = new ETypedElementToFormatStringMapImpl();
		return eTypedElementToFormatStringMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Map.Entry<ETypedElement, FormatProvider> createETypedElementToFormatStringKeyValue() {
		ETypedElementToFormatStringKeyValueImpl eTypedElementToFormatStringKeyValue = new ETypedElementToFormatStringKeyValueImpl();
		return eTypedElementToFormatStringKeyValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public SimpleFormatProvider createSimpleFormatProvider() {
		SimpleFormatProviderImpl simpleFormatProvider = new SimpleFormatProviderImpl();
		return simpleFormatProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperationEParametersFormatProvider createEOperationEParametersFormatProvider() {
		EOperationEParametersFormatProviderImpl eOperationEParametersFormatProvider = new EOperationEParametersFormatProviderImpl();
		return eOperationEParametersFormatProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperationEParametersFormatProviderParameters createEOperationEParametersFormatProviderParameters() {
		EOperationEParametersFormatProviderParametersImpl eOperationEParametersFormatProviderParameters = new EOperationEParametersFormatProviderParametersImpl();
		return eOperationEParametersFormatProviderParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public TreeFeatureNodeWizardPageProvider createTreeFeatureNodeWizardPageProvider() {
		TreeFeatureNodeWizardPageProviderImpl treeFeatureNodeWizardPageProvider = new TreeFeatureNodeWizardPageProviderImpl();
		return treeFeatureNodeWizardPageProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public HashMap<?, ?> createHashMapFromString(EDataType eDataType, String initialValue) {
		return (HashMap<?, ?>)super.createFromString(initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertHashMapToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public Color createColorFromString(EDataType eDataType, String initialValue)
  {
		return (Color)super.createFromString(eDataType, initialValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public String convertColorToString(EDataType eDataType, Object instanceValue)
  {
		return super.convertToString(eDataType, instanceValue);
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
	 * @generated_NOT
	 */
		public Unit<?> createUnitFromString(EDataType eDataType, String initialValue)
  {
		return (Unit<?>)super.createFromString(initialValue);
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<? extends Named> createListFromString(EDataType eDataType, String initialValue) {
		return (List<? extends Named>)super.createFromString(initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertListToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public WizardPage createWizardPageFromString(EDataType eDataType, String initialValue) {
		return (WizardPage)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertWizardPageToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Map<?, ?> createMapFromString(EDataType eDataType, String initialValue) {
		return (Map<?, ?>)super.createFromString(initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertMapToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public IWizardPage createIWizardPageFromString(EDataType eDataType, String initialValue) {
		return (IWizardPage)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertIWizardPageToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public CompoundCommand createCompoundCommandFromString(EDataType eDataType, String initialValue) {
		return (CompoundCommand)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertCompoundCommandToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EditingDomain createEditingDomainFromString(EDataType eDataType, String initialValue) {
		return (EditingDomain)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertEditingDomainToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ResourceSet createResourceSetFromString(EDataType eDataType, String initialValue) {
		return (ResourceSet)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertResourceSetToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public MPart createMPartFromString(EDataType eDataType, String initialValue) {
		return (MPart)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertMPartToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ESelectionService createESelectionServiceFromString(EDataType eDataType, String initialValue) {
		return (ESelectionService)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertESelectionServiceToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public Image createImageFromString(EDataType eDataType, String initialValue) {
		return (Image)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertImageToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public TreeMap<UnitConverter, String> createUnitConverterMapFromString(EDataType eDataType, String initialValue) {
		return (TreeMap<UnitConverter, String>)super.createFromString(initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertUnitConverterMapToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public HashMap<ETypedElement, UnitsProvider> createDisplayUnitsMapFromString(EDataType eDataType, String initialValue) {
		return (HashMap<ETypedElement, UnitsProvider>)super.createFromString(initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertDisplayUnitsMapToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public DecimalFormat createDecimalFormatFromString(EDataType eDataType, String initialValue) {
		return (DecimalFormat)super.createFromString(eDataType, initialValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public String convertDecimalFormatToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonEMFUIPackage getApogyCommonEMFUIPackage() {
		return (ApogyCommonEMFUIPackage)getEPackage();
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static ApogyCommonEMFUIPackage getPackage()
  {
		return ApogyCommonEMFUIPackage.eINSTANCE;
	}

} //ApogyCommonEMFUIFactoryImpl
