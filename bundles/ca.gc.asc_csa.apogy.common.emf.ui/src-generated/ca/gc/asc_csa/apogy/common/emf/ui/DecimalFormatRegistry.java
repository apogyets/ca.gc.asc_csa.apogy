/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui;

import org.eclipse.emf.ecore.EObject;

import ca.gc.asc_csa.apogy.common.emf.ui.impl.DecimalFormatRegistryImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decimal Format Registry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 *  -------------------------------------------------------------------------
 * Decimal formats preferences registry
 * -------------------------------------------------------------------------
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry#getEntriesMap <em>Entries Map</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getDecimalFormatRegistry()
 * @model
 * @generated
 */
public interface DecimalFormatRegistry extends EObject {
	
	/**
	 * @generated_NOT
	 */
	public DecimalFormatRegistry INSTANCE = DecimalFormatRegistryImpl.getInstance();
	
	/**
	 * Returns the value of the '<em><b>Entries Map</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entries Map</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entries Map</em>' reference.
	 * @see #setEntriesMap(ETypedElementToFormatStringMap)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getDecimalFormatRegistry_EntriesMap()
	 * @model
	 * @generated
	 */
	ETypedElementToFormatStringMap getEntriesMap();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry#getEntriesMap <em>Entries Map</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entries Map</em>' reference.
	 * @see #getEntriesMap()
	 * @generated
	 */
	void setEntriesMap(ETypedElementToFormatStringMap value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 *  Saves the registry in the preference.
	 * <!-- end-model-doc -->
	 * @model
	 * @generated
	 */
	void save();

} // DecimalFormatRegistry
