/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureTreeNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.TreeFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.TreeFeatureNodeWizardPageProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.TreeFeatureNodeWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tree Feature Node Wizard Page Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TreeFeatureNodeWizardPageProviderImpl extends WizardPagesProviderImpl implements TreeFeatureNodeWizardPageProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TreeFeatureNodeWizardPageProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		TreeFeatureNode treeFeatureNode = ApogyCommonEMFFactory.eINSTANCE.createTreeFeatureNode();
		
		return treeFeatureNode;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		AbstractFeatureTreeNode parent = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings = (MapBasedEClassSettings) settings;
			
			parent = (AbstractFeatureTreeNode) mapBasedEClassSettings.getUserDataMap().get("parent");			
		}
		
		
		TreeFeatureNode treeFeatureNode = (TreeFeatureNode) eObject;
		list.add(new TreeFeatureNodeWizardPage(treeFeatureNode, parent));
				
		return list;
	}
	
} //TreeFeatureNodeWizardPageProviderImpl
