package ca.gc.asc_csa.apogy.common.emf.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage
 * @generated
 */
public interface ApogyCommonEMFUIFactory extends EFactory
{
  /**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  ApogyCommonEMFUIFactory eINSTANCE = ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIFactoryImpl.init();

  /**
	 * Returns a new object of class '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Facade</em>'.
	 * @generated
	 */
	ApogyCommonEMFUIFacade createApogyCommonEMFUIFacade();

		/**
	 * Returns a new object of class '<em>Selection Based Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Selection Based Time Source</em>'.
	 * @generated
	 */
	SelectionBasedTimeSource createSelectionBasedTimeSource();

		/**
	 * Returns a new object of class '<em>Map Based EClass Settings</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Map Based EClass Settings</em>'.
	 * @generated
	 */
	MapBasedEClassSettings createMapBasedEClassSettings();

		/**
	 * Returns a new object of class '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Wizard Pages Provider</em>'.
	 * @generated
	 */
	WizardPagesProvider createWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Wizard Pages Provider Registry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Wizard Pages Provider Registry</em>'.
	 * @generated
	 */
	WizardPagesProviderRegistry createWizardPagesProviderRegistry();

		/**
	 * Returns a new object of class '<em>Named Described Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Named Described Wizard Pages Provider</em>'.
	 * @generated
	 */
	NamedDescribedWizardPagesProvider createNamedDescribedWizardPagesProvider();

		/**
	 * Returns a new object of class '<em>Named Setting</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Named Setting</em>'.
	 * @generated
	 */
	NamedSetting createNamedSetting();

		/**
	 * Returns a new object of class '<em>Display Units Registry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Display Units Registry</em>'.
	 * @generated
	 */
	DisplayUnitsRegistry createDisplayUnitsRegistry();

		/**
	 * Returns a new object of class '<em>ETyped Element To Units Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>ETyped Element To Units Map</em>'.
	 * @generated
	 */
	ETypedElementToUnitsMap createETypedElementToUnitsMap();

		/**
	 * Returns a new object of class '<em>Simple Units Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Simple Units Provider</em>'.
	 * @generated
	 */
	SimpleUnitsProvider createSimpleUnitsProvider();

		/**
	 * Returns a new object of class '<em>EOperation EParameters Units Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>EOperation EParameters Units Provider</em>'.
	 * @generated
	 */
	EOperationEParametersUnitsProvider createEOperationEParametersUnitsProvider();

		/**
	 * Returns a new object of class '<em>EOperation EParameters Units Provider Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>EOperation EParameters Units Provider Parameters</em>'.
	 * @generated
	 */
	EOperationEParametersUnitsProviderParameters createEOperationEParametersUnitsProviderParameters();

		/**
	 * Returns a new object of class '<em>Decimal Format Registry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Decimal Format Registry</em>'.
	 * @generated
	 */
	DecimalFormatRegistry createDecimalFormatRegistry();

		/**
	 * Returns a new object of class '<em>ETyped Element To Format String Map</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>ETyped Element To Format String Map</em>'.
	 * @generated
	 */
	ETypedElementToFormatStringMap createETypedElementToFormatStringMap();

		/**
	 * Returns a new object of class '<em>Simple Format Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Simple Format Provider</em>'.
	 * @generated
	 */
	SimpleFormatProvider createSimpleFormatProvider();

		/**
	 * Returns a new object of class '<em>EOperation EParameters Format Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>EOperation EParameters Format Provider</em>'.
	 * @generated
	 */
	EOperationEParametersFormatProvider createEOperationEParametersFormatProvider();

		/**
	 * Returns a new object of class '<em>EOperation EParameters Format Provider Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>EOperation EParameters Format Provider Parameters</em>'.
	 * @generated
	 */
	EOperationEParametersFormatProviderParameters createEOperationEParametersFormatProviderParameters();

		/**
	 * Returns a new object of class '<em>Tree Feature Node Wizard Page Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Tree Feature Node Wizard Page Provider</em>'.
	 * @generated
	 */
	TreeFeatureNodeWizardPageProvider createTreeFeatureNodeWizardPageProvider();

		/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyCommonEMFUIPackage getApogyCommonEMFUIPackage();

} //ApogyCommonEMFUIFactory
