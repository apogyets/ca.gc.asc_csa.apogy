/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.NamedDescribedWizardPage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Named Described Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NamedDescribedWizardPagesProviderImpl extends WizardPagesProviderImpl implements NamedDescribedWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NamedDescribedWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		if(eObject instanceof Named && eObject instanceof Described){
			Named named = (Named) eObject;

			if (named.getName() == null || "".equals(named.getName())) {
				if (settings instanceof NamedSetting) {
					NamedSetting namedSetting = (NamedSetting) settings;
					if (namedSetting.getContainingFeature() != null && namedSetting.getParent() != null) {
						ApogyCommonTransactionFacade.INSTANCE.basicSet(named, ApogyCommonEMFPackage.Literals.NAMED__NAME,
								ApogyCommonEMFFacade.INSTANCE.getDefaultName(namedSetting.getParent(), named,
										namedSetting.getContainingFeature()),
								true);
					}
				} else {
					ApogyCommonTransactionFacade.INSTANCE.basicSet(named, ApogyCommonEMFPackage.Literals.NAMED__NAME,
							named.eClass().getName());
				}
			}

			list.add(new NamedDescribedWizardPage((Named) eObject, (Described)eObject));
		}		
		return list;	
	}
} //NamedDescribedWizardPagesProviderImpl
