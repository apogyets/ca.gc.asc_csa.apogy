package ca.gc.asc_csa.apogy.common.emf.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc --> * <!-- begin-model-doc -->
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca),
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCommonEMFUI' copyrightText='Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation' childCreationExtenders='true' suppressGenModelAnnotations='false' modelName='ApogyCommonEMFUI' publicConstructors='true' modelDirectory='/ca.gc.asc_csa.apogy.common.emf.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.common.emf.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.common.emf'"
 * @generated
 */
public interface ApogyCommonEMFUIPackage extends EPackage
{
  /**
	 * The package name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  String eNAME = "ui";

  /**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  String eNS_URI = "ca.gc.asc_csa.apogy.common.emf.ui";

  /**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  String eNS_PREFIX = "ui";

  /**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  ApogyCommonEMFUIPackage eINSTANCE = ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl.init();

  /**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIFacadeImpl <em>Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIFacadeImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getApogyCommonEMFUIFacade()
	 * @generated
	 */
	int APOGY_COMMON_EMFUI_FACADE = 0;

		/**
	 * The feature id for the '<em><b>Unit Converter Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE__UNIT_CONVERTER_MAP = 0;

		/**
	 * The number of structural features of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE_FEATURE_COUNT = 1;

		/**
	 * The operation id for the '<em>Get Color For Range</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___GET_COLOR_FOR_RANGE__RANGES = 0;

		/**
	 * The operation id for the '<em>Get Display Units</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_UNITS__ETYPEDELEMENT = 1;

		/**
	 * The operation id for the '<em>Get Display Units</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_UNITS__EOPERATION_EOPERATIONEPARAMETERSUNITSPROVIDERPARAMETERS = 2;

		/**
	 * The operation id for the '<em>Add Units Provider To Registry</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___ADD_UNITS_PROVIDER_TO_REGISTRY__ETYPEDELEMENT_UNITSPROVIDER = 3;

		/**
	 * The operation id for the '<em>Convert To Native Units</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___CONVERT_TO_NATIVE_UNITS__NUMBER_UNIT_UNIT_ECLASSIFIER = 4;

		/**
	 * The operation id for the '<em>Get Display Format</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_FORMAT__ETYPEDELEMENT = 5;

		/**
	 * The operation id for the '<em>Get Display Format</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_FORMAT__EOPERATION_EOPERATIONEPARAMETERSFORMATPROVIDERPARAMETERS = 6;

		/**
	 * The operation id for the '<em>Add Format Provider To Registry</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___ADD_FORMAT_PROVIDER_TO_REGISTRY__ETYPEDELEMENT_FORMATPROVIDER = 7;

		/**
	 * The operation id for the '<em>Open Delete Named Dialog</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___OPEN_DELETE_NAMED_DIALOG__NAMED = 8;

		/**
	 * The operation id for the '<em>Open Delete Named Dialog</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___OPEN_DELETE_NAMED_DIALOG__LIST = 9;

		/**
	 * The operation id for the '<em>Save To Persisted State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___SAVE_TO_PERSISTED_STATE__MPART_STRING_EOBJECT_RESOURCESET = 10;

		/**
	 * The operation id for the '<em>Read From Persisted State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___READ_FROM_PERSISTED_STATE__MPART_STRING_RESOURCESET = 11;

		/**
	 * The operation id for the '<em>Get Image</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE___GET_IMAGE__ECLASS = 12;

		/**
	 * The number of operations of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_EMFUI_FACADE_OPERATION_COUNT = 13;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SelectionBasedTimeSourceImpl <em>Selection Based Time Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.SelectionBasedTimeSourceImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getSelectionBasedTimeSource()
	 * @generated
	 */
	int SELECTION_BASED_TIME_SOURCE = 1;

		/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE__NAME = ApogyCommonEMFPackage.TIME_SOURCE__NAME;

		/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE__DESCRIPTION = ApogyCommonEMFPackage.TIME_SOURCE__DESCRIPTION;

		/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE__TIME = ApogyCommonEMFPackage.TIME_SOURCE__TIME;

		/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE__OFFSET = ApogyCommonEMFPackage.TIME_SOURCE__OFFSET;

		/**
	 * The feature id for the '<em><b>Selection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE__SELECTION = ApogyCommonEMFPackage.TIME_SOURCE_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Selection Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE = ApogyCommonEMFPackage.TIME_SOURCE_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Selection Based Time Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE_FEATURE_COUNT = ApogyCommonEMFPackage.TIME_SOURCE_FEATURE_COUNT + 2;

		/**
	 * The operation id for the '<em>Dispose</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE___DISPOSE = ApogyCommonEMFPackage.TIME_SOURCE___DISPOSE;

		/**
	 * The number of operations of the '<em>Selection Based Time Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SELECTION_BASED_TIME_SOURCE_OPERATION_COUNT = ApogyCommonEMFPackage.TIME_SOURCE_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EClassSettingsImpl <em>EClass Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EClassSettingsImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEClassSettings()
	 * @generated
	 */
	int ECLASS_SETTINGS = 2;

		/**
	 * The number of structural features of the '<em>EClass Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ECLASS_SETTINGS_FEATURE_COUNT = 0;

		/**
	 * The number of operations of the '<em>EClass Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ECLASS_SETTINGS_OPERATION_COUNT = 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.MapBasedEClassSettingsImpl <em>Map Based EClass Settings</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.MapBasedEClassSettingsImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getMapBasedEClassSettings()
	 * @generated
	 */
	int MAP_BASED_ECLASS_SETTINGS = 3;

		/**
	 * The feature id for the '<em><b>User Data Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int MAP_BASED_ECLASS_SETTINGS__USER_DATA_MAP = ECLASS_SETTINGS_FEATURE_COUNT + 0;

		/**
	 * The number of structural features of the '<em>Map Based EClass Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int MAP_BASED_ECLASS_SETTINGS_FEATURE_COUNT = ECLASS_SETTINGS_FEATURE_COUNT + 1;

		/**
	 * The number of operations of the '<em>Map Based EClass Settings</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int MAP_BASED_ECLASS_SETTINGS_OPERATION_COUNT = ECLASS_SETTINGS_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderImpl <em>Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getWizardPagesProvider()
	 * @generated
	 */
	int WIZARD_PAGES_PROVIDER = 4;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER__PAGES = 0;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER__EOBJECT = 1;

		/**
	 * The number of structural features of the '<em>Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_FEATURE_COUNT = 2;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = 0;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = 1;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = 2;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = 3;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = 4;

		/**
	 * The number of operations of the '<em>Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_OPERATION_COUNT = 5;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl <em>Wizard Pages Provider Registry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getWizardPagesProviderRegistry()
	 * @generated
	 */
	int WIZARD_PAGES_PROVIDER_REGISTRY = 5;

		/**
	 * The feature id for the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID = 0;

		/**
	 * The feature id for the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID = 1;

		/**
	 * The feature id for the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID = 2;

		/**
	 * The feature id for the '<em><b>Wizard Pages Map</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_MAP = 3;

		/**
	 * The number of structural features of the '<em>Wizard Pages Provider Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_REGISTRY_FEATURE_COUNT = 4;

		/**
	 * The operation id for the '<em>Get Provider</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_REGISTRY___GET_PROVIDER__ECLASS = 0;

		/**
	 * The number of operations of the '<em>Wizard Pages Provider Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int WIZARD_PAGES_PROVIDER_REGISTRY_OPERATION_COUNT = 1;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl <em>Named Described Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getNamedDescribedWizardPagesProvider()
	 * @generated
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER = 6;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__PAGES = WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__EOBJECT = WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Named Described Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Named Described Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedSettingImpl <em>Named Setting</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedSettingImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getNamedSetting()
	 * @generated
	 */
	int NAMED_SETTING = 7;

		/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_SETTING__PARENT = ECLASS_SETTINGS_FEATURE_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Containing Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_SETTING__CONTAINING_FEATURE = ECLASS_SETTINGS_FEATURE_COUNT + 1;

		/**
	 * The number of structural features of the '<em>Named Setting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_SETTING_FEATURE_COUNT = ECLASS_SETTINGS_FEATURE_COUNT + 2;

		/**
	 * The number of operations of the '<em>Named Setting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int NAMED_SETTING_OPERATION_COUNT = ECLASS_SETTINGS_OPERATION_COUNT + 0;
	 /**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.DisplayUnitsRegistryImpl <em>Display Units Registry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.DisplayUnitsRegistryImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getDisplayUnitsRegistry()
	 * @generated
	 */
	int DISPLAY_UNITS_REGISTRY = 8;

		/**
	 * The feature id for the '<em><b>Entries Map</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int DISPLAY_UNITS_REGISTRY__ENTRIES_MAP = 0;

		/**
	 * The number of structural features of the '<em>Display Units Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int DISPLAY_UNITS_REGISTRY_FEATURE_COUNT = 1;

		/**
	 * The operation id for the '<em>Save</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int DISPLAY_UNITS_REGISTRY___SAVE = 0;

		/**
	 * The number of operations of the '<em>Display Units Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int DISPLAY_UNITS_REGISTRY_OPERATION_COUNT = 1;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToUnitsKeyValueImpl <em>ETyped Element To Units Key Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToUnitsKeyValueImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getETypedElementToUnitsKeyValue()
	 * @generated
	 */
	int ETYPED_ELEMENT_TO_UNITS_KEY_VALUE = 10;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToUnitsMapImpl <em>ETyped Element To Units Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToUnitsMapImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getETypedElementToUnitsMap()
	 * @generated
	 */
	int ETYPED_ELEMENT_TO_UNITS_MAP = 9;

		/**
	 * The feature id for the '<em><b>Entries</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_UNITS_MAP__ENTRIES = 0;

		/**
	 * The number of structural features of the '<em>ETyped Element To Units Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_UNITS_MAP_FEATURE_COUNT = 1;

		/**
	 * The number of operations of the '<em>ETyped Element To Units Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_UNITS_MAP_OPERATION_COUNT = 0;

		/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_UNITS_KEY_VALUE__KEY = 0;

		/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_UNITS_KEY_VALUE__VALUE = 1;

		/**
	 * The number of structural features of the '<em>ETyped Element To Units Key Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_UNITS_KEY_VALUE_FEATURE_COUNT = 2;

		/**
	 * The number of operations of the '<em>ETyped Element To Units Key Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_UNITS_KEY_VALUE_OPERATION_COUNT = 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider <em>Units Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getUnitsProvider()
	 * @generated
	 */
	int UNITS_PROVIDER = 11;

		/**
	 * The number of structural features of the '<em>Units Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int UNITS_PROVIDER_FEATURE_COUNT = 0;

		/**
	 * The operation id for the '<em>Get Provided Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int UNITS_PROVIDER___GET_PROVIDED_UNIT__UNITSPROVIDERPARAMETERS = 0;

		/**
	 * The number of operations of the '<em>Units Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int UNITS_PROVIDER_OPERATION_COUNT = 1;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters <em>Units Provider Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getUnitsProviderParameters()
	 * @generated
	 */
	int UNITS_PROVIDER_PARAMETERS = 12;

		/**
	 * The number of structural features of the '<em>Units Provider Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int UNITS_PROVIDER_PARAMETERS_FEATURE_COUNT = 0;

		/**
	 * The number of operations of the '<em>Units Provider Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int UNITS_PROVIDER_PARAMETERS_OPERATION_COUNT = 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleUnitsProviderImpl <em>Simple Units Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleUnitsProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getSimpleUnitsProvider()
	 * @generated
	 */
	int SIMPLE_UNITS_PROVIDER = 13;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersUnitsProviderImpl <em>EOperation EParameters Units Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersUnitsProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEOperationEParametersUnitsProvider()
	 * @generated
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER = 14;

		/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_UNITS_PROVIDER__UNIT = UNITS_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The number of structural features of the '<em>Simple Units Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_UNITS_PROVIDER_FEATURE_COUNT = UNITS_PROVIDER_FEATURE_COUNT + 1;

		/**
	 * The operation id for the '<em>Get Provided Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_UNITS_PROVIDER___GET_PROVIDED_UNIT__UNITSPROVIDERPARAMETERS = UNITS_PROVIDER___GET_PROVIDED_UNIT__UNITSPROVIDERPARAMETERS;

		/**
	 * The number of operations of the '<em>Simple Units Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_UNITS_PROVIDER_OPERATION_COUNT = UNITS_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The feature id for the '<em><b>Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER__MAP = UNITS_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The number of structural features of the '<em>EOperation EParameters Units Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER_FEATURE_COUNT = UNITS_PROVIDER_FEATURE_COUNT + 1;

		/**
	 * The operation id for the '<em>Get Provided Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER___GET_PROVIDED_UNIT__UNITSPROVIDERPARAMETERS = UNITS_PROVIDER___GET_PROVIDED_UNIT__UNITSPROVIDERPARAMETERS;

		/**
	 * The number of operations of the '<em>EOperation EParameters Units Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER_OPERATION_COUNT = UNITS_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersUnitsProviderParametersImpl <em>EOperation EParameters Units Provider Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersUnitsProviderParametersImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEOperationEParametersUnitsProviderParameters()
	 * @generated
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS = 15;

		/**
	 * The feature id for the '<em><b>Param</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS__PARAM = UNITS_PROVIDER_PARAMETERS_FEATURE_COUNT + 0;

		/**
	 * The number of structural features of the '<em>EOperation EParameters Units Provider Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS_FEATURE_COUNT = UNITS_PROVIDER_PARAMETERS_FEATURE_COUNT + 1;

		/**
	 * The number of operations of the '<em>EOperation EParameters Units Provider Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS_OPERATION_COUNT = UNITS_PROVIDER_PARAMETERS_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.DecimalFormatRegistryImpl <em>Decimal Format Registry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.DecimalFormatRegistryImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getDecimalFormatRegistry()
	 * @generated
	 */
	int DECIMAL_FORMAT_REGISTRY = 16;

		/**
	 * The feature id for the '<em><b>Entries Map</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP = 0;

		/**
	 * The number of structural features of the '<em>Decimal Format Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int DECIMAL_FORMAT_REGISTRY_FEATURE_COUNT = 1;

		/**
	 * The operation id for the '<em>Save</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int DECIMAL_FORMAT_REGISTRY___SAVE = 0;

		/**
	 * The number of operations of the '<em>Decimal Format Registry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int DECIMAL_FORMAT_REGISTRY_OPERATION_COUNT = 1;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringMapImpl <em>ETyped Element To Format String Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringMapImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getETypedElementToFormatStringMap()
	 * @generated
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_MAP = 17;

		/**
	 * The feature id for the '<em><b>Entries</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_MAP__ENTRIES = 0;

		/**
	 * The number of structural features of the '<em>ETyped Element To Format String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_MAP_FEATURE_COUNT = 1;

		/**
	 * The number of operations of the '<em>ETyped Element To Format String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_MAP_OPERATION_COUNT = 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringKeyValueImpl <em>ETyped Element To Format String Key Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringKeyValueImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getETypedElementToFormatStringKeyValue()
	 * @generated
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE = 18;

		/**
	 * The feature id for the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE__KEY = 0;

		/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE__VALUE = 1;

		/**
	 * The number of structural features of the '<em>ETyped Element To Format String Key Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE_FEATURE_COUNT = 2;

		/**
	 * The number of operations of the '<em>ETyped Element To Format String Key Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE_OPERATION_COUNT = 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider <em>Format Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getFormatProvider()
	 * @generated
	 */
	int FORMAT_PROVIDER = 19;

		/**
	 * The number of structural features of the '<em>Format Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FORMAT_PROVIDER_FEATURE_COUNT = 0;

		/**
	 * The operation id for the '<em>Get Provided Format</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS = 0;

		/**
	 * The number of operations of the '<em>Format Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FORMAT_PROVIDER_OPERATION_COUNT = 1;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters <em>Format Provider Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getFormatProviderParameters()
	 * @generated
	 */
	int FORMAT_PROVIDER_PARAMETERS = 20;

		/**
	 * The number of structural features of the '<em>Format Provider Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FORMAT_PROVIDER_PARAMETERS_FEATURE_COUNT = 0;

		/**
	 * The number of operations of the '<em>Format Provider Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int FORMAT_PROVIDER_PARAMETERS_OPERATION_COUNT = 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleFormatProviderImpl <em>Simple Format Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleFormatProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getSimpleFormatProvider()
	 * @generated
	 */
	int SIMPLE_FORMAT_PROVIDER = 21;

		/**
	 * The feature id for the '<em><b>Format Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_FORMAT_PROVIDER__FORMAT_PATTERN = FORMAT_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The number of structural features of the '<em>Simple Format Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_FORMAT_PROVIDER_FEATURE_COUNT = FORMAT_PROVIDER_FEATURE_COUNT + 1;

		/**
	 * The operation id for the '<em>Get Provided Format</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS = FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS;

		/**
	 * The number of operations of the '<em>Simple Format Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int SIMPLE_FORMAT_PROVIDER_OPERATION_COUNT = FORMAT_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderImpl <em>EOperation EParameters Format Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEOperationEParametersFormatProvider()
	 * @generated
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER = 22;

		/**
	 * The feature id for the '<em><b>Map</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP = FORMAT_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The number of structural features of the '<em>EOperation EParameters Format Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER_FEATURE_COUNT = FORMAT_PROVIDER_FEATURE_COUNT + 1;

		/**
	 * The operation id for the '<em>Get Provided Format</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS = FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS;

		/**
	 * The number of operations of the '<em>EOperation EParameters Format Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER_OPERATION_COUNT = FORMAT_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderParametersImpl <em>EOperation EParameters Format Provider Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderParametersImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEOperationEParametersFormatProviderParameters()
	 * @generated
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS = 23;

		/**
	 * The feature id for the '<em><b>Param</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS__PARAM = FORMAT_PROVIDER_PARAMETERS_FEATURE_COUNT + 0;

		/**
	 * The number of structural features of the '<em>EOperation EParameters Format Provider Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS_FEATURE_COUNT = FORMAT_PROVIDER_PARAMETERS_FEATURE_COUNT + 1;

		/**
	 * The number of operations of the '<em>EOperation EParameters Format Provider Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS_OPERATION_COUNT = FORMAT_PROVIDER_PARAMETERS_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.TreeFeatureNodeWizardPageProviderImpl <em>Tree Feature Node Wizard Page Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.TreeFeatureNodeWizardPageProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getTreeFeatureNodeWizardPageProvider()
	 * @generated
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER = 24;

		/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER__PAGES = WIZARD_PAGES_PROVIDER__PAGES;

		/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER__EOBJECT = WIZARD_PAGES_PROVIDER__EOBJECT;

		/**
	 * The number of structural features of the '<em>Tree Feature Node Wizard Page Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER_FEATURE_COUNT = WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

		/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

		/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

		/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

		/**
	 * The number of operations of the '<em>Tree Feature Node Wizard Page Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 * @ordered
	 */
	int TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER_OPERATION_COUNT = WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

		/**
	 * The meta object id for the '<em>Hash Map</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.HashMap
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getHashMap()
	 * @generated
	 */
	int HASH_MAP = 25;

		/**
	 * The meta object id for the '<em>Color</em>' data type.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @see org.eclipse.swt.graphics.Color
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getColor()
	 * @generated
	 */
  int COLOR = 26;


  /**
	 * The meta object id for the '<em>List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.List
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getList()
	 * @generated
	 */

	int LIST = 27;


		/**
	 * The meta object id for the '<em>Wizard Page</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.jface.wizard.WizardPage
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getWizardPage()
	 * @generated
	 */
	int WIZARD_PAGE = 28;


		/**
	 * The meta object id for the '<em>Map</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.Map
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getMap()
	 * @generated
	 */

	int MAP = 29;

		/**
	 * The meta object id for the '<em>IWizard Page</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.jface.wizard.IWizardPage
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getIWizardPage()
	 * @generated
	 */

	int IWIZARD_PAGE = 30;


		/**
	 * The meta object id for the '<em>Compound Command</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.emf.common.command.CompoundCommand
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getCompoundCommand()
	 * @generated
	 */

	int COMPOUND_COMMAND = 31;


		/**
	 * The meta object id for the '<em>Editing Domain</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.emf.edit.domain.EditingDomain
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEditingDomain()
	 * @generated
	 */

	int EDITING_DOMAIN = 32;



		/**
	 * The meta object id for the '<em>Resource Set</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.emf.ecore.resource.ResourceSet
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getResourceSet()
	 * @generated
	 */
	int RESOURCE_SET = 33;

		/**
	 * The meta object id for the '<em>MPart</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.e4.ui.model.application.ui.basic.MPart
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getMPart()
	 * @generated
	 */
	int MPART = 34;



		/**
	 * The meta object id for the '<em>ESelection Service</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.e4.ui.workbench.modeling.ESelectionService
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getESelectionService()
	 * @generated
	 */
	int ESELECTION_SERVICE = 35;



		/**
	 * The meta object id for the '<em>Image</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.swt.graphics.Image
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getImage()
	 * @generated
	 */
	int IMAGE = 36;



		/**
	 * The meta object id for the '<em>Unit Converter Map</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.TreeMap
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getUnitConverterMap()
	 * @generated
	 */
	int UNIT_CONVERTER_MAP = 37;



		/**
	 * The meta object id for the '<em>Display Units Map</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.util.HashMap
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getDisplayUnitsMap()
	 * @generated
	 */
	int DISPLAY_UNITS_MAP = 38;



		/**
	 * The meta object id for the '<em>Decimal Format</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see java.text.DecimalFormat
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getDecimalFormat()
	 * @generated
	 */
	int DECIMAL_FORMAT = 39;

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade <em>Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade
	 * @generated
	 */
	EClass getApogyCommonEMFUIFacade();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getUnitConverterMap <em>Unit Converter Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>Unit Converter Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getUnitConverterMap()
	 * @see #getApogyCommonEMFUIFacade()
	 * @generated
	 */
	EAttribute getApogyCommonEMFUIFacade_UnitConverterMap();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getColorForRange(ca.gc.asc_csa.apogy.common.emf.Ranges) <em>Get Color For Range</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Color For Range</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getColorForRange(ca.gc.asc_csa.apogy.common.emf.Ranges)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__GetColorForRange__Ranges();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getDisplayUnits(org.eclipse.emf.ecore.ETypedElement) <em>Get Display Units</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Display Units</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getDisplayUnits(org.eclipse.emf.ecore.ETypedElement)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__GetDisplayUnits__ETypedElement();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getDisplayUnits(org.eclipse.emf.ecore.EOperation, ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters) <em>Get Display Units</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Display Units</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getDisplayUnits(org.eclipse.emf.ecore.EOperation, ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__GetDisplayUnits__EOperation_EOperationEParametersUnitsProviderParameters();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#addUnitsProviderToRegistry(org.eclipse.emf.ecore.ETypedElement, ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider) <em>Add Units Provider To Registry</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Add Units Provider To Registry</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#addUnitsProviderToRegistry(org.eclipse.emf.ecore.ETypedElement, ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__AddUnitsProviderToRegistry__ETypedElement_UnitsProvider();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#convertToNativeUnits(java.lang.Number, javax.measure.unit.Unit, javax.measure.unit.Unit, org.eclipse.emf.ecore.EClassifier) <em>Convert To Native Units</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Convert To Native Units</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#convertToNativeUnits(java.lang.Number, javax.measure.unit.Unit, javax.measure.unit.Unit, org.eclipse.emf.ecore.EClassifier)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__ConvertToNativeUnits__Number_Unit_Unit_EClassifier();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getDisplayFormat(org.eclipse.emf.ecore.ETypedElement) <em>Get Display Format</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Display Format</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getDisplayFormat(org.eclipse.emf.ecore.ETypedElement)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__GetDisplayFormat__ETypedElement();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getDisplayFormat(org.eclipse.emf.ecore.EOperation, ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters) <em>Get Display Format</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Display Format</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getDisplayFormat(org.eclipse.emf.ecore.EOperation, ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__GetDisplayFormat__EOperation_EOperationEParametersFormatProviderParameters();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#addFormatProviderToRegistry(org.eclipse.emf.ecore.ETypedElement, ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider) <em>Add Format Provider To Registry</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Add Format Provider To Registry</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#addFormatProviderToRegistry(org.eclipse.emf.ecore.ETypedElement, ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__AddFormatProviderToRegistry__ETypedElement_FormatProvider();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#openDeleteNamedDialog(ca.gc.asc_csa.apogy.common.emf.Named) <em>Open Delete Named Dialog</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Open Delete Named Dialog</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#openDeleteNamedDialog(ca.gc.asc_csa.apogy.common.emf.Named)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__Named();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#openDeleteNamedDialog(java.util.List) <em>Open Delete Named Dialog</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Open Delete Named Dialog</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#openDeleteNamedDialog(java.util.List)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__List();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#saveToPersistedState(org.eclipse.e4.ui.model.application.ui.basic.MPart, java.lang.String, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.resource.ResourceSet) <em>Save To Persisted State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Save To Persisted State</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#saveToPersistedState(org.eclipse.e4.ui.model.application.ui.basic.MPart, java.lang.String, org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.resource.ResourceSet)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__SaveToPersistedState__MPart_String_EObject_ResourceSet();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#readFromPersistedState(org.eclipse.e4.ui.model.application.ui.basic.MPart, java.lang.String, org.eclipse.emf.ecore.resource.ResourceSet) <em>Read From Persisted State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Read From Persisted State</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#readFromPersistedState(org.eclipse.e4.ui.model.application.ui.basic.MPart, java.lang.String, org.eclipse.emf.ecore.resource.ResourceSet)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__ReadFromPersistedState__MPart_String_ResourceSet();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getImage(org.eclipse.emf.ecore.EClass) <em>Get Image</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Image</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade#getImage(org.eclipse.emf.ecore.EClass)
	 * @generated
	 */
	EOperation getApogyCommonEMFUIFacade__GetImage__EClass();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource <em>Selection Based Time Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Selection Based Time Source</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource
	 * @generated
	 */
	EClass getSelectionBasedTimeSource();

		/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource#getSelection <em>Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Selection</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource#getSelection()
	 * @see #getSelectionBasedTimeSource()
	 * @generated
	 */
	EReference getSelectionBasedTimeSource_Selection();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource#getSelectionService <em>Selection Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>Selection Service</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource#getSelectionService()
	 * @see #getSelectionBasedTimeSource()
	 * @generated
	 */
	EAttribute getSelectionBasedTimeSource_SelectionService();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings <em>EClass Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>EClass Settings</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings
	 * @generated
	 */
	EClass getEClassSettings();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings <em>Map Based EClass Settings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Map Based EClass Settings</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings
	 * @generated
	 */
	EClass getMapBasedEClassSettings();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings#getUserDataMap <em>User Data Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>User Data Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings#getUserDataMap()
	 * @see #getMapBasedEClassSettings()
	 * @generated
	 */
	EAttribute getMapBasedEClassSettings_UserDataMap();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider <em>Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider
	 * @generated
	 */
	EClass getWizardPagesProvider();

		/**
	 * Returns the meta object for the attribute list '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getPages <em>Pages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute list '<em>Pages</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getPages()
	 * @see #getWizardPagesProvider()
	 * @generated
	 */
	EAttribute getWizardPagesProvider_Pages();

		/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getEObject <em>EObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>EObject</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getEObject()
	 * @see #getWizardPagesProvider()
	 * @generated
	 */
	EReference getWizardPagesProvider_EObject();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getPages(org.eclipse.emf.ecore.EClass, ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings) <em>Get Pages</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Pages</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getPages(org.eclipse.emf.ecore.EClass, ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings)
	 * @generated
	 */
	EOperation getWizardPagesProvider__GetPages__EClass_EClassSettings();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#createEObject(org.eclipse.emf.ecore.EClass, ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings) <em>Create EObject</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Create EObject</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#createEObject(org.eclipse.emf.ecore.EClass, ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings)
	 * @generated
	 */
	EOperation getWizardPagesProvider__CreateEObject__EClass_EClassSettings();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#instantiateWizardPages(org.eclipse.emf.ecore.EObject, ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings) <em>Instantiate Wizard Pages</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Instantiate Wizard Pages</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#instantiateWizardPages(org.eclipse.emf.ecore.EObject, ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings)
	 * @generated
	 */
	EOperation getWizardPagesProvider__InstantiateWizardPages__EObject_EClassSettings();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getPerformFinishCommands(org.eclipse.emf.ecore.EObject, ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings, org.eclipse.emf.edit.domain.EditingDomain) <em>Get Perform Finish Commands</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Perform Finish Commands</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getPerformFinishCommands(org.eclipse.emf.ecore.EObject, ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings, org.eclipse.emf.edit.domain.EditingDomain)
	 * @generated
	 */
	EOperation getWizardPagesProvider__GetPerformFinishCommands__EObject_EClassSettings_EditingDomain();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getNextPage(org.eclipse.jface.wizard.IWizardPage) <em>Get Next Page</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Next Page</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider#getNextPage(org.eclipse.jface.wizard.IWizardPage)
	 * @generated
	 */
	EOperation getWizardPagesProvider__GetNextPage__IWizardPage();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry <em>Wizard Pages Provider Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Wizard Pages Provider Registry</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry
	 * @generated
	 */
	EClass getWizardPagesProviderRegistry();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID()
	 * @see #getWizardPagesProviderRegistry()
	 * @generated
	 */
	EAttribute getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID()
	 * @see #getWizardPagesProviderRegistry()
	 * @generated
	 */
	EAttribute getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID()
	 * @see #getWizardPagesProviderRegistry()
	 * @generated
	 */
	EAttribute getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWizardPagesMap <em>Wizard Pages Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>Wizard Pages Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getWizardPagesMap()
	 * @see #getWizardPagesProviderRegistry()
	 * @generated
	 */
	EAttribute getWizardPagesProviderRegistry_WizardPagesMap();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getProvider(org.eclipse.emf.ecore.EClass) <em>Get Provider</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Provider</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry#getProvider(org.eclipse.emf.ecore.EClass)
	 * @generated
	 */
	EOperation getWizardPagesProviderRegistry__GetProvider__EClass();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider <em>Named Described Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Named Described Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
	 * @generated
	 */
	EClass getNamedDescribedWizardPagesProvider();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting <em>Named Setting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Named Setting</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting
	 * @generated
	 */
	EClass getNamedSetting();

		/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting#getParent()
	 * @see #getNamedSetting()
	 * @generated
	 */
	EReference getNamedSetting_Parent();

		/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting#getContainingFeature <em>Containing Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Containing Feature</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting#getContainingFeature()
	 * @see #getNamedSetting()
	 * @generated
	 */
	EReference getNamedSetting_ContainingFeature();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry <em>Display Units Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Display Units Registry</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry
	 * @generated
	 */
	EClass getDisplayUnitsRegistry();

		/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry#getEntriesMap <em>Entries Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Entries Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry#getEntriesMap()
	 * @see #getDisplayUnitsRegistry()
	 * @generated
	 */
	EReference getDisplayUnitsRegistry_EntriesMap();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry#save() <em>Save</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Save</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry#save()
	 * @generated
	 */
	EOperation getDisplayUnitsRegistry__Save();

		/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>ETyped Element To Units Key Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>ETyped Element To Units Key Value</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.ETypedElement"
	 *        valueType="ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider" valueContainment="true"
	 * @generated
	 */
	EClass getETypedElementToUnitsKeyValue();

		/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getETypedElementToUnitsKeyValue()
	 * @generated
	 */
	EReference getETypedElementToUnitsKeyValue_Key();

		/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getETypedElementToUnitsKeyValue()
	 * @generated
	 */
	EReference getETypedElementToUnitsKeyValue_Value();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap <em>ETyped Element To Units Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>ETyped Element To Units Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap
	 * @generated
	 */
	EClass getETypedElementToUnitsMap();

		/**
	 * Returns the meta object for the map '{@link ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap#getEntries <em>Entries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the map '<em>Entries</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap#getEntries()
	 * @see #getETypedElementToUnitsMap()
	 * @generated
	 */
	EReference getETypedElementToUnitsMap_Entries();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider <em>Units Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Units Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider
	 * @generated
	 */
	EClass getUnitsProvider();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider#getProvidedUnit(ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters) <em>Get Provided Unit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Provided Unit</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider#getProvidedUnit(ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters)
	 * @generated
	 */
	EOperation getUnitsProvider__GetProvidedUnit__UnitsProviderParameters();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters <em>Units Provider Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Units Provider Parameters</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters
	 * @generated
	 */
	EClass getUnitsProviderParameters();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider <em>Simple Units Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Simple Units Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider
	 * @generated
	 */
	EClass getSimpleUnitsProvider();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider#getUnit()
	 * @see #getSimpleUnitsProvider()
	 * @generated
	 */
	EAttribute getSimpleUnitsProvider_Unit();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider <em>EOperation EParameters Units Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>EOperation EParameters Units Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider
	 * @generated
	 */
	EClass getEOperationEParametersUnitsProvider();

		/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider#getMap <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the containment reference '<em>Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider#getMap()
	 * @see #getEOperationEParametersUnitsProvider()
	 * @generated
	 */
	EReference getEOperationEParametersUnitsProvider_Map();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters <em>EOperation EParameters Units Provider Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>EOperation EParameters Units Provider Parameters</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters
	 * @generated
	 */
	EClass getEOperationEParametersUnitsProviderParameters();

		/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters#getParam <em>Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Param</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters#getParam()
	 * @see #getEOperationEParametersUnitsProviderParameters()
	 * @generated
	 */
	EReference getEOperationEParametersUnitsProviderParameters_Param();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry <em>Decimal Format Registry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Decimal Format Registry</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry
	 * @generated
	 */
	EClass getDecimalFormatRegistry();

		/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry#getEntriesMap <em>Entries Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Entries Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry#getEntriesMap()
	 * @see #getDecimalFormatRegistry()
	 * @generated
	 */
	EReference getDecimalFormatRegistry_EntriesMap();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry#save() <em>Save</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Save</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry#save()
	 * @generated
	 */
	EOperation getDecimalFormatRegistry__Save();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap <em>ETyped Element To Format String Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>ETyped Element To Format String Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap
	 * @generated
	 */
	EClass getETypedElementToFormatStringMap();

		/**
	 * Returns the meta object for the map '{@link ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap#getEntries <em>Entries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the map '<em>Entries</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap#getEntries()
	 * @see #getETypedElementToFormatStringMap()
	 * @generated
	 */
	EReference getETypedElementToFormatStringMap_Entries();

		/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>ETyped Element To Format String Key Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>ETyped Element To Format String Key Value</em>'.
	 * @see java.util.Map.Entry
	 * @model keyType="org.eclipse.emf.ecore.ETypedElement"
	 *        valueType="ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider" valueContainment="true"
	 * @generated
	 */
	EClass getETypedElementToFormatStringKeyValue();

		/**
	 * Returns the meta object for the reference '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getETypedElementToFormatStringKeyValue()
	 * @generated
	 */
	EReference getETypedElementToFormatStringKeyValue_Key();

		/**
	 * Returns the meta object for the containment reference '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getETypedElementToFormatStringKeyValue()
	 * @generated
	 */
	EReference getETypedElementToFormatStringKeyValue_Value();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider <em>Format Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Format Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider
	 * @generated
	 */
	EClass getFormatProvider();

		/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider#getProvidedFormat(ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters) <em>Get Provided Format</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the '<em>Get Provided Format</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider#getProvidedFormat(ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters)
	 * @generated
	 */
	EOperation getFormatProvider__GetProvidedFormat__FormatProviderParameters();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters <em>Format Provider Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Format Provider Parameters</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters
	 * @generated
	 */
	EClass getFormatProviderParameters();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider <em>Simple Format Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Simple Format Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider
	 * @generated
	 */
	EClass getSimpleFormatProvider();

		/**
	 * Returns the meta object for the attribute '{@link ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider#getFormatPattern <em>Format Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the attribute '<em>Format Pattern</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider#getFormatPattern()
	 * @see #getSimpleFormatProvider()
	 * @generated
	 */
	EAttribute getSimpleFormatProvider_FormatPattern();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider <em>EOperation EParameters Format Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>EOperation EParameters Format Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider
	 * @generated
	 */
	EClass getEOperationEParametersFormatProvider();

		/**
	 * Returns the meta object for the containment reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider#getMap <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the containment reference '<em>Map</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider#getMap()
	 * @see #getEOperationEParametersFormatProvider()
	 * @generated
	 */
	EReference getEOperationEParametersFormatProvider_Map();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters <em>EOperation EParameters Format Provider Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>EOperation EParameters Format Provider Parameters</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters
	 * @generated
	 */
	EClass getEOperationEParametersFormatProviderParameters();

		/**
	 * Returns the meta object for the reference '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters#getParam <em>Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for the reference '<em>Param</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters#getParam()
	 * @see #getEOperationEParametersFormatProviderParameters()
	 * @generated
	 */
	EReference getEOperationEParametersFormatProviderParameters_Param();

		/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.emf.ui.TreeFeatureNodeWizardPageProvider <em>Tree Feature Node Wizard Page Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for class '<em>Tree Feature Node Wizard Page Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.TreeFeatureNodeWizardPageProvider
	 * @generated
	 */
	EClass getTreeFeatureNodeWizardPageProvider();

		/**
	 * Returns the meta object for data type '{@link java.util.HashMap <em>Hash Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Hash Map</em>'.
	 * @see java.util.HashMap
	 * @model instanceClass="java.util.HashMap" typeParameters="key value"
	 * @generated
	 */
	EDataType getHashMap();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.swt.graphics.Color <em>Color</em>}'.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Color</em>'.
	 * @see org.eclipse.swt.graphics.Color
	 * @model instanceClass="org.eclipse.swt.graphics.Color"
	 * @generated
	 */
  EDataType getColor();

  /**
	 * Returns the meta object for data type '{@link java.util.List <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>List</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List<? extends ca.gc.asc_csa.apogy.common.emf.Named>"
	 * @generated
	 */
	EDataType getList();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.jface.wizard.WizardPage <em>Wizard Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Wizard Page</em>'.
	 * @see org.eclipse.jface.wizard.WizardPage
	 * @model instanceClass="org.eclipse.jface.wizard.WizardPage"
	 * @generated
	 */
	EDataType getWizardPage();

		/**
	 * Returns the meta object for data type '{@link java.util.Map <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Map</em>'.
	 * @see java.util.Map
	 * @model instanceClass="java.util.Map" typeParameters="key value"
	 * @generated
	 */
	EDataType getMap();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.jface.wizard.IWizardPage <em>IWizard Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>IWizard Page</em>'.
	 * @see org.eclipse.jface.wizard.IWizardPage
	 * @model instanceClass="org.eclipse.jface.wizard.IWizardPage"
	 * @generated
	 */
	EDataType getIWizardPage();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.common.command.CompoundCommand <em>Compound Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Compound Command</em>'.
	 * @see org.eclipse.emf.common.command.CompoundCommand
	 * @model instanceClass="org.eclipse.emf.common.command.CompoundCommand"
	 * @generated
	 */
	EDataType getCompoundCommand();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.edit.domain.EditingDomain <em>Editing Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Editing Domain</em>'.
	 * @see org.eclipse.emf.edit.domain.EditingDomain
	 * @model instanceClass="org.eclipse.emf.edit.domain.EditingDomain"
	 * @generated
	 */
	EDataType getEditingDomain();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.emf.ecore.resource.ResourceSet <em>Resource Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Resource Set</em>'.
	 * @see org.eclipse.emf.ecore.resource.ResourceSet
	 * @model instanceClass="org.eclipse.emf.ecore.resource.ResourceSet"
	 * @generated
	 */
	EDataType getResourceSet();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.e4.ui.model.application.ui.basic.MPart <em>MPart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>MPart</em>'.
	 * @see org.eclipse.e4.ui.model.application.ui.basic.MPart
	 * @model instanceClass="org.eclipse.e4.ui.model.application.ui.basic.MPart"
	 * @generated
	 */
	EDataType getMPart();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.e4.ui.workbench.modeling.ESelectionService <em>ESelection Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>ESelection Service</em>'.
	 * @see org.eclipse.e4.ui.workbench.modeling.ESelectionService
	 * @model instanceClass="org.eclipse.e4.ui.workbench.modeling.ESelectionService"
	 * @generated
	 */
	EDataType getESelectionService();

		/**
	 * Returns the meta object for data type '{@link org.eclipse.swt.graphics.Image <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Image</em>'.
	 * @see org.eclipse.swt.graphics.Image
	 * @model instanceClass="org.eclipse.swt.graphics.Image"
	 * @generated
	 */
	EDataType getImage();

		/**
	 * Returns the meta object for data type '{@link java.util.TreeMap <em>Unit Converter Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Unit Converter Map</em>'.
	 * @see java.util.TreeMap
	 * @model instanceClass="java.util.TreeMap<javax.measure.converter.UnitConverter, java.lang.String>"
	 * @generated
	 */
	EDataType getUnitConverterMap();

		/**
	 * Returns the meta object for data type '{@link java.util.HashMap <em>Display Units Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Display Units Map</em>'.
	 * @see java.util.HashMap
	 * @model instanceClass="java.util.HashMap<org.eclipse.emf.ecore.ETypedElement, ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider>"
	 * @generated
	 */
	EDataType getDisplayUnitsMap();

		/**
	 * Returns the meta object for data type '{@link java.text.DecimalFormat <em>Decimal Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the meta object for data type '<em>Decimal Format</em>'.
	 * @see java.text.DecimalFormat
	 * @model instanceClass="java.text.DecimalFormat"
	 * @generated
	 */
	EDataType getDecimalFormat();

		/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCommonEMFUIFactory getApogyCommonEMFUIFactory();

		/**
	 * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each operation of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->	 * @generated
	 */
  interface Literals
  {
    /**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIFacadeImpl <em>Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIFacadeImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getApogyCommonEMFUIFacade()
		 * @generated
		 */
		EClass APOGY_COMMON_EMFUI_FACADE = eINSTANCE.getApogyCommonEMFUIFacade();

			/**
		 * The meta object literal for the '<em><b>Unit Converter Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute APOGY_COMMON_EMFUI_FACADE__UNIT_CONVERTER_MAP = eINSTANCE.getApogyCommonEMFUIFacade_UnitConverterMap();

			/**
		 * The meta object literal for the '<em><b>Get Color For Range</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___GET_COLOR_FOR_RANGE__RANGES = eINSTANCE.getApogyCommonEMFUIFacade__GetColorForRange__Ranges();

			/**
		 * The meta object literal for the '<em><b>Get Display Units</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_UNITS__ETYPEDELEMENT = eINSTANCE.getApogyCommonEMFUIFacade__GetDisplayUnits__ETypedElement();

				/**
		 * The meta object literal for the '<em><b>Get Display Units</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_UNITS__EOPERATION_EOPERATIONEPARAMETERSUNITSPROVIDERPARAMETERS = eINSTANCE.getApogyCommonEMFUIFacade__GetDisplayUnits__EOperation_EOperationEParametersUnitsProviderParameters();

				/**
		 * The meta object literal for the '<em><b>Add Units Provider To Registry</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___ADD_UNITS_PROVIDER_TO_REGISTRY__ETYPEDELEMENT_UNITSPROVIDER = eINSTANCE.getApogyCommonEMFUIFacade__AddUnitsProviderToRegistry__ETypedElement_UnitsProvider();

				/**
		 * The meta object literal for the '<em><b>Convert To Native Units</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___CONVERT_TO_NATIVE_UNITS__NUMBER_UNIT_UNIT_ECLASSIFIER = eINSTANCE.getApogyCommonEMFUIFacade__ConvertToNativeUnits__Number_Unit_Unit_EClassifier();

				/**
		 * The meta object literal for the '<em><b>Get Display Format</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_FORMAT__ETYPEDELEMENT = eINSTANCE.getApogyCommonEMFUIFacade__GetDisplayFormat__ETypedElement();

				/**
		 * The meta object literal for the '<em><b>Get Display Format</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___GET_DISPLAY_FORMAT__EOPERATION_EOPERATIONEPARAMETERSFORMATPROVIDERPARAMETERS = eINSTANCE.getApogyCommonEMFUIFacade__GetDisplayFormat__EOperation_EOperationEParametersFormatProviderParameters();

				/**
		 * The meta object literal for the '<em><b>Add Format Provider To Registry</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___ADD_FORMAT_PROVIDER_TO_REGISTRY__ETYPEDELEMENT_FORMATPROVIDER = eINSTANCE.getApogyCommonEMFUIFacade__AddFormatProviderToRegistry__ETypedElement_FormatProvider();

				/**
		 * The meta object literal for the '<em><b>Open Delete Named Dialog</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___OPEN_DELETE_NAMED_DIALOG__NAMED = eINSTANCE.getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__Named();

			/**
		 * The meta object literal for the '<em><b>Open Delete Named Dialog</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___OPEN_DELETE_NAMED_DIALOG__LIST = eINSTANCE.getApogyCommonEMFUIFacade__OpenDeleteNamedDialog__List();

				/**
		 * The meta object literal for the '<em><b>Save To Persisted State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___SAVE_TO_PERSISTED_STATE__MPART_STRING_EOBJECT_RESOURCESET = eINSTANCE.getApogyCommonEMFUIFacade__SaveToPersistedState__MPart_String_EObject_ResourceSet();

				/**
		 * The meta object literal for the '<em><b>Read From Persisted State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___READ_FROM_PERSISTED_STATE__MPART_STRING_RESOURCESET = eINSTANCE.getApogyCommonEMFUIFacade__ReadFromPersistedState__MPart_String_ResourceSet();

				/**
		 * The meta object literal for the '<em><b>Get Image</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation APOGY_COMMON_EMFUI_FACADE___GET_IMAGE__ECLASS = eINSTANCE.getApogyCommonEMFUIFacade__GetImage__EClass();

				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SelectionBasedTimeSourceImpl <em>Selection Based Time Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.SelectionBasedTimeSourceImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getSelectionBasedTimeSource()
		 * @generated
		 */
		EClass SELECTION_BASED_TIME_SOURCE = eINSTANCE.getSelectionBasedTimeSource();

				/**
		 * The meta object literal for the '<em><b>Selection</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference SELECTION_BASED_TIME_SOURCE__SELECTION = eINSTANCE.getSelectionBasedTimeSource_Selection();

				/**
		 * The meta object literal for the '<em><b>Selection Service</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE = eINSTANCE.getSelectionBasedTimeSource_SelectionService();

				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EClassSettingsImpl <em>EClass Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EClassSettingsImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEClassSettings()
		 * @generated
		 */
		EClass ECLASS_SETTINGS = eINSTANCE.getEClassSettings();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.MapBasedEClassSettingsImpl <em>Map Based EClass Settings</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.MapBasedEClassSettingsImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getMapBasedEClassSettings()
		 * @generated
		 */
		EClass MAP_BASED_ECLASS_SETTINGS = eINSTANCE.getMapBasedEClassSettings();

				/**
		 * The meta object literal for the '<em><b>User Data Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute MAP_BASED_ECLASS_SETTINGS__USER_DATA_MAP = eINSTANCE.getMapBasedEClassSettings_UserDataMap();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderImpl <em>Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getWizardPagesProvider()
		 * @generated
		 */
		EClass WIZARD_PAGES_PROVIDER = eINSTANCE.getWizardPagesProvider();

			/**
		 * The meta object literal for the '<em><b>Pages</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute WIZARD_PAGES_PROVIDER__PAGES = eINSTANCE.getWizardPagesProvider_Pages();

			/**
		 * The meta object literal for the '<em><b>EObject</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference WIZARD_PAGES_PROVIDER__EOBJECT = eINSTANCE.getWizardPagesProvider_EObject();

			/**
		 * The meta object literal for the '<em><b>Get Pages</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = eINSTANCE.getWizardPagesProvider__GetPages__EClass_EClassSettings();

			/**
		 * The meta object literal for the '<em><b>Create EObject</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = eINSTANCE.getWizardPagesProvider__CreateEObject__EClass_EClassSettings();

			/**
		 * The meta object literal for the '<em><b>Instantiate Wizard Pages</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = eINSTANCE.getWizardPagesProvider__InstantiateWizardPages__EObject_EClassSettings();

			/**
		 * The meta object literal for the '<em><b>Get Perform Finish Commands</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = eINSTANCE.getWizardPagesProvider__GetPerformFinishCommands__EObject_EClassSettings_EditingDomain();

			/**
		 * The meta object literal for the '<em><b>Get Next Page</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = eINSTANCE.getWizardPagesProvider__GetNextPage__IWizardPage();

				/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl <em>Wizard Pages Provider Registry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getWizardPagesProviderRegistry()
		 * @generated
		 */
		EClass WIZARD_PAGES_PROVIDER_REGISTRY = eINSTANCE.getWizardPagesProviderRegistry();

			/**
		 * The meta object literal for the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID = eINSTANCE.getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID();

				/**
		 * The meta object literal for the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID = eINSTANCE.getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID();

				/**
		 * The meta object literal for the '<em><b>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID = eINSTANCE.getWizardPagesProviderRegistry_WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID();

			/**
		 * The meta object literal for the '<em><b>Wizard Pages Map</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_MAP = eINSTANCE.getWizardPagesProviderRegistry_WizardPagesMap();

			/**
		 * The meta object literal for the '<em><b>Get Provider</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation WIZARD_PAGES_PROVIDER_REGISTRY___GET_PROVIDER__ECLASS = eINSTANCE.getWizardPagesProviderRegistry__GetProvider__EClass();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl <em>Named Described Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getNamedDescribedWizardPagesProvider()
		 * @generated
		 */
		EClass NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER = eINSTANCE.getNamedDescribedWizardPagesProvider();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedSettingImpl <em>Named Setting</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedSettingImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getNamedSetting()
		 * @generated
		 */
		EClass NAMED_SETTING = eINSTANCE.getNamedSetting();

			/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference NAMED_SETTING__PARENT = eINSTANCE.getNamedSetting_Parent();

			/**
		 * The meta object literal for the '<em><b>Containing Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference NAMED_SETTING__CONTAINING_FEATURE = eINSTANCE.getNamedSetting_ContainingFeature();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.DisplayUnitsRegistryImpl <em>Display Units Registry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.DisplayUnitsRegistryImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getDisplayUnitsRegistry()
		 * @generated
		 */
		EClass DISPLAY_UNITS_REGISTRY = eINSTANCE.getDisplayUnitsRegistry();

			/**
		 * The meta object literal for the '<em><b>Entries Map</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference DISPLAY_UNITS_REGISTRY__ENTRIES_MAP = eINSTANCE.getDisplayUnitsRegistry_EntriesMap();

			/**
		 * The meta object literal for the '<em><b>Save</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation DISPLAY_UNITS_REGISTRY___SAVE = eINSTANCE.getDisplayUnitsRegistry__Save();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToUnitsKeyValueImpl <em>ETyped Element To Units Key Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToUnitsKeyValueImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getETypedElementToUnitsKeyValue()
		 * @generated
		 */
		EClass ETYPED_ELEMENT_TO_UNITS_KEY_VALUE = eINSTANCE.getETypedElementToUnitsKeyValue();

			/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference ETYPED_ELEMENT_TO_UNITS_KEY_VALUE__KEY = eINSTANCE.getETypedElementToUnitsKeyValue_Key();

			/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference ETYPED_ELEMENT_TO_UNITS_KEY_VALUE__VALUE = eINSTANCE.getETypedElementToUnitsKeyValue_Value();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToUnitsMapImpl <em>ETyped Element To Units Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToUnitsMapImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getETypedElementToUnitsMap()
		 * @generated
		 */
		EClass ETYPED_ELEMENT_TO_UNITS_MAP = eINSTANCE.getETypedElementToUnitsMap();

			/**
		 * The meta object literal for the '<em><b>Entries</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference ETYPED_ELEMENT_TO_UNITS_MAP__ENTRIES = eINSTANCE.getETypedElementToUnitsMap_Entries();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider <em>Units Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getUnitsProvider()
		 * @generated
		 */
		EClass UNITS_PROVIDER = eINSTANCE.getUnitsProvider();

			/**
		 * The meta object literal for the '<em><b>Get Provided Unit</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation UNITS_PROVIDER___GET_PROVIDED_UNIT__UNITSPROVIDERPARAMETERS = eINSTANCE.getUnitsProvider__GetProvidedUnit__UnitsProviderParameters();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters <em>Units Provider Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getUnitsProviderParameters()
		 * @generated
		 */
		EClass UNITS_PROVIDER_PARAMETERS = eINSTANCE.getUnitsProviderParameters();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleUnitsProviderImpl <em>Simple Units Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleUnitsProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getSimpleUnitsProvider()
		 * @generated
		 */
		EClass SIMPLE_UNITS_PROVIDER = eINSTANCE.getSimpleUnitsProvider();

			/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute SIMPLE_UNITS_PROVIDER__UNIT = eINSTANCE.getSimpleUnitsProvider_Unit();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersUnitsProviderImpl <em>EOperation EParameters Units Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersUnitsProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEOperationEParametersUnitsProvider()
		 * @generated
		 */
		EClass EOPERATION_EPARAMETERS_UNITS_PROVIDER = eINSTANCE.getEOperationEParametersUnitsProvider();

			/**
		 * The meta object literal for the '<em><b>Map</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference EOPERATION_EPARAMETERS_UNITS_PROVIDER__MAP = eINSTANCE.getEOperationEParametersUnitsProvider_Map();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersUnitsProviderParametersImpl <em>EOperation EParameters Units Provider Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersUnitsProviderParametersImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEOperationEParametersUnitsProviderParameters()
		 * @generated
		 */
		EClass EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS = eINSTANCE.getEOperationEParametersUnitsProviderParameters();

			/**
		 * The meta object literal for the '<em><b>Param</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS__PARAM = eINSTANCE.getEOperationEParametersUnitsProviderParameters_Param();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.DecimalFormatRegistryImpl <em>Decimal Format Registry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.DecimalFormatRegistryImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getDecimalFormatRegistry()
		 * @generated
		 */
		EClass DECIMAL_FORMAT_REGISTRY = eINSTANCE.getDecimalFormatRegistry();

			/**
		 * The meta object literal for the '<em><b>Entries Map</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP = eINSTANCE.getDecimalFormatRegistry_EntriesMap();

			/**
		 * The meta object literal for the '<em><b>Save</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation DECIMAL_FORMAT_REGISTRY___SAVE = eINSTANCE.getDecimalFormatRegistry__Save();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringMapImpl <em>ETyped Element To Format String Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringMapImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getETypedElementToFormatStringMap()
		 * @generated
		 */
		EClass ETYPED_ELEMENT_TO_FORMAT_STRING_MAP = eINSTANCE.getETypedElementToFormatStringMap();

			/**
		 * The meta object literal for the '<em><b>Entries</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference ETYPED_ELEMENT_TO_FORMAT_STRING_MAP__ENTRIES = eINSTANCE.getETypedElementToFormatStringMap_Entries();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringKeyValueImpl <em>ETyped Element To Format String Key Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringKeyValueImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getETypedElementToFormatStringKeyValue()
		 * @generated
		 */
		EClass ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE = eINSTANCE.getETypedElementToFormatStringKeyValue();

			/**
		 * The meta object literal for the '<em><b>Key</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE__KEY = eINSTANCE.getETypedElementToFormatStringKeyValue_Key();

			/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE__VALUE = eINSTANCE.getETypedElementToFormatStringKeyValue_Value();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider <em>Format Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getFormatProvider()
		 * @generated
		 */
		EClass FORMAT_PROVIDER = eINSTANCE.getFormatProvider();

			/**
		 * The meta object literal for the '<em><b>Get Provided Format</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EOperation FORMAT_PROVIDER___GET_PROVIDED_FORMAT__FORMATPROVIDERPARAMETERS = eINSTANCE.getFormatProvider__GetProvidedFormat__FormatProviderParameters();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters <em>Format Provider Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getFormatProviderParameters()
		 * @generated
		 */
		EClass FORMAT_PROVIDER_PARAMETERS = eINSTANCE.getFormatProviderParameters();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleFormatProviderImpl <em>Simple Format Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.SimpleFormatProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getSimpleFormatProvider()
		 * @generated
		 */
		EClass SIMPLE_FORMAT_PROVIDER = eINSTANCE.getSimpleFormatProvider();

			/**
		 * The meta object literal for the '<em><b>Format Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EAttribute SIMPLE_FORMAT_PROVIDER__FORMAT_PATTERN = eINSTANCE.getSimpleFormatProvider_FormatPattern();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderImpl <em>EOperation EParameters Format Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEOperationEParametersFormatProvider()
		 * @generated
		 */
		EClass EOPERATION_EPARAMETERS_FORMAT_PROVIDER = eINSTANCE.getEOperationEParametersFormatProvider();

			/**
		 * The meta object literal for the '<em><b>Map</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference EOPERATION_EPARAMETERS_FORMAT_PROVIDER__MAP = eINSTANCE.getEOperationEParametersFormatProvider_Map();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderParametersImpl <em>EOperation EParameters Format Provider Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.EOperationEParametersFormatProviderParametersImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEOperationEParametersFormatProviderParameters()
		 * @generated
		 */
		EClass EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS = eINSTANCE.getEOperationEParametersFormatProviderParameters();

			/**
		 * The meta object literal for the '<em><b>Param</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @generated
		 */
		EReference EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS__PARAM = eINSTANCE.getEOperationEParametersFormatProviderParameters_Param();

			/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.TreeFeatureNodeWizardPageProviderImpl <em>Tree Feature Node Wizard Page Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.TreeFeatureNodeWizardPageProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getTreeFeatureNodeWizardPageProvider()
		 * @generated
		 */
		EClass TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER = eINSTANCE.getTreeFeatureNodeWizardPageProvider();

			/**
		 * The meta object literal for the '<em>Hash Map</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.HashMap
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getHashMap()
		 * @generated
		 */
		EDataType HASH_MAP = eINSTANCE.getHashMap();

			/**
		 * The meta object literal for the '<em>Color</em>' data type.
		 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->		 * @see org.eclipse.swt.graphics.Color
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getColor()
		 * @generated
		 */
    EDataType COLOR = eINSTANCE.getColor();

    /**
		 * The meta object literal for the '<em>List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.List
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getList()
		 * @generated
		 */
		EDataType LIST = eINSTANCE.getList();

				/**
		 * The meta object literal for the '<em>Wizard Page</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.jface.wizard.WizardPage
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getWizardPage()
		 * @generated
		 */
		EDataType WIZARD_PAGE = eINSTANCE.getWizardPage();

				/**
		 * The meta object literal for the '<em>Map</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.Map
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getMap()
		 * @generated
		 */
		EDataType MAP = eINSTANCE.getMap();

				/**
		 * The meta object literal for the '<em>IWizard Page</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.jface.wizard.IWizardPage
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getIWizardPage()
		 * @generated
		 */
		EDataType IWIZARD_PAGE = eINSTANCE.getIWizardPage();

				/**
		 * The meta object literal for the '<em>Compound Command</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.emf.common.command.CompoundCommand
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getCompoundCommand()
		 * @generated
		 */
		EDataType COMPOUND_COMMAND = eINSTANCE.getCompoundCommand();

				/**
		 * The meta object literal for the '<em>Editing Domain</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.emf.edit.domain.EditingDomain
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getEditingDomain()
		 * @generated
		 */
		EDataType EDITING_DOMAIN = eINSTANCE.getEditingDomain();

				/**
		 * The meta object literal for the '<em>Resource Set</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.emf.ecore.resource.ResourceSet
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getResourceSet()
		 * @generated
		 */
		EDataType RESOURCE_SET = eINSTANCE.getResourceSet();

				/**
		 * The meta object literal for the '<em>MPart</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.e4.ui.model.application.ui.basic.MPart
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getMPart()
		 * @generated
		 */
		EDataType MPART = eINSTANCE.getMPart();

				/**
		 * The meta object literal for the '<em>ESelection Service</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.e4.ui.workbench.modeling.ESelectionService
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getESelectionService()
		 * @generated
		 */
		EDataType ESELECTION_SERVICE = eINSTANCE.getESelectionService();

				/**
		 * The meta object literal for the '<em>Image</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see org.eclipse.swt.graphics.Image
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getImage()
		 * @generated
		 */
		EDataType IMAGE = eINSTANCE.getImage();

				/**
		 * The meta object literal for the '<em>Unit Converter Map</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.TreeMap
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getUnitConverterMap()
		 * @generated
		 */
		EDataType UNIT_CONVERTER_MAP = eINSTANCE.getUnitConverterMap();

				/**
		 * The meta object literal for the '<em>Display Units Map</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.util.HashMap
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getDisplayUnitsMap()
		 * @generated
		 */
		EDataType DISPLAY_UNITS_MAP = eINSTANCE.getDisplayUnitsMap();

				/**
		 * The meta object literal for the '<em>Decimal Format</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->		 * @see java.text.DecimalFormat
		 * @see ca.gc.asc_csa.apogy.common.emf.ui.impl.ApogyCommonEMFUIPackageImpl#getDecimalFormat()
		 * @generated
		 */
		EDataType DECIMAL_FORMAT = eINSTANCE.getDecimalFormat();

  }

} //ApogyCommonEMFUIPackage
