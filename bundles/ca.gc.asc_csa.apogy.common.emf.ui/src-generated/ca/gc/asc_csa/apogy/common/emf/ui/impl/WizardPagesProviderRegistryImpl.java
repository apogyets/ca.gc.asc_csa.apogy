/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Wizard
 * Pages Provider Registry</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS POINT ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS ECLASS ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl#getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID <em>WIZARD PAGES PROVIDER CONTRIBUTORS PROVIDER ID</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.WizardPagesProviderRegistryImpl#getWizardPagesMap <em>Wizard Pages Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WizardPagesProviderRegistryImpl extends MinimalEObjectImpl.Container
		implements WizardPagesProviderRegistry {

	/**
	 * @generated_NOT
	 */
	private static WizardPagesProviderRegistry instance = null;

	/**
	 * @generated_NOT
	 */
	public static WizardPagesProviderRegistry getInstance() {
		if (instance == null) {
			instance = new WizardPagesProviderRegistryImpl();
		}
		return instance;
	}

	/**
	 * The default value of the
	 * '{@link #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID() <em>WIZARD
	 * PAGES PROVIDER CONTRIBUTORS POINT ID</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID()
	 * @generated
	 * @ordered
	 */
	protected static final String WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID_EDEFAULT = "ca.gc.asc_csa.apogy.common.emf.ui.wizardPagesProvider";

	/**
	 * The cached value of the
	 * '{@link #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID() <em>WIZARD
	 * PAGES PROVIDER CONTRIBUTORS POINT ID</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID()
	 * @generated
	 * @ordered
	 */
	protected String wizarD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID = WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID_EDEFAULT;

	/**
	 * The default value of the
	 * '{@link #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID() <em>WIZARD
	 * PAGES PROVIDER CONTRIBUTORS ECLASS ID</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID()
	 * @generated
	 * @ordered
	 */
	protected static final String WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID_EDEFAULT = "eClass";

	/**
	 * The cached value of the
	 * '{@link #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID() <em>WIZARD
	 * PAGES PROVIDER CONTRIBUTORS ECLASS ID</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID()
	 * @generated
	 * @ordered
	 */
	protected String wizarD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID = WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID_EDEFAULT;

	/**
	 * The default value of the
	 * '{@link #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID() <em>WIZARD
	 * PAGES PROVIDER CONTRIBUTORS PROVIDER ID</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID()
	 * @generated
	 * @ordered
	 */
	protected static final String WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID_EDEFAULT = "provider";

	/**
	 * The cached value of the
	 * '{@link #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID() <em>WIZARD
	 * PAGES PROVIDER CONTRIBUTORS PROVIDER ID</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID()
	 * @generated
	 * @ordered
	 */
	protected String wizarD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID = WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getWizardPagesMap() <em>Wizard Pages Map</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getWizardPagesMap()
	 * @generated
	 * @ordered
	 */
	protected Map<EClass, WizardPagesProvider> wizardPagesMap;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public WizardPagesProviderRegistryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.WIZARD_PAGES_PROVIDER_REGISTRY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID() {
		return wizarD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID() {
		return wizarD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID() {
		return wizarD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public Map<EClass, WizardPagesProvider> getWizardPagesMap() {
		Map<EClass, WizardPagesProvider> map = getWizardPagesMapGen();

		if (wizardPagesMap == null) {
			map = Collections.synchronizedMap(new HashMap<EClass, WizardPagesProvider>());

			IExtensionPoint extensionPoint = Platform.getExtensionRegistry()
					.getExtensionPoint(getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID());

			// Get all the extension points.
			IConfigurationElement[] contributors = extensionPoint.getConfigurationElements();

			for (int i = 0; i < contributors.length; i++) {
				IConfigurationElement contributor = contributors[i];

				String eClassStr = contributor.getAttribute(getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID());
				eClassStr = eClassStr.trim();
				String providerStr = contributor.getAttribute(getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID());
				providerStr = providerStr.trim();
				// Get the EClass and the WizardPagesProvider of the extension point.
				try 
				{
					EClass eClass = ApogyCommonEMFFacade.INSTANCE.getEClass(eClassStr);
					WizardPagesProvider wizardPagesProvider = (WizardPagesProvider) EcoreUtil.create(ApogyCommonEMFFacade.INSTANCE.getEClass(providerStr));
					
					if(eClass != null && wizardPagesProvider != null)
					{						
						map.put(eClass, wizardPagesProvider);
					}
					else
					{
						Logger.INSTANCE.log(Activator.ID, this, "WizardPagesProvider <"+ providerStr + "> does not define a valid EClass <" + eClassStr + "> !", EventSeverity.ERROR);
					}
				} 
				catch (Exception e) 
				{				
					Logger.INSTANCE.log(Activator.ID, this, "Failed to load contributed WizardPagesProvider <" + providerStr + "> for type <" + eClassStr + "> from <"
							+ contributor.getClass().getName() + ">", EventSeverity.ERROR, e);
				}
			}
			
			/*
			for(EClass eClass : map.keySet())
			{
				WizardPagesProvider wp = map.get(eClass);
				System.out.println(eClass + " -----> " + wp);
			}
			*/
			
			
			
			setWizardPagesMap(map);
		}
		return getWizardPagesMapGen();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Map<EClass, WizardPagesProvider> getWizardPagesMapGen() {
		return wizardPagesMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setWizardPagesMap(Map<EClass, WizardPagesProvider> newWizardPagesMap) {
		Map<EClass, WizardPagesProvider> oldWizardPagesMap = wizardPagesMap;
		wizardPagesMap = newWizardPagesMap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_MAP, oldWizardPagesMap, wizardPagesMap));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public WizardPagesProvider getProvider(EClass eClass) {		
		List<EClass> types = new ArrayList<EClass>();
		
		for (EClass type : getWizardPagesMap().keySet()) 
		{
			if(type != null)
			{
				if (type.isSuperTypeOf(eClass)) 
				{
					types.add(type);
				}
			}
			else
			{
				Logger.INSTANCE.log(Activator.ID, "Null type encountered in WizardPagesProvider.", EventSeverity.ERROR);
			}
		}

		if(types.isEmpty()){
			types.add(EcorePackage.Literals.EOBJECT);
		}
		EClass closestMatch = ApogyCommonEMFFacade.INSTANCE.findClosestMatch(eClass, types);

		return getWizardPagesMap().get(closestMatch);	
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID:
				return getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID();
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID:
				return getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID();
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID:
				return getWIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID();
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_MAP:
				return getWizardPagesMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_MAP:
				setWizardPagesMap((Map<EClass, WizardPagesProvider>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_MAP:
				setWizardPagesMap((Map<EClass, WizardPagesProvider>)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID:
				return WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID_EDEFAULT == null ? wizarD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID != null : !WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID_EDEFAULT.equals(wizarD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID);
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID:
				return WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID_EDEFAULT == null ? wizarD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID != null : !WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID_EDEFAULT.equals(wizarD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID);
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID:
				return WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID_EDEFAULT == null ? wizarD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID != null : !WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID_EDEFAULT.equals(wizarD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID);
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY__WIZARD_PAGES_MAP:
				return wizardPagesMap != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY___GET_PROVIDER__ECLASS:
				return getProvider((EClass)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (WIZARD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID: ");
		result.append(wizarD_PAGES_PROVIDER_CONTRIBUTORS_POINT_ID);
		result.append(", WIZARD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID: ");
		result.append(wizarD_PAGES_PROVIDER_CONTRIBUTORS_ECLASS_ID);
		result.append(", WIZARD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID: ");
		result.append(wizarD_PAGES_PROVIDER_CONTRIBUTORS_PROVIDER_ID);
		result.append(", wizardPagesMap: ");
		result.append(wizardPagesMap);
		result.append(')');
		return result.toString();
	}

} // WizardPagesProviderRegistryImpl
