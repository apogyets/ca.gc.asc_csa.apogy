/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap;
import ca.gc.asc_csa.apogy.common.emf.ui.preferences.PreferencesConstants;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decimal Format Registry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.DecimalFormatRegistryImpl#getEntriesMap <em>Entries Map</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DecimalFormatRegistryImpl extends MinimalEObjectImpl.Container implements DecimalFormatRegistry {
	
	/**
	 * @generated_NOT
	 */
	private static DecimalFormatRegistry instance = null;

	/**
	 * @generated_NOT
	 */
	public static DecimalFormatRegistry getInstance() {
		if (instance == null) {
			instance = new DecimalFormatRegistryImpl();
			String stringMap = Activator.getDefault().getPreferenceStore()
					.getString(PreferencesConstants.TYPED_ELEMENTS_FORMAT_ID);
			if (!"".equals(stringMap)) {
				EObject object = ApogyCommonEMFFacade.INSTANCE.deserializeString(stringMap,
						PreferencesConstants.TYPED_ELEMENTS_FORMAT_ID);
				if (object instanceof ETypedElementToFormatStringMap) {
					instance.setEntriesMap((ETypedElementToFormatStringMap) object);
				}
			}
		}

		return instance;
	}
	
	/**
	 * The cached value of the '{@link #getEntriesMap() <em>Entries Map</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntriesMap()
	 * @generated
	 * @ordered
	 */
	protected ETypedElementToFormatStringMap entriesMap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecimalFormatRegistryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.DECIMAL_FORMAT_REGISTRY;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public ETypedElementToFormatStringMap getEntriesMap() {
		ETypedElementToFormatStringMap map = getEntriesMapGen();
		
		if(map == null){
			map = ApogyCommonEMFUIFactory.eINSTANCE.createETypedElementToFormatStringMap();
			
			setEntriesMap(map);
		}
		
		return map;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElementToFormatStringMap getEntriesMapGen() {
		if (entriesMap != null && entriesMap.eIsProxy()) {
			InternalEObject oldEntriesMap = (InternalEObject)entriesMap;
			entriesMap = (ETypedElementToFormatStringMap)eResolveProxy(oldEntriesMap);
			if (entriesMap != oldEntriesMap) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP, oldEntriesMap, entriesMap));
			}
		}
		return entriesMap;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ETypedElementToFormatStringMap basicGetEntriesMap() {
		return entriesMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEntriesMap(ETypedElementToFormatStringMap newEntriesMap) {
		ETypedElementToFormatStringMap oldEntriesMap = entriesMap;
		entriesMap = newEntriesMap;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP, oldEntriesMap, entriesMap));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void save() {
		Activator.getDefault().getPreferenceStore().setValue(PreferencesConstants.TYPED_ELEMENTS_FORMAT_ID,
				ApogyCommonEMFFacade.INSTANCE.serializeEObject(getEntriesMap(),
						PreferencesConstants.TYPED_ELEMENTS_FORMAT_ID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP:
				if (resolve) return getEntriesMap();
				return basicGetEntriesMap();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP:
				setEntriesMap((ETypedElementToFormatStringMap)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP:
				setEntriesMap((ETypedElementToFormatStringMap)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY__ENTRIES_MAP:
				return entriesMap != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY___SAVE:
				save();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //DecimalFormatRegistryImpl
