/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui;

import org.eclipse.emf.ecore.EParameter;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>EOperation EParameters Units Provider Parameters</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters#getParam <em>Param</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getEOperationEParametersUnitsProviderParameters()
 * @model
 * @generated
 */
public interface EOperationEParametersUnitsProviderParameters extends UnitsProviderParameters {
	/**
	 * Returns the value of the '<em><b>Param</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Param</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Param</em>' reference.
	 * @see #setParam(EParameter)
	 * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage#getEOperationEParametersUnitsProviderParameters_Param()
	 * @model
	 * @generated
	 */
	EParameter getParam();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters#getParam <em>Param</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Param</em>' reference.
	 * @see #getParam()
	 * @generated
	 */
	void setParam(EParameter value);

} // EOperationEParametersUnitsProviderParameters
