package ca.gc.asc_csa.apogy.common.emf.ui.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Disposable;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.ui.*;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.DisplayUnitsRegistry;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToUnitsMap;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.NamedSetting;
import ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.SimpleUnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.UnitsProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProviderRegistry;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage
 * @generated
 */
public class ApogyCommonEMFUISwitch<T> extends Switch<T>
{
  /**
	 * The cached model package
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  protected static ApogyCommonEMFUIPackage modelPackage;

  /**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ApogyCommonEMFUISwitch()
  {
		if (modelPackage == null) {
			modelPackage = ApogyCommonEMFUIPackage.eINSTANCE;
		}
	}

  /**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
		return ePackage == modelPackage;
	}

  /**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
		switch (classifierID) {
			case ApogyCommonEMFUIPackage.APOGY_COMMON_EMFUI_FACADE: {
				ApogyCommonEMFUIFacade apogyCommonEMFUIFacade = (ApogyCommonEMFUIFacade)theEObject;
				T result = caseApogyCommonEMFUIFacade(apogyCommonEMFUIFacade);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE: {
				SelectionBasedTimeSource selectionBasedTimeSource = (SelectionBasedTimeSource)theEObject;
				T result = caseSelectionBasedTimeSource(selectionBasedTimeSource);
				if (result == null) result = caseTimeSource(selectionBasedTimeSource);
				if (result == null) result = caseNamed(selectionBasedTimeSource);
				if (result == null) result = caseDescribed(selectionBasedTimeSource);
				if (result == null) result = caseTimed(selectionBasedTimeSource);
				if (result == null) result = caseDisposable(selectionBasedTimeSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.ECLASS_SETTINGS: {
				EClassSettings eClassSettings = (EClassSettings)theEObject;
				T result = caseEClassSettings(eClassSettings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.MAP_BASED_ECLASS_SETTINGS: {
				MapBasedEClassSettings mapBasedEClassSettings = (MapBasedEClassSettings)theEObject;
				T result = caseMapBasedEClassSettings(mapBasedEClassSettings);
				if (result == null) result = caseEClassSettings(mapBasedEClassSettings);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER: {
				WizardPagesProvider wizardPagesProvider = (WizardPagesProvider)theEObject;
				T result = caseWizardPagesProvider(wizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_REGISTRY: {
				WizardPagesProviderRegistry wizardPagesProviderRegistry = (WizardPagesProviderRegistry)theEObject;
				T result = caseWizardPagesProviderRegistry(wizardPagesProviderRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER: {
				NamedDescribedWizardPagesProvider namedDescribedWizardPagesProvider = (NamedDescribedWizardPagesProvider)theEObject;
				T result = caseNamedDescribedWizardPagesProvider(namedDescribedWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(namedDescribedWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.NAMED_SETTING: {
				NamedSetting namedSetting = (NamedSetting)theEObject;
				T result = caseNamedSetting(namedSetting);
				if (result == null) result = caseEClassSettings(namedSetting);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.DISPLAY_UNITS_REGISTRY: {
				DisplayUnitsRegistry displayUnitsRegistry = (DisplayUnitsRegistry)theEObject;
				T result = caseDisplayUnitsRegistry(displayUnitsRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.ETYPED_ELEMENT_TO_UNITS_MAP: {
				ETypedElementToUnitsMap eTypedElementToUnitsMap = (ETypedElementToUnitsMap)theEObject;
				T result = caseETypedElementToUnitsMap(eTypedElementToUnitsMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.ETYPED_ELEMENT_TO_UNITS_KEY_VALUE: {
				@SuppressWarnings("unchecked") Map.Entry<ETypedElement, UnitsProvider> eTypedElementToUnitsKeyValue = (Map.Entry<ETypedElement, UnitsProvider>)theEObject;
				T result = caseETypedElementToUnitsKeyValue(eTypedElementToUnitsKeyValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.UNITS_PROVIDER: {
				UnitsProvider unitsProvider = (UnitsProvider)theEObject;
				T result = caseUnitsProvider(unitsProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.UNITS_PROVIDER_PARAMETERS: {
				UnitsProviderParameters unitsProviderParameters = (UnitsProviderParameters)theEObject;
				T result = caseUnitsProviderParameters(unitsProviderParameters);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.SIMPLE_UNITS_PROVIDER: {
				SimpleUnitsProvider simpleUnitsProvider = (SimpleUnitsProvider)theEObject;
				T result = caseSimpleUnitsProvider(simpleUnitsProvider);
				if (result == null) result = caseUnitsProvider(simpleUnitsProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_UNITS_PROVIDER: {
				EOperationEParametersUnitsProvider eOperationEParametersUnitsProvider = (EOperationEParametersUnitsProvider)theEObject;
				T result = caseEOperationEParametersUnitsProvider(eOperationEParametersUnitsProvider);
				if (result == null) result = caseUnitsProvider(eOperationEParametersUnitsProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_UNITS_PROVIDER_PARAMETERS: {
				EOperationEParametersUnitsProviderParameters eOperationEParametersUnitsProviderParameters = (EOperationEParametersUnitsProviderParameters)theEObject;
				T result = caseEOperationEParametersUnitsProviderParameters(eOperationEParametersUnitsProviderParameters);
				if (result == null) result = caseUnitsProviderParameters(eOperationEParametersUnitsProviderParameters);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.DECIMAL_FORMAT_REGISTRY: {
				DecimalFormatRegistry decimalFormatRegistry = (DecimalFormatRegistry)theEObject;
				T result = caseDecimalFormatRegistry(decimalFormatRegistry);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.ETYPED_ELEMENT_TO_FORMAT_STRING_MAP: {
				ETypedElementToFormatStringMap eTypedElementToFormatStringMap = (ETypedElementToFormatStringMap)theEObject;
				T result = caseETypedElementToFormatStringMap(eTypedElementToFormatStringMap);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.ETYPED_ELEMENT_TO_FORMAT_STRING_KEY_VALUE: {
				@SuppressWarnings("unchecked") Map.Entry<ETypedElement, FormatProvider> eTypedElementToFormatStringKeyValue = (Map.Entry<ETypedElement, FormatProvider>)theEObject;
				T result = caseETypedElementToFormatStringKeyValue(eTypedElementToFormatStringKeyValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.FORMAT_PROVIDER: {
				FormatProvider formatProvider = (FormatProvider)theEObject;
				T result = caseFormatProvider(formatProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.FORMAT_PROVIDER_PARAMETERS: {
				FormatProviderParameters formatProviderParameters = (FormatProviderParameters)theEObject;
				T result = caseFormatProviderParameters(formatProviderParameters);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.SIMPLE_FORMAT_PROVIDER: {
				SimpleFormatProvider simpleFormatProvider = (SimpleFormatProvider)theEObject;
				T result = caseSimpleFormatProvider(simpleFormatProvider);
				if (result == null) result = caseFormatProvider(simpleFormatProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER: {
				EOperationEParametersFormatProvider eOperationEParametersFormatProvider = (EOperationEParametersFormatProvider)theEObject;
				T result = caseEOperationEParametersFormatProvider(eOperationEParametersFormatProvider);
				if (result == null) result = caseFormatProvider(eOperationEParametersFormatProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.EOPERATION_EPARAMETERS_FORMAT_PROVIDER_PARAMETERS: {
				EOperationEParametersFormatProviderParameters eOperationEParametersFormatProviderParameters = (EOperationEParametersFormatProviderParameters)theEObject;
				T result = caseEOperationEParametersFormatProviderParameters(eOperationEParametersFormatProviderParameters);
				if (result == null) result = caseFormatProviderParameters(eOperationEParametersFormatProviderParameters);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonEMFUIPackage.TREE_FEATURE_NODE_WIZARD_PAGE_PROVIDER: {
				TreeFeatureNodeWizardPageProvider treeFeatureNodeWizardPageProvider = (TreeFeatureNodeWizardPageProvider)theEObject;
				T result = caseTreeFeatureNodeWizardPageProvider(treeFeatureNodeWizardPageProvider);
				if (result == null) result = caseWizardPagesProvider(treeFeatureNodeWizardPageProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

  /**
	 * Returns the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApogyCommonEMFUIFacade(ApogyCommonEMFUIFacade object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Selection Based Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Selection Based Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSelectionBasedTimeSource(SelectionBasedTimeSource object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EClass Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EClass Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEClassSettings(EClassSettings object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Map Based EClass Settings</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Map Based EClass Settings</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMapBasedEClassSettings(MapBasedEClassSettings object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProviderRegistry(WizardPagesProviderRegistry object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Described Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedDescribedWizardPagesProvider(NamedDescribedWizardPagesProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Setting</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Setting</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedSetting(NamedSetting object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Display Units Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Display Units Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDisplayUnitsRegistry(DisplayUnitsRegistry object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>ETyped Element To Units Key Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ETyped Element To Units Key Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseETypedElementToUnitsKeyValue(Map.Entry<ETypedElement, UnitsProvider> object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>ETyped Element To Units Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ETyped Element To Units Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseETypedElementToUnitsMap(ETypedElementToUnitsMap object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Units Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Units Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnitsProvider(UnitsProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Units Provider Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Units Provider Parameters</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnitsProviderParameters(UnitsProviderParameters object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Units Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Units Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleUnitsProvider(SimpleUnitsProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EOperation EParameters Units Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EOperation EParameters Units Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEOperationEParametersUnitsProvider(EOperationEParametersUnitsProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EOperation EParameters Units Provider Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EOperation EParameters Units Provider Parameters</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEOperationEParametersUnitsProviderParameters(EOperationEParametersUnitsProviderParameters object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Decimal Format Registry</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decimal Format Registry</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecimalFormatRegistry(DecimalFormatRegistry object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>ETyped Element To Format String Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ETyped Element To Format String Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseETypedElementToFormatStringMap(ETypedElementToFormatStringMap object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>ETyped Element To Format String Key Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ETyped Element To Format String Key Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseETypedElementToFormatStringKeyValue(Map.Entry<ETypedElement, FormatProvider> object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Format Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Format Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormatProvider(FormatProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Format Provider Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Format Provider Parameters</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFormatProviderParameters(FormatProviderParameters object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Format Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Format Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleFormatProvider(SimpleFormatProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EOperation EParameters Format Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EOperation EParameters Format Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEOperationEParametersFormatProvider(EOperationEParametersFormatProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EOperation EParameters Format Provider Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EOperation EParameters Format Provider Parameters</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEOperationEParametersFormatProviderParameters(EOperationEParametersFormatProviderParameters object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Tree Feature Node Wizard Page Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tree Feature Node Wizard Page Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTreeFeatureNodeWizardPageProvider(TreeFeatureNodeWizardPageProvider object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamed(Named object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Described</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Described</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDescribed(Described object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Timed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimed(Timed object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Disposable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Disposable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDisposable(Disposable object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>Time Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimeSource(TimeSource object) {
		return null;
	}

		/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
  @Override
  public T defaultCase(EObject object)
  {
		return null;
	}

} //ApogyCommonEMFUISwitch
