/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.impl;

import java.util.Date;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ui.PlatformUI;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.impl.TimeSourceImpl;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;
import ca.gc.asc_csa.apogy.common.emf.ui.SelectionBasedTimeSource;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Selection Based Time Source</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SelectionBasedTimeSourceImpl#getSelection <em>Selection</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.ui.impl.SelectionBasedTimeSourceImpl#getSelectionService <em>Selection Service</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SelectionBasedTimeSourceImpl extends TimeSourceImpl implements SelectionBasedTimeSource {
	
	/**
	 * The cached value of the '{@link #getSelection() <em>Selection</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelection()
	 * @generated
	 * @ordered
	 */
	protected Timed selection;
	
	/**
	 * @generated_NOT
	 */
	private Adapter timeAdapter;

	/**
	 * @generated_NOT
	 */
	private ISelectionListener selectionListener;
	
	/**
	 * The default value of the '{@link #getSelectionService() <em>Selection Service</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionService()
	 * @generated
	 * @ordered
	 */
	protected static final ESelectionService SELECTION_SERVICE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSelectionService() <em>Selection Service</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionService()
	 * @generated
	 * @ordered
	 */
	protected ESelectionService selectionService = SELECTION_SERVICE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SelectionBasedTimeSourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUIPackage.Literals.SELECTION_BASED_TIME_SOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timed getSelection() {
		if (selection != null && selection.eIsProxy()) {
			InternalEObject oldSelection = (InternalEObject)selection;
			selection = (Timed)eResolveProxy(oldSelection);
			if (selection != oldSelection) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION, oldSelection, selection));
			}
		}
		return selection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timed basicGetSelection() {
		return selection;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setSelection(Timed newSelection) {
		if(getSelection() != null){
			getSelection().eAdapters().remove(getAdapter());
		}
		setSelectionGen(newSelection);
		if(getSelection() != null)
		{			
			// Update the time source time.
			updateTime(getSelection().getTime());			
		}
		else
		{
			updateTime(null);
		}
		
		if(getSelection() != null){
			getSelection().eAdapters().add(getAdapter());
		}
	}
	
	@Override
	public Date getTime() {
		if (getSelectionService() == null) {
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCommonEMFUIPackage.Literals.SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE,
					PlatformUI.getWorkbench().getService(ESelectionService.class));
		}
		return super.getTime();
	}

	/**
	 * @generated_NOT
	 */
	private Adapter getAdapter(){
		if(timeAdapter == null){
			timeAdapter = new AdapterImpl(){
				@Override
				public void notifyChanged(Notification msg) {
					if(msg.getFeature() == ApogyCommonEMFPackage.Literals.TIMED__TIME){
						if(msg.getNewValue() != null){
							updateTime((Date)msg.getNewValue());
						}else{
							updateTime(null);
						}
					}
				}
			};
		}
		return timeAdapter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionGen(Timed newSelection) {
		Timed oldSelection = selection;
		selection = newSelection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION, oldSelection, selection));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ESelectionService getSelectionService() {
		return selectionService;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setSelectionService(ESelectionService newSelectionService) {
		if(selectionService != null){
			selectionService.removeSelectionListener(getSelectionListener());
		}
	
		if(selectionService != newSelectionService){
			setSelectionServiceGen(newSelectionService);
			getSelectionService().addSelectionListener(getSelectionListener());
		}		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionServiceGen(ESelectionService newSelectionService) {
		ESelectionService oldSelectionService = selectionService;
		selectionService = newSelectionService;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE, oldSelectionService, selectionService));
	}
	
	/**
	 * @generated_NOT
	 */
	private ISelectionListener getSelectionListener() {
		if (selectionListener == null) {
			selectionListener = new ISelectionListener() {
				public void selectionChanged(MPart part, Object selection) {
					if (selection instanceof Timed) {
						if (ApogyCommonTransactionFacade.INSTANCE
								.getTransactionalEditingDomain(SelectionBasedTimeSourceImpl.this) != null) {
							ApogyCommonTransactionFacade.INSTANCE.basicSet(SelectionBasedTimeSourceImpl.this,
									ApogyCommonEMFUIPackage.Literals.SELECTION_BASED_TIME_SOURCE__SELECTION,
									(Timed) selection);
						} else {
							setSelection((Timed) selection);
						}
					}
				}
			};
		}
		return selectionListener;
	}
	 
	
	/**
	 * @generated_NOT
	 */
	@Override
	public void dispose() {
		if(selectionService != null){
			selectionService.removeSelectionListener(getSelectionListener());
		}
		
		if(getSelection() != null){
			getSelection().eAdapters().remove(getAdapter());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION:
				if (resolve) return getSelection();
				return basicGetSelection();
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE:
				return getSelectionService();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION:
				setSelection((Timed)newValue);
				return;
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE:
				setSelectionService((ESelectionService)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION:
				setSelection((Timed)null);
				return;
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE:
				setSelectionService(SELECTION_SERVICE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION:
				return selection != null;
			case ApogyCommonEMFUIPackage.SELECTION_BASED_TIME_SOURCE__SELECTION_SERVICE:
				return SELECTION_SERVICE_EDEFAULT == null ? selectionService != null : !SELECTION_SERVICE_EDEFAULT.equals(selectionService);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (selectionService: ");
		result.append(selectionService);
		result.append(')');
		return result.toString();
	}

} //SelectionBasedTimeSourceImpl
