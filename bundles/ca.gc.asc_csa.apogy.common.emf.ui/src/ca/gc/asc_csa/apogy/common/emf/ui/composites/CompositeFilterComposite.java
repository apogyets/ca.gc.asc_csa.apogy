package ca.gc.asc_csa.apogy.common.emf.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.CompositeFilter;
import ca.gc.asc_csa.apogy.common.emf.CompositeFilterType;
import ca.gc.asc_csa.apogy.common.emf.IFilter;
import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.NewChildWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;


public class CompositeFilterComposite<T> extends Composite 
{
	protected CompositeFilter<T> compositeFilter;
		
	private CompositeFilterTypeComboComposite compositeFilterTypeComboComposite;
	private Tree tree;
	private TreeViewer treeViewer;
	private Button btnNew;
	private Button btnDelete;	
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	private Composite topComposite;
	
	public CompositeFilterComposite(Composite parent, int style) 
	{
		this(parent, style, null);
	}
	
	public CompositeFilterComposite(Composite parent, int style, CompositeFilterComposite<T> compositeFilter) 
	{
		super(parent, style);
		
		setLayout(new GridLayout(2, false));
		
		topComposite = new Composite(this, SWT.NONE);
		topComposite.setLayout(new GridLayout(2, false));
		topComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		Label lblFilterBehaviour = new Label(topComposite, SWT.NONE);
		lblFilterBehaviour.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblFilterBehaviour.setAlignment(SWT.RIGHT);
		lblFilterBehaviour.setText("Filter Behaviour:");
		
		compositeFilterTypeComboComposite = new CompositeFilterTypeComboComposite(topComposite, SWT.NONE)
		{
			@Override
			protected void newSelection(CompositeFilterType compositeFilterType) 
			{
				if(getCompositeFilter() != null)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet(getCompositeFilter(), ApogyCommonEMFPackage.Literals.COMPOSITE_FILTER__FILTER_CHAIN_TYPE, compositeFilterType);
					// getCompositeFilter().setFilterChainType(compositeFilterType);
				}
			}
		};
		compositeFilterTypeComboComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		if(getCompositeFilter() != null)
		{
			compositeFilterTypeComboComposite.setSelectedCompositeFilterType(getCompositeFilter().getFilterChainType());
		}
		
		new Label(this, SWT.NONE);
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		ColumnViewerToolTipSupport.enableFor(treeViewer);
		
		tree = treeViewer.getTree();
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tree.setLinesVisible(true);
		
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{			
			@SuppressWarnings("unchecked")
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				newSelection((IFilter<T>)((IStructuredSelection) event.getSelection()).getFirstElement());				
			}
		});
		
		
		// Buttons on right side.
		Composite rightComposite = new Composite(this, SWT.NONE);
		rightComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		rightComposite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(rightComposite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() {
			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{
					CompositeFilter<T> parentCompositeFilter = getCompositeFilter();
					
					// Adding to a selected Composite Filter.
					if( ((IStructuredSelection) treeViewer.getSelection()).getFirstElement() instanceof CompositeFilter)
					{
						parentCompositeFilter = (CompositeFilter)((IStructuredSelection) treeViewer.getSelection()).getFirstElement();
					}
										
					// If the composite filter is not in an Editing Domain, adds it to a temporary one.
					EditingDomain domain = AdapterFactoryEditingDomain.getEditingDomainFor(parentCompositeFilter);
					if(domain == null)
					{
						ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(parentCompositeFilter);
					}
					
					/**
					 * Creates and opens the wizard to create a new child
					 */
					NewChildWizard newChildWizard = new NewChildWizard(ApogyCommonEMFPackage.Literals.COMPOSITE_FILTER__FILTERS, parentCompositeFilter);
										
					// Listener that sets the new child as the selected item
					newChildWizard.getCreatedChild().addChangeListener(new IChangeListener() 
					{						
						@Override
						public void handleChange(ChangeEvent event) 
						{
							EObject selected = ((WritableValue<EObject>)event.getObservable()).getValue();
																			
							if(!treeViewer.isBusy())
							{
								treeViewer.setSelection(new StructuredSelection(selected));
								treeViewer.refresh();
							}							
						}
					});
					WizardDialog dialog = new WizardDialog(getShell(), newChildWizard);

					dialog.open();					
				}
			}
		});
				
		btnDelete = new Button(rightComposite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@SuppressWarnings("unchecked")
			@Override
			public void widgetSelected(SelectionEvent e) 
			{
				String toolsToDeleteMessage = "";

				List<IFilter<T>> selectedFilters = getSelectedFilters();
				Iterator<IFilter<T>> filters = selectedFilters.iterator();
				while (filters.hasNext()) 
				{
					IFilter<T> filter = filters.next();
					toolsToDeleteMessage = toolsToDeleteMessage + filter.getName();

					if (filters.hasNext()) 
					{
						toolsToDeleteMessage = toolsToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected filters", null,
						"Are you sure to delete these filters: " + toolsToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (IFilter<T> filter : selectedFilters) 
					{												
						try 
						{
							CompositeFilter<T> parent = CompositeFilterComposite.this.compositeFilter;
							if(filter.eContainer() instanceof CompositeFilter)
							{
								parent = (CompositeFilter<T>) filter.eContainer();
							}
							
							EditingDomain domain = AdapterFactoryEditingDomain.getEditingDomainFor(parent);
							if(domain instanceof TransactionalEditingDomain)
							{
								// Removes the tool from the list.
								ApogyCommonTransactionFacade.INSTANCE.basicRemove(parent, ApogyCommonEMFPackage.Literals.COMPOSITE_FILTER__FILTERS, filter);										
							}
							else
							{
								getCompositeFilter().getFilters().remove(filter);
							}							
						} 
						catch (Exception ex)	
						{
							getCompositeFilter().getFilters().remove(filter);
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the tool <"+ filter.getName() + ">",
									EventSeverity.ERROR, ex);
						}
					}
					
					// Forces updates of the viewer.
					treeViewer.setInput(getCompositeFilter());
					
				}
			}
		});
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}		
	
	public CompositeFilter<T> getCompositeFilter() {
		return compositeFilter;
	}

	public void setCompositeFilter(CompositeFilter<T> compositeFilter)
	{
		this.compositeFilter = compositeFilter;
	
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		if(compositeFilter != null)
		{
			m_bindingContext = customInitDataBindings();
			
			compositeFilterTypeComboComposite.setSelectedCompositeFilterType(compositeFilter.getFilterChainType());
			
			treeViewer.setInput(compositeFilter);						
		}				
	}

	@SuppressWarnings("unchecked")
	public List<IFilter<T>> getSelectedFilters()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	protected void newSelection(IFilter<T> filter)
	{
		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	
	private class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@SuppressWarnings("rawtypes")
		@Override
		public Object[] getElements(Object inputElement) 
		{					
			Object[] elements = null;				
			if(inputElement instanceof CompositeFilter)
			{								
				return ((CompositeFilter) inputElement).getFilters().toArray();
			}			
					
			return elements;
		}

		@SuppressWarnings("unchecked")
		@Override
		public Object[] getChildren(Object parentElement) 
		{
			Object[] children = null;			
			if(parentElement instanceof CompositeFilter)
			{
				CompositeFilter<T> compositeFilter = (CompositeFilter<T>) parentElement;
				children = compositeFilter.getFilters().toArray();
			}
			return children;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof CompositeFilter)
			{
				CompositeFilter<T> compositeFilter = (CompositeFilter<T>) element;
				return !compositeFilter.getFilters().isEmpty();
			}		
			else
			{
				return false;
			}
		}	
	}
}
