package ca.gc.asc_csa.apogy.common.emf.ui.handlers;

import java.util.List;

import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartImpl;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBar;
import org.eclipse.e4.ui.model.application.ui.menu.impl.HandledToolItemImpl;

import ca.gc.asc_csa.apogy.common.emf.ui.parts.ToolBarMenuPart;
import ca.gc.asc_csa.apogy.common.ui.ApogyCommonUIRCPConstants;

@SuppressWarnings("restriction")
public class OpenToolBarPartHandler {

	boolean initNeeded = true;

	@CanExecute
	public boolean canExecute(MPart part, MUIElement element) {
		if (element == part && element instanceof PartImpl
				&& ((PartImpl) part).getObject() instanceof ToolBarMenuPart) {
			return true;
		}

		boolean canExecute = false;

		if (element instanceof HandledToolItemImpl && ((HandledToolItemImpl) element).getCommand().getElementId()
				.equals(ApogyCommonUIRCPConstants.COMMAND__OPEN_TOOL_BAR_MENU__ID) && part instanceof PartImpl) {
			PartImpl impl = (PartImpl) part;
			if (impl.getObject() instanceof ToolBarMenuPart) {
				canExecute = ((ToolBarMenuPart) impl.getObject()).canExecute();
			}
		}

		if (canExecute && initNeeded) {
			((ToolBarMenuPart) ((PartImpl) part).getObject())
					.init((MToolBar) (MElementContainer<?>) element.getParent());
			initNeeded = false;
		}

		if (!canExecute && !initNeeded) {
			initNeeded = true;
		}

		return canExecute;
	}

	@Execute
	public void execute(MUIElement element, MPart toolBarMenuPart,
			@Named(ApogyCommonUIRCPConstants.COMMAND_PARAMETER__OPEN_TOOL_BAR_MENU_ID__ID) String parameterID) {
		if (element instanceof HandledToolItemImpl) {
			HandledToolItemImpl toolItem = (HandledToolItemImpl) element;
			/** Select the new selected */
			((ToolBarMenuPart) ((PartImpl) toolBarMenuPart).getObject()).toolItemSelectedChanged(parameterID,
					toolItem.isSelected());
			List<MUIElement> toolbarElements = toolItem.getParent().getChildren();
			/** Deselect the other elements */
			for (MUIElement toolbarElement : toolbarElements) {
				if (toolbarElement instanceof HandledToolItemImpl
						&& ((HandledToolItemImpl) toolbarElement).getCommand().getElementId()
								.equals(ApogyCommonUIRCPConstants.COMMAND__OPEN_TOOL_BAR_MENU__ID)
						&& toolbarElement != element) {
					((HandledToolItemImpl) toolbarElement).setSelected(false);
				}
			}
		} else {
			for (MUIElement toolbarElement : toolBarMenuPart.getToolbar().getChildren()) {
				if (parameterID != null && toolbarElement instanceof HandledToolItemImpl
						&& ((HandledToolItemImpl) toolbarElement).getCommand().getElementId()
								.equals(ApogyCommonUIRCPConstants.COMMAND__OPEN_TOOL_BAR_MENU__ID)) {
					HandledToolItemImpl handledToolItem = (HandledToolItemImpl) toolbarElement;
					
					if (parameterID.equals(handledToolItem.getParameters().get(0).getValue())) {
						handledToolItem.setSelected(true);
					} else {
						handledToolItem.setSelected(false);
					}
				}
			}
		}
	}
}