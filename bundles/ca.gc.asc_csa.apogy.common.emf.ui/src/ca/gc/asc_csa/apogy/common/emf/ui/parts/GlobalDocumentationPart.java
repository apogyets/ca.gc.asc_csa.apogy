package ca.gc.asc_csa.apogy.common.emf.ui.parts;

import java.util.HashMap;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectDocumentationComposite;

public class GlobalDocumentationPart extends AbstractEObjectSelectionPart{

	private ISelectionListener selectionListener;
	
	
	@Override
	protected EObject getInitializeObject() {
		selectionService.addSelectionListener(getSelectionListener());
		Object obj = selectionService.getSelection();
		
		if(obj instanceof EObject){
			return (EObject)obj;
		}
		return null;
	}
	
	@Override
	protected void setCompositeContents(EObject eObject) {
		((EObjectDocumentationComposite)getActualComposite()).setEObject(eObject);
	}

	@Override
	protected void createContentComposite(Composite parent, int style) {
		new EObjectDocumentationComposite(parent, SWT.None);
	}
	
	@Override
	public void userPreDestroy(MPart mPart) {
		selectionService.removeSelectionListener(getSelectionListener());
	}	
	
	private ISelectionListener getSelectionListener() {
		if(selectionListener == null){
			selectionListener = new ISelectionListener() {
					@Override
					public void selectionChanged(MPart part, Object selection) {
						if (selection == null || selection instanceof EObject) {
							setEObject((EObject) selection);
						}
					}
				};
		}
		return selectionListener;
	}

	@Override
	protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners() {
		return null;
	}

}
