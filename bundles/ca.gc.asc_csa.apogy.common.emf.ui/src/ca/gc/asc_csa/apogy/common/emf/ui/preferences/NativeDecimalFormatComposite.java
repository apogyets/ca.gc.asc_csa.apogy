package ca.gc.asc_csa.apogy.common.emf.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateListStrategy;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import ca.gc.asc_csa.apogy.common.emf.ui.Activator;

public class NativeDecimalFormatComposite extends Composite {

	private DataBindingContext bindingContext;

	private Text doubleText;
	private Text floatText;
	private Text byteText;
	private Text shortText;
	private Text intText;
	private Text longText;

	private List<Text> textList;

	public NativeDecimalFormatComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (bindingContext != null) {
					bindingContext.dispose();
				}
			}
		});

		textList = new ArrayList<Text>();

		/** Synthax information button */
		Button infoButton = new Button(this, SWT.None);
		infoButton.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, true, 2, 1));
		ImageDescriptor image = AbstractUIPlugin.imageDescriptorFromPlugin("org.eclipse.jface",
				"/icons/full/message_info.png");
		infoButton.setImage(image.createImage());
		infoButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String message = "0\trepresents a digit\n" + "#\trepresents a digit, zero shows as absent\n"
						+ ".\trepresents a placeholder for decimal separator\n"
						+ ",\trepresents a placeholder for grouping separator\n\n" + "Examples with input 123123.123:\n"
						+ "\t- 000000000.000000 : " + new DecimalFormat("000000000.000000").format(123123.123)
						+ "\n\t- #########.###### : " + new DecimalFormat("#########.######").format(123123.123)
						+ "\n\t- ###,###.### : " + new DecimalFormat("###,###.###").format(123123.123);
				MessageDialog.openInformation(getShell(), "Synthax", message);
			}
		});

		/** Double */
		Label doubleLabel = new Label(this, SWT.None);
		doubleLabel.setText("Double float :");
		doubleLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		doubleText = new Text(this, SWT.BORDER);
		doubleText.setText(" ");
		doubleText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		textList.add(doubleText);

		/** Float */
		Label floatLabel = new Label(this, SWT.None);
		floatLabel.setText("Floating point :");
		floatLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		floatText = new Text(this, SWT.BORDER);
		floatText.setText(" ");
		floatText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		textList.add(floatText);

		/** Byte */
		Label byteLabel = new Label(this, SWT.None);
		byteLabel.setText("Byte :");
		byteLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		byteText = new Text(this, SWT.BORDER);
		byteText.setText(" ");
		byteText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		textList.add(byteText);

		/** Short */
		Label shortLabel = new Label(this, SWT.None);
		shortLabel.setText("Short integer :");
		shortLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		shortText = new Text(this, SWT.BORDER);
		shortText.setText(" ");
		shortText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		textList.add(shortText);

		/** Int */
		Label intLabel = new Label(this, SWT.None);
		intLabel.setText("Integer :");
		intLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		intText = new Text(this, SWT.BORDER);
		intText.setText(" ");
		intText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		textList.add(intText);

		/** Long */
		Label longLabel = new Label(this, SWT.None);
		longLabel.setText("Long integer :");
		longLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));

		longText = new Text(this, SWT.BORDER);
		longText.setText(" ");
		longText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		textList.add(longText);

		updateTexts();
		initDataBindings();
	}

	/**
	 * Updates all the texts.
	 */
	public void updateTexts() {
		doubleText.setText(
				Activator.getDefault().getPreferenceStore().getString(PreferencesConstants.NATIVE_FORMAT_DOUBLE_ID));
		floatText.setText(
				Activator.getDefault().getPreferenceStore().getString(PreferencesConstants.NATIVE_FORMAT_FLOAT_ID));
		byteText.setText(
				Activator.getDefault().getPreferenceStore().getString(PreferencesConstants.NATIVE_FORMAT_BYTE_ID));
		shortText.setText(
				Activator.getDefault().getPreferenceStore().getString(PreferencesConstants.NATIVE_FORMAT_SHORT_ID));
		intText.setText(
				Activator.getDefault().getPreferenceStore().getString(PreferencesConstants.NATIVE_FORMAT_INT_ID));
		longText.setText(
				Activator.getDefault().getPreferenceStore().getString(PreferencesConstants.NATIVE_FORMAT_LONG_ID));
	}

	private void initDataBindings() {

		if (bindingContext != null) {
			bindingContext.dispose();
		}

		bindingContext = new DataBindingContext();

		/** Update value strategies */
		UpdateValueStrategy policyNever = new UpdateValueStrategy(UpdateListStrategy.POLICY_NEVER);
		UpdateValueStrategy validFormatUpdateStrategy = new UpdateValueStrategy(UpdateListStrategy.POLICY_UPDATE)
				.setConverter(new Converter(String.class, Color.class) {
					@Override
					public Object convert(Object fromObject) {
						if (fromObject != null && isValid((String) fromObject)) {
							return getDisplay().getSystemColor(SWT.COLOR_TRANSPARENT);
						}
						return getDisplay().getSystemColor(SWT.COLOR_RED);
					}
				});

		/** Bind the text with the background for each text */
		for (Text text : textList) {
			IObservableValue<?> textText = WidgetProperties.text(SWT.Modify).observe(text);
			IObservableValue<?> backgroundText = WidgetProperties.background().observe(text);

			bindingContext.bindValue(backgroundText, textText, policyNever, validFormatUpdateStrategy);
		}
	}

	/**
	 * Used to save the content of the texts boxes in the preferences if their
	 * input is valid.
	 */
	public void savePreferences() {
		if (isValid(doubleText.getText())) {
			Activator.getDefault().getPreferenceStore().setValue(PreferencesConstants.NATIVE_FORMAT_DOUBLE_ID,
					doubleText.getText());
		}
		if (isValid(floatText.getText())) {
			Activator.getDefault().getPreferenceStore().setValue(PreferencesConstants.NATIVE_FORMAT_FLOAT_ID,
					floatText.getText());
		}
		if (isValid(byteText.getText())) {
			Activator.getDefault().getPreferenceStore().setValue(PreferencesConstants.NATIVE_FORMAT_BYTE_ID,
					byteText.getText());
		}
		if (isValid(shortText.getText())) {
			Activator.getDefault().getPreferenceStore().setValue(PreferencesConstants.NATIVE_FORMAT_SHORT_ID,
					shortText.getText());
		}
		if (isValid(intText.getText())) {
			Activator.getDefault().getPreferenceStore().setValue(PreferencesConstants.NATIVE_FORMAT_INT_ID,
					intText.getText());
		}
		if (isValid(longText.getText())) {
			Activator.getDefault().getPreferenceStore().setValue(PreferencesConstants.NATIVE_FORMAT_LONG_ID,
					longText.getText());
		}
	}

	/**
	 * Tries to create a {@link DecimalFormat} with the specified
	 * {@link String}.
	 * 
	 * @param str
	 *            {@link String} to use as pattern for the created
	 *            {@link DecimalFormat}.
	 * @return true if a {@link DecimalFormat} can be created, false otherwise.
	 */
	private boolean isValid(String str) {
		if("".equals(str)){
			return false;
		}
		
		for (int i = 0; i < str.length(); i++) {
			char character = str.charAt(i);			
			if (character != '0' && character != '.' && character != '#' && character != ',') {
				return false;
			}
		}

		try {
			new DecimalFormat(str);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
