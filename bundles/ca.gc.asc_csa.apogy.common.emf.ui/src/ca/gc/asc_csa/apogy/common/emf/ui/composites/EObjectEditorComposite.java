package ca.gc.asc_csa.apogy.common.emf.ui.composites;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.llarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.NewChildWizard;

public class EObjectEditorComposite extends Composite {

	private ISelectionChangedListener selectionChangedListener;
	private EObjectComposite eObjectComposite;

	private Button btnNew;

	ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	/**
	 * Creates the parentComposite.
	 * 
	 * @param parent
	 * @param style
	 */
	public EObjectEditorComposite(Composite parent, int style) {
		super(parent, style);
		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				dispose();
			}
		});
		setLayout(new GridLayout(2, false));

		eObjectComposite = new EObjectComposite(this, SWT.None) {
			@Override
			protected void newSelection(ISelection selection) {
				EObjectEditorComposite.this.newSelection(selection);
				checkEnableNewButton(getSelectedEObject());
			}

			@Override
			protected AdapterFactoryLabelProvider getLabelProvider() {
				return EObjectEditorComposite.this.getLabelProvider();
			}

			@Override
			protected AdapterFactoryContentProvider getContentProvider() {
				return EObjectEditorComposite.this.getContentProvider();
			}
		};
		eObjectComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 3));

		btnNew = new Button(this, SWT.NONE);
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnNew.setText("New");
		btnNew.addListener(SWT.Selection, getBtnNewListener());

		Button btnDelete = new Button(this, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnDelete.setText("Delete");
		btnDelete.addListener(SWT.Selection, getBtnDeleteListener());
	}

	protected void newSelection(ISelection selection) {
		checkEnableNewButton((EObject) ((TreeSelection) selection).getFirstElement());
	}
	
	protected void refresh(){
		eObjectComposite.refresh();
	}

	@Override
	public void dispose() {
		super.dispose();
		if (selectionChangedListener != null) {
			eObjectComposite.removeListener(SWT.Selection, (Listener) selectionChangedListener);
		}
	}

	private void checkEnableNewButton(EObject eObject) {
		if (eObject != null) {
			btnNew.setEnabled(!ApogyCommonEMFFacade.INSTANCE.getSettableEReferences(eObject).isEmpty());
		} else {
			btnNew.setEnabled(false);
		}

	}

	/**
	 * Sets the root object for the parentComposite
	 * 
	 * @param eObject
	 *            The root eObject
	 */
	public void setEObject(EObject eObject) {
		eObjectComposite.setEObject(eObject);
		eObjectComposite.setSelectedEObject(eObject);
		checkEnableNewButton(eObject);
	}

	public EObject getSelectedEObject() {
		return eObjectComposite.getSelectedEObject();
	}

	protected AdapterFactoryContentProvider getContentProvider() {
		return new AdapterFactoryContentProvider(adapterFactory);
	}

	protected AdapterFactoryLabelProvider getLabelProvider() {
		return new AdapterFactoryLabelProvider(adapterFactory);
	}

	protected Listener getBtnNewListener() {
		return new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (event.type == SWT.Selection) {
					/**
					 * Creates and opens the wizard to create a new child
					 */
					NewChildWizard newChildWizard = new NewChildWizard(
							ApogyCommonEMFFacade.INSTANCE.getSettableEReferences(eObjectComposite.getSelectedEObject()),
							eObjectComposite.getSelectedEObject());
					// Listener that sets the new child as the selected item
					newChildWizard.getCreatedChild().addChangeListener(new IChangeListener() {
						@SuppressWarnings("unchecked")
						@Override
						public void handleChange(ChangeEvent event) {
							eObjectComposite
									.setSelectedEObject(((WritableValue<EObject>) event.getObservable()).getValue());
						}
					});
					WizardDialog dialog = new WizardDialog(getShell(), newChildWizard);

					dialog.open();
				}
			}
		};
	}

	protected Listener getBtnDeleteListener() {
		return new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (event.type == SWT.Selection) {
					// Get the editing domain of the object
					EditingDomain editingDomain = AdapterFactoryEditingDomain
							.getEditingDomainFor(eObjectComposite.getSelectedEObject());

					Command command = null;
					if (editingDomain != null) {
						// If the containing feature is a list, the object is
						// removed from the list
						if (eObjectComposite.getSelectedEObject().eContainingFeature() != null) {
							if (eObjectComposite.getSelectedEObject().eContainingFeature().isMany()) {
								command = new RemoveCommand(editingDomain,
										(EList<?>) eObjectComposite.getSelectedEObject().eContainer()
												.eGet(eObjectComposite.getSelectedEObject().eContainingFeature()),
										eObjectComposite.getSelectedEObject());
							}
							// Otherwise, if the feature is not a list, the
							// EStructuralFeature of the parent is set to null
							else {
								command = new SetCommand(editingDomain,
										eObjectComposite.getSelectedEObject().eContainer(),
										eObjectComposite.getSelectedEObject().eContainingFeature(), null);
							}
						}
					}
					editingDomain.getCommandStack().execute(command);
				}

			}
		};
	}
}