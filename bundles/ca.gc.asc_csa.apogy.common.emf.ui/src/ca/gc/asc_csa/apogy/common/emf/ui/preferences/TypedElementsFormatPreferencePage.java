package ca.gc.asc_csa.apogy.common.emf.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ca.gc.asc_csa.apogy.common.emf.ui.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.DecimalFormatRegistry;

public class TypedElementsFormatPreferencePage extends PreferencePage implements IWorkbenchPreferencePage {
	private DecimalFormatsMapComposite composite;
	private Group structuralFeaturesUnitsGroup;

	/**
	 * Create the preference page.
	 */
	public TypedElementsFormatPreferencePage() {
	}

	/**
	 * Create contents of the preference page.
	 * 
	 * @param parent
	 */
	@Override
	public Control createContents(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		container.setLayout(new GridLayout(1, false));

		structuralFeaturesUnitsGroup = new Group(container, SWT.None);
		structuralFeaturesUnitsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		structuralFeaturesUnitsGroup.setLayout(new FillLayout());
		structuralFeaturesUnitsGroup.setText("Typed elements format display");

		composite = new DecimalFormatsMapComposite(structuralFeaturesUnitsGroup, SWT.None);
		composite.setMap(EcoreUtil.copy(DecimalFormatRegistry.INSTANCE.getEntriesMap()));

		return container;
	}

	/**
	 * Initialize the preference page.
	 */
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	public boolean performOk() {
		storePreferences();
		return super.performOk();
	}

	@Override
	protected void performApply() {
		storePreferences();
		super.performApply();
	}

	@Override
	protected void performDefaults() {
		DecimalFormatRegistry.INSTANCE.getEntriesMap().getEntries().clear();
		composite.setMap(EcoreUtil.copy(DecimalFormatRegistry.INSTANCE.getEntriesMap()));
		super.performDefaults();
	}

	private void storePreferences() {
		DecimalFormatRegistry.INSTANCE.setEntriesMap(composite.getMap());
		DecimalFormatRegistry.INSTANCE.save();
	}
}
