package ca.gc.asc_csa.apogy.common.emf.ui.parts;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PreDestroy;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.workbench.modeling.ISelectionListener;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;

abstract public class AbstractEObjectSelectionPart extends AbstractPart {

	protected EObject eObject;	
	
	/**
	 * Listener that sets the new selection in the part if it's an
	 * {@link EObject} or null.
	 */
	protected final ISelectionListener DEFAULT_LISTENER = new ISelectionListener() {
		@Override
		public void selectionChanged(MPart part, Object selection) {
			if (selection == null || selection instanceof EObject) {
				setEObject((EObject) selection);
			}
		}
	};

	@Override
	protected EObject getInitializeObject() {
		HashMap<String, ISelectionListener> map = getSelectionProvidersIdsToSelectionListeners();
		if(map != null){
			Iterator<ISelectionListener> iteListeners = map.values().iterator();
			List<Object> selectedObjects = new ArrayList<>();
			
			for (Iterator<String> iteIds = map.keySet().iterator(); iteIds
					.hasNext();) {
				String id = verifyID(iteIds.next());
				selectionService.addSelectionListener(id, iteListeners.next());
				selectedObjects.add(selectionService.getSelection(id));
			}
			
			for(Object object : selectedObjects){
				if(object == selectionService.getSelection()){
					eObject = (EObject) object;
				}
			}
			if(eObject == null){
				for(Object object : selectedObjects){
					if(object != null){
						eObject = (EObject) object;
						break;
					}
				}
			}
		}
		return eObject;
	}
	
	/**
	 * Verifies that if the ids have been made unique by adding a number at the end, that the
	 * listener is put on the corresponding MPart id.
	 * 
	 * @param id
	 */
	private String verifyID(String id) {
		Pattern idNumberEndPattern = Pattern.compile("[^0-9]+([0-9]+)$");
		Matcher matcher = idNumberEndPattern
				.matcher(mPart.getElementId().substring(0, mPart.getElementId().indexOf("_" + perspectiveID)));
		if (matcher.find()) {
			String endNumber = matcher.group(1);
			if (!id.endsWith(endNumber)) {
				id = id.concat("_" + endNumber);
			}
		}
		id = id.concat("_" + perspectiveID);
		return id;
	}

	@Override
	protected final void setCompositeContent(EObject eObject) {
		this.eObject = eObject;
		setCompositeContents(eObject);
	}

	/**
	 * Sets the {@link EObject} to display in the content composite.
	 */
	abstract protected void setCompositeContents(EObject eObject);

	@Override
	protected void createNoContentComposite(Composite parent, int style) {
		new NoContentComposite(parent, style) {
			@Override
			protected String getMessage() {
				return "No compatible selection";
			}
		};
	}

	/**
	 * Specifies the {@link Composite} to create in the part.
	 */
	@Override
	abstract protected void createContentComposite(Composite parent, int style);

	/**
	 * This method returns a map of the IDs of the parts that the part listens
	 * as key. The value of each key needs to be the listener that reacts to the
	 * new selection. {@link AbstractEObjectSelectionPart} has a default
	 * implementation of this listener called DEFAULT_LISTENER.
	 * 
	 * @return {@link HashMap} of {@link String} to {@link ISelectionListener}
	 */
	abstract protected HashMap<String, ISelectionListener> getSelectionProvidersIdsToSelectionListeners();
	
	@PreDestroy
	public void dispose(){
		HashMap<String, ISelectionListener> map = getSelectionProvidersIdsToSelectionListeners();
		if(map != null){
			for(String id : map.keySet()){
				String uniqueID = verifyID(id);
				selectionService.removeSelectionListener(uniqueID, map.get(id));
			}
		}
	}
}