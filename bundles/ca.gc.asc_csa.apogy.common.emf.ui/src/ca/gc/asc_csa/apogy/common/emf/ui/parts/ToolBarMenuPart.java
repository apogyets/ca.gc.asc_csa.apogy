package ca.gc.asc_csa.apogy.common.emf.ui.parts;

import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBar;
import org.eclipse.e4.ui.model.application.ui.menu.impl.HandledToolItemImpl;

import ca.gc.asc_csa.apogy.common.ui.ApogyCommonUIRCPConstants;

/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

@SuppressWarnings("restriction")
public interface ToolBarMenuPart {

	/**
	 * Is notified when a menu item is selected.
	 * 
	 * @param parameterID
	 *            ID specified in the handledToolItem's parameters.
	 * @param selected
	 *            value of selected of the ToolItem.
	 */
	void toolItemSelectedChanged(String parameterID, boolean selected);

	/**
	 * Specifies if a toolItemSelectedChanged can be called.
	 */
	boolean canExecute();

	/**
	 * Initializes the part's content if needed. This can be used for a
	 * persistedState.
	 * 
	 * @param toolBar
	 *            The toolBar of the part.
	 */
	default void init(MToolBar toolBar) {
		for (MUIElement toolbarElement : toolBar.getChildren()) {
			if (toolbarElement instanceof HandledToolItemImpl && ((HandledToolItemImpl) toolbarElement).getCommand()
					.getElementId().equals(ApogyCommonUIRCPConstants.COMMAND__OPEN_TOOL_BAR_MENU__ID)) {
				((HandledToolItemImpl) toolbarElement).setSelected(false);
			}
		}
	};
}
