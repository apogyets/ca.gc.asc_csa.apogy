package ca.gc.asc_csa.apogy.common.emf.ui.composites;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;

public class EClassSelectionComposite extends Composite 
{
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private EClass superType;
	private EClass selectedEclass;
	
	private ComboViewer comboViewer;;
	
	public EClassSelectionComposite(Composite parent, int style) 
	{
		this(parent, style, null);		
	}

	/**
	 * 
	 * @param parent The composite parent.
	 * @param style The composite style.
	 * @param superType Super type for which all sub classes are to be show. Null shows all available EClasses.
	 */
	public EClassSelectionComposite(Composite parent, int style, EClass superType) 
	{
		super(parent, style);
		setLayout(new FillLayout());
		
		this.superType = superType;
		
		comboViewer = createCombo(this, SWT.READ_ONLY);		
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{		
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{	
				if(event.getSelection().isEmpty())
				{
					selectedEclass = null;
					newEClassSelected(selectedEclass);
					comboViewer.getCombo().setToolTipText("");
				}
				else if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					if(iStructuredSelection.getFirstElement() instanceof EClass)
					{
						selectedEclass = (EClass)iStructuredSelection.getFirstElement();
						comboViewer.getCombo().setToolTipText(selectedEclass.getInstanceClass().getName());
						newEClassSelected(selectedEclass);
					}
					else
					{
						selectedEclass = null;
						comboViewer.getCombo().setToolTipText("");
						newEClassSelected(selectedEclass);
					}
				}
			}
		});	
	}
	
	public ComboViewer getComboViewer() {
		return comboViewer;
	}

	public EClass getSelectedEclass() {
		return selectedEclass;
	}

	public void select(EClass eClass)
	{
		if(comboViewer != null && !comboViewer.getCombo().isDisposed())
		{
			if(eClass != null)
			{
				comboViewer.setSelection(new StructuredSelection(eClass), true);
			}
			else
			{
				comboViewer.setSelection(new StructuredSelection(), true);
			}
		}
	}
	
	private ComboViewer createCombo(Composite parent, int style)
	{
		ComboViewer comboViewer = new ComboViewer(parent, SWT.DROP_DOWN);		
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
				
		// Display the combo element sorted by displayed name.
		comboViewer.setComparator(new ViewerComparator()
		{
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) 
			{
				EClass eClass1 = (EClass) e1;
				EClass eClass2 = (EClass) e2;
				
				String name1 = eClass1.getName();
				String name2 = eClass2.getName();
								
				return name1.compareTo(name2);				
			}
		});
		
		if(superType != null)
		{
			comboViewer.setInput(ApogyCommonEMFFacade.INSTANCE.getAllSubEClasses(superType));
		}
		else
		{
			comboViewer.setInput(ApogyCommonEMFFacade.INSTANCE.getAllAvailableEClasses());
		}
				
		return comboViewer;
	}	
	
	protected void newEClassSelected(EClass newEclass)
	{		
	}
}
