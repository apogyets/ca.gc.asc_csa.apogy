package ca.gc.asc_csa.apogy.common.emf.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.Map.Entry;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.ISWTObservableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.IViewerObservableValue;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.ETypedElementToFormatStringMap;
import ca.gc.asc_csa.apogy.common.emf.ui.FormatProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.ETypedElementToFormatStringKeyValueImpl;

public class DecimalFormatsMapComposite extends Composite {

	private DataBindingContext dataBindingContext;

	private AdapterFactory adapterFactory = new ComposedAdapterFactory(
			ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	private TreeViewer treeViewer;
	private Button deleteButton;

	private ETypedElementToFormatStringMap map;

	public DecimalFormatsMapComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));
		addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (dataBindingContext != null) {
					dataBindingContext.dispose();
				}
			}
		});

		/** Tree viewer */
		treeViewer = new TreeViewer(this, SWT.BORDER);
		Tree tree = treeViewer.getTree();
		GridData gd_treeViewer = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2);
		gd_treeViewer.heightHint = 1;
		tree.setLayoutData(gd_treeViewer);
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);

		TreeViewerColumn treeViewerTypedElementColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn treeclmnTypedElement = treeViewerTypedElementColumn.getColumn();
		treeclmnTypedElement.setWidth(150);
		treeclmnTypedElement.setText("Typed element");

		TreeViewerColumn treeViewerProviderColumn = new TreeViewerColumn(treeViewer, SWT.NONE);
		TreeColumn treeclmnProvider = treeViewerProviderColumn.getColumn();
		treeclmnProvider.setWidth(150);
		treeclmnProvider.setText("Format");

		treeViewer.setLabelProvider(new FormatProvidersLabelProvider(adapterFactory));
		treeViewer.setContentProvider(new FormatProvidersContentProvider(adapterFactory));

		/** Button */
		deleteButton = new Button(this, SWT.None);
		deleteButton.setText("Delete");
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		deleteButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Object obj = treeViewer.getStructuredSelection().getFirstElement();
				
				if(obj instanceof ETypedElementToFormatStringKeyValueImpl){
					ETypedElementToFormatStringKeyValueImpl entry = (ETypedElementToFormatStringKeyValueImpl) treeViewer
							.getStructuredSelection().getFirstElement();
					map.getEntries().removeKey(entry.getKey());
				}else if(obj instanceof EParameter){
					EParameter param = (EParameter)obj;
					EOperationEParametersFormatProvider provider = (EOperationEParametersFormatProvider) map.getEntries().get(param.getEOperation());
					provider.getMap().getEntries().removeKey(param);
				}
				treeViewer.refresh();
			}
		});

		initDataBindings();

	}

	private void initDataBindings() {
		if (dataBindingContext != null) {
			dataBindingContext.dispose();
		}

		dataBindingContext = new DataBindingContext();

		/** TreViewer selection observable */
		IViewerObservableValue treeViewerSelectionObservableValue = ViewerProperties.singleSelection()
				.observe(treeViewer);
		/** Enable button observable */
		ISWTObservableValue deleteButtonEnableObservableValue = WidgetProperties.enabled().observe(deleteButton);

		dataBindingContext.bindValue(deleteButtonEnableObservableValue, treeViewerSelectionObservableValue,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, boolean.class) {

							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));

	}

	/**
	 * Sets the map that is displayed in the composite
	 * 
	 * @param map
	 */
	public void setMap(ETypedElementToFormatStringMap map) {

		this.map = map;

		treeViewer.setInput(map);
	}

	/**
	 * Gets the modified map.
	 */
	public ETypedElementToFormatStringMap getMap() {
		return map;
	}

	/**
	 * Label provider for the tree viewer.
	 */
	// TODO add other FormatProvider types if new are created.
	private class FormatProvidersLabelProvider extends AdapterFactoryLabelProvider {

		public FormatProvidersLabelProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		private static final int TYPED_ELEMENT_COLUMN_ID = 0;
		private static final int FORMAT_COLUMN_ID = 1;

		@Override
		public String getColumnText(Object object, int columnIndex) 
		{
			if (object instanceof Entry) 
			{
				@SuppressWarnings("unchecked")
				Entry<ETypedElement, FormatProvider> entry = (Entry<ETypedElement, FormatProvider>) object;
				switch (columnIndex) {
				case TYPED_ELEMENT_COLUMN_ID:
					
					//Return the fully qualified name of the type element.																							
					return getString(entry.getKey());
				case FORMAT_COLUMN_ID:
					/** FormatProvider itemProvider .toString() */
					return entry.getValue() instanceof EOperationEParametersFormatProvider ? ""
							: super.getColumnText(entry.getValue(), columnIndex);
				default:
				}
			}

			if (object instanceof EParameter) {
				EParameter param = (EParameter) object;
				switch (columnIndex) {
				case TYPED_ELEMENT_COLUMN_ID:
					/** Typed element name */
					return param.getName();
				case FORMAT_COLUMN_ID:
					EOperationEParametersFormatProviderParameters params = ApogyCommonEMFUIFactory.eINSTANCE
							.createEOperationEParametersFormatProviderParameters();
					params.setParam(param);
					return ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(param.getEOperation(), params).toPattern();
				default:
				}
			}
			return super.getColumnText(object, columnIndex);
		}

		@Override
		public Image getColumnImage(Object object, int columnIndex) {
			return null;
		}
		
		private String getString(final ETypedElement element)
		{
			EObject currentETypedElement = element;
			String string = null;
			
			while(currentETypedElement != null)
			{				
				if(currentETypedElement instanceof EStructuralFeature)
				{
					EStructuralFeature eStructuralFeature = (EStructuralFeature) currentETypedElement;
					
					if(string != null) string = eStructuralFeature.getName() + "." + string;
					else string = eStructuralFeature.getName();
											
					currentETypedElement = eStructuralFeature.eContainer();
				}
				else if(currentETypedElement instanceof EAttribute)
				{
					EAttribute eAttribute = (EAttribute) currentETypedElement;
					if(string != null)  string = eAttribute.getName() + "." + string;
					else string = eAttribute.getName();
					
					currentETypedElement = eAttribute.eContainer();
				}
				else if(currentETypedElement instanceof EClass)
				{
					EClass eClass = (EClass) currentETypedElement;
					
					if(string != null) string = eClass.getName() + "." + string;
					else string = eClass.getName();
					
					currentETypedElement = eClass.eContainer();
				}
				else
				{
					currentETypedElement = null;
				}
			}
			return string;
		}
	}

	/**
	 * Content provider for the native formats list tree.
	 */
	// TODO add other FormatProvider types if new are created.
	private class FormatProvidersContentProvider extends AdapterFactoryContentProvider {

		public FormatProvidersContentProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public Object[] getChildren(Object object) {
			if (object instanceof ETypedElementToFormatStringKeyValueImpl) {
				ETypedElementToFormatStringKeyValueImpl entry = (ETypedElementToFormatStringKeyValueImpl) object;

				return ((EOperationEParametersFormatProvider) entry.getValue()).getMap().getEntries().keySet()
						.toArray();
			}
			return null;
		}

		@Override
		public boolean hasChildren(Object object) {
			if (object instanceof ETypedElementToFormatStringKeyValueImpl) {
				ETypedElementToFormatStringKeyValueImpl entry = (ETypedElementToFormatStringKeyValueImpl) object;

				return entry.getValue() instanceof EOperationEParametersFormatProvider;
			}
			return false;
		}

	}

}
