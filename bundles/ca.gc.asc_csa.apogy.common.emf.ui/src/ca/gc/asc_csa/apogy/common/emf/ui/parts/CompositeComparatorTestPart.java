package ca.gc.asc_csa.apogy.common.emf.ui.parts;

import javax.annotation.PostConstruct;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.CompositeComparator;
import ca.gc.asc_csa.apogy.common.emf.EComparator;
import ca.gc.asc_csa.apogy.common.emf.EIdComparator;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.TimedComparator;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.ComparatorDetailsComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.CompositeComparatorComposite;

public class CompositeComparatorTestPart 
{
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());

	private CompositeComparatorComposite<Timed> compositeFilterComposite;
	private ComparatorDetailsComposite<Timed> filterDetailsComposite;
	
	@PostConstruct
	protected void createContentComposite(Composite parent) 
	{				
		parent.setLayout(new FillLayout());
		
		Composite top = new Composite(parent, SWT.BORDER);
		GridLayout gl_parent = new GridLayout(2, false);
		gl_parent.verticalSpacing = 10;
		top.setLayout(gl_parent);
		
		// Tools section on the left.
		Section sctnTools = formToolkit.createSection(top, Section.TITLE_BAR);
		sctnTools.setForeground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND));
		sctnTools.setFont(SWTResourceManager.getFont("Ubuntu", 18, SWT.BOLD));
		sctnTools.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(sctnTools);
		sctnTools.setText("Comparators");
				
		compositeFilterComposite = new CompositeComparatorComposite<Timed>(sctnTools, SWT.NONE)
		{
			@Override
			protected void newSelection(EComparator<Timed> comparator) {
				filterDetailsComposite.setComparator(comparator);
			}
		};
		compositeFilterComposite.setCompositeComparator(createComparator());		
		
		GridLayout gridLayout = (GridLayout) compositeFilterComposite.getLayout();
		gridLayout.marginWidth = 0;
		formToolkit.adapt(compositeFilterComposite);
		formToolkit.paintBordersFor(compositeFilterComposite);
		sctnTools.setClient(compositeFilterComposite);
				
		// Tools section on the left.
		Section sctnToolsDetails = formToolkit.createSection(top, Section.TITLE_BAR);
		sctnToolsDetails.setForeground(SWTResourceManager.getColor(SWT.COLOR_TITLE_BACKGROUND));
		sctnToolsDetails.setFont(SWTResourceManager.getFont("Ubuntu", 18, SWT.BOLD));
		sctnToolsDetails.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(sctnToolsDetails);
		sctnToolsDetails.setText("Details");
				
		filterDetailsComposite = new ComparatorDetailsComposite<Timed>(sctnToolsDetails, SWT.NONE);
		GridLayout gridLayout1 = (GridLayout) filterDetailsComposite.getLayout();
		gridLayout1.marginWidth = 0;
		formToolkit.adapt(filterDetailsComposite);
		formToolkit.paintBordersFor(filterDetailsComposite);
		sctnToolsDetails.setClient(filterDetailsComposite);
	}
	
	private CompositeComparator<Timed> createComparator()
	{
		CompositeComparator<Timed> comparator = ApogyCommonEMFFactory.eINSTANCE.createCompositeComparator();
		
		TimedComparator<Timed> comparator1 = ApogyCommonEMFFactory.eINSTANCE.createTimedComparator();
		EIdComparator<Timed> afterFilter = ApogyCommonEMFFactory.eINSTANCE.createEIdComparator();
		
		comparator.getComparators().add(comparator1);
		comparator.getComparators().add(afterFilter);
		
		return comparator;
	}
}
