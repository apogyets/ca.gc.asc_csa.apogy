package ca.gc.asc_csa.apogy.common.emf.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.swt.graphics.RGB;

public class PreferencesConstants 
{
	public static final String INSTANCE_TYPE_NAME_FILTER_ID = "INSTANCE_TYPE_NAME_FILTER_ID";
	public static final String DEFAULT_INSTANCE_TYPE_NAME_FILTER = "";
		
	public static final String INSTANCE_TYPE_CLASS_FILTER_ID = "INSTANCE_TYPE_CLASS_FILTER_ID";
	public static final String DEFAULT_INSTANCE_TYPE_CLASS_FILTER = "";
		
	
	/**
	 * Range colors
	 */
	public static final RGB DEFAULT_UNKNOWN_COLOR = null;
	public static final RGB DEFAULT_NOMINAL_COLOR = null;
	public static final RGB DEFAULT_WARNING_COLOR = new RGB(255, 255, 0);
	public static final RGB DEFAULT_ALARM_COLOR = new RGB(255, 0, 0);
	public static final RGB DEFAULT_OUT_OF_RANGE_COLOR = new RGB(0,0,255);
		
	/**
	 * Display units 
	 */
	/** Natives */
	public static final String NATIVE_TO_DISPLAY_UNITS_ID = "NATIVE_TO_DISPLAY_UNITS_ID";
	public static final String DEFAULT_NATIVE_TO_DISPLAY_UNITS = "rad=°,rad/s=°/s, rev/s=°/s";
	
	/** By typed elements */
	public static final String TYPED_ELEMENTS_UNITS_ID = "TYPED_ELEMENTS_UNITS_ID";
	public static final String DEFAULT_TYPED_ELEMENTS_UNITS = "";
	
	
	/**
	 * Number formats
	 */
	/** Natives */
	public static final String NATIVE_FORMAT_DOUBLE_ID = "NATIVE_FORMAT_DOUBLE_ID";
	public static final String NATIVE_FORMAT_FLOAT_ID = "NATIVE_FORMAT_FLOAT_ID";
	public static final String NATIVE_FORMAT_BYTE_ID = "NATIVE_FORMAT_BYTE_ID";
	public static final String NATIVE_FORMAT_SHORT_ID = "NATIVE_FORMAT_SHORT_ID";
	public static final String NATIVE_FORMAT_INT_ID = "NATIVE_FORMAT_INT_ID";
	public static final String NATIVE_FORMAT_LONG_ID = "NATIVE_FORMAT_LONG_ID";
	/** Default values */
	public static final String DEFAULT_NATIVE_FORMAT_BYTE = "0.###";
	public static final String DEFAULT_NATIVE_FORMAT_SHORT = "0.###";
	public static final String DEFAULT_NATIVE_FORMAT_INT = "0.###";
	public static final String DEFAULT_NATIVE_FORMAT_LONG = "0.###";
	public static final String DEFAULT_NATIVE_FORMAT_DOUBLE = "0.0########";
	public static final String DEFAULT_NATIVE_FORMAT_FLOAT = "0.0#####";

	/** By typed elements */
	public static final String TYPED_ELEMENTS_FORMAT_ID = "TYPED_ELEMENTS_FORMAT_ID";
	public static final String DEFAULT_TYPED_ELEMENTS_FORMAT = "";

	/** Verifies if the {@link String} matches with a format preference ID. */
	public static boolean isFormatPreference(String ID) {
		if (ID.equals(PreferencesConstants.NATIVE_FORMAT_BYTE_ID)
				|| ID.equals(PreferencesConstants.NATIVE_FORMAT_DOUBLE_ID)
				|| ID.equals(PreferencesConstants.NATIVE_FORMAT_FLOAT_ID)
				|| ID.equals(PreferencesConstants.NATIVE_FORMAT_INT_ID)
				|| ID.equals(PreferencesConstants.NATIVE_FORMAT_LONG_ID)
				|| ID.equals(PreferencesConstants.NATIVE_FORMAT_SHORT_ID)
				|| ID.equals(PreferencesConstants.TYPED_ELEMENTS_FORMAT_ID)) {
			return true;
		}
		return false;
	}
	
}
