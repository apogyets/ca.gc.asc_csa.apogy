package ca.gc.asc_csa.apogy.common.emf.ui.wizards;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureSpecifier;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureTreeNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractRootNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.TreeFeatureNode;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;


public class TreeFeatureNodeWizardPage extends WizardPage
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.emf.ui.wizards.TreeFeatureNodeWizardPage";
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private TreeFeatureNode treeFeatureNode;
	private AbstractFeatureTreeNode parent;
	
	private Tree tree;
	private TreeViewer treeViewer;
	
	private Text txtFeatureName;
	private Text txtFeatureDescription;
	private Text txtFeatureType;
	
	public TreeFeatureNodeWizardPage(TreeFeatureNode treeFeatureNode, AbstractFeatureTreeNode parent)
	{
		super(WIZARD_PAGE_ID);
		setTitle("New Feature");
		setDescription("Select the feature.");
						
		this.treeFeatureNode = treeFeatureNode;
		this.parent = parent;
	}

	@Override
	public void createControl(Composite arg0) 
	{		
		Composite container = new Composite(arg0, SWT.NONE);
		container.setLayout(new GridLayout(2,false));
				
		if(!hasSubNodes(parent))
		{
			Label lblMessage = new Label(container, SWT.NONE);
			GridData gd_lblMessage = new GridData(SWT.FILL, SWT.TOP, true, false);
			lblMessage.setLayoutData(gd_lblMessage);
			
			lblMessage.setText("There are no sub-feature available !");
			setErrorMessage("There are no sub-feature available !");
			setPageComplete(false);
		}
		else
		{
			Composite leftComposite = new Composite(container, SWT.NONE);
			GridData gd_leftComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
			leftComposite.setLayoutData(gd_leftComposite);
			leftComposite.setLayout(new GridLayout(1, false));
		
			Label blbFeatures = new Label(leftComposite, SWT.NONE);
			blbFeatures.setText("Available Features");
			
			treeViewer = new TreeViewer(leftComposite, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
			tree = treeViewer.getTree();
			GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 3);
			gd_tree.widthHint = 200;
			gd_tree.minimumWidth = 200;
			tree.setLayoutData(gd_tree);
			tree.setLinesVisible(true);
			
			treeViewer.setContentProvider(new CustomContentProvider());
			treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
			treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
			{						
				@Override
				public void selectionChanged(SelectionChangedEvent event) 
				{
					if(treeViewer.getSelection() instanceof StructuredSelection)
					{
						StructuredSelection structuredSelection= (StructuredSelection) treeViewer.getSelection();
						EStructuralFeature eStructuralFeature = (EStructuralFeature) structuredSelection.getFirstElement();
						
						ApogyCommonTransactionFacade.INSTANCE.basicSet(treeFeatureNode, ApogyCommonEMFPackage.Literals.ABSTRACT_FEATURE_SPECIFIER__STRUCTURAL_FEATURE, eStructuralFeature, true);
						
						txtFeatureName.setText(eStructuralFeature.getName());
						txtFeatureDescription.setText(ApogyCommonEMFFacade.INSTANCE.getFullDescription(eStructuralFeature));
						txtFeatureType.setText(eStructuralFeature.getEType().getInstanceTypeName());
						txtFeatureType.setToolTipText(eStructuralFeature.getEType().getInstanceTypeName());
					}							
				}
			});
			
			treeViewer.setInput(parent);
	
			Composite rightComposite = new Composite(container, SWT.NONE);
			GridData gd_rightComposite = new GridData(SWT.FILL, SWT.FILL, true, false);
			rightComposite.setLayoutData(gd_rightComposite);
			rightComposite.setLayout(new GridLayout(2, false));
			
			Label lblFeatureDetails = new Label(rightComposite, SWT.NONE);
			lblFeatureDetails.setText("Feature Details");		
			GridData gd_lblFeatureDetails = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
			lblFeatureDetails.setLayoutData(gd_lblFeatureDetails);
			
			// Feature Name
			Label lblFeatureName = new Label(rightComposite, SWT.NONE);
			lblFeatureName.setText("Name : ");		
			GridData gd_lblFeatureName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
			lblFeatureName.setLayoutData(gd_lblFeatureName);
			
			txtFeatureName = new Text(rightComposite, SWT.BORDER);
			txtFeatureName.setEditable(false);
			txtFeatureName.setText("");
			GridData gd_txtFeatureName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
			txtFeatureName.setLayoutData(gd_txtFeatureName);
			
			// Feature Description
			Label lblFeatureDescription = new Label(rightComposite, SWT.NONE);
			lblFeatureDescription.setText("Description : ");		
			GridData gd_lblFeatureDescription = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
			lblFeatureDescription.setLayoutData(gd_lblFeatureDescription);
			
			txtFeatureDescription = new Text(rightComposite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
			txtFeatureDescription.setEditable(false);
			txtFeatureDescription.setText("");
			GridData gd_txtFeatureDescription = new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1);
			gd_txtFeatureDescription.minimumHeight = 100;
			gd_txtFeatureDescription.heightHint = 100;
			txtFeatureDescription.setLayoutData(gd_txtFeatureDescription);
			
			// Feature Type
			Label lblFeatureType = new Label(rightComposite, SWT.NONE);
			lblFeatureType.setText("Type : ");		
			GridData gd_lblFeatureType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
			lblFeatureType.setLayoutData(gd_lblFeatureType);
	
			txtFeatureType = new Text(rightComposite, SWT.BORDER);
			txtFeatureType.setEditable(false);
			txtFeatureType.setText("");		
			GridData gd_txtFeatureType = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
			txtFeatureType.setLayoutData(gd_txtFeatureType);
		}
		
		setControl(container);
	}
		
	private boolean hasSubNodes(AbstractFeatureTreeNode parent)
	{
		if(parent instanceof AbstractRootNode)
		{
			AbstractRootNode abstractRootNode = (AbstractRootNode) parent;
			if(abstractRootNode.getSourceClass() == null)
			{
				return false;
			}
			else
			{
				return (abstractRootNode.getSourceClass().getEAllStructuralFeatures().size() > 0);
			}
		}
		else if(parent instanceof AbstractFeatureSpecifier)
		{								
			AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) parent;	
			if(abstractFeatureSpecifier.getStructuralFeature() != null)
			{
				EStructuralFeature eStructuralFeature = abstractFeatureSpecifier.getStructuralFeature();
				if(eStructuralFeature.getEType() instanceof EClass)
				{
					EClass eClass = (EClass) eStructuralFeature.getEType();
					return eClass.getEAllStructuralFeatures().size() > 0;
				}
				else
				{
					return false;
				}
			}

		}
		return false;
	}
	

	private class CustomContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{							
			if(inputElement instanceof AbstractRootNode)
			{
				AbstractRootNode abstractRootNode = (AbstractRootNode) inputElement;
				if(abstractRootNode.getSourceClass() != null)
				{
					return abstractRootNode.getSourceClass().getEAllStructuralFeatures().toArray();
				}
				else
				{
					return null;
				}
			}
			else if(inputElement instanceof AbstractFeatureSpecifier)
			{								
				AbstractFeatureSpecifier abstractFeatureSpecifier = (AbstractFeatureSpecifier) inputElement;	
				if(abstractFeatureSpecifier.getStructuralFeature() != null)
				{
					EStructuralFeature eStructuralFeature = abstractFeatureSpecifier.getStructuralFeature();
					if(eStructuralFeature.getEType() instanceof EClass)
					{
						EClass eClass = (EClass) eStructuralFeature.getEType();
						return eClass.getEAllStructuralFeatures().toArray();
					}
					else
					{
						return null;
					}
				}									
				return null;
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{	
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			return false;
		}
	}
}
