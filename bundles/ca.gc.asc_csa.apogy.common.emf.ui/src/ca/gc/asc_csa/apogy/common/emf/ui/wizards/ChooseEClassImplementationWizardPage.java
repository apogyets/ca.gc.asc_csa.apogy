package ca.gc.asc_csa.apogy.common.emf.ui.wizards;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectListComposite;

public class ChooseEClassImplementationWizardPage extends WizardPage {

	private final static String DESCRPTION_DEFAULT = "None available.";
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.emf.ui.wizards.ChooseEClassWizardPage";

	private EObjectListComposite eClassesListComposite;
	
	private List<EClass> eClasses;
	private Text txtDescriptiontext;

	/**
	 * Constructor for the WizardPage.
	 * 
	 * @param pageName
	 * @wbp.parser.constructor
	 */
	public ChooseEClassImplementationWizardPage() {
		super(WIZARD_PAGE_ID);
		setTitle("New Child");
		setDescription("Select the new child's reference and type.");
	}

	public ChooseEClassImplementationWizardPage(EClass superClass) {
		this(ApogyCommonEMFFacade.INSTANCE.getAllSubEClasses(superClass));	
	}
	
	public ChooseEClassImplementationWizardPage(List<EClass> eClasses) {
		this();
		
		SortedSet<EClass> sortedClasses = ApogyCommonEMFFacade.INSTANCE.sortAlphabetically(eClasses);
		
		this.eClasses = new ArrayList<EClass>();
		this.eClasses.addAll(sortedClasses);	
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, false));

		eClassesListComposite = new EObjectListComposite(container, SWT.None) {
			@Override
			protected void newSelection(TreeSelection selection) 
			{
				ChooseEClassImplementationWizardPage.this.newSelection(selection);
				ChooseEClassImplementationWizardPage.this.validate();
			}

			@Override
			protected StyledCellLabelProvider getLabelProvider() {
				return new EClassLabelProvider();
			}
		};
		
		EList<EObject> eObjectsEClassList = new BasicEList<EObject>();
		eObjectsEClassList.addAll(eClasses);

		eClassesListComposite.setEObjectsList(eObjectsEClassList);
		eClassesListComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));

		setControl(container);
		
		Label lblDescription = new Label(container, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		txtDescriptiontext = new Text(container, SWT.BORDER | SWT.READ_ONLY | SWT.WRAP | SWT.MULTI);
		txtDescriptiontext.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		txtDescriptiontext.setText(DESCRPTION_DEFAULT);		
		
		parent.layout();
		container.layout();
		eClassesListComposite.layout();
		validate();
	}

	/**
	 * Returns the selected {@link EClass} in the {@link EObjectListComposite}
	 * of EClasses
	 * 
	 * @return EReference The {@link EClass} selected by the user
	 */
	public EClass getSelectedEClass() {
		return (EClass) eClassesListComposite.getSelectedEObject();
	}

	/**
	 * Label provider of the EClassesListComposite
	 */
	private class EClassLabelProvider extends StyledCellLabelProvider {		
		@Override
		public void update(ViewerCell cell) {
			if (cell.getElement() instanceof EClass) {
				EClass eClass = (EClass) cell.getElement();
				cell.setText(eClass.getName());
				cell.setImage(ApogyCommonEMFUIFacade.INSTANCE.getImage(eClass));
			}
		}

		@Override
		public String getToolTipText(Object element) {
			if (element instanceof EClass) {
				return ((EClass) element).getInstanceClassName();
			}
			return super.getToolTipText(element);
		}

		@Override
		public Point getToolTipShift(Object object) {
			return new Point(5, 5);
		}

		@Override
		public int getToolTipDisplayDelayTime(Object object) {
			return 500;
		}

		@Override
		public int getToolTipTimeDisplayed(Object object) {
			return 5000;
		}
	}

	/**
	 * This method is invoked to validate the content.
	 */
	protected void validate() {
		String errorEClass = "";

		if (eClassesListComposite == null || eClassesListComposite.getSelectedEObject() == null) {
			errorEClass = "<Type> ";
		}
		if (errorEClass != "") 
		{
			setErrorMessage(errorEClass + "must be selected");
			setPageComplete(false);
		} 
		else 
		{			
			String description = DESCRPTION_DEFAULT;
			if(eClassesListComposite.getSelectedEObject() != null)				
			{						
				String tmp = ApogyCommonEMFFacade.INSTANCE.getDocumentation((EClass) eClassesListComposite.getSelectedEObject());				
				if(tmp != null) description = tmp;							
			}
						
			if(txtDescriptiontext != null) 
			{
				txtDescriptiontext.setText(description);
			}
			
			setErrorMessage(null);
			setPageComplete(true);
		}
	}

	/**
	 * This method is called when a new selection is made in the
	 * parentComposite.
	 * 
	 * @param selection
	 *            Reference to the selection.
	 */
	protected void newSelection(TreeSelection selection) {
	}
}
