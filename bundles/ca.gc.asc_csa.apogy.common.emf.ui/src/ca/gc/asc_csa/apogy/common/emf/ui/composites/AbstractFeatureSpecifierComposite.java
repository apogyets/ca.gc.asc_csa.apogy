package ca.gc.asc_csa.apogy.common.emf.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureSpecifier;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class AbstractFeatureSpecifierComposite extends Composite 
{
	private AbstractFeatureSpecifier abstractFeatureSpecifier;
	
	private Text txtFeatureName;
	private Text txtFeatureDescription;
	private Text txtFeatureType;	
	private Text txtFeatureIndex;
	
	private DataBindingContext m_bindingContext;
	
	public AbstractFeatureSpecifierComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
			
		Label lblFeatureDetails = new Label(this, SWT.NONE);
		lblFeatureDetails.setText("Feature Details");		
		GridData gd_lblFeatureDetails = new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1);
		lblFeatureDetails.setLayoutData(gd_lblFeatureDetails);
		
		// Feature Name
		Label lblFeatureName = new Label(this, SWT.NONE);
		lblFeatureName.setText("Name : ");		
		GridData gd_lblFeatureName = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		lblFeatureName.setLayoutData(gd_lblFeatureName);
		
		txtFeatureName = new Text(this, SWT.NONE);
		txtFeatureName.setEditable(false);
		txtFeatureName.setText("");
		GridData gd_txtFeatureName = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		txtFeatureName.setLayoutData(gd_txtFeatureName);
		
		// Feature Description
		Label lblFeatureDescription = new Label(this, SWT.NONE);
		lblFeatureDescription.setText("Description : ");		
		GridData gd_lblFeatureDescription = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		lblFeatureDescription.setLayoutData(gd_lblFeatureDescription);
		
		txtFeatureDescription = new Text(this, SWT.NONE | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		txtFeatureDescription.setEditable(false);
		txtFeatureDescription.setText("");
		GridData gd_txtFeatureDescription = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtFeatureDescription.minimumHeight = 100;
		gd_txtFeatureDescription.heightHint = 100;
		txtFeatureDescription.setLayoutData(gd_txtFeatureDescription);
		
		// Feature Type
		Label lblFeatureType = new Label(this, SWT.NONE);
		lblFeatureType.setText("Type : ");		
		GridData gd_lblFeatureType = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		lblFeatureType.setLayoutData(gd_lblFeatureType);

		txtFeatureType = new Text(this, SWT.NONE);
		txtFeatureType.setEditable(false);
		txtFeatureType.setText("");	
		txtFeatureType.setToolTipText("The type (class) of the feature value.");
		GridData gd_txtFeatureType = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		txtFeatureType.setLayoutData(gd_txtFeatureType);

		// Index
		Label lblFeatureIsMultiValued= new Label(this, SWT.NONE);
		lblFeatureIsMultiValued.setText("Index : ");
		GridData gd_lblFeatureIsMultiValued = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		lblFeatureIsMultiValued.setLayoutData(gd_lblFeatureIsMultiValued);

		txtFeatureIndex = new Text(this, SWT.NONE);
		txtFeatureIndex.setEditable(false);
		txtFeatureIndex.setText("");
		txtFeatureIndex.setToolTipText("The index of the feature of interest if this feature is an array.");
		GridData gd_txtFeatureIndex = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_txtFeatureIndex.minimumWidth = 50;
		gd_txtFeatureIndex.widthHint = 50;
		txtFeatureIndex.setLayoutData(gd_txtFeatureIndex);
		
		// Dispose listener.
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();			
			}
		});
	}

	public AbstractFeatureSpecifier getAbstractFeatureSpecifier() 
	{
		return abstractFeatureSpecifier;
	}

	public void setAbstractFeatureSpecifier(AbstractFeatureSpecifier newAbstractFeatureSpecifier) 
	{				
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.abstractFeatureSpecifier = newAbstractFeatureSpecifier;
		
		if(newAbstractFeatureSpecifier != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
	}	
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();

		// Is Multi-Value class.		
		IObservableValue<Boolean> observeFeatureMultiValue = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  										 	FeaturePath.fromList(ApogyCommonEMFPackage.Literals.ABSTRACT_FEATURE_SPECIFIER__MULTI_VALUED)).observe(getAbstractFeatureSpecifier());
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledtxtFeatureIndexObserveWidget = WidgetProperties.enabled().observe(txtFeatureIndex);
		bindingContext.bindValue(observeEnabledtxtFeatureIndexObserveWidget, observeFeatureMultiValue, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		EStructuralFeature eStructuralFeature = getAbstractFeatureSpecifier().getStructuralFeature();
		
		// Update displayed value.
		getDisplay().asyncExec(new Runnable() 
		{			
			@Override
			public void run() {				
				txtFeatureName.setText(eStructuralFeature.getName());
				txtFeatureDescription.setText(ApogyCommonEMFFacade.INSTANCE.getFullDescription(eStructuralFeature));
				txtFeatureType.setText(eStructuralFeature.getEType().getInstanceTypeName());
				txtFeatureType.setToolTipText(eStructuralFeature.getEType().getInstanceTypeName());
				txtFeatureIndex.setText(Integer.toString(getAbstractFeatureSpecifier().getIndex()));
			}
		});
		
				
		return bindingContext;
	}
}
