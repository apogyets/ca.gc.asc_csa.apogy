package ca.gc.asc_csa.apogy.common.emf.ui.composites;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.CompositeFilterType;

public class CompositeFilterTypeComboComposite extends Composite 
{
	private CompositeFilterType selectedCompositeFilterType;
	private ComboViewer comboViewer;
	
	public CompositeFilterTypeComboComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new FillLayout());
		comboViewer = createCombo(this, SWT.READ_ONLY);		
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{		
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{	
				if(event.getSelection().isEmpty())
				{
					newSelection(null);
				}
				else if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					if(iStructuredSelection.getFirstElement() instanceof CompositeFilterType)
					{
						newSelection((CompositeFilterType)iStructuredSelection.getFirstElement());
					}
					else
					{
						newSelection(null);
					}
				}
			}
		});				
	}
	
	private ComboViewer createCombo(Composite parent, int style)
	{
		ComboViewer comboViewer = new ComboViewer(parent, SWT.DROP_DOWN | SWT.READ_ONLY);		
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setLabelProvider(new LabelProvider()
		{
			@Override
			public String getText(Object element) 
			{
				if(element instanceof CompositeFilterType)
				{
					CompositeFilterType type = (CompositeFilterType) element;
					return type.getName();
				}
				else
				{
					return "";
				}
			}
		});
				
		// Display the combo element sorted by displayed name.
		comboViewer.setComparator(new ViewerComparator()
		{
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) 
			{
				CompositeFilterType type1 = (CompositeFilterType) e1;
				CompositeFilterType type2 = (CompositeFilterType) e2;
				
				return type1.getName().compareTo(type2.getName());				
			}
		});
						
		comboViewer.setInput(CompositeFilterType.VALUES);
				
		return comboViewer;
	}		
	
	public CompositeFilterType getSelectedCompositeFilterType() {
		return selectedCompositeFilterType;
	}

	public void setSelectedCompositeFilterType(CompositeFilterType selectedVariable) 
	{
		this.selectedCompositeFilterType = selectedVariable;
		
		if(selectedVariable != null)
		{
			comboViewer.setSelection(new StructuredSelection(selectedVariable));
		}
	}
	
	protected void newSelection(CompositeFilterType compositeFilterType)
	{		
	}
}
