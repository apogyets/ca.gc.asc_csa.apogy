/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards;

import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ConicalFieldOfView;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;

public class ConicalFOVApexAngleWizardPage extends WizardPage 
{
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.ConicalFOVApexAngleWizardPage";
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 		

	private ConicalFieldOfView conicalFieldOfView;	
	private TypedElementSimpleUnitsComposite apexAngleComposite;

	private Adapter adapter;
	
	public ConicalFOVApexAngleWizardPage(ConicalFieldOfView conicalFieldOfView)
	{
		super(WIZARD_PAGE_ID);
		this.conicalFieldOfView = conicalFieldOfView;
				
		setTitle("Apex Angle");
		setDescription("Sets the Conical field of view apex angle.");				
	}
	
	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));

		apexAngleComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Apex Angle :";
			}
		};
		apexAngleComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		apexAngleComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsSensorsFOVPackage.Literals.CONICAL_FIELD_OF_VIEW__FIELD_OF_VIEW_ANGLE), getConicalFieldOfView());		
		
		setControl(top);
		
		if(conicalFieldOfView != null)  conicalFieldOfView.eAdapters().add(getAdapter());
		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(conicalFieldOfView != null) conicalFieldOfView.eAdapters().remove(getAdapter());
			}
		});
		
		validate();
	}
	
	public ConicalFieldOfView getConicalFieldOfView() {
		return conicalFieldOfView;
	}

	public void setConicalFieldOfView(ConicalFieldOfView conicalFieldOfView) {
		this.conicalFieldOfView = conicalFieldOfView;
	}

	protected void validate()
	{
		setErrorMessage(null);
		
		if(conicalFieldOfView.getFieldOfViewAngle() < 0)
		{
			setErrorMessage("The specified apex angle should equal or greater than zero !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof ConicalFieldOfView)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
}
