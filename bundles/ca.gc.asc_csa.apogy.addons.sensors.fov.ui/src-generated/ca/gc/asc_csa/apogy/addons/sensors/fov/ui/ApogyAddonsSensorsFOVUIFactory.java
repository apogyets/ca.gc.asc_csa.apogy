package ca.gc.asc_csa.apogy.addons.sensors.fov.ui;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage
 * @generated
 */
public interface ApogyAddonsSensorsFOVUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	ApogyAddonsSensorsFOVUIFactory eINSTANCE = ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl.ApogyAddonsSensorsFOVUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Field Of View Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Field Of View Presentation</em>'.
	 * @generated
	 */
	FieldOfViewPresentation createFieldOfViewPresentation();

	/**
	 * Returns a new object of class '<em>Circular Sector Field Of View Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Circular Sector Field Of View Presentation</em>'.
	 * @generated
	 */
	CircularSectorFieldOfViewPresentation createCircularSectorFieldOfViewPresentation();

	/**
	 * Returns a new object of class '<em>Conical Field Of View Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Conical Field Of View Presentation</em>'.
	 * @generated
	 */
	ConicalFieldOfViewPresentation createConicalFieldOfViewPresentation();

	/**
	 * Returns a new object of class '<em>Rectangular Frustrum Field Of View Presentation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return a new object of class '<em>Rectangular Frustrum Field Of View Presentation</em>'.
	 * @generated
	 */
	RectangularFrustrumFieldOfViewPresentation createRectangularFrustrumFieldOfViewPresentation();

	/**
	 * Returns a new object of class '<em>Circular Sector Field Of View Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Circular Sector Field Of View Wizard Pages Provider</em>'.
	 * @generated
	 */
	CircularSectorFieldOfViewWizardPagesProvider createCircularSectorFieldOfViewWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Conical Field Of View Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conical Field Of View Wizard Pages Provider</em>'.
	 * @generated
	 */
	ConicalFieldOfViewWizardPagesProvider createConicalFieldOfViewWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rectangular Frustrum Field Of View Wizard Pages Provider</em>'.
	 * @generated
	 */
	RectangularFrustrumFieldOfViewWizardPagesProvider createRectangularFrustrumFieldOfViewWizardPagesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyAddonsSensorsFOVUIPackage getApogyAddonsSensorsFOVUIPackage();

} //ApogyAddonsSensorsFOVUIFactory
