/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVFacade;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVFactory;
import ca.gc.asc_csa.apogy.addons.sensors.fov.DistanceRange;
import ca.gc.asc_csa.apogy.addons.sensors.fov.RectangularFrustrumFieldOfView;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.RectangularFrustrumFieldOfViewWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.DistanceRangeWizardPage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.RectangularFrustrumFieldOfViewAnglesWizardPage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rectangular Frustrum Field Of View Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RectangularFrustrumFieldOfViewWizardPagesProviderImpl extends FieldOfViewWizardPagesProviderImpl implements RectangularFrustrumFieldOfViewWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RectangularFrustrumFieldOfViewWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsFOVUIPackage.Literals.RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		RectangularFrustrumFieldOfView fov = ApogyAddonsSensorsFOVFactory.eINSTANCE.createRectangularFrustrumFieldOfView();
		
		DistanceRange distanceRange = ApogyAddonsSensorsFOVFacade.INSTANCE.createDistanceRange(0, 1.0);				
		fov.setRange(distanceRange);
		
		fov.setHorizontalFieldOfViewAngle(Math.toRadians(60.0));
		fov.setVerticalFieldOfViewAngle(Math.toRadians(45.0));
		
		return fov;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		RectangularFrustrumFieldOfView fov = (RectangularFrustrumFieldOfView) eObject;
		
		// Add Horizontal + vertical FOV angles.
		list.add(new RectangularFrustrumFieldOfViewAnglesWizardPage(fov));
		
		// Add Distance Range page.
		list.add(new DistanceRangeWizardPage(fov.getRange()));
	
		return list;
	}
} //RectangularFrustrumFieldOfViewWizardPagesProviderImpl
