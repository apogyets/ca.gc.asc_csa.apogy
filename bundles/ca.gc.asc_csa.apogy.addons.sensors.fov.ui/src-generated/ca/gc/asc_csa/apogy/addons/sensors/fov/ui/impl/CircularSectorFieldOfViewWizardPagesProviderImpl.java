/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.impl;

import ca.gc.asc_csa.apogy.addons.sensors.fov.AngularSpan;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVFacade;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVFactory;
import ca.gc.asc_csa.apogy.addons.sensors.fov.CircularSectorFieldOfView;
import ca.gc.asc_csa.apogy.addons.sensors.fov.DistanceRange;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.ApogyAddonsSensorsFOVUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.CircularSectorFieldOfViewWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.AngularSpanWizardPage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ui.wizards.DistanceRangeWizardPage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Circular Sector Field Of View Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CircularSectorFieldOfViewWizardPagesProviderImpl extends FieldOfViewWizardPagesProviderImpl implements CircularSectorFieldOfViewWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CircularSectorFieldOfViewWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsFOVUIPackage.Literals.CIRCULAR_SECTOR_FIELD_OF_VIEW_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		CircularSectorFieldOfView fov = ApogyAddonsSensorsFOVFactory.eINSTANCE.createCircularSectorFieldOfView();
		
		DistanceRange distanceRange = ApogyAddonsSensorsFOVFacade.INSTANCE.createDistanceRange(0, 1.0);				
		fov.setRange(distanceRange);
		
		AngularSpan angularSpan = ApogyAddonsSensorsFOVFacade.INSTANCE.createAngularSpan(Math.toRadians(-22.5), Math.toRadians(22.5));
		fov.setAngularSpan(angularSpan);
		
		return fov;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		CircularSectorFieldOfView fov = (CircularSectorFieldOfView) eObject;
		
		// Add Angular span page.
		list.add(new AngularSpanWizardPage(fov.getAngularSpan()));
		
		// Add Distance Range page.
		list.add(new DistanceRangeWizardPage(fov.getRange()));
	
		return list;
	}
} //CircularSectorFieldOfViewWizardPagesProviderImpl
