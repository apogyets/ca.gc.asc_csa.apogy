/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.geometry.data3d.provider;


import ca.gc.asc_csa.apogy.common.geometry.data3d.ApogyCommonGeometryData3DPackage;
import ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler;

import ca.gc.asc_csa.apogy.common.processors.provider.ProcessorItemProvider;

import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.common.geometry.data3d.InfiniteHeightVoxelResampler} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class InfiniteHeightVoxelResamplerItemProvider extends ProcessorItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfiniteHeightVoxelResamplerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFilterTypePropertyDescriptor(object);
			addMinimumNumberOfPointPerVoxelPropertyDescriptor(object);
			addShortRangeLimitPropertyDescriptor(object);
			addShortRangeResolutionPropertyDescriptor(object);
			addLongRangeLimitPropertyDescriptor(object);
			addLongRangeResolutionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Filter Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFilterTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_InfiniteHeightVoxelResampler_filterType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_InfiniteHeightVoxelResampler_filterType_feature", "_UI_InfiniteHeightVoxelResampler_type"),
				 ApogyCommonGeometryData3DPackage.Literals.INFINITE_HEIGHT_VOXEL_RESAMPLER__FILTER_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Minimum Number Of Point Per Voxel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinimumNumberOfPointPerVoxelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_InfiniteHeightVoxelResampler_minimumNumberOfPointPerVoxel_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_InfiniteHeightVoxelResampler_minimumNumberOfPointPerVoxel_feature", "_UI_InfiniteHeightVoxelResampler_type"),
				 ApogyCommonGeometryData3DPackage.Literals.INFINITE_HEIGHT_VOXEL_RESAMPLER__MINIMUM_NUMBER_OF_POINT_PER_VOXEL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Short Range Limit feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShortRangeLimitPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_InfiniteHeightVoxelResampler_shortRangeLimit_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_InfiniteHeightVoxelResampler_shortRangeLimit_feature", "_UI_InfiniteHeightVoxelResampler_type"),
				 ApogyCommonGeometryData3DPackage.Literals.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_LIMIT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Short Range Resolution feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShortRangeResolutionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_InfiniteHeightVoxelResampler_shortRangeResolution_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_InfiniteHeightVoxelResampler_shortRangeResolution_feature", "_UI_InfiniteHeightVoxelResampler_type"),
				 ApogyCommonGeometryData3DPackage.Literals.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_RESOLUTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Long Range Limit feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLongRangeLimitPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_InfiniteHeightVoxelResampler_longRangeLimit_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_InfiniteHeightVoxelResampler_longRangeLimit_feature", "_UI_InfiniteHeightVoxelResampler_type"),
				 ApogyCommonGeometryData3DPackage.Literals.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_LIMIT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Long Range Resolution feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLongRangeResolutionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_InfiniteHeightVoxelResampler_longRangeResolution_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_InfiniteHeightVoxelResampler_longRangeResolution_feature", "_UI_InfiniteHeightVoxelResampler_type"),
				 ApogyCommonGeometryData3DPackage.Literals.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_RESOLUTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns InfiniteHeightVoxelResampler.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/InfiniteHeightVoxelResampler"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		IProgressMonitor labelValue = ((InfiniteHeightVoxelResampler)object).getProgressMonitor();
		String label = labelValue == null ? null : labelValue.toString();
		return label == null || label.length() == 0 ?
			getString("_UI_InfiniteHeightVoxelResampler_type") :
			getString("_UI_InfiniteHeightVoxelResampler_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(InfiniteHeightVoxelResampler.class)) {
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__FILTER_TYPE:
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__MINIMUM_NUMBER_OF_POINT_PER_VOXEL:
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_LIMIT:
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__SHORT_RANGE_RESOLUTION:
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_LIMIT:
			case ApogyCommonGeometryData3DPackage.INFINITE_HEIGHT_VOXEL_RESAMPLER__LONG_RANGE_RESOLUTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
