package ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.vehicle.VehiclePathPlannerTool;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.composites.VehiclePathPlannerToolComposite;
import ca.gc.asc_csa.apogy.core.invocator.Variable;

public class VehiclePathPlannerToolWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards.VehiclePathPlannerToolWizardPage";
	
	private VehiclePathPlannerTool vehiclePathPlannerTool;
	private VehiclePathPlannerToolComposite vehiclePathPlannerToolComposite;
	
	public VehiclePathPlannerToolWizardPage(VehiclePathPlannerTool vehiclePathPlannerTool) 
	{
		super(WIZARD_PAGE_ID);
		this.vehiclePathPlannerTool = vehiclePathPlannerTool;
		
		setTitle("Vehicle Path Planner Tool");
		setDescription("Configure the Vehicle Path Planner Tool settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));
		
		vehiclePathPlannerToolComposite = new VehiclePathPlannerToolComposite(container, SWT.None)
		{
			@Override
			protected void newVariableSelected(Variable variable) 
			{
				validate();
			}	
		};
		vehiclePathPlannerToolComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		vehiclePathPlannerToolComposite.setVehiclePathPlannerTool(vehiclePathPlannerTool);
		
		setControl(container);
		vehiclePathPlannerToolComposite.setFocus();				
		
		validate();
	}	
	
	protected void validate()
	{				
		setErrorMessage(null);		
		
		if(vehiclePathPlannerTool.getVariable() == null)
		{
			setErrorMessage("The variable is not set !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
}
