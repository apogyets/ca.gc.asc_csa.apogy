package ca.gc.asc_csa.apogy.addons.vehicle.ui.composites;

import java.text.DecimalFormat;

import javax.vecmath.Vector3d;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.OrientationCorrectionMode;
import ca.gc.asc_csa.apogy.addons.vehicle.VehiclePoseCorrector;
import ca.gc.asc_csa.apogy.addons.vehicle.ZCorrectionMode;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.GeometricUtils;
import ca.gc.asc_csa.apogy.common.math.Matrix3x3;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.ApogyCorePackage;

public class VehiclePoseCorrectorComposite extends Composite 
{
	public static final String DEGREE_STRING = "\u00b0";
		
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private VehiclePoseCorrector vehiclePoseCorrector;
	
	private Adapter orientationCorrectionAdapter;
	
	private Button btnEnable;
	private Button btnReinitialize;
	private ComboViewer comboZCorrectionMode;
	private ComboViewer comboOrientationCorrectionMode;
	private Text txtZCorrection;
	private Tuple3dComposite orientationCorrectionComposite;
	private Tuple3d orientationCorrection = ApogyCommonMathFacade.INSTANCE.createTuple3d(0,0,0);
	
	private DecimalFormat zCorrectionFormat = new DecimalFormat("0.000");
	private DataBindingContext m_bindingContext;
		
	public VehiclePoseCorrectorComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(3, false));
		
		Label lblEnable = new Label(this, SWT.NONE);
		lblEnable.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEnable.setText("Enabled :");
		
		btnEnable = new Button(this, SWT.CHECK);
		
		btnReinitialize = new Button(this, SWT.NONE);
		btnReinitialize.setToolTipText("Forces re-initialization of the pose corrector meshes contact geometries.");
		btnReinitialize.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnReinitialize.setText("Re-Initialize");
		btnReinitialize.addSelectionListener(new SelectionListener() 
		{	
			@Override
			public void widgetSelected(SelectionEvent e) 
			{				
				if(getVehiclePoseCorrector() != null)
				{
					// Run this in a job.
					Job job = new Job("")
					{
						@Override
						protected IStatus run(IProgressMonitor monitor) 
						{
							getVehiclePoseCorrector().reInitialize();
							return Status.OK_STATUS;
						}
					};
					job.schedule();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) 
			{								
			}
		});
		
		Label lblZCorrectionMode = new Label(this, SWT.NONE);
		lblZCorrectionMode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblZCorrectionMode.setText("Z Correction Mode:");
		
		comboZCorrectionMode = new ComboViewer(this, SWT.NONE);				
		comboZCorrectionMode.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		comboZCorrectionMode.setContentProvider(new ArrayContentProvider());
		comboZCorrectionMode.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		comboZCorrectionMode.setInput(ZCorrectionMode.values());
		
		Label lblOrientationCorrectionMode = new Label(this, SWT.NONE);
		lblOrientationCorrectionMode.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblOrientationCorrectionMode.setText("Orientation Correction Mode:");
		
		comboOrientationCorrectionMode = new ComboViewer(this, SWT.NONE);
		comboOrientationCorrectionMode.getCombo().setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		comboOrientationCorrectionMode.setContentProvider(new ArrayContentProvider());
		comboOrientationCorrectionMode.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		comboOrientationCorrectionMode.setInput(OrientationCorrectionMode.values());
		
		
		Label lblCurrentZCorrectionm = new Label(this, SWT.NONE);
		lblCurrentZCorrectionm.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCurrentZCorrectionm.setText("Z Correction (m):");
		
		txtZCorrection = new Text(this, SWT.BORDER);
		txtZCorrection.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		txtZCorrection.setEditable(false);
		
		Label lblCurrentOrientationCorrection = new Label(this, SWT.NONE);
		lblCurrentOrientationCorrection.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblCurrentOrientationCorrection.setText("Orientation Correction (" + DEGREE_STRING + "):");
		
		orientationCorrectionComposite = new Tuple3dComposite(this, SWT.NONE, "0.00");
		GridData gd_orientation = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_orientation.widthHint = 300;
		gd_orientation.minimumWidth = 300;
		orientationCorrectionComposite.setLayoutData(gd_orientation);
		orientationCorrectionComposite.setEnableEditing(false);		
		orientationCorrectionComposite.setTuple3d(orientationCorrection);
		
		enableControls(false);
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if(m_bindingContext != null) m_bindingContext.dispose();
				if(vehiclePoseCorrector!= null) vehiclePoseCorrector.eAdapters().remove(getOrientationCorrectionAdapter()); 				
			}
		});
	}

	public VehiclePoseCorrector getVehiclePoseCorrector() {
		return vehiclePoseCorrector;
	}

	public void setVehiclePoseCorrector(VehiclePoseCorrector vehiclePoseCorrector) 
	{
		if(vehiclePoseCorrector!= null) vehiclePoseCorrector.eAdapters().remove(getOrientationCorrectionAdapter());
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.vehiclePoseCorrector = vehiclePoseCorrector;
		
		// Enables / disabled the controls.
		enableControls(vehiclePoseCorrector != null);
		
		if(vehiclePoseCorrector != null)
		{
			m_bindingContext = customInitDataBindings();
			vehiclePoseCorrector.eAdapters().add(getOrientationCorrectionAdapter());
		}
	}
	
	public void enableControls(boolean enabled)
	{
		if(btnEnable != null && !btnEnable.isDisposed()) btnEnable.setEnabled(enabled);		
		if(btnReinitialize != null && !btnReinitialize.isDisposed()) btnReinitialize.setEnabled(enabled);		
		if(txtZCorrection != null && !txtZCorrection.isDisposed()) txtZCorrection.setEnabled(enabled);
		if(orientationCorrectionComposite != null && !orientationCorrectionComposite.isDisposed()) orientationCorrectionComposite.setEnabled(enabled);
		
		if(comboZCorrectionMode != null && comboZCorrectionMode.getCombo() != null && !comboZCorrectionMode.getCombo().isDisposed())
		{			
			comboZCorrectionMode.getControl().setEnabled(enabled);
			comboZCorrectionMode.getCombo().setEnabled(enabled);			
		}
		
		if(comboOrientationCorrectionMode != null && comboOrientationCorrectionMode.getCombo() != null && !comboOrientationCorrectionMode.getCombo().isDisposed())
		{
			comboOrientationCorrectionMode.getControl().setEnabled(enabled);
			comboOrientationCorrectionMode.getCombo().setEnabled(enabled);
		}
	}

	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Enabled.*/
		IObservableValue<Double> observeEnabled = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
																	  FeaturePath.fromList(ApogyCorePackage.Literals.ABSTRACT_APOGY_SYSTEM_POSE_CORRECTOR__ENABLED)).observe(vehiclePoseCorrector);
		IObservableValue<String> observeEnabledButtton = WidgetProperties.selection().observe(btnEnable);
				
		bindingContext.bindValue(observeEnabledButtton,
								 observeEnabled, 
								 new UpdateValueStrategy(),	
								 new UpdateValueStrategy());
		
		
		/* Z Correction Value.*/
		IObservableValue<Double> observeZCorrection = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(),
												FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.VEHICLE_POSE_CORRECTOR__ZCORRECTION)).observe(vehiclePoseCorrector);
		IObservableValue<String> observeZCorrectionTxt = WidgetProperties.text(SWT.Modify).observe(txtZCorrection);

		bindingContext.bindValue(observeZCorrectionTxt, 
								 observeZCorrection,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER), 
				new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class) {
					@Override
					public Object convert(Object fromObject) 
					{
						return zCorrectionFormat.format((Double) fromObject);
					}

				}));

		
		/* Z Correction enablement.*/
		IObservableValue<String> observeZCorrectionEnable = WidgetProperties.enabled().observe(txtZCorrection);
		bindingContext.bindValue(observeZCorrectionEnable, 
								observeEnabled,
								new UpdateValueStrategy(), 
								new UpdateValueStrategy());																
		
		/* Orientation correction enablement */
		IObservableValue<String> observeOrientationCorrectionEnable = WidgetProperties.enabled().observe(orientationCorrectionComposite);
		bindingContext.bindValue(observeOrientationCorrectionEnable, 
								observeEnabled,
								new UpdateValueStrategy(), 
								new UpdateValueStrategy());	
						
		/* Z Correction Mode.*/
		IObservableValue<Double> observeZCorrectionMode = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(),
																				  FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.VEHICLE_POSE_CORRECTOR__ZCORRECTION_MODE)).observe(vehiclePoseCorrector);
		
		IObservableValue<?> observeZCorrectionModeCombViewer = ViewerProperties.singleSelection().observe(comboZCorrectionMode);
		bindingContext.bindValue(observeZCorrectionModeCombViewer, 
								 observeZCorrectionMode,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		/* Orientation Correction Mode.*/
		IObservableValue<Double> observeOrientationCorrectionMode = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(),
																				  FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.VEHICLE_POSE_CORRECTOR__ORIENTATION_CORRECTION_MODE)).observe(vehiclePoseCorrector);
		
		IObservableValue<?> observeOrientationCorrectionModeComboViewer = ViewerProperties.singleSelection().observe(comboOrientationCorrectionMode);
		bindingContext.bindValue(observeOrientationCorrectionModeComboViewer, 
								 observeOrientationCorrectionMode,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());

				
		return bindingContext;
	}

	protected Adapter getOrientationCorrectionAdapter() 
	{
		if(orientationCorrectionAdapter == null)
		{
			orientationCorrectionAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof VehiclePoseCorrector)
					{
						int featureID = msg.getFeatureID(VehiclePoseCorrector.class);
						if(featureID == ApogyAddonsVehiclePackage.VEHICLE_POSE_CORRECTOR__ORIENTATION_CORRECTION)
						{
							if(msg.getNewValue() instanceof Matrix3x3)
							{
								Matrix3x3 rot = (Matrix3x3) msg.getNewValue();
								Vector3d r = GeometricUtils.extractRotationFromXYZRotMatrix(rot.asMatrix3d());
								orientationCorrection.setX(Math.toDegrees(r.x));
								orientationCorrection.setY(Math.toDegrees(r.y));
								orientationCorrection.setZ(Math.toDegrees(r.z));
							}
						}
					}
				}
			};
		}
		return orientationCorrectionAdapter;
	}
	
	
}
