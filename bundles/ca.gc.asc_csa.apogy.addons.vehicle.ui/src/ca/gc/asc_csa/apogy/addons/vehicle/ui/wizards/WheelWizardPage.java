package ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards;

import java.text.DecimalFormat;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.Wheel;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.TypedElementSimpleUnitsComposite;

public class WheelWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards.WheelWizardPage";
	
	public static String NO_VALUE_AVAILABLE_STRING = "N/A";
	private int LABEL_WIDTH = 200; 
	private int VALUE_WIDTH = 100; 
	private int BUTTON_WIDTH = 50; 		
	
	private Wheel wheel;
	
	private TypedElementSimpleUnitsComposite wheelRadiusComposite;
	private TypedElementSimpleUnitsComposite wheelWidthComposite;
		
	private Adapter adapter;
	
	public WheelWizardPage(Wheel wheel)
	{
		super(WIZARD_PAGE_ID);
		this.wheel = wheel;
				
		setTitle("Wheel");
		setDescription("Sets the wheel dimension.");	
		
	}
	@Override
	public void createControl(Composite parent) 
	{		
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));

		wheelRadiusComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Wheel Radius :";
			}
		};
		wheelRadiusComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		wheelRadiusComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.WHEEL__RADIUS), getWheel());

		
		wheelWidthComposite = new TypedElementSimpleUnitsComposite(top, SWT.NONE, true, true, true, NO_VALUE_AVAILABLE_STRING, LABEL_WIDTH, VALUE_WIDTH, BUTTON_WIDTH)
		{
			DecimalFormat format = new DecimalFormat("0.00");			
			@Override
			protected DecimalFormat getDecimalFormat() {
				return format;
			}
			
			@Override
			protected String getLabelText() {
				return "Wheel Width :";
			}
		};
		wheelWidthComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		wheelWidthComposite.setTypedElement(FeaturePath.fromList(ApogyAddonsVehiclePackage.Literals.WHEEL__LENGTH), getWheel());
	
		
		setControl(top);
		
		if(wheel != null)  wheel.eAdapters().add(getAdapter());		
		top.addDisposeListener(new DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent arg0) {				
				if(wheel != null) wheel.eAdapters().remove(getAdapter());
			}
		});
		
		validate();
	}

	public Wheel getWheel() {
		return wheel;
	}
	public void setWheel(Wheel wheel) 
	{
		this.wheel = wheel;
		
		if(wheelRadiusComposite != null && !wheelRadiusComposite.isDisposed())
		{
			wheelRadiusComposite.setInstance(wheel);
		}
		
		if(wheelWidthComposite != null && !wheelWidthComposite.isDisposed())
		{
			wheelWidthComposite.setInstance(wheel);
		}
	}
	
	protected void validate()
	{
		setErrorMessage(null);
		
		if(wheel.getRadius() <= 0)
		{
			setErrorMessage("The specified radius must be greater than zero !");
		}
		
		if(wheel.getLength() <= 0)
		{
			setErrorMessage("The specified width must be greater than zero !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	
	protected Adapter getAdapter() 
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof Wheel)
					{
						validate();
					}
				}
			};
		}
		return adapter;
	}
}
