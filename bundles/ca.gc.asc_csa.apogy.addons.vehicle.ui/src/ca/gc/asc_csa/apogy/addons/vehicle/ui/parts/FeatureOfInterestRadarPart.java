package ca.gc.asc_csa.apogy.addons.vehicle.ui.parts;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIRCPConstants;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.composites.FeatureOfInterestRadarComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFactory;
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;
import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIFacade;
import ca.gc.asc_csa.apogy.core.invocator.ui.parts.AbstractSessionBasedPart;

public class FeatureOfInterestRadarPart extends AbstractSessionBasedPart 
{
	private double maximumRadius = 10.0;
	private FeatureOfInterestRadarComposite featureOfInterestRadarComposite = null;
	private VariableFeatureReference variableFeatureReference = ApogyCoreInvocatorFactory.eINSTANCE.createVariableFeatureReference();
	
	@Override
	protected void newInvocatorSession(InvocatorSession invocatorSession) 
	{
		if(invocatorSession != null)
		{
			if(featureOfInterestRadarComposite != null)
			{
				VariableFeatureReference vfr = (VariableFeatureReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__FOI_RADAR_PART__VARIABLE_FEATURE_REFERENCE_ID);
				if(vfr != null)
				{
					this.variableFeatureReference = vfr;
					featureOfInterestRadarComposite.setVariableFeatureReference(vfr);				
				}												
								
				String maximumRadiusString = mPart.getPersistedState().get(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__FOI_RADAR_PART__MAXIMUM_RADIUS);								
				if(maximumRadiusString != null) 
				{
					try 
					{
						maximumRadius = Double.parseDouble(maximumRadiusString);
					} 
					catch (Exception e) 
					{
					}
					
					featureOfInterestRadarComposite.setMaximumRadius(maximumRadius);
				}
				
				featureOfInterestRadarComposite.setVariablesList(invocatorSession.getEnvironment().getVariablesList());
			}
		}		
	}

	@Override
	protected void createContentComposite(Composite parent, int style) 
	{		
		parent.setLayout(new FillLayout());
		
		VariablesList variables = null;
		
		if(ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession() != null &&
		   ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment() != null)
		{
			variables = ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession().getEnvironment().getVariablesList();
		}
				
		ApogyCommonTransactionFacade.INSTANCE.addInTempTransactionalEditingDomain(variableFeatureReference);
		featureOfInterestRadarComposite = new FeatureOfInterestRadarComposite(parent, style, variables, variableFeatureReference);
		featureOfInterestRadarComposite.setMaximumRadius(maximumRadius);
	}
	
	@Override
	public void userPostConstruct(MPart mPart) 
	{	
		try
		{
			String maximumRadiusString = mPart.getPersistedState().get(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__FOI_RADAR_PART__MAXIMUM_RADIUS);
			if(maximumRadiusString != null)
			{
				try
				{
					maximumRadius = Double.parseDouble(maximumRadiusString);
				}
				catch (Exception e) 
				{
				}
			}
			
			VariableFeatureReference vfr = (VariableFeatureReference) ApogyCoreInvocatorUIFacade.INSTANCE.readFromPersistedState(mPart, ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__FOI_RADAR_PART__VARIABLE_FEATURE_REFERENCE_ID);
			if(vfr != null)
			{
				this.variableFeatureReference = vfr;
			}
		}
		catch(Throwable t)
		{
			t.printStackTrace();
		}				
	}

	@Override
	public void userPersistState(MPart mPart)
	{		
		if(featureOfInterestRadarComposite != null)
		{
			// Persist the maximum radius.
			mPart.getPersistedState().put(ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__FOI_RADAR_PART__MAXIMUM_RADIUS, Double.toString(featureOfInterestRadarComposite.getMaximumRadius()));
			
			// Persits the Variable Feature Reference.
			ApogyCoreInvocatorUIFacade.INSTANCE.saveToPersistedState(mPart, ApogyAddonsVehicleUIRCPConstants.PERSISTED_STATE__FOI_RADAR_PART__VARIABLE_FEATURE_REFERENCE_ID, variableFeatureReference);
		}
	}
}
