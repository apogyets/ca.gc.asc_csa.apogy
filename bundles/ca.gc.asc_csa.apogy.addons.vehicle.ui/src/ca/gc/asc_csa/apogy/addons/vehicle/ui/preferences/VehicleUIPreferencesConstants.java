package ca.gc.asc_csa.apogy.addons.vehicle.ui.preferences;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import org.eclipse.swt.graphics.RGB;


public class VehicleUIPreferencesConstants 
{
	public static final String DEFAULT_PHYSICAL_WHEEL_VISIBILITY_ID = "DEFAULT_PHYSICAL_WHEEL_VISIBILITY_ID";
	public static final boolean DEFAULT_PHYSICAL_WHEEL_VISIBILITY = false;
	
	public static final String DEFAULT_PHYSICAL_WHEEL_COLOR_ID = "DEFAULT_PHYSICAL_WHEEL_COLOR_ID";		
	public static final RGB DEFAULT_PHYSICAL_WHEEL_COLOR  = new RGB(255,255,255);
	
	public static final String DEFAULT_LANDER_SPHERICAL_FOOT_VISIBILITY_ID = "DEFAULT_LANDER_SPHERICAL_FOOT_VISIBILITY_ID";
	public static final boolean DEFAULT_LANDER_SPHERICAL_FOOT_VISIBILITY = false;
	
	public static final String DEFAULT_LANDER_SPHERICAL_FOOT_COLOR_ID = "DEFAULT_LANDER_SPHERICAL_FOOT_COLOR_ID";		
	public static final RGB DEFAULT_LANDER_SPHERICAL_FOOT_COLOR  = new RGB(255,255,255);
	
	public static final String DEFAULT_THRUSTER_PLUME_ENVELOPE_VISIBILITY_ID = "DEFAULT_THRUSTER_PLUME_ENVELOPE_VISIBILITY_ID";
	public static final boolean DEFAULT_THRUSTER_PLUME_ENVELOPE_VISIBILITY = false;
	
	public static final String DEFAULT_THRUSTER_PLUME_ENVELOPE_COLOR_ID = "DEFAULT_THRUSTER_PLUME_ENVELOPE_COLOR_ID";		
	public static final RGB DEFAULT_THRUSTER_PLUME_ENVELOPE_COLOR  = new RGB(255,0,0);
	
	public static final String DEFAULT_THRUSTER_PLUME_ENVELOPE_LENGTH_ID = "DEFAULT_THRUSTER_PLUME_ENVELOPE_LENGTH_ID";
	public static final double DEFAULT_THRUSTER_PLUME_ENVELOPE_LENGTH = 1.0;
}
