package ca.gc.asc_csa.apogy.addons.vehicle.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import ca.gc.asc_csa.apogy.addons.vehicle.VehiclePoseCorrector;
import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter;
import ca.gc.asc_csa.apogy.core.invocator.VariableFeatureReference;
import ca.gc.asc_csa.apogy.core.invocator.VariablesList;
import ca.gc.asc_csa.apogy.core.ui.composites.ApogySystemApiAdapterBasedComposite;
import ca.gc.asc_csa.apogy.core.ui.composites.ApogySystemPoseComposite;

public class VehiculePoseComposite extends ApogySystemApiAdapterBasedComposite 
{
	private TabFolder tabFolder;
	private TabItem tbtmSystemPose;
	private TabItem tbtmMvehiclePoseCorrector;
	
	private ApogySystemPoseComposite apogySystemPoseComposite;
	private VehiclePoseCorrectorComposite vehiclePoseCorrectorComposite;
	private AttitudeIndicatorComposite attitudeIndicatorComposite;
	
	
	
	public VehiculePoseComposite(Composite parent, int style) 
	{
		this(parent, style, null, null, 0.0);
	}
	
	public VehiculePoseComposite(Composite parent, int style, VariablesList variables, VariableFeatureReference variableFeatureReference, double tripDistance) 
	{
		super(parent, style);		
		setLayout(new GridLayout(1, false));
		
		tabFolder = new TabFolder(this, SWT.BORDER);
		GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1);
		gd_tabFolder.minimumWidth = 450;
		gd_tabFolder.widthHint = 450;
		tabFolder.setLayoutData(gd_tabFolder);
		
		tbtmSystemPose = new TabItem(tabFolder, SWT.NONE);		
		tbtmSystemPose.setText("System Pose");
				
		// TAB Vehicle Pose	
		Group grVehiculePose = new Group(tabFolder, SWT.BORDER);	
		tbtmSystemPose.setControl(grVehiculePose);
		grVehiculePose.setLayout(new GridLayout(1, false));
		grVehiculePose.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		grVehiculePose.setText("Vehicle Pose");
		
		apogySystemPoseComposite = new ApogySystemPoseComposite(grVehiculePose, SWT.NONE, variables, variableFeatureReference, tripDistance);
		apogySystemPoseComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));		
		
		tbtmMvehiclePoseCorrector = new TabItem(tabFolder, SWT.NONE);		
		tbtmMvehiclePoseCorrector.setText("Pose Corrector");

		// TAB Pose Corrector
		Group grVehiculePoseCorrector = new Group(tabFolder, SWT.BORDER);		
		tbtmMvehiclePoseCorrector.setControl(grVehiculePoseCorrector);
		grVehiculePoseCorrector.setLayout(new GridLayout(1, false));
		grVehiculePoseCorrector.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false));
		grVehiculePoseCorrector.setText("Pose Corrector");
		
		vehiclePoseCorrectorComposite = new VehiclePoseCorrectorComposite(grVehiculePoseCorrector, SWT.NONE);
		vehiclePoseCorrectorComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));		
		
		// Attitude Display
		Group grAttitude = new Group(this, SWT.BORDER);		
		grAttitude.setLayout(new GridLayout(1, false));
		GridData gdAttitude = new GridData(SWT.LEFT, SWT.FILL, false, false);
		gdAttitude.verticalSpan = 1;
		grAttitude.setLayoutData(gdAttitude);
		grAttitude.setText("Attitude");
		
		attitudeIndicatorComposite = new AttitudeIndicatorComposite(grAttitude, SWT.NONE, 450, 450);
		attitudeIndicatorComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));		
		attitudeIndicatorComposite.setVariableFeatureReference(variableFeatureReference);				
		attitudeIndicatorComposite.setAttitude(0, 0);	
		
		setVariableFeatureReference(variableFeatureReference);
	}

	@Override
	public void dispose() 
	{
		super.dispose();
	}

	public double getTripDistance() 
	{
		if(apogySystemPoseComposite != null && !apogySystemPoseComposite.isDisposed())
		{
			return apogySystemPoseComposite.getTripDistance();
		}
		else
		{
			return 0;
		}
	}

	public void setTripDistance(double tripDistance) 
	{
		if(apogySystemPoseComposite != null && !apogySystemPoseComposite.isDisposed())
		{
			apogySystemPoseComposite.setTripDistance(tripDistance);
		}
	}
	
	public void setVariableList(final VariablesList variables) 
	{
		if(apogySystemPoseComposite != null && !apogySystemPoseComposite.isDisposed())
		{
			apogySystemPoseComposite.setVariableList(variables);
		}
	}
		
	public AttitudeIndicatorComposite getAttitudeIndicatorComposite()
	{
		return attitudeIndicatorComposite;
	}
	
	@Override
	public void setVariableFeatureReference(VariableFeatureReference variableFeatureReference) 
	{
		super.setVariableFeatureReference(variableFeatureReference);
		
		if(apogySystemPoseComposite != null && !apogySystemPoseComposite.isDisposed())
		{
			apogySystemPoseComposite.setVariableFeatureReference(variableFeatureReference);
		}
		
		if(attitudeIndicatorComposite != null && !attitudeIndicatorComposite.isDisposed())
		{
			attitudeIndicatorComposite.setVariableFeatureReference(variableFeatureReference);
		}	
	}
	
	@Override
	protected void apogySystemApiAdapterChanged(ApogySystemApiAdapter oldApogySystemApiAdapter, ApogySystemApiAdapter newApogySystemApiAdapter) 
	{
		if(newApogySystemApiAdapter != null)
		{
			if(newApogySystemApiAdapter.getPoseCorrector() instanceof VehiclePoseCorrector)
			{
				vehiclePoseCorrectorComposite.setVehiclePoseCorrector((VehiclePoseCorrector) newApogySystemApiAdapter.getPoseCorrector());
			}
			else
			{
				vehiclePoseCorrectorComposite.setVehiclePoseCorrector(null);
			}
		}
		else
		{
			vehiclePoseCorrectorComposite.setVehiclePoseCorrector(null);
		}
	}
}
