package ca.gc.asc_csa.apogy.addons.vehicle.ui.composites;

import java.text.DecimalFormat;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class AttitudeIndicatorSettingComposite extends Composite 
{
	private AttitudeIndicatorComposite attitudeIndicatorComposite;
	
	private DecimalFormat decimalFormat = new DecimalFormat("0.0");
	
	private Text txtRollWarningMinLimit;
	private Text txtRollWarningMaxLimit;
	private Text txtRollAlarmMinLimit;
	private Text txtRollAlarmMaxLimit;
	private Text txtPitchWarningMinLimit;
	private Text txtPitchWarningMaxLimit;
	private Text txtPitchAlarmMinLimit;
	private Text txtPitchAlarmMaxLimit;

	public AttitudeIndicatorSettingComposite(Composite parent, int style, AttitudeIndicatorComposite attitudeIndicatorComposite) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		this.attitudeIndicatorComposite = attitudeIndicatorComposite;
		
		Group grpPitchLimits = new Group(this, SWT.NONE);
		grpPitchLimits.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpPitchLimits.setText("Pitch Limits");
		grpPitchLimits.setLayout(new GridLayout(2, false));
		
		Group grpPitchWarningLimits = new Group(grpPitchLimits, SWT.NONE);
		grpPitchWarningLimits.setLayout(new GridLayout(2, false));
		grpPitchWarningLimits.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpPitchWarningLimits.setText("Warning Limits");
		
		Label lblMinimumdeg_1 = new Label(grpPitchWarningLimits, SWT.NONE);
		lblMinimumdeg_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMinimumdeg_1.setText("Minimum (deg):");
		
		txtPitchWarningMinLimit = new Text(grpPitchWarningLimits, SWT.BORDER);
		GridData gd_txtPitchWarningMinLimit = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtPitchWarningMinLimit.minimumWidth = 100;
		gd_txtPitchWarningMinLimit.widthHint = 100;
		txtPitchWarningMinLimit.setLayoutData(gd_txtPitchWarningMinLimit);
		
		Label lblMaximumdeg_2 = new Label(grpPitchWarningLimits, SWT.NONE);
		lblMaximumdeg_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMaximumdeg_2.setText("Maximum (deg):");
		
		txtPitchWarningMaxLimit = new Text(grpPitchWarningLimits, SWT.BORDER);
		GridData gd_txtPitchWarningMaxLimit = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtPitchWarningMaxLimit.widthHint = 100;
		gd_txtPitchWarningMaxLimit.minimumHeight = 100;
		txtPitchWarningMaxLimit.setLayoutData(gd_txtPitchWarningMaxLimit);
		
		Group grpPitchAlarmLimits = new Group(grpPitchLimits, SWT.NONE);
		grpPitchAlarmLimits.setLayout(new GridLayout(2, false));
		grpPitchAlarmLimits.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpPitchAlarmLimits.setText("Alarm Limits");
		
		Label lblMinimumdeg_2 = new Label(grpPitchAlarmLimits, SWT.NONE);
		lblMinimumdeg_2.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMinimumdeg_2.setText("Minimum (deg):");
		
		txtPitchAlarmMinLimit = new Text(grpPitchAlarmLimits, SWT.BORDER);
		GridData gd_txtPitchAlarmMinLimit = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtPitchAlarmMinLimit.minimumWidth = 100;
		gd_txtPitchAlarmMinLimit.widthHint = 100;
		txtPitchAlarmMinLimit.setLayoutData(gd_txtPitchAlarmMinLimit);
		
		Label lblMaximumdeg_3 = new Label(grpPitchAlarmLimits, SWT.NONE);
		lblMaximumdeg_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMaximumdeg_3.setText("Maximum (deg):");
		
		txtPitchAlarmMaxLimit = new Text(grpPitchAlarmLimits, SWT.BORDER);
		GridData gd_txtPitchAlarmMaxLimit = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtPitchAlarmMaxLimit.widthHint = 100;
		gd_txtPitchAlarmMaxLimit.minimumWidth = 100;
		txtPitchAlarmMaxLimit.setLayoutData(gd_txtPitchAlarmMaxLimit);
		
		Group grpRollLimits = new Group(this, SWT.NONE);
		grpRollLimits.setText("Roll Limits");
		grpRollLimits.setLayout(new GridLayout(2, false));
		
		Group grpRollWarningLimits = new Group(grpRollLimits, SWT.NONE);
		grpRollWarningLimits.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpRollWarningLimits.setText("Warning Limits");
		grpRollWarningLimits.setLayout(new GridLayout(2, false));
		
		Label lblMinnimudeg = new Label(grpRollWarningLimits, SWT.NONE);
		lblMinnimudeg.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMinnimudeg.setText("Minimum (deg):");
		
		txtRollWarningMinLimit = new Text(grpRollWarningLimits, SWT.BORDER);
		GridData gd_txtRollWarningMinLimit = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtRollWarningMinLimit.minimumWidth = 100;
		gd_txtRollWarningMinLimit.widthHint = 100;
		txtRollWarningMinLimit.setLayoutData(gd_txtRollWarningMinLimit);
		
		Label lblMaximumdeg = new Label(grpRollWarningLimits, SWT.NONE);
		lblMaximumdeg.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMaximumdeg.setText("Maximum (deg):");
		
		txtRollWarningMaxLimit = new Text(grpRollWarningLimits, SWT.BORDER);
		GridData gd_txtRollWarningMaxLimit = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtRollWarningMaxLimit.widthHint = 100;
		gd_txtRollWarningMaxLimit.minimumWidth = 100;
		txtRollWarningMaxLimit.setLayoutData(gd_txtRollWarningMaxLimit);
		
		Group grpRollAlarmLimits = new Group(grpRollLimits, SWT.NONE);
		grpRollAlarmLimits.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		grpRollAlarmLimits.setText("Alarm Limits");
		grpRollAlarmLimits.setLayout(new GridLayout(2, false));
		
		Label lblMinimumdeg = new Label(grpRollAlarmLimits, SWT.NONE);
		lblMinimumdeg.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMinimumdeg.setText("Minimum (deg):");
		
		txtRollAlarmMinLimit = new Text(grpRollAlarmLimits, SWT.BORDER);
		GridData gd_txtRollAlarmMinLimit = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtRollAlarmMinLimit.minimumWidth = 100;
		gd_txtRollAlarmMinLimit.widthHint = 100;
		txtRollAlarmMinLimit.setLayoutData(gd_txtRollAlarmMinLimit);
		
		Label lblMaximumdeg_1 = new Label(grpRollAlarmLimits, SWT.NONE);
		lblMaximumdeg_1.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblMaximumdeg_1.setText("Maximum (deg):");
		
		txtRollAlarmMaxLimit = new Text(grpRollAlarmLimits, SWT.BORDER);
		GridData gd_txtRollAlarmMaxLimit = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtRollAlarmMaxLimit.widthHint = 100;
		gd_txtRollAlarmMaxLimit.minimumWidth = 100;
		txtRollAlarmMaxLimit.setLayoutData(gd_txtRollAlarmMaxLimit);
		
		// Sets initial values.
		initializaDisplayedValues();
	}

	public AttitudeIndicatorComposite getAttitudeIndicatorComposite() {
		return attitudeIndicatorComposite;
	}

	public void setAttitudeIndicatorComposite(AttitudeIndicatorComposite attitudeIndicatorComposite) 
	{
		this.attitudeIndicatorComposite = attitudeIndicatorComposite;
		initializaDisplayedValues();
	}
	
	public void applySettings()
	{
		if(getAttitudeIndicatorComposite() != null)
		{
			// Pitch Alarm Values.
			getAttitudeIndicatorComposite().setPitchAlarmValues(Math.toRadians(Double.parseDouble(txtPitchAlarmMinLimit.getText())), Math.toRadians(Double.parseDouble(txtPitchAlarmMaxLimit.getText())));
			
			// Pitch Warning Values.
			getAttitudeIndicatorComposite().setPitchWarningValues(Math.toRadians(Double.parseDouble(txtPitchWarningMinLimit.getText())), Math.toRadians(Double.parseDouble(txtPitchWarningMaxLimit.getText())));
			
			// Roll Alarm Values.
			getAttitudeIndicatorComposite().setRollAlarmValues(Math.toRadians(Double.parseDouble(txtRollAlarmMinLimit.getText())), Math.toRadians(Double.parseDouble(txtRollAlarmMaxLimit.getText())));

			// Roll Warning Values.
			getAttitudeIndicatorComposite().setRollWarningValues(Math.toRadians(Double.parseDouble(txtRollWarningMinLimit.getText())), Math.toRadians(Double.parseDouble(txtRollWarningMaxLimit.getText())));			
		}
	}
	
	private void initializaDisplayedValues()
	{
		if(attitudeIndicatorComposite != null)
		{
			txtPitchWarningMinLimit.setText(decimalFormat.format(Math.toDegrees(this.attitudeIndicatorComposite.getPitchWarningMinLimit())));
			txtPitchWarningMaxLimit.setText(decimalFormat.format(Math.toDegrees(this.attitudeIndicatorComposite.getPitchWarningMaxLimit())));
			txtPitchAlarmMinLimit.setText(decimalFormat.format(Math.toDegrees(this.attitudeIndicatorComposite.getPitchAlarmMinLimit())));
			txtPitchAlarmMaxLimit.setText(decimalFormat.format(Math.toDegrees(this.attitudeIndicatorComposite.getPitchAlarmMaxLimit())));
			
			txtRollWarningMinLimit.setText(decimalFormat.format(Math.toDegrees(this.attitudeIndicatorComposite.getRollWarningMinLimit())));
			txtRollWarningMaxLimit.setText(decimalFormat.format(Math.toDegrees(this.attitudeIndicatorComposite.getRollWarningMaxLimit())));
			txtRollAlarmMinLimit.setText(decimalFormat.format(Math.toDegrees(this.attitudeIndicatorComposite.getRollAlarmMinLimit())));
			txtRollAlarmMaxLimit.setText(decimalFormat.format(Math.toDegrees(this.attitudeIndicatorComposite.getRollAlarmMaxLimit())));	
		}
	}
}
