package ca.gc.asc_csa.apogy.addons.vehicle.ui.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.addons.vehicle.ui.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIFactory;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.LanderSphericalFootPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.PathPlannerToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.PhysicalWheelPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterPresentation;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.VehiclePathPlannerToolWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyAddonsVehicleUIFactoryImpl extends EFactoryImpl implements ApogyAddonsVehicleUIFactory
{
  /**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public static ApogyAddonsVehicleUIFactory init()
  {
		try {
			ApogyAddonsVehicleUIFactory theApogyAddonsVehicleUIFactory = (ApogyAddonsVehicleUIFactory)EPackage.Registry.INSTANCE.getEFactory(ApogyAddonsVehicleUIPackage.eNS_URI);
			if (theApogyAddonsVehicleUIFactory != null) {
				return theApogyAddonsVehicleUIFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ApogyAddonsVehicleUIFactoryImpl();
	}

  /**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ApogyAddonsVehicleUIFactoryImpl()
  {
		super();
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  @Override
  public EObject create(EClass eClass)
  {
		switch (eClass.getClassifierID()) {
			case ApogyAddonsVehicleUIPackage.PHYSICAL_WHEEL_PRESENTATION: return createPhysicalWheelPresentation();
			case ApogyAddonsVehicleUIPackage.LANDER_SPHERICAL_FOOT_PRESENTATION: return createLanderSphericalFootPresentation();
			case ApogyAddonsVehicleUIPackage.THRUSTER_PRESENTATION: return createThrusterPresentation();
			case ApogyAddonsVehicleUIPackage.WHEEL_WIZARD_PAGES_PROVIDER: return createWheelWizardPagesProvider();
			case ApogyAddonsVehicleUIPackage.LANDER_SPHERICAL_FOOT_PAGES_PROVIDER: return createLanderSphericalFootPagesProvider();
			case ApogyAddonsVehicleUIPackage.THRUSTER_WIZARD_PAGES_PROVIDER: return createThrusterWizardPagesProvider();
			case ApogyAddonsVehicleUIPackage.PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER: return createPathPlannerToolWizardPagesProvider();
			case ApogyAddonsVehicleUIPackage.VEHICLE_PATH_PLANNER_TOOL_WIZARD_PAGES_PROVIDER: return createVehiclePathPlannerToolWizardPagesProvider();
			case ApogyAddonsVehicleUIPackage.VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER: return createVehicleTrajectoryPickingToolWizardPagesProvider();
			case ApogyAddonsVehicleUIPackage.THRUSTER_BINDING_WIZARD_PAGES_PROVIDER: return createThrusterBindingWizardPagesProvider();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public PhysicalWheelPresentation createPhysicalWheelPresentation()
  {
		PhysicalWheelPresentationImpl physicalWheelPresentation = new PhysicalWheelPresentationImpl();
		return physicalWheelPresentation;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public LanderSphericalFootPresentation createLanderSphericalFootPresentation()
  {
		LanderSphericalFootPresentationImpl landerSphericalFootPresentation = new LanderSphericalFootPresentationImpl();
		return landerSphericalFootPresentation;
	}

  /**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @generated
	 */
  public ThrusterPresentation createThrusterPresentation()
  {
		ThrusterPresentationImpl thrusterPresentation = new ThrusterPresentationImpl();
		return thrusterPresentation;
	}

  /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WheelWizardPagesProvider createWheelWizardPagesProvider() {
		WheelWizardPagesProviderImpl wheelWizardPagesProvider = new WheelWizardPagesProviderImpl();
		return wheelWizardPagesProvider;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LanderSphericalFootPagesProvider createLanderSphericalFootPagesProvider() {
		LanderSphericalFootPagesProviderImpl landerSphericalFootPagesProvider = new LanderSphericalFootPagesProviderImpl();
		return landerSphericalFootPagesProvider;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThrusterWizardPagesProvider createThrusterWizardPagesProvider() {
		ThrusterWizardPagesProviderImpl thrusterWizardPagesProvider = new ThrusterWizardPagesProviderImpl();
		return thrusterWizardPagesProvider;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public PathPlannerToolWizardPagesProvider createPathPlannerToolWizardPagesProvider() {
		PathPlannerToolWizardPagesProviderImpl pathPlannerToolWizardPagesProvider = new PathPlannerToolWizardPagesProviderImpl();
		return pathPlannerToolWizardPagesProvider;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public VehiclePathPlannerToolWizardPagesProvider createVehiclePathPlannerToolWizardPagesProvider() {
		VehiclePathPlannerToolWizardPagesProviderImpl vehiclePathPlannerToolWizardPagesProvider = new VehiclePathPlannerToolWizardPagesProviderImpl();
		return vehiclePathPlannerToolWizardPagesProvider;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public VehicleTrajectoryPickingToolWizardPagesProvider createVehicleTrajectoryPickingToolWizardPagesProvider() {
		VehicleTrajectoryPickingToolWizardPagesProviderImpl vehicleTrajectoryPickingToolWizardPagesProvider = new VehicleTrajectoryPickingToolWizardPagesProviderImpl();
		return vehicleTrajectoryPickingToolWizardPagesProvider;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ThrusterBindingWizardPagesProvider createThrusterBindingWizardPagesProvider() {
		ThrusterBindingWizardPagesProviderImpl thrusterBindingWizardPagesProvider = new ThrusterBindingWizardPagesProviderImpl();
		return thrusterBindingWizardPagesProvider;
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyAddonsVehicleUIPackage getApogyAddonsVehicleUIPackage() {
		return (ApogyAddonsVehicleUIPackage)getEPackage();
	}

		/**
	 * <!-- begin-user-doc -->
   * <!-- end-user-doc -->	 * @deprecated
	 * @generated
	 */
  @Deprecated
  public static ApogyAddonsVehicleUIPackage getPackage()
  {
		return ApogyAddonsVehicleUIPackage.eINSTANCE;
	}

} //ApogyAddonsVehicleUIFactoryImpl
