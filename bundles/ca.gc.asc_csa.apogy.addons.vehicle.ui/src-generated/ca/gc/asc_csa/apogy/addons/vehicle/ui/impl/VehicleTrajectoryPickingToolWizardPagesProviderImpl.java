/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.vehicle.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.geometry.paths.ApogyAddonsGeometryPathsFactory;
import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath;
import ca.gc.asc_csa.apogy.addons.ui.AbstractToolEClassSettings;
import ca.gc.asc_csa.apogy.addons.ui.impl.TrajectoryPickingToolWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehicleFactory;
import ca.gc.asc_csa.apogy.addons.vehicle.VehicleTrajectoryPickingTool;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.VehicleTrajectoryPickingToolWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards.VehicleTrajectoryPickingToolWizardPage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vehicle Trajectory Picking Tool Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VehicleTrajectoryPickingToolWizardPagesProviderImpl extends TrajectoryPickingToolWizardPagesProviderImpl implements VehicleTrajectoryPickingToolWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VehicleTrajectoryPickingToolWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsVehicleUIPackage.Literals.VEHICLE_TRAJECTORY_PICKING_TOOL_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		VehicleTrajectoryPickingTool tool = ApogyAddonsVehicleFactory.eINSTANCE.createVehicleTrajectoryPickingTool();
		WayPointPath path = ApogyAddonsGeometryPathsFactory.eINSTANCE.createWayPointPath();
		
		tool.getPaths().add(path);
		tool.setActivePath(path);
		tool.setAltitudeOffset(0.0);		
		
		if(settings instanceof AbstractToolEClassSettings)
		{
			AbstractToolEClassSettings abstractToolEClassSettings = (AbstractToolEClassSettings) settings;
			tool.setName(abstractToolEClassSettings.getName());
			tool.setDescription(abstractToolEClassSettings.getDescription());
		}
		
		return tool;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();
		list.addAll(super.instantiateWizardPages(eObject, settings));

		VehicleTrajectoryPickingTool tool = (VehicleTrajectoryPickingTool) eObject;		
		
		list.add(new VehicleTrajectoryPickingToolWizardPage(tool));

		return list;
	}
} //VehicleTrajectoryPickingToolWizardPagesProviderImpl
