/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.vehicle.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehicleFactory;
import ca.gc.asc_csa.apogy.addons.vehicle.Wheel;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.WheelWizardPagesProvider;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.wizards.WheelWizardPage;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.topology.ui.impl.NodeWizardPagesProviderImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Wheel Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WheelWizardPagesProviderImpl extends NodeWizardPagesProviderImpl implements WheelWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WheelWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsVehicleUIPackage.Literals.WHEEL_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		Wheel wheel = ApogyAddonsVehicleFactory.eINSTANCE.createWheel();
		wheel.setRadius(0.5);
		wheel.setLength(0.1);
		return wheel;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		Wheel wheel = (Wheel) eObject;
		
		// Add Wheel page.
		list.add(new WheelWizardPage(wheel));
		
		return list;
	}
} //WheelWizardPagesProviderImpl
