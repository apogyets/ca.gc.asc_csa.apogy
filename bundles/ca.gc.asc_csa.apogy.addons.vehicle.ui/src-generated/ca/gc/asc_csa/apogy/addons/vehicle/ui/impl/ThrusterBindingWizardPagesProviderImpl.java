/**
 * Copyright (c) 2015-2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.vehicle.ui.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehicleFactory;
import ca.gc.asc_csa.apogy.addons.vehicle.ApogyAddonsVehiclePackage;
import ca.gc.asc_csa.apogy.addons.vehicle.Thruster;
import ca.gc.asc_csa.apogy.addons.vehicle.ThrusterBinding;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ApogyAddonsVehicleUIPackage;
import ca.gc.asc_csa.apogy.addons.vehicle.ui.ThrusterBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.AbstractTopologyBindingWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.TopologyNodeSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Thruster Binding Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ThrusterBindingWizardPagesProviderImpl extends AbstractTopologyBindingWizardPagesProviderImpl implements ThrusterBindingWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThrusterBindingWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsVehicleUIPackage.Literals.THRUSTER_BINDING_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		ThrusterBinding thrusterBinding = ApogyAddonsVehicleFactory.eINSTANCE.createThrusterBinding();		
		return thrusterBinding;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
		
		// Add topology node selection page.
		List<Node> nodes = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			nodes = (List<Node>) mapBasedEClassSettings.getUserDataMap().get(TopologyUIBindingsConstants.NODES_LIST_USER_ID);
		}
		
		ThrusterBinding binding = (ThrusterBinding) eObject;		
		
		TopologyNodeSelectionWizardPage topologyNodeSelectionWizardPage = new TopologyNodeSelectionWizardPage("Select Thruster", 
				"Select the Thruster that this binding is controlling.", 
				binding, 
				ApogyAddonsVehiclePackage.Literals.THRUSTER, 
				nodes) 
		{	
			@Override
			protected void nodeSelected(Node nodeSelected) 
			{
				if(binding != null && nodeSelected instanceof Thruster)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((ThrusterBinding) binding, ApogyAddonsVehiclePackage.Literals.THRUSTER_BINDING__THRUSTER, (Thruster) nodeSelected, true);
					
					setMessage("Node selected : " + nodeSelected.getNodeId());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("The selected Node is not a Thruster !");
				}
			}
		};
		
		list.add(topologyNodeSelectionWizardPage);
		
		return list;
	}
} //ThrusterBindingWizardPagesProviderImpl
