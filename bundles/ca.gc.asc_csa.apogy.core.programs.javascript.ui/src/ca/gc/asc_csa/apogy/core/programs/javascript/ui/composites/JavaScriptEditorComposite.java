package ca.gc.asc_csa.apogy.core.programs.javascript.ui.composites;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram;
import ca.gc.asc_csa.apogy.workspace.ApogyWorkspaceFacade;

public class JavaScriptEditorComposite extends Composite {

	private JavaScriptProgram javaScriptProgram;
	private Text scriptPath;
	private Text scriptPreview;
	private Button saveButton;
	private Button reloadButton;
	private Button browseButton;
	
	private DataBindingContext bindingContext;
	
	private Adapter javaScriptProgramAdapter;


	public JavaScriptEditorComposite(Composite parent, int style) 
	{
		super(parent, style);		
	}

	public JavaScriptEditorComposite(Composite parent, int style, JavaScriptProgram javaScriptProgram) 
	{
		this(parent, style);
				
		this.setLayout(new GridLayout(3, false));					
		
		// Script Path
		Label scriptPathLabel = new Label(this, SWT.NONE);
		scriptPathLabel.setText("Script Path : ");
		
		scriptPath = new Text(this, SWT.None);	
		scriptPath.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));				
		
		browseButton =  new Button(this, SWT.PUSH);
		browseButton.setText("Browse..."); 
		browseButton.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false));
		browseButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{
				FileDialog fd = new FileDialog(getShell(), SWT.OPEN);
		        fd.setText("Select Javascript File");
		        
		        String relativePath  = ApogyWorkspaceFacade.INSTANCE.getActiveProject().getFullPath().toOSString() + File.separator + ApogyWorkspaceFacade.INSTANCE.getDefaultProgramsFolderName() + File.separator;			        
		        IPath workspacePath  = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		        IPath absolutePath   = workspacePath.append(relativePath);
		        String absolutePathString = absolutePath.toOSString();		        
		        fd.setFilterPath(absolutePathString);
		        
		        String[] filterExt = { "*.js" };
		        fd.setFilterExtensions(filterExt);
		        
		        String selected = fd.open();
		        if(selected != null)
		        {
		        	try
		        	{
		        		// Keeps what is after the workspace location,
		        		String path = relativePath + selected.substring(selected.lastIndexOf(File.separator) + 1);
		        		
		        		ApogyCommonTransactionFacade.INSTANCE.basicSet(javaScriptProgram, ApogyCoreJavaScriptProgramsPackage.Literals.JAVA_SCRIPT_PROGRAM__SCRIPT_PATH, path, true);
		        	}
		        	catch (Throwable t) 
		        	{
						t.printStackTrace();
					}
		        } 		        
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
			}
		});
		
		reloadButton = new Button(this, SWT.PUSH);
		reloadButton.setText("Reload");
		reloadButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				try 
				{
					scriptPreview.setText(load(javaScriptProgram));
					if(saveButton != null && !saveButton.isDisposed())
					{
						saveButton.setEnabled(false);
					}
				} 
				catch (Exception e) 
				{				
					e.printStackTrace();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) 
			{	
			}
		});
				
		saveButton = new Button(this, SWT.PUSH);
		saveButton.setEnabled(false);
		saveButton.setText("Save");
		saveButton.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) 
			{				
				try 
				{
					save(scriptPreview.getText(), javaScriptProgram);
					if(saveButton != null && !saveButton.isDisposed())
					{
						saveButton.setEnabled(false);
					}
				} 
				catch (Exception e) 
				{				
					e.printStackTrace();
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) 
			{							
			}
		});
							
		// Filler
		new Label(this, SWT.NONE);
		
		// Script text.
		scriptPreview = new Text(this, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scriptPreview.setEditable(true);		
		GridData gd_scriptPreview = new GridData(SWT.FILL, SWT.TOP, true, false, 3,1);
		gd_scriptPreview.minimumHeight = 250;
		gd_scriptPreview.heightHint = 250;
		scriptPreview.setLayoutData(gd_scriptPreview);
		
		// Loads and displays script.
		try 
		{			
			scriptPreview.setText(load(javaScriptProgram));
		} 
		catch (Exception e) 
		{		
			e.printStackTrace();
		}	
		
		setJavaScriptProgram(javaScriptProgram);
		
		scriptPreview.addModifyListener(new ModifyListener() 
		{		
			@Override
			public void modifyText(ModifyEvent arg0) 
			{				
				if(saveButton != null && !saveButton.isDisposed())
				{
					saveButton.setEnabled(true);
				}
				
				if(reloadButton != null && !reloadButton.isDisposed())
				{
					reloadButton.setEnabled(true);
				}
			}
		});			
		
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent arg0) 
			{
				// Unregister from JavaScriptProgram.
				if(javaScriptProgram != null)
				{
					javaScriptProgram.eAdapters().remove(getJavaScriptProgramAdapter());
				}
				
				if(bindingContext != null){
					bindingContext.dispose();
				}
			}
		});
	}
	
	
	public JavaScriptProgram getJavaScriptProgram() {
		return javaScriptProgram;
	}

	public void setJavaScriptProgram(final JavaScriptProgram javaScriptProgram) 
	{
		getDisplay().asyncExec(new Runnable() {
			
			@Override
			public void run() 
			{	
				if(bindingContext != null)
				{
					bindingContext.dispose();
					bindingContext = null;
				}
				
				if(JavaScriptEditorComposite.this.javaScriptProgram != null)
				{
					javaScriptProgram.eAdapters().remove(getJavaScriptProgramAdapter());
					scriptPreview.setText("");
					saveButton.setEnabled(false);
					reloadButton.setEnabled(false);
				}
								
				JavaScriptEditorComposite.this.javaScriptProgram = javaScriptProgram;
				
				if(javaScriptProgram != null)
				{
					bindingContext = customInitDataBindings();
					
					javaScriptProgram.eAdapters().add(getJavaScriptProgramAdapter());
					
					try 
					{
						scriptPreview.setText(load(javaScriptProgram));
						saveButton.setEnabled(false);
					} 
					catch (Exception e) 
					{			
						e.printStackTrace();
					}
				}
				else
				{
					scriptPreview.setText("");
				}
			}
		});
		
	}

	protected String load(JavaScriptProgram javaScriptProgram) throws Exception
	{
		IPath absolutePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(javaScriptProgram.getScriptPath());						
		BufferedReader in = new BufferedReader(new FileReader(absolutePath.toOSString()));
		String line = null;
		StringBuffer sb = new StringBuffer();
		while ((line = in.readLine()) != null) 
		{
			sb.append(line + "\n");
		}
		in.close();		
		
		return sb.toString();
	}
	
	
	protected void save(String text, JavaScriptProgram javaScriptProgram) throws Exception
	{
		IPath absolutePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(javaScriptProgram.getScriptPath());						
		BufferedWriter out = new BufferedWriter(new FileWriter(absolutePath.toOSString()));
		
		out.write(text);
		out.close();		
	}
	
	@SuppressWarnings("unchecked")
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<String> observeScriptPath = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getDefaultEditingDomain(), 
				  										FeaturePath.fromList(ApogyCoreJavaScriptProgramsPackage.Literals.JAVA_SCRIPT_PROGRAM__SCRIPT_PATH)).observe(getJavaScriptProgram());
		
		IObservableValue<String> observeScriptPathTxt = WidgetProperties.text(SWT.Modify).observe(scriptPath);

		bindingContext.bindValue(observeScriptPathTxt, 
								 observeScriptPath,
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		return bindingContext;
	}
	
	private Adapter getJavaScriptProgramAdapter() 
	{
		if(javaScriptProgramAdapter == null)
		{
			javaScriptProgramAdapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					if(msg.getNotifier() instanceof JavaScriptProgram)
					{
						int featureId = msg.getFeatureID(JavaScriptProgram.class);
						
						if(featureId == ApogyCoreJavaScriptProgramsPackage.JAVA_SCRIPT_PROGRAM__SCRIPT_PATH)
						{
							try 
							{
								// Forces a reload.
								setJavaScriptProgram(getJavaScriptProgram());
							} 
							catch (Exception e) 
							{			
								e.printStackTrace();
							}
						}
					}
				}
			};
		}
		return javaScriptProgramAdapter;
	}
}
