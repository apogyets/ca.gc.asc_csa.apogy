/**
 * Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.core.invocator.Program;
import ca.gc.asc_csa.apogy.core.invocator.ui.impl.ProgramUIFactoryImpl;
import ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.JavaScriptProgramUIFactory;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.composites.JavaScriptEditorComposite;

// import org.eclipse.wst.jsdt.internal.ui.javaeditor.JavaEditor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Script Program UI Factory</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JavaScriptProgramUIFactoryImpl extends ProgramUIFactoryImpl implements JavaScriptProgramUIFactory {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaScriptProgramUIFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreJavaScriptProgramsUIPackage.Literals.JAVA_SCRIPT_PROGRAM_UI_FACTORY;
	}

	
	@Override
	public Composite createProgramComposite(Composite parent, Program program) 
	{
		JavaScriptProgram javaScriptProgram = (JavaScriptProgram) program;					
		JavaScriptEditorComposite editor = new JavaScriptEditorComposite(parent, SWT.BORDER, javaScriptProgram);				
		return editor;
	}	
} //JavaScriptProgramUIFactoryImpl
