/**
 * Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.ui;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;

import ca.gc.asc_csa.apogy.core.invocator.ui.ApogyCoreInvocatorUIPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Copyright (c) 2017 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCoreJavaScriptProgramsUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)' modelName='ApogyCoreJavaScriptProgramsUI' suppressGenModelAnnotations='false' publicConstructors='true' modelDirectory='/ca.gc.asc_csa.apogy.core.programs.javascript.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.core.programs.javascript.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.core.programs.javascript'"
 * @generated
 */
public interface ApogyCoreJavaScriptProgramsUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.core.programs.javascript.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCoreJavaScriptProgramsUIPackage eINSTANCE = ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.ApogyCoreJavaScriptProgramsUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.JavaScriptProgramUIFactoryImpl <em>Java Script Program UI Factory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.JavaScriptProgramUIFactoryImpl
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.ApogyCoreJavaScriptProgramsUIPackageImpl#getJavaScriptProgramUIFactory()
	 * @generated
	 */
	int JAVA_SCRIPT_PROGRAM_UI_FACTORY = 0;

	/**
	 * The number of structural features of the '<em>Java Script Program UI Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_UI_FACTORY_FEATURE_COUNT = ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORY_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Create Program Composite</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_UI_FACTORY___CREATE_PROGRAM_COMPOSITE__COMPOSITE_PROGRAM_ISELECTIONLISTENER = ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORY___CREATE_PROGRAM_COMPOSITE__COMPOSITE_PROGRAM_ISELECTIONLISTENER;

	/**
	 * The number of operations of the '<em>Java Script Program UI Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_UI_FACTORY_OPERATION_COUNT = ApogyCoreInvocatorUIPackage.PROGRAM_UI_FACTORY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.JavaScriptProgramPagesProviderImpl <em>Java Script Program Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.JavaScriptProgramPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.ApogyCoreJavaScriptProgramsUIPackageImpl#getJavaScriptProgramPagesProvider()
	 * @generated
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER = 1;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER__PAGES = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER__EOBJECT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Java Script Program Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Java Script Program Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonEMFUIPackage.NAMED_DESCRIBED_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.programs.javascript.ui.JavaScriptProgramUIFactory <em>Java Script Program UI Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Script Program UI Factory</em>'.
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.JavaScriptProgramUIFactory
	 * @generated
	 */
	EClass getJavaScriptProgramUIFactory();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.core.programs.javascript.ui.JavaScriptProgramPagesProvider <em>Java Script Program Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Script Program Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.JavaScriptProgramPagesProvider
	 * @generated
	 */
	EClass getJavaScriptProgramPagesProvider();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCoreJavaScriptProgramsUIFactory getApogyCoreJavaScriptProgramsUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.JavaScriptProgramUIFactoryImpl <em>Java Script Program UI Factory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.JavaScriptProgramUIFactoryImpl
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.ApogyCoreJavaScriptProgramsUIPackageImpl#getJavaScriptProgramUIFactory()
		 * @generated
		 */
		EClass JAVA_SCRIPT_PROGRAM_UI_FACTORY = eINSTANCE.getJavaScriptProgramUIFactory();
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.JavaScriptProgramPagesProviderImpl <em>Java Script Program Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.JavaScriptProgramPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl.ApogyCoreJavaScriptProgramsUIPackageImpl#getJavaScriptProgramPagesProvider()
		 * @generated
		 */
		EClass JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER = eINSTANCE.getJavaScriptProgramPagesProvider();

	}

} //ApogyCoreJavaScriptProgramsUIPackage
