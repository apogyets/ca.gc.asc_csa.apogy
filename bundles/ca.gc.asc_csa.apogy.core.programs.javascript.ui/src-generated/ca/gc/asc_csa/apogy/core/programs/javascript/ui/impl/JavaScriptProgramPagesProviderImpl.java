/**
 * Canadian Space Agency / Agence spatiale canadienne 2017 Copyrights (c)
 */
package ca.gc.asc_csa.apogy.core.programs.javascript.ui.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyCoreJavaScriptProgramsFactory;
import ca.gc.asc_csa.apogy.core.programs.javascript.ApogyJavaScriptFacade;
import ca.gc.asc_csa.apogy.core.programs.javascript.JavaScriptProgram;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.ApogyCoreJavaScriptProgramsUIPackage;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.JavaScriptProgramPagesProvider;
import ca.gc.asc_csa.apogy.core.programs.javascript.ui.wizards.JavaScriptWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Script Program Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class JavaScriptProgramPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements JavaScriptProgramPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaScriptProgramPagesProviderImpl() 
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() 
	{
		return ApogyCoreJavaScriptProgramsUIPackage.Literals.JAVA_SCRIPT_PROGRAM_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		JavaScriptProgram javaScriptProgram = ApogyCoreJavaScriptProgramsFactory.eINSTANCE.createJavaScriptProgram();					
		return javaScriptProgram;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{				
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		
		list.add(new JavaScriptWizardPage((JavaScriptProgram) eObject));
								
		return list;
	}
	
	@Override
	public CompoundCommand getPerformFinishCommands(EObject eObject, EClassSettings settings, EditingDomain editingDomain) 
	{
		JavaScriptProgram javaScriptProgram = (JavaScriptProgram) eObject;
		IPath absolutePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(javaScriptProgram.getScriptPath());	
		
		try
		{
			BufferedReader in = new BufferedReader(new FileReader(absolutePath.toOSString()));
			in.readLine();
			in.close();		
		}
		catch (Exception e) 
		{
			// File does not exist, create one and populate it.
			try
			{
				populateScript(javaScriptProgram);
			}
			catch (Exception et) 
			{
				et.printStackTrace();
			}
		}
				
		return null;
	}
	
	private void populateScript(JavaScriptProgram javaScriptProgram) throws Exception
	{		
		IPath absolutePath = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(javaScriptProgram.getScriptPath());						
		BufferedWriter out = new BufferedWriter(new FileWriter(absolutePath.toOSString()));
		
		out.write(ApogyJavaScriptFacade.INSTANCE.createJavaScriptCodeTemplate(ApogyCoreInvocatorFacade.INSTANCE.getActiveInvocatorSession(), javaScriptProgram));
		out.close();	
	}
	
} //JavaScriptProgramPagesProviderImpl
