/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.jme3.utils;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Point3d;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.util.BufferUtils;

import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesFactory;
import ca.gc.asc_csa.apogy.common.images.EImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.core.environment.earth.GeographicCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.EarthAtmosphereWorksiteNode;
import ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.jme3.Activator;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.ui.jme3.EarthSurfaceEnvironmentJMEConstants;

public class AtmosphereJME3Utilities 
{	
	private static int wmsImageCount = 0;
	public static double EARTH_RADIUS_METERS = 6371 * 1000;	

	private static Map<EarthAtmosphereWorksiteNode, SurfaceImage> surfaceImageMap = new HashMap<EarthAtmosphereWorksiteNode, SurfaceImage>();
	
	public static double getHorizonAngle(double altitude)
	{
		return -Math.acos(EARTH_RADIUS_METERS / (EARTH_RADIUS_METERS + altitude));
	}
	
	public static double computeHorizonDistance(double altitude)
	{
		double gamma = getHorizonAngle(altitude);
		return Math.abs(gamma) * EARTH_RADIUS_METERS;
	}
	
	public static double computeHorizonRadius(double altitude, double horizonDistance, double altitudecutoff)
	{
		double gamma = getHorizonAngle(altitude);		
		double dx = horizonDistance * Math.cos(gamma);
		double dy = horizonDistance * Math.sin(gamma);
		
		Point2d p1 = new Point2d(-dx, -dy);
		Point2d p2 = new Point2d(0, -altitudecutoff);
		Point2d p3 = new Point2d(dx, -dy);
		
		double mr = (p2.y - p1.y) / (p2.x - p1.x);
		double mt = (p3.y - p2.y) / (p3.x - p2.x);
		
		double x = (mr*mt*(p3.y - p1.y) + mr *(p2.x + p3.x) - mt*(p1.x + p2.x)) / (2*(mr-mt));
	    double y = (p1.y + p2.y)/2 - (x - (p1.x + p2.x)/2) / mr;

	    double radius = Math.sqrt( Math.pow((p2.x -x), 2) +  Math.pow((p2.y-y), 2));
	    
	    return radius;
	}
	
	public static Circle computeHorizonCircle(double altitude, double horizonDistance, double altitudecutoff)
	{
		double gamma = getHorizonAngle(altitude);		
		double dx = horizonDistance * Math.cos(gamma);
		double dy = horizonDistance * Math.sin(gamma);
	
		Point2d p1 = new Point2d(-dx, dy);
		Point2d p2 = null;
		if(altitude <= altitudecutoff)
		{
			p2 = new Point2d(0, -altitude);
		}
		else
		{
			p2 = new Point2d(0, -altitudecutoff);
		}		
		Point2d p3 = new Point2d(dx, dy);
	
		return finCircleThroughPoints(p1,p2,p3);
	}
	
	public static Circle finCircleThroughPoints(Point2d p1,Point2d p2, Point2d p3)
	{
		double mr = (p2.y - p1.y) / (p2.x - p1.x);
		double mt = (p3.y - p2.y) / (p3.x - p2.x);
		
		double xCenter = (mr*mt*(p3.y - p1.y) + mr *(p2.x + p3.x) - mt*(p1.x + p2.x)) / (2*(mr-mt));
	    double yCenter = (p1.y + p2.y)/2 - (xCenter - (p1.x + p2.x)/2) / mr;

	    double radius = Math.sqrt( Math.pow((p2.x -xCenter), 2) +  Math.pow((p2.y-yCenter), 2));
	    
	    return new Circle(radius, xCenter, yCenter);
	}
	
	public static List<Point3d> getCircle(Circle circle)
	{
		List<Point3d> points = new ArrayList<Point3d>();
		
		double angle = 0.0;
		while(angle < Math.PI * 2)
		{
			angle += Math.toRadians(0.1);
			
			double x = circle.xCenter + circle.radius * Math.cos(angle);
			double z = circle.yCenter + circle.radius * Math.sin(angle);
			
			Point3d p = new Point3d(x, 0, z);
			points.add(p);
		}
		
		return points;
	}
	
	public static List<Vector3f> getHyperbola(double a, double b, double d)
	{
		List<Vector3f> points = new ArrayList<Vector3f>();
					
		double x = a;
		double y = 0;
		while(y < d)
		{
			y = (b/a) * Math.sqrt(x*x - a*a);
			Vector3f p = new Vector3f((float) y, 0, (float)-x);
			points.add(p);
			x += 100.0;
		}		
		
		return points;
	}
	
	public static Mesh getParabolaGeometry(List<Vector3f> profile)
	{					
		List<Vector3f> verticesList = new ArrayList<Vector3f>();
		List<Integer> indexesList = new ArrayList<Integer>();	
		List<Vector2f> textureCoordinatesList = new ArrayList<Vector2f>();
		
		double angle = 0;
		double angleIncrement = Math.toRadians(10.0);
		List<Vector3f> previousProfile = profile;
		
		verticesList.addAll(previousProfile);
		
		// double profileLenght = getProfileLenght(profile);
		
		//System.out.println("profileLenght " + profileLenght);
		
		// Creates the texture coordinates.
//		for(Vector3f v : profile)
//		{
//			double distance = getGeodesicDistance(profile, v);
//			
//			double x = distance * Math.cos(angle) + profileLenght;
//			double y = distance * Math.sin(angle) + profileLenght;
//			
//			float textureX = (float) (x/ (2 * profileLenght));
//			float textureY = (float) (y/ (2 * profileLenght));
//			
//			Vector2f textCoord = new Vector2f(textureX, textureY);
//			textureCoordinatesList.add(textCoord);
//		}
		
		for(Vector3f v : profile)
		{
			double distance = Math.sqrt(v.x * v.x + v.y * v.y);
			
			double x = distance * Math.cos(angle) +  EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS;
			double y = distance * Math.sin(angle) +  EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS;
			
			float textureX = (float) (x/ (2 *  EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS));
			float textureY = (float) (y/ (2 *  EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS));
			
			Vector2f textCoord = new Vector2f(textureX, textureY);
			textureCoordinatesList.add(textCoord);
		}
						
		while(angle <= 2 * Math.PI)
		{
			List<Vector3f> rotatedProfile = rotateAroundZ(profile, angle);
			verticesList.addAll(rotatedProfile);						
			
			// Creates the texture coordinates.
//			for(Vector3f v : rotatedProfile)
//			{
//				double distance = getGeodesicDistance(rotatedProfile, v);
//				
//				double x = distance * Math.cos(angle) + profileLenght;
//				double y = distance * Math.sin(angle) + profileLenght;
//				
//				float textureX = (float) (x/ (2 * profileLenght));
//				float textureY = (float) (y/ (2 * profileLenght));
//				
//				Vector2f textCoord = new Vector2f(textureX, textureY);
//				textureCoordinatesList.add(textCoord);
//			}
			
			
			for(Vector3f v : profile)
			{
				double distance = Math.sqrt(v.x * v.x + v.y * v.y);
				
				double x = distance * Math.cos(angle) +  EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS;
				double y = distance * Math.sin(angle) +  EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS;
				
				float textureX = (float) (x/ (2 *  EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS));
				float textureY = (float) (y/ (2 *  EarthSurfaceEnvironmentJMEConstants.AZIMUTH_DISPLAY_RADIUS));
				
				Vector2f textCoord = new Vector2f(textureX, textureY);
				textureCoordinatesList.add(textCoord);
			}
			
			// Meshes the two profiles.
			for(int i = 0; i < rotatedProfile.size() - 1; i++)
			{
				
				int a0 = verticesList.indexOf(previousProfile.get(i));
				int a1 = verticesList.indexOf(previousProfile.get(i+1));
				
				int b0 = verticesList.indexOf(rotatedProfile.get(i));
				int b1 = verticesList.indexOf(rotatedProfile.get(i+1));
				
				// T1
				indexesList.add(new Integer(a0));
				indexesList.add(new Integer(b0));
				indexesList.add(new Integer(b1));
				
				// T2
				indexesList.add(new Integer(a0));
				indexesList.add(new Integer(a1));
				indexesList.add(new Integer(b1));				
			}
						
			// Update the previousProfile.
			previousProfile = rotatedProfile;
			
			angle += angleIncrement;
		}
			
		Mesh mesh = new Mesh();
		mesh.setBuffer( com.jme3.scene.VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(JME3Utilities.convertToFloatArray(verticesList)));
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Index, 3, BufferUtils.createIntBuffer(JME3Utilities.convertToIntArray(indexesList)));
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(JME3Utilities.convertListOfVector2fToFloatArray(textureCoordinatesList)));
		// mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Normal, 3, BufferUtils.createFloatBuffer(JME3Utilities.convertToFloatArray(normalslList)));							
		mesh.updateBound();							
		
		
		return mesh;
	}
	
	public static AbstractEImage getImageFromWMS(EarthAtmosphereWorksiteNode worksiteNode, GeographicCoordinates geographicCoordinates, double bufferDistance) throws Exception
	{		
		/*
		System.out.println("-----------------------------");
		System.out.println("Lat " + Math.toDegrees(geographicCoordinates.getLatitude()));
		System.out.println("Lon " + Math.toDegrees(geographicCoordinates.getLongitude()));
		System.out.println("Ele " + geographicCoordinates.getElevation());
		System.out.println("Cnt " + wmsImageCount);
		System.out.println("-----------------------------");
		*/
		
		// First, check if an image exist for the worksite.
		SurfaceImage surfaceImage = surfaceImageMap.get(worksiteNode);
		
		// If there are no surfaceImage yet, create one.
		if(surfaceImage == null)
		{
			// Attempt to get an image.
			surfaceImage = getSurfaceImage(geographicCoordinates, bufferDistance);
			
			if(surfaceImage != null)
			{
				surfaceImageMap.put(worksiteNode, surfaceImage);
			}			
		}
		else
		{
			// Check if the current position still project on the surface image.
			if(!projectsOntoSurfaceImage(surfaceImage, geographicCoordinates))
			{
				// Attempt to get an image.
				surfaceImage = getSurfaceImage(geographicCoordinates, bufferDistance);
				
				if(surfaceImage != null)
				{
					surfaceImageMap.put(worksiteNode, surfaceImage);
				}	
			}
		}
		
		// ExtractS the portion of the image applicable to the current position.
		if(surfaceImage != null)
		{
			return getSubImage(surfaceImage, geographicCoordinates);
		}		
		
		return null;
	}
	
	private static SurfaceImage getSurfaceImage(GeographicCoordinates geographicCoordinates, double bufferDistance)
	{
		double gamma = Math.abs(getHorizonAngle(geographicCoordinates.getElevation()));
		double bufferAngle = Math.abs(bufferDistance / EARTH_RADIUS_METERS);
		
		double deltaLatitudeAngle = gamma + bufferAngle;
		double deltaLongitudeAngle = gamma + bufferAngle;
		
		double minLatitude = geographicCoordinates.getLatitude() - deltaLatitudeAngle;
		double maxLatitude = geographicCoordinates.getLatitude() + deltaLatitudeAngle;
		
		if(minLatitude > Math.toRadians(90)) minLatitude = Math.toRadians(90);
		if(minLatitude < Math.toRadians(-90)) minLatitude = Math.toRadians(-90);
		
		if(maxLatitude > Math.toRadians(90)) maxLatitude = Math.toRadians(90);
		if(maxLatitude < Math.toRadians(-90)) maxLatitude = Math.toRadians(-90);
				
		double minLongitude = geographicCoordinates.getLongitude() - deltaLongitudeAngle;
		double maxLongitude = geographicCoordinates.getLongitude() + deltaLongitudeAngle;
	
		String serverRequestURL = "https://ows.mundialis.de/services/service?&VERSION=1.1.1&REQUEST=";
		String layerName = "OSM-WMS";
		String bboxString = "&BBOX=" + Math.toDegrees(minLongitude) + "," + Math.toDegrees(minLatitude) + "," + Math.toDegrees(maxLongitude) + "," + Math.toDegrees(maxLatitude);		
		String requestString = "GetMap&LAYERS=" + layerName + "&STYLES=&SRS=EPSG:4326" + bboxString + "&WIDTH=1024&HEIGHT=1024&FORMAT=image/png";						
		String urlString = serverRequestURL + requestString;		
				
		try
		{
			Logger.INSTANCE.log(Activator.PLUGIN_ID, "Getting new image from WMS : " + urlString, EventSeverity.INFO);
									
			URI uri = new URI(urlString);			
			URL url = uri.toURL();	
		    URLConnection c = url.openConnection();
		 
		    // set the connection timeout to 5 seconds	    
		    c.setConnectTimeout(5000);
		    c.setReadTimeout(5000);
		    		   		
			Image img = null;					
			img = ImageIO.read(c.getInputStream());		
			
		    BufferedImage bufferedImage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
	
		    // Draw the image on to the buffered image
		    Graphics2D bGr = bufferedImage.createGraphics();
		    bGr.drawImage(img, 0, 0, null);
		    bGr.dispose();
		
		    SurfaceImage surfaceImage = new SurfaceImage();
		    surfaceImage.minLatitude = minLatitude;
		    surfaceImage.maxLatitude = maxLatitude;
		    surfaceImage.minLongitude = minLongitude;
		    surfaceImage.maxLongitude = maxLongitude;
		    surfaceImage.image = bufferedImage;
		    
		    wmsImageCount++;
			
			Logger.INSTANCE.log(Activator.PLUGIN_ID, "Sucessfully got image number <" + wmsImageCount +"> from WMS." + urlString, EventSeverity.INFO);
		    
		    return surfaceImage;
		    		
		}
		catch(Exception e)
		{
			Logger.INSTANCE.log(Activator.PLUGIN_ID, "Failed to get image from WMS : " + urlString, EventSeverity.ERROR, e);
		}
		
		return null;
	}
	
	private static boolean projectsOntoSurfaceImage(SurfaceImage surfaceImage, GeographicCoordinates geographicCoordinates)
	{
		double gamma = Math.abs(getHorizonAngle(geographicCoordinates.getElevation()));
		
		double projectionMinLatitude = geographicCoordinates.getLatitude() - gamma;
		double projectionMaxLatitude = geographicCoordinates.getLatitude() + gamma;
		
		double projectionMinLongitude = geographicCoordinates.getLongitude() - gamma;
		double projectionMaxLongitude = geographicCoordinates.getLongitude() + gamma;
		
		return ((projectionMinLatitude >= surfaceImage.minLatitude) &&
			    (projectionMaxLatitude <= surfaceImage.maxLatitude) &&
			    (projectionMinLongitude >= surfaceImage.minLongitude) &&
			    (projectionMaxLongitude <= surfaceImage.maxLongitude));
		
	}
	
	private static AbstractEImage getSubImage(SurfaceImage surfaceImage, GeographicCoordinates geographicCoordinates)
	{
		if(surfaceImage.image != null)
		{
			int zoneImageWidth = surfaceImage.image.getWidth();
			int zoneImageHeight = surfaceImage.image.getHeight();
			
			double gamma = Math.abs(getHorizonAngle(geographicCoordinates.getElevation()));			
			
			double projectionVerticalOffset = ((geographicCoordinates.getLatitude() - gamma) -  surfaceImage.minLatitude) /
												(surfaceImage.maxLatitude - surfaceImage.minLatitude);
			
			double projectionHorizontalOffset   = ((geographicCoordinates.getLongitude() - gamma) - surfaceImage.minLongitude) /
											      (surfaceImage.maxLongitude - surfaceImage.minLongitude);			
			
			int subImageHeight = (int) Math.round(((2 * gamma) / (surfaceImage.maxLatitude - surfaceImage.minLatitude)) * zoneImageWidth);
						
			int subImageWidth = (int) Math.round(((2 * gamma) / (surfaceImage.maxLongitude - surfaceImage.minLongitude)) * zoneImageHeight);

			int widthOffset = (int) Math.floor(projectionHorizontalOffset * zoneImageWidth);
			int heightOffset = zoneImageHeight -(int) Math.floor(projectionVerticalOffset  * zoneImageHeight) - subImageHeight;
			if(heightOffset < 0) heightOffset = 0;
				
		    EImage zoneImage = ApogyCommonImagesFactory.eINSTANCE.createEImage();
		    zoneImage.setImageContent(surfaceImage.image);

		    try
		    {			
		    	return EImagesUtilities.INSTANCE.getSubImage(zoneImage, widthOffset, heightOffset, subImageWidth, subImageHeight);
		    }
		    catch (Exception e) 
		    {
		    	e.printStackTrace();
			}
		}
				
		return null;
	}
	
	
	
//	public static AbstractEImage getImageFromWMSOLD(GeographicCoordinates geographicCoordinates) throws Exception
//	{		
//		ServiceTracker proxyTracker = new ServiceTracker(FrameworkUtil.getBundle(AtmosphereJME3Utilities.class).getBundleContext(), IProxyService.class.getName(), null);
//		proxyTracker.open();
//
//		
//		if(!done)
//		{
//			// -Djdk.http.auth.tunneling.disabledSchemes=
////			
////			Authenticator.setDefault(new ProxyAuthenticator("user", "password"));
////			System.setProperty("http.proxyHost", "host");
////			System.setProperty("http.proxyPort", "port");			
//						
//			System.clearProperty("http.proxyHost");
//			System.clearProperty("http.proxyPort");			
//					
//			System.out.println("http.proxySet             :" + System.getProperty("http.proxySet"));
//			System.out.println("java.net.useSystemProxies :" + System.getProperty("java.net.useSystemProxies"));
//			System.out.println("http.proxyHost            :" + System.getProperty("http.proxyHost"));
//			System.out.println("http.proxyPort            :" + System.getProperty("http.proxyPort"));
//													
//			done = true;
//		}
//		
////		System.clearProperty("http.proxyHost");
////		System.clearProperty("http.proxyPort");			
//		System.out.println("AtmosphereJME3Utilities.getImageFromWMS()");
//		
//		double gamma = Math.abs(getHorizonAngle(geographicCoordinates.getElevation()));
//		
//		double minLatitude = Math.toDegrees(geographicCoordinates.getLatitude() - gamma);
//		double maxLatitude = Math.toDegrees(geographicCoordinates.getLatitude() + gamma);
//		
//		if(minLatitude > 90) minLatitude = 90;
//		if(minLatitude < -90) minLatitude = -90;
//		
//		if(maxLatitude > 90) maxLatitude = 90;
//		if(maxLatitude < -90) maxLatitude = -90;
//				
//		double minLongitude = Math.toDegrees(geographicCoordinates.getLongitude() - gamma);
//		double maxLongitude = Math.toDegrees(geographicCoordinates.getLongitude() + gamma);
//			
//		String serverRequestURL = "http://ows.mundialis.de/services/service?&VERSION=1.1.1&REQUEST=";
//		String layerName = "OSM-WMS";
//		String bboxString = "&BBOX=" + minLongitude + "," + minLatitude + "," + maxLongitude + "," + maxLatitude;		
//		String requestString = "GetMap&LAYERS=" + layerName + "&STYLES=&SRS=EPSG:4326" + bboxString + "&WIDTH=512&HEIGHT=512&FORMAT=image/png";						
//		String urlString = serverRequestURL + requestString;		
//		System.out.println("URL" + urlString);
//		
//		URI uri = new URI(urlString);
//		
//	      IProxyService proxyService = (IProxyService) proxyTracker.getService();
//	      IProxyData[] proxyDataForHost = proxyService.select(uri);
//
////	      for (IProxyData data : proxyDataForHost) 
////	      {
////	    	  if (data.getHost() != null) 
////	    	  {
////	    		  System.setProperty("http.proxySet", "true");
////	    		  System.setProperty("http.proxyHost", data.getHost());
////	          }
////	          if (data.getHost() != null) 
////	          {
////	              System.setProperty("http.proxyPort", String.valueOf(data.getPort()));
////	          }
////	      }
//	      
//	      // Close the service and close the service tracker
//	      proxyService = null;
//	      proxyTracker.close();
//
//	    
//		URL url = uri.toURL();	
//	    URLConnection c = url.openConnection();
//	 
//	    // set the connection timeout to 5 seconds	    
//	    c.setConnectTimeout(5000);
//	    c.setReadTimeout(5000);
//	    		   		
//		Image img = null;					
//		img = ImageIO.read(c.getInputStream());		
//		
//	    BufferedImage bufferedImage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
//
//	    // Draw the image on to the buffered image
//	    Graphics2D bGr = bufferedImage.createGraphics();
//	    bGr.drawImage(img, 0, 0, null);
//	    bGr.dispose();
//	
//	    EImage eImage = ApogyCommonImagesFactory.eINSTANCE.createEImage();
//	    eImage.setImageContent(bufferedImage);
//
//	    EImagesUtilities.INSTANCE.saveImageAsPNG("/home/pallard/tmp/Surface.png", eImage);
//	    
//	    return eImage;
//	}
	
	/**
	 * 
	 * @param zoom
	 * @param latitude
	 * @return
	 * @see https://wiki.openstreetmap.org/wiki/Zoom_levels
	 */
	public static double getPixelResolution(double zoom, double latitude)
	{
		double earthCircumference = 2*Math.PI * EARTH_RADIUS_METERS;		
		
		double t = Math.pow(2, (zoom + 8));
		
		return Math.cos(latitude)  * (earthCircumference / t);
	}
		
	private static double getProfileLenght(List<Vector3f> profile)
	{
		double distance = 0;
		
		Iterator<Vector3f> it = profile.iterator();
		
		Vector3f previous = null;
		Vector3f current = null;
		while(it.hasNext())
		{			
			current = it.next();		
			if(previous == null) previous = current;			
			distance += previous.distance(current);			
			previous = current;
		}
		
		return distance;
	}
	
	private static double getGeodesicDistance(List<Vector3f> profile, Vector3f value)
	{
		double distance = 0;
	
		Iterator<Vector3f> it = profile.iterator();
		
		Vector3f previous = null;
		Vector3f current = null;
		
		while(it.hasNext() && current != value)
		{			
			current = it.next();
		
			if(previous == null) previous = current;
			
			distance += previous.distance(current);
			
			previous = current;
		}
		
		return distance;
	}
	
	private static List<Vector3f> rotateAroundZ(List<Vector3f> profile, double angle)
	{
		List<Vector3f> rotatedProfile = new ArrayList<Vector3f>();
		
		Matrix4d m = new Matrix4d();
		m.setIdentity();
		Matrix3d rot = new Matrix3d();
		rot.setIdentity();
		rot.rotZ(angle);
		m.setRotation(rot);
		
		for(Vector3f v : profile)
		{
			Point3d p = new Point3d(v.x, v.y, v.z);
			m.transform(p);
			
			Vector3f rotatedV = new Vector3f((float) p.x, (float) p.y, (float) p.z);
			rotatedProfile.add(rotatedV);
		}
		
		return rotatedProfile;
	}
	
}
