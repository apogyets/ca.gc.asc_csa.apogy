package ca.gc.asc_csa.apogy.core.environment.earth.atmosphere.ui.jme3.utils;

public class Circle 
{
	public double radius;
	public double xCenter;
	public double yCenter;
	
	public Circle(double radius, double xCenter,double yCenter)
	{
		this.radius = radius;
		this.xCenter = xCenter;
		this.yCenter = yCenter;
	}
	
	public String toString()
	{
		String s = new String();
		
		s+= "Radius : " + radius + " xCenter " + xCenter + " yCenter " + yCenter;
		
		return s;
	}
}
