package ca.gc.asc_csa.apogy.common.topology.impl;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import javax.vecmath.Matrix4d;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathPackage;
import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.topology.AbstractViewPointReference;
import ca.gc.asc_csa.apogy.common.topology.AggregateContentNode;
import ca.gc.asc_csa.apogy.common.topology.AggregateGroupNode;
import ca.gc.asc_csa.apogy.common.topology.CADNode;
import ca.gc.asc_csa.apogy.common.topology.ContentNode;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.INodeVisitor;
import ca.gc.asc_csa.apogy.common.topology.Leaf;
import ca.gc.asc_csa.apogy.common.topology.Link;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodeDescriptionFilter;
import ca.gc.asc_csa.apogy.common.topology.NodeFilter;
import ca.gc.asc_csa.apogy.common.topology.NodeFilterChain;
import ca.gc.asc_csa.apogy.common.topology.NodeIdFilter;
import ca.gc.asc_csa.apogy.common.topology.NodeIsChildOfFilter;
import ca.gc.asc_csa.apogy.common.topology.NodePath;
import ca.gc.asc_csa.apogy.common.topology.NodeTypeFilter;
import ca.gc.asc_csa.apogy.common.topology.PickAndPlaceNode;
import ca.gc.asc_csa.apogy.common.topology.PositionNode;
import ca.gc.asc_csa.apogy.common.topology.ReferencedContentNode;
import ca.gc.asc_csa.apogy.common.topology.ReferencedGroupNode;
import ca.gc.asc_csa.apogy.common.topology.RegexNodeFilter;
import ca.gc.asc_csa.apogy.common.topology.RotationNode;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFactory;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.ArbitraryViewPoint;
import ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint;
import ca.gc.asc_csa.apogy.common.topology.TopologyProvider;
import ca.gc.asc_csa.apogy.common.topology.TransformNode;
import ca.gc.asc_csa.apogy.common.topology.URLNode;
import java.util.Collection;
import java.util.List;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc --> * @generated
 */
public class ApogyCommonTopologyPackageImpl extends EPackageImpl implements ApogyCommonTopologyPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass topologyProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass nodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass linkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass leafEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass urlNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass cadNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass contentNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass referencedContentNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass aggregateContentNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass groupNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass referencedGroupNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass aggregateGroupNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass positionNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass rotationNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass transformNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass pickAndPlaceNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass nodePathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass abstractViewPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass abstractViewPointReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass arbitraryViewPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass attachedViewPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass apogyCommonTopologyFacadeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass nodeFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass nodeFilterChainEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass nodeTypeFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass regexNodeFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass nodeIdFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass nodeDescriptionFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EClass nodeIsChildOfFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType matrix4dEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType eClassEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType iNodeVisitorEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType collectionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private EDataType listEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonTopologyPackageImpl() {
		super(eNS_URI, ApogyCommonTopologyFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonTopologyPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonTopologyPackage init() {
		if (isInited) return (ApogyCommonTopologyPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonTopologyPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonTopologyPackageImpl theApogyCommonTopologyPackage = (ApogyCommonTopologyPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonTopologyPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonTopologyPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ApogyCommonMathPackage.eINSTANCE.eClass();
		ApogyCommonEMFPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonTopologyPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonTopologyPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonTopologyPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonTopologyPackage.eNS_URI, theApogyCommonTopologyPackage);
		return theApogyCommonTopologyPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getTopologyProvider() {
		return topologyProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getTopologyProvider_TopologyRoot() {
		return (EReference)topologyProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNode() {
		return nodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getNode_Parent() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getNode_Description() {
		return (EAttribute)nodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getNode_NodeId() {
		return (EAttribute)nodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getNode__Accept__INodeVisitor() {
		return nodeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getLink() {
		return linkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getLink_Node() {
		return (EReference)linkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getLeaf() {
		return leafEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getURLNode() {
		return urlNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getURLNode_Url() {
		return (EAttribute)urlNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getURLNode_PolygonCount() {
		return (EAttribute)urlNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getURLNode_VertexCount() {
		return (EAttribute)urlNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getCADNode() {
		return cadNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getCADNode_NodeName() {
		return (EAttribute)cadNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getContentNode() {
		return contentNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getContentNode_Content() {
		return (EReference)contentNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getReferencedContentNode() {
		return referencedContentNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getReferencedContentNode_ReferencedContent() {
		return (EReference)referencedContentNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAggregateContentNode() {
		return aggregateContentNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getAggregateContentNode_AggregateContent() {
		return (EReference)aggregateContentNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getGroupNode() {
		return groupNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getGroupNode_Children() {
		return (EReference)groupNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getReferencedGroupNode() {
		return referencedGroupNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getReferencedGroupNode_ReferencedChildren() {
		return (EReference)referencedGroupNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAggregateGroupNode() {
		return aggregateGroupNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getAggregateGroupNode_AggregatedChildren() {
		return (EReference)aggregateGroupNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getPositionNode() {
		return positionNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getPositionNode_Position() {
		return (EReference)positionNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getRotationNode() {
		return rotationNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getRotationNode_RotationMatrix() {
		return (EReference)rotationNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getTransformNode() {
		return transformNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getTransformNode__AsMatrix4d() {
		return transformNodeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getTransformNode__SetTransformation__Matrix4d() {
		return transformNodeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getPickAndPlaceNode() {
		return pickAndPlaceNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNodePath() {
		return nodePathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getNodePath_NodeIds() {
		return (EAttribute)nodePathEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAbstractViewPoint() {
		return abstractViewPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getAbstractViewPoint_Name() {
		return (EAttribute)abstractViewPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getAbstractViewPoint_Position() {
		return (EReference)abstractViewPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getAbstractViewPoint_RotationMatrix() {
		return (EReference)abstractViewPointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getAbstractViewPoint__Initialize__Node() {
		return abstractViewPointEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAbstractViewPointReference() {
		return abstractViewPointReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getAbstractViewPointReference_AbstractViewPoint() {
		return (EReference)abstractViewPointReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getArbitraryViewPoint() {
		return arbitraryViewPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getAttachedViewPoint() {
		return attachedViewPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getAttachedViewPoint_AllowTranslation() {
		return (EAttribute)attachedViewPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getAttachedViewPoint_AllowRotation() {
		return (EAttribute)attachedViewPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getAttachedViewPoint_Node() {
		return (EReference)attachedViewPointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getAttachedViewPoint_NodePath() {
		return (EReference)attachedViewPointEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getApogyCommonTopologyFacade() {
		return apogyCommonTopologyFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateLink__Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateContentNode__Object() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateReferencedContentNode__Object() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateAggregateContentNode__Object() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreatePositionNode__double_double_double() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateRotationNodeXYZ__double_double_double() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateRotationNodeYZX__double_double_double() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateRotationNodeZYX__double_double_double() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateTransformNodeXYZ__double_double_double_double_double_double() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateTransformNodeYZX__double_double_double_double_double_double() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateTransformNodeZYX__double_double_double_double_double_double() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateTransformNode__Matrix4d() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreatePickAndPlaceNode__Matrix4d() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateAttachedViewPoint__Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__ExpressNodeInRootFrame__Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__ExpressRootInNodeFrame__Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__ExpressInFrame__Node_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__FindNodesByDescription__String_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__FindNodesByID__String_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__FindNodesByType__EClass_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__FindRoot__Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__DoNodesShareTopologyTree__Node_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__FindNodePath__Node_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__CreateNodePath__Node_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__ResolveNode__Node_NodePath() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__GetEuclideanDistance__Node_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__GetGeodesicDistance__Node_Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__PrintTopology__Node() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EOperation getApogyCommonTopologyFacade__Filter__NodeFilter_Collection() {
		return apogyCommonTopologyFacadeEClass.getEOperations().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNodeFilter() {
		return nodeFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNodeFilterChain() {
		return nodeFilterChainEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNodeTypeFilter() {
		return nodeTypeFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getNodeTypeFilter_Clazz() {
		return (EAttribute)nodeTypeFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getRegexNodeFilter() {
		return regexNodeFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getRegexNodeFilter_Regex() {
		return (EAttribute)regexNodeFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNodeIdFilter() {
		return nodeIdFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNodeDescriptionFilter() {
		return nodeDescriptionFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EClass getNodeIsChildOfFilter() {
		return nodeIsChildOfFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EReference getNodeIsChildOfFilter_ParentNode() {
		return (EReference)nodeIsChildOfFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EAttribute getNodeIsChildOfFilter_ParentNodeIncluded() {
		return (EAttribute)nodeIsChildOfFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getMatrix4d() {
		return matrix4dEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getEClass() {
		return eClassEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getINodeVisitor() {
		return iNodeVisitorEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getCollection() {
		return collectionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public EDataType getList() {
		return listEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonTopologyFactory getApogyCommonTopologyFactory() {
		return (ApogyCommonTopologyFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		topologyProviderEClass = createEClass(TOPOLOGY_PROVIDER);
		createEReference(topologyProviderEClass, TOPOLOGY_PROVIDER__TOPOLOGY_ROOT);

		nodeEClass = createEClass(NODE);
		createEReference(nodeEClass, NODE__PARENT);
		createEAttribute(nodeEClass, NODE__DESCRIPTION);
		createEAttribute(nodeEClass, NODE__NODE_ID);
		createEOperation(nodeEClass, NODE___ACCEPT__INODEVISITOR);

		linkEClass = createEClass(LINK);
		createEReference(linkEClass, LINK__NODE);

		leafEClass = createEClass(LEAF);

		urlNodeEClass = createEClass(URL_NODE);
		createEAttribute(urlNodeEClass, URL_NODE__URL);
		createEAttribute(urlNodeEClass, URL_NODE__POLYGON_COUNT);
		createEAttribute(urlNodeEClass, URL_NODE__VERTEX_COUNT);

		cadNodeEClass = createEClass(CAD_NODE);
		createEAttribute(cadNodeEClass, CAD_NODE__NODE_NAME);

		contentNodeEClass = createEClass(CONTENT_NODE);
		createEReference(contentNodeEClass, CONTENT_NODE__CONTENT);

		referencedContentNodeEClass = createEClass(REFERENCED_CONTENT_NODE);
		createEReference(referencedContentNodeEClass, REFERENCED_CONTENT_NODE__REFERENCED_CONTENT);

		aggregateContentNodeEClass = createEClass(AGGREGATE_CONTENT_NODE);
		createEReference(aggregateContentNodeEClass, AGGREGATE_CONTENT_NODE__AGGREGATE_CONTENT);

		groupNodeEClass = createEClass(GROUP_NODE);
		createEReference(groupNodeEClass, GROUP_NODE__CHILDREN);

		referencedGroupNodeEClass = createEClass(REFERENCED_GROUP_NODE);
		createEReference(referencedGroupNodeEClass, REFERENCED_GROUP_NODE__REFERENCED_CHILDREN);

		aggregateGroupNodeEClass = createEClass(AGGREGATE_GROUP_NODE);
		createEReference(aggregateGroupNodeEClass, AGGREGATE_GROUP_NODE__AGGREGATED_CHILDREN);

		positionNodeEClass = createEClass(POSITION_NODE);
		createEReference(positionNodeEClass, POSITION_NODE__POSITION);

		rotationNodeEClass = createEClass(ROTATION_NODE);
		createEReference(rotationNodeEClass, ROTATION_NODE__ROTATION_MATRIX);

		transformNodeEClass = createEClass(TRANSFORM_NODE);
		createEOperation(transformNodeEClass, TRANSFORM_NODE___AS_MATRIX4D);
		createEOperation(transformNodeEClass, TRANSFORM_NODE___SET_TRANSFORMATION__MATRIX4D);

		pickAndPlaceNodeEClass = createEClass(PICK_AND_PLACE_NODE);

		nodePathEClass = createEClass(NODE_PATH);
		createEAttribute(nodePathEClass, NODE_PATH__NODE_IDS);

		abstractViewPointEClass = createEClass(ABSTRACT_VIEW_POINT);
		createEAttribute(abstractViewPointEClass, ABSTRACT_VIEW_POINT__NAME);
		createEReference(abstractViewPointEClass, ABSTRACT_VIEW_POINT__POSITION);
		createEReference(abstractViewPointEClass, ABSTRACT_VIEW_POINT__ROTATION_MATRIX);
		createEOperation(abstractViewPointEClass, ABSTRACT_VIEW_POINT___INITIALIZE__NODE);

		abstractViewPointReferenceEClass = createEClass(ABSTRACT_VIEW_POINT_REFERENCE);
		createEReference(abstractViewPointReferenceEClass, ABSTRACT_VIEW_POINT_REFERENCE__ABSTRACT_VIEW_POINT);

		arbitraryViewPointEClass = createEClass(ARBITRARY_VIEW_POINT);

		attachedViewPointEClass = createEClass(ATTACHED_VIEW_POINT);
		createEAttribute(attachedViewPointEClass, ATTACHED_VIEW_POINT__ALLOW_TRANSLATION);
		createEAttribute(attachedViewPointEClass, ATTACHED_VIEW_POINT__ALLOW_ROTATION);
		createEReference(attachedViewPointEClass, ATTACHED_VIEW_POINT__NODE);
		createEReference(attachedViewPointEClass, ATTACHED_VIEW_POINT__NODE_PATH);

		apogyCommonTopologyFacadeEClass = createEClass(APOGY_COMMON_TOPOLOGY_FACADE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_LINK__NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_CONTENT_NODE__OBJECT);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_REFERENCED_CONTENT_NODE__OBJECT);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_AGGREGATE_CONTENT_NODE__OBJECT);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_POSITION_NODE__DOUBLE_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_ROTATION_NODE_XYZ__DOUBLE_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_ROTATION_NODE_YZX__DOUBLE_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_ROTATION_NODE_ZYX__DOUBLE_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_TRANSFORM_NODE_XYZ__DOUBLE_DOUBLE_DOUBLE_DOUBLE_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_TRANSFORM_NODE_YZX__DOUBLE_DOUBLE_DOUBLE_DOUBLE_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_TRANSFORM_NODE_ZYX__DOUBLE_DOUBLE_DOUBLE_DOUBLE_DOUBLE_DOUBLE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_TRANSFORM_NODE__MATRIX4D);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_PICK_AND_PLACE_NODE__MATRIX4D);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_ATTACHED_VIEW_POINT__NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___EXPRESS_NODE_IN_ROOT_FRAME__NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___EXPRESS_ROOT_IN_NODE_FRAME__NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___EXPRESS_IN_FRAME__NODE_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___FIND_NODES_BY_DESCRIPTION__STRING_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___FIND_NODES_BY_ID__STRING_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___FIND_NODES_BY_TYPE__ECLASS_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___FIND_ROOT__NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___DO_NODES_SHARE_TOPOLOGY_TREE__NODE_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___FIND_NODE_PATH__NODE_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___CREATE_NODE_PATH__NODE_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___RESOLVE_NODE__NODE_NODEPATH);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___GET_EUCLIDEAN_DISTANCE__NODE_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___GET_GEODESIC_DISTANCE__NODE_NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___PRINT_TOPOLOGY__NODE);
		createEOperation(apogyCommonTopologyFacadeEClass, APOGY_COMMON_TOPOLOGY_FACADE___FILTER__NODEFILTER_COLLECTION);

		nodeFilterEClass = createEClass(NODE_FILTER);

		nodeFilterChainEClass = createEClass(NODE_FILTER_CHAIN);

		nodeTypeFilterEClass = createEClass(NODE_TYPE_FILTER);
		createEAttribute(nodeTypeFilterEClass, NODE_TYPE_FILTER__CLAZZ);

		regexNodeFilterEClass = createEClass(REGEX_NODE_FILTER);
		createEAttribute(regexNodeFilterEClass, REGEX_NODE_FILTER__REGEX);

		nodeIdFilterEClass = createEClass(NODE_ID_FILTER);

		nodeDescriptionFilterEClass = createEClass(NODE_DESCRIPTION_FILTER);

		nodeIsChildOfFilterEClass = createEClass(NODE_IS_CHILD_OF_FILTER);
		createEReference(nodeIsChildOfFilterEClass, NODE_IS_CHILD_OF_FILTER__PARENT_NODE);
		createEAttribute(nodeIsChildOfFilterEClass, NODE_IS_CHILD_OF_FILTER__PARENT_NODE_INCLUDED);

		// Create data types
		matrix4dEDataType = createEDataType(MATRIX4D);
		eClassEDataType = createEDataType(ECLASS);
		iNodeVisitorEDataType = createEDataType(INODE_VISITOR);
		collectionEDataType = createEDataType(COLLECTION);
		listEDataType = createEDataType(LIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		ApogyCommonMathPackage theApogyCommonMathPackage = (ApogyCommonMathPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonMathPackage.eNS_URI);
		ApogyCommonEMFPackage theApogyCommonEMFPackage = (ApogyCommonEMFPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFPackage.eNS_URI);

		// Create type parameters
		ETypeParameter contentNodeEClass_T = addETypeParameter(contentNodeEClass, "T");
		ETypeParameter referencedContentNodeEClass_T = addETypeParameter(referencedContentNodeEClass, "T");
		ETypeParameter aggregateContentNodeEClass_T = addETypeParameter(aggregateContentNodeEClass, "T");
		addETypeParameter(collectionEDataType, "T");
		addETypeParameter(listEDataType, "T");

		// Set bounds for type parameters

		// Add supertypes to classes
		linkEClass.getESuperTypes().add(this.getNode());
		leafEClass.getESuperTypes().add(this.getNode());
		urlNodeEClass.getESuperTypes().add(this.getLeaf());
		cadNodeEClass.getESuperTypes().add(this.getURLNode());
		contentNodeEClass.getESuperTypes().add(this.getLeaf());
		EGenericType g1 = createEGenericType(this.getContentNode());
		EGenericType g2 = createEGenericType(referencedContentNodeEClass_T);
		g1.getETypeArguments().add(g2);
		referencedContentNodeEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getContentNode());
		g2 = createEGenericType(aggregateContentNodeEClass_T);
		g1.getETypeArguments().add(g2);
		aggregateContentNodeEClass.getEGenericSuperTypes().add(g1);
		groupNodeEClass.getESuperTypes().add(this.getNode());
		referencedGroupNodeEClass.getESuperTypes().add(this.getGroupNode());
		aggregateGroupNodeEClass.getESuperTypes().add(this.getGroupNode());
		positionNodeEClass.getESuperTypes().add(this.getAggregateGroupNode());
		rotationNodeEClass.getESuperTypes().add(this.getAggregateGroupNode());
		transformNodeEClass.getESuperTypes().add(this.getPositionNode());
		transformNodeEClass.getESuperTypes().add(this.getRotationNode());
		pickAndPlaceNodeEClass.getESuperTypes().add(this.getTransformNode());
		arbitraryViewPointEClass.getESuperTypes().add(this.getAbstractViewPoint());
		attachedViewPointEClass.getESuperTypes().add(this.getAbstractViewPoint());
		g1 = createEGenericType(theApogyCommonEMFPackage.getIFilter());
		g2 = createEGenericType(this.getNode());
		g1.getETypeArguments().add(g2);
		nodeFilterEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(theApogyCommonEMFPackage.getCompositeFilter());
		g2 = createEGenericType(this.getNode());
		g1.getETypeArguments().add(g2);
		nodeFilterChainEClass.getEGenericSuperTypes().add(g1);
		nodeTypeFilterEClass.getESuperTypes().add(this.getNodeFilter());
		regexNodeFilterEClass.getESuperTypes().add(this.getNodeFilter());
		nodeIdFilterEClass.getESuperTypes().add(this.getRegexNodeFilter());
		nodeDescriptionFilterEClass.getESuperTypes().add(this.getRegexNodeFilter());
		nodeIsChildOfFilterEClass.getESuperTypes().add(this.getNodeFilter());

		// Initialize classes, features, and operations; add parameters
		initEClass(topologyProviderEClass, TopologyProvider.class, "TopologyProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTopologyProvider_TopologyRoot(), this.getGroupNode(), null, "topologyRoot", null, 0, 1, TopologyProvider.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nodeEClass, Node.class, "Node", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNode_Parent(), this.getNode(), null, "parent", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNode_Description(), theEcorePackage.getEString(), "description", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNode_NodeId(), theEcorePackage.getEString(), "nodeId", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getNode__Accept__INodeVisitor(), null, "accept", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getINodeVisitor(), "visitor", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(linkEClass, Link.class, "Link", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLink_Node(), this.getNode(), null, "node", null, 0, 1, Link.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(leafEClass, Leaf.class, "Leaf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(urlNodeEClass, URLNode.class, "URLNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getURLNode_Url(), theEcorePackage.getEString(), "url", null, 0, 1, URLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getURLNode_PolygonCount(), theEcorePackage.getEInt(), "polygonCount", null, 0, 1, URLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getURLNode_VertexCount(), theEcorePackage.getEInt(), "vertexCount", null, 0, 1, URLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cadNodeEClass, CADNode.class, "CADNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCADNode_NodeName(), theEcorePackage.getEString(), "nodeName", null, 0, 1, CADNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(contentNodeEClass, ContentNode.class, "ContentNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(contentNodeEClass_T);
		initEReference(getContentNode_Content(), g1, null, "content", null, 0, 1, ContentNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referencedContentNodeEClass, ReferencedContentNode.class, "ReferencedContentNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(referencedContentNodeEClass_T);
		initEReference(getReferencedContentNode_ReferencedContent(), g1, null, "referencedContent", null, 0, 1, ReferencedContentNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aggregateContentNodeEClass, AggregateContentNode.class, "AggregateContentNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(aggregateContentNodeEClass_T);
		initEReference(getAggregateContentNode_AggregateContent(), g1, null, "aggregateContent", null, 0, 1, AggregateContentNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(groupNodeEClass, GroupNode.class, "GroupNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGroupNode_Children(), this.getNode(), null, "children", null, 0, -1, GroupNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(referencedGroupNodeEClass, ReferencedGroupNode.class, "ReferencedGroupNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getReferencedGroupNode_ReferencedChildren(), this.getNode(), null, "referencedChildren", null, 0, -1, ReferencedGroupNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aggregateGroupNodeEClass, AggregateGroupNode.class, "AggregateGroupNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAggregateGroupNode_AggregatedChildren(), this.getNode(), null, "aggregatedChildren", null, 0, -1, AggregateGroupNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(positionNodeEClass, PositionNode.class, "PositionNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPositionNode_Position(), theApogyCommonMathPackage.getTuple3d(), null, "position", null, 0, 1, PositionNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rotationNodeEClass, RotationNode.class, "RotationNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRotationNode_RotationMatrix(), theApogyCommonMathPackage.getMatrix3x3(), null, "rotationMatrix", null, 0, 1, RotationNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transformNodeEClass, TransformNode.class, "TransformNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getTransformNode__AsMatrix4d(), this.getMatrix4d(), "asMatrix4d", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformNode__SetTransformation__Matrix4d(), null, "setTransformation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMatrix4d(), "matrix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(pickAndPlaceNodeEClass, PickAndPlaceNode.class, "PickAndPlaceNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodePathEClass, NodePath.class, "NodePath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNodePath_NodeIds(), theEcorePackage.getEString(), "nodeIds", null, 0, -1, NodePath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractViewPointEClass, AbstractViewPoint.class, "AbstractViewPoint", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractViewPoint_Name(), theEcorePackage.getEString(), "name", null, 0, 1, AbstractViewPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractViewPoint_Position(), theApogyCommonMathPackage.getTuple3d(), null, "position", null, 0, 1, AbstractViewPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractViewPoint_RotationMatrix(), theApogyCommonMathPackage.getMatrix3x3(), null, "rotationMatrix", null, 0, 1, AbstractViewPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAbstractViewPoint__Initialize__Node(), null, "initialize", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(abstractViewPointReferenceEClass, AbstractViewPointReference.class, "AbstractViewPointReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractViewPointReference_AbstractViewPoint(), this.getAbstractViewPoint(), null, "abstractViewPoint", null, 0, 1, AbstractViewPointReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(arbitraryViewPointEClass, ArbitraryViewPoint.class, "ArbitraryViewPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(attachedViewPointEClass, AttachedViewPoint.class, "AttachedViewPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttachedViewPoint_AllowTranslation(), theEcorePackage.getEBoolean(), "allowTranslation", "false", 0, 1, AttachedViewPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttachedViewPoint_AllowRotation(), theEcorePackage.getEBoolean(), "allowRotation", "false", 0, 1, AttachedViewPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachedViewPoint_Node(), this.getNode(), null, "node", null, 0, 1, AttachedViewPoint.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttachedViewPoint_NodePath(), this.getNodePath(), null, "nodePath", null, 0, 1, AttachedViewPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(apogyCommonTopologyFacadeEClass, ApogyCommonTopologyFacade.class, "ApogyCommonTopologyFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getApogyCommonTopologyFacade__CreateLink__Node(), this.getLink(), "createLink", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "node", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateContentNode__Object(), null, "createContentNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		ETypeParameter t1 = addETypeParameter(op, "T");
		g1 = createEGenericType(t1);
		addEParameter(op, g1, "content", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getContentNode());
		g2 = createEGenericType(t1);
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonTopologyFacade__CreateReferencedContentNode__Object(), null, "createReferencedContentNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		t1 = addETypeParameter(op, "T");
		g1 = createEGenericType(t1);
		addEParameter(op, g1, "content", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getReferencedContentNode());
		g2 = createEGenericType(t1);
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonTopologyFacade__CreateAggregateContentNode__Object(), null, "createAggregateContentNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		t1 = addETypeParameter(op, "T");
		g1 = createEGenericType(t1);
		addEParameter(op, g1, "content", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getAggregateContentNode());
		g2 = createEGenericType(t1);
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonTopologyFacade__CreatePositionNode__double_double_double(), this.getPositionNode(), "createPositionNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "x", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "y", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "z", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateRotationNodeXYZ__double_double_double(), this.getRotationNode(), "createRotationNodeXYZ", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "x", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "y", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "z", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateRotationNodeYZX__double_double_double(), this.getRotationNode(), "createRotationNodeYZX", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "x", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "y", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "z", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateRotationNodeZYX__double_double_double(), this.getRotationNode(), "createRotationNodeZYX", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "x", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "y", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "z", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateTransformNodeXYZ__double_double_double_double_double_double(), this.getTransformNode(), "createTransformNodeXYZ", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "tx", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "ty", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "tz", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "rx", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "ry", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "rz", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateTransformNodeYZX__double_double_double_double_double_double(), this.getTransformNode(), "createTransformNodeYZX", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "tx", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "ty", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "tz", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "rx", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "ry", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "rz", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateTransformNodeZYX__double_double_double_double_double_double(), this.getTransformNode(), "createTransformNodeZYX", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "tx", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "ty", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "tz", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "rx", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "ry", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDouble(), "rz", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateTransformNode__Matrix4d(), this.getTransformNode(), "createTransformNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMatrix4d(), "matrix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreatePickAndPlaceNode__Matrix4d(), this.getPickAndPlaceNode(), "createPickAndPlaceNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMatrix4d(), "matrix", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__CreateAttachedViewPoint__Node(), this.getAttachedViewPoint(), "createAttachedViewPoint", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "attachmentNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__ExpressNodeInRootFrame__Node(), this.getMatrix4d(), "expressNodeInRootFrame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "node", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__ExpressRootInNodeFrame__Node(), this.getMatrix4d(), "expressRootInNodeFrame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "node", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__ExpressInFrame__Node_Node(), this.getMatrix4d(), "expressInFrame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "sourceFrame", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "targetFrame", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__FindNodesByDescription__String_Node(), this.getNode(), "findNodesByDescription", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "description", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__FindNodesByID__String_Node(), this.getNode(), "findNodesByID", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "nodeId", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__FindNodesByType__EClass_Node(), this.getNode(), "findNodesByType", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEClass(), "clazz", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__FindRoot__Node(), this.getNode(), "findRoot", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "node", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__DoNodesShareTopologyTree__Node_Node(), theEcorePackage.getEBoolean(), "doNodesShareTopologyTree", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "node1", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "node2", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__FindNodePath__Node_Node(), null, "findNodePath", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "fromNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "toNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getNode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonTopologyFacade__CreateNodePath__Node_Node(), this.getNodePath(), "createNodePath", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "fromNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "toNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__ResolveNode__Node_NodePath(), this.getNode(), "resolveNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNodePath(), "nodePath", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__GetEuclideanDistance__Node_Node(), theEcorePackage.getEDouble(), "getEuclideanDistance", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "fromNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "toNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__GetGeodesicDistance__Node_Node(), theEcorePackage.getEDouble(), "getGeodesicDistance", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "fromNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "toNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__PrintTopology__Node(), null, "printTopology", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNode(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonTopologyFacade__Filter__NodeFilter_Collection(), null, "filter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNodeFilter(), "filter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getCollection());
		g2 = createEGenericType(this.getNode());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "nodes", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getCollection());
		g2 = createEGenericType(this.getNode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(nodeFilterEClass, NodeFilter.class, "NodeFilter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodeFilterChainEClass, NodeFilterChain.class, "NodeFilterChain", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodeTypeFilterEClass, NodeTypeFilter.class, "NodeTypeFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNodeTypeFilter_Clazz(), this.getEClass(), "clazz", null, 0, 1, NodeTypeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(regexNodeFilterEClass, RegexNodeFilter.class, "RegexNodeFilter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRegexNodeFilter_Regex(), theEcorePackage.getEString(), "regex", null, 0, 1, RegexNodeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nodeIdFilterEClass, NodeIdFilter.class, "NodeIdFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodeDescriptionFilterEClass, NodeDescriptionFilter.class, "NodeDescriptionFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodeIsChildOfFilterEClass, NodeIsChildOfFilter.class, "NodeIsChildOfFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNodeIsChildOfFilter_ParentNode(), this.getNode(), null, "parentNode", null, 0, 1, NodeIsChildOfFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNodeIsChildOfFilter_ParentNodeIncluded(), theEcorePackage.getEBoolean(), "parentNodeIncluded", "true", 0, 1, NodeIsChildOfFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize data types
		initEDataType(matrix4dEDataType, Matrix4d.class, "Matrix4d", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eClassEDataType, EClass.class, "EClass", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iNodeVisitorEDataType, INodeVisitor.class, "INodeVisitor", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(collectionEDataType, Collection.class, "Collection", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listEDataType, List.class, "List", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "documentation", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca),\n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "prefix", "ApogyCommonTopology",
			 "childCreationExtenders", "true",
			 "extensibleProviderFactory", "true",
			 "multipleEditorPages", "false",
			 "copyrightText", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "modelName", "ApogyCommonTopology",
			 "suppressGenModelAnnotations", "false",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.common.topology/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.common.topology.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.common"
		   });	
		addAnnotation
		  (nodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nThe base class of all nodes in a topology."
		   });	
		addAnnotation
		  (getNode_Parent(), 
		   source, 
		   new String[] {
			 "documentation", "* The parent of the node.",
			 "notify", "true",
			 "propertyCategory", "NODE"
		   });	
		addAnnotation
		  (getNode_Description(), 
		   source, 
		   new String[] {
			 "documentation", "* The description of the node.",
			 "notify", "true",
			 "propertyCategory", "NODE"
		   });	
		addAnnotation
		  (getNode_NodeId(), 
		   source, 
		   new String[] {
			 "documentation", "* The node identifier.",
			 "notify", "true",
			 "propertyCategory", "NODE"
		   });	
		addAnnotation
		  (leafEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA node that has no children."
		   });	
		addAnnotation
		  (urlNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA node that refers to a file where geometry is stored."
		   });	
		addAnnotation
		  (contentNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA node that exposes generic data. This data is typically not a Node."
		   });	
		addAnnotation
		  (referencedContentNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA Content Node that refers the data it exposes."
		   });	
		addAnnotation
		  (aggregateContentNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA Content Node that contains the data it exposes."
		   });	
		addAnnotation
		  (groupNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSuper class of all node that refers/contain child nodes."
		   });	
		addAnnotation
		  (getGroupNode_Children(), 
		   source, 
		   new String[] {
			 "children", "false",
			 "createChild", "false"
		   });	
		addAnnotation
		  (referencedGroupNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA GroupNode that refers to its child nodes."
		   });	
		addAnnotation
		  (getReferencedGroupNode_ReferencedChildren(), 
		   source, 
		   new String[] {
			 "children", "true",
			 "createChild", "false"
		   });	
		addAnnotation
		  (aggregateGroupNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA GroupNode that contains its child nodes."
		   });	
		addAnnotation
		  (getAggregateGroupNode_AggregatedChildren(), 
		   source, 
		   new String[] {
			 "children", "true",
			 "createChild", "true"
		   });	
		addAnnotation
		  (positionNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Aggregate Group Node that contains position information."
		   });	
		addAnnotation
		  (getPositionNode_Position(), 
		   source, 
		   new String[] {
			 "children", "false",
			 "createChild", "false"
		   });	
		addAnnotation
		  (rotationNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Aggregate Group Node that contains orientation information."
		   });	
		addAnnotation
		  (getRotationNode_RotationMatrix(), 
		   source, 
		   new String[] {
			 "children", "false",
			 "createChild", "false"
		   });	
		addAnnotation
		  (transformNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nAn Aggregate Group Node that contains both position and orientation information."
		   });	
		addAnnotation
		  (nodePathEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nClass representing a node path in a topology. Node are referred to by their nodeID."
		   });	
		addAnnotation
		  (getNodePath_NodeIds(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe list of nodeID, from root to end."
		   });	
		addAnnotation
		  (abstractViewPointEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nRepresent a viewpoint in the 3D world."
		   });	
		addAnnotation
		  (getAbstractViewPoint__Initialize__Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nInitialize the view point for a given topology.\n@param root The root of the topology that the view point will be used in."
		   });	
		addAnnotation
		  (getAbstractViewPoint_Name(), 
		   source, 
		   new String[] {
			 "documentation", "The name of the ViewPoint"
		   });	
		addAnnotation
		  (getAbstractViewPoint_Position(), 
		   source, 
		   new String[] {
			 "children", "false",
			 "createChild", "false"
		   });	
		addAnnotation
		  (getAbstractViewPoint_RotationMatrix(), 
		   source, 
		   new String[] {
			 "children", "false",
			 "createChild", "false"
		   });	
		addAnnotation
		  (abstractViewPointReferenceEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA reference to an AbstractViewPoint."
		   });	
		addAnnotation
		  (getAbstractViewPointReference_AbstractViewPoint(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe AbstractViewPoint."
		   });	
		addAnnotation
		  (arbitraryViewPointEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecialized case of AbstractViewPoint that represent an arbitrary view point that can be moved around in the 3D world."
		   });	
		addAnnotation
		  (attachedViewPointEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nSpecialized case of AbstractViewPoint that represent a view point which position and orientation are attached to a node in the topology."
		   });	
		addAnnotation
		  (getAttachedViewPoint_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe node to which the view point is attached."
		   });	
		addAnnotation
		  (getAttachedViewPoint_NodePath(), 
		   source, 
		   new String[] {
			 "documentation", "*\nA node path referring to the node."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateLink__Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a Link node that point to a specified Node.\n@param node The node the Link point to.\n@return The Link node created."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateContentNode__Object(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a ContentNode that contains a specified content.\n@param content The content of the node.\n@return The ContentNode created."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateReferencedContentNode__Object(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a ReferencedContentNode that refers to a specified content.\n@param content The content the node is to refer to.\n@return The ReferencedContentNode created."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateAggregateContentNode__Object(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a AggregateContentNode that contains a specified content.\n@param content The content of the node.\n@return The AggregateContentNode created."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreatePositionNode__double_double_double(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a PositionNode with x,y,and z coordinates.\n@param x the position along the x axis.\n@param y the position along the y axis.\n@param z the position along the z axis."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateRotationNodeXYZ__double_double_double(), 
		   source, 
		   new String[] {
			 "documentation", "Creates a rotation node with the rotation order\nx*y*z\n\n@param x\n           the rotation around the x axis, in radians.\n@param y\n           the rotation around the y axis, in radians.\n@param z\n           the rotation around the z axis, in radians."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateRotationNodeYZX__double_double_double(), 
		   source, 
		   new String[] {
			 "documentation", "Creates a rotation node with the rotation order\ny*z*x\n\n@param x\n           the rotation around the x axis, in radians.\n@param y\n           the rotation around the y axis, in radians.\n@param z\n           the rotation around the z axis, in radians."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateRotationNodeZYX__double_double_double(), 
		   source, 
		   new String[] {
			 "documentation", "Creates a rotation node with the rotation order\nz*y*x\n\n@param x\n           the rotation around the x axis, in radians.\n@param y\n           the rotation around the y axis, in radians.\n@param z\n           the rotation around the z axis, in radians."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateTransformNodeXYZ__double_double_double_double_double_double(), 
		   source, 
		   new String[] {
			 "documentation", "Creates a transformation node with the rotation\norder x*y*z\n\n@param tx\n           the x translation.\n@param ty\n           the y translation.\n@param tz\n           the z translation.\n@param rx\n           the rotation around the x axis, in radians.\n@param ry\n           the rotation around the y axis, in radians.\n@param rz\n           the rotation around the z axis, in radians."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateTransformNodeYZX__double_double_double_double_double_double(), 
		   source, 
		   new String[] {
			 "documentation", "Creates a transformation node with the rotation\norder y*z*x\n\n@param tx\n           the x translation.\n@param ty\n           the y translation.\n@param tz\n           the z translation.\n@param rx\n           the rotation around the x axis, in radians.\n@param ry\n           the rotation around the y axis, in radians.\n@param rz\n           the rotation around the z axis, in radians."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateTransformNodeZYX__double_double_double_double_double_double(), 
		   source, 
		   new String[] {
			 "documentation", "Creates a transformation node with the rotation\norder z*y*x\n\n@param tx\n           the x translation.\n@param ty\n           the y translation.\n@param tz\n           the z translation.\n@param rx\n           the rotation around the x axis, in radians.\n@param ry\n           the rotation around the y axis, in radians.\n@param rz\n           the rotation around the z axis, in radians."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateTransformNode__Matrix4d(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a TransformNode using a pose expressed as a Matrix4d (4x4 matrix).\n@param matrix The matrix.\n@result The TransformNode created."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreatePickAndPlaceNode__Matrix4d(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a PickAndPlaceNode using a pose expressed as a Matrix4d (4x4 matrix).\n@param matrix The matrix.\n@result The PickAndPlaceNode created."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateAttachedViewPoint__Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates an AttachedViewPoint.\n@param attachmentNode The node to attach the View Point in.\n@return The AttachedViewPoint."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__ExpressNodeInRootFrame__Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturn the 4x4 matrix expressing the pose of a specified Node in its root node.\n@param node The specified Node.\n@return The pose."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__ExpressRootInNodeFrame__Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturn the 4x4 matrix expressing the pose of a Node\'s root in that Node reference.\n@param node The specified Node.\n@return The pose."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__ExpressInFrame__Node_Node(), 
		   source, 
		   new String[] {
			 "documentation", "Compute the transformation to express sourceFrame\ninto targetFrame.\n<p>\nFor example\n<ul>\n<li>sourceFrame could be laser scanner</li>\n<li>targetFrame could be the sonar sensor</li>\n</ul>\nThis method can be used to express the point cloud coming out of the\nlaser scanner in the frame of the sonar sensor.\n</p>"
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__FindNodesByDescription__String_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns all Node for which the description matches a specified string.\n@param description The description to match.\n@param root The root node of the topology.\n@return The list of matching Nodes."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__FindNodesByID__String_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns all Node for which the Id matches a specified string.\n@param nodeId The Id to match.\n@param root The root node of the topology.\n@return The list of matching Nodes."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__FindNodesByType__EClass_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns all Node that are matches or are sub-classes of a specified EClass.\n@param clazz The EClass to match.\n@param root The root node of the topology.\n@return The list of matching Nodes."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__FindRoot__Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns the root node (i.e. moves up the topology tree until the ultimate parent node is found) of a specified Node.\n@param node The specified Node.\n@return The root Node."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__DoNodesShareTopologyTree__Node_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns whether or not two specified Nodes shared the same topology tree.\n@param node1 The first Node.\n@param node2 The second Node.\nreturn True if the nodes share the same root Node."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__FindNodePath__Node_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nFinds the list of Node that connects fromNode to toNode.\n@param fromNode The node from which to start.\n@param toNode The destination node.\n@return The list of nodes connecting fromNode to toNode. Never null."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__CreateNodePath__Node_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nCreates a NodePath leading from the fromNode to the toNode.\n@param fromNode The node from which to start.\n@param toNode The destination node. Note that this node MUST be part of the root node descendant.\n@return The NodePath, null if toNode is not a descendant of from Node."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__ResolveNode__Node_NodePath(), 
		   source, 
		   new String[] {
			 "documentation", "*\nResolves the Node referred by a NodePath given the root node.\n@param root The root node to start resolving the node path from.\n@param nodePath The nodePath referring to a Node.\n@return The Node resolve from the NodePath, null is no match is found."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__GetEuclideanDistance__Node_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns the Euclidian (i.e. straight line) distance between two Nodes. The nodes must be part of a common topology tree.\n@param fromNode The first Node.\n@param fromNode The second Node.\nreturn The distance between the specified Nodes, NaN if the specified node do no share the same topology tree."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__GetGeodesicDistance__Node_Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nReturns the geodesic (i.e. along the connecting edges) distance between two Nodes. The nodes must be part of a common topology tree.\n@param fromNode The first Node.\n@param fromNode The second Node.\nreturn The distance between the specified Nodes, NaN if the specified node do no share the same topology tree."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__PrintTopology__Node(), 
		   source, 
		   new String[] {
			 "documentation", "*\nPrints the topology as text to the console.\n@param root The root node of the topology."
		   });	
		addAnnotation
		  (getApogyCommonTopologyFacade__Filter__NodeFilter_Collection(), 
		   source, 
		   new String[] {
			 "documentation", "Applies a NodfeFilter to a list of Nodes.\n@param filter The filter to apply.\n@param nodes The list of Nodes on which to apply the filter.\n@return The list of Node that passes through the filter."
		   });	
		addAnnotation
		  (nodeFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "Abstract base class of all Node filters."
		   });	
		addAnnotation
		  (nodeFilterChainEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nNodeFilter based on a chain of filter."
		   });	
		addAnnotation
		  (nodeTypeFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nNode filter that filters Node based on their class type."
		   });	
		addAnnotation
		  (getNodeTypeFilter_Clazz(), 
		   source, 
		   new String[] {
			 "documentation", "*\nThe class to match."
		   });	
		addAnnotation
		  (regexNodeFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "Base class for NodeFilter that use a regular expression matching as the way to determine if a node passes the filter."
		   });	
		addAnnotation
		  (getRegexNodeFilter_Regex(), 
		   source, 
		   new String[] {
			 "documentation", "The string representing the regular expression that needs to be matched."
		   });	
		addAnnotation
		  (nodeIdFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA Node filter that filters Node based on their nodeId."
		   });	
		addAnnotation
		  (nodeDescriptionFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA Node filter that filters Node based on their description field."
		   });	
		addAnnotation
		  (nodeIsChildOfFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nA Node filter that filter a Node on whether it is under the parent specified parent node."
		   });	
		addAnnotation
		  (getNodeIsChildOfFilter_ParentNode(), 
		   source, 
		   new String[] {
			 "documentation", "The parent node."
		   });	
		addAnnotation
		  (getNodeIsChildOfFilter_ParentNodeIncluded(), 
		   source, 
		   new String[] {
			 "documentation", "Whether or not to include the parent Node itself passes the filter."
		   });
	}

} //ApogyCommonTopologyPackageImpl
