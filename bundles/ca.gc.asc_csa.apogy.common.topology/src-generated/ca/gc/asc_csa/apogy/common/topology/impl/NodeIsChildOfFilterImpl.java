/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodeIsChildOfFilter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node Is Child Of Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.impl.NodeIsChildOfFilterImpl#getParentNode <em>Parent Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.impl.NodeIsChildOfFilterImpl#isParentNodeIncluded <em>Parent Node Included</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NodeIsChildOfFilterImpl extends NodeFilterImpl implements NodeIsChildOfFilter 
{
	/*
	 * List of child Nodes. 
	 */
	private Collection<Node> childNodes = new ArrayList<Node>();
	
	/**
	 * The cached value of the '{@link #getParentNode() <em>Parent Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParentNode()
	 * @generated
	 * @ordered
	 */
	protected Node parentNode;

	/**
	 * The default value of the '{@link #isParentNodeIncluded() <em>Parent Node Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParentNodeIncluded()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PARENT_NODE_INCLUDED_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isParentNodeIncluded() <em>Parent Node Included</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isParentNodeIncluded()
	 * @generated
	 * @ordered
	 */
	protected boolean parentNodeIncluded = PARENT_NODE_INCLUDED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeIsChildOfFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyPackage.Literals.NODE_IS_CHILD_OF_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getParentNode() {
		if (parentNode != null && parentNode.eIsProxy()) {
			InternalEObject oldParentNode = (InternalEObject)parentNode;
			parentNode = (Node)eResolveProxy(oldParentNode);
			if (parentNode != oldParentNode) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE, oldParentNode, parentNode));
			}
		}
		return parentNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node basicGetParentNode() {
		return parentNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setParentNode(Node newParentNode) 
	{
		childNodes.clear();
		
		if(newParentNode != null)
		{
			childNodes.addAll(ApogyCommonTopologyFacade.INSTANCE.findNodesByType(ApogyCommonTopologyPackage.Literals.NODE, newParentNode));
		}
		
		setParentNodeGen(newParentNode);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentNodeGen(Node newParentNode) {
		Node oldParentNode = parentNode;
		parentNode = newParentNode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE, oldParentNode, parentNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isParentNodeIncluded() {
		return parentNodeIncluded;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentNodeIncluded(boolean newParentNodeIncluded) {
		boolean oldParentNodeIncluded = parentNodeIncluded;
		parentNodeIncluded = newParentNodeIncluded;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE_INCLUDED, oldParentNodeIncluded, parentNodeIncluded));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE:
				if (resolve) return getParentNode();
				return basicGetParentNode();
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE_INCLUDED:
				return isParentNodeIncluded();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE:
				setParentNode((Node)newValue);
				return;
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE_INCLUDED:
				setParentNodeIncluded((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE:
				setParentNode((Node)null);
				return;
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE_INCLUDED:
				setParentNodeIncluded(PARENT_NODE_INCLUDED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE:
				return parentNode != null;
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER__PARENT_NODE_INCLUDED:
				return parentNodeIncluded != PARENT_NODE_INCLUDED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (parentNodeIncluded: ");
		result.append(parentNodeIncluded);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean matches(Node node) 
	{
		if(node == getParentNode() && isParentNodeIncluded())
		{
			return true;
		}
		else
		{
			return childNodes.contains(node);
		}				
	}
} //NodeIsChildOfFilterImpl
