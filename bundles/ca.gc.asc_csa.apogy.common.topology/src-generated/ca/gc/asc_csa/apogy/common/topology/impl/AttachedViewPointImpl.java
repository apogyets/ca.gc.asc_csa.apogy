/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.NodePath;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attached View Point</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.impl.AttachedViewPointImpl#isAllowTranslation <em>Allow Translation</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.impl.AttachedViewPointImpl#isAllowRotation <em>Allow Rotation</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.impl.AttachedViewPointImpl#getNode <em>Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.impl.AttachedViewPointImpl#getNodePath <em>Node Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttachedViewPointImpl extends AbstractViewPointImpl implements AttachedViewPoint 
{
	private Node root = null;
	private Node theNode;
	
	/**
	 * The default value of the '{@link #isAllowTranslation() <em>Allow Translation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllowTranslation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ALLOW_TRANSLATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAllowTranslation() <em>Allow Translation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllowTranslation()
	 * @generated
	 * @ordered
	 */
	protected boolean allowTranslation = ALLOW_TRANSLATION_EDEFAULT;

	/**
	 * The default value of the '{@link #isAllowRotation() <em>Allow Rotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllowRotation()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ALLOW_ROTATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAllowRotation() <em>Allow Rotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAllowRotation()
	 * @generated
	 * @ordered
	 */
	protected boolean allowRotation = ALLOW_ROTATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNodePath() <em>Node Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNodePath()
	 * @generated
	 * @ordered
	 */
	protected NodePath nodePath;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttachedViewPointImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyPackage.Literals.ATTACHED_VIEW_POINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAllowTranslation() {
		return allowTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllowTranslation(boolean newAllowTranslation) {
		boolean oldAllowTranslation = allowTranslation;
		allowTranslation = newAllowTranslation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_TRANSLATION, oldAllowTranslation, allowTranslation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAllowRotation() {
		return allowRotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllowRotation(boolean newAllowRotation) {
		boolean oldAllowRotation = allowRotation;
		allowRotation = newAllowRotation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_ROTATION, oldAllowRotation, allowRotation));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Node getNode() {
		Node node = basicGetNode();
		return node != null && node.eIsProxy() ? (Node)eResolveProxy((InternalEObject)node) : node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public Node basicGetNode() 
	{
		if(theNode == null)
		{
			if(getNodePath() != null && root != null)
			{
				theNode = ApogyCommonTopologyFacade.INSTANCE.resolveNode(root, getNodePath());								
			}
		}
		return theNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodePath getNodePath() {
		return nodePath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNodePath(NodePath newNodePath, NotificationChain msgs) {
		NodePath oldNodePath = nodePath;
		nodePath = newNodePath;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH, oldNodePath, newNodePath);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void setNodePath(NodePath newNodePath) 
	{
		setNodePathGen(newNodePath);
		
		// Clear the node to force resolving later.
		theNode = null;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNodePathGen(NodePath newNodePath) {
		if (newNodePath != nodePath) {
			NotificationChain msgs = null;
			if (nodePath != null)
				msgs = ((InternalEObject)nodePath).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH, null, msgs);
			if (newNodePath != null)
				msgs = ((InternalEObject)newNodePath).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH, null, msgs);
			msgs = basicSetNodePath(newNodePath, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH, newNodePath, newNodePath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH:
				return basicSetNodePath(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_TRANSLATION:
				return isAllowTranslation();
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_ROTATION:
				return isAllowRotation();
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE:
				if (resolve) return getNode();
				return basicGetNode();
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH:
				return getNodePath();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_TRANSLATION:
				setAllowTranslation((Boolean)newValue);
				return;
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_ROTATION:
				setAllowRotation((Boolean)newValue);
				return;
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH:
				setNodePath((NodePath)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_TRANSLATION:
				setAllowTranslation(ALLOW_TRANSLATION_EDEFAULT);
				return;
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_ROTATION:
				setAllowRotation(ALLOW_ROTATION_EDEFAULT);
				return;
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH:
				setNodePath((NodePath)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_TRANSLATION:
				return allowTranslation != ALLOW_TRANSLATION_EDEFAULT;
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__ALLOW_ROTATION:
				return allowRotation != ALLOW_ROTATION_EDEFAULT;
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE:
				return basicGetNode() != null;
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT__NODE_PATH:
				return nodePath != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (allowTranslation: ");
		result.append(allowTranslation);
		result.append(", allowRotation: ");
		result.append(allowRotation);
		result.append(')');
		return result.toString();
	}

	@Override
	public void initialize(Node root) 
	{		
		this.root = root;

		if(root != null)
		{
			try
			{
				// Attempt to resolve the node specified by the NodePath.
				if(getNodePath() != null)
				{
					theNode = ApogyCommonTopologyFacade.INSTANCE.resolveNode(root, getNodePath());			
				}
			}
			catch (Throwable t) 
			{		
			}
		}
	}
} //AttachedViewPointImpl
