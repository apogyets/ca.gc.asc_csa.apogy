/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.topology;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract View Point Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * A reference to an AbstractViewPoint.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.AbstractViewPointReference#getAbstractViewPoint <em>Abstract View Point</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getAbstractViewPointReference()
 * @model
 * @generated
 */
public interface AbstractViewPointReference extends EObject {
	/**
	 * Returns the value of the '<em><b>Abstract View Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The AbstractViewPoint.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Abstract View Point</em>' reference.
	 * @see #setAbstractViewPoint(AbstractViewPoint)
	 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getAbstractViewPointReference_AbstractViewPoint()
	 * @model
	 * @generated
	 */
	AbstractViewPoint getAbstractViewPoint();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.AbstractViewPointReference#getAbstractViewPoint <em>Abstract View Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract View Point</em>' reference.
	 * @see #getAbstractViewPoint()
	 * @generated
	 */
	void setAbstractViewPoint(AbstractViewPoint value);

} // AbstractViewPointReference
