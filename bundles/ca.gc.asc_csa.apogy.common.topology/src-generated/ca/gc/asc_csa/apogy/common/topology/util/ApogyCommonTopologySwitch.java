package ca.gc.asc_csa.apogy.common.topology.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.emf.CompositeFilter;
import ca.gc.asc_csa.apogy.common.emf.IFilter;
import ca.gc.asc_csa.apogy.common.emf.Named;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import ca.gc.asc_csa.apogy.common.topology.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc --> * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage
 * @generated
 */
public class ApogyCommonTopologySwitch<T1> extends Switch<T1> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected static ApogyCommonTopologyPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public ApogyCommonTopologySwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyCommonTopologyPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T1 doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyCommonTopologyPackage.TOPOLOGY_PROVIDER: {
				TopologyProvider topologyProvider = (TopologyProvider)theEObject;
				T1 result = caseTopologyProvider(topologyProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.NODE: {
				Node node = (Node)theEObject;
				T1 result = caseNode(node);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.LINK: {
				Link link = (Link)theEObject;
				T1 result = caseLink(link);
				if (result == null) result = caseNode(link);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.LEAF: {
				Leaf leaf = (Leaf)theEObject;
				T1 result = caseLeaf(leaf);
				if (result == null) result = caseNode(leaf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.URL_NODE: {
				URLNode urlNode = (URLNode)theEObject;
				T1 result = caseURLNode(urlNode);
				if (result == null) result = caseLeaf(urlNode);
				if (result == null) result = caseNode(urlNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.CAD_NODE: {
				CADNode cadNode = (CADNode)theEObject;
				T1 result = caseCADNode(cadNode);
				if (result == null) result = caseURLNode(cadNode);
				if (result == null) result = caseLeaf(cadNode);
				if (result == null) result = caseNode(cadNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.CONTENT_NODE: {
				ContentNode<?> contentNode = (ContentNode<?>)theEObject;
				T1 result = caseContentNode(contentNode);
				if (result == null) result = caseLeaf(contentNode);
				if (result == null) result = caseNode(contentNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.REFERENCED_CONTENT_NODE: {
				ReferencedContentNode<?> referencedContentNode = (ReferencedContentNode<?>)theEObject;
				T1 result = caseReferencedContentNode(referencedContentNode);
				if (result == null) result = caseContentNode(referencedContentNode);
				if (result == null) result = caseLeaf(referencedContentNode);
				if (result == null) result = caseNode(referencedContentNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.AGGREGATE_CONTENT_NODE: {
				AggregateContentNode<?> aggregateContentNode = (AggregateContentNode<?>)theEObject;
				T1 result = caseAggregateContentNode(aggregateContentNode);
				if (result == null) result = caseContentNode(aggregateContentNode);
				if (result == null) result = caseLeaf(aggregateContentNode);
				if (result == null) result = caseNode(aggregateContentNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.GROUP_NODE: {
				GroupNode groupNode = (GroupNode)theEObject;
				T1 result = caseGroupNode(groupNode);
				if (result == null) result = caseNode(groupNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.REFERENCED_GROUP_NODE: {
				ReferencedGroupNode referencedGroupNode = (ReferencedGroupNode)theEObject;
				T1 result = caseReferencedGroupNode(referencedGroupNode);
				if (result == null) result = caseGroupNode(referencedGroupNode);
				if (result == null) result = caseNode(referencedGroupNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.AGGREGATE_GROUP_NODE: {
				AggregateGroupNode aggregateGroupNode = (AggregateGroupNode)theEObject;
				T1 result = caseAggregateGroupNode(aggregateGroupNode);
				if (result == null) result = caseGroupNode(aggregateGroupNode);
				if (result == null) result = caseNode(aggregateGroupNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.POSITION_NODE: {
				PositionNode positionNode = (PositionNode)theEObject;
				T1 result = casePositionNode(positionNode);
				if (result == null) result = caseAggregateGroupNode(positionNode);
				if (result == null) result = caseGroupNode(positionNode);
				if (result == null) result = caseNode(positionNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.ROTATION_NODE: {
				RotationNode rotationNode = (RotationNode)theEObject;
				T1 result = caseRotationNode(rotationNode);
				if (result == null) result = caseAggregateGroupNode(rotationNode);
				if (result == null) result = caseGroupNode(rotationNode);
				if (result == null) result = caseNode(rotationNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.TRANSFORM_NODE: {
				TransformNode transformNode = (TransformNode)theEObject;
				T1 result = caseTransformNode(transformNode);
				if (result == null) result = casePositionNode(transformNode);
				if (result == null) result = caseRotationNode(transformNode);
				if (result == null) result = caseAggregateGroupNode(transformNode);
				if (result == null) result = caseGroupNode(transformNode);
				if (result == null) result = caseNode(transformNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.PICK_AND_PLACE_NODE: {
				PickAndPlaceNode pickAndPlaceNode = (PickAndPlaceNode)theEObject;
				T1 result = casePickAndPlaceNode(pickAndPlaceNode);
				if (result == null) result = caseTransformNode(pickAndPlaceNode);
				if (result == null) result = casePositionNode(pickAndPlaceNode);
				if (result == null) result = caseRotationNode(pickAndPlaceNode);
				if (result == null) result = caseAggregateGroupNode(pickAndPlaceNode);
				if (result == null) result = caseGroupNode(pickAndPlaceNode);
				if (result == null) result = caseNode(pickAndPlaceNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.NODE_PATH: {
				NodePath nodePath = (NodePath)theEObject;
				T1 result = caseNodePath(nodePath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.ABSTRACT_VIEW_POINT: {
				AbstractViewPoint abstractViewPoint = (AbstractViewPoint)theEObject;
				T1 result = caseAbstractViewPoint(abstractViewPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.ABSTRACT_VIEW_POINT_REFERENCE: {
				AbstractViewPointReference abstractViewPointReference = (AbstractViewPointReference)theEObject;
				T1 result = caseAbstractViewPointReference(abstractViewPointReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.ARBITRARY_VIEW_POINT: {
				ArbitraryViewPoint arbitraryViewPoint = (ArbitraryViewPoint)theEObject;
				T1 result = caseArbitraryViewPoint(arbitraryViewPoint);
				if (result == null) result = caseAbstractViewPoint(arbitraryViewPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.ATTACHED_VIEW_POINT: {
				AttachedViewPoint attachedViewPoint = (AttachedViewPoint)theEObject;
				T1 result = caseAttachedViewPoint(attachedViewPoint);
				if (result == null) result = caseAbstractViewPoint(attachedViewPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.APOGY_COMMON_TOPOLOGY_FACADE: {
				ApogyCommonTopologyFacade apogyCommonTopologyFacade = (ApogyCommonTopologyFacade)theEObject;
				T1 result = caseApogyCommonTopologyFacade(apogyCommonTopologyFacade);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.NODE_FILTER: {
				NodeFilter nodeFilter = (NodeFilter)theEObject;
				T1 result = caseNodeFilter(nodeFilter);
				if (result == null) result = caseIFilter(nodeFilter);
				if (result == null) result = caseNamed(nodeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.NODE_FILTER_CHAIN: {
				NodeFilterChain nodeFilterChain = (NodeFilterChain)theEObject;
				T1 result = caseNodeFilterChain(nodeFilterChain);
				if (result == null) result = caseCompositeFilter(nodeFilterChain);
				if (result == null) result = caseIFilter(nodeFilterChain);
				if (result == null) result = caseNamed(nodeFilterChain);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.NODE_TYPE_FILTER: {
				NodeTypeFilter nodeTypeFilter = (NodeTypeFilter)theEObject;
				T1 result = caseNodeTypeFilter(nodeTypeFilter);
				if (result == null) result = caseNodeFilter(nodeTypeFilter);
				if (result == null) result = caseIFilter(nodeTypeFilter);
				if (result == null) result = caseNamed(nodeTypeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.REGEX_NODE_FILTER: {
				RegexNodeFilter regexNodeFilter = (RegexNodeFilter)theEObject;
				T1 result = caseRegexNodeFilter(regexNodeFilter);
				if (result == null) result = caseNodeFilter(regexNodeFilter);
				if (result == null) result = caseIFilter(regexNodeFilter);
				if (result == null) result = caseNamed(regexNodeFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.NODE_ID_FILTER: {
				NodeIdFilter nodeIdFilter = (NodeIdFilter)theEObject;
				T1 result = caseNodeIdFilter(nodeIdFilter);
				if (result == null) result = caseRegexNodeFilter(nodeIdFilter);
				if (result == null) result = caseNodeFilter(nodeIdFilter);
				if (result == null) result = caseIFilter(nodeIdFilter);
				if (result == null) result = caseNamed(nodeIdFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.NODE_DESCRIPTION_FILTER: {
				NodeDescriptionFilter nodeDescriptionFilter = (NodeDescriptionFilter)theEObject;
				T1 result = caseNodeDescriptionFilter(nodeDescriptionFilter);
				if (result == null) result = caseRegexNodeFilter(nodeDescriptionFilter);
				if (result == null) result = caseNodeFilter(nodeDescriptionFilter);
				if (result == null) result = caseIFilter(nodeDescriptionFilter);
				if (result == null) result = caseNamed(nodeDescriptionFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyPackage.NODE_IS_CHILD_OF_FILTER: {
				NodeIsChildOfFilter nodeIsChildOfFilter = (NodeIsChildOfFilter)theEObject;
				T1 result = caseNodeIsChildOfFilter(nodeIsChildOfFilter);
				if (result == null) result = caseNodeFilter(nodeIsChildOfFilter);
				if (result == null) result = caseIFilter(nodeIsChildOfFilter);
				if (result == null) result = caseNamed(nodeIsChildOfFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Topology Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Topology Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTopologyProvider(TopologyProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNode(Node object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseLink(Link object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Leaf</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Leaf</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseLeaf(Leaf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>URL Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>URL Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseURLNode(URLNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>CAD Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>CAD Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseCADNode(CADNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Content Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Content Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseContentNode(ContentNode<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referenced Content Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referenced Content Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseReferencedContentNode(ReferencedContentNode<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aggregate Content Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aggregate Content Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseAggregateContentNode(AggregateContentNode<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Group Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Group Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseGroupNode(GroupNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Referenced Group Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Referenced Group Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseReferencedGroupNode(ReferencedGroupNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aggregate Group Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aggregate Group Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAggregateGroupNode(AggregateGroupNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Position Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Position Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 casePositionNode(PositionNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rotation Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rotation Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseRotationNode(RotationNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transform Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transform Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseTransformNode(TransformNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pick And Place Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pick And Place Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 casePickAndPlaceNode(PickAndPlaceNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNodePath(NodePath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract View Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract View Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractViewPoint(AbstractViewPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract View Point Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract View Point Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAbstractViewPointReference(AbstractViewPointReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Arbitrary View Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Arbitrary View Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseArbitraryViewPoint(ArbitraryViewPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attached View Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attached View Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseAttachedViewPoint(AttachedViewPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseApogyCommonTopologyFacade(ApogyCommonTopologyFacade object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNodeFilter(NodeFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Filter Chain</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Filter Chain</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNodeFilterChain(NodeFilterChain object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Type Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Type Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNodeTypeFilter(NodeTypeFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Regex Node Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Regex Node Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseRegexNodeFilter(RegexNodeFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Id Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Id Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNodeIdFilter(NodeIdFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Description Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Description Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNodeDescriptionFilter(NodeDescriptionFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Is Child Of Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Is Child Of Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNodeIsChildOfFilter(NodeIsChildOfFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T1 caseNamed(Named object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IFilter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IFilter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseIFilter(IFilter<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T> T1 caseCompositeFilter(CompositeFilter<T> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T1 defaultCase(EObject object) {
		return null;
	}

} //ApogyCommonTopologySwitch
