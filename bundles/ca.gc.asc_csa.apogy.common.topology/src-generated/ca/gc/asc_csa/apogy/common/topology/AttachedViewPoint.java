/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.topology;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attached View Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Specialized case of AbstractViewPoint that represent a view point which position and orientation are attached to a node in the topology.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint#isAllowTranslation <em>Allow Translation</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint#isAllowRotation <em>Allow Rotation</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint#getNode <em>Node</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint#getNodePath <em>Node Path</em>}</li>
 * </ul>
 *
 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getAttachedViewPoint()
 * @model
 * @generated
 */
public interface AttachedViewPoint extends AbstractViewPoint {
	/**
	 * Returns the value of the '<em><b>Allow Translation</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allow Translation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allow Translation</em>' attribute.
	 * @see #setAllowTranslation(boolean)
	 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getAttachedViewPoint_AllowTranslation()
	 * @model default="false" unique="false"
	 * @generated
	 */
	boolean isAllowTranslation();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint#isAllowTranslation <em>Allow Translation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allow Translation</em>' attribute.
	 * @see #isAllowTranslation()
	 * @generated
	 */
	void setAllowTranslation(boolean value);

	/**
	 * Returns the value of the '<em><b>Allow Rotation</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allow Rotation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allow Rotation</em>' attribute.
	 * @see #setAllowRotation(boolean)
	 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getAttachedViewPoint_AllowRotation()
	 * @model default="false" unique="false"
	 * @generated
	 */
	boolean isAllowRotation();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint#isAllowRotation <em>Allow Rotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allow Rotation</em>' attribute.
	 * @see #isAllowRotation()
	 * @generated
	 */
	void setAllowRotation(boolean value);

	/**
	 * Returns the value of the '<em><b>Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * The node to which the view point is attached.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Node</em>' reference.
	 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getAttachedViewPoint_Node()
	 * @model transient="true" changeable="false" volatile="true"
	 * @generated
	 */
	Node getNode();

	/**
	 * Returns the value of the '<em><b>Node Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * *
	 * A node path referring to the node.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Node Path</em>' containment reference.
	 * @see #setNodePath(NodePath)
	 * @see ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage#getAttachedViewPoint_NodePath()
	 * @model containment="true"
	 * @generated
	 */
	NodePath getNodePath();

	/**
	 * Sets the value of the '{@link ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint#getNodePath <em>Node Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node Path</em>' containment reference.
	 * @see #getNodePath()
	 * @generated
	 */
	void setNodePath(NodePath value);

} // AttachedViewPoint
