package ca.gc.asc_csa.apogy.common.topology.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyFacade;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.PositionNode;
import ca.gc.asc_csa.apogy.common.topology.RotationNode;
import ca.gc.asc_csa.apogy.common.topology.TransformNode;

public class NodeRelativePoseListener 
{
	private Adapter adapter = null;
	private List<Node> nodes = new ArrayList<Node>();

	
	private double minimumDistanceDeltaMeters = 1E-6;
	private double minimumOrientationDeltaRad = 0.01;
	
	private Matrix4d previousPose = null;
	private Matrix4d newPose = null;
	private Node fromNode;
	private Node toNode;
	
	/**
	 * Method called when a change in relative pose is detected. Sub-classes should override this method.
	 * @param newPose The new pose. Can be null if to or from node is null.
	 */
	public void relativePoseChanged(Matrix4d newPose) 
	{		
	}
	
	public Node getFromNode() 
	{
		return fromNode;
	}

	public void setFromNode(Node fromNode) 
	{
		if(this.fromNode != fromNode)
		{
			this.fromNode = fromNode;
			updateNodeList();
			relativePoseChanged(computeNewPose());
		}
	}

	public Node getToNode() 
	{
		return toNode;
	}

	public void setToNode(Node toNode) 
	{
		if(this.toNode != toNode)
		{
			this.toNode = toNode;
			updateNodeList();
			relativePoseChanged(computeNewPose());
		}
	}
	
	public Matrix4d getNewPose() {
		return newPose;
	}

	/**
	 * Dispose of this NodeRelativePoseListener. 
	 * The adapter is removed from all node and the adapter is then set to null.
	 * Should be called when the NodeRelativePoseListener is no longer needed.
	 */
	public void dispose()
	{
		unregisterAdapter();
		fromNode = null;
		toNode = null;
		adapter = null;
	}
	
	private boolean hasPoseChanged(Matrix4d newPose, Matrix4d newPreviousPose)
	{
		boolean result = false;
		if(previousPose == null)
		{
			if(newPose != null)	previousPose = new Matrix4d(newPose);
			result =  true;
		}
		else if(newPose != null && previousPose != null)
		{
			if(!result)
			{
				// First checks is translation has occured.
				Vector3d previousPosition = new Vector3d();
				Vector3d newPosition = new Vector3d();
				
				previousPose.get(previousPosition);
				newPose.get(newPosition);
				
				newPosition.sub(previousPosition);
				if(newPosition.length() > minimumDistanceDeltaMeters)
				{
					result =  true;
				}
			}
			
			if(!result)
			{
				// Then checks if rotation has occured.
				Matrix3d previousRot = new Matrix3d();
				Matrix3d newRot = new Matrix3d();
				
				previousPose.get(previousRot);
				newPose.get(newRot);
				
				Vector3d previousOrientation = new Vector3d(1,0,0);
				Vector3d newOrientation = new Vector3d(1,0,0);
				
				previousRot.transform(previousOrientation);
				newRot.transform(newOrientation);
				
				if(Math.abs(previousOrientation.angle(newOrientation)) > minimumOrientationDeltaRad)
				{
					result =  true;
				}
			}
		}
		
		if(newPose != null)
		{
			previousPose = new Matrix4d(newPose);
		}
		else
		{
			previousPose = new Matrix4d();
			previousPose.setIdentity();
		}
		
		return result;
	}
	
	private void updatePose(Matrix4d newPose)
	{
		if(hasPoseChanged(newPose, previousPose))
		{
			this.newPose = newPose;
			relativePoseChanged(newPose);
		}
	}
	
	private void updateNodeList()
	{
		// Unregister from previous nodes.
		unregisterAdapter();
		
		// Clears list of nodes.
		nodes.clear();
		
		Node currentNode = fromNode;
		while(currentNode != null)
		{
			if(currentNode instanceof PositionNode || 
			   currentNode instanceof RotationNode)
			{
				if(!nodes.contains(currentNode)) nodes.add(currentNode);
			}
			
			currentNode = currentNode.getParent();
		}
							
		// Gets the lineage of to node.
		currentNode = toNode;
		while(currentNode != null)
		{			
			if(currentNode instanceof PositionNode || 
			   currentNode instanceof RotationNode)
			{
				if(!nodes.contains(currentNode)) nodes.add(currentNode);
			}
			
			currentNode = currentNode.getParent();
		}
		
		// Register to new nodes.
		registerAdapter();		
	}
	
	private void registerAdapter()
	{
		for(Node node : nodes)
		{
			node.eAdapters().add(getAdapter());
		}
	}
	
	private void unregisterAdapter()
	{
		for(Node node : nodes)
		{
			node.eAdapters().remove(getAdapter());
		}
	}
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{								
					if(msg.getNotifier() instanceof TransformNode)
					{			
						int featureId = msg.getFeatureID(TransformNode.class);
						if(featureId == ApogyCommonTopologyPackage.TRANSFORM_NODE__POSITION ||
						   featureId == ApogyCommonTopologyPackage.TRANSFORM_NODE__ROTATION_MATRIX)
						{							
							updatePose(computeNewPose());
						}
						else if(featureId == ApogyCommonTopologyPackage.TRANSFORM_NODE__PARENT)
						{
							updateNodeList();
							updatePose(computeNewPose());
						}
					}	
					else if(msg.getNotifier() instanceof PositionNode)
					{			
						int featureId = msg.getFeatureID(PositionNode.class);
						if(featureId == ApogyCommonTopologyPackage.POSITION_NODE__POSITION)
						{
							updatePose(computeNewPose());
						}
						else if(featureId == ApogyCommonTopologyPackage.POSITION_NODE__PARENT)
						{
							updateNodeList();
							updatePose(computeNewPose());
						}
					}					
					else if(msg.getNotifier() instanceof RotationNode)
					{			
						int featureId = msg.getFeatureID(RotationNode.class);
						if(featureId == ApogyCommonTopologyPackage.ROTATION_NODE__ROTATION_MATRIX)
						{
							updatePose(computeNewPose());
						}
						else if(featureId == ApogyCommonTopologyPackage.ROTATION_NODE__PARENT)
						{
							updateNodeList();
							updatePose(computeNewPose());
						}
					}											
				}
			};
		}
		return adapter;
	}
	
	protected Matrix4d computeNewPose()
	{
		if(fromNode != null && toNode != null)
		{
			return ApogyCommonTopologyFacade.INSTANCE.expressInFrame(fromNode, toNode);
		}
		else
		{
			return null;
		}
	}
}
