package ca.gc.asc_csa.apogy.rcp.mainmenu.dialogs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.descriptor.basic.MPartDescriptor;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.MUILabel;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspectiveStack;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainer;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainerElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.basic.MStackElement;
import org.eclipse.e4.ui.model.application.ui.basic.MTrimmedWindow;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartSashContainerImpl;
import org.eclipse.e4.ui.model.application.ui.basic.impl.PartStackImpl;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.IResourceUtilities;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.dialogs.ViewContentProvider;
import org.eclipse.ui.internal.dialogs.ViewLabelProvider;
import org.eclipse.wb.swt.SWTResourceManager;

import ca.gc.asc_csa.apogy.rcp.ApogyRCPConstants;

@SuppressWarnings("restriction")
public class AddViewDialog extends Dialog {

	TreeViewer treeViewerViews;

	private MApplication application;
	private MPerspective perspective;
	private EModelService modelService;

	@Inject
	private EPartService partService;

	private IResourceUtilities<?> resourceUtilities;
	private MTrimmedWindow window;

	@Inject
	public AddViewDialog(@Named(IServiceConstants.ACTIVE_SHELL) Shell parentShell, MApplication application,
			EModelService modelService) {
		super(parentShell);
		setShellStyle(SWT.SHELL_TRIM | SWT.SHEET);
		this.application = application;
		this.resourceUtilities = (IResourceUtilities<?>) PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getService(IResourceUtilities.class);
		this.window = (MTrimmedWindow) modelService.find(ApogyRCPConstants.MAIN_WINDOW__ID, application);
		this.modelService = modelService;
		this.perspective = ((MPerspectiveStack) modelService.find(ApogyRCPConstants.PERSPECTIVE_STACK__ID, window))
				.getSelectedElement();
	}

	@Override
	protected void configureShell(Shell shell) {
		shell.setText("Add View");
		super.configureShell(shell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = new Composite(parent, SWT.None);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		composite.setLayout(new GridLayout(4, false));

		/**
		 * TreeViewer
		 */
		treeViewerViews = new TreeViewer(composite,
				SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.SINGLE);
		Tree treeViews = treeViewerViews.getTree();
		treeViews.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
		treeViews.setLinesVisible(true);
		treeViews.setHeaderVisible(true);
		treeViewerViews.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				getButton(IDialogConstants.OK_ID).setEnabled(canBeAdded());
			}
		});
		treeViewerViews.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				if (getSelectedPart() != null && canBeAdded()) {
					okPressed();
				} else if (treeViewerViews.getSelection() != null) {
					Object selectedObject = ((IStructuredSelection) event.getSelection()).getFirstElement();
					if (treeViewerViews.getExpandedState(selectedObject)) {
						treeViewerViews.collapseToLevel(selectedObject, AbstractTreeViewer.ALL_LEVELS);
					} else {
						treeViewerViews.expandToLevel(selectedObject, 1);
					}

					if (!treeViewerViews.isBusy()) {
						for (TreeColumn treeColumn : treeViewerViews.getTree().getColumns()) {
							treeColumn.pack();
						}
					}
				}
			}
		});
		// Adjusts the columns when the tree is expanded.
		treeViewerViews.getTree().addListener(SWT.Expand, new Listener() {
			public void handleEvent(Event e) {
				TreeItem treeItem = (TreeItem) e.item;
				TreeColumn[] treeColumns = treeItem.getParent().getColumns();
				Display.getCurrent().asyncExec(new Runnable() {

					@Override
					public void run() {
						for (TreeColumn treeColumn : treeColumns)
							treeColumn.pack();
					}
				});

			}
		});

		TreeViewerColumn treeViewerNameColumn = new TreeViewerColumn(treeViewerViews, SWT.NONE);
		TreeColumn treeclmnName = treeViewerNameColumn.getColumn();
		treeclmnName.setWidth(100);
		treeclmnName.setText("Name");

		TreeViewerColumn treeViewerDescriptionColumn = new TreeViewerColumn(treeViewerViews, SWT.NONE);
		TreeColumn treeclmnDescription = treeViewerDescriptionColumn.getColumn();
		treeclmnDescription.setWidth(100);
		treeclmnDescription.setText("Description");

		treeViewerViews.setContentProvider(new ViewsContentProvider(application));
		treeViewerViews.setLabelProvider(new ViewsLabelProvider(application.getContext(), modelService, partService,
				(MWindow) modelService.find(ApogyRCPConstants.MAIN_WINDOW__ID, application),
				SWTResourceManager.getColor(SWT.COLOR_BLACK)));

		treeViewerViews.setInput(application);
		if (!treeViewerViews.isBusy()) {
			for (TreeColumn column : treeViewerViews.getTree().getColumns()) {
				column.pack();
			}
		}

		return composite;
	}

	/**
	 * Method to know if the selected object can be added in a perspective. this
	 * method relies on the RCPConstants.UI_ELEMENT_STANDALONE constant.
	 * 
	 * @return true if the selection can be added, false otherwise
	 */
	private boolean canBeAdded() {
		Object selection = treeViewerViews.getStructuredSelection().getFirstElement();
		// Only includes the supported UI types.
		if (selection instanceof MPerspective || selection instanceof MPartDescriptor) {
			return true;
		}
		if (selection instanceof MPart) {
			MPart part = (MPart) selection;
			if (part.getPersistedState() != null && part.getPersistedState()
					.containsKey(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE)) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected void okPressed() {
		Object selection = treeViewerViews.getStructuredSelection().getFirstElement();
		MUIElement elementToAdd = null;

		/** Clone the element */
		if (selection instanceof MPart || selection instanceof MPerspective) {
			elementToAdd = modelService.cloneElement((MUIElement) selection, null);
		}

		/** If the selection is a E3 view */
		else if (selection instanceof MPartDescriptor) {
			/** Create the part from the descriptor. */
			elementToAdd = partService.createPart(((MPartDescriptor) selection).getElementId());
		}

		EPartService partService = window.getContext().get(EPartService.class);

		/**
		 * MPart
		 */
		if (elementToAdd instanceof MPart) {
			MPartStack partStack = null;
			/** If the perspective is empty */
			if (perspective.getChildren().isEmpty()) {
				/** Add a sash container and a partStack */
				MPartSashContainer container = (MPartSashContainer) modelService.cloneSnippet(application,
						findEmptySnippetID(MPartSashContainer.class), window);
				partStack = (MPartStack) modelService.cloneSnippet(application, findEmptySnippetID(MPartStack.class),
						window);
				makeIDsUnique(perspective, container);
				makeIDsUnique(perspective, partStack);
				container.getChildren().add(partStack);
				perspective.getChildren().add(container);
			} else {
				/** Find the first partStack */
				MUIElement element = partService.getActivePart().getParent();
				if (element instanceof MPartStack) {
					partStack = (MPartStack) element;
				} else {
					List<MPartSashContainerElement> elements = perspective.getChildren();
					partStack = getFirstPartStack(elements);
				}
			}

			/** Make the id unique if it's not an E3 view. */
			if (!(selection instanceof MPartDescriptor)) {
				if (selection instanceof MPart && ((MPart) selection).getContributorURI().equals(
						"bundleclass://org.eclipse.ui.workbench/org.eclipse.ui.internal.e4.compatibility.CompatibilityView")) {
					makeIDsUnique(perspective, elementToAdd);
				}

			}
			/** Add the part to the stack and activate it. */
			partStack.getChildren().add((MStackElement) elementToAdd);
			partService.showPart((MPart) elementToAdd, PartState.ACTIVATE);
		}

		/**
		 * MPerspective
		 */
		else if (elementToAdd instanceof MPerspective) {
			/** Get the perspective and the copy */
			MPerspective perspectiveToAdd = (MPerspective) elementToAdd;

			/** Get unique IDs number */
			int j = getUniqueNumberID(perspectiveToAdd);

			/** Get all elements */
			List<MUIElement> partSashElementToAdd = new ArrayList<>();
			partSashElementToAdd.addAll(modelService.findElements(perspectiveToAdd, null, MPart.class, null));
			partSashElementToAdd
					.addAll(modelService.findElements(perspectiveToAdd, null, MPartSashContainer.class, null));
			partSashElementToAdd.addAll(modelService.findElements(perspectiveToAdd, null, MPartStack.class, null));

			for (MUIElement element : partSashElementToAdd) {
				if (!(element instanceof MPart) || !(element instanceof MPart && ((MPart) element).getContributionURI()
						.equals("bundleclass://org.eclipse.ui.workbench/org.eclipse.ui.internal.e4.compatibility.CompatibilityView"))) {
					/** Make the IDs unique */
					element.setElementId(element.getElementId() + "_" + j + "_" + perspective.getElementId());
				}
			}

			/** Add all the elements to the active perspective */
			for (Iterator<MPartSashContainerElement> ite = perspectiveToAdd.getChildren().iterator(); ite.hasNext();) {
				MPartSashContainerElement element = ite.next();
				ite.remove();
				splitPerspective(perspective).getChildren().add(element);
			}
		}
		super.okPressed();
	}

	/**
	 * Gets a unique number for every {@link MPart}, {@link MPartSashContainer}
	 * and {@link MPartStack} in a {@link MPerspective} to have all unique Ids.
	 */
	private int getUniqueNumberID(MPerspective perspective) {
		/** Copy the perspective */
		MPerspective perspectiveToAddCopy = (MPerspective) modelService.cloneElement(perspective, null);

		List<MPartSashContainerElement> elements = perspectiveToAddCopy.getChildren();
		List<MUIElement> partSashElements = new ArrayList<>();

		/** Get all the elements */
		for (Iterator<MPartSashContainerElement> ite = elements.iterator(); ite.hasNext();) {
			MPartSashContainerElement element = ite.next();
			partSashElements.addAll(modelService.findElements(element, null, MPart.class, null));
			partSashElements.addAll(modelService.findElements(element, null, MPartSashContainer.class, null));
			partSashElements.addAll(modelService.findElements(element, null, MPartStack.class, null));
		}

		int i = 1;

		for (MUIElement element : elements) {
			String originalID = element.getElementId();
			element.setElementId(originalID + "_" + i);
			/** Incrementally add a number */
			while (recursiveFindSameID(element, this.perspective)) {
				i++;
				element.setElementId(originalID + "_" + i);
			}
		}

		return i;
	}

	/**
	 * Makes the id of the element unique in the perspective.
	 */
	private void makeIDsUnique(MPerspective perspective, MUIElement element) {
		int j = 0;
		String originalID = element.getElementId();
		/** Incrementally add a number */
		while (recursiveFindSameID(element, perspective)) {
			j++;
			element.setElementId(originalID.concat("_" + Integer.toString(j)));
		}

		if (element instanceof MElementContainer<?>) {
			for (MPart part : modelService.findElements(element, null, MPart.class, null)) {
				makeIDsUnique(perspective, part);
			}
		}

		if (element instanceof MPart || element instanceof MPartSashContainer || element instanceof MPartStack) {
			element.setElementId(element.getElementId().concat("_" + perspective.getElementId()));
		}
	}

	/**
	 * Specifies if there is an element in the container that has the same ID as
	 * the specified element.
	 */
	private boolean recursiveFindSameID(MUIElement element, MElementContainer<? extends MUIElement> container) {
		for (MUIElement containedElement : container.getChildren()) {
			if (containedElement.getElementId().startsWith(element.getElementId())) {
				return true;
			}
			if (containedElement instanceof MElementContainer<?>) {
				if (recursiveFindSameID(element, (MElementContainer<?>) containedElement)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Finds the empty {@link MUIElement}'s ID corresponding to the wanted type.
	 */
	private String findEmptySnippetID(Class<?> clazz) {
		for (MUIElement element : application.getSnippets()) {
			if (clazz.isAssignableFrom(element.getClass())) {
				return element.getElementId();
			}
		}
		return "";
	}

	/**
	 * Places the elements of a {@link MPerspective} in
	 * {@link MPartSashContainer}s to be able to add a new element.
	 * 
	 * @param perspective
	 *            the perspective to split
	 * @return Reference to the {@link MPartSashContainer} to use to add the new
	 *         element.
	 */
	private MPartSashContainer splitPerspective(MPerspective perspective) {
		List<MPartSashContainerElement> children = new ArrayList<>(perspective.getChildren());
		/** If the perspective is empty add a sashContainer */
		if (children.size() == 0) {
			MPartSashContainer sashContainer = (MPartSashContainer) modelService.cloneSnippet(application,
					findEmptySnippetID(MPartSashContainer.class), window);
			makeIDsUnique(perspective, sashContainer);
			perspective.getChildren().add(sashContainer);
			return sashContainer;
		} else {
			int i = 0;
			if (children.size() != 1) {
				i = children.size() - 1;
			}
			/** Create a sash sashContainer */
			MPartSashContainer sashContainer = (MPartSashContainer) modelService.cloneSnippet(application,
					findEmptySnippetID(MPartSashContainer.class), window);
			makeIDsUnique(perspective, sashContainer);

			/**
			 * Place the elements in the perspective in the new sashContainer
			 */
			perspective.getChildren().remove(children.get(i));
			sashContainer.getChildren().add(children.get(i));

			/** Place the sashContainer in the perspective */
			perspective.getChildren().add(sashContainer);

			return sashContainer;
		}

	}

	/**
	 * Gets the part selected in the {@link TreeViewer}.
	 * 
	 * @return {@link MPart}
	 */
	public MPart getSelectedPart() {
		if (treeViewerViews.getStructuredSelection() != null) {
			/**
			 * Directly return the MPart if the selected item is in the model
			 */
			if (treeViewerViews.getStructuredSelection().getFirstElement() instanceof MPart) {
				return ((MPart) treeViewerViews.getStructuredSelection().getFirstElement());
			}
			/**
			 * Create and return the part if the selected item is an eclipse 3
			 * view.
			 */
			if (treeViewerViews.getStructuredSelection().getFirstElement() instanceof MPartDescriptor) {
				return partService.createPart(
						((MPartDescriptor) treeViewerViews.getStructuredSelection().getFirstElement()).getElementId());
			}
		}
		return null;
	}

	/**
	 * Gets the first available {@link MPartStack} available in the element.
	 * 
	 * @param element
	 *            reference to the {@link MPartSashContainerElement}.
	 * @return {@link MPartStack}
	 */
	private MPartStack getFirstPartStack(MPartSashContainerElement element) {
		if (element instanceof PartStackImpl) {
			return (PartStackImpl) element;
		}
		if (element instanceof PartSashContainerImpl) {
			return getFirstPartStack(((PartSashContainerImpl) element).getChildren());
		}

		return null;
	}

	/**
	 * Recursive function to get the first available {@link MPartStack}
	 * available in the elements list.
	 * 
	 * @param element
	 *            reference to the {@link List<MPartSashContainerElement>}.
	 * @return {@link MPartStack}
	 */
	private MPartStack getFirstPartStack(List<MPartSashContainerElement> elements) {
		MPartStack partStack = null;
		for (MPartSashContainerElement elementChild : elements) {
			if (!(elementChild instanceof MWindow)) {
				partStack = getFirstPartStack(elementChild);
				if (partStack != null) {
					break;
				}
			}

		}
		return partStack;
	}

	/**
	 * Label provider for the {@link TreeViewer}.
	 */
	private class ViewsLabelProvider extends ViewLabelProvider implements ITableLabelProvider {

		private final int NAME_COLUMN = 0;
		private final int DESCRIPTION_COLUMN = 1;

		public ViewsLabelProvider(IEclipseContext context, EModelService modelService, EPartService partService,
				MWindow window, Color dimmedForeground) {
			super(context, modelService, partService, window, dimmedForeground);
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			switch (columnIndex) {
			case NAME_COLUMN:
				if (element instanceof MUILabel || element instanceof MPartSashContainer) {
					String iconURI = "";
					if (element instanceof MUILabel) {
						iconURI = ((MUILabel) element).getIconURI();
					}
					if (iconURI != null && iconURI != "") {
						URI uri = URI.createURI(iconURI);
						if (uri != null) {
							ImageDescriptor descriptor = (ImageDescriptor) resourceUtilities
									.imageDescriptorFromURI(uri);
							return descriptor.createImage();
						}
					} else {
						/**
						 * Gets the default image if there is no icon.
						 */
						URI uri = URI
								.createURI("platform:/plugin/org.eclipse.ui/icons/full/eview16/defaultview_misc.png");
						ImageDescriptor descriptor = (ImageDescriptor) resourceUtilities.imageDescriptorFromURI(uri);
						return descriptor.createImage();
					}

				}
				return super.getImage(element);
			default:
				return null;
			}

		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			switch (columnIndex) {
			case NAME_COLUMN:
				if (element instanceof String) {
					return (String) element;
				} else if (element instanceof MUILabel) {
					return ((MUILabel) element).getLabel();
				}
			case DESCRIPTION_COLUMN:
				if (element instanceof String) {
					return "";
				} else if (element instanceof MUILabel) {
					return ((MUILabel) element).getTooltip();
				}
			default:
				return "";
			}
		}

	}

	/**
	 * Content provider for the {@link TreeViewer}.
	 */
	private class ViewsContentProvider extends ViewContentProvider {

		private final String APOGY_PERSPECTIVE_STR = "Apogy Perspectives";
		private final String APOGY_PART_STR = "Apogy Views";

		public ViewsContentProvider(MApplication application) {
			super(application);
		}

		@Override
		public Object[] getElements(Object element) 
		{
			List<Object> elements = new ArrayList<>();

			/**
			 * Add the models shared elements.
			 */
			elements.addAll(Arrays.asList(super.getElements(element)));			
			elements = sortAlphabetically(elements);
				
			// Re-order the element in alphabetical order.							
			elements.add(0, APOGY_PART_STR);
			elements.add(0, APOGY_PERSPECTIVE_STR);						
			
			
			return elements.toArray();
		}

		@Override
		public Object[] getChildren(Object element) 
		{
			System.out.println("AddViewDialog.ViewsContentProvider.getChildren()");
			
			if (element instanceof String) 
			{
				/**
				 * Gets the models shared elements.
				 */
				if ((String) element == APOGY_PERSPECTIVE_STR || (String) element == APOGY_PART_STR) {
					List<MPerspective> perspectives = new ArrayList<MPerspective>();
					List<MPart> parts = new ArrayList<MPart>();

					// Get the snippets
					for (Iterator<MUIElement> ite = application.getSnippets().iterator(); ite.hasNext();) 
					{
						MUIElement muiElement = ite.next();
						if (muiElement instanceof MPerspective) 
						{
							if (muiElement.getPersistedState() != null
									&& muiElement.getPersistedState()
											.containsKey(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE)
									&& Boolean.parseBoolean(muiElement.getPersistedState()
											.get(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE))) 
							{
								perspectives.add((MPerspective) muiElement);
								for (MPart part : modelService.findElements(muiElement, null, MPart.class, null)) 
								{
									if (part.getPersistedState() != null
											&& part.getPersistedState().containsKey(
													ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE)
											&& Boolean.parseBoolean(part.getPersistedState()
													.get(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE))) 
									{
										parts.add(part);
									}
								}
							}
						} 
						else if (muiElement instanceof MPart) 
						{
							MPart part = (MPart) muiElement;
							if (part.getPersistedState() != null
									&& part.getPersistedState()
											.containsKey(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE)
									&& Boolean.parseBoolean(part.getPersistedState()
											.get(ApogyRCPConstants.PERSISTED_STATE__MUIELEMENT__STANDALONE))) {
								parts.add(part);
							}
						}
					}

					if ((String) element == APOGY_PERSPECTIVE_STR) 
					{						
						// Sort Perspective alphabetically.
						List<MPerspective> sorted = sortMPerspectiveAlphabetically(perspectives);
						return sorted.toArray();
					} 
					else if ((String) element == APOGY_PART_STR) 
					{
						// Sort Parts alphabetically.
						List<MPart> sortedParts = sortMPartsAlphabetically(parts);
						return sortedParts.toArray();						
					}
				}
			}
			if (element instanceof MPerspective) 
			{
				List<MPart> parts = modelService.findElements((MPerspective) element, null, MPart.class, null);				
				List<MPart> sortedParts = sortMPartsAlphabetically(parts);
				return sortedParts.toArray();				
			}
			return super.getChildren(element);
		}

		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof MPerspective) {
				return !((MPerspective) element).getChildren().isEmpty();
			}
			return super.hasChildren(element);
		}
		
		private List<MPerspective> sortMPerspectiveAlphabetically(List<MPerspective> mUIElements)
		{
			SortedSet<MPerspective> sorted = new TreeSet<MPerspective>(new Comparator<MPerspective>() 
			{
				@Override
				public int compare(MPerspective arg0, MPerspective arg1) 
				{									
					if(arg0.getLabel() != null && arg1.getLabel() != null)
					{
						if(arg0.getLabel().compareTo(arg1.getLabel()) > 0)
						{
							return 1;
						}
						else if(arg0.getLabel().compareTo(arg1.getLabel()) < 0)
						{
							return -1;
						}
						else
						{
							if(arg0.hashCode() > arg1.hashCode())
							{
								return 1;
							}
							else
							{
								return -1;
							}
						}
					}
					
					if(arg0.hashCode() > arg1.hashCode())
					{
						return 1;
					}
					else
					{
						return -1;
					}																									
				}
			});		
			sorted.addAll(mUIElements);														
			
			List<MPerspective> result = new ArrayList<MPerspective>();
			result.addAll(sorted);
							
			return result;
		}
		
		private List<MPart> sortMPartsAlphabetically(List<MPart> mParts)
		{
			SortedSet<MPart> sorted = new TreeSet<MPart>(new Comparator<MPart>() 
			{
				@Override
				public int compare(MPart arg0, MPart arg1) 
				{
					if(arg0.getLabel() != null && arg1.getLabel() != null)
					{
						if(arg0.getLabel().compareTo(arg1.getLabel()) > 0)
						{
							return 1;
						}
						else if(arg0.getLabel().compareTo(arg1.getLabel()) < 0)
						{
							return -1;
						}
						else
						{
							if(arg0.hashCode() > arg1.hashCode())
							{
								return 1;
							}
							else
							{
								return -1;
							}
						}
					}
					
					if(arg0.hashCode() > arg1.hashCode())
					{
						return 1;
					}
					else
					{
						return -1;
					}											
				}
			});		
			sorted.addAll(mParts);														
			
			List<MPart> result = new ArrayList<MPart>();
			result.addAll(sorted);
			return result;
		}
		
		private List<Object> sortAlphabetically(List<Object> elements)
		{
			SortedSet<Object> sorted = new TreeSet<Object>();		
			sorted.addAll(elements);														
			
			List<Object> result = new ArrayList<Object>();
			result.addAll(sorted);
			return result;
		}
	}
}
