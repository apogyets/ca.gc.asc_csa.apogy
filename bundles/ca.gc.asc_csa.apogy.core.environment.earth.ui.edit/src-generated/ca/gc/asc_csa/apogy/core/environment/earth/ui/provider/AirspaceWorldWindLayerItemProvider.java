/**
 * *******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Canadian Space Agency (CSA) - Initial API and implementation
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
 *           
 * *******************************************************************************
 */
package ca.gc.asc_csa.apogy.core.environment.earth.ui.provider;


import ca.gc.asc_csa.apogy.core.environment.earth.ApogyEarthEnvironmentFactory;

import ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer;
import ca.gc.asc_csa.apogy.core.environment.earth.ui.ApogyEarthEnvironmentUIPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.core.environment.earth.ui.AirspaceWorldWindLayer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AirspaceWorldWindLayerItemProvider extends AbstractWorldWindLayerItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AirspaceWorldWindLayerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addLowerAltitudePropertyDescriptor(object);
			addUpperAltitudePropertyDescriptor(object);
			addUrlPropertyDescriptor(object);
			addColorPropertyDescriptor(object);
			addOpacityPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Lower Altitude feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLowerAltitudePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AirspaceWorldWindLayer_lowerAltitude_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AirspaceWorldWindLayer_lowerAltitude_feature", "_UI_AirspaceWorldWindLayer_type"),
				 ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Upper Altitude feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUpperAltitudePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AirspaceWorldWindLayer_upperAltitude_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AirspaceWorldWindLayer_upperAltitude_feature", "_UI_AirspaceWorldWindLayer_type"),
				 ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Url feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUrlPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AirspaceWorldWindLayer_url_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AirspaceWorldWindLayer_url_feature", "_UI_AirspaceWorldWindLayer_type"),
				 ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__URL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Color feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AirspaceWorldWindLayer_color_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AirspaceWorldWindLayer_color_feature", "_UI_AirspaceWorldWindLayer_type"),
				 ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__COLOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Opacity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOpacityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AirspaceWorldWindLayer_opacity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AirspaceWorldWindLayer_opacity_feature", "_UI_AirspaceWorldWindLayer_type"),
				 ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__OPACITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns AirspaceWorldWindLayer.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AirspaceWorldWindLayer"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((AirspaceWorldWindLayer)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_AirspaceWorldWindLayer_type") :
			getString("_UI_AirspaceWorldWindLayer_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AirspaceWorldWindLayer.class)) {
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__LOWER_ALTITUDE:
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__UPPER_ALTITUDE:
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__URL:
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__COLOR:
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__OPACITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ApogyEarthEnvironmentUIPackage.AIRSPACE_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST,
				 ApogyEarthEnvironmentFactory.eINSTANCE.createGeographicCoordinates()));

		newChildDescriptors.add
			(createChildParameter
				(ApogyEarthEnvironmentUIPackage.Literals.AIRSPACE_WORLD_WIND_LAYER__GEOGRAPHIC_COORDINATES_LIST,
				 ApogyEarthEnvironmentFactory.eINSTANCE.createEarthSurfaceLocation()));
	}

}
