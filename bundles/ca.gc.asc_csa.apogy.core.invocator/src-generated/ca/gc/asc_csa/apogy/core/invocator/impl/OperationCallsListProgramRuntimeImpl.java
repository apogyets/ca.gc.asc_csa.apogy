/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.invocator.impl;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.Diagnostician;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorFacade;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.OperationCall;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsList;
import ca.gc.asc_csa.apogy.core.invocator.OperationCallsListProgramRuntime;
import ca.gc.asc_csa.apogy.core.invocator.ProgramRuntimeState;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Operation Calls List Program Runtime</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.impl.OperationCallsListProgramRuntimeImpl#getIndexLastExecuted <em>Index Last Executed</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.impl.OperationCallsListProgramRuntimeImpl#getIndexCurrentlyExecuted <em>Index Currently Executed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationCallsListProgramRuntimeImpl extends AbstractProgramRuntimeImpl
		implements OperationCallsListProgramRuntime {
	/**
	 * The default value of the '{@link #getIndexLastExecuted() <em>Index Last Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexLastExecuted()
	 * @generated
	 * @ordered
	 */
	protected static final int INDEX_LAST_EXECUTED_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getIndexLastExecuted() <em>Index Last Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexLastExecuted()
	 * @generated
	 * @ordered
	 */
	protected int indexLastExecuted = INDEX_LAST_EXECUTED_EDEFAULT;

	/**
	 * The default value of the '{@link #getIndexCurrentlyExecuted() <em>Index Currently Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexCurrentlyExecuted()
	 * @generated
	 * @ordered
	 */
	protected static final int INDEX_CURRENTLY_EXECUTED_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getIndexCurrentlyExecuted() <em>Index Currently Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIndexCurrentlyExecuted()
	 * @generated
	 * @ordered
	 */
	protected int indexCurrentlyExecuted = INDEX_CURRENTLY_EXECUTED_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public OperationCallsListProgramRuntimeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorPackage.Literals.OPERATION_CALLS_LIST_PROGRAM_RUNTIME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIndexLastExecuted() {
		return indexLastExecuted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexLastExecuted(int newIndexLastExecuted) {
		int oldIndexLastExecuted = indexLastExecuted;
		indexLastExecuted = newIndexLastExecuted;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_LAST_EXECUTED, oldIndexLastExecuted, indexLastExecuted));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIndexCurrentlyExecuted() {
		return indexCurrentlyExecuted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIndexCurrentlyExecuted(int newIndexCurrentlyExecuted) {
		int oldIndexCurrentlyExecuted = indexCurrentlyExecuted;
		indexCurrentlyExecuted = newIndexCurrentlyExecuted;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_CURRENTLY_EXECUTED, oldIndexCurrentlyExecuted, indexCurrentlyExecuted));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_LAST_EXECUTED:
				return getIndexLastExecuted();
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_CURRENTLY_EXECUTED:
				return getIndexCurrentlyExecuted();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_LAST_EXECUTED:
				setIndexLastExecuted((Integer)newValue);
				return;
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_CURRENTLY_EXECUTED:
				setIndexCurrentlyExecuted((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_LAST_EXECUTED:
				setIndexLastExecuted(INDEX_LAST_EXECUTED_EDEFAULT);
				return;
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_CURRENTLY_EXECUTED:
				setIndexCurrentlyExecuted(INDEX_CURRENTLY_EXECUTED_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_LAST_EXECUTED:
				return indexLastExecuted != INDEX_LAST_EXECUTED_EDEFAULT;
			case ApogyCoreInvocatorPackage.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_CURRENTLY_EXECUTED:
				return indexCurrentlyExecuted != INDEX_CURRENTLY_EXECUTED_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (indexLastExecuted: ");
		result.append(indexLastExecuted);
		result.append(", indexCurrentlyExecuted: ");
		result.append(indexCurrentlyExecuted);
		result.append(')');
		return result.toString();
	}

	@Override
	public void init() {
		if (getProgram() instanceof OperationCallsList) {
			/** Validate */
			Diagnostic diagnosticContext = Diagnostician.INSTANCE.validate(getProgram());
			if (diagnosticContext.getSeverity() == Diagnostic.OK) {
				/** Set to initialized */
				ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
						ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
						ProgramRuntimeState.INITIALIZED, true);
				return;
			}
		}

		/** If it can't be executed */
		ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
				ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.NOT_INITIALIZED,
				true);
	}

	@Override
	public void terminate() {
		/** If running */
		if (getState() == ProgramRuntimeState.RUNNING || getState() == ProgramRuntimeState.RUNNING_SUSPENDED) {
			/** Set to running_terminated */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
					ProgramRuntimeState.RUNNING_TERMINATED, true);
		} else {
			/** Set to terminated */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.TERMINATED,
					true);
		}
	}

	@Override
	public void resume() {
		/** If the runtime is suspended */
		if (getState() == ProgramRuntimeState.SUSPENDED) {
			/** Set to initialized */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.INITIALIZED,
					true);
		}

		if (getState() != ProgramRuntimeState.INITIALIZED) {
			/** Try to init */
			init();
			/** If init worked */
			if (getState() != ProgramRuntimeState.INITIALIZED) {
				stepOver();
			}
		} else {
			stepOver();
		}
	}

	@Override
	public void suspend() {
		/** If running */
		if (getState() == ProgramRuntimeState.RUNNING) {
			/** Set to running_suspended */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
					ProgramRuntimeState.RUNNING_SUSPENDED, true);
		} else if (getState() != ProgramRuntimeState.RUNNING_TERMINATED){
			/** Set to suspended */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.SUSPENDED,
					true);
		}
	}

	@Override
	public void stepInto() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void stepOver() 
	{
		/** If the last executed opCall is the last of the list */
		if (indexLastExecuted == getOperationCallsList().getOperationCalls().size() - 1) 
		{
			/** Terminate */
			ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
					ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
					ProgramRuntimeState.TERMINATED, true);
		}
		/** If not terminated */
		else if (getState() != ProgramRuntimeState.TERMINATED) 
		{
			/** If suspended */
			if (getState() == ProgramRuntimeState.SUSPENDED) 
			{
				/** Set to running_suspended */
				ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
						ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
						ProgramRuntimeState.RUNNING_SUSPENDED, true);
			} 
			else 
			{
				/** Set to running */
				ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
						ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE, ProgramRuntimeState.RUNNING,
						true);
			}

			/** Create the execution job */
			Job job = new Job("Executing OperationCallsList") {
				@Override
				protected IStatus run(IProgressMonitor monitor) 
				{
					/** Get the next opCall to execute */
					OperationCall next = getOperationCallsList().getOperationCalls().get(indexLastExecuted + 1);

					/** Execute and set the last executed opsCall */
					ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
							ApogyCoreInvocatorPackage.Literals.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_CURRENTLY_EXECUTED,
							getOperationCallsList().getOperationCalls().indexOf(next), true);
					ApogyCoreInvocatorFacade.INSTANCE.exec(next);
					/**
					 * If the state has been changed to terminated because of a
					 * synchronization error, transactions exceptions can occur,
					 * this fixes it.
					 */
					if (OperationCallsListProgramRuntimeImpl.this.getState() != ProgramRuntimeState.TERMINATED) {
						ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
								ApogyCoreInvocatorPackage.Literals.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_CURRENTLY_EXECUTED,
								-1);

						/** Set last executed */
						ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
								ApogyCoreInvocatorPackage.Literals.OPERATION_CALLS_LIST_PROGRAM_RUNTIME__INDEX_LAST_EXECUTED,
								getOperationCallsList().getOperationCalls().indexOf(next));
					}
					/** If running*/
					if(OperationCallsListProgramRuntimeImpl.this.getState() == ProgramRuntimeState.RUNNING_TERMINATED){
						/** Terminate */
						ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
								ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
								ProgramRuntimeState.TERMINATED, true);
					}
					/** Otherwise, if running_suspended */
					else if (OperationCallsListProgramRuntimeImpl.this.getState() == ProgramRuntimeState.RUNNING_SUSPENDED) {
						/** If last opCall */
						if (next == getOperationCallsList().getOperationCalls()
								.get(getOperationCallsList().getOperationCalls().size() - 1)) {
							/** Terminate */
							ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
									ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
									ProgramRuntimeState.TERMINATED, true);
						}
						/** Otherwise */
						else {
							/** Set to suspended */
							ApogyCommonTransactionFacade.INSTANCE.basicSet(OperationCallsListProgramRuntimeImpl.this,
									ApogyCoreInvocatorPackage.Literals.ABSTRACT_PROGRAM_RUNTIME__STATE,
									ProgramRuntimeState.SUSPENDED, true);
						}

					}
					/** Otherwise if running */
					else if (OperationCallsListProgramRuntimeImpl.this.getState() == ProgramRuntimeState.RUNNING) {
						/** stepOver to next opCall */
						stepOver();
					}
					return Status.OK_STATUS;
				}
			};
			/** Schedule the job */
			job.schedule();
		}
	}

	@Override
	public void stepReturn() throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	private OperationCallsList getOperationCallsList() {
		if (getProgram() instanceof OperationCallsList) {
			return (OperationCallsList) getProgram();
		}
		return null;
	}

} // OperationCallsListProgramRuntimeImpl