/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.core.invocator.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ca.gc.asc_csa.apogy.core.invocator.AbstractInitialConditions;
import ca.gc.asc_csa.apogy.core.invocator.AbstractInitializationData;
import ca.gc.asc_csa.apogy.core.invocator.ApogyCoreInvocatorPackage;
import ca.gc.asc_csa.apogy.core.invocator.TypeMember;
import ca.gc.asc_csa.apogy.core.invocator.TypeMemberInitialConditions;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Initial Conditions</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.impl.AbstractInitialConditionsImpl#getAbstractInitializationData <em>Abstract Initialization Data</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.invocator.impl.AbstractInitialConditionsImpl#getTypeMembersInitialConditions <em>Type Members Initial Conditions</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractInitialConditionsImpl extends MinimalEObjectImpl.Container implements AbstractInitialConditions {
	/**
	 * The cached value of the '{@link #getAbstractInitializationData() <em>Abstract Initialization Data</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractInitializationData()
	 * @generated
	 * @ordered
	 */
	protected AbstractInitializationData abstractInitializationData;

	/**
	 * The cached value of the '{@link #getTypeMembersInitialConditions() <em>Type Members Initial Conditions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeMembersInitialConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<TypeMemberInitialConditions> typeMembersInitialConditions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractInitialConditionsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCoreInvocatorPackage.Literals.ABSTRACT_INITIAL_CONDITIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractInitializationData getAbstractInitializationData() {
		return abstractInitializationData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAbstractInitializationData(AbstractInitializationData newAbstractInitializationData, NotificationChain msgs) {
		AbstractInitializationData oldAbstractInitializationData = abstractInitializationData;
		abstractInitializationData = newAbstractInitializationData;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA, oldAbstractInitializationData, newAbstractInitializationData);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractInitializationData(AbstractInitializationData newAbstractInitializationData) {
		if (newAbstractInitializationData != abstractInitializationData) {
			NotificationChain msgs = null;
			if (abstractInitializationData != null)
				msgs = ((InternalEObject)abstractInitializationData).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA, null, msgs);
			if (newAbstractInitializationData != null)
				msgs = ((InternalEObject)newAbstractInitializationData).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA, null, msgs);
			msgs = basicSetAbstractInitializationData(newAbstractInitializationData, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA, newAbstractInitializationData, newAbstractInitializationData));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TypeMemberInitialConditions> getTypeMembersInitialConditions() {
		if (typeMembersInitialConditions == null) {
			typeMembersInitialConditions = new EObjectContainmentEList<TypeMemberInitialConditions>(TypeMemberInitialConditions.class, this, ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__TYPE_MEMBERS_INITIAL_CONDITIONS);
		}
		return typeMembersInitialConditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public TypeMemberInitialConditions getTypeMemberInitialConditionsFor(TypeMember typeMember) 
	{
		TypeMemberInitialConditions result = null;
		
		Iterator<TypeMemberInitialConditions> it = getTypeMembersInitialConditions().iterator();
		while(it.hasNext() && result == null)
		{
			TypeMemberInitialConditions typeMemberInitialConditions = it.next();
			if(typeMemberInitialConditions.getTypeMember() == typeMember)
			{
				result = typeMemberInitialConditions;
			}
		}
		
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA:
				return basicSetAbstractInitializationData(null, msgs);
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__TYPE_MEMBERS_INITIAL_CONDITIONS:
				return ((InternalEList<?>)getTypeMembersInitialConditions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA:
				return getAbstractInitializationData();
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__TYPE_MEMBERS_INITIAL_CONDITIONS:
				return getTypeMembersInitialConditions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA:
				setAbstractInitializationData((AbstractInitializationData)newValue);
				return;
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__TYPE_MEMBERS_INITIAL_CONDITIONS:
				getTypeMembersInitialConditions().clear();
				getTypeMembersInitialConditions().addAll((Collection<? extends TypeMemberInitialConditions>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA:
				setAbstractInitializationData((AbstractInitializationData)null);
				return;
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__TYPE_MEMBERS_INITIAL_CONDITIONS:
				getTypeMembersInitialConditions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__ABSTRACT_INITIALIZATION_DATA:
				return abstractInitializationData != null;
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS__TYPE_MEMBERS_INITIAL_CONDITIONS:
				return typeMembersInitialConditions != null && !typeMembersInitialConditions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCoreInvocatorPackage.ABSTRACT_INITIAL_CONDITIONS___GET_TYPE_MEMBER_INITIAL_CONDITIONS_FOR__TYPEMEMBER:
				return getTypeMemberInitialConditionsFor((TypeMember)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //AbstractInitialConditionsImpl
