package ca.gc.asc_csa.apogy.addons.geometry.paths.ui.composites;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.addons.geometry.paths.WayPointPath;

public class WayPointPathComboComposite extends Composite 
{
	private ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private Collection<WayPointPath> list = new ArrayList<WayPointPath>();
	private WayPointPath selectedWayPointPath;
	private ComboViewer comboViewer;
	
	public WayPointPathComboComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new FillLayout());
		comboViewer = createCombo(this, SWT.READ_ONLY);		
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{		
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{	
				if(event.getSelection().isEmpty())
				{
					newSelection(null);
				}
				else if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					if(iStructuredSelection.getFirstElement() instanceof WayPointPath)
					{
						newSelection((WayPointPath)iStructuredSelection.getFirstElement());
					}
					else
					{
						newSelection(null);
					}
				}
			}
		});				
	}
	
	private ComboViewer createCombo(Composite parent, int style)
	{
		ComboViewer comboViewer = new ComboViewer(parent, SWT.DROP_DOWN);		
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
				
		// Display the combo element sorted by displayed name.
		comboViewer.setComparator(new ViewerComparator()
		{
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) 
			{
				WayPointPath wayPointPath1 = (WayPointPath) e1;
				WayPointPath wayPointPath2 = (WayPointPath) e2;
				
				if(wayPointPath1.getLength() > wayPointPath2.getLength())
				{
					return 1;
				}
				else if(wayPointPath1.getLength() < wayPointPath2.getLength())
				{
					return -1;
				}
				else
				{					
					return (wayPointPath1.hashCode() - wayPointPath2.hashCode());
				}
			}
		});
						
		comboViewer.setInput(this.list);
				
		return comboViewer;
	}				
	
	public Collection<WayPointPath> getList() {
		return list;
	}

	public void setList(Collection<WayPointPath> list) 
	{
		this.list.clear();
		
		if(list != null)
		{
			this.list.addAll(list);			
		}
		
		if(comboViewer != null)
		{
			comboViewer.setInput(this.list);
			comboViewer.refresh();
		}
	}

	public WayPointPath getSelectedWayPointPath() {
		return selectedWayPointPath;
	}

	public void setSelectedWayPointPath(WayPointPath selectedWayPointPath) 
	{
		this.selectedWayPointPath = selectedWayPointPath;
		
		if(selectedWayPointPath != null)
		{
			comboViewer.setSelection(new StructuredSelection(selectedWayPointPath));
		}
		else
		{
			comboViewer.setSelection(null);
		}
	}
	
	protected void newSelection(WayPointPath selectedWayPointPath)
	{		
	}
}
