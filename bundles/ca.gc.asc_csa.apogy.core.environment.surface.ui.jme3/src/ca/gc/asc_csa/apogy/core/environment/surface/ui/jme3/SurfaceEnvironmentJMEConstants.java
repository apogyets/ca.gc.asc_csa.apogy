package ca.gc.asc_csa.apogy.core.environment.surface.ui.jme3;

public class SurfaceEnvironmentJMEConstants 
{
	public static final float CELESTIAL_SPHERE_RADIUS = 20000.0f; 							// 20km.
	public static final float ATMOSPHERE_RADIUS       = CELESTIAL_SPHERE_RADIUS * 0.95f;	
	public static final float HORIZON_RADIUS          = CELESTIAL_SPHERE_RADIUS * 0.85f;				
}
