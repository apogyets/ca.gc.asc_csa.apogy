package ca.gc.asc_csa.apogy.common.topology.bindings.ui.composites;

import java.util.Collection;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureSpecifier;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureTreeNode;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.AbstractFeatureSpecifierComposite;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsSet;

public abstract class BindingsSetComposite extends Composite 
{	
	private FormToolkit formToolkit = new FormToolkit(Display.getCurrent());
	private BindingsSet bindingsSet;
	
	private DataBindingContext m_bindingContext;

	private FeatureRootsListComposite featureRootsListComposite;
	private AbstractFeatureSpecifierComposite abstractFeatureSpecifierComposite;
	
	private BindingsListComposite bindingsListComposite;
	private AbstractTopologyBindingOverviewComposite abstractTopologyBindingOverviewComposite;
	private AbtractTopologyBindingDetailsComposite abtractTopologyBindingDetailsComposite;
	
	public BindingsSetComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		ScrolledForm topScrolledForm = formToolkit.createScrolledForm(this);
		topScrolledForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		topScrolledForm.setShowFocusedControl(true);
		formToolkit.paintBordersFor(topScrolledForm);
		topScrolledForm.setText(null);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 1;
			topScrolledForm.getBody().setLayout(tableWrapLayout);
		}
		
		// Features Section.
		Section featuresSection = formToolkit.createSection(topScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnFeatures = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnFeatures.valign = TableWrapData.FILL;		
		twd_sctnFeatures.grabVertical = true;
		twd_sctnFeatures.grabHorizontal = true;
		featuresSection.setLayoutData(twd_sctnFeatures);
		formToolkit.paintBordersFor(featuresSection);
		featuresSection.setText("Features");
		featuresSection.setLayout(new GridLayout(1, false));			
		
		ScrolledForm featuresScrolledForm = formToolkit.createScrolledForm(featuresSection);
		featuresScrolledForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		featuresScrolledForm.setShowFocusedControl(true);
		formToolkit.paintBordersFor(featuresScrolledForm);
		featuresScrolledForm.setText(null);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 2;
			featuresScrolledForm.getBody().setLayout(tableWrapLayout);
		}
		featuresSection.setClient(featuresScrolledForm);
		
		// Features List
		Section featuresListSection = formToolkit.createSection(featuresScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnFeaturesList = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnFeaturesList.valign = TableWrapData.FILL;		
		twd_sctnFeaturesList.grabVertical = true;
		twd_sctnFeaturesList.grabHorizontal = true;
		featuresListSection.setLayoutData(twd_sctnFeaturesList);
		formToolkit.paintBordersFor(featuresListSection);
		featuresListSection.setText("Features List");
		
		featureRootsListComposite = new FeatureRootsListComposite(featuresListSection, SWT.NONE)
		{
			@Override
			protected void newAbstractFeatureTreeNodeSelected(AbstractFeatureTreeNode selectedAbstractFeatureTreeNode) 
			{
				if(selectedAbstractFeatureTreeNode instanceof AbstractFeatureSpecifier)
				{					
					abstractFeatureSpecifierComposite.setAbstractFeatureSpecifier((AbstractFeatureSpecifier) selectedAbstractFeatureTreeNode);
				}
			}
		};
		GridData gd_featureRootsListComposite = new GridData(SWT.FILL, SWT.TOP, true, false,1,1);
		gd_featureRootsListComposite.minimumHeight = 200;
		gd_featureRootsListComposite.heightHint = 200;
		featureRootsListComposite.setLayoutData(gd_featureRootsListComposite);
				
		formToolkit.adapt(featureRootsListComposite);
		formToolkit.paintBordersFor(featureRootsListComposite);
		featuresListSection.setClient(featureRootsListComposite);
		
		// Feature Details.
		Section featureDetailsSection = formToolkit.createSection(featuresScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnFeatureDetails = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnFeatureDetails.valign = TableWrapData.FILL;		
		twd_sctnFeatureDetails.grabVertical = true;
		twd_sctnFeatureDetails.grabHorizontal = true;
		featureDetailsSection.setLayoutData(twd_sctnFeatureDetails);
		formToolkit.paintBordersFor(featureDetailsSection);
		featureDetailsSection.setText("Feature Details");
		
		abstractFeatureSpecifierComposite = new AbstractFeatureSpecifierComposite(featureDetailsSection, SWT.NONE);
		GridData gd_abstractFeatureSpecifierComposite = new GridData(SWT.FILL, SWT.TOP, true, false,1,1);
		gd_abstractFeatureSpecifierComposite.minimumHeight = 200;
		gd_abstractFeatureSpecifierComposite.heightHint = 200;
		abstractFeatureSpecifierComposite.setLayoutData(gd_abstractFeatureSpecifierComposite);
				
		formToolkit.adapt(abstractFeatureSpecifierComposite);
		formToolkit.paintBordersFor(abstractFeatureSpecifierComposite);
		featureDetailsSection.setClient(abstractFeatureSpecifierComposite);
				
		// Bindings.
		Section sctnBindings = formToolkit.createSection(topScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnBindings = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnBindings.valign = TableWrapData.FILL;		
		twd_sctnBindings.grabVertical = true;
		twd_sctnBindings.grabHorizontal = true;
		sctnBindings.setLayoutData(twd_sctnBindings);
		formToolkit.paintBordersFor(sctnBindings);
		sctnBindings.setText("Bindings");
		sctnBindings.setLayout(new GridLayout(1, false));			
		
		ScrolledForm bindingsScrolledForm = formToolkit.createScrolledForm(sctnBindings);
		bindingsScrolledForm.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		bindingsScrolledForm.setShowFocusedControl(true);
		formToolkit.paintBordersFor(bindingsScrolledForm);
		bindingsScrolledForm.setText(null);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 2;
			bindingsScrolledForm.getBody().setLayout(tableWrapLayout);
		}
		sctnBindings.setClient(bindingsScrolledForm);
		
		// Bindings List
		Section bindingsListSection = formToolkit.createSection(bindingsScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnBindingsList = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 2, 1);
		twd_sctnBindingsList.valign = TableWrapData.FILL;		
		twd_sctnBindingsList.grabVertical = true;
		twd_sctnBindingsList.grabHorizontal = true;
		bindingsListSection.setLayoutData(twd_sctnBindingsList);
		formToolkit.paintBordersFor(bindingsListSection);
		bindingsListSection.setText("Bindings List");
		
		// Bindings.
		bindingsListComposite = new BindingsListComposite(bindingsListSection, SWT.NONE)
		{
			@Override
			protected void newBindingSelected(AbstractTopologyBinding selectedAbstractTopologyBinding) 
			{
				abstractTopologyBindingOverviewComposite.setAbstractTopologyBinding(selectedAbstractTopologyBinding);
				abtractTopologyBindingDetailsComposite.setAbstractTopologyBinding(selectedAbstractTopologyBinding);
			}
			
			@Override
			public Collection<Node> getAvailableTopologyNodesForBinding() 
			{
				return BindingsSetComposite.this.getAvailableNodes();
			}
		};
		GridData gd_bindingsListComposite = new GridData(SWT.FILL, SWT.TOP, true, false);
		gd_bindingsListComposite.minimumHeight = 200;
		gd_bindingsListComposite.heightHint = 200;
		bindingsListComposite.setLayoutData(gd_bindingsListComposite);	
		
		formToolkit.adapt(bindingsListComposite);
		formToolkit.paintBordersFor(bindingsListComposite);
		bindingsListSection.setClient(bindingsListComposite);
		
		// Binding Overview.
		Section bindingsOverviewSection = formToolkit.createSection(bindingsScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnBindingOverview = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnBindingOverview.valign = TableWrapData.FILL;		
		twd_sctnBindingOverview.grabVertical = true;
		twd_sctnBindingOverview.grabHorizontal = true;
		bindingsOverviewSection.setLayoutData(twd_sctnBindingOverview);
		formToolkit.paintBordersFor(bindingsOverviewSection);
		bindingsOverviewSection.setText("Binding Overview");	
						
		abstractTopologyBindingOverviewComposite = new AbstractTopologyBindingOverviewComposite(bindingsOverviewSection, SWT.BORDER);
		GridData gd_abstractTopologyBindingOverviewComposite = new GridData(SWT.FILL, SWT.TOP, true, false);
		gd_abstractTopologyBindingOverviewComposite.minimumHeight = 200;
		gd_abstractTopologyBindingOverviewComposite.heightHint = 200;
		abstractTopologyBindingOverviewComposite.setLayoutData(gd_abstractTopologyBindingOverviewComposite);	
		
		formToolkit.adapt(abstractTopologyBindingOverviewComposite);
		formToolkit.paintBordersFor(abstractTopologyBindingOverviewComposite);
		bindingsOverviewSection.setClient(abstractTopologyBindingOverviewComposite);
		
		// Binding details
		Section bindingsDetailsSection = formToolkit.createSection(bindingsScrolledForm.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnBindingDetails = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 1, 1);
		twd_sctnBindingDetails.valign = TableWrapData.FILL;		
		twd_sctnBindingDetails.grabVertical = true;
		twd_sctnBindingDetails.grabHorizontal = true;
		bindingsDetailsSection.setLayoutData(twd_sctnBindingDetails);
		formToolkit.paintBordersFor(bindingsDetailsSection);
		bindingsDetailsSection.setText("Binding Details");	
						
		abtractTopologyBindingDetailsComposite = new AbtractTopologyBindingDetailsComposite(bindingsDetailsSection, SWT.NONE);
		GridData gd_abtractTopologyBindingDetailsComposite = new GridData(SWT.FILL, SWT.TOP, true, true);
		gd_abtractTopologyBindingDetailsComposite.minimumHeight = 200;
		gd_abtractTopologyBindingDetailsComposite.heightHint = 200;
		abtractTopologyBindingDetailsComposite.setLayoutData(gd_abtractTopologyBindingDetailsComposite);	
		
		formToolkit.adapt(abtractTopologyBindingDetailsComposite);
		formToolkit.paintBordersFor(abtractTopologyBindingDetailsComposite);
		bindingsDetailsSection.setClient(abtractTopologyBindingDetailsComposite);
				
		// Cleanup on dispose.
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();			
			}
		});
	}

	/**
	 * Returns the BindingsSet being displayed.
	 * @return The BindingsSet, can be null.
	 */
	public BindingsSet getBindingsSet() {
		return bindingsSet;
	}

	/**
	 * Sets the BindingsSet to display.
	 * @param newBindingsSet The BindingsSet to display, can be null.
	 */
	public void setBindingsSet(BindingsSet newBindingsSet) 	
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.bindingsSet = newBindingsSet;			
		
		if(newBindingsSet != null)
		{			
			featureRootsListComposite.setFeatureRootsList(newBindingsSet.getFeatureRootsList());
			bindingsListComposite.setBindingsList(newBindingsSet.getBindingsList());
			
			abstractTopologyBindingOverviewComposite.setAbstractTopologyBinding(null);
			abtractTopologyBindingDetailsComposite.setAbstractTopologyBinding(null);
			abstractFeatureSpecifierComposite.setAbstractFeatureSpecifier(null);
			
			m_bindingContext = initDataBindingsCustom();
		}		
		else
		{
			featureRootsListComposite.setFeatureRootsList(null);
			bindingsListComposite.setBindingsList(null);
			
			abstractTopologyBindingOverviewComposite.setAbstractTopologyBinding(null);
			abtractTopologyBindingDetailsComposite.setAbstractTopologyBinding(null);
			abstractFeatureSpecifierComposite.setAbstractFeatureSpecifier(null);
		}
	}
	
	/**
	 * Returns the list of Node available for binding.
	 * @return The collection of Node, can be empty, never null.
	 */
	public abstract Collection<Node> getAvailableNodes();
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		return bindingContext;
	}	
}
