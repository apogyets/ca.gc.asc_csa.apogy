package ca.gc.asc_csa.apogy.common.topology.bindings.ui.composites;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsList;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsSet;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public abstract class BindingsListComposite extends Composite 
{
	private BindingsList bindingsList;
		
	private Tree tree;
	private TreeViewer treeViewer;
	
	private Button btnNew;
	private Button btnDelete;	
	
	private DataBindingContext m_bindingContext;
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	public BindingsListComposite(Composite parent, int style) 
	{
		super(parent, style);		
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CustomContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				AbstractTopologyBinding selectedAbstractTopologyBinding = (AbstractTopologyBinding)((IStructuredSelection) event.getSelection()).getFirstElement(); 
				newBindingSelected(selectedAbstractTopologyBinding);					
			}
		});
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setToolTipText("Create a new Binding.");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{				
				if (event.type == SWT.Selection) 
				{					
					MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();					
					String name = ApogyCommonEMFFacade.INSTANCE.getDefaultName(getBindingsList(), null, ApogyCommonTopologyBindingsPackage.Literals.BINDINGS_LIST__BINDINGS);
					
					settings.getUserDataMap().put(TopologyUIBindingsConstants.NAME_USER_ID, name);
					settings.getUserDataMap().put(TopologyUIBindingsConstants.NODES_LIST_USER_ID, getAvailableTopologyNodesForBinding());
					
					if(getBindingsList().eContainer() instanceof BindingsSet)
					{
						BindingsSet bindingsSet = (BindingsSet) getBindingsList().eContainer();						
						settings.getUserDataMap().put(TopologyUIBindingsConstants.FEATURE_ROOT_LIST_USER_ID, bindingsSet.getFeatureRootsList());	
					}								
											
					Wizard wizard = new ApogyEObjectWizard(ApogyCommonTopologyBindingsPackage.Literals.BINDINGS_LIST__BINDINGS, getBindingsList(), settings, ApogyCommonTopologyBindingsPackage.Literals.ABSTRACT_TOPOLOGY_BINDING); 
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getBindingsList());
					}					
				}
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setToolTipText("Deletes the selected Binding(s).");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String deleteMessage = "";

				Iterator<AbstractTopologyBinding> bindingsIterator = getSelectedBindings().iterator();
				while (bindingsIterator.hasNext()) 
				{
					AbstractTopologyBinding binding = bindingsIterator.next();
					deleteMessage = deleteMessage + binding.getName();

					if (bindingsIterator.hasNext()) 
					{
						deleteMessage = deleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected Bindings", null,
						"Are you sure to delete these Bindings : " + deleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					List<AbstractTopologyBinding> toRemove = getSelectedBindings();
					ApogyCommonTransactionFacade.INSTANCE.basicRemove(getBindingsList(), ApogyCommonTopologyBindingsPackage.Literals.BINDINGS_LIST__BINDINGS, toRemove);
				}
				
				// Forces the viewer to refresh its input.
				if(!treeViewer.isBusy())
				{					
					treeViewer.setInput(getBindingsList());
				}					
			}
		});
	
		
		// Cleanup on dispose
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();			
			}
		});
	}

	public BindingsList getBindingsList() 
	{
		return bindingsList;
	}

	public void setBindingsList(BindingsList newBindingsList) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.bindingsList = newBindingsList;	
		
		if(newBindingsList != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}	
		
		treeViewer.setInput(newBindingsList);
	}
	
	@SuppressWarnings("unchecked")
	public List<AbstractTopologyBinding> getSelectedBindings()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	public abstract Collection<Node> getAvailableTopologyNodesForBinding();
	
	protected void newBindingSelected(AbstractTopologyBinding selectedAbstractTopologyBinding)
	{		
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	private class CustomContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof BindingsList)
			{								
				BindingsList bindingsList = (BindingsList) inputElement;					
				return bindingsList.getBindings().toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof BindingsList)
			{								
				BindingsList bindingsList = (BindingsList) parentElement;					
				return bindingsList.getBindings().toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{					
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof BindingsList)
			{
				BindingsList bindingsList = (BindingsList) element;				
				return bindingsList.getBindings().isEmpty();
			}		
			else
			{
				return false;
			}
		}
	}
}
