package ca.gc.asc_csa.apogy.common.topology.bindings.ui;

public class TopologyUIBindingsConstants 
{
	public static final String NAME_USER_ID = "name";
	public static final String FEATURE_ROOT_LIST_USER_ID = "featureRootsList";
	public static final String NODES_LIST_USER_ID = "nodes";
	
	public static final String ENUMERATION_SWITCH_BINDING_USER_ID = "enumerationSwitchBinding";
}
