/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.bindings.BooleanBinding;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.TopologyTreeEditingComposite;

public class BooleanBindingTrueCaseTopologyWizardPage extends WizardPage 
{	
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.BooleanBindingTrueCaseTopologyWizardPage";		
	
	private BooleanBinding booleanBinding;
	
	private TopologyTreeEditingComposite trueCaseTopologyTreeComposite;

	
	public BooleanBindingTrueCaseTopologyWizardPage(BooleanBinding booleanBinding)
	{
		super(WIZARD_PAGE_ID);
		setTitle("True Case Topology");
		setDescription("Define the topology for the True case of the binding.");
		
		this.booleanBinding = booleanBinding;
	}
	
	@Override
	public void createControl(Composite parent) 
	{	
		Composite container = new Composite(parent, SWT.BORDER);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		container.setLayout(new GridLayout(1, false));	
		setControl(container);

		trueCaseTopologyTreeComposite = new TopologyTreeEditingComposite(container, SWT.NONE, true);
		GridData gd_trueCaseTopologyTreeComposite = new GridData(SWT.FILL, SWT.FILL, true, true,1,1);
		trueCaseTopologyTreeComposite.setLayoutData(gd_trueCaseTopologyTreeComposite);
		
		trueCaseTopologyTreeComposite.setRoot(booleanBinding.getTrueCase().getTopologyRoot());
	}

}
