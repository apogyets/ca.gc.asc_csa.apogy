package ca.gc.asc_csa.apogy.common.topology.bindings.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.BindingsList;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class EnumerationSwitchBindingCasesComposite extends Composite 
{
	private EnumerationSwitchBinding enumerationSwitchBinding;
	
	private Tree tree;
	private TreeViewer treeViewer;
	
	private Button btnNew;
	private Button btnDelete;	
	
	private DataBindingContext m_bindingContext;
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	public EnumerationSwitchBindingCasesComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(2, false));
		
		treeViewer = new TreeViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tree.widthHint = 200;
		gd_tree.minimumWidth = 200;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CustomContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				Object object = ((IStructuredSelection) event.getSelection()).getFirstElement(); 
				if(object instanceof EnumerationCase)
				{
					EnumerationCase enumerationCase = (EnumerationCase) object;
					enumerationCaseSelected(enumerationCase);	
				}					
			}
		});
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setToolTipText("Create a new Enumeration Case.");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{				
				if (event.type == SWT.Selection) 
				{					
					MapBasedEClassSettings settings = ApogyCommonEMFUIFactory.eINSTANCE.createMapBasedEClassSettings();					
					String name = ApogyCommonEMFFacade.INSTANCE.getDefaultName(getEnumerationSwitchBinding(), null, ApogyCommonTopologyBindingsPackage.Literals.ENUMERATION_SWITCH_BINDING__CASES);
					
					settings.getUserDataMap().put(TopologyUIBindingsConstants.NAME_USER_ID, name);		
					settings.getUserDataMap().put(TopologyUIBindingsConstants.ENUMERATION_SWITCH_BINDING_USER_ID, enumerationSwitchBinding);
											
					Wizard wizard = new ApogyEObjectWizard(ApogyCommonTopologyBindingsPackage.Literals.ENUMERATION_SWITCH_BINDING__CASES, getEnumerationSwitchBinding(), settings, ApogyCommonTopologyBindingsPackage.Literals.ENUMERATION_CASE); 
					WizardDialog dialog = new WizardDialog(getShell(), wizard);
					dialog.open();
					
					// Forces the viewer to refresh its input.
					if(!treeViewer.isBusy())
					{					
						treeViewer.setInput(getEnumerationSwitchBinding());
					}					
				}
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setToolTipText("Deletes the selected Enumeration Case(s).");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String deleteMessage = "";

				Iterator<EnumerationCase> bindingsIterator = getSelectedBindings().iterator();
				while (bindingsIterator.hasNext()) 
				{
					EnumerationCase binding = bindingsIterator.next();
					deleteMessage = deleteMessage + binding.getEnumerationLiterals();

					if (bindingsIterator.hasNext()) 
					{
						deleteMessage = deleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected Enumeration Case", null,
						"Are you sure to delete these  Enumeration Case : " + deleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					List<EnumerationCase> toRemove = getSelectedBindings();
					ApogyCommonTransactionFacade.INSTANCE.basicRemove(getEnumerationSwitchBinding(), ApogyCommonTopologyBindingsPackage.Literals.ENUMERATION_SWITCH_BINDING__CASES, toRemove);
				}
				
				// Forces the viewer to refresh its input.
				if(!treeViewer.isBusy())
				{					
					treeViewer.setInput(getEnumerationSwitchBinding());
				}					
			}
		});
	
		
		// Cleanup on dispose
		addDisposeListener(new DisposeListener() 
		{			
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{
				if (m_bindingContext != null) m_bindingContext.dispose();			
			}
		});
	}

	public EnumerationSwitchBinding getEnumerationSwitchBinding() {
		return enumerationSwitchBinding;
	}

	public void setEnumerationSwitchBinding(EnumerationSwitchBinding newEnumerationSwitchBinding) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.enumerationSwitchBinding = newEnumerationSwitchBinding;
		
		if(newEnumerationSwitchBinding != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}	
		
		treeViewer.setInput(newEnumerationSwitchBinding);
	}
	
	@SuppressWarnings("unchecked")
	public List<EnumerationCase> getSelectedBindings()
	{
		return ((IStructuredSelection) treeViewer.getSelection()).toList();
	}
	
	protected void enumerationCaseSelected(EnumerationCase selectedEnumerationCase)
	{		
	}
	
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(treeViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) {
								return fromObject != null;
							}
						}));
		
		return bindingContext;
	}
	
	private class CustomContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof EnumerationSwitchBinding)
			{								
				EnumerationSwitchBinding bindingsList = (EnumerationSwitchBinding) inputElement;					
				return bindingsList.getCases().toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof EnumerationSwitchBinding)
			{								
				EnumerationSwitchBinding bindingsList = (EnumerationSwitchBinding) parentElement;					
				return bindingsList.getCases().toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{					
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof BindingsList)
			{
				EnumerationSwitchBinding bindingsList = (EnumerationSwitchBinding) element;				
				return !bindingsList.getCases().isEmpty();
			}		
			else
			{
				return false;
			}
		}
	}
}
