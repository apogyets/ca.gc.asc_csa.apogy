package ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.topology.bindings.AbstractTopologyBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.Axis;

public abstract class AxisSelectionWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.AxisSelectionWizardPage";		
	protected AbstractTopologyBinding abstractTopologyBinding;
	private Axis selectedAxis;
	
	private ComboViewer comboViewer;
	
	public AxisSelectionWizardPage(String title, String description, AbstractTopologyBinding abstractTopologyBinding) 
	{
		super(WIZARD_PAGE_ID);
		setTitle(title);
		setDescription(description);
		
		this.abstractTopologyBinding = abstractTopologyBinding;
		
		setPageComplete(false);
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.BORDER);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		container.setLayout(new GridLayout(2, false));					
		
		Label lblAxis = new Label(container, SWT.NONE);
		lblAxis.setText("Axis : ");
		
		comboViewer = createCombo(container, SWT.NONE);		
		comboViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				if(event.getSelection().isEmpty())
				{
					selectedAxis = null;
					axisSelected(selectedAxis);
				}
				else if(event.getSelection() instanceof IStructuredSelection)
				{
					IStructuredSelection iStructuredSelection = (IStructuredSelection) event.getSelection();
					if(iStructuredSelection.getFirstElement() instanceof Axis)
					{
						selectedAxis = ((Axis)iStructuredSelection.getFirstElement());
					}
					else
					{
						selectedAxis = null;
					}
					axisSelected(selectedAxis);
				}	
				validate();
			}
		});
		comboViewer.setSelection(new StructuredSelection(Axis.XAXIS));
		
		setControl(container);
		validate();
	}
	
	/**
	 * Returns the Axis currently selected.
	 * @return The Axis selected, can be null.
	 */
	public Axis getSelectedAxis()
	{
		return selectedAxis;
	}
	
	/**
	 * Method called when the users makes a selection.
	 * @param axisSelected The Axis selected, can be null.
	 */
	abstract protected void axisSelected(Axis axisSelected);
	
	/**
	 * This method is invoked to validate the content.
	 */
	protected void validate() 
	{			
		setPageComplete(getErrorMessage() == null);
	}
	
	private ComboViewer createCombo(Composite parent, int style)
	{
		ComboViewer comboViewer = new ComboViewer(parent, SWT.DROP_DOWN | SWT.READ_ONLY);		
		comboViewer.setContentProvider(ArrayContentProvider.getInstance());
		comboViewer.setLabelProvider(new LabelProvider()
		{
			@Override
			public String getText(Object element) 
			{
				if(element instanceof Axis)
				{
					Axis axis = (Axis) element;
					return axis.getLiteral();
				}
				else
				{
					return "";
				}
			}
		});
				
		
		comboViewer.setInput(Axis.VALUES);
				
		return comboViewer;
	}	
}
