/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.RotationNode;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsFactory;
import ca.gc.asc_csa.apogy.common.topology.bindings.ApogyCommonTopologyBindingsPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.Axis;
import ca.gc.asc_csa.apogy.common.topology.bindings.RotationBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.RotationBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.AxisSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.TopologyNodeSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rotation Binding Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RotationBindingWizardPagesProviderImpl extends AbstractTopologyBindingWizardPagesProviderImpl implements RotationBindingWizardPagesProvider 
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RotationBindingWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyBindingsUIPackage.Literals.ROTATION_BINDING_WIZARD_PAGES_PROVIDER;
	}	
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		RotationBinding rotationBinding = ApogyCommonTopologyBindingsFactory.eINSTANCE.createRotationBinding();			
		return rotationBinding;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
		
		// Add topology node selection page.
		List<Node> nodes = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			nodes = (List<Node>) mapBasedEClassSettings.getUserDataMap().get(TopologyUIBindingsConstants.NODES_LIST_USER_ID);
		}
		
		RotationBinding rotationBinding = (RotationBinding) eObject;		
		
		TopologyNodeSelectionWizardPage topologyNodeSelectionWizardPage = new TopologyNodeSelectionWizardPage("Select Rotation Node", 
				"Select the Rotation Node that this binding is controlling.", 
				rotationBinding, 
				ApogyCommonTopologyPackage.Literals.ROTATION_NODE, 
				nodes) 
		{	
			@Override
			protected void nodeSelected(Node nodeSelected) 
			{
				if(rotationBinding != null && nodeSelected instanceof RotationNode)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((RotationBinding) abstractTopologyBinding, ApogyCommonTopologyBindingsPackage.Literals.ROTATION_BINDING__ROTATION_NODE, (RotationNode) nodeSelected, true);
					
					setMessage("Node selected : " + nodeSelected.getNodeId());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("The selected Node is not a RotationNode !");
				}
			}
		};
		
		list.add(topologyNodeSelectionWizardPage);
		
		AxisSelectionWizardPage axisSelectionWizardPage = new AxisSelectionWizardPage("Rotation Axis", "Select the axis about which the rotation is to occur", rotationBinding) {
			
			@Override
			protected void axisSelected(Axis axisSelected) 
			{
				if(rotationBinding != null)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((RotationBinding) abstractTopologyBinding, ApogyCommonTopologyBindingsPackage.Literals.ROTATION_BINDING__ROTATION_AXIS, axisSelected, true);
					
					setMessage("Axis selected : " + axisSelected.getLiteral());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("No rotation axis selected !");
				}
				
			}
		};
		
		list.add(axisSelectionWizardPage);
		
		return list;
	}
} //RotationBindingWizardPagesProviderImpl
