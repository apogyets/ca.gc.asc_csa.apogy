/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.*;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.BooleanBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationSwitchBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.RotationBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TransformMatrixBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TranslationBindingWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage
 * @generated
 */
public class ApogyCommonTopologyBindingsUISwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ApogyCommonTopologyBindingsUIPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonTopologyBindingsUISwitch() {
		if (modelPackage == null) {
			modelPackage = ApogyCommonTopologyBindingsUIPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ApogyCommonTopologyBindingsUIPackage.APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE: {
				ApogyCommonTopologyBindingsUIFacade apogyCommonTopologyBindingsUIFacade = (ApogyCommonTopologyBindingsUIFacade)theEObject;
				T result = caseApogyCommonTopologyBindingsUIFacade(apogyCommonTopologyBindingsUIFacade);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER: {
				AbstractTopologyBindingWizardPagesProvider abstractTopologyBindingWizardPagesProvider = (AbstractTopologyBindingWizardPagesProvider)theEObject;
				T result = caseAbstractTopologyBindingWizardPagesProvider(abstractTopologyBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(abstractTopologyBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyBindingsUIPackage.ROTATION_BINDING_WIZARD_PAGES_PROVIDER: {
				RotationBindingWizardPagesProvider rotationBindingWizardPagesProvider = (RotationBindingWizardPagesProvider)theEObject;
				T result = caseRotationBindingWizardPagesProvider(rotationBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(rotationBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(rotationBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyBindingsUIPackage.TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER: {
				TranslationBindingWizardPagesProvider translationBindingWizardPagesProvider = (TranslationBindingWizardPagesProvider)theEObject;
				T result = caseTranslationBindingWizardPagesProvider(translationBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(translationBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(translationBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyBindingsUIPackage.TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER: {
				TransformMatrixBindingWizardPagesProvider transformMatrixBindingWizardPagesProvider = (TransformMatrixBindingWizardPagesProvider)theEObject;
				T result = caseTransformMatrixBindingWizardPagesProvider(transformMatrixBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(transformMatrixBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(transformMatrixBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyBindingsUIPackage.BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER: {
				BooleanBindingWizardPagesProvider booleanBindingWizardPagesProvider = (BooleanBindingWizardPagesProvider)theEObject;
				T result = caseBooleanBindingWizardPagesProvider(booleanBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(booleanBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(booleanBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyBindingsUIPackage.ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER: {
				EnumerationSwitchBindingWizardPagesProvider enumerationSwitchBindingWizardPagesProvider = (EnumerationSwitchBindingWizardPagesProvider)theEObject;
				T result = caseEnumerationSwitchBindingWizardPagesProvider(enumerationSwitchBindingWizardPagesProvider);
				if (result == null) result = caseAbstractTopologyBindingWizardPagesProvider(enumerationSwitchBindingWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(enumerationSwitchBindingWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ApogyCommonTopologyBindingsUIPackage.ENUMERATION_CASE_WIZARD_PAGES_PROVIDER: {
				EnumerationCaseWizardPagesProvider enumerationCaseWizardPagesProvider = (EnumerationCaseWizardPagesProvider)theEObject;
				T result = caseEnumerationCaseWizardPagesProvider(enumerationCaseWizardPagesProvider);
				if (result == null) result = caseWizardPagesProvider(enumerationCaseWizardPagesProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApogyCommonTopologyBindingsUIFacade(ApogyCommonTopologyBindingsUIFacade object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractTopologyBindingWizardPagesProvider(AbstractTopologyBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rotation Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rotation Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRotationBindingWizardPagesProvider(RotationBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Translation Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Translation Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTranslationBindingWizardPagesProvider(TranslationBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transform Matrix Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transform Matrix Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransformMatrixBindingWizardPagesProvider(TransformMatrixBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Boolean Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Boolean Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBooleanBindingWizardPagesProvider(BooleanBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Switch Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Switch Binding Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerationSwitchBindingWizardPagesProvider(EnumerationSwitchBindingWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enumeration Case Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enumeration Case Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnumerationCaseWizardPagesProvider(EnumerationCaseWizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wizard Pages Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWizardPagesProvider(WizardPagesProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ApogyCommonTopologyBindingsUISwitch
