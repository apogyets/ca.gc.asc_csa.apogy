/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui;

import ca.gc.asc_csa.apogy.common.emf.ui.WizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enumeration Case Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Wizard page provider used to create a EnumerationCase.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage#getEnumerationCaseWizardPagesProvider()
 * @model
 * @generated
 */
public interface EnumerationCaseWizardPagesProvider extends WizardPagesProvider {
} // EnumerationCaseWizardPagesProvider
