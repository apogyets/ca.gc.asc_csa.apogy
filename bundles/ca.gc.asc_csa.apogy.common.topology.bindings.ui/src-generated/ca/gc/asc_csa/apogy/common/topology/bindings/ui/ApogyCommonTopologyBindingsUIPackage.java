/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyCommonTopologyBindingsUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca)\n    Canadian Space Agency (CSA) - Initial API and implementation' modelName='ApogyCommonTopologyBindingsUI' complianceLevel='6.0' modelDirectory='/ca.gc.asc_csa.apogy.common.topology.bindings.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.common.topology.bindings.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.common.topology.bindings'"
 * @generated
 */
public interface ApogyCommonTopologyBindingsUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.common.topology.bindings.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyCommonTopologyBindingsUIPackage eINSTANCE = ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIFacadeImpl <em>Facade</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIFacadeImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getApogyCommonTopologyBindingsUIFacade()
	 * @generated
	 */
	int APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE = 0;

	/**
	 * The number of structural features of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Is EEnum Literal In Use</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE___IS_EENUM_LITERAL_IN_USE__ENUMERATIONSWITCHBINDING_EENUMLITERAL = 0;

	/**
	 * The number of operations of the '<em>Facade</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.AbstractTopologyBindingWizardPagesProviderImpl <em>Abstract Topology Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.AbstractTopologyBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getAbstractTopologyBindingWizardPagesProvider()
	 * @generated
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER = 1;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Abstract Topology Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Abstract Topology Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.RotationBindingWizardPagesProviderImpl <em>Rotation Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.RotationBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getRotationBindingWizardPagesProvider()
	 * @generated
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER = 2;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Rotation Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Rotation Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROTATION_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.TranslationBindingWizardPagesProviderImpl <em>Translation Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.TranslationBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getTranslationBindingWizardPagesProvider()
	 * @generated
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER = 3;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Translation Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Translation Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.TransformMatrixBindingWizardPagesProviderImpl <em>Transform Matrix Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.TransformMatrixBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getTransformMatrixBindingWizardPagesProvider()
	 * @generated
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER = 4;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Transform Matrix Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Transform Matrix Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.BooleanBindingWizardPagesProviderImpl <em>Boolean Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.BooleanBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getBooleanBindingWizardPagesProvider()
	 * @generated
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER = 5;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Boolean Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Boolean Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.EnumerationSwitchBindingWizardPagesProviderImpl <em>Enumeration Switch Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.EnumerationSwitchBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getEnumerationSwitchBindingWizardPagesProvider()
	 * @generated
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER = 6;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Enumeration Switch Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Enumeration Switch Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.EnumerationCaseWizardPagesProviderImpl <em>Enumeration Case Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.EnumerationCaseWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getEnumerationCaseWizardPagesProvider()
	 * @generated
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER = 7;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Enumeration Case Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Enumeration Case Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENUMERATION_CASE_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonEMFUIPackage.WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFacade <em>Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Facade</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFacade
	 * @generated
	 */
	EClass getApogyCommonTopologyBindingsUIFacade();

	/**
	 * Returns the meta object for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFacade#isEEnumLiteralInUse(ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding, org.eclipse.emf.ecore.EEnumLiteral) <em>Is EEnum Literal In Use</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is EEnum Literal In Use</em>' operation.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFacade#isEEnumLiteralInUse(ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding, org.eclipse.emf.ecore.EEnumLiteral)
	 * @generated
	 */
	EOperation getApogyCommonTopologyBindingsUIFacade__IsEEnumLiteralInUse__EnumerationSwitchBinding_EEnumLiteral();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider <em>Abstract Topology Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Topology Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider
	 * @generated
	 */
	EClass getAbstractTopologyBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.RotationBindingWizardPagesProvider <em>Rotation Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rotation Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.RotationBindingWizardPagesProvider
	 * @generated
	 */
	EClass getRotationBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.TranslationBindingWizardPagesProvider <em>Translation Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Translation Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.TranslationBindingWizardPagesProvider
	 * @generated
	 */
	EClass getTranslationBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.TransformMatrixBindingWizardPagesProvider <em>Transform Matrix Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transform Matrix Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.TransformMatrixBindingWizardPagesProvider
	 * @generated
	 */
	EClass getTransformMatrixBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.BooleanBindingWizardPagesProvider <em>Boolean Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Boolean Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.BooleanBindingWizardPagesProvider
	 * @generated
	 */
	EClass getBooleanBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationSwitchBindingWizardPagesProvider <em>Enumeration Switch Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Switch Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationSwitchBindingWizardPagesProvider
	 * @generated
	 */
	EClass getEnumerationSwitchBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationCaseWizardPagesProvider <em>Enumeration Case Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enumeration Case Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.EnumerationCaseWizardPagesProvider
	 * @generated
	 */
	EClass getEnumerationCaseWizardPagesProvider();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyCommonTopologyBindingsUIFactory getApogyCommonTopologyBindingsUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIFacadeImpl <em>Facade</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIFacadeImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getApogyCommonTopologyBindingsUIFacade()
		 * @generated
		 */
		EClass APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE = eINSTANCE.getApogyCommonTopologyBindingsUIFacade();

		/**
		 * The meta object literal for the '<em><b>Is EEnum Literal In Use</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE___IS_EENUM_LITERAL_IN_USE__ENUMERATIONSWITCHBINDING_EENUMLITERAL = eINSTANCE.getApogyCommonTopologyBindingsUIFacade__IsEEnumLiteralInUse__EnumerationSwitchBinding_EEnumLiteral();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.AbstractTopologyBindingWizardPagesProviderImpl <em>Abstract Topology Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.AbstractTopologyBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getAbstractTopologyBindingWizardPagesProvider()
		 * @generated
		 */
		EClass ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getAbstractTopologyBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.RotationBindingWizardPagesProviderImpl <em>Rotation Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.RotationBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getRotationBindingWizardPagesProvider()
		 * @generated
		 */
		EClass ROTATION_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getRotationBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.TranslationBindingWizardPagesProviderImpl <em>Translation Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.TranslationBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getTranslationBindingWizardPagesProvider()
		 * @generated
		 */
		EClass TRANSLATION_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getTranslationBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.TransformMatrixBindingWizardPagesProviderImpl <em>Transform Matrix Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.TransformMatrixBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getTransformMatrixBindingWizardPagesProvider()
		 * @generated
		 */
		EClass TRANSFORM_MATRIX_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getTransformMatrixBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.BooleanBindingWizardPagesProviderImpl <em>Boolean Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.BooleanBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getBooleanBindingWizardPagesProvider()
		 * @generated
		 */
		EClass BOOLEAN_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getBooleanBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.EnumerationSwitchBindingWizardPagesProviderImpl <em>Enumeration Switch Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.EnumerationSwitchBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getEnumerationSwitchBindingWizardPagesProvider()
		 * @generated
		 */
		EClass ENUMERATION_SWITCH_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getEnumerationSwitchBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.EnumerationCaseWizardPagesProviderImpl <em>Enumeration Case Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.EnumerationCaseWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.ApogyCommonTopologyBindingsUIPackageImpl#getEnumerationCaseWizardPagesProvider()
		 * @generated
		 */
		EClass ENUMERATION_CASE_WIZARD_PAGES_PROVIDER = eINSTANCE.getEnumerationCaseWizardPagesProvider();

	}

} //ApogyCommonTopologyBindingsUIPackage
