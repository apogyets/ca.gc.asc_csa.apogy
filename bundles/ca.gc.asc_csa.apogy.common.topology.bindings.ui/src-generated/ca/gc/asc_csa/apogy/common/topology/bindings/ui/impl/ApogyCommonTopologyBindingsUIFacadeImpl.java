/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationCase;
import ca.gc.asc_csa.apogy.common.topology.bindings.EnumerationSwitchBinding;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIFacade;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ApogyCommonTopologyBindingsUIFacadeImpl extends MinimalEObjectImpl.Container implements ApogyCommonTopologyBindingsUIFacade 
{
	private static ApogyCommonTopologyBindingsUIFacade instance;

	public static ApogyCommonTopologyBindingsUIFacade getInstance() {
		if (instance == null) {
			instance = new ApogyCommonTopologyBindingsUIFacadeImpl();
		}

		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogyCommonTopologyBindingsUIFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonTopologyBindingsUIPackage.Literals.APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean isEEnumLiteralInUse(EnumerationSwitchBinding enumerationSwitchBinding, EEnumLiteral eEnumLiteral) 
	{
		Set<Integer> used = new TreeSet<Integer>();
		
		for(EnumerationCase enumCase : enumerationSwitchBinding.getCases())
		{
			for(EEnumLiteral literal : enumCase.getEnumerationLiterals())
			{
				used.add(literal.getValue());
			}
		}
		
		return used.contains(eEnumLiteral.getValue());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonTopologyBindingsUIPackage.APOGY_COMMON_TOPOLOGY_BINDINGS_UI_FACADE___IS_EENUM_LITERAL_IN_USE__ENUMERATIONSWITCHBINDING_EENUMLITERAL:
				return isEEnumLiteralInUse((EnumerationSwitchBinding)arguments.get(0), (EEnumLiteral)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ApogyCommonTopologyBindingsUIFacadeImpl
