package ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import com.jme3.asset.AssetManager;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.material.Material;
import com.jme3.material.RenderState.FaceCullMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.control.BillboardControl;
import com.jme3.scene.shape.Sphere;
import com.jme3.util.BufferUtils;

import ca.gc.asc_csa.apogy.addons.ApogyAddonsPackage;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.images.AbstractEImage;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3RenderEngineDelegate;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.JME3Utilities;
import ca.gc.asc_csa.apogy.common.topology.ui.jme3.scene_objects.DefaultJME3SceneObject;
import ca.gc.asc_csa.apogy.core.environment.earth.HorizontalCoordinates;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.ApogyEarthSurfaceOrbitEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationTool;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.EarthOrbitingSpacecraftLocationToolNode;
import ca.gc.asc_csa.apogy.core.environment.earth.surface.orbit.ui.scene_objects.EarthOrbitingSpacecraftLocationToolNodeSceneObject;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.jme3.SurfaceEnvironmentJMEConstants;

public class EarthOrbitingSpacecraftLocationToolJME3Object extends DefaultJME3SceneObject<EarthOrbitingSpacecraftLocationToolNode> implements IPropertyChangeListener, EarthOrbitingSpacecraftLocationToolNodeSceneObject
{							
	public static final double SPACECRAFT_ANGULAR_DIAMETER_RADIANS = Math.toRadians(0.25);
	public static final double SPACECRAFT_TEXT_ANGULAR_HEIGHT_RADIANS = Math.toRadians(1);
	
	
	public static final ColorRGBA DEFAULT_GRID_COLOR = new ColorRGBA(0.0f, 1.0f, 0.0f, 1.0f);	
	public static final float DISPLAY_RADIUS  = (float) (SurfaceEnvironmentJMEConstants.CELESTIAL_SPHERE_RADIUS * 0.75f);
	public static final int FONT_SIZE = Math.round((float)(20 * (DISPLAY_RADIUS / 1000.0)));	
	
	
	private EarthOrbitingSpacecraftLocationTool earthOrbitingSpacecraftLocationTool;
	
	private Adapter adapter;		
	private AssetManager assetManager;	
	
	private Node spacecraftTransform = null;	
	private Geometry spacecraftSphere;	
	
	private Node flag = null;
	private BillboardControl billboardControl;
	private Geometry flagGeometry = null;	
	
	private Geometry vectorGeometry = null;
	
	public EarthOrbitingSpacecraftLocationToolJME3Object(EarthOrbitingSpacecraftLocationToolNode node, JME3RenderEngineDelegate jme3RenderEngineDelegate) 
	{
		super(node, jme3RenderEngineDelegate);		
				
		this.assetManager = jme3Application.getAssetManager();
		assetManager.registerLocator("/", FileLocator.class);
		
		earthOrbitingSpacecraftLocationTool = node.getEarthOrbitingSpacecraftLocationTool();
							
		Job job = new Job("EarthOrbitModelPassToolNodeJME3Object initialize.")
		{
			@Override
			protected IStatus run(IProgressMonitor monitor) 
			{														
				jme3Application.enqueue(new Callable<Object>() 
				{
					@Override
					public Object call() throws Exception 
					{			
						if(earthOrbitingSpacecraftLocationTool != null)
						{
							getAttachmentNode().attachChild(getSpacecraftTransform());
							
							getSpacecraftTransform().attachChild(getFlag());
							
							createGeometry();																					
							
							earthOrbitingSpacecraftLocationTool.eAdapters().add(EarthOrbitingSpacecraftLocationToolJME3Object.this.getAdapter());
						}
						return null;
					}
				});
				return Status.OK_STATUS;				
			}
		};
		job.schedule();
		
		// Activator.getDefault().getPreferenceStore().addPropertyChangeListener(this);		
	}
	
	@Override
	public void dispose() 
	{		
		if(earthOrbitingSpacecraftLocationTool != null)
		{
			earthOrbitingSpacecraftLocationTool.eAdapters().remove(EarthOrbitingSpacecraftLocationToolJME3Object.this.getAdapter());
		}
		
		if(billboardControl != null)
		{
			billboardControl.setEnabled(false);
			billboardControl.setSpatial(null);			
		}
		
		super.dispose();				
	}	
	
	@Override
	public void propertyChange(final PropertyChangeEvent event) 
	{				
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{
				// TODO
				return null;
			}
		});		
	}
							
	@Override
	public List<Geometry> getGeometries() 
	{		
		List<Geometry> geometries = new ArrayList<Geometry>();
		
		geometries.add(vectorGeometry);	
		
		return geometries;
	}
	
	private void createGeometry()
	{
		// Adds the trajectory
		vectorGeometry = createToolGeometry();		
		getAttachmentNode().attachChild(vectorGeometry);
	}
	
	private void updateGeometry()
	{		
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{					
				// Detach previous trajectory geometry
				if(vectorGeometry != null) getAttachmentNode().detachChild(vectorGeometry);				
				
				// Adds the trajectory
				vectorGeometry = createToolGeometry();
				if(earthOrbitingSpacecraftLocationTool.isShowVector())
				{					
					getAttachmentNode().attachChild(vectorGeometry);
				}
												
				return null;
			}	
		});		
	}	
	
	private void updateName()
	{
		jme3Application.enqueue(new Callable<Object>() 
		{
			@Override
			public Object call() throws Exception 
			{					
				// Detach the flag ad clear the billboard.
				getSpacecraftTransform().detachChild(getFlag());				
				getBillboardControl().setSpatial(null);
				
				// Clears the flag.				
				flag = null;
								
				// Detach previous trajectory geometry
				if(vectorGeometry != null) getAttachmentNode().detachChild(vectorGeometry);				
				
				// Adds the trajectory
				vectorGeometry = createToolGeometry();		
				getAttachmentNode().attachChild(vectorGeometry);
				
				return null;
			}	
		});
	}
	
	private Geometry createToolGeometry()
	{
		HorizontalCoordinates spacecraftPosition = earthOrbitingSpacecraftLocationTool.getSpacecraftPosition();
		
		Geometry geometry = null;

		Mesh mesh = new Mesh();
		mesh.setMode(Mesh.Mode.Lines);	
		geometry = new Geometry("SpacecraftVector", mesh);		
		
		// Show only if the spacecraft is visible.
		if(spacecraftPosition != null && spacecraftPosition.getAltitude() > 0)
		{				
			List<Vector3f> verticesList = new ArrayList<Vector3f>();
			List<Integer> indexesList = new ArrayList<Integer>();
			
			Vector3f p0 = new Vector3f(0, 0, 0);
						
			double r = Math.cos(spacecraftPosition.getAltitude()) * DISPLAY_RADIUS;				
			
			float x = (float) ( r * Math.cos(-spacecraftPosition.getAzimuth()));
			float y = (float) ( r * Math.sin(-spacecraftPosition.getAzimuth()));
			float z = (float) ( DISPLAY_RADIUS * Math.sin(spacecraftPosition.getAltitude()));

			Vector3f p1 = new Vector3f(x, y, z);				
			
			// Adds the points
			verticesList.add(p0);			
			verticesList.add(p1);			
			
			indexesList.add(0);
			indexesList.add(1);	
						
			mesh.setBuffer( com.jme3.scene.VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(JME3Utilities.convertToFloatArray(verticesList)));
			mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Index, 2, BufferUtils.createIntBuffer(JME3Utilities.convertToIntArray(indexesList)));								
			mesh.updateBound();
			mesh.updateCounts();
			
			// Attaches the flag
			if(!getSpacecraftTransform().hasChild(getFlag()))
			{
				getSpacecraftTransform().attachChild(getFlag());
			}
			
			// Update the spacecraft transform						
			getSpacecraftTransform().setLocalTranslation(x,y,z);
			
			// Attach the spacecraft from the scene.
			if(!getSpacecraftTransform().hasChild(getSpacecraftSphere()))
			{			
				getSpacecraftTransform().attachChild(getSpacecraftSphere());
			}
		}		
		else
		{
			// Detach the spacecraft from the scene.
			if(getSpacecraftTransform().hasChild(getSpacecraftSphere()))
			{
				getSpacecraftTransform().detachChild(getSpacecraftSphere());
			}
			
			// Detach the spacecraft text from the scene.
			if(getSpacecraftTransform().hasChild(getFlag()))
			{
				getSpacecraftTransform().detachChild(getFlag());
			}			
		}
		
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", DEFAULT_GRID_COLOR.clone());
        geometry.setMaterial(mat);
        geometry.setShadowMode(ShadowMode.Off);
        
        return geometry;
	}	
			
	private Node getSpacecraftTransform()
	{
		if(spacecraftTransform == null)
		{
			spacecraftTransform = new Node("Spacecraft Transform");					
		}
		
		return spacecraftTransform;
	}
	
	private Geometry getSpacecraftSphere()
	{				
		if(spacecraftSphere == null)
		{
			float spacecraftRadius = (float) (DISPLAY_RADIUS * Math.tan(SPACECRAFT_ANGULAR_DIAMETER_RADIANS / 2.0) );
									
			System.out.println("spacecraftRadius " + spacecraftRadius);			
			Sphere sphere = new Sphere(10, 36, spacecraftRadius);
			Material mat = new Material(assetManager,  "Common/MatDefs/Misc/Unshaded.j3md");	
			// mat.setColor("GlowColor",DEFAULT_GRID_COLOR.clone());    	
			mat.getAdditionalRenderState().setFaceCullMode(FaceCullMode.Off);
			
	        spacecraftSphere = new Geometry("Spacecraft", sphere);
	        spacecraftSphere.setMaterial(mat);
	        spacecraftSphere.setShadowMode(ShadowMode.Off);
		}
		return spacecraftSphere;
	}

	private BillboardControl getBillboardControl()
	{
		if(billboardControl == null)
		{
			billboardControl = new BillboardControl();			
			billboardControl.setAlignment(BillboardControl.Alignment.Screen);
		}
		
		return billboardControl;
	}
	
	private Node getFlag()
	{
		if(flag == null)
		{			
			flag = createFlag(getFlagText(), FONT_SIZE);
		}
		return flag;
	}
	
	private Node createFlag(String text, int fontSize)
	{		
		Node node = new Node("Flag");	
		node.setShadowMode(ShadowMode.Off);
		
		Node flagAttachmentPoint = new Node("Flag Attachment Point");
		flagAttachmentPoint.setLocalTranslation(0, 0, 0);		
		node.attachChild(flagAttachmentPoint);		
	
		// First, create the image that will hold the text.
		Font font = new Font("Serif", Font.BOLD, fontSize);				
		BufferedImage bufferedImage = createTextImage(text, font, 0);
				
		// Based on the image size, create the flag geometry.
		float widthToHeightRatio = (float) bufferedImage.getWidth() / (float) bufferedImage.getHeight();
		float flagHeight = (float) (DISPLAY_RADIUS * Math.tan(SPACECRAFT_TEXT_ANGULAR_HEIGHT_RADIANS) ); 
		float flagWidth = flagHeight * widthToHeightRatio;
	
		Mesh flagMesh = createFlagMesh(flagWidth, flagHeight);
	
		flagGeometry = new Geometry("Flag Geometry", flagMesh);		
		Material mat = JME3Utilities.createMaterial(bufferedImage, assetManager);         
        flagGeometry.setMaterial(mat);
                
        flagAttachmentPoint.addControl(getBillboardControl());     
        flagAttachmentPoint.attachChild(flagGeometry);	
        
		return node;
	}
	
	private Mesh createFlagMesh(float flagWidth, float flagHeight)
	{
		Vector3f [] vertices = new Vector3f[4];
		vertices[0] = new Vector3f(0, 0, 0);
		vertices[1] = new Vector3f(flagWidth, 0, 0);
		vertices[2] = new Vector3f(0, flagHeight, 0);	
		vertices[3] = new Vector3f(flagWidth, flagHeight, 0);		
		
		int [] indexes = { 2,0,1, 1,3,2 };
									
		Vector2f[] texCoord = new Vector2f[4];
		texCoord[0] = new Vector2f(0,0);
		texCoord[1] = new Vector2f(1,0);
		texCoord[2] = new Vector2f(0,1);
		texCoord[3] = new Vector2f(1,1);
		
		Mesh mesh = new Mesh();		
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(texCoord));
		mesh.setBuffer(com.jme3.scene.VertexBuffer.Type.Index,    3, BufferUtils.createIntBuffer(indexes));									
		mesh.updateBound();
		mesh.updateCounts();
		
		return mesh;
	}
	
	private String getFlagText()
	{
		String text = null;				
		
		if(earthOrbitingSpacecraftLocationTool != null)
		{
			text = earthOrbitingSpacecraftLocationTool.getName();
		}
		
		if(text == null || text.length() == 0)
		{
			text = "?";
		}
		
		return text;
	}
	
	private BufferedImage createTextImage(String text, Font font, int borderWidth) 
	{			
		Color textColor = JME3Utilities.convertToAWTColor(DEFAULT_GRID_COLOR);
		Color backgroundColor = JME3Utilities.convertToAWTColor(ColorRGBA.Black);
		AbstractEImage original = EImagesUtilities.INSTANCE.createTextImage(text, font, textColor, backgroundColor, borderWidth);
		
		int[] borderColor = JME3Utilities.convertToColorIntRGBA(FLAG_POLE_COLOR);         
        AbstractEImage imageWithBorder = EImagesUtilities.INSTANCE.addBorder(original, borderWidth, borderColor[0], borderColor[1], borderColor[2]);
                   
        return imageWithBorder.asBufferedImage();
    }
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{					
					if(msg.getNotifier() instanceof EarthOrbitingSpacecraftLocationTool)
					{
						int featureId = msg.getFeatureID(EarthOrbitingSpacecraftLocationTool.class);
						switch (featureId) 
						{
							case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SPACECRAFT_POSITION:
								updateGeometry();
							break;

							case ApogyAddonsPackage.SIMPLE3_DTOOL__VISIBLE:
							{
								boolean newVisible = msg.getNewBooleanValue();
								setVisible(newVisible);
							}
							
							case ApogyCommonEMFPackage.NAMED__NAME:
							{
								updateName();
							}
							break;
							
							case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__SHOW_VECTOR:
							{
								updateGeometry();
							}
							break;
							
							default:
							break;
						}												
					}
					else if(msg.getNotifier() instanceof EarthOrbitingSpacecraftLocationToolNode)
					{
						int featureId = msg.getFeatureID(EarthOrbitingSpacecraftLocationToolNode.class);
						switch (featureId) 
						{
							case ApogyEarthSurfaceOrbitEnvironmentUIPackage.EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL__EARTH_ORBITING_SPACECRAFT_LOCATION_TOOL_NODE:
								EarthOrbitingSpacecraftLocationToolNode old = (EarthOrbitingSpacecraftLocationToolNode) msg.getOldValue();
								old.eAdapters().remove(getAdapter());
								
								earthOrbitingSpacecraftLocationTool = (EarthOrbitingSpacecraftLocationTool) msg.getNewValue();
								
								if(earthOrbitingSpacecraftLocationTool != null)
								{
									earthOrbitingSpacecraftLocationTool.eAdapters().add(getAdapter());
									updateGeometry();
								}
							break;
								

							default:
							break;
						}	
					}
				}
			};
		}
		
		return adapter;
	}
}
