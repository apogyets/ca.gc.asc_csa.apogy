/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui;

import ca.gc.asc_csa.apogy.common.topology.bindings.ui.ApogyCommonTopologyBindingsUIPackage;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * <!-- end-model-doc -->
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ApogyAddonsSensorsFOVBindingsUIFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel prefix='ApogyAddonsSensorsFOVBindingsUI' childCreationExtenders='true' extensibleProviderFactory='true' multipleEditorPages='false' copyrightText='Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca)\n    Canadian Space Agency (CSA) - Initial API and implementation' modelName='ApogyAddonsSensorsFOVBindingsUI' publicConstructors='true' suppressGenModelAnnotations='false' complianceLevel='6.0' modelDirectory='/ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui/src-generated' editDirectory='/ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.edit/src-generated' basePackage='ca.gc.asc_csa.apogy.addons.sensors.fov.bindings'"
 * @generated
 */
public interface ApogyAddonsSensorsFOVBindingsUIPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ui";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ui";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyAddonsSensorsFOVBindingsUIPackage eINSTANCE = ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ApogyAddonsSensorsFOVBindingsUIPackageImpl.init();

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.CircularSectorFieldOfViewBindingWizardPagesProviderImpl <em>Circular Sector Field Of View Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.CircularSectorFieldOfViewBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ApogyAddonsSensorsFOVBindingsUIPackageImpl#getCircularSectorFieldOfViewBindingWizardPagesProvider()
	 * @generated
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER = 0;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Circular Sector Field Of View Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Circular Sector Field Of View Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ConicalFieldOfViewBindingWizardPagesProviderImpl <em>Conical Field Of View Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ConicalFieldOfViewBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ApogyAddonsSensorsFOVBindingsUIPackageImpl#getConicalFieldOfViewBindingWizardPagesProvider()
	 * @generated
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER = 1;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Conical Field Of View Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Conical Field Of View Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.RectangularFrustrumFieldOfViewBindingWizardPagesProviderImpl <em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.RectangularFrustrumFieldOfViewBindingWizardPagesProviderImpl
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ApogyAddonsSensorsFOVBindingsUIPackageImpl#getRectangularFrustrumFieldOfViewBindingWizardPagesProvider()
	 * @generated
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER = 2;

	/**
	 * The feature id for the '<em><b>Pages</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER__PAGES = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__PAGES;

	/**
	 * The feature id for the '<em><b>EObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER__EOBJECT;

	/**
	 * The number of structural features of the '<em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PAGES__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Create EObject</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___CREATE_EOBJECT__ECLASS_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Instantiate Wizard Pages</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___INSTANTIATE_WIZARD_PAGES__EOBJECT_ECLASSSETTINGS;

	/**
	 * The operation id for the '<em>Get Perform Finish Commands</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_PERFORM_FINISH_COMMANDS__EOBJECT_ECLASSSETTINGS_EDITINGDOMAIN;

	/**
	 * The operation id for the '<em>Get Next Page</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER___GET_NEXT_PAGE__IWIZARDPAGE;

	/**
	 * The number of operations of the '<em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT = ApogyCommonTopologyBindingsUIPackage.ABSTRACT_TOPOLOGY_BINDING_WIZARD_PAGES_PROVIDER_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.CircularSectorFieldOfViewBindingWizardPagesProvider <em>Circular Sector Field Of View Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Circular Sector Field Of View Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.CircularSectorFieldOfViewBindingWizardPagesProvider
	 * @generated
	 */
	EClass getCircularSectorFieldOfViewBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ConicalFieldOfViewBindingWizardPagesProvider <em>Conical Field Of View Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conical Field Of View Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ConicalFieldOfViewBindingWizardPagesProvider
	 * @generated
	 */
	EClass getConicalFieldOfViewBindingWizardPagesProvider();

	/**
	 * Returns the meta object for class '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.RectangularFrustrumFieldOfViewBindingWizardPagesProvider <em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>'.
	 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.RectangularFrustrumFieldOfViewBindingWizardPagesProvider
	 * @generated
	 */
	EClass getRectangularFrustrumFieldOfViewBindingWizardPagesProvider();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ApogyAddonsSensorsFOVBindingsUIFactory getApogyAddonsSensorsFOVBindingsUIFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.CircularSectorFieldOfViewBindingWizardPagesProviderImpl <em>Circular Sector Field Of View Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.CircularSectorFieldOfViewBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ApogyAddonsSensorsFOVBindingsUIPackageImpl#getCircularSectorFieldOfViewBindingWizardPagesProvider()
		 * @generated
		 */
		EClass CIRCULAR_SECTOR_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getCircularSectorFieldOfViewBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ConicalFieldOfViewBindingWizardPagesProviderImpl <em>Conical Field Of View Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ConicalFieldOfViewBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ApogyAddonsSensorsFOVBindingsUIPackageImpl#getConicalFieldOfViewBindingWizardPagesProvider()
		 * @generated
		 */
		EClass CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getConicalFieldOfViewBindingWizardPagesProvider();

		/**
		 * The meta object literal for the '{@link ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.RectangularFrustrumFieldOfViewBindingWizardPagesProviderImpl <em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.RectangularFrustrumFieldOfViewBindingWizardPagesProviderImpl
		 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ApogyAddonsSensorsFOVBindingsUIPackageImpl#getRectangularFrustrumFieldOfViewBindingWizardPagesProvider()
		 * @generated
		 */
		EClass RECTANGULAR_FRUSTRUM_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER = eINSTANCE.getRectangularFrustrumFieldOfViewBindingWizardPagesProvider();

	}

} //ApogyAddonsSensorsFOVBindingsUIPackage
