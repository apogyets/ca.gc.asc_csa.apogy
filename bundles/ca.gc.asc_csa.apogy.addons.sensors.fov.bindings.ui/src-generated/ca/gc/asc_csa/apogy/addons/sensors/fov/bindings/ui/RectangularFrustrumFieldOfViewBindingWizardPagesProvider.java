/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui;

import ca.gc.asc_csa.apogy.common.topology.bindings.ui.AbstractTopologyBindingWizardPagesProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * Wizard page provider used to create a RectangularFrustrumFieldOfViewBinding.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ApogyAddonsSensorsFOVBindingsUIPackage#getRectangularFrustrumFieldOfViewBindingWizardPagesProvider()
 * @model
 * @generated
 */
public interface RectangularFrustrumFieldOfViewBindingWizardPagesProvider extends AbstractTopologyBindingWizardPagesProvider {
} // RectangularFrustrumFieldOfViewBindingWizardPagesProvider
