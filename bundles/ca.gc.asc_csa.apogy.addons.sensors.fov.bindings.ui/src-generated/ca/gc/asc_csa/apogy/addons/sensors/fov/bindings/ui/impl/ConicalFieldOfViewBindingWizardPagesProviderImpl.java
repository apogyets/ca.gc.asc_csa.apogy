/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.addons.sensors.fov.ApogyAddonsSensorsFOVPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.ConicalFieldOfView;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ApogyAddonsSensorsFOVBindingsFactory;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ApogyAddonsSensorsFOVBindingsPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ConicalFieldOfViewBinding;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ApogyAddonsSensorsFOVBindingsUIPackage;
import ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ConicalFieldOfViewBindingWizardPagesProvider;
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.MapBasedEClassSettings;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.TopologyUIBindingsConstants;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.impl.AbstractTopologyBindingWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.common.topology.bindings.ui.wizards.TopologyNodeSelectionWizardPage;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conical Field Of View Binding Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConicalFieldOfViewBindingWizardPagesProviderImpl extends AbstractTopologyBindingWizardPagesProviderImpl implements ConicalFieldOfViewBindingWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConicalFieldOfViewBindingWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyAddonsSensorsFOVBindingsUIPackage.Literals.CONICAL_FIELD_OF_VIEW_BINDING_WIZARD_PAGES_PROVIDER;
	}
	
	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		ConicalFieldOfViewBinding conicalFieldOfViewBinding = ApogyAddonsSensorsFOVBindingsFactory.eINSTANCE.createConicalFieldOfViewBinding();		
		return conicalFieldOfViewBinding;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));
		
		// Add topology node selection page.
		List<Node> nodes = null;
		if(settings instanceof MapBasedEClassSettings)
		{
			MapBasedEClassSettings mapBasedEClassSettings  = (MapBasedEClassSettings) settings;
			nodes = (List<Node>) mapBasedEClassSettings.getUserDataMap().get(TopologyUIBindingsConstants.NODES_LIST_USER_ID);
		}
		
		ConicalFieldOfViewBinding binding = (ConicalFieldOfViewBinding) eObject;		
		
		TopologyNodeSelectionWizardPage topologyNodeSelectionWizardPage = new TopologyNodeSelectionWizardPage("Select Conical Field Of View", 
				"Select the Conical Field Of View that this binding is controlling.", 
				binding, 
				ApogyAddonsSensorsFOVPackage.Literals.CONICAL_FIELD_OF_VIEW, 
				nodes) 
		{	
			@Override
			protected void nodeSelected(Node nodeSelected) 
			{
				if(binding != null && nodeSelected instanceof ConicalFieldOfView)
				{
					ApogyCommonTransactionFacade.INSTANCE.basicSet((ConicalFieldOfViewBinding) binding, ApogyAddonsSensorsFOVBindingsPackage.Literals.CONICAL_FIELD_OF_VIEW_BINDING__FOV, (ConicalFieldOfView) nodeSelected, true);
					
					setMessage("Node selected : " + nodeSelected.getNodeId());
					setErrorMessage(null);
				}		
				else
				{
					setErrorMessage("The selected Node is not a Conical Field Of View !");
				}
			}
		};
		
		list.add(topologyNodeSelectionWizardPage);
		
		return list;
	}
} //ConicalFieldOfViewBindingWizardPagesProviderImpl
