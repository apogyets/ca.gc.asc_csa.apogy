/**
 * Copyright (c) 2018 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca)
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.ApogyAddonsSensorsFOVBindingsUIPackage
 * @generated
 */
public interface ApogyAddonsSensorsFOVBindingsUIFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ApogyAddonsSensorsFOVBindingsUIFactory eINSTANCE = ca.gc.asc_csa.apogy.addons.sensors.fov.bindings.ui.impl.ApogyAddonsSensorsFOVBindingsUIFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Circular Sector Field Of View Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Circular Sector Field Of View Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	CircularSectorFieldOfViewBindingWizardPagesProvider createCircularSectorFieldOfViewBindingWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Conical Field Of View Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conical Field Of View Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	ConicalFieldOfViewBindingWizardPagesProvider createConicalFieldOfViewBindingWizardPagesProvider();

	/**
	 * Returns a new object of class '<em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rectangular Frustrum Field Of View Binding Wizard Pages Provider</em>'.
	 * @generated
	 */
	RectangularFrustrumFieldOfViewBindingWizardPagesProvider createRectangularFrustrumFieldOfViewBindingWizardPagesProvider();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ApogyAddonsSensorsFOVBindingsUIPackage getApogyAddonsSensorsFOVBindingsUIPackage();

} //ApogyAddonsSensorsFOVBindingsUIFactory
