/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 *  
 *  Contributors:
 *      Pierre Allard (Pierre.Allard@canada.ca), 
 *      Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *      Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *      Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *      Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.transaction;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.command.AbstractOverrideableCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionFacadeImpl;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * *
 * This provides tools to process and manage EMF Transaction.
 * <!-- end-model-doc -->
 *
 *
 * @see ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionPackage#getApogyCommonTransactionFacade()
 * @model
 * @generated
 */
public interface ApogyCommonTransactionFacade extends EObject {
	
	/**
	 * Facade Singleton.
	 * @generated_NOT
	 */
	public static ApogyCommonTransactionFacade INSTANCE = ApogyCommonTransactionFacadeImpl.getInstance();
	
	public static final int EXECUTE_COMMAND_ON_OWNER_DOMAIN = 0;
	public static final int EXECUTE_EMF_METHOD = 1;
	public static final int DONT_EXECUTE = 2;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns the default Apogy {@link TransactionalEditingDomain).
	 * @return Reference to the Apogy {@link TransactionalEditingDomain).
	 * <!-- end-model-doc -->
	 * @model kind="operation" dataType="ca.gc.asc_csa.apogy.common.transaction.TransactionalEditingDomain" unique="false"
	 * @generated
	 */
	TransactionalEditingDomain getDefaultEditingDomain();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Puts the specified {@link EObject} inside a {@link ResourceSet} that uses a {@link TransactionalEditingDomain}.
	 * It is important to remove the {@link EObject} from the temporary {@link TransactionalEditingDomain} with
	 * {@link #ca.gc.asc_csa.apogy.common.transaction.impl.ApogyCommonTransactionFacadeImpl.removeFromEditingDomain(EObject eObject) removeFromEditingDomain(EObject eObject)}
	 * when the temporary {@link TransactionalEditingDomain}
	 * is no longer needed to be able to save the modification made to the {@link EObject}.
	 * @param eObject The reference to the {@link EObject}.
	 * <!-- end-model-doc -->
	 * @model eObjectUnique="false"
	 * @generated
	 */
	void addInTempTransactionalEditingDomain(EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Removes the specified {@link EObject}'s {@link Resource} from it's {@link ResourceSet}.
	 * Then, disconnect the {@link Resource} from it's {@link TransactionalEditingDomain}.
	 * @param eObject The reference to the {@link EObject}.
	 * <!-- end-model-doc -->
	 * @model eObjectUnique="false"
	 * @generated
	 */
	void removeFromEditingDomain(EObject eObject);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Verifies if child can be set in parent.
	 * @param owner
	 * @param child
	 * @param feature The child's feature. Used to determine if the feature is a containment or a reference.
	 * @return true if a set or add can be executed, false otherwise.
	 * <!-- end-model-doc -->
	 * @model unique="false" ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	int areEditingDomainsValid(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicSet(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false"
	 * @generated
	 */
	void basicSet(EObject owner, EStructuralFeature feature, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicAdd(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false"
	 * @generated
	 */
	void basicAdd(EObject owner, EStructuralFeature feature, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" collectionDataType="ca.gc.asc_csa.apogy.common.transaction.Collection" collectionUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicAdd(EObject owner, EStructuralFeature feature, Collection<?> collection, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" collectionDataType="ca.gc.asc_csa.apogy.common.transaction.Collection" collectionUnique="false"
	 * @generated
	 */
	void basicAdd(EObject owner, EStructuralFeature feature, Collection<?> collection);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicRemove(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false"
	 * @generated
	 */
	void basicRemove(EObject owner, EStructuralFeature feature, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" collectionDataType="ca.gc.asc_csa.apogy.common.transaction.Collection" collectionUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicRemove(EObject owner, EStructuralFeature feature, Collection<?> collection, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" collectionDataType="ca.gc.asc_csa.apogy.common.transaction.Collection" collectionUnique="false"
	 * @generated
	 */
	void basicRemove(EObject owner, EStructuralFeature feature, Collection<?> collection);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicClear(EObject owner, EStructuralFeature feature, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false"
	 * @generated
	 */
	void basicClear(EObject owner, EStructuralFeature feature);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false" fixUnique="false"
	 * @generated
	 */
	void basicDelete(EObject owner, EStructuralFeature feature, Object value, boolean fix);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ownerUnique="false" featureUnique="false" valueUnique="false"
	 * @generated
	 */
	void basicDelete(EObject owner, EStructuralFeature feature, Object value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model commandDataType="ca.gc.asc_csa.apogy.common.transaction.AbstractOverrideableCommand" commandUnique="false"
	 * @generated
	 */
	void executeCommand(AbstractOverrideableCommand command);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model dataType="ca.gc.asc_csa.apogy.common.transaction.TransactionalEditingDomain" unique="false" eObjectUnique="false"
	 * @generated
	 */
	TransactionalEditingDomain getTransactionalEditingDomain(EObject eObject);

} // ApogyCommonTransactionFacade
