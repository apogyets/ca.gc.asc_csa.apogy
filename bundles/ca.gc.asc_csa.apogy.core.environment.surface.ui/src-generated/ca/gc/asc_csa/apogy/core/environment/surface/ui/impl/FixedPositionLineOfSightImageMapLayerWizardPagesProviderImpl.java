/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui.impl;

import javax.vecmath.Color3f;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.FixedPositionLineOfSightImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.FixedPositionLineOfSightImageMapLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.FixedPositionLineOfSightImageMapLayerWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Line Of Sight Image Map Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FixedPositionLineOfSightImageMapLayerWizardPagesProviderImpl extends CartesianTriangularMeshDerivedImageMapLayerWizardPagesProviderImpl implements FixedPositionLineOfSightImageMapLayerWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedPositionLineOfSightImageMapLayerWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentUIPackage.Literals.FIXED_POSITION_LINE_OF_SIGHT_IMAGE_MAP_LAYER_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		FixedPositionLineOfSightImageMapLayer imageLayer = ApogySurfaceEnvironmentFactory.eINSTANCE.createFixedPositionLineOfSightImageMapLayer();
		imageLayer.setHeight(0.0);
		imageLayer.setLineOfSightAvailableColor(new Color3f(0.0f, 1.0f, 0.0f));
		imageLayer.setLineOfSightNotAvailableColor(new Color3f(1.0f, 0.0f, 0.0f));
		imageLayer.setObserverPosition(ApogyCommonMathFacade.INSTANCE.createTuple3d(0, 0, 0));
		
		ImageMapLayerUISettings layerUISettings = (ImageMapLayerUISettings) settings;		
		imageLayer.setName(layerUISettings.getName());		
		
		return imageLayer;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();		
		list.addAll(super.instantiateWizardPages(eObject, settings));	
		
		ImageMapLayerUISettings layerUISettings = (ImageMapLayerUISettings) settings;							
		list.add(new FixedPositionLineOfSightImageMapLayerWizardPage((FixedPositionLineOfSightImageMapLayer) eObject, layerUISettings));
		
		return list;
	}
} //LineOfSightImageMapLayerWizardPagesProviderImpl
