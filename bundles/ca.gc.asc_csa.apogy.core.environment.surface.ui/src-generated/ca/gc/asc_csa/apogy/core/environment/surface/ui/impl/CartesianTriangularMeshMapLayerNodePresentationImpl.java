/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import ca.gc.asc_csa.apogy.common.geometry.data3d.CartesianTriangularMesh;
import ca.gc.asc_csa.apogy.common.geometry.data3d.ui.impl.CartesianTriangularMeshPresentationImpl;
import ca.gc.asc_csa.apogy.common.geometry.data3d.ui.scene_objects.CartesianTriangularMeshSceneObject;
import ca.gc.asc_csa.apogy.common.math.ApogyCommonMathFacade;
import ca.gc.asc_csa.apogy.common.math.Tuple3d;
import ca.gc.asc_csa.apogy.common.topology.ui.MeshPresentationMode;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayerNode;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.CartesianTriangularMeshMapLayerNodePresentation;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.scene_objects.CartesianTriangularMeshMapLayerNodeSceneObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cartesian Triangular Mesh Map Layer Node Presentation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CartesianTriangularMeshMapLayerNodePresentationImpl extends CartesianTriangularMeshPresentationImpl implements CartesianTriangularMeshMapLayerNodePresentation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	protected CartesianTriangularMeshMapLayerNodePresentationImpl() 
	{
		super();
		
		// Initializes values from preferences
		applyPreferences();	
		
		// Register a listener to the preference store
		Activator.getDefault().getPreferenceStore().addPropertyChangeListener(getPreferencesListener());	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentUIPackage.Literals.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER_NODE_PRESENTATION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public CartesianTriangularMesh basicGetMesh() {

		CartesianTriangularMesh mesh = null;

		if (getNode() != null) 
		{
			CartesianTriangularMeshMapLayerNode  cNode= (CartesianTriangularMeshMapLayerNode) getNode();

			return cNode.getCartesianTriangularMeshMapLayer().getCurrentMesh();
		}

		return mesh;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public boolean isSetMesh() {
		return getNode() != null;
	}
	
	@Override
	protected void updateSceneObject(Notification notification) 
	{
		if (sceneObject != null) 
		{
			CartesianTriangularMeshMapLayerNodeSceneObject meshSceneObject = (CartesianTriangularMeshMapLayerNodeSceneObject) sceneObject;
		
			if(notification.getNotifier() instanceof CartesianTriangularMeshMapLayerNodePresentation)
			{
				int featureID = notification.getFeatureID(CartesianTriangularMeshMapLayerNodePresentation.class);
				
				switch (featureID) 
				{
					case ApogySurfaceEnvironmentUIPackage.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER_NODE_PRESENTATION__POINT_SIZE:
						int newPointSizeValue = notification.getNewIntValue();
						meshSceneObject.setPointSize(newPointSizeValue);
					break;
					
					case ApogySurfaceEnvironmentUIPackage.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER_NODE_PRESENTATION__PRESENTATION_MODE:
						MeshPresentationMode newMode = (MeshPresentationMode) notification.getNewValue();
						meshSceneObject.setPresentationMode(newMode);	
					break;
					
					case ApogySurfaceEnvironmentUIPackage.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER_NODE_PRESENTATION__TRANSPARENCY:
						meshSceneObject.setTransparency(notification.getNewFloatValue());
					break;

					case ApogySurfaceEnvironmentUIPackage.CARTESIAN_TRIANGULAR_MESH_MAP_LAYER_NODE_PRESENTATION__USE_SHADING:
						meshSceneObject.setUseShading(notification.getNewBooleanValue());
					break;
					
					default:
					break;
				}
			}
		}
		super.updateSceneObject(notification);
	}
	
	@Override
	protected void initialSceneObject() 
	{
		CartesianTriangularMeshSceneObject meshSceneObject = (CartesianTriangularMeshSceneObject) sceneObject;
		meshSceneObject.setPointSize(getPointSize());
		meshSceneObject.setPresentationMode(getPresentationMode());
		meshSceneObject.setTransparency(getTransparency());

		super.initialSceneObject();
	}

	@Override
	public Tuple3d basicGetCentroid() 
	{
		Tuple3d centroid = ApogyCommonMathFacade.INSTANCE.createTuple3d(0.0, 0.0, 0.0);

		if (getSceneObject() != null) {
			centroid = ApogyCommonMathFacade.INSTANCE.createTuple3d(getSceneObject().getCentroid());
		}
		return centroid;
	}
	
	@Override
	public boolean isEnableTextureProjection() 
	{	
		return true;
	}
	
	protected IPropertyChangeListener getPreferencesListener()
	{
		if(preferencesListener == null)
		{
			preferencesListener = new IPropertyChangeListener() 
			{

				public void propertyChange(PropertyChangeEvent event) 
				{	
					applyPreferences();		
				}	
			};
		}
		
		return preferencesListener;
	}
} //CartesianTriangularMeshMapLayerNodePresentationImpl
