/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.ui.impl;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.wizard.WizardPage;

import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings;
import ca.gc.asc_csa.apogy.common.emf.ui.impl.NamedDescribedWizardPagesProviderImpl;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.TopologyTreeMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.TopologyTreeMapLayerWizardPagesProvider;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.TopologyTreeMapLayerWizardPage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Topology Tree Map Layer Wizard Pages Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TopologyTreeMapLayerWizardPagesProviderImpl extends NamedDescribedWizardPagesProviderImpl implements TopologyTreeMapLayerWizardPagesProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TopologyTreeMapLayerWizardPagesProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentUIPackage.Literals.TOPOLOGY_TREE_MAP_LAYER_WIZARD_PAGES_PROVIDER;
	}

	@Override
	public EObject createEObject(EClass eClass, EClassSettings settings) 
	{
		TopologyTreeMapLayer layer = ApogySurfaceEnvironmentFactory.eINSTANCE.createTopologyTreeMapLayer();
		layer.setTopologyTreeRoot(ApogySurfaceEnvironmentFactory.eINSTANCE.createTopologyTreeMapLayerNode());
	
		// TODO Custom EClassSettings
		if(settings instanceof ImageMapLayerUISettings)
		{
			ImageMapLayerUISettings imageMapLayerUISettings = (ImageMapLayerUISettings) settings;
			layer.setName(imageMapLayerUISettings.getName());
		}
		
		return layer;
	}
	
	@Override
	public EList<WizardPage> instantiateWizardPages(EObject eObject, EClassSettings settings) 
	{
		EList<WizardPage> list = new BasicEList<>();
		list.addAll(super.instantiateWizardPages(eObject, settings));

		// Add pages to create the topology.
		list.add(new TopologyTreeMapLayerWizardPage((TopologyTreeMapLayer) eObject));
		
		return list;
	}
} //TopologyTreeMapLayerWizardPagesProviderImpl
