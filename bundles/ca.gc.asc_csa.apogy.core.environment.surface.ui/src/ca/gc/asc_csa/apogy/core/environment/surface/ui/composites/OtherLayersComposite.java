package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;

public class OtherLayersComposite extends Composite 
{
	private Map map;
	
	private OtherLayersListComposite otherLayersListComposite;	
	private OtherLayersDetailsComposite otherLayersDetailsComposite;
	private final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
		
	public OtherLayersComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, false));
		
		ScrolledForm scrldfrmImagesLayers = formToolkit.createScrolledForm(this);
		scrldfrmImagesLayers.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrmImagesLayers);
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.makeColumnsEqualWidth = true;
			tableWrapLayout.numColumns = 2;
			scrldfrmImagesLayers.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnImageLayers = formToolkit.createSection(scrldfrmImagesLayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnImageLayers = new TableWrapData(TableWrapData.LEFT, TableWrapData.TOP, 2, 1);
		twd_sctnImageLayers.valign = TableWrapData.FILL;
		twd_sctnImageLayers.grabVertical = true;
		sctnImageLayers.setLayoutData(twd_sctnImageLayers);
		formToolkit.paintBordersFor(sctnImageLayers);
		sctnImageLayers.setText("Other Layers");
		
		otherLayersListComposite = new OtherLayersListComposite(sctnImageLayers, SWT.NONE)
		{
			@Override
			protected void newAbstractMapLayerSelected(AbstractMapLayer abstractMapLayer)
			{		
				otherLayersDetailsComposite.setAbstractMapLayer(abstractMapLayer);				
			}
		};
		formToolkit.adapt(otherLayersListComposite);
		formToolkit.paintBordersFor(otherLayersListComposite);
		sctnImageLayers.setClient(otherLayersListComposite);
									
		Section sctnImageLayerDetails = formToolkit.createSection(scrldfrmImagesLayers.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnImageLayerDetails);
		sctnImageLayerDetails.setText("Layer Details");
		
		otherLayersDetailsComposite = new OtherLayersDetailsComposite(sctnImageLayerDetails, SWT.NONE);
		formToolkit.adapt(otherLayersDetailsComposite);
		formToolkit.paintBordersFor(otherLayersDetailsComposite);
		sctnImageLayerDetails.setClient(otherLayersDetailsComposite);		
		
		scrldfrmImagesLayers.reflow(true);
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map newMap) 
	{		
		this.map = newMap;
		otherLayersListComposite.setMap(newMap);
		otherLayersDetailsComposite.setAbstractMapLayer(null);		
	}
}
