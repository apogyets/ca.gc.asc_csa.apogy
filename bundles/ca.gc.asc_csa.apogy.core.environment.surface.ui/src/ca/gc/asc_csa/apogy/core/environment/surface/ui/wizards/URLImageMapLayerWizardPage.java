package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.images.ApogyCommonImagesFactory;
import ca.gc.asc_csa.apogy.common.images.EImagesUtilities;
import ca.gc.asc_csa.apogy.common.images.URLEImage;
import ca.gc.asc_csa.apogy.common.images.ui.composites.ImageDisplayComposite;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.common.ui.composites.URLSelectionComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.URLImageMapLayer;

public class URLImageMapLayerWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.URLImageMapLayerWizardPage";
	
	private Text imageWidthInMeters;
	private Text imageHeightInMeters;
	
	private URLImageMapLayer urlImageMapLayer;
	private URLSelectionComposite urlSelectionComposite;
		
	private ImageDisplayComposite imageDisplayComposite;				
	private Label lblImageWidthValue;	
	private Label lblImageHeightValue;
		
	private DataBindingContext m_bindingContext;
	
	private String urlString = null;
	
	public URLImageMapLayerWizardPage(URLImageMapLayer urlImageMapLayer) 
	{
		super(WIZARD_PAGE_ID);
		this.urlImageMapLayer = urlImageMapLayer;
		
		if(urlImageMapLayer != null)
		{
			this.urlString = urlImageMapLayer.getUrl();
		}
		
		setTitle("Image Layer : URL selection");
		setDescription("Sets the Image Layer URL.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		
		Composite settingComposite = new Composite(top, SWT.None);
		settingComposite.setLayout(new GridLayout(2, false));
		settingComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
						
		Label lblimageWidthInMeters = new Label(settingComposite, SWT.NONE);
		lblimageWidthInMeters.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblimageWidthInMeters.setText("Image width (m):");
		
		imageWidthInMeters = new Text(settingComposite, SWT.BORDER);
		imageWidthInMeters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		imageWidthInMeters.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				validate();
			}
		});
		
		Label lblimageHeightInMeters = new Label(settingComposite, SWT.NONE);
		lblimageHeightInMeters.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblimageHeightInMeters.setText("Image height (m):");
		
		imageHeightInMeters = new Text(settingComposite, SWT.BORDER);
		imageHeightInMeters.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		imageHeightInMeters.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				validate();
			}
		});
				
		// URL Selection		
		urlSelectionComposite = new URLSelectionComposite(top, SWT.None, new String[]{"*.gif","*.png", "*.jpg", "*.jpeg"}, true, true, true)
		{
			@Override
			protected void urlStringSelected(String newURLString) 
			{		
				URLImageMapLayerWizardPage.this.urlString = newURLString;
				
				validate();
				
				ApogyCommonTransactionFacade.INSTANCE.basicSet(urlImageMapLayer, ApogySurfaceEnvironmentPackage.Literals.URL_MAP_LAYER__URL, newURLString);
			}
		};
		urlSelectionComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		
		if(urlImageMapLayer != null && urlImageMapLayer.getUrl() != null)
		{
			urlSelectionComposite.setUrlString(urlImageMapLayer.getUrl());
		}
		
		// Image Preview.
		Group grImagePreview = new Group(top, SWT.BORDER);
		grImagePreview.setText("Image Preview");
		grImagePreview.setLayout(new GridLayout(2, false));
		grImagePreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite imageSizeComposite = new Composite(grImagePreview, SWT.NONE);		
		imageSizeComposite.setLayout(new GridLayout(2, false));
		imageSizeComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 2, 1));
		
		Label lblImageWidth = new Label(imageSizeComposite, SWT.NONE);
		lblImageWidth.setText("Image Width (pixels):");
		lblImageWidth.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		
		lblImageWidthValue = new Label(imageSizeComposite, SWT.BORDER);
		GridData gd_lblImageWidthValue = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_lblImageWidthValue.widthHint = 100;
		gd_lblImageWidthValue.minimumWidth = 100;
		lblImageWidthValue.setLayoutData(gd_lblImageWidthValue);
		
		Label lblImageHeight = new Label(imageSizeComposite, SWT.NONE);	
		lblImageHeight.setText("Image Height (pixels):");
		lblImageHeight.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		
		lblImageHeightValue = new Label(imageSizeComposite, SWT.BORDER);
		GridData gd_lblImageHeightValue = new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1);
		gd_lblImageHeightValue.widthHint = 100;
		gd_lblImageHeightValue.minimumWidth = 100;
		lblImageHeightValue.setLayoutData(gd_lblImageHeightValue);
		
		// Filler
		new Label(grImagePreview, SWT.NONE);	
		
		// Image Preview
		imageDisplayComposite = new ImageDisplayComposite(grImagePreview, SWT.BORDER);
		imageDisplayComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		imageDisplayComposite.addListener(SWT.Resize, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				imageDisplayComposite.fitImage();
			}
		});		
										
		setControl(top);
		urlSelectionComposite.setFocus();	
		
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
		
	protected void updateImagePreview(String urlString)
	{
		try
		{
			// Update preview image.
			URLEImage urlEImage = ApogyCommonImagesFactory.eINSTANCE.createURLEImage();
			urlEImage.setUrl(urlString);			
			
			lblImageWidthValue.setText(Integer.toString(urlEImage.getWidth()));
			lblImageHeightValue.setText(Integer.toString(urlEImage.getHeight()));
			
			ImageData imageData = EImagesUtilities.INSTANCE.convertToImageData(urlEImage.asBufferedImage());
										
			imageDisplayComposite.setImageData(imageData);
			imageDisplayComposite.fitImage();
		}
		catch(Exception e)
		{
			e.printStackTrace();				
			setErrorMessage("Failed to load image !");
			imageDisplayComposite.setImageData(null);			
		}
	}
	
	protected void validate()
	{
		setErrorMessage(null);
		
		if(urlImageMapLayer.getHeight() <= 0)
		{
			setErrorMessage("Invalid image Height specified <" + urlImageMapLayer.getHeight() + "> must be larger than zero.");			
		}
		
		if(urlImageMapLayer.getWidth() <= 0)
		{		
			setErrorMessage("Invalid image Width specified <" + urlImageMapLayer.getWidth() + "> must be larger than zero.");
			return;
		}
		
		// Checks the URL.
		boolean urlValid = (urlString!= null) && (urlString.length() > 0);		
		if(urlValid)
		{
			updateImagePreview(urlString);			
		}
		else
		{			
			setErrorMessage("Invalid URL specified !");
		}		
		
		setPageComplete(getErrorMessage() == null);
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Image Width Value. */
		IObservableValue<Double> observeImageWidthResolution = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(urlImageMapLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER__WIDTH)).observe(urlImageMapLayer);
		IObservableValue<String> observeImageWidthText = WidgetProperties.text(SWT.Modify).observe(imageWidthInMeters);

		bindingContext.bindValue(observeImageWidthText,
								observeImageWidthResolution, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		/* Image height Value. */
		IObservableValue<Double> observeImageHeight = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(urlImageMapLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.IMAGE_MAP_LAYER__HEIGHT)).observe(urlImageMapLayer);
		IObservableValue<String> observeImageHeightText = WidgetProperties.text(SWT.Modify).observe(imageHeightInMeters);

		bindingContext.bindValue(observeImageHeightText,
								 observeImageHeight, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		return bindingContext;
	}
}
