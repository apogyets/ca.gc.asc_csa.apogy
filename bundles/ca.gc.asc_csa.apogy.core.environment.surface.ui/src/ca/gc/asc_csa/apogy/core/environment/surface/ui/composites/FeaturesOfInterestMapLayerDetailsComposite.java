package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.FeaturesOfInterestMapLayer;

public class FeaturesOfInterestMapLayerDetailsComposite extends Composite 
{
	private FeaturesOfInterestMapLayer featuresOfInterestMapLayer;
	private Composite compositeDetails;
	
	public FeaturesOfInterestMapLayerDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.minimumWidth = 399;
		gd_compositeDetails.widthHint = 399;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public FeaturesOfInterestMapLayer getFeaturesOfInterestMapLayer() {
		return featuresOfInterestMapLayer;
	}

	public void setFeaturesOfInterestMapLayer(FeaturesOfInterestMapLayer newFeaturesOfInterestMapLayer) 
	{
		this.featuresOfInterestMapLayer = newFeaturesOfInterestMapLayer;
			
		// Update Details EMFForm.
		if(newFeaturesOfInterestMapLayer != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newFeaturesOfInterestMapLayer);
		}
	}		
}
