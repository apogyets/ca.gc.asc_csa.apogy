package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.converters.ApogyCommonConvertersFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayerNode;
import ca.gc.asc_csa.apogy.core.environment.surface.FeaturesOfInterestMapLayer;

public class FeaturesOfInterestMapLayerPresentationComposite extends Composite 
{
	private FeaturesOfInterestMapLayer featuresOfInterestMapLayer;
	private Composite compositeDetails;
	
	public FeaturesOfInterestMapLayerPresentationComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.minimumWidth = 300;
		gd_compositeDetails.widthHint = 300;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public FeaturesOfInterestMapLayer getFeaturesOfInterestMapLayer() {
		return featuresOfInterestMapLayer;
	}

	public void setFeaturesOfInterestMapLayer(FeaturesOfInterestMapLayer featuresOfInterestMapLayer) 
	{
		this.featuresOfInterestMapLayer = featuresOfInterestMapLayer;
				
		// Update Details EMFForm.
		NodePresentation presentation = null;
		if(featuresOfInterestMapLayer != null)
		{
			
			AbstractMapLayerNode node = featuresOfInterestMapLayer.getAbstractMapLayerNode();
			
			if(node != null)
			{
				presentation = (NodePresentation) ApogyCommonConvertersFacade.INSTANCE.convert(node, NodePresentation.class);				
			}			
		}
		ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, presentation,"No presentation information available.");
	}		
}
