package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshDerivedImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ApogySurfaceEnvironmentUIRCPConstants;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.ImageMapLayerUISettings;

public class CartesianTriangularMeshDerivedImageMapLayerWizardPage extends WizardPage 
{	
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.CartesianTriangularMeshDerivedImageMapLayerWizardPage";
	
	private CartesianTriangularMeshDerivedImageMapLayer cartesianTriangularMeshDerivedImageMapLayer;
	private ImageMapLayerUISettings uiSettings;
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	
	private Tree tree;
	private TreeViewer treeViewer;
	private Text txtRequiredresolutiontext;
	
	private DataBindingContext m_bindingContext;
					
	public CartesianTriangularMeshDerivedImageMapLayerWizardPage(CartesianTriangularMeshDerivedImageMapLayer cartesianTriangularMeshDerivedImageMapLayer, 	
																 ImageMapLayerUISettings uiSettings) 
	{
		super(WIZARD_PAGE_ID);
		
		this.cartesianTriangularMeshDerivedImageMapLayer = cartesianTriangularMeshDerivedImageMapLayer;
		this.uiSettings = uiSettings;
		
		setTitle("Mesh Derived Image Map Layer");
		setDescription("Select the required resolution and map layer containing the mesh for which to generate the image.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, false));
		
		Label lblRequiredResolutionmpixel = new Label(container, SWT.NONE);
		lblRequiredResolutionmpixel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblRequiredResolutionmpixel.setText("Resolution (m/pixel):");
		
		txtRequiredresolutiontext = new Text(container, SWT.BORDER);
		txtRequiredresolutiontext.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	
		Label lblCartesianTriangularMeshMapLayer = new Label(container, SWT.NONE);
		lblCartesianTriangularMeshMapLayer.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblCartesianTriangularMeshMapLayer.setText("Source Mesh Layer:");
		
		// List the available ImageMapLayer.
		treeViewer = new TreeViewer(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 300;
		gd_tree.minimumWidth = 300;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		// Tree showing all the CartesianTriangularMeshMapLayers.
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				CartesianTriangularMeshMapLayer cartesianTriangularMeshMapLayer = (CartesianTriangularMeshMapLayer)((IStructuredSelection) event.getSelection()).getFirstElement();									
				uiSettings.getUserDataMap().put(ApogySurfaceEnvironmentUIRCPConstants.USER_DATA__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER__ID, cartesianTriangularMeshMapLayer);										
				ApogyCommonTransactionFacade.INSTANCE.basicSet(cartesianTriangularMeshDerivedImageMapLayer, ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_DERIVED_IMAGE_MAP_LAYER__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER, cartesianTriangularMeshMapLayer);				
				validate();
			}
		});
		treeViewer.setInput(uiSettings.getMap());	
		
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		setControl(container);
		
		validate();
		
		// Dispose
		container.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	protected void validate()
	{
		
		if(uiSettings.getUserDataMap().get(ApogySurfaceEnvironmentUIRCPConstants.USER_DATA__CARTESIAN_TRIANGULAR_MESH_MAP_LAYER__ID) != null)
		{
			setErrorMessage(null);
			setPageComplete(true);
		}
		else
		{
			setErrorMessage("The Cartesian Triangular Mesh Map Layer is not set !");
			setPageComplete(false);
		}
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Resolution Value. */
		IObservableValue<Double> observeRequiredResolution = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(cartesianTriangularMeshDerivedImageMapLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_DERIVED_IMAGE_MAP_LAYER__REQUIRED_RESOLUTION)).observe(cartesianTriangularMeshDerivedImageMapLayer);
		IObservableValue<String> observeRequiredResolutionValueText = WidgetProperties.text(SWT.Modify).observe(txtRequiredresolutiontext);

		bindingContext.bindValue(observeRequiredResolutionValueText,
								observeRequiredResolution, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}

									}));
		
		return bindingContext;
	}
	
	public class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{								
			if(inputElement instanceof Map)
			{								
				Map map = (Map) inputElement;
								
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof Map)
			{								
				Map map = (Map) parentElement;
				
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof Map)
			{
				Map map = (Map) element;		
				return !filterMap(map).isEmpty();
			}		
			else
			{
				return false;
			}
		}
		
		protected List<CartesianTriangularMeshMapLayer> filterMap(Map map)
		{
			List<CartesianTriangularMeshMapLayer> imageMapLayers = new ArrayList<CartesianTriangularMeshMapLayer>();
			for(AbstractMapLayer layer : map.getLayers())
			{
				if(layer instanceof CartesianTriangularMeshMapLayer)
				{
					imageMapLayers.add(((CartesianTriangularMeshMapLayer) layer));
				}
			}
			
			return imageMapLayers;
		}
	}	
}
