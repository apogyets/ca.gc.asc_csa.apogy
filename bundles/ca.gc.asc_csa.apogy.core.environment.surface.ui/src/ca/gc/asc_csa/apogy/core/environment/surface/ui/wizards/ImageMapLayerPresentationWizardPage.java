package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;

import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.Map;
import ca.gc.asc_csa.apogy.core.environment.surface.MapsList;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.MapLayerPresentationUISettings;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.ImageMapLayerPresentationComposite;

public class ImageMapLayerPresentationWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.ImageMapLayerPresentationWizardPage";
	
	private MapLayerPresentationUISettings mapLayerPresentationUISettings;
	
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private Tree tree;
	private TreeViewer treeViewer;
	
	private ImageMapLayerPresentationComposite imageMapLayerPresentationComposite;
					
	public ImageMapLayerPresentationWizardPage(MapLayerPresentationUISettings mapLayerPresentationUISettings) 
	{
		super(WIZARD_PAGE_ID);
		
		this.mapLayerPresentationUISettings = mapLayerPresentationUISettings;
		
		setTitle("Image Map Layer Presentation Settings");
		setDescription("Configure the Image Map Layer Presentation settings");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(2, false));
	
		Group imageLayersGroup = new Group(container, SWT.NONE);
		imageLayersGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		imageLayersGroup.setText("Image Layers");
		imageLayersGroup.setLayout(new GridLayout(1, false));
		
		// List the available ImageMapLayer.
		treeViewer = new TreeViewer(imageLayersGroup, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		tree = treeViewer.getTree();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		gd_tree.widthHint = 300;
		gd_tree.minimumWidth = 300;
		tree.setLayoutData(gd_tree);
		tree.setLinesVisible(true);
		
		treeViewer.setContentProvider(new CompositeFilterContentProvider());
		treeViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		treeViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				ImageMapLayer imageMapLayer = (ImageMapLayer)((IStructuredSelection) event.getSelection()).getFirstElement();									
				mapLayerPresentationUISettings.setImageMapLayer(imageMapLayer);				
				validate();
			}
		});
		
		MapsList mapList = (MapsList) mapLayerPresentationUISettings.getCartesianTriangularMeshMapLayer().getMap().eContainer();				
		treeViewer.setInput(mapList);
								
		setControl(container);			
		
		Group presentationSettingsGroup = new Group(container, SWT.NONE);
		presentationSettingsGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		presentationSettingsGroup.setText("Settings");
		presentationSettingsGroup.setLayout(new GridLayout(2, false));
		
		imageMapLayerPresentationComposite = new ImageMapLayerPresentationComposite(presentationSettingsGroup, SWT.NONE);
		imageMapLayerPresentationComposite.setImageMapLayerPresentation(mapLayerPresentationUISettings.getImageMapLayerPresentation());		
		new Label(presentationSettingsGroup, SWT.NONE);
	}	
		
	protected void validate()
	{
		
		if(mapLayerPresentationUISettings.getImageMapLayer() != null)
		{
			setErrorMessage(null);
			setPageComplete(true);
		}
		else
		{
			setErrorMessage("The Image Map Layer is not set !");
			setPageComplete(false);
		}
	}
	
	public class CompositeFilterContentProvider implements ITreeContentProvider 
	{
		@Override
		public void dispose() 
		{
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
		{		
		}
		
		@Override
		public Object[] getElements(Object inputElement) 
		{	
			if(inputElement instanceof MapsList)
			{
				MapsList mapsList = (MapsList) inputElement;
				return mapsList.getMaps().toArray();
			}			
			else if(inputElement instanceof Map)
			{								
				Map map = (Map) inputElement;
								
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object[] getChildren(Object parentElement) 
		{
			if(parentElement instanceof MapsList)
			{
				MapsList mapsList = (MapsList) parentElement;
				return mapsList.getMaps().toArray();
			}
			else if(parentElement instanceof Map)
			{								
				Map map = (Map) parentElement;
				
				// Keeps only ImageMapLayer.			
				return filterMap(map).toArray();
			}			
					
			return null;
		}

		@Override
		public Object getParent(Object element) 
		{		
			return null;
		}

		@Override
		public boolean hasChildren(Object element) 
		{		
			if(element instanceof MapsList)
			{
				MapsList mapsList = (MapsList) element;
				return !mapsList.getMaps().isEmpty();
			}
			else if(element instanceof Map)
			{
				Map map = (Map) element;		
				return !filterMap(map).isEmpty();
			}		
			else
			{
				return false;
			}
		}
		
		protected List<ImageMapLayer> filterMap(Map map)
		{
			List<ImageMapLayer> imageMapLayers = new ArrayList<ImageMapLayer>();
			for(AbstractMapLayer layer : map.getLayers())
			{
				if(layer instanceof ImageMapLayer)
				{
					imageMapLayers.add(((ImageMapLayer) layer));
				}
			}
			
			return imageMapLayers;
		}
	}	
}
