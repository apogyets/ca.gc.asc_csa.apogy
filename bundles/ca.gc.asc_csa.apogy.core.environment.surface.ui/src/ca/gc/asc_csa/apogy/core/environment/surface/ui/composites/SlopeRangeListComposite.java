package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshDiscreteSlopeImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.SlopeRange;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.Activator;


public class SlopeRangeListComposite extends Composite 
{	
	private CartesianTriangularMeshDiscreteSlopeImageMapLayer cartesianTriangularMeshDiscreteSlopeImageMapLayer;
		
	private TableViewer tableViewer;
	private Table table;
	private Button btnNew;
	private Button btnDelete;	
			
	private AdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
	private DataBindingContext m_bindingContext;
	
	public SlopeRangeListComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(2, false));
		
		tableViewer = new TableViewer(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.MULTI);
		table = tableViewer.getTable();
		GridData gd_tree = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_tree.widthHint = 300;
		gd_tree.minimumWidth = 300;
		table.setLayoutData(gd_tree);
		table.setLinesVisible(true);
		
		TableViewerColumn tableViewerColumnItem_Name = new TableViewerColumn(tableViewer, SWT.NONE);
		TableColumn trclmnItemName = tableViewerColumnItem_Name.getColumn();
		trclmnItemName.setWidth(200);
		
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));		
		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() 
		{						
			@Override
			public void selectionChanged(SelectionChangedEvent event) 
			{
				System.out.println("SELECTION : " + event);
				
				newSlopeRangeSelected((SlopeRange)((IStructuredSelection) event.getSelection()).getFirstElement());					
			}
		});
		
		// Buttons.
		Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite.setLayout(new GridLayout(1, false));	
		
		btnNew = new Button(composite, SWT.NONE);
		btnNew.setSize(74, 29);
		btnNew.setText("New");
		btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnNew.setEnabled(true);
		btnNew.addListener(SWT.Selection, new Listener() 
		{		
			@Override
			public void handleEvent(Event event) 
			{
				if (event.type == SWT.Selection) 
				{					
					SlopeRange newSlopeRange = ApogySurfaceEnvironmentFactory.eINSTANCE.createSlopeRange();
					newSlopeRange.setName("TEST");
					newSlopeRange.setSlopeLowerBound(0);
					newSlopeRange.setSlopeUpperBound(10);
					
					// Adds the slope range.
					ApogyCommonTransactionFacade.INSTANCE.basicAdd(getCartesianTriangularMeshDiscreteSlopeImageMapLayer(), ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER__SLOPE_RANGES, newSlopeRange);
					
					// Forces the viewer to refresh its input.
					if(!tableViewer.isBusy())
					{					
						tableViewer.setInput(getCartesianTriangularMeshDiscreteSlopeImageMapLayer().getSlopeRanges());
					}		
					
					// Selects the range just created.
					tableViewer.setSelection(new StructuredSelection(newSlopeRange));
				}					
			}
		});
				
		btnDelete = new Button(composite, SWT.NONE);
		btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		btnDelete.setSize(74, 29);
		btnDelete.setText("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addSelectionListener(new SelectionAdapter() 
		{
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				String imageMapLayersToDeleteMessage = "";

				Iterator<SlopeRange> slopeRangeList = getSelectedSlopeRanges().iterator();
				while (slopeRangeList.hasNext()) 
				{
					SlopeRange slopeRange = slopeRangeList.next();
					imageMapLayersToDeleteMessage = imageMapLayersToDeleteMessage + slopeRange.getName();

					if (slopeRangeList.hasNext()) 
					{
						imageMapLayersToDeleteMessage = imageMapLayersToDeleteMessage + ", ";
					}
				}

				MessageDialog dialog = new MessageDialog(null, "Delete the selected Slope Range", null,
						"Are you sure to delete these Slope Range: " + imageMapLayersToDeleteMessage, MessageDialog.QUESTION,
						new String[] { "Yes", "No" }, 1);
				int result = dialog.open();
				if (result == 0) 
				{
					for (SlopeRange slopeRange :  getSelectedSlopeRanges()) 
					{
						try 
						{
							ApogyCommonTransactionFacade.INSTANCE.basicRemove(getCartesianTriangularMeshDiscreteSlopeImageMapLayer(), ApogySurfaceEnvironmentPackage.Literals.CARTESIAN_TRIANGULAR_MESH_DISCRETE_SLOPE_IMAGE_MAP_LAYER__SLOPE_RANGES, slopeRange);
						} 
						catch (Exception e)	
						{
							Logger.INSTANCE.log(Activator.ID,
									"Unable to delete the Slope Range <"+ slopeRange.getName() + ">",
									EventSeverity.ERROR, e);
						}
					}
				}
				
				// Forces the viewer to refresh its input.
				if(!tableViewer.isBusy())
				{					
					tableViewer.setInput(getCartesianTriangularMeshDiscreteSlopeImageMapLayer().getSlopeRanges());
				}					
			}
		});
		
		// Dispose
		addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();				
			}
		});
	}	
	
	public CartesianTriangularMeshDiscreteSlopeImageMapLayer getCartesianTriangularMeshDiscreteSlopeImageMapLayer() {
		return cartesianTriangularMeshDiscreteSlopeImageMapLayer;
	}

	public void setCartesianTriangularMeshDiscreteSlopeImageMapLayer(CartesianTriangularMeshDiscreteSlopeImageMapLayer newLayer) 
	{
		if (m_bindingContext != null) 
		{
			m_bindingContext.dispose();
		}
		
		this.cartesianTriangularMeshDiscreteSlopeImageMapLayer = newLayer;
		
		if(newLayer != null)
		{
			m_bindingContext = customInitDataBindings();
			tableViewer.setInput(newLayer.getSlopeRanges());
			
			if(!newLayer.getSlopeRanges().isEmpty())
			{				
				SlopeRange selectedSlopeRange = newLayer.getSlopeRanges().get(0);
				tableViewer.setSelection(new StructuredSelection(selectedSlopeRange), true);
				newSlopeRangeSelected(selectedSlopeRange);
			}
			else
			{
				tableViewer.setSelection(null, true);
			}			
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<SlopeRange> getSelectedSlopeRanges()
	{
		return ((IStructuredSelection) tableViewer.getSelection()).toList();
	}
	
	public SlopeRange getSelectedSlopeRange()
	{
		List<SlopeRange> selected =  getSelectedSlopeRanges();
		if(selected != null && !selected.isEmpty())
		{
			return selected.get(0);
		}
		else
		{
			return null;	
		}
	}

	protected void newSlopeRangeSelected(SlopeRange newSlopeRange)
	{		
	}
	
	protected DataBindingContext customInitDataBindings() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		IObservableValue<?> observeSingleSelectionViewer = ViewerProperties.singleSelection().observe(tableViewer);
		
		/* Delete Button Enabled Binding. */
		IObservableValue<?> observeEnabledBtnDeleteObserveWidget = WidgetProperties.enabled().observe(btnDelete);
		bindingContext.bindValue(observeEnabledBtnDeleteObserveWidget, observeSingleSelectionViewer, null,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE)
						.setConverter(new Converter(Object.class, Boolean.class) {
							@Override
							public Object convert(Object fromObject) 
							{
								Boolean result = (fromObject != null);
								result = result && (getCartesianTriangularMeshDiscreteSlopeImageMapLayer().getSlopeRanges().size() > 1);
								return result;
							}
						}));
		
		return bindingContext;
	}
}
