package ca.gc.asc_csa.apogy.core.environment.surface.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.AbstractMapLayer;

public class OtherLayersDetailsComposite extends Composite 
{
	private AbstractMapLayer abstractMapLayer;
	private Composite compositeDetails;
	
	public OtherLayersDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.minimumWidth = 400;
		gd_compositeDetails.widthHint = 400;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));		
	}

	public AbstractMapLayer getAbstractMapLayer() {
		return abstractMapLayer;
	}

	public void setAbstractMapLayer(AbstractMapLayer newAbstractMapLayer) 
	{
		this.abstractMapLayer = newAbstractMapLayer;				
		
		// Update Details EMFForm.
		if(newAbstractMapLayer != null)
		{
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newAbstractMapLayer);
		}
	}		
}
