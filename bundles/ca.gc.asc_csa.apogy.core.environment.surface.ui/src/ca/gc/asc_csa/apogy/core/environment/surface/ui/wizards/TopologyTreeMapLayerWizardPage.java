package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.ui.composites.TopologyTreeEditingComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.TopologyTreeMapLayer;

public class TopologyTreeMapLayerWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.TopologyTreeMapLayerWizardPage";
		
	private TopologyTreeMapLayer topologyTreeMapLayer;				
		
	private TopologyTreeEditingComposite topologyTreeEditingComposite;
	
	private DataBindingContext m_bindingContext;
	
	public TopologyTreeMapLayerWizardPage(TopologyTreeMapLayer  topologyTreeMapLayer) 
	{
		super(WIZARD_PAGE_ID);
		this.topologyTreeMapLayer = topologyTreeMapLayer;
			
		setTitle("Topology Tree.");
		setDescription("Create the topology tree under the layer.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		
		topologyTreeEditingComposite = new TopologyTreeEditingComposite(top, SWT.BORDER, true);
		topologyTreeEditingComposite.setRoot(topologyTreeMapLayer.getTopologyTreeRoot());
		
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		setControl(top);
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);
		
		setPageComplete(getErrorMessage() == null);
	}

	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
		return bindingContext;
	}
}
