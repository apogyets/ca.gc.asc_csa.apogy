package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.RectangleShapeImageLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.AbstractShapeImageLayerComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.ImageMapLayerPreviewComposite;

public class RectangleShapeImageLayerWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.RectangleShapeImageLayerWizardPage";
	
	@SuppressWarnings("unused")
	private AbstractShapeImageLayerComposite  abstractShapeImageLayerComposite;
	
	private Text txtRectangleWidth;
	private Text txtRectangleHeight;	
	private Button btnShowCenterLines;
	
	private RectangleShapeImageLayer rectangleShapeImageLayer;
		
	private ImageMapLayerPreviewComposite imageMapLayerPreviewComposite;				
		
	private DataBindingContext m_bindingContext;
	
	public RectangleShapeImageLayerWizardPage(RectangleShapeImageLayer rectangleShapeImageLayer) 
	{
		super(WIZARD_PAGE_ID);
		this.rectangleShapeImageLayer = rectangleShapeImageLayer;
			
		setTitle("Rectangle Image Layer Settings.");
		setDescription("Sets the Rectangle Image Layer settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));
		
		Group layerSizeGroup = new Group(top, SWT.None);
		layerSizeGroup.setLayout(new GridLayout(2, false));
		layerSizeGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		layerSizeGroup.setText("Layer General Settings");
		
		abstractShapeImageLayerComposite = new AbstractShapeImageLayerComposite(layerSizeGroup, SWT.NONE, rectangleShapeImageLayer, false)
		{
			@Override
			protected void imageResolutionChanged(double newImageWidth) {
				validate();
			}
			
			@Override
			protected void imageHeightChanged(double newImageWidth) {
				validate();
			}
			
			@Override
			protected void imageWidthChanged(double newImageWidth) {
				validate();
			}
		};
		
		// Ellipse Seetting.
		Group ellipseGroup = new Group(top, SWT.None);
		ellipseGroup.setLayout(new GridLayout(2, false));
		ellipseGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ellipseGroup.setText("Rectangle Parameters");
			
		Label lblEllipseWidth = new Label(ellipseGroup, SWT.NONE);
		lblEllipseWidth.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEllipseWidth.setText("Rectangle Width (m):");
		
		txtRectangleWidth = new Text(ellipseGroup, SWT.BORDER);
		txtRectangleWidth.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtRectangleWidth.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				validate();
			}
		});
		
		Label lblEllipseHeight = new Label(ellipseGroup, SWT.NONE);
		lblEllipseHeight.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEllipseHeight.setText("Rectangle Width (m):");
		
		txtRectangleHeight = new Text(ellipseGroup, SWT.BORDER);
		txtRectangleHeight.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtRectangleHeight.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				validate();
			}
		});
		
		Label lblShowCenterLines = new Label(ellipseGroup, SWT.NONE);
		lblShowCenterLines.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblShowCenterLines.setText("Show Center Lines:");
		
		btnShowCenterLines = new Button(ellipseGroup, SWT.CHECK);
		btnShowCenterLines.setAlignment(SWT.RIGHT);
		btnShowCenterLines.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnShowCenterLines.setText("");
		btnShowCenterLines.setToolTipText("Whether or not to show the center lines.");		
		
		// Image Preview.
		Group grImagePreview = new Group(top, SWT.BORDER);
		grImagePreview.setText("Image Preview");
		grImagePreview.setLayout(new GridLayout(3, false));
		grImagePreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		imageMapLayerPreviewComposite = new ImageMapLayerPreviewComposite(grImagePreview, SWT.BORDER, rectangleShapeImageLayer);
		imageMapLayerPreviewComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));		
					
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}
	
	protected void validate()
	{
		setErrorMessage(null);
		
		if(rectangleShapeImageLayer.getRequiredResolution() <= 0)
		{
			setErrorMessage("Invalid image resolution specified <" + rectangleShapeImageLayer.getRequiredResolution() + "> must be larger than zero.");
		}
		
		if(rectangleShapeImageLayer.getRectangleWidth() <= 0)
		{
			setErrorMessage("Invalid Rectangle Width specified <" + rectangleShapeImageLayer.getRectangleWidth() + "> must be larger than zero.");
		}
		
		if(rectangleShapeImageLayer.getRectangleHeight() <= 0)
		{
			setErrorMessage("Invalid Rectangle Height specified <" + rectangleShapeImageLayer.getRectangleHeight() + "> must be larger than zero.");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
		/* Ellipse Width Value. */
		IObservableValue<Double> observEllipseWidthInMeters = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(rectangleShapeImageLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.RECTANGLE_SHAPE_IMAGE_LAYER__RECTANGLE_WIDTH)).observe(rectangleShapeImageLayer);
		IObservableValue<String> observeTxtEllipseWidth = WidgetProperties.text(SWT.Modify).observe(txtRectangleWidth);

		bindingContext.bindValue(observeTxtEllipseWidth,
								  observEllipseWidthInMeters, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		/* Ellipse Height Value. */
		IObservableValue<Double> observEllipseHeightInMeters = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(rectangleShapeImageLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.RECTANGLE_SHAPE_IMAGE_LAYER__RECTANGLE_HEIGHT)).observe(rectangleShapeImageLayer);
		IObservableValue<String> observeTxtEllipseHeight = WidgetProperties.text(SWT.Modify).observe(txtRectangleHeight);

		bindingContext.bindValue(observeTxtEllipseHeight,
								 observEllipseHeightInMeters, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		
		// TODO btnShowCenterLines
		
		return bindingContext;
	}
}
