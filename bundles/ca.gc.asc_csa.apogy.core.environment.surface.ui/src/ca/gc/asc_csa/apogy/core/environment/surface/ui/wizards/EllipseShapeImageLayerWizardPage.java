package ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;
import ca.gc.asc_csa.apogy.core.environment.surface.EllipseShapeImageLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.AbstractShapeImageLayerComposite;
import ca.gc.asc_csa.apogy.core.environment.surface.ui.composites.ImageMapLayerPreviewComposite;

public class EllipseShapeImageLayerWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.core.environment.surface.ui.wizards.EllipseShapeImageLayerWizardPage";
	
	@SuppressWarnings("unused")
	private AbstractShapeImageLayerComposite  abstractShapeImageLayerComposite;
	
	private Text txtEllipseWidth;
	private Text txtEllipseHeight;	
	private Button btnShowCenterLines;
	
	private EllipseShapeImageLayer ellipseShapeImageLayer;
		
	private ImageMapLayerPreviewComposite imageMapLayerPreviewComposite;				
		
	private DataBindingContext m_bindingContext;
	
	public EllipseShapeImageLayerWizardPage(EllipseShapeImageLayer ellipseShapeImageLayer) 
	{
		super(WIZARD_PAGE_ID);
		this.ellipseShapeImageLayer = ellipseShapeImageLayer;
			
		setTitle("Ellipse Image Layer Settings.");
		setDescription("Sets the Ellipse Image Layer settings.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));
		
		Group layerSizeGroup = new Group(top, SWT.None);
		layerSizeGroup.setLayout(new GridLayout(2, false));
		layerSizeGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		layerSizeGroup.setText("Layer General Settings");
		
		abstractShapeImageLayerComposite = new AbstractShapeImageLayerComposite(layerSizeGroup, SWT.NONE, ellipseShapeImageLayer, false)
		{
			@Override
			protected void imageResolutionChanged(double newImageWidth) {
				validate();
			}
			
			@Override
			protected void imageHeightChanged(double newImageWidth) {
				validate();
			}
			
			@Override
			protected void imageWidthChanged(double newImageWidth) {
				validate();
			}
		};
		
		// Ellipse Seetting.
		Group ellipseGroup = new Group(top, SWT.None);
		ellipseGroup.setLayout(new GridLayout(2, false));
		ellipseGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		ellipseGroup.setText("Ellipse Parameters");
			
		Label lblEllipseWidth = new Label(ellipseGroup, SWT.NONE);
		lblEllipseWidth.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEllipseWidth.setText("Ellipse Width (m):");
		
		txtEllipseWidth = new Text(ellipseGroup, SWT.BORDER);
		txtEllipseWidth.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtEllipseWidth.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				validate();
			}
		});
		
		Label lblEllipseHeight = new Label(ellipseGroup, SWT.NONE);
		lblEllipseHeight.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEllipseHeight.setText("Ellipse Width (m):");
		
		txtEllipseHeight = new Text(ellipseGroup, SWT.BORDER);
		txtEllipseHeight.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		txtEllipseHeight.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				validate();
			}
		});
		
		Label lblShowCenterLines = new Label(ellipseGroup, SWT.NONE);
		lblShowCenterLines.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblShowCenterLines.setText("Show Center Lines:");
		
		btnShowCenterLines = new Button(ellipseGroup, SWT.CHECK);
		btnShowCenterLines.setAlignment(SWT.RIGHT);
		btnShowCenterLines.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		btnShowCenterLines.setText("");
		btnShowCenterLines.setToolTipText("Whether or not to show the center lines.");		
		
		// Image Preview.
		Group grImagePreview = new Group(top, SWT.BORDER);
		grImagePreview.setText("Image Preview");
		grImagePreview.setLayout(new GridLayout(3, false));
		grImagePreview.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		imageMapLayerPreviewComposite = new ImageMapLayerPreviewComposite(grImagePreview, SWT.BORDER, ellipseShapeImageLayer);
		imageMapLayerPreviewComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));		
					
		setControl(top);	
	
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {
				if (m_bindingContext != null) m_bindingContext.dispose();
				
			}
		});
	}	
	
	protected void validate()
	{
		setErrorMessage(null);
		
		if(ellipseShapeImageLayer.getRequiredResolution() <= 0)
		{
			setErrorMessage("Invalid image resolution specified <" + ellipseShapeImageLayer.getRequiredResolution() + "> must be larger than zero.");
		}
		if(ellipseShapeImageLayer.getEllipseWidth() <= 0)
		{
			setErrorMessage("Invalid Ellipse Width specified <" + ellipseShapeImageLayer.getEllipseWidth() + "> must be larger than zero.");
		}
		
		if(ellipseShapeImageLayer.getEllipseHeight() <= 0)
		{
			setErrorMessage("Invalid Ellipse Height specified <" + ellipseShapeImageLayer.getEllipseHeight() + "> must be larger than zero.");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();			
		
		/* Ellipse Width Value. */
		IObservableValue<Double> observEllipseWidthInMeters = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(ellipseShapeImageLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.ELLIPSE_SHAPE_IMAGE_LAYER__ELLIPSE_WIDTH)).observe(ellipseShapeImageLayer);
		IObservableValue<String> observeTxtEllipseWidth = WidgetProperties.text(SWT.Modify).observe(txtEllipseWidth);

		bindingContext.bindValue(observeTxtEllipseWidth,
								  observEllipseWidthInMeters, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		/* Ellipse Height Value. */
		IObservableValue<Double> observEllipseHeightInMeters = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(ellipseShapeImageLayer), 
																	  FeaturePath.fromList(ApogySurfaceEnvironmentPackage.Literals.ELLIPSE_SHAPE_IMAGE_LAYER__ELLIPSE_HEIGHT)).observe(ellipseShapeImageLayer);
		IObservableValue<String> observeTxtEllipseHeight = WidgetProperties.text(SWT.Modify).observe(txtEllipseHeight);

		bindingContext.bindValue(observeTxtEllipseHeight,
								 observEllipseHeightInMeters, 
								 new UpdateValueStrategy().setConverter(new Converter(String.class, Double.class)
								 {																		
									@Override
									public Object convert(Object fromObject) 
									{										
										return Double.parseDouble((String) fromObject);
									}
			
									}), 
								 	new UpdateValueStrategy().setConverter(new Converter(Double.class, String.class)
								 	{																		 											
										@Override
										public Object convert(Object fromObject) 
										{											
											return ((Double) fromObject).toString();
										}
									}));
		
		
		// TODO btnShowCenterLines
		
		return bindingContext;
	}
}
