/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
@GenModel(prefix="ApogySurfaceEnvironmentUI",
		  childCreationExtenders="true",
		  extensibleProviderFactory="true",
		  multipleEditorPages="false",
          copyrightText="Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
    Pierre Allard (Pierre.Allard@canada.ca), 
    Regent L'Archeveque (Regent.Larcheveque@canada.ca),
    Sebastien Gemme (Sebastien.Gemme@canada.ca),
    Canadian Space Agency (CSA) - Initial API and implementation",
		  modelName="ApogyCoreSurfaceEnvironmentUI",
		  complianceLevel="6.0",
		  suppressGenModelAnnotations="false",
		  dynamicTemplates="true", 
		  templateDirectory="platform:/plugin/ca.gc.asc_csa.apogy.common.emf.templates")
@GenModel(modelDirectory="/ca.gc.asc_csa.apogy.core.environment.surface.ui/src-generated")
@GenModel(editDirectory="/ca.gc.asc_csa.apogy.core.environment.surface.ui.edit/src-generated")

package ca.gc.asc_csa.apogy.core.environment.surface.ui

import ca.gc.asc_csa.apogy.common.emf.Described
import ca.gc.asc_csa.apogy.common.emf.Named
import ca.gc.asc_csa.apogy.common.emf.ui.EClassSettings
import ca.gc.asc_csa.apogy.common.emf.ui.NamedDescribedWizardPagesProvider
import ca.gc.asc_csa.apogy.common.images.AbstractEImage
import ca.gc.asc_csa.apogy.common.math.Matrix4x4

import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation
import ca.gc.asc_csa.apogy.common.geometry.data3d.ui.CartesianTriangularMeshPresentation

import ca.gc.asc_csa.apogy.core.ApogySystemApiAdapter
import ca.gc.asc_csa.apogy.core.FeatureOfInterestList
import ca.gc.asc_csa.apogy.core.PoseProvider
import ca.gc.asc_csa.apogy.core.environment.Star

import ca.gc.asc_csa.apogy.core.environment.surface.Map
import ca.gc.asc_csa.apogy.core.environment.surface.CartesianTriangularMeshMapLayer
import ca.gc.asc_csa.apogy.core.environment.surface.FeaturesOfInterestMapLayer
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayerPresentation
import ca.gc.asc_csa.apogy.core.environment.surface.RectangularRegion
import ca.gc.asc_csa.apogy.core.environment.surface.RectangularRegionProvider
import ca.gc.asc_csa.apogy.core.environment.surface.ImageMapLayer

import ca.gc.asc_csa.apogy.core.invocator.AbstractToolsListContainer
import ca.gc.asc_csa.apogy.core.invocator.InvocatorSession
import ca.gc.asc_csa.apogy.core.invocator.Variable
import org.eclipse.emf.ecore.EObject


// Types
type List<T> wraps java.util.List
type HashMap<key, value> wraps java.util.HashMap

type Point2d wraps javax.vecmath.Point2d
type Color3f wraps javax.vecmath.Color3f
type Point3f wraps javax.vecmath.Point3f

type XYSeries wraps  org.jfree.data.xy.XYSeries
type XYPlot wraps org.jfree.chart.plot.XYPlot
type XYDataItem wraps org.jfree.data.xy.XYDataItem
type AbstractXYAnnotation wraps org.jfree.chart.annotations.AbstractXYAnnotation
type ChartComposite wraps org.jfree.experimental.chart.swt.ChartComposite
type JFreeChart wraps org.jfree.chart.JFreeChart



class AbstractSurfaceWorksitePresentation extends NodePresentation
{
	@GenModel(propertyCategory="AXIS")
	boolean axisVisible = "true"
	
	@GenModel(propertyCategory="AXIS", apogy_units="m")
	double axisLength = "10.0"
	
	@GenModel(propertyCategory="SKY")
	boolean azimuthVisible = "true"
	
	@GenModel(propertyCategory="SKY")
	boolean elevationLinesVisible = "true"
		
	@GenModel(propertyCategory="SKY")
	boolean azimuthLinesVisible = "true"
		
	@GenModel(propertyCategory="PLANE")
	boolean planeVisible = "true"
	
	@GenModel(propertyCategory="PLANE", apogy_units="m")
	double planeGridSize = "1.0"
	
	@GenModel(propertyCategory="PLANE", apogy_units="m")
	double planeSize = "10.0"	
}

class AbstractSurfaceWorksiteSkyPresentation extends NodePresentation
{
	@GenModel(propertyCategory="Horizon")
	boolean horizonVisible = "true"
}

class MoonPresentation extends NodePresentation
{
	
}

class FeatureOfInterestNodePresentation extends NodePresentation
{
	@GenModel(propertyCategory="FLAG", apogy_units="m")
	double poleHeight
	
	@GenModel(propertyCategory="FLAG")
	boolean flagVisible = "false"
}

class ApogySurfaceEnvironmentUIFacade
{
	//op void getImageMapLayerPresentationExtent(ImageMapLayerPresentation imageMapLayerPresentation, Tuple3d lowerLeftCorner, Tuple3d upperRightCorner)
	
	//op void getImageMapLayerPresentationExtent(List<ImageMapLayerPresentation> imageMapLayerPresentations, Tuple3d lowerLeftCorner, Tuple3d upperRightCorner)
	
	/*
	 * Returns an image representing the assembly of the specified list of ImageMapLayerPresentation. 
	 * @param imageMapLayerPresentations The specified list of ImageMapLayerPresentation.
	 * @return The image representing the specified list of ImageMapLayerPresentation.
	 */
	//op AbstractEImage getImageMapLayerPresentationImage(List<ImageMapLayerPresentation> imageMapLayerPresentations)
	
	/*
	 * Returns the ImageMapLayerPresentation with the highest resolution (minimum meters/pixel).
	 * @return The ImageMapLayerPresentation with the highest resolution (minimum meters/pixel)
	 */
	//op ImageMapLayerPresentation getBestResolutionMapLayer(List<ImageMapLayerPresentation> imageMapLayerPresentations)
	
	/*
	 * Returns the list of visible ImageMapLayerPresentation from"" a specified imageMapLayerPresentations.
	 * @param imageMapLayerPresentations The specified list of ImageMapLayerPresentation.
	 * @return The list of visible ImageMapLayerPresentation.
	 */
	//op List<ImageMapLayerPresentation> getVisibleImageMapLayerPresentation(List<ImageMapLayerPresentation> imageMapLayerPresentations)				
				
				
	op List<RectangularRegionProvider> getVisibleRectangularRegionProvider(MapViewConfiguration mapViewConfiguration)
				
	/*
	 * Returns an image representing the specified MapViewExtent using a specified MapViewConfiguration.
	 * If the MapViewExtent is larger than the area covered by the MapViewConfiguration, transparent pixels will be added.
	 * @param mapViewConfiguration The specified MapViewConfiguration.
	 * @param mapViewExtent The specified ground area to be covered.
	 * @param maximumImageSizePixels The maximum size, in pixel, of the generated image.
	 * @return The image representing the specified MapViewExtent.
	 */
	op AbstractEImage getImageMapLayerPresentationImage(MapViewConfiguration mapViewConfiguration, RectangularRegion mapViewExtent, int maximumImageSizePixels)
	
	/*
	 * Computes the length of a 2D trajectory.
	 * @param The XYSeries.
	 * @return The length of the trajectory represented in the XYSerie.
	 */
	op double getTrajectoryLength(XYSeries xySeries)
	
	/*
	 * Return the identifier associated with a given MapViewConfiguration.
	 * @param mapViewConfiguration The given MapViewConfiguration.
	 * @return The identifier, null if none is found.
	 */
	op String getMapViewConfigurationIdentifier(MapViewConfiguration mapViewConfiguration)
	
	/*
	 * Return the MapViewConfiguration (in the Active Session) with the specified identifier.
	 * @param identifier The MapViewConfiguration identifier.
	 * @return The MapViewConfiguration with the specified identifier, null if non is found.
	 */
	op MapViewConfiguration getActiveMapViewConfiguration(String identifier)
	
	/*
	 * Return the MapViewConfigurationList in the Active Session.
	 * @return The MapViewConfigurationList in the Active Session, null if none is found.
	 */
	op MapViewConfigurationList getActiveMapViewConfigurationList() 	
	
	op List<FeatureOfInterestList> getFeatureOfInterestLists(InvocatorSession session)	
}

// Utilities

class EnvironmentSurfaceUIUtilities
{
	op Point3f toPoint3f(Star star)		
	
	op float getPointSizeForMagnitude(float magnitude, float magnitudeRangeMinimum, float magnitudeRangeMaximum, float minimumPointSize, float maximumPointSize)	
}

// MapView Definitions

class MapViewConfigurationList extends AbstractToolsListContainer
{
	contains MapViewConfiguration[0..*] mapViewConfigurations
}


/*
 * Configuration used for the Map View.
 */
class MapViewConfiguration extends Named, Described
{		
	/*
	 * List of Maps being displayed.
	 */
	@GenModel(children="true", notify="true", property="None")
	contains ImageMapLayerPresentation[0..*] mapLayers

	/*
	 * Color of the map background.
	 */
	Color3f backgroundColor = "1.0,1.0,1.0"

	/*
	 * List of Apogy System instances being tracked.
	 */
	@GenModel(children="true", notify="true", property="None")
	contains MapAnnotation[0..*] mapAnnotations		
	
	/*
	 * Default region to use when none is covered by the map Layers
	 */
	refers transient RectangularRegion[1] defaultRectangularRegion
	
	/*
	 * The image representing the active ImageMapLayers.
	 */
	@GenModel(children="false", notify="true", property="None")
	refers derived transient readonly AbstractEImage mapImage			
	
	/* 
	 * The rectangular region covered by the image layers defined in the map.
	 */
	@GenModel(children="false", notify="true", property="Readonly")
	refers derived transient readonly RectangularRegion[1] mapImageRectangularRegion
	
	/* 
	 * The rectangular region covered by the map (typically larger than
	 * mapImageRectangularRegion and contains mapImageRectangularRegion).
	 */
	@GenModel(children="false", notify="true", property="Readonly")
	refers derived transient RectangularRegion[1] extent
	
	/* 
	 * Forces the updates of all derived values.
	 */
	op void forceUpdate()
}

/*
 * Presentation properties for an item displayed 
 * on the MapView.
 */
abstract class MapViewItemPresentation
{
	/*
	 * Visibility
	 */
	boolean visible = "true"
}


/*
 * Presentation properties for an ImageMapLayer displayed on the MapView.
 */
//class ImageMapLayerPresentation extends MapViewItemPresentation, RectangularRegionImage
//{
	/** Image Map Layer being displayed.*/
//	refers ImageMapLayer imageMapLayer
	
	/**
	 * The alpha to use to display this layer, from fully opaque (1.0f) to fully transparent (0.0f).
	 */
//	float alpha = "1.0"	
//}


/*
 * Presentation properties for an object shown on top of the maps.
 */
abstract class MapAnnotation extends MapViewItemPresentation
{
	/*
	 *  TheXYPlot being used by MapView.
	 */
	op List<AbstractXYAnnotation> getXYShapeAnnotation()
}

/*
 * Base class of all map tools.
 */
abstract class MapTool extends MapAnnotation
{
	/*
	 * Whether or not the tool is active.
	 */	
	boolean active = "true"
	
	/*
	 * Method that is called to give access to the underlying ChartComposite and JFreeChart used to display the map.
	 * @param composite The ChartComposite used to display the map.
	 * @param chart The JFreeChart used to display the map.
	 */
	op void initialize(ChartComposite composite, JFreeChart chart)
	
	/*
	 * Method that is called to dispose of the tool.
	 */
	op void dispose()
	
	/*
	 * Method called when the user clicks on the map with the mouse.
	 * @param mouseButton The mouse button clicked
	 * @param x The absolute position x coordinates of the point selected.
	 * @param y The absolute position y coordinates of the point selected.
	 */
	op void positionSelected(int mouseButton, double x, double y)
}

/*
 * Tool that shows the distance between two point on the map. The use needs to 
 * selects two positions on the map by clicking using the left button on the mouse.
 */
class MapRuler extends MapTool
{	
	/*
	 * The color of the ruler.
	 */
	Color3f rulerColor	= "0.0,1.0,0.0"
}

/**
 * Presentation properties associated with a CartesianTriangularMeshMapLayerNode.
 */
class CartesianTriangularMeshMapLayerNodePresentation extends CartesianTriangularMeshPresentation
{	
}

/*
 * Presentation properties for a FeaturesOfInterestMapLayer.
 */
class FeaturesOfInterestMapLayerPresentation extends MapAnnotation, RectangularRegionProvider
{
	@GenModel(notify="true", propertyCategory="FOI_PROPERTIES")
	refers FeaturesOfInterestMapLayer featuresOfInterestMapLayer
		
	@GenModel(notify="true", propertyCategory="FOI_PROPERTIES", apogy_units="m")
	double featureOfInterestRadius = "0.25"
	
	/** The color of the vector. */
	@GenModel(propertyCategory="FOI_PROPERTIES")
	Color3f featureOfInterestColor = "0.0,0.0,1.0"		
}

// Trajectory Annotation and Tools.

/*
 * Base class for classes providing a trajectory.
 */
abstract class TrajectoryProvider
{
	/*
	 * Re-initialize the TrajectoryProvider.
	 */
	op void initialize()
	
	/*
	 * Clears the list of points and associated trajectory.
	 */
	op void clear()
	
	/* 
	 * Returns the current trajectory as a list of Point2d.
	 */
	op List<Point2d> asListOfPoint2d()		
	
	/*
	 * The XYSeries containing the trajectory data to be displayed.
	 */
	@GenModel(property="None")
	op XYSeries getXYSeries()
	
	/*
	 * Latest x coordinates.
	 */	
	transient XYDataItem latestPosition
	
	/*
	 * The azimuth of the latest position, in radians, as measured relative to the x axis, positive clockwise.
	 */
	@GenModel(apogy_units="rad")
	transient double azimuthAngle = "0.0"		
	
	/*
	 * The current length of the trajectory.
	 */
	@GenModel(apogy_units="m")
	transient double trajectoryLength = "0.0"
	
	/*
	 * The color of the ruler.
	 */
	Color3f trajectoryColor	= "0.0,1.0,0.0"
}

/* 
 * Base class for user map tools that display trajectory on a map.
 */
abstract class AbstractTrajectoryTool extends TrajectoryProvider, MapTool
{
	
}

/*
 * Tool that allows a user to define a trajectory by clicking on a map.
 * Clicking on the left mouse button add a point to the trajectory, cliking on 
 * the right mouse button removes the last point of the trajectory.
 */
class TrajectoryPickingTool extends AbstractTrajectoryTool
{
	
}

/*
 * Base class for TrajectoryProvider that are providing trajectory based on a VariableTrajectoryAnnotation.
 */
abstract class VariableTrajectoryProvider extends TrajectoryProvider
{
	@GenModel(property="None")
	container VariableTrajectoryAnnotation variableAnnotation opposite trajectoryProvider
	
	/*
	 *  The Apogy System being displayed. This is automatically updated.
	 */
	refers transient PoseProvider[1] poseProvider
}

/*
 * Default implementation of VariableTrajectoryProvider. This implementation makes use of thresholds 
 * to limits the number of position update of the trajectory to improve performance.
 */
class DefaultVariableTrajectoryProvider extends VariableTrajectoryProvider
{
	/*
	 *  Minimum distance to keep between points added to the XYSeries.
	 */
	@GenModel(propertyCategory="THRESHOLDS", apogy_units="m")
	double distanceThreshold = "0.5"
	
	/*
	 *  Minimum azimuth change that will trigger an azimuthAngle change.
	 */
	@GenModel(propertyCategory="THRESHOLDS", apogy_units="rad")
	double azimuthThreshold = "0.017"
}

// Variable Annotations.

/*
 * Base class used for MapAnnotation representing a variable on a Map.
 */
abstract class AbstractVariableAnnotation extends MapAnnotation
{		
	/*
	 * The Apogy System being displayed.
	 */
	refers Variable[1] variable
	
	/*
	 * The instance of the object adapted by the Apogy System.
	 */
	refers transient EObject[0..1] variableInstance	
	
	/*
	 * The current instance of  ApogySystemApiAdapter handling the variable.
	 */
	refers transient ApogySystemApiAdapter[0..1] apogySystemApiAdapter
	
	/*
	 * Method called when the pose of the variable changes.
	 * @param newPose The new pose matrix.
	 */
	op void updatePose(Matrix4x4 newPose)
}

/*
 * AbstractVariableAnnotation that shows the position and orientation of a variable on a Map.
 */
class PoseVariableAnnotation extends AbstractVariableAnnotation
{
	/*
	 * The length of the vector.NamedWizardPagesProvider
	 */
	@GenModel(propertyCategory="VECTOR_PROPERTIES", apogy_units="m")
	double vectorlength = "10.0"
	
	/*
	 * The color of the vector.
	 */
	@GenModel(propertyCategory="VECTOR_PROPERTIES")
	Color3f vectorColor = "0.0,0.0,1.0"
	
	/*
	 * Whether to show the pose as text.
	 */
	boolean showPose = "true"	
		
	/*
	 * Method called when the position or orientation of the variable changes.
	 * @param x The x coordinates, in meters.
	 * @param y The y coordinates, in meters.
	 * @param zRotation The azimuth, in radians. 
	 */
	op void updatePose(@GenModel(apogy_units="m") double x, @GenModel(apogy_units="m") double y, @GenModel(apogy_units="rad") double zRotation)
}	

/*
 * AbstractVariableAnnotation that draws the trajectory of a variable on a map. 
 */
class VariableTrajectoryAnnotation extends AbstractVariableAnnotation, TrajectoryProvider
{	
	/*
	 * The trajectory provider.
	 */
	contains VariableTrajectoryProvider[1] trajectoryProvider opposite variableAnnotation
}

/*
 * Specialization of PoseVariableAnnotation that also draws the shape of the vehicle on the map. 
 */
class VehicleVariableAnnotation extends PoseVariableAnnotation
{
	/*
	 * The length of the vehicle.
	 */
	@GenModel(propertyCategory="VEHICLE_DIMENSIONS",apogy_units="m")
	double vehicleLength = "1.0"
	
	/*
	 * The width of the vehicle.
	 */
	@GenModel(propertyCategory="VEHICLE_DIMENSIONS",apogy_units="m")
	double vehicleWidth = "0.5"
}

// Wizard Support for Map

class MapUISettings extends EClassSettings
{
	/*
	 * Name to give the MapUISettings.
	 */
	String name
}

class MapWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

// Wizard Support for CartesianTriangularMeshMapLayer

class CartesianTriangularMeshMapLayerUISettings extends EClassSettings
{
	/*
	 * Name to give the MapUISettings.
	 */
	String name
}

// Wizard Provider to create CartesianTriangularMeshURLMapLayer
class CartesianTriangularMeshURLMapLayerWizardPagesProvider extends NamedDescribedWizardPagesProvider
{		
}

// Wizard Support for ImageMapLayer

class ImageMapLayerUISettings extends EClassSettings
{
	/*
	 * Name to give the ImageMapLayer.
	 */
	String name
	
	/*
	 * Map where the ImageMapLayer is to be contained.
	 */
	refers Map map		
	
	/*
 	 * Map that stores user data.
 	 */
 	HashMap<String, Object> userDataMap
}

// Wizard Provider to create ImageMapLayer
class URLImageMapLayerWizardPagesProvider extends NamedDescribedWizardPagesProvider
{		
}

// Wizard Support for MapLayerPresentation
class MapLayerPresentationUISettings extends EClassSettings
{	
	String name
	refers CartesianTriangularMeshMapLayer cartesianTriangularMeshMapLayer
	refers ImageMapLayer imageMapLayer 
	refers ImageMapLayerPresentation imageMapLayerPresentation
}

// Wizard Provider to create ImageMapLayerPresentation
class ImageMapLayerPresentationWizardPagesProvider extends NamedDescribedWizardPagesProvider
{		
		
}

// Wizard Provider to create CartesianTriangularMeshDerivedImageMapLayer
class CartesianTriangularMeshDerivedImageMapLayerWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

// Wizard Provider to create CartesianTriangularMeshSlopeImageMapLayer
class CartesianTriangularMeshSlopeImageMapLayerWizardPagesProvider extends CartesianTriangularMeshDerivedImageMapLayerWizardPagesProvider
{	
}

// Wizard Provider to create CartesianTriangularMeshDiscreteSlopeImageMapLayer
class CartesianTriangularMeshDiscreteSlopeImageMapLayerWizardPagesProvider extends CartesianTriangularMeshDerivedImageMapLayerWizardPagesProvider
{	
}

// Wizard Provider to create CartesianTriangularMeshHeightImageMapLayer
class CartesianTriangularMeshHeightImageMapLayerWizardPagesProvider extends CartesianTriangularMeshDerivedImageMapLayerWizardPagesProvider
{	
}

// Wizard Provider to create LineOfSightImageMapLayer
class FixedPositionLineOfSightImageMapLayerWizardPagesProvider extends CartesianTriangularMeshDerivedImageMapLayerWizardPagesProvider
{	
}

// Wizard support to create EllipseShapeImageLayer
class EllipseShapeImageLayerWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

// Wizard support to create RectangleShapeImageLayer
class RectangleShapeImageLayerWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

// Wizard support to create CartesianCoordinatesPolygonShapeImageMapLayer
class CartesianCoordinatesPolygonShapeImageMapLayerWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

// Wizard support to create FeaturesOfInterestMapLayer.
class FeaturesOfInterestMapLayerWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}

// Wizard Support for TopologyTreeMapLayer
class TopologyTreeMapLayerWizardPagesProvider extends NamedDescribedWizardPagesProvider
{	
}