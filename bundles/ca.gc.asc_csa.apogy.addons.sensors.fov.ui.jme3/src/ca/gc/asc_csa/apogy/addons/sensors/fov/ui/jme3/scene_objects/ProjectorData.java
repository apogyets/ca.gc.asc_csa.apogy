package ca.gc.asc_csa.apogy.addons.sensors.fov.ui.jme3.scene_objects;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import com.jme3.math.Vector3f;
import com.jme3.post.SimpleTextureProjector;
import com.jme3.scene.debug.WireFrustum;

public class ProjectorData 
{
	public SimpleTextureProjector projector;
	public WireFrustum frustum;
	public Vector3f[] frustumPoints;
}
