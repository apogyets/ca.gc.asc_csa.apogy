package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.wizards;
/*
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;

public class EMFFormsWizardPage extends WizardPage {

	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.emf.ui.emfforms.wizards.EMFFormsWizardPage";

	private EObject eObject;
	/**
	 * Constructor for the WizardPage.
	 * 
	 * @param pageName
	 */
	public EMFFormsWizardPage() {
		super(WIZARD_PAGE_ID);
		setTitle("Edit element");
	}

	public EMFFormsWizardPage(EObject eObject) {
		this();
		this.eObject = eObject;
	}

	/**
	 * @see IDialogPage#createControl(Composite)
	 */
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.None);
		container.setLayout(new GridLayout(1, false));
		
		ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(container, this.eObject);

		setControl(container);

		setPageComplete(true);
	}
}