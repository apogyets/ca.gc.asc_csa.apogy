/**
 * Copyright (c) 2011-2013 EclipseSource Muenchen GmbH and others.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eugen Neufeld - initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VContainedElement;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.swt.core.AbstractSWTRenderer;
import org.eclipse.emfforms.spi.swt.core.SWTDataElementIdHelper;
import org.eclipse.emfforms.spi.swt.core.layout.GridDescriptionFactory;
import org.eclipse.emfforms.spi.swt.core.layout.SWTGridCell;
import org.eclipse.emfforms.spi.swt.core.layout.SWTGridDescription;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

public abstract class AbstractCustomElementSWTRenderer extends AbstractSWTRenderer<VContainedElement> {

	public AbstractCustomElementSWTRenderer(VContainedElement vElement, ViewModelContext viewContext,
			ReportService reportService) {
		super(vElement, viewContext, reportService);
	}

	private SWTGridDescription rendererGridDescription;

	@Override
	protected void dispose() {
		rendererGridDescription = null;
		super.dispose();
	}

	@Override
	public SWTGridDescription getGridDescription(SWTGridDescription gridDescription) {
		if (rendererGridDescription == null) {
			final int columns = 3;

			rendererGridDescription = GridDescriptionFactory.INSTANCE.createEmptyGridDescription();
			rendererGridDescription.setRows(1);
			rendererGridDescription.setColumns(columns);

			final List<SWTGridCell> grid = new ArrayList<SWTGridCell>();

			/** Create the cell for the label */
			final SWTGridCell labelCell = createLabelCell(grid.size());
			grid.add(labelCell);

			/** Create the cell for the validation icon */
			final SWTGridCell validationCell = createValidationCell(grid.size());
			grid.add(validationCell);

			/** Create the cell for the control */
			final SWTGridCell controlCell = createControlCell(grid.size());
			grid.add(controlCell);

			rendererGridDescription.setGrid(grid);
		}
		return rendererGridDescription;
	}

	@Override
	protected Control renderControl(SWTGridCell cell, Composite parent) {
		int controlIndex = cell.getColumn();
		switch (controlIndex) {
		case 0:
			return createLabel(parent);
		case 1:
			return createValidationIcon(parent);
		case 2:
			return createControl(parent);
		default:
			throw new IllegalArgumentException(String.format(
					"The provided SWTGridCell (%1$s) cannot be used by this (%2$s) renderer.", cell.toString(), //$NON-NLS-1$
					toString()));
		}
	}

	/**
	 * Creates the label in front of the control. The text on this label can be
	 * overwritten with {@link #getLabelText()}.
	 * 
	 * @param parent
	 * @return
	 */
	protected Control createLabel(Composite parent) {
		Label label = new Label(parent, SWT.None);
		label.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TRANSPARENT));
		label.setText(getLabelText());
		return label;
	}

	/**
	 * Can be overwritter to change the label text.
	 * 
	 * @return
	 */
	protected String getLabelText() {
		return getViewModelContext().getDomainModel().eClass().getName();
	}

	/**
	 * Creates the validation icon.
	 * 
	 * @param composite
	 * @return
	 */
	protected Label createValidationIcon(Composite composite) {
		final Label validationLabel = new Label(composite, SWT.NONE);
		SWTDataElementIdHelper.setElementIdDataWithSubId(validationLabel, getVElement(), "control_validation", //$NON-NLS-1$
				getViewModelContext());
		validationLabel.setBackground(composite.getBackground());
		return validationLabel;
	}

	/**
	 * Creates the control to display the wanted value. This should only be a
	 * control (Label, text, etc...) because EMFForms does not work with
	 * composites. To have a composite, you need to have a different grid
	 * description and create cells for theses controls.
	 * 
	 * @param parent
	 * @return
	 */
	protected abstract Control createControl(Composite parent);

	/**
	 * Creates the cell for the label.
	 * 
	 * @param column
	 * @return
	 */
	protected SWTGridCell createLabelCell(int column) {
		final SWTGridCell labelCell = new SWTGridCell(0, column, this);
		labelCell.setHorizontalGrab(false);
		labelCell.setVerticalGrab(false);
		labelCell.setHorizontalFill(false);
		labelCell.setHorizontalAlignment(SWTGridCell.Alignment.BEGINNING);
		labelCell.setVerticalFill(false);
		labelCell.setVerticalAlignment(SWTGridCell.Alignment.CENTER);
		labelCell.setRenderer(this);
		return labelCell;
	}

	/**
	 * Creates the cell for the validation icon.
	 * 
	 * @param column
	 * @return
	 */
	protected SWTGridCell createValidationCell(int column) {
		final SWTGridCell validationCell = new SWTGridCell(0, column, this);
		validationCell.setHorizontalGrab(false);
		validationCell.setVerticalGrab(false);
		validationCell.setHorizontalFill(false);
		validationCell.setHorizontalAlignment(SWTGridCell.Alignment.CENTER);
		validationCell.setVerticalFill(false);
		validationCell.setVerticalAlignment(SWTGridCell.Alignment.CENTER);
		validationCell.setRenderer(this);
		validationCell.setPreferredSize(10, 10);
		return validationCell;
	}

	/**
	 * Creates the cell for the validation control.
	 * 
	 * @param column
	 * @return
	 */
	protected SWTGridCell createControlCell(int column) {
		final SWTGridCell controlCell = new SWTGridCell(0, column, this);
		controlCell.setHorizontalGrab(true);
		controlCell.setVerticalGrab(false);
		controlCell.setHorizontalFill(true);
		controlCell.setVerticalFill(false);
		controlCell.setVerticalAlignment(SWTGridCell.Alignment.CENTER);
		controlCell.setRenderer(this);
		return controlCell;
	}

}
