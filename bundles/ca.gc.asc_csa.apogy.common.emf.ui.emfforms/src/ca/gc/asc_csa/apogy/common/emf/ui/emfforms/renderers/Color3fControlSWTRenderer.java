package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.renderers;

import javax.inject.Inject;
import javax.vecmath.Color3f;

import org.eclipse.emf.ecp.view.spi.context.ViewModelContext;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.template.model.VTViewTemplateProvider;
import org.eclipse.emfforms.spi.common.report.ReportService;
import org.eclipse.emfforms.spi.core.services.databinding.DatabindingFailedException;
import org.eclipse.emfforms.spi.core.services.databinding.EMFFormsDatabinding;
import org.eclipse.emfforms.spi.core.services.label.EMFFormsLabelProvider;
import org.eclipse.swt.graphics.RGB;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.Activator;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

public class Color3fControlSWTRenderer extends AbstractColorControlSWTRenderer{

	@Inject
	public Color3fControlSWTRenderer(VControl vElement, ViewModelContext viewContext, ReportService reportService,
			EMFFormsDatabinding emfFormsDatabinding, EMFFormsLabelProvider emfFormsLabelProvider,
			VTViewTemplateProvider vtViewTemplateProvider) {
		super(vElement, viewContext, reportService, emfFormsDatabinding, emfFormsLabelProvider, vtViewTemplateProvider);
	}

	@Override
	protected RGB getRGB() {

		Object obj = getValue();
		/** Converts color3f */
		if (obj instanceof Color3f) {
			Color3f color = (Color3f) obj;

			int red = Math.round(255.0f * (color.getX()));
			int green = Math.round(255.0f * (color.getY()));
			int blue = Math.round(255.0f * (color.getZ()));

			if(red >= 0 && green >= 0 && blue >= 0){
				return new RGB(red, green, blue);
			}
		}
		return null;

	}

	@Override
	protected Object convert(RGB rgb) {
		if(rgb != null){
			Color3f color = new Color3f( ((float)rgb.red / 255.0f), ((float)rgb.green / 255.0f), ((float)rgb.blue / 255.0f));
			return color;
		}else{
			return getNullObj();
		}
	}

	@Override
	protected Object getNullObj() {
		return new Color3f(-1, -1, -1);
	}
	
	@Override
	protected Object getValue() {
		try {
			Object obj = getModelValue().getValue();

			if (obj instanceof Color3f) {
				Color3f color = (Color3f) obj;

				if (color.getX() >= 0 && color.getY() >= 0 && color.getZ() >= 0) {
					return color;
				}
			}
			return null;
		} catch (DatabindingFailedException e) {
			Logger.INSTANCE.log(Activator.ID, " error setting the RGB value. ", EventSeverity.ERROR);
		}
		return null;
	}

	

}
