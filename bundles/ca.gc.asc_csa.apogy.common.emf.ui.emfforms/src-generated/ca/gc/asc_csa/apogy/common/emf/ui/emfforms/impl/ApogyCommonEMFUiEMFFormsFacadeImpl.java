/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.ui.emfforms.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecp.ui.view.swt.ECPSWTViewRenderer;
import org.eclipse.emf.ecp.view.spi.model.VControl;
import org.eclipse.emf.ecp.view.spi.model.VFeaturePathDomainModelReference;
import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.emf.ecp.view.spi.model.VViewFactory;
import org.eclipse.emf.ecp.view.spi.provider.ViewProviderHelper;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.Activator;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsPackage;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.ui.composites.NoContentComposite;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Facade</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ApogyCommonEMFUiEMFFormsFacadeImpl extends MinimalEObjectImpl.Container implements ApogyCommonEMFUiEMFFormsFacade 
{
	private enum PropertyType	
	{
		NONE, READONLY, EDITABLE
	}
		
	private static ApogyCommonEMFUiEMFFormsFacade instance = null;

	public static ApogyCommonEMFUiEMFFormsFacade getInstance() {
		if (instance == null) {
			instance = new ApogyCommonEMFUiEMFFormsFacadeImpl();
		}
		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApogyCommonEMFUiEMFFormsFacadeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFUiEMFFormsPackage.Literals.APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void createEMFForms(Composite parent, EObject eObject) {
		createEMFForms(parent, eObject, false);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void createEMFForms(Composite parent, EObject eObject, String message) {
		createEMFForms(parent, eObject, null, message);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void createEMFForms(Composite parent, EObject eObject, boolean readOnly) 
	{
		if(eObject == null)
		{
			createEMFForms(parent, eObject, null, "Object null");	
		}
		else
		{
			VView viewModel = ViewProviderHelper.getView(eObject, null);			
			// VView viewModel = createDefaultViewModel(eObject);
			if (readOnly) {
				viewModel.setAllContentsReadOnly();
			}
			createEMFForms(parent, eObject, viewModel);	
		}		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public void createEMFForms(Composite parent, EObject eObject, VView viewModel) {
		createEMFForms(parent, eObject, viewModel, "Object null");
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void createEMFForms(Composite parent, EObject eObject, VView viewModel, String message) 
	{				
		// Dispose of the children.
		for (Control control : parent.getChildren()) {
			control.dispose();
		}

		// If the EObject is null, display the error message.
		if (eObject == null) {
			parent.setLayout(new FillLayout());
			new NoContentComposite(parent, SWT.None) {
				@Override
				protected String getMessage() {
					return message;
				}
			};
		} else {
			// If the parent has the wrong layout, change it to GridLayout.
			if (!(parent.getLayout() instanceof GridLayout)) {
				parent.setLayout(GridLayoutFactory.fillDefaults().margins(10, 10).create());
			}

			// Get the viewModel
			if (viewModel == null) 
			{
				viewModel = ViewProviderHelper.getView(eObject, null);
				// viewModel = createDefaultViewModel(eObject);
			}
					
			try 
			{
				// Render
				ECPSWTViewRenderer.INSTANCE.render(parent, eObject, viewModel);

				// If there is no controls in the EMFForms, dispose it and
				// display an error message.
				Composite composite = (Composite) parent.getChildren()[0];
				if (composite.getChildren().length == 0) {
					composite.dispose();
					Composite errorComposite = new NoContentComposite(parent, SWT.None) {
						@Override
						protected String getMessage() {
							return "Selected object has no editable feature";
						}
					};
					errorComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
				}
			} 
			catch (Exception e) 
			{
				String messageInfo = this.getClass().getSimpleName() + ".setCompositeContents(): "
						+ "Error while opening EMF Forms";
				Logger.INSTANCE.log(Activator.ID, this, messageInfo, EventSeverity.ERROR);
				e.printStackTrace();
			}
		}
		parent.layout();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public VView createDefaultViewModel(EObject eObject) 
	{
		EClass eClass = eObject.eClass();
		
		VView vView = VViewFactory.eINSTANCE.createView();
		vView.setRootEClass(eClass);
		vView.setVisible(true);
		
		List<VControl> vControls = new ArrayList<>();
		
		// Adds all attributes			
		for(EAttribute attribute : eClass.getEAllAttributes())
		{
			VControl vControl = createVControl(attribute);
			if(vControl != null) vControls.add(vControl);
		}
		
		// Adds all References		
		for(EReference eReference : eClass.getEAllReferences())
		{
			VControl vControl = createVControl(eReference);
			if(vControl != null) vControls.add(vControl);
		}
		
		// Sort VControl in alphabetical order and add them to view model.
		SortedSet<VControl> sortedVControls = sortVControlAlphabetically(vControls);		
		vView.getChildren().addAll(sortedVControls);
				
		return vView;
	}
	
	/**
	 * Sorts a list of VControl alphabetically.
	 * @param attributes The list of VControl. 
	 * @return The VControl sorted alphabetically.
	 */
	private SortedSet<VControl> sortVControlAlphabetically(List<VControl> vcontrols)
	{
		TreeSet<VControl> treeSet = new TreeSet<VControl>(new Comparator<VControl>() 
		{
			@Override
			public int compare(VControl arg0, VControl arg1) 
			{
				return arg0.getLabel().compareTo(arg1.getLabel());
			}
		});
						
		treeSet.addAll(vcontrols);
		
		return treeSet;
	}
	
	/**
	 * Creates a VControl for a given attribute.
	 * @param attribute The attribute
	 * @return The VCOntrol, null if the attribute GenModel property is NONE.
	 */
	private VControl createVControl(EAttribute attribute)
	{
		VControl vControl = null;		
		String property = getAnnotationDetail(attribute, "property");		
		PropertyType properType = getPropertyType(property);
		switch (properType) 
		{
			case READONLY:
			{
				vControl = VViewFactory.eINSTANCE.createControl();
				vControl.setLabel(attribute.getName());							
				vControl.setReadonly(true);
				
				VFeaturePathDomainModelReference ref1 = VViewFactory.eINSTANCE.createFeaturePathDomainModelReference();
				ref1.setDomainModelEFeature(attribute);
				vControl.setDomainModelReference(ref1);
			}
			break;
			
			case EDITABLE:
			{
				vControl = VViewFactory.eINSTANCE.createControl();
				vControl.setLabel(attribute.getName());

		
				VFeaturePathDomainModelReference ref1 = VViewFactory.eINSTANCE.createFeaturePathDomainModelReference();
				ref1.setDomainModelEFeature(attribute);
				vControl.setDomainModelReference(ref1);
			}
			break;

			default:
			break;
		}		
		
		return vControl;
	}
	
	/**
	 * Creates a VControl for a given reference.
	 * @param eReference The reference
	 * @return The VCOntrol, null if the reference GenModel property is NONE.
	 */
	private VControl createVControl(EReference eReference)
	{
		VControl vControl = null;	
		String property = getAnnotationDetail(eReference, "property");	
		PropertyType properType = getPropertyType(property);
		switch (properType) 
		{
			case READONLY:
			{
				vControl = VViewFactory.eINSTANCE.createControl();
				vControl.setLabel(eReference.getName());							
				vControl.setReadonly(true);
				
				VFeaturePathDomainModelReference ref1 = VViewFactory.eINSTANCE.createFeaturePathDomainModelReference();
				ref1.setDomainModelEFeature(eReference);
				vControl.setDomainModelReference(ref1);
			}
			break;
			
			case EDITABLE:
			{
				vControl = VViewFactory.eINSTANCE.createControl();
				vControl.setLabel(eReference.getName());
		
				VFeaturePathDomainModelReference ref1 = VViewFactory.eINSTANCE.createFeaturePathDomainModelReference();
				ref1.setDomainModelEFeature(eReference);
				vControl.setDomainModelReference(ref1);
			}
			break;

			default:
			break;
		}		
		
		return vControl;
	}
	
	/**
	 * Extracts the GenModel property type from a property string. 
	 * @param propertyString The property string.
	 * @return The property type. If propertyString is null EDITABLE is returned. If the property string is empty, NONE is returned. 
	 */
	private PropertyType getPropertyType(String propertyString)
	{
		if(propertyString == null)
		{
			return PropertyType.EDITABLE;					
		}
		else if(propertyString.contains("Editable"))
		{
			return PropertyType.EDITABLE;	
		}
		else if(propertyString.contains("None"))
		{
			return PropertyType.NONE;
		}
		else if(propertyString.contains("Readonly"))
		{
			return PropertyType.READONLY;
		}
		return PropertyType.NONE;
	}
	
	/**
	 * Returns the string value found in the EAnnotation GenModel for a given
	 * ETypedElement and key.
	 * 
	 * @param eTypedElement
	 *            The eTypedElement.
	 * @param key
	 *            The key of the details in the annotation.
	 * @return The value string found, or null if none could be extracted.
	 */
	protected String getAnnotationDetail(ETypedElement eTypedElement, String key) 
	{
		EAnnotation annotation = eTypedElement.getEAnnotation("http://www.eclipse.org/emf/2002/GenModel");
		if (annotation != null) {
			EMap<String, String> map = annotation.getDetails();
			if (map != null)
				return map.get(key);
		}
		return null;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFUiEMFFormsPackage.APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT:
				createEMFForms((Composite)arguments.get(0), (EObject)arguments.get(1));
				return null;
			case ApogyCommonEMFUiEMFFormsPackage.APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_STRING:
				createEMFForms((Composite)arguments.get(0), (EObject)arguments.get(1), (String)arguments.get(2));
				return null;
			case ApogyCommonEMFUiEMFFormsPackage.APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_BOOLEAN:
				createEMFForms((Composite)arguments.get(0), (EObject)arguments.get(1), (Boolean)arguments.get(2));
				return null;
			case ApogyCommonEMFUiEMFFormsPackage.APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_VVIEW:
				createEMFForms((Composite)arguments.get(0), (EObject)arguments.get(1), (VView)arguments.get(2));
				return null;
			case ApogyCommonEMFUiEMFFormsPackage.APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_EMF_FORMS__COMPOSITE_EOBJECT_VVIEW_STRING:
				createEMFForms((Composite)arguments.get(0), (EObject)arguments.get(1), (VView)arguments.get(2), (String)arguments.get(3));
				return null;
			case ApogyCommonEMFUiEMFFormsPackage.APOGY_COMMON_EMF_UI_EMF_FORMS_FACADE___CREATE_DEFAULT_VIEW_MODEL__EOBJECT:
				return createDefaultViewModel((EObject)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ApogyCommonEMFUiEMFFormsFacadeImpl
