/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.core.environment.surface.impl;

import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.core.environment.surface.AbstractLineOfSightImageMapLayer;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentFactory;
import ca.gc.asc_csa.apogy.core.environment.surface.ApogySurfaceEnvironmentPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Line Of Sight Image Map Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.impl.AbstractLineOfSightImageMapLayerImpl#getTargetHeightAboveGround <em>Target Height Above Ground</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.impl.AbstractLineOfSightImageMapLayerImpl#isUseHeightPerpendicularToGround <em>Use Height Perpendicular To Ground</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.impl.AbstractLineOfSightImageMapLayerImpl#getLineOfSightAvailableColor <em>Line Of Sight Available Color</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.core.environment.surface.impl.AbstractLineOfSightImageMapLayerImpl#getLineOfSightNotAvailableColor <em>Line Of Sight Not Available Color</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AbstractLineOfSightImageMapLayerImpl extends CartesianTriangularMeshDerivedImageMapLayerImpl implements AbstractLineOfSightImageMapLayer 
{
	public static final short NO_MESH_PROJECTION = 0;
	public static final short NO_LINE_OF_SIGHT = 1;
	public static final short LINE_OF_SIGHT = 2;

	protected Point3d[][] pixelsIntersectionPoints;
	protected Vector3d[][] pixelNormals;
	protected short[][] lineOfSights;

	
	/**
	 * The default value of the '{@link #getTargetHeightAboveGround() <em>Target Height Above Ground</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetHeightAboveGround()
	 * @generated
	 * @ordered
	 */
	protected static final double TARGET_HEIGHT_ABOVE_GROUND_EDEFAULT = 1.0;

	/**
	 * The cached value of the '{@link #getTargetHeightAboveGround() <em>Target Height Above Ground</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetHeightAboveGround()
	 * @generated
	 * @ordered
	 */
	protected double targetHeightAboveGround = TARGET_HEIGHT_ABOVE_GROUND_EDEFAULT;

	/**
	 * The default value of the '{@link #isUseHeightPerpendicularToGround() <em>Use Height Perpendicular To Ground</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseHeightPerpendicularToGround()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USE_HEIGHT_PERPENDICULAR_TO_GROUND_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUseHeightPerpendicularToGround() <em>Use Height Perpendicular To Ground</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseHeightPerpendicularToGround()
	 * @generated
	 * @ordered
	 */
	protected boolean useHeightPerpendicularToGround = USE_HEIGHT_PERPENDICULAR_TO_GROUND_EDEFAULT;

	/**
	 * The default value of the '{@link #getLineOfSightAvailableColor() <em>Line Of Sight Available Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineOfSightAvailableColor()
	 * @generated
	 * @ordered
	 */
	protected static final Color3f LINE_OF_SIGHT_AVAILABLE_COLOR_EDEFAULT = (Color3f)ApogySurfaceEnvironmentFactory.eINSTANCE.createFromString(ApogySurfaceEnvironmentPackage.eINSTANCE.getColor3f(), "0.0,1.0,0.0");
	/**
	 * The cached value of the '{@link #getLineOfSightAvailableColor() <em>Line Of Sight Available Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineOfSightAvailableColor()
	 * @generated
	 * @ordered
	 */
	protected Color3f lineOfSightAvailableColor = LINE_OF_SIGHT_AVAILABLE_COLOR_EDEFAULT;
	/**
	 * The default value of the '{@link #getLineOfSightNotAvailableColor() <em>Line Of Sight Not Available Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineOfSightNotAvailableColor()
	 * @generated
	 * @ordered
	 */
	protected static final Color3f LINE_OF_SIGHT_NOT_AVAILABLE_COLOR_EDEFAULT = (Color3f)ApogySurfaceEnvironmentFactory.eINSTANCE.createFromString(ApogySurfaceEnvironmentPackage.eINSTANCE.getColor3f(), "1.0,0.0,0.0");
	/**
	 * The cached value of the '{@link #getLineOfSightNotAvailableColor() <em>Line Of Sight Not Available Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLineOfSightNotAvailableColor()
	 * @generated
	 * @ordered
	 */
	protected Color3f lineOfSightNotAvailableColor = LINE_OF_SIGHT_NOT_AVAILABLE_COLOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractLineOfSightImageMapLayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogySurfaceEnvironmentPackage.Literals.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTargetHeightAboveGround() {
		return targetHeightAboveGround;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetHeightAboveGround(double newTargetHeightAboveGround) {
		double oldTargetHeightAboveGround = targetHeightAboveGround;
		targetHeightAboveGround = newTargetHeightAboveGround;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__TARGET_HEIGHT_ABOVE_GROUND, oldTargetHeightAboveGround, targetHeightAboveGround));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isUseHeightPerpendicularToGround() {
		return useHeightPerpendicularToGround;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseHeightPerpendicularToGround(boolean newUseHeightPerpendicularToGround) {
		boolean oldUseHeightPerpendicularToGround = useHeightPerpendicularToGround;
		useHeightPerpendicularToGround = newUseHeightPerpendicularToGround;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__USE_HEIGHT_PERPENDICULAR_TO_GROUND, oldUseHeightPerpendicularToGround, useHeightPerpendicularToGround));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Color3f getLineOfSightAvailableColor() {
		return lineOfSightAvailableColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLineOfSightAvailableColor(Color3f newLineOfSightAvailableColor) {
		Color3f oldLineOfSightAvailableColor = lineOfSightAvailableColor;
		lineOfSightAvailableColor = newLineOfSightAvailableColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_AVAILABLE_COLOR, oldLineOfSightAvailableColor, lineOfSightAvailableColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Color3f getLineOfSightNotAvailableColor() {
		return lineOfSightNotAvailableColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLineOfSightNotAvailableColor(Color3f newLineOfSightNotAvailableColor) {
		Color3f oldLineOfSightNotAvailableColor = lineOfSightNotAvailableColor;
		lineOfSightNotAvailableColor = newLineOfSightNotAvailableColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_NOT_AVAILABLE_COLOR, oldLineOfSightNotAvailableColor, lineOfSightNotAvailableColor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__TARGET_HEIGHT_ABOVE_GROUND:
				return getTargetHeightAboveGround();
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__USE_HEIGHT_PERPENDICULAR_TO_GROUND:
				return isUseHeightPerpendicularToGround();
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_AVAILABLE_COLOR:
				return getLineOfSightAvailableColor();
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_NOT_AVAILABLE_COLOR:
				return getLineOfSightNotAvailableColor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__TARGET_HEIGHT_ABOVE_GROUND:
				setTargetHeightAboveGround((Double)newValue);
				return;
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__USE_HEIGHT_PERPENDICULAR_TO_GROUND:
				setUseHeightPerpendicularToGround((Boolean)newValue);
				return;
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_AVAILABLE_COLOR:
				setLineOfSightAvailableColor((Color3f)newValue);
				return;
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_NOT_AVAILABLE_COLOR:
				setLineOfSightNotAvailableColor((Color3f)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__TARGET_HEIGHT_ABOVE_GROUND:
				setTargetHeightAboveGround(TARGET_HEIGHT_ABOVE_GROUND_EDEFAULT);
				return;
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__USE_HEIGHT_PERPENDICULAR_TO_GROUND:
				setUseHeightPerpendicularToGround(USE_HEIGHT_PERPENDICULAR_TO_GROUND_EDEFAULT);
				return;
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_AVAILABLE_COLOR:
				setLineOfSightAvailableColor(LINE_OF_SIGHT_AVAILABLE_COLOR_EDEFAULT);
				return;
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_NOT_AVAILABLE_COLOR:
				setLineOfSightNotAvailableColor(LINE_OF_SIGHT_NOT_AVAILABLE_COLOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__TARGET_HEIGHT_ABOVE_GROUND:
				return targetHeightAboveGround != TARGET_HEIGHT_ABOVE_GROUND_EDEFAULT;
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__USE_HEIGHT_PERPENDICULAR_TO_GROUND:
				return useHeightPerpendicularToGround != USE_HEIGHT_PERPENDICULAR_TO_GROUND_EDEFAULT;
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_AVAILABLE_COLOR:
				return LINE_OF_SIGHT_AVAILABLE_COLOR_EDEFAULT == null ? lineOfSightAvailableColor != null : !LINE_OF_SIGHT_AVAILABLE_COLOR_EDEFAULT.equals(lineOfSightAvailableColor);
			case ApogySurfaceEnvironmentPackage.ABSTRACT_LINE_OF_SIGHT_IMAGE_MAP_LAYER__LINE_OF_SIGHT_NOT_AVAILABLE_COLOR:
				return LINE_OF_SIGHT_NOT_AVAILABLE_COLOR_EDEFAULT == null ? lineOfSightNotAvailableColor != null : !LINE_OF_SIGHT_NOT_AVAILABLE_COLOR_EDEFAULT.equals(lineOfSightNotAvailableColor);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (targetHeightAboveGround: ");
		result.append(targetHeightAboveGround);
		result.append(", useHeightPerpendicularToGround: ");
		result.append(useHeightPerpendicularToGround);
		result.append(", lineOfSightAvailableColor: ");
		result.append(lineOfSightAvailableColor);
		result.append(", lineOfSightNotAvailableColor: ");
		result.append(lineOfSightNotAvailableColor);
		result.append(')');
		return result.toString();
	}

	/**
	 * Returns an array of int representing the color for each lineOfsight value
	 * @param lineOfSights The array of line of sight flags.
	 * @return the array containing the color associated with the line of sight status.
	 */
	abstract protected int[][] getPixelsColor(short[][] lineOfSights);
} //AbstractLineOfSightImageMapLayerImpl
