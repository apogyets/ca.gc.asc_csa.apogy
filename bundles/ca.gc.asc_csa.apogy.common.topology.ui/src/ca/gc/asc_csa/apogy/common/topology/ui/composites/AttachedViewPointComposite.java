package ca.gc.asc_csa.apogy.common.topology.ui.composites;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.AttachedViewPoint;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class AttachedViewPointComposite extends Composite 
{
	private AttachedViewPoint attachedViewPoint;
	
	private AbstractViewPointComposite abstractViewPointComposite;
	
	private Button enableRotationButton;
	private Button enableTranslationButton;
	
	private DataBindingContext m_bindingContext;
	
	public AttachedViewPointComposite(Composite parent, int style) 
	{
		super(parent, style);	
		setLayout(new GridLayout(1, false) );
		
		abstractViewPointComposite = new AbstractViewPointComposite(this, SWT.None, null);
		abstractViewPointComposite.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		
		Composite bottom = new Composite(this, SWT.BORDER);
		bottom.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false, 1, 1));
		bottom.setLayout(new GridLayout(4,false));
		
		// Rotation Enable.
		Label lblEnableRotationLabel = new Label(bottom, SWT.NONE);
		lblEnableRotationLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEnableRotationLabel.setText("Enable Rotation:");
		
		enableRotationButton = new Button(bottom, SWT.CHECK);
		
		// Translation Enable.
		Label lblEnableTranslationLabel = new Label(bottom, SWT.NONE);
		lblEnableTranslationLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblEnableTranslationLabel.setText("Enable Translation:");
		
		enableTranslationButton = new Button(bottom, SWT.CHECK);
		
		// Adds dispose cleanup.
		this.addDisposeListener(new DisposeListener() 
		{		
			@Override
			public void widgetDisposed(DisposeEvent e) 
			{				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}

	public AttachedViewPoint getAttachedViewPoint() {
		return attachedViewPoint;
	}

	public void setAttachedViewPoint(AttachedViewPoint attachedViewPoint) 
	{
		if(m_bindingContext != null) m_bindingContext.dispose();
		
		this.attachedViewPoint = attachedViewPoint;
		
		if(attachedViewPoint != null)
		{
			m_bindingContext = initDataBindingsCustom();
		}
			
	}
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();						
		
		/* Enable Rotation.*/
		IObservableValue<Double> observeAllowRotation = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(attachedViewPoint), 
				  FeaturePath.fromList(ApogyCommonTopologyPackage.Literals.ATTACHED_VIEW_POINT__ALLOW_ROTATION)).observe(attachedViewPoint);
		
		IObservableValue<String> observeEnableRotationButton = WidgetProperties.selection().observe(enableRotationButton);
		
		bindingContext.bindValue(observeEnableRotationButton,
									observeAllowRotation, 
									new UpdateValueStrategy(),	
									new UpdateValueStrategy());		
		
		/* Enable Translation.*/
		IObservableValue<Double> observeAllowTranslation = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(attachedViewPoint), 
				  FeaturePath.fromList(ApogyCommonTopologyPackage.Literals.ATTACHED_VIEW_POINT__ALLOW_TRANSLATION)).observe(attachedViewPoint);
		
		IObservableValue<String> observeEnableTranslationButton = WidgetProperties.selection().observe(enableTranslationButton);
		
		bindingContext.bindValue(observeEnableTranslationButton,
								observeAllowTranslation, 
								new UpdateValueStrategy(),	
								new UpdateValueStrategy());		
		
		return bindingContext;
	}
	
}
