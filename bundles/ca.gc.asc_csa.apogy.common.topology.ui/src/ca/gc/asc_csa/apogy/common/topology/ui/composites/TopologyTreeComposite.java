package ca.gc.asc_csa.apogy.common.topology.ui.composites;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.composites.EObjectComposite;
import ca.gc.asc_csa.apogy.common.emf.ui.wizards.ApogyEObjectWizard;
import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;
import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.GroupNode;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.Activator;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;


public class TopologyTreeComposite extends Composite 
{	
	private Node root;	
	private EObjectComposite eObjectComposite;
	private Button btnNew;
	private Button btnDelete;
	private boolean editingEnabled = true;
		
	public TopologyTreeComposite(Composite parent, int style, boolean editingEnabled) 
	{
		super(parent, style);	
		this.editingEnabled = editingEnabled;
		setLayout(new GridLayout(2, false));
		
		eObjectComposite = new EObjectComposite(this, SWT.None) {
			@Override
			protected void newSelection(ISelection selection) 
			{				
				checkEnableNewButton(getSelectedEObject());
				
				if(getSelectedEObject() instanceof Node)
				{
					TopologyTreeComposite.this.newSelection((Node) getSelectedNode());
				}
			}
		};
		eObjectComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 3));
		
		if(editingEnabled)
		{
			// Buttons.
			Composite composite = new Composite(this, SWT.NONE);
			composite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
			composite.setLayout(new GridLayout(1, false));	
			
			btnNew = new Button(composite, SWT.NONE);
			btnNew.setSize(74, 29);
			btnNew.setText("New");
			btnNew.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnNew.setEnabled(false);
			btnNew.addListener(SWT.Selection, new Listener() 
			{		
				@Override
				public void handleEvent(Event event) 
				{				
					if (event.type == SWT.Selection) 
					{	
						Node parent = getSelectedNode();
														
						if(parent != null)
						{
							ApogyEObjectWizard wizard = new ApogyEObjectWizard(ApogyCommonTopologyPackage.Literals.GROUP_NODE__CHILDREN, parent, null, ApogyCommonTopologyPackage.Literals.NODE); 
							WizardDialog dialog = new WizardDialog(getShell(), wizard);
							dialog.open();	
							
							EObject createdEObject = wizard.getCreatedEObject();
							if(createdEObject != null)
							{
								eObjectComposite.setSelectedEObject(createdEObject);
							}
						}
					}
				}
			});
					
			btnDelete = new Button(composite, SWT.NONE);
			btnDelete.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
			btnDelete.setSize(74, 29);
			btnDelete.setText("Delete");
			btnDelete.setEnabled(false);
			btnDelete.addSelectionListener(new SelectionAdapter() 
			{
				@Override
				public void widgetSelected(SelectionEvent event) 
				{
					Node node = getSelectedNode();
					
					if(node != null)
					{
						GroupNode parent = (GroupNode) node.getParent();
						
						if(ApogyCommonTransactionFacade.INSTANCE.areEditingDomainsValid(node.eContainer(), ApogyCommonTopologyPackage.Literals.GROUP_NODE__CHILDREN, node, false) == ApogyCommonTransactionFacade.EXECUTE_COMMAND_ON_OWNER_DOMAIN)
						{
							try 
							{
								ApogyCommonTransactionFacade.INSTANCE.basicRemove(node.eContainer(), ApogyCommonTopologyPackage.Literals.GROUP_NODE__CHILDREN, node);
							} 
							catch (Exception e)	
							{
								Logger.INSTANCE.log(Activator.ID, "Unable to delete the Node <"+ node.getNodeId() + ">", EventSeverity.ERROR, e);
							}
						}
						else
						{		
							try 
							{
								ApogyCommonTransactionFacade.INSTANCE.basicRemove(node.eContainer(), ApogyCommonTopologyPackage.Literals.GROUP_NODE__CHILDREN, node, true);
							} 
							catch (Exception e)	
							{
								Logger.INSTANCE.log(Activator.ID,
										"Unable to delete the Node <"+ node.getNodeId() + ">",
										EventSeverity.ERROR, e);
							}									
						}
						
						// Have the viewer select the parent of the deleted object.
						eObjectComposite.setSelectedEObject(parent);
					}
					
					// Forces the viewer to update.
					eObjectComposite.setEObject(getRoot());
				}
			});
		}
	}	
	
	public Node getRoot() {
		return root;
	}

	public void setRoot(Node root) 
	{
		this.root = root;
		
		if(eObjectComposite != null && !eObjectComposite.isDisposed())
		{
			eObjectComposite.setEObject(root);
		}
	}
	
	public void selectNode(Node selectedNode)
	{
		if(eObjectComposite != null && !eObjectComposite.isDisposed())
		{
			eObjectComposite.setSelectedEObject(selectedNode);
		}
		newSelection(selectedNode);
	}
	
	public Node getSelectedNode()
	{
		if(eObjectComposite.getSelectedEObject() instanceof Node)
		{
			return (Node) eObjectComposite.getSelectedEObject();	
		}
		else
		{
			return null;
		}		
	}
	
	protected void newSelection(Node selectedNode) 
	{
	}

	private void checkEnableNewButton(EObject eObject) 
	{
		if(this.editingEnabled)
		{
			if (eObject != null)			 
			{
				if(eObject instanceof GroupNode)
				{
					btnNew.setEnabled(!ApogyCommonEMFFacade.INSTANCE.getSettableEReferences(eObject).isEmpty());
				}
				
				btnDelete.setEnabled(true);
			} 
			else 
			{
				btnNew.setEnabled(false);
				btnDelete.setEnabled(false);
			}
		}
	}
}
