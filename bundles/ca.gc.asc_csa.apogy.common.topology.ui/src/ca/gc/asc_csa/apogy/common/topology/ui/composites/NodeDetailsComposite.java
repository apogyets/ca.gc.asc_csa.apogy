package ca.gc.asc_csa.apogy.common.topology.ui.composites;

import org.eclipse.emf.ecp.view.spi.model.VView;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.emf.ui.emfforms.ApogyCommonEMFUiEMFFormsFacade;
import ca.gc.asc_csa.apogy.common.topology.Node;

public class NodeDetailsComposite extends Composite 
{
	private Node node;
	
	private Composite compositeDetails;
		
	public NodeDetailsComposite(Composite parent, int style) 
	{
		super(parent, style);
		setLayout(new GridLayout(1,false));
		
		compositeDetails = new Composite(this, SWT.NONE);		
		GridData gd_compositeDetails = new GridData(SWT.FILL, SWT.FILL, false, true, 1, 1);
		gd_compositeDetails.widthHint = 398;
		gd_compositeDetails.minimumWidth = 398;
		compositeDetails.setLayoutData(gd_compositeDetails);
		compositeDetails.setLayout(new GridLayout(1, false));	
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node newNode) 
	{
		this.node = newNode;
		// Update Details EMFForm.
		if(newNode != null)
		{
			VView viewModel = ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createDefaultViewModel(newNode);
			ApogyCommonEMFUiEMFFormsFacade.INSTANCE.createEMFForms(compositeDetails, newNode, viewModel);
		}
	}		
}
