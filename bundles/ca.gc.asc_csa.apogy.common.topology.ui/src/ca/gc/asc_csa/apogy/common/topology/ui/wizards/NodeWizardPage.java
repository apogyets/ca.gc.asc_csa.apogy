package ca.gc.asc_csa.apogy.common.topology.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.databinding.FeaturePath;
import org.eclipse.emf.databinding.edit.EMFEditProperties;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ca.gc.asc_csa.apogy.common.topology.ApogyCommonTopologyPackage;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class NodeWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.ui.wizards.NodeWizardPage";
	
	private Text nodeIDText;
	private Text descriptionText;	
	private Node node;
		
	private DataBindingContext m_bindingContext;
		
	
	public NodeWizardPage(Node node) 
	{
		super(WIZARD_PAGE_ID);
		this.node = node;
				
		setTitle("Node");
		setDescription("Sets the Node ID and description.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		
		Composite settingComposite = new Composite(top, SWT.None);
		settingComposite.setLayout(new GridLayout(2, false));
		settingComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
						
		Label lblNodeId = new Label(settingComposite, SWT.NONE);
		lblNodeId.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNodeId.setText("Node ID:");
		
		nodeIDText = new Text(settingComposite, SWT.BORDER);
		nodeIDText.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		nodeIDText.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				validate();
			}
		});
		
		Label lblDescription = new Label(settingComposite, SWT.NONE);
		lblDescription.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblDescription.setText("Description:");
		
		descriptionText = new Text(settingComposite, SWT.BORDER | SWT.WRAP | SWT.MULTI);
		descriptionText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		descriptionText.addKeyListener(new KeyListener() 
		{
			
			@Override
			public void keyReleased(KeyEvent e) 
			{
				validate();
			}
			
			@Override
			public void keyPressed(KeyEvent e) 
			{	
				validate();
			}
		});
							
		setControl(top);
		
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}			
		
	protected void validate()
	{
		setErrorMessage(null);
		
		if(node.getNodeId() == null || node.getNodeId().length() == 0)
		{
			setErrorMessage("The specified Node ID is invalid!");
		}
		
		setPageComplete(getErrorMessage() == null);
	}
	
	@SuppressWarnings("unchecked")
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
		
		/* Image Width Value. */
		IObservableValue<Double> observeImageWidthResolution = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(node), 
																	  FeaturePath.fromList(ApogyCommonTopologyPackage.Literals.NODE__NODE_ID)).observe(node);
		IObservableValue<String> observeImageWidthText = WidgetProperties.text(SWT.Modify).observe(nodeIDText);

		bindingContext.bindValue(observeImageWidthText,
								 observeImageWidthResolution, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		/* Image height Value. */
		IObservableValue<Double> observeImageHeight = EMFEditProperties.value(ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(node), 
																	  FeaturePath.fromList(ApogyCommonTopologyPackage.Literals.NODE__DESCRIPTION)).observe(node);
		IObservableValue<String> observeImageHeightText = WidgetProperties.text(SWT.Modify).observe(descriptionText);

		bindingContext.bindValue(observeImageHeightText,
								 observeImageHeight, 
								 new UpdateValueStrategy(), 
								 new UpdateValueStrategy());
		
		return bindingContext;
	}
}
