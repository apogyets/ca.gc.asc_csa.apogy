package ca.gc.asc_csa.apogy.common.topology.ui.converters;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import ca.gc.asc_csa.apogy.common.converters.IConverter;
import ca.gc.asc_csa.apogy.common.topology.Node;
import ca.gc.asc_csa.apogy.common.topology.ui.Activator;
import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;

public class NodeToNodePresentationConverter implements IConverter 
{

	@Override
	public boolean canConvert(Object arg0) 
	{
		if(arg0 instanceof Node)
		{
			Node node = (Node) arg0;
			return Activator.getTopologyPresentationRegistry().getPresentationNode(node) != null;
		}
		return false;
	}

	@Override
	public Object convert(Object arg0) throws Exception 
	{	
		Node node = (Node) arg0;
		return Activator.getTopologyPresentationRegistry().getPresentationNode(node);
	}

	@Override
	public Class<?> getInputType() 
	{		
		return Node.class;
	}

	@Override
	public Class<?> getOutputType() 
	{		
		return NodePresentation.class;
	}

}
