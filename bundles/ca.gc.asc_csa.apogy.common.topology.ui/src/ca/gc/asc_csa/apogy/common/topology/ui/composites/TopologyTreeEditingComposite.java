package ca.gc.asc_csa.apogy.common.topology.ui.composites;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import ca.gc.asc_csa.apogy.common.topology.Node;

public class TopologyTreeEditingComposite extends Composite 
{
	protected Node root;
	
	protected TopologyTreeComposite topologyTreeComposite;
	protected NodeOverviewComposite nodeOverviewComposite;
	protected NodeDetailsComposite nodeDetailsComposite;

	protected final FormToolkit formToolkit = new FormToolkit(Display.getDefault());
	
	public TopologyTreeEditingComposite(Composite parent, int style, boolean editingEnabled) 
	{
		super(parent, style);
		setLayout(new GridLayout(1, true));
		
		ScrolledForm scrldfrmDEM = formToolkit.createScrolledForm(this);
		scrldfrmDEM.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		formToolkit.paintBordersFor(scrldfrmDEM);	
		{
			TableWrapLayout tableWrapLayout = new TableWrapLayout();
			tableWrapLayout.numColumns = 2;
			tableWrapLayout.makeColumnsEqualWidth = true;
			scrldfrmDEM.getBody().setLayout(tableWrapLayout);
		}
		
		Section sctnTopology = formToolkit.createSection(scrldfrmDEM.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		TableWrapData twd_sctnDemlist = new TableWrapData(TableWrapData.FILL, TableWrapData.TOP, 3, 1);
		twd_sctnDemlist.valign = TableWrapData.FILL;
		twd_sctnDemlist.grabVertical = true;
		twd_sctnDemlist.grabHorizontal = true;
		sctnTopology.setLayoutData(twd_sctnDemlist);
		formToolkit.paintBordersFor(sctnTopology);
		sctnTopology.setText("Topology");
		
		topologyTreeComposite = new TopologyTreeComposite(sctnTopology, SWT.NONE, editingEnabled)
		{
			@Override
			protected void newSelection(Node node) 
			{
				nodeOverviewComposite.setNode(node);
				nodeDetailsComposite.setNode(node);		
				nodeSelected(node);
			}
		};		
			
		GridData gd_topologyTreeComposite = new GridData(SWT.FILL, SWT.FILL, true, true);
		topologyTreeComposite.setLayoutData(gd_topologyTreeComposite);
		
		formToolkit.adapt(topologyTreeComposite);
		formToolkit.paintBordersFor(topologyTreeComposite);
		sctnTopology.setClient(topologyTreeComposite);
		
		
		Section sctnDemoverview = formToolkit.createSection(scrldfrmDEM.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnDemoverview);
		sctnDemoverview.setText("Node Overview");
		
		nodeOverviewComposite = new NodeOverviewComposite(sctnDemoverview, SWT.NONE);
		formToolkit.adapt(nodeOverviewComposite);
		formToolkit.paintBordersFor(nodeOverviewComposite);
		sctnDemoverview.setClient(nodeOverviewComposite);				
				
		Section sctnDemdetails = formToolkit.createSection(scrldfrmDEM.getBody(), Section.EXPANDED | Section.TWISTIE | Section.TITLE_BAR);
		formToolkit.paintBordersFor(sctnDemdetails);
		sctnDemdetails.setText("Node Details");
		
		nodeDetailsComposite = new NodeDetailsComposite(sctnDemdetails, SWT.NONE);
		formToolkit.adapt(nodeDetailsComposite);
		formToolkit.paintBordersFor(nodeDetailsComposite);
		sctnDemdetails.setClient(nodeDetailsComposite);		
		new Label(this, SWT.NONE);
	}

	public void selectNode(Node selectedNode)
	{
		if(topologyTreeComposite != null && !topologyTreeComposite.isDisposed())
		{
			topologyTreeComposite.selectNode(selectedNode);
		}
		
		nodeSelected(selectedNode);
	}
	
	public Node getRoot() 
	{
		return root;
	}

	public void setRoot(Node newNode) 
	{		
		this.root = newNode;
		
		if(topologyTreeComposite != null && !topologyTreeComposite.isDisposed()) topologyTreeComposite.setRoot(newNode);
		if(nodeDetailsComposite != null && !nodeDetailsComposite.isDisposed()) nodeDetailsComposite.setNode(newNode);			
	}
	
	protected void nodeSelected(Node node)
	{		
	}
}
