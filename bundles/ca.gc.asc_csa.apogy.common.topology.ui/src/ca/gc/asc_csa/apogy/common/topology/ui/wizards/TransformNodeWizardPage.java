package ca.gc.asc_csa.apogy.common.topology.ui.wizards;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import ca.gc.asc_csa.apogy.common.math.ui.composites.RotationMatrixComposite;
import ca.gc.asc_csa.apogy.common.math.ui.composites.Tuple3dComposite;
import ca.gc.asc_csa.apogy.common.topology.TransformNode;
import ca.gc.asc_csa.apogy.common.transaction.ApogyCommonTransactionFacade;

public class TransformNodeWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.ui.wizards.TransformNodeWizardPage";
		
	private TransformNode transformNode;
	
	private Tuple3dComposite positionComposite;
	private RotationMatrixComposite rotationComposite;
	
	private DataBindingContext m_bindingContext;
		
	
	public TransformNodeWizardPage(TransformNode transformNode) 
	{
		super(WIZARD_PAGE_ID);
		this.transformNode = transformNode;
				
		setTitle("Rotation Node");
		setDescription("Sets the Node orientation.");
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(2, false));
		
		Label lblPosition = new Label(top, SWT.NONE);
		lblPosition.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblPosition.setText("Position (m):");
				
		EditingDomain  editingDomain = ApogyCommonTransactionFacade.INSTANCE.getTransactionalEditingDomain(transformNode);
		
		positionComposite = new Tuple3dComposite(top, SWT.NONE, editingDomain, "0.000");
		positionComposite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false, 1, 1));
		positionComposite.setTuple3d(transformNode.getPosition());
	
		Label lblNodeRotation = new Label(top, SWT.NONE);
		lblNodeRotation.setLayoutData(new GridData(SWT.RIGHT, SWT.TOP, false, false, 1, 1));
		lblNodeRotation.setText("Rotation (deg):");
		
		rotationComposite = new RotationMatrixComposite(top, SWT.NONE, editingDomain);
		rotationComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 1, 1));
		rotationComposite.setMatrix3x3(transformNode.getRotationMatrix());
							
		setControl(top);
		
		// Bindings
		m_bindingContext = initDataBindingsCustom();
		
		// Dispose
		top.addDisposeListener(new DisposeListener() {			
			@Override
			public void widgetDisposed(DisposeEvent e) {				
				if(m_bindingContext != null) m_bindingContext.dispose();
			}
		});
	}			
	
	protected void validate()
	{
		setErrorMessage(null);			
		
		setPageComplete(getErrorMessage() == null);
	}
		
	private DataBindingContext initDataBindingsCustom() 
	{
		DataBindingContext bindingContext = new DataBindingContext();
						
		return bindingContext;
	}
}
