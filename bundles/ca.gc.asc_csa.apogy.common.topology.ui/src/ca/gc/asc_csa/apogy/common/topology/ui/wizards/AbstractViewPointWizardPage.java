package ca.gc.asc_csa.apogy.common.topology.ui.wizards;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import ca.gc.asc_csa.apogy.common.topology.AbstractViewPoint;
import ca.gc.asc_csa.apogy.common.topology.ui.composites.AbstractViewPointComposite;

public class AbstractViewPointWizardPage extends WizardPage 
{
	private final static String WIZARD_PAGE_ID = "ca.gc.asc_csa.apogy.common.topology.ui.wizards.AbstractViewPointWizardPage";
		
	private AbstractViewPoint abstractViewPoint;
	private Adapter adapter;
	private AbstractViewPointComposite abstractViewPointComposite;
			
	public AbstractViewPointWizardPage(AbstractViewPoint abstractViewPoint) 
	{
		super(WIZARD_PAGE_ID);
		this.abstractViewPoint = abstractViewPoint;
				
		setTitle("View Point");
		setDescription("Sets the View Point name and pose.");
		
		if(abstractViewPoint != null) abstractViewPoint.eAdapters().add(getAdapter());
		
		validate();
	}

	@Override
	public void createControl(Composite parent) 
	{
		Composite top = new Composite(parent, SWT.None);
		top.setLayout(new GridLayout(1, false));
		
		abstractViewPointComposite = new AbstractViewPointComposite(top, SWT.NONE, abstractViewPoint);		
		abstractViewPointComposite.setAbstractViewPoint(abstractViewPoint);
		abstractViewPointComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		setControl(top);
		
		top.addDisposeListener(new  DisposeListener() {
			
			@Override
			public void widgetDisposed(DisposeEvent e) {				
				if(abstractViewPointComposite != null && !abstractViewPointComposite.isDisposed()) abstractViewPointComposite.dispose();
				if(abstractViewPoint != null) abstractViewPoint.eAdapters().remove(getAdapter());

			}
		});
	}			
	
	
	protected void validate()
	{
		setErrorMessage(null);			
			
		if(abstractViewPoint.getName() == null)
		{
			setErrorMessage("Name is not set !");
		}
		
		setPageComplete(getErrorMessage() == null);
	}			
	
	private Adapter getAdapter()
	{
		if(adapter == null)
		{
			adapter = new AdapterImpl()
			{
				@Override
				public void notifyChanged(Notification msg) 
				{
					validate();
				}
			};
		}
		
		return adapter;
	}
}
