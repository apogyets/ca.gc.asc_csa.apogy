package ca.gc.asc_csa.apogy.common.topology.ui.util;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ca.gc.asc_csa.apogy.common.topology.ui.NodePresentation;

public class UiTopologyUtils {

	private UiTopologyUtils() {
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> getChildren(List<NodePresentation> lstNodes,
			Class<T> type) {
		ArrayList<T> lstNode = new ArrayList<T>();

		Iterator<NodePresentation> iterator = lstNodes.iterator();
		while (iterator.hasNext()) {
			NodePresentation currentNode = iterator.next();
			if (type.isInstance(currentNode)) {
				lstNode.add((T) currentNode);
			}
		}

		return lstNode;
	}
}
