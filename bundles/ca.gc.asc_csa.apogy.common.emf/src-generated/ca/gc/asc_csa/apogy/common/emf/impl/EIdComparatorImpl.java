/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.impl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.EIdComparator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EId Comparator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EIdComparatorImpl<T extends EObject> extends EComparatorImpl<T> implements EIdComparator<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EIdComparatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFPackage.Literals.EID_COMPARATOR;
	}

	@Override
	public int compare(EObject o1, EObject o2) 
	{		
		String o1Id = ApogyCommonEMFFacade.INSTANCE.getID(o1);
		String o2Id = ApogyCommonEMFFacade.INSTANCE.getID(o2);
		
		if(o1Id == null)
		{
			return -1;
		}
		
		if(o2Id == null)
		{
			return 1;
		}
		
		return o1Id.compareTo(o2Id);
	}
} //EIdComparatorImpl
