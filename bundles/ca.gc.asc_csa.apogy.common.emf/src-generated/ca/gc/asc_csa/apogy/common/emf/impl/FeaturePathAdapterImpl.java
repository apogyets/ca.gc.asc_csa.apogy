/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapterEntry;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Feature
 * Path Adapter</b></em>'. <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class FeaturePathAdapterImpl extends MinimalEObjectImpl.Container implements FeaturePathAdapter {
	private EObject root;
	private List<? extends EStructuralFeature> featurePath;
	/**
	 * List of {@link FeaturePathAdapterEntry}, this is used to know if
	 * notifyChanged needs to be called and to remove the adapters when no
	 * longer needed.
	 */
	private List<FeaturePathAdapterEntry> featurePathAdapterEntryList;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected FeaturePathAdapterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFPackage.Literals.FEATURE_PATH_ADAPTER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public List<FeaturePathAdapterEntry> getFeaturePathAdapterEntryList() {
		if (featurePathAdapterEntryList == null) {
			featurePathAdapterEntryList = new ArrayList<FeaturePathAdapterEntry>();
		}
		return featurePathAdapterEntryList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public List<? extends EStructuralFeature> getFeaturePath() {
		if (featurePath == null) {
			featurePath = getFeatureList();
		}
		return featurePath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void init(EObject root) {
		this.root = root;
		addAdapterOnFeaturePath(root, getFeaturePath());
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void dispose() 
	{
		if(root != null)
		{
			removeAdapterOnFeaturePath(root, getFeaturePath());
			root = null;
		}
		featurePath = null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	abstract public List<? extends EStructuralFeature> getFeatureList();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	private void notifyAdapterOnFeatureChanged(Notification msg) 
	{
		List<FeaturePathAdapterEntry> entries = new ArrayList<FeaturePathAdapterEntry>();
		entries.addAll(getFeaturePathAdapterEntryList());
		for (FeaturePathAdapterEntry entry : entries) 
		{
			if (entry.getNotifier() == msg.getNotifier() && entry.getFeature() == msg.getFeature()) 
			{
				List<EStructuralFeature> nextFeaturesList = new ArrayList<>();
				nextFeaturesList.addAll(getFeaturePath());
				/**
				 * Keeps only the StructuralFeatures up to the notification's
				 * feature.
				 */
				for (int i = 0; i <= getFeaturePath().indexOf(msg.getFeature()); i++) 
				{
					nextFeaturesList.remove(0);
				}

				/** Remove the adapters if needed. */
				
				List<FeaturePathAdapterEntry> entryList = new ArrayList<FeaturePathAdapterEntry>();
				entryList.addAll(getFeaturePathAdapterEntryList());
				for (FeaturePathAdapterEntry entry2 : entryList) 
				{
					if (entry2.getNotifier() == msg.getOldValue()) 
					{
						removeAdapterOnFeaturePath((EObject) msg.getOldValue(), nextFeaturesList);
						break;
					}
				}

				/** Add the adapters if needed. */
				if (!nextFeaturesList.isEmpty() && msg.getNewValue() != null) 
				{
					addAdapterOnFeaturePath((EObject) msg.getNewValue(), nextFeaturesList);
				}

				notifyChanged(msg);
				break;
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	abstract public void notifyChanged(Notification msg);

	/**
	 * @generated_NOT
	 */
	private void addAdapterOnFeaturePath(EObject root, List<? extends EStructuralFeature> featurePath) {
		/** Create the adapter */
		Adapter adapter = new AdapterImpl() {
			@Override
			public void notifyChanged(Notification msg) {
				FeaturePathAdapterImpl.this.notifyAdapterOnFeatureChanged(msg);
			}
		};

		/** Add the adapter to the EObject and add the entry to the list. */
		root.eAdapters().add(adapter);
		FeaturePathAdapterEntry entry = ApogyCommonEMFFactory.eINSTANCE.createFeaturePathAdapterEntry();
		entry.setNotifier(root);
		entry.setAdapter(adapter);
		entry.setFeature(featurePath.get(0));
		this.getFeaturePathAdapterEntryList().add(entry);

		/**
		 * If necessary add adapters on the next EObject in the feature path.
		 */
		if (featurePath.size() > 1) {
			List<EStructuralFeature> nextFeaturesList = new ArrayList<>();
			nextFeaturesList.addAll(featurePath);
			nextFeaturesList.remove(0);

			/** Get the next object */
			Object next = root.eGet(featurePath.get(0));

			/** If the object is a list */
			if (next instanceof List) {
				/** For each object of the list */
				for (Object object : (List<?>) next) {
					if (object instanceof EObject) {
						if (!nextFeaturesList.isEmpty() && nextFeaturesList.get(0).getEContainingClass()
								.isSuperTypeOf(((EObject) object).eClass())) {
							addAdapterOnFeaturePath((EObject) object, nextFeaturesList);
						}
					}
				}
			}
			/** Otherwise, if the object is a EObject */
			else if (next instanceof EObject) {
				if (!nextFeaturesList.isEmpty()
						&& nextFeaturesList.get(0).getEContainingClass().isSuperTypeOf(((EObject) next).eClass())) {
					addAdapterOnFeaturePath((EObject) next, nextFeaturesList);
				}
			}
		}
	}

	/**
	 * @generated_NOT
	 */
	private void removeAdapterOnFeaturePath(EObject root, List<? extends EStructuralFeature> featurePath) {
		/**
		 * Remove the adapter of the EObject and remove the entry from the
		 * HashMap
		 */
		FeaturePathAdapterEntry entryToDelete = null;
		for (FeaturePathAdapterEntry entry : getFeaturePathAdapterEntryList()) {
			if (entry.getNotifier() == root) {
				entryToDelete = entry;
				root.eAdapters().remove(entry.getAdapter());
				break;
			}
		}
		if (entryToDelete != null) {
			getFeaturePathAdapterEntryList().remove(entryToDelete);
		}

		/**
		 * If necessary remove adapters on the next EObject in the feature path
		 */
		if (featurePath.size() > 1) {
			List<EStructuralFeature> nextFeaturePath = new ArrayList<>();
			nextFeaturePath.addAll(featurePath);
			nextFeaturePath.remove(0);

			/** Get the next object */
			Object next = root.eGet(featurePath.get(0));

			/** If the object is a list */
			if (next instanceof List) {
				/** For each object of the list */
				for (Object object : (List<?>) next) {
					if (object instanceof EObject) {
						if (!nextFeaturePath.isEmpty()) {
							removeAdapterOnFeaturePath((EObject) object, nextFeaturePath);
						}
					}
				}
			}
			/** Otherwise, if the object is a EObject */
			else if (next instanceof EObject) {
				if (!nextFeaturePath.isEmpty()) {
					removeAdapterOnFeaturePath((EObject) next, nextFeaturePath);
				}
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ApogyCommonEMFPackage.FEATURE_PATH_ADAPTER___INIT__EOBJECT:
				init((EObject)arguments.get(0));
				return null;
			case ApogyCommonEMFPackage.FEATURE_PATH_ADAPTER___DISPOSE:
				dispose();
				return null;
			case ApogyCommonEMFPackage.FEATURE_PATH_ADAPTER___GET_FEATURE_PATH:
				return getFeaturePath();
			case ApogyCommonEMFPackage.FEATURE_PATH_ADAPTER___NOTIFY_CHANGED__NOTIFICATION:
				notifyChanged((Notification)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} // FeaturePathAdapterImpl
