/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.impl;

import org.eclipse.emf.ecore.EClass;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.TimedComparator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timed Comparator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TimedComparatorImpl<T extends Timed> extends EComparatorImpl<T> implements TimedComparator<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedComparatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFPackage.Literals.TIMED_COMPARATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public int compare(Timed o1, Timed o2) 
	{
		long t1 = o1.getTime().getTime();
		long t2 = o2.getTime().getTime();
		
		if(t1 < t2)
		{
			return -1;
		}
		else if(t1 > t2)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

} //TimedComparatorImpl
