/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.impl;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.TimedAfterFilter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timed After Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.impl.TimedAfterFilterImpl#isInclusive <em>Inclusive</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.impl.TimedAfterFilterImpl#getAfterDate <em>After Date</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimedAfterFilterImpl<T extends Timed> extends IFilterImpl<T> implements TimedAfterFilter<T> {
	/**
	 * The default value of the '{@link #isInclusive() <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInclusive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INCLUSIVE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isInclusive() <em>Inclusive</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInclusive()
	 * @generated
	 * @ordered
	 */
	protected boolean inclusive = INCLUSIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAfterDate() <em>After Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAfterDate()
	 * @generated
	 * @ordered
	 */
	protected static final Date AFTER_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAfterDate() <em>After Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAfterDate()
	 * @generated
	 * @ordered
	 */
	protected Date afterDate = AFTER_DATE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimedAfterFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFPackage.Literals.TIMED_AFTER_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInclusive() {
		return inclusive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInclusive(boolean newInclusive) {
		boolean oldInclusive = inclusive;
		inclusive = newInclusive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFPackage.TIMED_AFTER_FILTER__INCLUSIVE, oldInclusive, inclusive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getAfterDate() {
		return afterDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAfterDate(Date newAfterDate) {
		Date oldAfterDate = afterDate;
		afterDate = newAfterDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFPackage.TIMED_AFTER_FILTER__AFTER_DATE, oldAfterDate, afterDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	public boolean matches(Timed object) 
	{
		if(isInclusive())
		{
			return object.getTime().getTime() >= getAfterDate().getTime();
		}
		else
		{
			return object.getTime().getTime() > getAfterDate().getTime();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER__INCLUSIVE:
				return isInclusive();
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER__AFTER_DATE:
				return getAfterDate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER__INCLUSIVE:
				setInclusive((Boolean)newValue);
				return;
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER__AFTER_DATE:
				setAfterDate((Date)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER__INCLUSIVE:
				setInclusive(INCLUSIVE_EDEFAULT);
				return;
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER__AFTER_DATE:
				setAfterDate(AFTER_DATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER__INCLUSIVE:
				return inclusive != INCLUSIVE_EDEFAULT;
			case ApogyCommonEMFPackage.TIMED_AFTER_FILTER__AFTER_DATE:
				return AFTER_DATE_EDEFAULT == null ? afterDate != null : !AFTER_DATE_EDEFAULT.equals(afterDate);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (inclusive: ");
		result.append(inclusive);
		result.append(", afterDate: ");
		result.append(afterDate);
		result.append(')');
		return result.toString();
	}

} //TimedAfterFilterImpl
