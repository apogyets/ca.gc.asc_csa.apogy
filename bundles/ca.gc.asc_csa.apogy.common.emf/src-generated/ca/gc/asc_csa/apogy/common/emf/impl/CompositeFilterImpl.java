/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.impl;

import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.CompositeFilter;
import ca.gc.asc_csa.apogy.common.emf.CompositeFilterType;
import ca.gc.asc_csa.apogy.common.emf.IFilter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.impl.CompositeFilterImpl#getFilterChainType <em>Filter Chain Type</em>}</li>
 *   <li>{@link ca.gc.asc_csa.apogy.common.emf.impl.CompositeFilterImpl#getFilters <em>Filters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeFilterImpl<T> extends IFilterImpl<T> implements CompositeFilter<T> {
	/**
	 * The default value of the '{@link #getFilterChainType() <em>Filter Chain Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterChainType()
	 * @generated
	 * @ordered
	 */
	protected static final CompositeFilterType FILTER_CHAIN_TYPE_EDEFAULT = CompositeFilterType.AND;

	/**
	 * The cached value of the '{@link #getFilterChainType() <em>Filter Chain Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterChainType()
	 * @generated
	 * @ordered
	 */
	protected CompositeFilterType filterChainType = FILTER_CHAIN_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFilters() <em>Filters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilters()
	 * @generated
	 * @ordered
	 */
	protected EList<IFilter<T>> filters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFPackage.Literals.COMPOSITE_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeFilterType getFilterChainType() {
		return filterChainType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterChainType(CompositeFilterType newFilterChainType) {
		CompositeFilterType oldFilterChainType = filterChainType;
		filterChainType = newFilterChainType == null ? FILTER_CHAIN_TYPE_EDEFAULT : newFilterChainType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTER_CHAIN_TYPE, oldFilterChainType, filterChainType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IFilter<T>> getFilters() {
		if (filters == null) {
			filters = new EObjectContainmentEList<IFilter<T>>(IFilter.class, this, ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTERS);
		}
		return filters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTERS:
				return ((InternalEList<?>)getFilters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTER_CHAIN_TYPE:
				return getFilterChainType();
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTERS:
				return getFilters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTER_CHAIN_TYPE:
				setFilterChainType((CompositeFilterType)newValue);
				return;
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTERS:
				getFilters().clear();
				getFilters().addAll((Collection<? extends IFilter<T>>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTER_CHAIN_TYPE:
				setFilterChainType(FILTER_CHAIN_TYPE_EDEFAULT);
				return;
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTERS:
				getFilters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTER_CHAIN_TYPE:
				return filterChainType != FILTER_CHAIN_TYPE_EDEFAULT;
			case ApogyCommonEMFPackage.COMPOSITE_FILTER__FILTERS:
				return filters != null && !filters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (filterChainType: ");
		result.append(filterChainType);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean matches(T object) 
	{
		boolean matches = false;
		if(getFilters().size() == 0) return true;
		
		switch (getFilterChainType().getValue()) 
		{
			case CompositeFilterType.AND_VALUE:
			{
				Iterator<IFilter<T>> it = getFilters().iterator();
				matches = true;
				while(it.hasNext() && matches)
				{
					matches = it.next().matches(object);
				}
			}
			break;
			
			case CompositeFilterType.OR_VALUE:
			{
				Iterator<IFilter<T>> it = getFilters().iterator();
				matches = false;
				while(it.hasNext() && !matches)
				{
					matches = it.next().matches(object);
				}
			}
			break;

			default:
			break;
		}		
		return matches;
	}
} //CompositeFilterImpl
