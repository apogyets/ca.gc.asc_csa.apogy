/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/
package ca.gc.asc_csa.apogy.common.emf.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;

import javax.measure.unit.Unit;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.ui.workbench.modeling.ESelectionService;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureListNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureSpecifier;
import ca.gc.asc_csa.apogy.common.emf.AbstractFeatureTreeNode;
import ca.gc.asc_csa.apogy.common.emf.AbstractRootNode;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFactory;
import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.BrowseableTimeSource;
import ca.gc.asc_csa.apogy.common.emf.CollectionTimedTimeSource;
import ca.gc.asc_csa.apogy.common.emf.CompositeComparator;
import ca.gc.asc_csa.apogy.common.emf.CompositeFilter;
import ca.gc.asc_csa.apogy.common.emf.CompositeFilterType;
import ca.gc.asc_csa.apogy.common.emf.CurrentTimeSource;
import ca.gc.asc_csa.apogy.common.emf.Described;
import ca.gc.asc_csa.apogy.common.emf.Disposable;
import ca.gc.asc_csa.apogy.common.emf.Duration;
import ca.gc.asc_csa.apogy.common.emf.EClassFilter;
import ca.gc.asc_csa.apogy.common.emf.EComparator;
import ca.gc.asc_csa.apogy.common.emf.EIdComparator;
import ca.gc.asc_csa.apogy.common.emf.EObjectReference;
import ca.gc.asc_csa.apogy.common.emf.FeatureNodeAdapter;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapter;
import ca.gc.asc_csa.apogy.common.emf.FeaturePathAdapterEntry;
import ca.gc.asc_csa.apogy.common.emf.FixedTimeSource;
import ca.gc.asc_csa.apogy.common.emf.IFilter;
import ca.gc.asc_csa.apogy.common.emf.ListFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.ListRootNode;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.NamedComparator;
import ca.gc.asc_csa.apogy.common.emf.Ranges;
import ca.gc.asc_csa.apogy.common.emf.Server;
import ca.gc.asc_csa.apogy.common.emf.Startable;
import ca.gc.asc_csa.apogy.common.emf.TimeDirection;
import ca.gc.asc_csa.apogy.common.emf.TimeSource;
import ca.gc.asc_csa.apogy.common.emf.Timed;
import ca.gc.asc_csa.apogy.common.emf.TimedAfterFilter;
import ca.gc.asc_csa.apogy.common.emf.TimedBeforeFilter;
import ca.gc.asc_csa.apogy.common.emf.TimedComparator;
import ca.gc.asc_csa.apogy.common.emf.TimedCompositeFilter;
import ca.gc.asc_csa.apogy.common.emf.TreeFeatureNode;
import ca.gc.asc_csa.apogy.common.emf.TreeRootNode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ApogyCommonEMFPackageImpl extends EPackageImpl implements ApogyCommonEMFPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass apogyCommonEMFFacadeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eObjectReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass describedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass startableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass durationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixedTimeSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass currentTimeSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass browseableTimeSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass collectionTimedTimeSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass disposableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureNodeAdapterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractFeatureNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractFeatureListNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractFeatureTreeNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractRootNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass treeRootNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass treeFeatureNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass listRootNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass listFeatureNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractFeatureSpecifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featurePathAdapterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featurePathAdapterEntryEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eIdComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedBeforeFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedAfterFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timedCompositeFilterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum timeDirectionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum rangesEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum compositeFilterTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType exceptionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType listEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType sortedSetEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eClassFilterEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType numberEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType iFileEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType resourceEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType unitEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType collectionEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType uriEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType jobEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType resourceSetEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType listNamedEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType listFeatureEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType adapterEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType notificationEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType hashMapEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType eSelectionServiceEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ApogyCommonEMFPackageImpl() {
		super(eNS_URI, ApogyCommonEMFFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ApogyCommonEMFPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ApogyCommonEMFPackage init() {
		if (isInited) return (ApogyCommonEMFPackage)EPackage.Registry.INSTANCE.getEPackage(ApogyCommonEMFPackage.eNS_URI);

		// Obtain or create and register package
		ApogyCommonEMFPackageImpl theApogyCommonEMFPackage = (ApogyCommonEMFPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ApogyCommonEMFPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ApogyCommonEMFPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theApogyCommonEMFPackage.createPackageContents();

		// Initialize created meta-data
		theApogyCommonEMFPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theApogyCommonEMFPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ApogyCommonEMFPackage.eNS_URI, theApogyCommonEMFPackage);
		return theApogyCommonEMFPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApogyCommonEMFFacade() {
		return apogyCommonEMFFacadeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getApogyCommonEMFFacade_DateFormatString() {
		return (EAttribute)apogyCommonEMFFacadeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetAllAvailableEClasses() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetAllSubEClasses__EClass() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__FilterEClasses__List_EClassFilter() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__FilterEClasses__List_List() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__FindClosestMatch__EClass_List() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetEClass__String() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__SortAlphabetically__List() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetAllAvailableEOperations__EClass() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__SortEOperationsAlphabetically__List() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetDocumentation__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetDocumentation__EClass() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetEngineeringUnitsAsString__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetEngineeringUnits__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetValueUpdateRate__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetWarningOCLExpression__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetAlarmOCLExpression__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetOutOfRangeOCLExpression__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetWarningMinValue__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetWarningMaxValue__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetAlarmMinValue__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetAlarmMaxValue__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetOutOfRangeMinValue__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetOutOfRangeMaxValue__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetRange__ETypedElement_Object() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetFullDescription__ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetAncestriesString__AbstractFeatureNode() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetFeatureRoot__AbstractFeatureNode() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetAncestries__AbstractFeatureNode() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetDescendants__AbstractFeatureNode() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetChildEStructuralFeatures__AbstractFeatureNode() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__Resolve__EObject_AbstractFeatureNode() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__SetValue__EObject_AbstractFeatureNode_Object() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__IsResolved__EObject_AbstractFeatureNode() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetLeaf__ListRootNode() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetFile__Resource() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetContent__URI() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__SortTimed__Collection() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(36);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetTimeSpan__Collection() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(37);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetID__EObject() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(38);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetEObjectById__ResourceSet_String() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(39);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetEObjectsByType__EObject_EClass() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(40);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetDefaultName__EObject_EObject_ETypedElement() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(41);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetChildEClasses__EClass() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(42);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__GetSettableEReferences__EObject() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(43);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__ToString__List_String() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(44);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__Format__Date() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(45);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__StopAllStartables__EObject() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(46);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__SerializeEObject__EObject_String() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(47);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getApogyCommonEMFFacade__DeserializeString__String_String() {
		return apogyCommonEMFFacadeEClass.getEOperations().get(48);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEObjectReference() {
		return eObjectReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEObjectReference_EObject() {
		return (EReference)eObjectReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamed() {
		return namedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamed_Name() {
		return (EAttribute)namedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDescribed() {
		return describedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescribed_Description() {
		return (EAttribute)describedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimed() {
		return timedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimed_Time() {
		return (EAttribute)timedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServer() {
		return serverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServer_ServerJob() {
		return (EAttribute)serverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStartable() {
		return startableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStartable_Started() {
		return (EAttribute)startableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDuration() {
		return durationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDuration_Value() {
		return (EAttribute)durationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDuration_Days() {
		return (EAttribute)durationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDuration_Hours() {
		return (EAttribute)durationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDuration_Minutes() {
		return (EAttribute)durationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDuration_Seconds() {
		return (EAttribute)durationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDuration_Milliseconds() {
		return (EAttribute)durationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDuration__GetDuration__Timed_Timed() {
		return durationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimeSource() {
		return timeSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimeSource_Offset() {
		return (EAttribute)timeSourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixedTimeSource() {
		return fixedTimeSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCurrentTimeSource() {
		return currentTimeSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCurrentTimeSource_UpdatePeriod() {
		return (EAttribute)currentTimeSourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCurrentTimeSource_Paused() {
		return (EAttribute)currentTimeSourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCurrentTimeSource__Pause() {
		return currentTimeSourceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCurrentTimeSource__Resume() {
		return currentTimeSourceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBrowseableTimeSource() {
		return browseableTimeSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBrowseableTimeSource_StartTime() {
		return (EAttribute)browseableTimeSourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBrowseableTimeSource_UpdatePeriod() {
		return (EAttribute)browseableTimeSourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBrowseableTimeSource_TimeAcceration() {
		return (EAttribute)browseableTimeSourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBrowseableTimeSource_TimeDirection() {
		return (EAttribute)browseableTimeSourceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBrowseableTimeSource__PlayForward() {
		return browseableTimeSourceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBrowseableTimeSource__PlayReverse() {
		return browseableTimeSourceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBrowseableTimeSource__Pause() {
		return browseableTimeSourceEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBrowseableTimeSource__Reset() {
		return browseableTimeSourceEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCollectionTimedTimeSource() {
		return collectionTimedTimeSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCollectionTimedTimeSource_LoopEnable() {
		return (EAttribute)collectionTimedTimeSourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCollectionTimedTimeSource_TimedsList() {
		return (EReference)collectionTimedTimeSourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCollectionTimedTimeSource_CurrentTimedElement() {
		return (EReference)collectionTimedTimeSourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCollectionTimedTimeSource_EarliestDate() {
		return (EAttribute)collectionTimedTimeSourceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCollectionTimedTimeSource_LatestDate() {
		return (EAttribute)collectionTimedTimeSourceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCollectionTimedTimeSource__JumpToNext() {
		return collectionTimedTimeSourceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCollectionTimedTimeSource__JumpToPrevious() {
		return collectionTimedTimeSourceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDisposable() {
		return disposableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDisposable__Dispose() {
		return disposableEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureNodeAdapter() {
		return featureNodeAdapterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureNodeAdapter_SourceObject() {
		return (EReference)featureNodeAdapterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureNodeAdapter_FeatureNode() {
		return (EReference)featureNodeAdapterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureNodeAdapter_CurrentValue() {
		return (EAttribute)featureNodeAdapterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeatureNodeAdapter_Resolved() {
		return (EAttribute)featureNodeAdapterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractFeatureNode() {
		return abstractFeatureNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractFeatureListNode() {
		return abstractFeatureListNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractFeatureListNode_Parent() {
		return (EReference)abstractFeatureListNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractFeatureListNode_Child() {
		return (EReference)abstractFeatureListNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractFeatureTreeNode() {
		return abstractFeatureTreeNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractFeatureTreeNode_Parent() {
		return (EReference)abstractFeatureTreeNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractFeatureTreeNode_Children() {
		return (EReference)abstractFeatureTreeNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractRootNode() {
		return abstractRootNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractRootNode_SourceClass() {
		return (EReference)abstractRootNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTreeRootNode() {
		return treeRootNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTreeFeatureNode() {
		return treeFeatureNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getListRootNode() {
		return listRootNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getListFeatureNode() {
		return listFeatureNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractFeatureSpecifier() {
		return abstractFeatureSpecifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAbstractFeatureSpecifier_StructuralFeature() {
		return (EReference)abstractFeatureSpecifierEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractFeatureSpecifier_MultiValued() {
		return (EAttribute)abstractFeatureSpecifierEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractFeatureSpecifier_Index() {
		return (EAttribute)abstractFeatureSpecifierEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeaturePathAdapter() {
		return featurePathAdapterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFeaturePathAdapter__Init__EObject() {
		return featurePathAdapterEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFeaturePathAdapter__Dispose() {
		return featurePathAdapterEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFeaturePathAdapter__GetFeaturePath() {
		return featurePathAdapterEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFeaturePathAdapter__NotifyChanged__Notification() {
		return featurePathAdapterEClass.getEOperations().get(3);
	}

		/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeaturePathAdapterEntry() {
		return featurePathAdapterEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeaturePathAdapterEntry_Notifier() {
		return (EReference)featurePathAdapterEntryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeaturePathAdapterEntry_Feature() {
		return (EReference)featurePathAdapterEntryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFeaturePathAdapterEntry_Adapter() {
		return (EAttribute)featurePathAdapterEntryEClass.getEStructuralFeatures().get(2);
	}

		/**
	 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIComparator() {
		return iComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIComparator__Compare__Object_Object() {
		return iComparatorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEComparator() {
		return eComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeComparator() {
		return compositeComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeComparator_Comparators() {
		return (EReference)compositeComparatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEIdComparator() {
		return eIdComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedComparator() {
		return timedComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedComparator() {
		return namedComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIFilter() {
		return iFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIFilter__Matches__Object() {
		return iFilterEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIFilter__Filter__Collection() {
		return iFilterEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeFilter() {
		return compositeFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCompositeFilter_FilterChainType() {
		return (EAttribute)compositeFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeFilter_Filters() {
		return (EReference)compositeFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedBeforeFilter() {
		return timedBeforeFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedBeforeFilter_Inclusive() {
		return (EAttribute)timedBeforeFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedBeforeFilter_BeforeDate() {
		return (EAttribute)timedBeforeFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedAfterFilter() {
		return timedAfterFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedAfterFilter_Inclusive() {
		return (EAttribute)timedAfterFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimedAfterFilter_AfterDate() {
		return (EAttribute)timedAfterFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimedCompositeFilter() {
		return timedCompositeFilterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTimeDirection() {
		return timeDirectionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRanges() {
		return rangesEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCompositeFilterType() {
		return compositeFilterTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getException() {
		return exceptionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getList() {
		return listEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getSortedSet() {
		return sortedSetEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEClassFilter() {
		return eClassFilterEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNumber() {
		return numberEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIFile() {
		return iFileEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getResource() {
		return resourceEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getUnit() {
		return unitEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getCollection() {
		return collectionEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getEList() {
		return eListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getURI() {
		return uriEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getJob() {
		return jobEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getResourceSet() {
		return resourceSetEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getListNamed() {
		return listNamedEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getListFeature() {
		return listFeatureEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAdapter() {
		return adapterEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNotification() {
		return notificationEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getHashMap() {
		return hashMapEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getESelectionService() {
		return eSelectionServiceEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApogyCommonEMFFactory getApogyCommonEMFFactory() {
		return (ApogyCommonEMFFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		apogyCommonEMFFacadeEClass = createEClass(APOGY_COMMON_EMF_FACADE);
		createEAttribute(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE__DATE_FORMAT_STRING);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ALL_AVAILABLE_ECLASSES);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ALL_SUB_ECLASSES__ECLASS);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___FILTER_ECLASSES__LIST_ECLASSFILTER);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___FILTER_ECLASSES__LIST_LIST);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___FIND_CLOSEST_MATCH__ECLASS_LIST);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ECLASS__STRING);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___SORT_ALPHABETICALLY__LIST);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ALL_AVAILABLE_EOPERATIONS__ECLASS);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___SORT_EOPERATIONS_ALPHABETICALLY__LIST);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_DOCUMENTATION__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_DOCUMENTATION__ECLASS);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ENGINEERING_UNITS_AS_STRING__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ENGINEERING_UNITS__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_VALUE_UPDATE_RATE__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_WARNING_OCL_EXPRESSION__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ALARM_OCL_EXPRESSION__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_OUT_OF_RANGE_OCL_EXPRESSION__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_WARNING_MIN_VALUE__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_WARNING_MAX_VALUE__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ALARM_MIN_VALUE__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ALARM_MAX_VALUE__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_OUT_OF_RANGE_MIN_VALUE__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_OUT_OF_RANGE_MAX_VALUE__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_RANGE__ETYPEDELEMENT_OBJECT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_FULL_DESCRIPTION__ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ANCESTRIES_STRING__ABSTRACTFEATURENODE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_FEATURE_ROOT__ABSTRACTFEATURENODE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ANCESTRIES__ABSTRACTFEATURENODE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_DESCENDANTS__ABSTRACTFEATURENODE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_CHILD_ESTRUCTURAL_FEATURES__ABSTRACTFEATURENODE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___RESOLVE__EOBJECT_ABSTRACTFEATURENODE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___SET_VALUE__EOBJECT_ABSTRACTFEATURENODE_OBJECT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___IS_RESOLVED__EOBJECT_ABSTRACTFEATURENODE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_LEAF__LISTROOTNODE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_FILE__RESOURCE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_CONTENT__URI);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___SORT_TIMED__COLLECTION);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_TIME_SPAN__COLLECTION);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_ID__EOBJECT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_EOBJECT_BY_ID__RESOURCESET_STRING);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_EOBJECTS_BY_TYPE__EOBJECT_ECLASS);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_DEFAULT_NAME__EOBJECT_EOBJECT_ETYPEDELEMENT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_CHILD_ECLASSES__ECLASS);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___GET_SETTABLE_EREFERENCES__EOBJECT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___TO_STRING__LIST_STRING);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___FORMAT__DATE);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___STOP_ALL_STARTABLES__EOBJECT);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___SERIALIZE_EOBJECT__EOBJECT_STRING);
		createEOperation(apogyCommonEMFFacadeEClass, APOGY_COMMON_EMF_FACADE___DESERIALIZE_STRING__STRING_STRING);

		eObjectReferenceEClass = createEClass(EOBJECT_REFERENCE);
		createEReference(eObjectReferenceEClass, EOBJECT_REFERENCE__EOBJECT);

		namedEClass = createEClass(NAMED);
		createEAttribute(namedEClass, NAMED__NAME);

		describedEClass = createEClass(DESCRIBED);
		createEAttribute(describedEClass, DESCRIBED__DESCRIPTION);

		timedEClass = createEClass(TIMED);
		createEAttribute(timedEClass, TIMED__TIME);

		serverEClass = createEClass(SERVER);
		createEAttribute(serverEClass, SERVER__SERVER_JOB);

		startableEClass = createEClass(STARTABLE);
		createEAttribute(startableEClass, STARTABLE__STARTED);

		durationEClass = createEClass(DURATION);
		createEAttribute(durationEClass, DURATION__VALUE);
		createEAttribute(durationEClass, DURATION__DAYS);
		createEAttribute(durationEClass, DURATION__HOURS);
		createEAttribute(durationEClass, DURATION__MINUTES);
		createEAttribute(durationEClass, DURATION__SECONDS);
		createEAttribute(durationEClass, DURATION__MILLISECONDS);
		createEOperation(durationEClass, DURATION___GET_DURATION__TIMED_TIMED);

		timeSourceEClass = createEClass(TIME_SOURCE);
		createEAttribute(timeSourceEClass, TIME_SOURCE__OFFSET);

		fixedTimeSourceEClass = createEClass(FIXED_TIME_SOURCE);

		currentTimeSourceEClass = createEClass(CURRENT_TIME_SOURCE);
		createEAttribute(currentTimeSourceEClass, CURRENT_TIME_SOURCE__UPDATE_PERIOD);
		createEAttribute(currentTimeSourceEClass, CURRENT_TIME_SOURCE__PAUSED);
		createEOperation(currentTimeSourceEClass, CURRENT_TIME_SOURCE___PAUSE);
		createEOperation(currentTimeSourceEClass, CURRENT_TIME_SOURCE___RESUME);

		browseableTimeSourceEClass = createEClass(BROWSEABLE_TIME_SOURCE);
		createEAttribute(browseableTimeSourceEClass, BROWSEABLE_TIME_SOURCE__START_TIME);
		createEAttribute(browseableTimeSourceEClass, BROWSEABLE_TIME_SOURCE__UPDATE_PERIOD);
		createEAttribute(browseableTimeSourceEClass, BROWSEABLE_TIME_SOURCE__TIME_ACCERATION);
		createEAttribute(browseableTimeSourceEClass, BROWSEABLE_TIME_SOURCE__TIME_DIRECTION);
		createEOperation(browseableTimeSourceEClass, BROWSEABLE_TIME_SOURCE___PLAY_FORWARD);
		createEOperation(browseableTimeSourceEClass, BROWSEABLE_TIME_SOURCE___PLAY_REVERSE);
		createEOperation(browseableTimeSourceEClass, BROWSEABLE_TIME_SOURCE___PAUSE);
		createEOperation(browseableTimeSourceEClass, BROWSEABLE_TIME_SOURCE___RESET);

		collectionTimedTimeSourceEClass = createEClass(COLLECTION_TIMED_TIME_SOURCE);
		createEAttribute(collectionTimedTimeSourceEClass, COLLECTION_TIMED_TIME_SOURCE__LOOP_ENABLE);
		createEReference(collectionTimedTimeSourceEClass, COLLECTION_TIMED_TIME_SOURCE__TIMEDS_LIST);
		createEReference(collectionTimedTimeSourceEClass, COLLECTION_TIMED_TIME_SOURCE__CURRENT_TIMED_ELEMENT);
		createEAttribute(collectionTimedTimeSourceEClass, COLLECTION_TIMED_TIME_SOURCE__EARLIEST_DATE);
		createEAttribute(collectionTimedTimeSourceEClass, COLLECTION_TIMED_TIME_SOURCE__LATEST_DATE);
		createEOperation(collectionTimedTimeSourceEClass, COLLECTION_TIMED_TIME_SOURCE___JUMP_TO_NEXT);
		createEOperation(collectionTimedTimeSourceEClass, COLLECTION_TIMED_TIME_SOURCE___JUMP_TO_PREVIOUS);

		disposableEClass = createEClass(DISPOSABLE);
		createEOperation(disposableEClass, DISPOSABLE___DISPOSE);

		featureNodeAdapterEClass = createEClass(FEATURE_NODE_ADAPTER);
		createEReference(featureNodeAdapterEClass, FEATURE_NODE_ADAPTER__SOURCE_OBJECT);
		createEReference(featureNodeAdapterEClass, FEATURE_NODE_ADAPTER__FEATURE_NODE);
		createEAttribute(featureNodeAdapterEClass, FEATURE_NODE_ADAPTER__CURRENT_VALUE);
		createEAttribute(featureNodeAdapterEClass, FEATURE_NODE_ADAPTER__RESOLVED);

		abstractFeatureNodeEClass = createEClass(ABSTRACT_FEATURE_NODE);

		abstractFeatureListNodeEClass = createEClass(ABSTRACT_FEATURE_LIST_NODE);
		createEReference(abstractFeatureListNodeEClass, ABSTRACT_FEATURE_LIST_NODE__PARENT);
		createEReference(abstractFeatureListNodeEClass, ABSTRACT_FEATURE_LIST_NODE__CHILD);

		abstractFeatureTreeNodeEClass = createEClass(ABSTRACT_FEATURE_TREE_NODE);
		createEReference(abstractFeatureTreeNodeEClass, ABSTRACT_FEATURE_TREE_NODE__PARENT);
		createEReference(abstractFeatureTreeNodeEClass, ABSTRACT_FEATURE_TREE_NODE__CHILDREN);

		abstractRootNodeEClass = createEClass(ABSTRACT_ROOT_NODE);
		createEReference(abstractRootNodeEClass, ABSTRACT_ROOT_NODE__SOURCE_CLASS);

		treeRootNodeEClass = createEClass(TREE_ROOT_NODE);

		treeFeatureNodeEClass = createEClass(TREE_FEATURE_NODE);

		listRootNodeEClass = createEClass(LIST_ROOT_NODE);

		listFeatureNodeEClass = createEClass(LIST_FEATURE_NODE);

		abstractFeatureSpecifierEClass = createEClass(ABSTRACT_FEATURE_SPECIFIER);
		createEReference(abstractFeatureSpecifierEClass, ABSTRACT_FEATURE_SPECIFIER__STRUCTURAL_FEATURE);
		createEAttribute(abstractFeatureSpecifierEClass, ABSTRACT_FEATURE_SPECIFIER__MULTI_VALUED);
		createEAttribute(abstractFeatureSpecifierEClass, ABSTRACT_FEATURE_SPECIFIER__INDEX);

		featurePathAdapterEClass = createEClass(FEATURE_PATH_ADAPTER);
		createEOperation(featurePathAdapterEClass, FEATURE_PATH_ADAPTER___INIT__EOBJECT);
		createEOperation(featurePathAdapterEClass, FEATURE_PATH_ADAPTER___DISPOSE);
		createEOperation(featurePathAdapterEClass, FEATURE_PATH_ADAPTER___GET_FEATURE_PATH);
		createEOperation(featurePathAdapterEClass, FEATURE_PATH_ADAPTER___NOTIFY_CHANGED__NOTIFICATION);

		featurePathAdapterEntryEClass = createEClass(FEATURE_PATH_ADAPTER_ENTRY);
		createEReference(featurePathAdapterEntryEClass, FEATURE_PATH_ADAPTER_ENTRY__NOTIFIER);
		createEReference(featurePathAdapterEntryEClass, FEATURE_PATH_ADAPTER_ENTRY__FEATURE);
		createEAttribute(featurePathAdapterEntryEClass, FEATURE_PATH_ADAPTER_ENTRY__ADAPTER);

		iComparatorEClass = createEClass(ICOMPARATOR);
		createEOperation(iComparatorEClass, ICOMPARATOR___COMPARE__OBJECT_OBJECT);

		eComparatorEClass = createEClass(ECOMPARATOR);

		compositeComparatorEClass = createEClass(COMPOSITE_COMPARATOR);
		createEReference(compositeComparatorEClass, COMPOSITE_COMPARATOR__COMPARATORS);

		eIdComparatorEClass = createEClass(EID_COMPARATOR);

		timedComparatorEClass = createEClass(TIMED_COMPARATOR);

		namedComparatorEClass = createEClass(NAMED_COMPARATOR);

		iFilterEClass = createEClass(IFILTER);
		createEOperation(iFilterEClass, IFILTER___MATCHES__OBJECT);
		createEOperation(iFilterEClass, IFILTER___FILTER__COLLECTION);

		compositeFilterEClass = createEClass(COMPOSITE_FILTER);
		createEAttribute(compositeFilterEClass, COMPOSITE_FILTER__FILTER_CHAIN_TYPE);
		createEReference(compositeFilterEClass, COMPOSITE_FILTER__FILTERS);

		timedBeforeFilterEClass = createEClass(TIMED_BEFORE_FILTER);
		createEAttribute(timedBeforeFilterEClass, TIMED_BEFORE_FILTER__INCLUSIVE);
		createEAttribute(timedBeforeFilterEClass, TIMED_BEFORE_FILTER__BEFORE_DATE);

		timedAfterFilterEClass = createEClass(TIMED_AFTER_FILTER);
		createEAttribute(timedAfterFilterEClass, TIMED_AFTER_FILTER__INCLUSIVE);
		createEAttribute(timedAfterFilterEClass, TIMED_AFTER_FILTER__AFTER_DATE);

		timedCompositeFilterEClass = createEClass(TIMED_COMPOSITE_FILTER);

		// Create enums
		timeDirectionEEnum = createEEnum(TIME_DIRECTION);
		rangesEEnum = createEEnum(RANGES);
		compositeFilterTypeEEnum = createEEnum(COMPOSITE_FILTER_TYPE);

		// Create data types
		exceptionEDataType = createEDataType(EXCEPTION);
		listEDataType = createEDataType(LIST);
		sortedSetEDataType = createEDataType(SORTED_SET);
		eClassFilterEDataType = createEDataType(ECLASS_FILTER);
		numberEDataType = createEDataType(NUMBER);
		iFileEDataType = createEDataType(IFILE);
		resourceEDataType = createEDataType(RESOURCE);
		unitEDataType = createEDataType(UNIT);
		collectionEDataType = createEDataType(COLLECTION);
		eListEDataType = createEDataType(ELIST);
		uriEDataType = createEDataType(URI);
		jobEDataType = createEDataType(JOB);
		resourceSetEDataType = createEDataType(RESOURCE_SET);
		listNamedEDataType = createEDataType(LIST_NAMED);
		listFeatureEDataType = createEDataType(LIST_FEATURE);
		adapterEDataType = createEDataType(ADAPTER);
		notificationEDataType = createEDataType(NOTIFICATION);
		hashMapEDataType = createEDataType(HASH_MAP);
		eSelectionServiceEDataType = createEDataType(ESELECTION_SERVICE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters
		ETypeParameter iComparatorEClass_T = addETypeParameter(iComparatorEClass, "T");
		ETypeParameter eComparatorEClass_T = addETypeParameter(eComparatorEClass, "T");
		ETypeParameter compositeComparatorEClass_T = addETypeParameter(compositeComparatorEClass, "T");
		ETypeParameter eIdComparatorEClass_T = addETypeParameter(eIdComparatorEClass, "T");
		ETypeParameter timedComparatorEClass_T = addETypeParameter(timedComparatorEClass, "T");
		ETypeParameter namedComparatorEClass_T = addETypeParameter(namedComparatorEClass, "T");
		ETypeParameter iFilterEClass_T = addETypeParameter(iFilterEClass, "T");
		ETypeParameter compositeFilterEClass_T = addETypeParameter(compositeFilterEClass, "T");
		ETypeParameter timedBeforeFilterEClass_T = addETypeParameter(timedBeforeFilterEClass, "T");
		ETypeParameter timedAfterFilterEClass_T = addETypeParameter(timedAfterFilterEClass, "T");
		ETypeParameter timedCompositeFilterEClass_T = addETypeParameter(timedCompositeFilterEClass, "T");
		addETypeParameter(listEDataType, "T");
		addETypeParameter(sortedSetEDataType, "T");
		addETypeParameter(collectionEDataType, "T");
		addETypeParameter(eListEDataType, "EObject");
		addETypeParameter(hashMapEDataType, "key");
		addETypeParameter(hashMapEDataType, "value");

		// Set bounds for type parameters
		EGenericType g1 = createEGenericType(theEcorePackage.getEObject());
		eIdComparatorEClass_T.getEBounds().add(g1);
		g1 = createEGenericType(this.getTimed());
		timedComparatorEClass_T.getEBounds().add(g1);
		g1 = createEGenericType(this.getNamed());
		namedComparatorEClass_T.getEBounds().add(g1);
		g1 = createEGenericType(this.getTimed());
		timedBeforeFilterEClass_T.getEBounds().add(g1);
		g1 = createEGenericType(this.getTimed());
		timedAfterFilterEClass_T.getEBounds().add(g1);
		g1 = createEGenericType(this.getTimed());
		timedCompositeFilterEClass_T.getEBounds().add(g1);

		// Add supertypes to classes
		serverEClass.getESuperTypes().add(this.getStartable());
		timeSourceEClass.getESuperTypes().add(this.getNamed());
		timeSourceEClass.getESuperTypes().add(this.getDescribed());
		timeSourceEClass.getESuperTypes().add(this.getTimed());
		timeSourceEClass.getESuperTypes().add(this.getDisposable());
		fixedTimeSourceEClass.getESuperTypes().add(this.getTimeSource());
		currentTimeSourceEClass.getESuperTypes().add(this.getTimeSource());
		browseableTimeSourceEClass.getESuperTypes().add(this.getTimeSource());
		collectionTimedTimeSourceEClass.getESuperTypes().add(this.getBrowseableTimeSource());
		abstractFeatureListNodeEClass.getESuperTypes().add(this.getAbstractFeatureNode());
		abstractFeatureTreeNodeEClass.getESuperTypes().add(this.getAbstractFeatureNode());
		treeRootNodeEClass.getESuperTypes().add(this.getAbstractFeatureTreeNode());
		treeRootNodeEClass.getESuperTypes().add(this.getAbstractRootNode());
		treeFeatureNodeEClass.getESuperTypes().add(this.getAbstractFeatureTreeNode());
		treeFeatureNodeEClass.getESuperTypes().add(this.getAbstractFeatureSpecifier());
		listRootNodeEClass.getESuperTypes().add(this.getAbstractFeatureListNode());
		listRootNodeEClass.getESuperTypes().add(this.getAbstractRootNode());
		listFeatureNodeEClass.getESuperTypes().add(this.getAbstractFeatureListNode());
		listFeatureNodeEClass.getESuperTypes().add(this.getAbstractFeatureSpecifier());
		g1 = createEGenericType(this.getIComparator());
		EGenericType g2 = createEGenericType(eComparatorEClass_T);
		g1.getETypeArguments().add(g2);
		eComparatorEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getNamed());
		eComparatorEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getEComparator());
		g2 = createEGenericType(compositeComparatorEClass_T);
		g1.getETypeArguments().add(g2);
		compositeComparatorEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getEComparator());
		g2 = createEGenericType(eIdComparatorEClass_T);
		g1.getETypeArguments().add(g2);
		eIdComparatorEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getEComparator());
		g2 = createEGenericType(timedComparatorEClass_T);
		g1.getETypeArguments().add(g2);
		timedComparatorEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getEComparator());
		g2 = createEGenericType(namedComparatorEClass_T);
		g1.getETypeArguments().add(g2);
		namedComparatorEClass.getEGenericSuperTypes().add(g1);
		iFilterEClass.getESuperTypes().add(this.getNamed());
		g1 = createEGenericType(this.getIFilter());
		g2 = createEGenericType(compositeFilterEClass_T);
		g1.getETypeArguments().add(g2);
		compositeFilterEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getIFilter());
		g2 = createEGenericType(timedBeforeFilterEClass_T);
		g1.getETypeArguments().add(g2);
		timedBeforeFilterEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getIFilter());
		g2 = createEGenericType(timedAfterFilterEClass_T);
		g1.getETypeArguments().add(g2);
		timedAfterFilterEClass.getEGenericSuperTypes().add(g1);
		g1 = createEGenericType(this.getCompositeFilter());
		g2 = createEGenericType(timedCompositeFilterEClass_T);
		g1.getETypeArguments().add(g2);
		timedCompositeFilterEClass.getEGenericSuperTypes().add(g1);

		// Initialize classes, features, and operations; add parameters
		initEClass(apogyCommonEMFFacadeEClass, ApogyCommonEMFFacade.class, "ApogyCommonEMFFacade", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getApogyCommonEMFFacade_DateFormatString(), theEcorePackage.getEString(), "dateFormatString", "yyyy.MM.dd HH:mm:ss.SSS z", 0, 1, ApogyCommonEMFFacade.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getApogyCommonEMFFacade__GetAllAvailableEClasses(), null, "getAllAvailableEClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__GetAllSubEClasses__EClass(), null, "getAllSubEClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "superClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__FilterEClasses__List_EClassFilter(), null, "filterEClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "eClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEClassFilter(), "filter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__FilterEClasses__List_List(), null, "filterEClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "eClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getEClassFilter());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "filters", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__FindClosestMatch__EClass_List(), theEcorePackage.getEClass(), "findClosestMatch", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "eClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "eClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetEClass__String(), theEcorePackage.getEClass(), "getEClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "str", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__SortAlphabetically__List(), null, "sortAlphabetically", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "eClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSortedSet());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__GetAllAvailableEOperations__EClass(), null, "getAllAvailableEOperations", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "eClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEOperation());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__SortEOperationsAlphabetically__List(), null, "sortEOperationsAlphabetically", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEOperation());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "eOperations", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSortedSet());
		g2 = createEGenericType(theEcorePackage.getEOperation());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__GetDocumentation__ETypedElement(), theEcorePackage.getEString(), "getDocumentation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetDocumentation__EClass(), theEcorePackage.getEString(), "getDocumentation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "eClass", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetEngineeringUnitsAsString__ETypedElement(), theEcorePackage.getEString(), "getEngineeringUnitsAsString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetEngineeringUnits__ETypedElement(), this.getUnit(), "getEngineeringUnits", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetValueUpdateRate__ETypedElement(), theEcorePackage.getEDoubleObject(), "getValueUpdateRate", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetWarningOCLExpression__ETypedElement(), theEcorePackage.getEString(), "getWarningOCLExpression", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetAlarmOCLExpression__ETypedElement(), theEcorePackage.getEString(), "getAlarmOCLExpression", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetOutOfRangeOCLExpression__ETypedElement(), theEcorePackage.getEString(), "getOutOfRangeOCLExpression", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetWarningMinValue__ETypedElement(), this.getNumber(), "getWarningMinValue", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetWarningMaxValue__ETypedElement(), this.getNumber(), "getWarningMaxValue", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetAlarmMinValue__ETypedElement(), this.getNumber(), "getAlarmMinValue", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetAlarmMaxValue__ETypedElement(), this.getNumber(), "getAlarmMaxValue", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetOutOfRangeMinValue__ETypedElement(), this.getNumber(), "getOutOfRangeMinValue", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetOutOfRangeMaxValue__ETypedElement(), this.getNumber(), "getOutOfRangeMaxValue", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetRange__ETypedElement_Object(), this.getRanges(), "getRange", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetFullDescription__ETypedElement(), theEcorePackage.getEString(), "getFullDescription", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "eTypedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetAncestriesString__AbstractFeatureNode(), theEcorePackage.getEString(), "getAncestriesString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractFeatureNode(), "abstractFeatureNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetFeatureRoot__AbstractFeatureNode(), this.getAbstractRootNode(), "getFeatureRoot", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractFeatureNode(), "abstractFeatureNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetAncestries__AbstractFeatureNode(), null, "getAncestries", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractFeatureNode(), "abstractFeatureNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getAbstractFeatureNode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__GetDescendants__AbstractFeatureNode(), null, "getDescendants", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractFeatureNode(), "abstractFeatureNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(this.getAbstractFeatureNode());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__GetChildEStructuralFeatures__AbstractFeatureNode(), null, "getChildEStructuralFeatures", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractFeatureNode(), "abstractFeatureNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEStructuralFeature());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__Resolve__EObject_AbstractFeatureNode(), theEcorePackage.getEJavaObject(), "resolve", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "sourceObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractFeatureNode(), "abstractFeatureNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__SetValue__EObject_AbstractFeatureNode_Object(), null, "setValue", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "sourceObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractFeatureNode(), "abstractFeatureNode", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEJavaObject(), "value", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__IsResolved__EObject_AbstractFeatureNode(), theEcorePackage.getEBoolean(), "isResolved", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "sourceObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAbstractFeatureNode(), "abstractFeatureNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetLeaf__ListRootNode(), this.getAbstractFeatureListNode(), "getLeaf", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getListRootNode(), "listRootNode", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetFile__Resource(), this.getIFile(), "getFile", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetContent__URI(), null, "getContent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getURI(), "uri", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getEList());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__SortTimed__Collection(), null, "sortTimed", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getCollection());
		g2 = createEGenericType(this.getTimed());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "timedCollection", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getSortedSet());
		g2 = createEGenericType(this.getTimed());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__GetTimeSpan__Collection(), theEcorePackage.getEDouble(), "getTimeSpan", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getCollection());
		g2 = createEGenericType(this.getTimed());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "timedCollection", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetID__EObject(), theEcorePackage.getEString(), "getID", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetEObjectById__ResourceSet_String(), theEcorePackage.getEObject(), "getEObjectById", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResourceSet(), "resourceSet", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "id", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetEObjectsByType__EObject_EClass(), null, "getEObjectsByType", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "rootContainer", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "eClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEObject());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__GetDefaultName__EObject_EObject_ETypedElement(), theEcorePackage.getEString(), "getDefaultName", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eContainer", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getETypedElement(), "typedElement", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__GetChildEClasses__EClass(), null, "getChildEClasses", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEClass(), "parentEClass", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getList());
		g2 = createEGenericType(theEcorePackage.getEClass());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__GetSettableEReferences__EObject(), null, "getSettableEReferences", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getEList());
		g2 = createEGenericType(theEcorePackage.getEReference());
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		op = initEOperation(getApogyCommonEMFFacade__ToString__List_String(), theEcorePackage.getEString(), "toString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getListNamed(), "nameds", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "separator", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__Format__Date(), theEcorePackage.getEString(), "format", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEDate(), "date", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__StopAllStartables__EObject(), null, "stopAllStartables", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__SerializeEObject__EObject_String(), theEcorePackage.getEString(), "serializeEObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "eObject", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "uriID", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getApogyCommonEMFFacade__DeserializeString__String_String(), theEcorePackage.getEObject(), "deserializeString", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "str", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEString(), "uriID", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(eObjectReferenceEClass, EObjectReference.class, "EObjectReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEObjectReference_EObject(), theEcorePackage.getEObject(), null, "eObject", null, 0, 1, EObjectReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(namedEClass, Named.class, "Named", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamed_Name(), theEcorePackage.getEString(), "name", null, 0, 1, Named.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(describedEClass, Described.class, "Described", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDescribed_Description(), theEcorePackage.getEString(), "description", null, 0, 1, Described.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedEClass, Timed.class, "Timed", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimed_Time(), theEcorePackage.getEDate(), "time", null, 0, 1, Timed.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serverEClass, Server.class, "Server", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServer_ServerJob(), this.getJob(), "serverJob", null, 0, 1, Server.class, IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(startableEClass, Startable.class, "Startable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStartable_Started(), theEcorePackage.getEBoolean(), "started", "false", 0, 1, Startable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(durationEClass, Duration.class, "Duration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDuration_Value(), theEcorePackage.getELong(), "value", null, 0, 1, Duration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDuration_Days(), theEcorePackage.getEByte(), "days", null, 0, 1, Duration.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDuration_Hours(), theEcorePackage.getEByte(), "hours", null, 0, 1, Duration.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDuration_Minutes(), theEcorePackage.getEByte(), "minutes", null, 0, 1, Duration.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDuration_Seconds(), theEcorePackage.getEByte(), "seconds", null, 0, 1, Duration.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getDuration_Milliseconds(), theEcorePackage.getEInt(), "milliseconds", null, 0, 1, Duration.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(getDuration__GetDuration__Timed_Timed(), this.getDuration(), "getDuration", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTimed(), "firstEvent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTimed(), "secondEvent", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(timeSourceEClass, TimeSource.class, "TimeSource", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimeSource_Offset(), theEcorePackage.getEInt(), "offset", "0", 0, 1, TimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fixedTimeSourceEClass, FixedTimeSource.class, "FixedTimeSource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(currentTimeSourceEClass, CurrentTimeSource.class, "CurrentTimeSource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCurrentTimeSource_UpdatePeriod(), theEcorePackage.getEInt(), "updatePeriod", "1000", 0, 1, CurrentTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCurrentTimeSource_Paused(), theEcorePackage.getEBoolean(), "paused", "false", 0, 1, CurrentTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getCurrentTimeSource__Pause(), null, "pause", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getCurrentTimeSource__Resume(), null, "resume", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(browseableTimeSourceEClass, BrowseableTimeSource.class, "BrowseableTimeSource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBrowseableTimeSource_StartTime(), theEcorePackage.getEDate(), "startTime", null, 0, 1, BrowseableTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBrowseableTimeSource_UpdatePeriod(), theEcorePackage.getEInt(), "updatePeriod", "1000", 0, 1, BrowseableTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBrowseableTimeSource_TimeAcceration(), theEcorePackage.getEFloat(), "timeAcceration", "10.0", 0, 1, BrowseableTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBrowseableTimeSource_TimeDirection(), this.getTimeDirection(), "timeDirection", null, 0, 1, BrowseableTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getBrowseableTimeSource__PlayForward(), null, "playForward", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getBrowseableTimeSource__PlayReverse(), null, "playReverse", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getBrowseableTimeSource__Pause(), null, "pause", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getBrowseableTimeSource__Reset(), null, "reset", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(collectionTimedTimeSourceEClass, CollectionTimedTimeSource.class, "CollectionTimedTimeSource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCollectionTimedTimeSource_LoopEnable(), theEcorePackage.getEBoolean(), "loopEnable", "false", 0, 1, CollectionTimedTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCollectionTimedTimeSource_TimedsList(), this.getTimed(), null, "timedsList", null, 0, -1, CollectionTimedTimeSource.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCollectionTimedTimeSource_CurrentTimedElement(), this.getTimed(), null, "currentTimedElement", null, 0, 1, CollectionTimedTimeSource.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCollectionTimedTimeSource_EarliestDate(), theEcorePackage.getEDate(), "earliestDate", null, 0, 1, CollectionTimedTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCollectionTimedTimeSource_LatestDate(), theEcorePackage.getEDate(), "latestDate", null, 0, 1, CollectionTimedTimeSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getCollectionTimedTimeSource__JumpToNext(), null, "jumpToNext", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getCollectionTimedTimeSource__JumpToPrevious(), null, "jumpToPrevious", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(disposableEClass, Disposable.class, "Disposable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getDisposable__Dispose(), null, "dispose", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(featureNodeAdapterEClass, FeatureNodeAdapter.class, "FeatureNodeAdapter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureNodeAdapter_SourceObject(), theEcorePackage.getEObject(), null, "sourceObject", null, 0, 1, FeatureNodeAdapter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureNodeAdapter_FeatureNode(), this.getAbstractFeatureNode(), null, "featureNode", null, 0, 1, FeatureNodeAdapter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureNodeAdapter_CurrentValue(), theEcorePackage.getEJavaObject(), "currentValue", null, 0, 1, FeatureNodeAdapter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeatureNodeAdapter_Resolved(), theEcorePackage.getEBoolean(), "resolved", "false", 0, 1, FeatureNodeAdapter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractFeatureNodeEClass, AbstractFeatureNode.class, "AbstractFeatureNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractFeatureListNodeEClass, AbstractFeatureListNode.class, "AbstractFeatureListNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractFeatureListNode_Parent(), this.getAbstractFeatureListNode(), this.getAbstractFeatureListNode_Child(), "parent", null, 0, 1, AbstractFeatureListNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractFeatureListNode_Child(), this.getAbstractFeatureListNode(), this.getAbstractFeatureListNode_Parent(), "child", null, 0, 1, AbstractFeatureListNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractFeatureTreeNodeEClass, AbstractFeatureTreeNode.class, "AbstractFeatureTreeNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractFeatureTreeNode_Parent(), this.getAbstractFeatureTreeNode(), this.getAbstractFeatureTreeNode_Children(), "parent", null, 0, 1, AbstractFeatureTreeNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAbstractFeatureTreeNode_Children(), this.getAbstractFeatureTreeNode(), this.getAbstractFeatureTreeNode_Parent(), "children", null, 0, -1, AbstractFeatureTreeNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractRootNodeEClass, AbstractRootNode.class, "AbstractRootNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractRootNode_SourceClass(), theEcorePackage.getEClass(), null, "sourceClass", null, 1, 1, AbstractRootNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(treeRootNodeEClass, TreeRootNode.class, "TreeRootNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(treeFeatureNodeEClass, TreeFeatureNode.class, "TreeFeatureNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(listRootNodeEClass, ListRootNode.class, "ListRootNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(listFeatureNodeEClass, ListFeatureNode.class, "ListFeatureNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(abstractFeatureSpecifierEClass, AbstractFeatureSpecifier.class, "AbstractFeatureSpecifier", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAbstractFeatureSpecifier_StructuralFeature(), theEcorePackage.getEStructuralFeature(), null, "structuralFeature", null, 1, 1, AbstractFeatureSpecifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractFeatureSpecifier_MultiValued(), theEcorePackage.getEBoolean(), "multiValued", "false", 0, 1, AbstractFeatureSpecifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractFeatureSpecifier_Index(), theEcorePackage.getEInt(), "index", "0", 0, 1, AbstractFeatureSpecifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featurePathAdapterEClass, FeaturePathAdapter.class, "FeaturePathAdapter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getFeaturePathAdapter__Init__EObject(), null, "init", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theEcorePackage.getEObject(), "root", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getFeaturePathAdapter__Dispose(), null, "dispose", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getFeaturePathAdapter__GetFeaturePath(), this.getListFeature(), "getFeaturePath", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getFeaturePathAdapter__NotifyChanged__Notification(), null, "notifyChanged", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getNotification(), "msg", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(featurePathAdapterEntryEClass, FeaturePathAdapterEntry.class, "FeaturePathAdapterEntry", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeaturePathAdapterEntry_Notifier(), theEcorePackage.getEObject(), null, "notifier", null, 0, 1, FeaturePathAdapterEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeaturePathAdapterEntry_Feature(), theEcorePackage.getEStructuralFeature(), null, "feature", null, 0, 1, FeaturePathAdapterEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFeaturePathAdapterEntry_Adapter(), this.getAdapter(), "adapter", null, 0, 1, FeaturePathAdapterEntry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iComparatorEClass, Comparator.class, "IComparator", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIComparator__Compare__Object_Object(), theEcorePackage.getEInt(), "compare", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(iComparatorEClass_T);
		addEParameter(op, g1, "o1", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(iComparatorEClass_T);
		addEParameter(op, g1, "o2", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(eComparatorEClass, EComparator.class, "EComparator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(compositeComparatorEClass, CompositeComparator.class, "CompositeComparator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(this.getIComparator());
		g2 = createEGenericType(compositeComparatorEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(getCompositeComparator_Comparators(), g1, null, "comparators", null, 1, -1, CompositeComparator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eIdComparatorEClass, EIdComparator.class, "EIdComparator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(timedComparatorEClass, TimedComparator.class, "TimedComparator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(namedComparatorEClass, NamedComparator.class, "NamedComparator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(iFilterEClass, IFilter.class, "IFilter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIFilter__Matches__Object(), theEcorePackage.getEBoolean(), "matches", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(iFilterEClass_T);
		addEParameter(op, g1, "object", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getIFilter__Filter__Collection(), null, "filter", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getCollection());
		g2 = createEGenericType(iFilterEClass_T);
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "objects", 0, 1, !IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(this.getCollection());
		g2 = createEGenericType(iFilterEClass_T);
		g1.getETypeArguments().add(g2);
		initEOperation(op, g1);

		initEClass(compositeFilterEClass, CompositeFilter.class, "CompositeFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCompositeFilter_FilterChainType(), this.getCompositeFilterType(), "filterChainType", "AND", 0, 1, CompositeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getIFilter());
		g2 = createEGenericType(compositeFilterEClass_T);
		g1.getETypeArguments().add(g2);
		initEReference(getCompositeFilter_Filters(), g1, null, "filters", null, 0, -1, CompositeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedBeforeFilterEClass, TimedBeforeFilter.class, "TimedBeforeFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimedBeforeFilter_Inclusive(), theEcorePackage.getEBoolean(), "inclusive", "true", 0, 1, TimedBeforeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedBeforeFilter_BeforeDate(), theEcorePackage.getEDate(), "beforeDate", null, 0, 1, TimedBeforeFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedAfterFilterEClass, TimedAfterFilter.class, "TimedAfterFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimedAfterFilter_Inclusive(), theEcorePackage.getEBoolean(), "inclusive", "true", 0, 1, TimedAfterFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimedAfterFilter_AfterDate(), theEcorePackage.getEDate(), "afterDate", null, 0, 1, TimedAfterFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timedCompositeFilterEClass, TimedCompositeFilter.class, "TimedCompositeFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(timeDirectionEEnum, TimeDirection.class, "TimeDirection");
		addEEnumLiteral(timeDirectionEEnum, TimeDirection.FORWARD);
		addEEnumLiteral(timeDirectionEEnum, TimeDirection.REVERSE);

		initEEnum(rangesEEnum, Ranges.class, "Ranges");
		addEEnumLiteral(rangesEEnum, Ranges.UNKNOWN);
		addEEnumLiteral(rangesEEnum, Ranges.NOMINAL);
		addEEnumLiteral(rangesEEnum, Ranges.WARNING);
		addEEnumLiteral(rangesEEnum, Ranges.ALARM);
		addEEnumLiteral(rangesEEnum, Ranges.OUT_OF_RANGE);

		initEEnum(compositeFilterTypeEEnum, CompositeFilterType.class, "CompositeFilterType");
		addEEnumLiteral(compositeFilterTypeEEnum, CompositeFilterType.AND);
		addEEnumLiteral(compositeFilterTypeEEnum, CompositeFilterType.OR);

		// Initialize data types
		initEDataType(exceptionEDataType, Exception.class, "Exception", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listEDataType, List.class, "List", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(sortedSetEDataType, SortedSet.class, "SortedSet", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eClassFilterEDataType, EClassFilter.class, "EClassFilter", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(numberEDataType, Number.class, "Number", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(iFileEDataType, IFile.class, "IFile", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(resourceEDataType, Resource.class, "Resource", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(unitEDataType, Unit.class, "Unit", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "javax.measure.unit.Unit<?>");
		initEDataType(collectionEDataType, Collection.class, "Collection", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eListEDataType, EList.class, "EList", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(uriEDataType, org.eclipse.emf.common.util.URI.class, "URI", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(jobEDataType, Job.class, "Job", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(resourceSetEDataType, ResourceSet.class, "ResourceSet", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(listNamedEDataType, List.class, "ListNamed", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<? extends ca.gc.asc_csa.apogy.common.emf.Named>");
		initEDataType(listFeatureEDataType, List.class, "ListFeature", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS, "java.util.List<? extends org.eclipse.emf.ecore.EStructuralFeature>");
		initEDataType(adapterEDataType, Adapter.class, "Adapter", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(notificationEDataType, Notification.class, "Notification", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(hashMapEDataType, HashMap.class, "HashMap", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(eSelectionServiceEDataType, ESelectionService.class, "ESelectionService", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/GenModel
		createGenModelAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/GenModel</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGenModelAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/GenModel";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "documentation", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca),\n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n\t   Olivier L. Larouche (Olivier.LLarouche@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "prefix", "ApogyCommonEMF",
			 "copyrightText", "Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).\nAll rights reserved. This program and the accompanying materials\nare made available under the terms of the Eclipse Public License v1.0\nwhich accompanies this distribution, and is available at\nhttp://www.eclipse.org/legal/epl-v10.html\n\nContributors:\n    Pierre Allard (Pierre.Allard@canada.ca), \n    Regent L\'Archeveque (Regent.Larcheveque@canada.ca),\n    Sebastien Gemme (Sebastien.Gemme@canada.ca),\n\tOlivier L. Larouche (Olivier.LLarouche@canada.ca),\n    Canadian Space Agency (CSA) - Initial API and implementation",
			 "childCreationExtenders", "true",
			 "suppressGenModelAnnotations", "false",
			 "extensibleProviderFactory", "true",
			 "modelName", "ApogyCommonEMF",
			 "modelDirectory", "/ca.gc.asc_csa.apogy.common.emf/src-generated",
			 "editDirectory", "/ca.gc.asc_csa.apogy.common.emf.edit/src-generated",
			 "basePackage", "ca.gc.asc_csa.apogy.common"
		   });	
		addAnnotation
		  (exceptionEDataType, 
		   source, 
		   new String[] {
			 "documentation", " ------------------------------------------------------------------------------------------\n\nDataTypes.\n\n------------------------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (apogyCommonEMFFacadeEClass, 
		   source, 
		   new String[] {
			 "documentation", " ------------------------------------------------------------------------------------------\n\nFacade & Utilities.\n\n------------------------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetAllAvailableEClasses(), 
		   source, 
		   new String[] {
			 "documentation", "Returns all available EClass currently installed."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetAllSubEClasses__EClass(), 
		   source, 
		   new String[] {
			 "documentation", "Returns all the sub classes of the specified super class.  The result list contains\nonly implementation classes (not abstract).\n@param superClass Super type of the classes to be found.\n@return List of sub classes."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__FindClosestMatch__EClass_List(), 
		   source, 
		   new String[] {
			 "documentation", "Finds the closest match in a {@link List} of {@link EClass}.\n@param eClass reference to the EClass to find the closest match.\n@param eClasses list of EClasses to search.\n@return {@link EClass} of the closest super class of the specified EClass."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetEClass__String(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the {@link EClass} that fits the specified fully qualified name.\n@param str Fully qualified name.\n@return Reference to the class or null there is no match."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetDocumentation__ETypedElement(), 
		   source, 
		   new String[] {
			 "documentation", "Documentation tags utility methods."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetEngineeringUnits__ETypedElement(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the Unit defined for a specified eTypedElement.\n@param eTypedElement The specified ETypedElement.\n@return the Unit, or null if none found."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetAncestriesString__AbstractFeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "Feature Tree Methods."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetFeatureRoot__AbstractFeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "Method that return the root of the tree for a\nspecified AbstractFeatureNode."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetAncestries__AbstractFeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "Method that return the list of ancestor of a\nspecified AbstractFeatureNode, from FeatureRoot to\n(and including) the specified AbstractFeatureNode."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetDescendants__AbstractFeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "Method that return the list of descendants of a\nspecified AbstractFeatureNode, excluding the specified\nAbstractFeatureNode."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetChildEStructuralFeatures__AbstractFeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "Method that return the list of available child\nEStructuralFeature type for a given Node."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__Resolve__EObject_AbstractFeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "Method that resolve the value of a FeatureNode\nfor a specified source EObjet."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__IsResolved__EObject_AbstractFeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "Method that tells whether or not the\nFeatureNode is currently accessible for\nthe specified source EObject."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetFile__Resource(), 
		   source, 
		   new String[] {
			 "documentation", "Resource Support."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetContent__URI(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the content contained in the resource.\n@param uri Resource uri.\n@return Reference to the content."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__SortTimed__Collection(), 
		   source, 
		   new String[] {
			 "documentation", "Timed Support"
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetTimeSpan__Collection(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the time span of a collection\nof Timed elements.",
			 "apogy_units", "s"
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetID__EObject(), 
		   source, 
		   new String[] {
			 "documentation", "Return an EObject unique ID.\n@param eObject The EObject.\n@return the unique ID, null if none is found."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetEObjectById__ResourceSet_String(), 
		   source, 
		   new String[] {
			 "documentation", "Searches the ResourceSet for an EObject with the specified id.\n@param resourceSet Reference to the resourceSet\n@param id Object identifier."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetEObjectsByType__EObject_EClass(), 
		   source, 
		   new String[] {
			 "documentation", "Searches the content of a specified EObject for children that are of type or sub type of a EClass.\n@param rootContainer The root EObject to search from.\n@param eClass The EClass.\n@return The list of EObject."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetDefaultName__EObject_EObject_ETypedElement(), 
		   source, 
		   new String[] {
			 "documentation", "Generates and returns a default name for a specific feature container.\n@param eContainer The instance of the container.\n@param eObject The instance of the object to name.\n@param typedElement The typed element containing or referencing the EObject.\n@return A unique name."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetChildEClasses__EClass(), 
		   source, 
		   new String[] {
			 "documentation", "Returns all the child classes of the specified parent class. The result list contains\nonly implementation classes (not abstract).\n@param parentEClass Type of the class to find child classes.\n@return List of child classes."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__GetSettableEReferences__EObject(), 
		   source, 
		   new String[] {
			 "documentation", "Returns a list of all the object\'s child EReferences that are either empty or a list\n@param eObject The parent object\n@return List of EReferences that are either empty or a list"
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__ToString__List_String(), 
		   source, 
		   new String[] {
			 "documentation", "Returns the string representation of the list of {@link Named}.\n@param nameds List of named.\n@param separator String used to separate the nameds.\n@return Reference to the string representation."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__Format__Date(), 
		   source, 
		   new String[] {
			 "documentation", "Formats a specified Date as string. Uses the format defined in attribute dateFormatString.\n@param date The specified date.\n@return A string representation of the specified date."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__StopAllStartables__EObject(), 
		   source, 
		   new String[] {
			 "documentation", "Searches for {@link Startable} in the containment hierarchy of an {@link EObject} and stops them if they are running.\n@param root"
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__SerializeEObject__EObject_String(), 
		   source, 
		   new String[] {
			 "documentation", "Serializes an {@link EObject} to obtain a {@link String}.\nThis methods uses a copy of the {@link EObject}, so it will not be removed for it\'s ResourceSet.\n\n@param eObject reference to the EObject to serialize.\n@param id id to use as id for the serialized resource. Cannot be null.\n@return The EObject serialized."
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade__DeserializeString__String_String(), 
		   source, 
		   new String[] {
			 "documentation", "Deserializes a {@link String} to obtain an {@link EObject}.\n\n@param eObject reference to the EObject to serialize.\n@param id id to use as id for the serialized resource. Cannot be null.\n@return The EObject"
		   });	
		addAnnotation
		  (getApogyCommonEMFFacade_DateFormatString(), 
		   source, 
		   new String[] {
			 "documentation", "The date format string used for representing date as string."
		   });	
		addAnnotation
		  (eObjectReferenceEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nReference to an {@link EObject}."
		   });	
		addAnnotation
		  (getEObjectReference_EObject(), 
		   source, 
		   new String[] {
			 "children", "true"
		   });	
		addAnnotation
		  (namedEClass, 
		   source, 
		   new String[] {
			 "documentation", "Named Element."
		   });	
		addAnnotation
		  (describedEClass, 
		   source, 
		   new String[] {
			 "documentation", "Described Element."
		   });	
		addAnnotation
		  (timedEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nTimed.\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (durationEClass, 
		   source, 
		   new String[] {
			 "documentation", "This class handles the concept of time duration.  It provides methods to process duration\nbetween two {@link Timed} event.  It provides methods to express the duration in years, months,\ndays, minutes, seconds and milliseconds."
		   });	
		addAnnotation
		  (getDuration__GetDuration__Timed_Timed(), 
		   source, 
		   new String[] {
			 "documentation", "Creates and returns the duration between the {@link firstEvent} and the {@link secondEvent}.\n@param firstEvent First {@link Timed} event.\n@param secondEvent Second {@link Timed} event.\n@return Duration instance."
		   });	
		addAnnotation
		  (getDuration_Value(), 
		   source, 
		   new String[] {
			 "documentation", " Duration value in ms."
		   });	
		addAnnotation
		  (getDuration_Days(), 
		   source, 
		   new String[] {
			 "documentation", " Number of days."
		   });	
		addAnnotation
		  (getDuration_Hours(), 
		   source, 
		   new String[] {
			 "documentation", " Number of hours."
		   });	
		addAnnotation
		  (getDuration_Minutes(), 
		   source, 
		   new String[] {
			 "documentation", " Number of minutes."
		   });	
		addAnnotation
		  (getDuration_Seconds(), 
		   source, 
		   new String[] {
			 "documentation", " Number of seconds."
		   });	
		addAnnotation
		  (getDuration_Milliseconds(), 
		   source, 
		   new String[] {
			 "documentation", " Number of milliseconds."
		   });	
		addAnnotation
		  (timeSourceEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\n\nTime Sources.\n\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getTimeSource_Offset(), 
		   source, 
		   new String[] {
			 "documentation", "*\nOffset in milliseconds",
			 "apogy_units", "ms",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (fixedTimeSourceEClass, 
		   source, 
		   new String[] {
			 "documentation", "Time source that provides a fixed time."
		   });	
		addAnnotation
		  (currentTimeSourceEClass, 
		   source, 
		   new String[] {
			 "documentation", "Time source that provides the current time."
		   });	
		addAnnotation
		  (getCurrentTimeSource__Pause(), 
		   source, 
		   new String[] {
			 "documentation", "Pause the time."
		   });	
		addAnnotation
		  (getCurrentTimeSource__Resume(), 
		   source, 
		   new String[] {
			 "documentation", "Resume time update."
		   });	
		addAnnotation
		  (getCurrentTimeSource_UpdatePeriod(), 
		   source, 
		   new String[] {
			 "documentation", "The update period of the time, in milliseconds",
			 "apogy_units", "ms",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getCurrentTimeSource_Paused(), 
		   source, 
		   new String[] {
			 "documentation", "Whether or not the time source has been paused.",
			 "notify", "true",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (browseableTimeSourceEClass, 
		   source, 
		   new String[] {
			 "documentation", "*\nTime source that provides an accelerated time."
		   });	
		addAnnotation
		  (getBrowseableTimeSource__PlayForward(), 
		   source, 
		   new String[] {
			 "documentation", "Play time forward."
		   });	
		addAnnotation
		  (getBrowseableTimeSource__PlayReverse(), 
		   source, 
		   new String[] {
			 "documentation", "Play time in reverse."
		   });	
		addAnnotation
		  (getBrowseableTimeSource__Pause(), 
		   source, 
		   new String[] {
			 "documentation", "Pause time."
		   });	
		addAnnotation
		  (getBrowseableTimeSource__Reset(), 
		   source, 
		   new String[] {
			 "documentation", "Reset the time to the start time"
		   });	
		addAnnotation
		  (getBrowseableTimeSource_StartTime(), 
		   source, 
		   new String[] {
			 "documentation", "The start time. Default to current\ntime upon creation.",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getBrowseableTimeSource_UpdatePeriod(), 
		   source, 
		   new String[] {
			 "documentation", "The update period of the time.",
			 "apogy_units", "ms",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getBrowseableTimeSource_TimeAcceration(), 
		   source, 
		   new String[] {
			 "documentation", "The time acceleration.",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (getBrowseableTimeSource_TimeDirection(), 
		   source, 
		   new String[] {
			 "documentation", "The time direction.",
			 "notify", "true",
			 "property", "Editable"
		   });	
		addAnnotation
		  (collectionTimedTimeSourceEClass, 
		   source, 
		   new String[] {
			 "documentation", "Time source that is based on a list of Timed elements."
		   });	
		addAnnotation
		  (getCollectionTimedTimeSource__JumpToNext(), 
		   source, 
		   new String[] {
			 "documentation", "*\nJumps to the next Timed element."
		   });	
		addAnnotation
		  (getCollectionTimedTimeSource__JumpToPrevious(), 
		   source, 
		   new String[] {
			 "documentation", "*\n Jumps to the previous Timed element."
		   });	
		addAnnotation
		  (getCollectionTimedTimeSource_LoopEnable(), 
		   source, 
		   new String[] {
			 "documentation", "Whether or not to enable time looping when end of list of time is reached."
		   });	
		addAnnotation
		  (getCollectionTimedTimeSource_TimedsList(), 
		   source, 
		   new String[] {
			 "documentation", "The collection of Timed elements.",
			 "property", "None"
		   });	
		addAnnotation
		  (getCollectionTimedTimeSource_CurrentTimedElement(), 
		   source, 
		   new String[] {
			 "documentation", "The current Timed element.",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getCollectionTimedTimeSource_EarliestDate(), 
		   source, 
		   new String[] {
			 "documentation", "The earliest date found in the collection of Timed.",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getCollectionTimedTimeSource_LatestDate(), 
		   source, 
		   new String[] {
			 "documentation", "The latest date found in the collection of Timed.",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (disposableEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nDisposable.\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (rangesEEnum, 
		   source, 
		   new String[] {
			 "documentation", "*\nRanges Definition"
		   });	
		addAnnotation
		  (featureNodeAdapterEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nFeature Tree\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getFeatureNodeAdapter_SourceObject(), 
		   source, 
		   new String[] {
			 "documentation", "The object onto which to apply the FeatureNode."
		   });	
		addAnnotation
		  (getFeatureNodeAdapter_FeatureNode(), 
		   source, 
		   new String[] {
			 "documentation", "The FeatureNode defining which feature to monitor."
		   });	
		addAnnotation
		  (getFeatureNodeAdapter_Resolved(), 
		   source, 
		   new String[] {
			 "documentation", "Tells whether or not the featureNode is currently resolved."
		   });	
		addAnnotation
		  (abstractFeatureNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "Base class of all Feature Nodes."
		   });	
		addAnnotation
		  (abstractFeatureListNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "Defines an AbstractFeatureNode that is part of a List."
		   });	
		addAnnotation
		  (getAbstractFeatureListNode_Parent(), 
		   source, 
		   new String[] {
			 "documentation", "The parent node of this ListNode.",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getAbstractFeatureListNode_Child(), 
		   source, 
		   new String[] {
			 "documentation", "The child node of this ListNode.",
			 "property", "None"
		   });	
		addAnnotation
		  (abstractFeatureTreeNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "Defines an AbstractFeatureNode that is part of a Tree."
		   });	
		addAnnotation
		  (getAbstractFeatureTreeNode_Parent(), 
		   source, 
		   new String[] {
			 "documentation", "The parent node of this TreeNode.",
			 "property", "Readonly"
		   });	
		addAnnotation
		  (getAbstractFeatureTreeNode_Children(), 
		   source, 
		   new String[] {
			 "documentation", "The children node(s) of this TreeNode.",
			 "property", "None"
		   });	
		addAnnotation
		  (abstractRootNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "Defines an AbstractFeatureNode that is a root."
		   });	
		addAnnotation
		  (getAbstractRootNode_SourceClass(), 
		   source, 
		   new String[] {
			 "documentation", "The source class.",
			 "notify", "true"
		   });	
		addAnnotation
		  (treeRootNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "Defines an AbstractFeatureNode that is the root of a Tree."
		   });	
		addAnnotation
		  (treeFeatureNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "Defines an AbstractFeatureNode that is a node in a Tree."
		   });	
		addAnnotation
		  (listRootNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "Defines an AbstractFeatureNode that is the root of a List."
		   });	
		addAnnotation
		  (listFeatureNodeEClass, 
		   source, 
		   new String[] {
			 "documentation", "Defines an AbstractFeatureNode that is a node in a List."
		   });	
		addAnnotation
		  (abstractFeatureSpecifierEClass, 
		   source, 
		   new String[] {
			 "documentation", "Class that describes a StructuralFeature of interest."
		   });	
		addAnnotation
		  (getAbstractFeatureSpecifier_StructuralFeature(), 
		   source, 
		   new String[] {
			 "documentation", "The EStructuralFeature that is referred by this FeatureSpecifier.",
			 "notify", "true"
		   });	
		addAnnotation
		  (getAbstractFeatureSpecifier_MultiValued(), 
		   source, 
		   new String[] {
			 "documentation", "Whether or not this FeatureSpecifier refers to a multi-valued feature.",
			 "notify", "true"
		   });	
		addAnnotation
		  (getAbstractFeatureSpecifier_Index(), 
		   source, 
		   new String[] {
			 "documentation", "Index of the value if the EStructuralFeature referred\nto by this FeatureSpecifier if multi-valued.",
			 "notify", "true"
		   });	
		addAnnotation
		  (featurePathAdapterEClass, 
		   source, 
		   new String[] {
			 "documentation", "-------------------------------------------------------------------------\nFeaturePathAdapter\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getFeaturePathAdapter__Init__EObject(), 
		   source, 
		   new String[] {
			 "documentation", "Needs to be called to initialize the adapters.\n@param root first {@link EObject} to add {@link Adapter} to."
		   });	
		addAnnotation
		  (getFeaturePathAdapter__Dispose(), 
		   source, 
		   new String[] {
			 "documentation", "Removes all the adapters."
		   });	
		addAnnotation
		  (getFeaturePathAdapter__GetFeaturePath(), 
		   source, 
		   new String[] {
			 "documentation", "Is called to initialize the features to listen with the adapters that will be created.\nThis list needs to be in the order of containment starting with the root."
		   });	
		addAnnotation
		  (getFeaturePathAdapter__NotifyChanged__Notification(), 
		   source, 
		   new String[] {
			 "documentation", "Is called when an object in the featurePath has a notifyChanged called on a feature in the featurePath."
		   });	
		addAnnotation
		  (featurePathAdapterEntryEClass, 
		   source, 
		   new String[] {
			 "documentation", "Class used to have a map with 3 data."
		   });	
		addAnnotation
		  (iComparatorEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nComparators\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getIComparator__Compare__Object_Object(), 
		   source, 
		   new String[] {
			 "documentation", "Compares two objects.\n@see java.util.Comparator"
		   });	
		addAnnotation
		  (eComparatorEClass, 
		   source, 
		   new String[] {
			 "documentation", "Base class of all Comparators."
		   });	
		addAnnotation
		  (compositeComparatorEClass, 
		   source, 
		   new String[] {
			 "documentation", "Defines a composite comparator that uses a list of Comparator to compare two objects.\nThis comparator compare(T o1, T o2) method iterates over it list of comparator until\none that does not return equality is found."
		   });	
		addAnnotation
		  (getCompositeComparator_Comparators(), 
		   source, 
		   new String[] {
			 "documentation", "The list of comparator used."
		   });	
		addAnnotation
		  (eIdComparatorEClass, 
		   source, 
		   new String[] {
			 "documentation", "Comparator that compares EObject based on their generated Id."
		   });	
		addAnnotation
		  (timedComparatorEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nComparators\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (namedComparatorEClass, 
		   source, 
		   new String[] {
			 "documentation", "Comparator for Named."
		   });	
		addAnnotation
		  (iFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nFilters\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getIFilter__Matches__Object(), 
		   source, 
		   new String[] {
			 "documentation", "Determines whether or not a specified object should pass through the filter.\n@param object The specified object.\n@return True if the object satisfies the filter conditions, false otherwise."
		   });	
		addAnnotation
		  (getIFilter__Filter__Collection(), 
		   source, 
		   new String[] {
			 "documentation", "Filters a list of objects\n@param objects The list of object to filter.\n@return The list of objects that passed the filter. Never null, but can be empty."
		   });	
		addAnnotation
		  (compositeFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "A specialization of Filter that makes use of a list of Filter.\nIf the list of filter is empty, matches(T object) always returns true."
		   });	
		addAnnotation
		  (getCompositeFilter_FilterChainType(), 
		   source, 
		   new String[] {
			 "documentation", "The type of chain."
		   });	
		addAnnotation
		  (getCompositeFilter_Filters(), 
		   source, 
		   new String[] {
			 "documentation", "The list of filter used."
		   });	
		addAnnotation
		  (compositeFilterTypeEEnum, 
		   source, 
		   new String[] {
			 "documentation", "*\nThe types of FilterChain."
		   });	
		addAnnotation
		  (compositeFilterTypeEEnum.getELiterals().get(0), 
		   source, 
		   new String[] {
			 "documentation", "*\nAll filter must matches for the object to pass through."
		   });	
		addAnnotation
		  (compositeFilterTypeEEnum.getELiterals().get(1), 
		   source, 
		   new String[] {
			 "documentation", "*\nAt least one filter must matches for the object to pass through."
		   });	
		addAnnotation
		  (timedBeforeFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", " -------------------------------------------------------------------------\nTimed Filters\n-------------------------------------------------------------------------"
		   });	
		addAnnotation
		  (getTimedBeforeFilter_Inclusive(), 
		   source, 
		   new String[] {
			 "documentation", "Whether or not a Timed with perfect match to beforeDate should be allowed thru."
		   });	
		addAnnotation
		  (getTimedBeforeFilter_BeforeDate(), 
		   source, 
		   new String[] {
			 "notify", "true"
		   });	
		addAnnotation
		  (timedAfterFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", " A Filter that matches dates after a specified date."
		   });	
		addAnnotation
		  (getTimedAfterFilter_Inclusive(), 
		   source, 
		   new String[] {
			 "documentation", "Whether or not a Timed with perfect match to afterDate should be allowed thru."
		   });	
		addAnnotation
		  (getTimedAfterFilter_AfterDate(), 
		   source, 
		   new String[] {
			 "notify", "true"
		   });	
		addAnnotation
		  (timedCompositeFilterEClass, 
		   source, 
		   new String[] {
			 "documentation", "A composite Filter for Timed."
		   });
	}

} //ApogyCommonEMFPackageImpl
