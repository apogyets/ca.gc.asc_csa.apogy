/**
 * Copyright (c) 2016 Canadian Space Agency (CSA) / Agence spatiale canadienne (ASC).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 * 	Olivier L. Larouche (Olivier.LLarouche@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 */
package ca.gc.asc_csa.apogy.common.emf.impl;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFPackage;
import ca.gc.asc_csa.apogy.common.emf.Named;
import ca.gc.asc_csa.apogy.common.emf.NamedComparator;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Named Comparator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class NamedComparatorImpl<T extends Named> extends EComparatorImpl<T> implements NamedComparator<T> {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NamedComparatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ApogyCommonEMFPackage.Literals.NAMED_COMPARATOR;
	}

	@Override
	public int compare(T o1, T o2) 
	{
		String name1 = o1.getName();
		String name2 = o2.getName();
		
		if(name1 == null && name2 != null)
		{
			return -1;
		}		
		else if(name1 != null && name2 == null)
		{
			return 1;
		}		
		else if(name1 == null && name2 == null)
		{
			return 0;
		}
		else
		{		
			return name1.compareTo(name2);
		}
	}

	
} //NamedComparatorImpl
