package ca.gc.asc_csa.apogy.common.emf;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import javax.measure.unit.NonSI;
import javax.measure.unit.SI;
import javax.measure.unit.UnitFormat;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import ca.gc.asc_csa.apogy.common.log.EventSeverity;
import ca.gc.asc_csa.apogy.common.log.Logger;

public class Activator implements BundleActivator 
{
	public static final String ID = "ca.gc.asc_csa.apogy.common.emf";
	
	/*
	 * Refers to the default Editing Domain.
	 */
	public static final String APOGY_DEFAULT_EDITING_DOMAIN_ID = ID + ".editingDomain";
	
	// Adds some aliases to units.
	static
	{
		UnitFormat.getInstance().alias(SI.VOLT, "volt");		
		
		UnitFormat.getInstance().alias(NonSI.DEGREE_ANGLE, "deg");		
		UnitFormat.getInstance().alias(NonSI.DEGREE_ANGLE, "degree");
		
		UnitFormat.getInstance().alias(NonSI.FOOT, "foot");
		UnitFormat.getInstance().alias(NonSI.FOOT, "feet");				
	}
	
	private static BundleContext context;
	
	static BundleContext getContext() {
		return context;
	}
	
	@Override
	public void start(BundleContext context) throws Exception {
		Activator.context = context;	
		
		try
		{
			ApogyCommonEMFFacade.INSTANCE.getAllSubEClasses(ApogyCommonEMFPackage.Literals.APOGY_COMMON_EMF_FACADE);
			Logger.INSTANCE.log(ID, Activator.this, "EMF Class Definitions loaded", EventSeverity.INFO);				
		}
		catch (UnsatisfiedLinkError e)
		{
			Logger.INSTANCE.log(ID, Activator.this, "Error while loading EMF Class Definitions", EventSeverity.ERROR, e);
		}
		catch (Throwable e)
		{
			Logger.INSTANCE.log(ID, Activator.this, "Error while loading EMF Class Definitions", EventSeverity.ERROR, e);
		}	
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		Activator.context = null;
	}
}
