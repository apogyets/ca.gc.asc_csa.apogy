package ca.gc.asc_csa.apogy.common.emf;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/

import java.util.List;

public class DefaultListEventDelegate<T> implements ListEventDelegate<T> {

	@Override
	public void added(T element) {

	}

	@Override
	public void addedMany(List<T> elements) {
		for (T t : elements) {
			added(t);
		}

	}

	@Override
	public void removed(T element) {

	}

	@Override
	public void removedMany(List<T> elements) {
		for (T t : elements) {
			removed(t);
		}

	}

}
