package ca.gc.asc_csa.apogy.core.programs.controllers.provider;
/********************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency 
 * Contributors:
 *     Pierre Allard (Pierre.Allard@canada.ca), 
 *     Regent L'Archeveque (Regent.Larcheveque@canada.ca),
 *     Sebastien Gemme (Sebastien.Gemme@canada.ca),
 *     Canadian Space Agency (CSA) - Initial API and implementation
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 1.0 which is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * SPDX-License-Identifier: EPL-1.0
********************************************************************************/


import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;

import javax.measure.unit.Unit;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import ca.gc.asc_csa.apogy.common.emf.ApogyCommonEMFFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFacade;
import ca.gc.asc_csa.apogy.common.emf.ui.ApogyCommonEMFUIFactory;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersFormatProviderParameters;
import ca.gc.asc_csa.apogy.common.emf.ui.EOperationEParametersUnitsProviderParameters;
import ca.gc.asc_csa.apogy.core.programs.controllers.ApogyCoreProgramsControllersPackage;
import ca.gc.asc_csa.apogy.core.programs.controllers.LinearInputConditioning;
import ca.gc.asc_csa.apogy.core.programs.controllers.ValueSource;

/**
 * This is the item provider adapter for a {@link ca.gc.asc_csa.apogy.core.programs.controllers.LinearInputConditioning} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc --> * @generated
 */
public class LinearInputConditioningItemProvider
	extends AbstractInputConditioningItemProvider 
		{
	
	private DecimalFormat decimalFormat = new DecimalFormat("0.000");
	
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	public LinearInputConditioningItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addMinimumPropertyDescriptor(object);
			addMaximumPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Minimum feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void addMinimumPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinearInputConditioning_minimum_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinearInputConditioning_minimum_feature", "_UI_LinearInputConditioning_type"),
				 ApogyCoreProgramsControllersPackage.Literals.LINEAR_INPUT_CONDITIONING__MINIMUM,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Maximum feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	protected void addMaximumPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinearInputConditioning_maximum_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinearInputConditioning_maximum_feature", "_UI_LinearInputConditioning_type"),
				 ApogyCoreProgramsControllersPackage.Literals.LINEAR_INPUT_CONDITIONING__MAXIMUM,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns LinearInputConditioning.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LinearInputConditioning"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated_NOT
	 */
	@Override
	public String getText(Object object) 
	{
		String label = getString("_UI_LinearInputScalling_type");		
		LinearInputConditioning linearInputScalling = (LinearInputConditioning)object;

		if (linearInputScalling.eContainer() instanceof ValueSource) {
			ValueSource valueSource = (ValueSource) linearInputScalling.eContainer();
			if (valueSource.getBindedEDataTypeArgument() != null
					&& valueSource.getBindedEDataTypeArgument().getEParameter() != null) {
				Unit<?> modelUnit = ApogyCommonEMFFacade.INSTANCE
						.getEngineeringUnits(valueSource.getBindedEDataTypeArgument().getEParameter());
				if (modelUnit != null) {
					EOperationEParametersUnitsProviderParameters unitsParams = ApogyCommonEMFUIFactory.eINSTANCE
							.createEOperationEParametersUnitsProviderParameters();
					unitsParams.setParam(valueSource.getBindedEDataTypeArgument().getEParameter());
					Unit<?> displayUnit = ApogyCommonEMFUIFacade.INSTANCE.getDisplayUnits(
							valueSource.getBindedEDataTypeArgument().getEParameter().getEOperation(), unitsParams);

					EOperationEParametersFormatProviderParameters formatParams = ApogyCommonEMFUIFactory.eINSTANCE
							.createEOperationEParametersFormatProviderParameters();
					formatParams.setParam(valueSource.getBindedEDataTypeArgument().getEParameter());
					DecimalFormat format = ApogyCommonEMFUIFacade.INSTANCE.getDisplayFormat(
							valueSource.getBindedEDataTypeArgument().getEParameter().getEOperation(), formatParams);

					float min = linearInputScalling.getMinimum();
					float max = linearInputScalling.getMaximum();

					if (displayUnit != null && displayUnit != modelUnit) {
						min = ((Double) modelUnit.getConverterTo(displayUnit).convert(min)).floatValue();
						max = ((Double) modelUnit.getConverterTo(displayUnit).convert(max)).floatValue();
					}

					label += " (min " + format.format(min);
					label += ", max " + format.format(max);
				} else {
					label += " (min " + decimalFormat.format(linearInputScalling.getMinimum());
					label += ", max " + decimalFormat.format(linearInputScalling.getMaximum());
				}

			}
		}

		return label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LinearInputConditioning.class)) {
			case ApogyCoreProgramsControllersPackage.LINEAR_INPUT_CONDITIONING__MINIMUM:
			case ApogyCoreProgramsControllersPackage.LINEAR_INPUT_CONDITIONING__MAXIMUM:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
